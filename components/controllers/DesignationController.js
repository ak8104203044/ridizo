'use strict';
radizoApp.controller('DesignationController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'designation_masters';
    $scope.exportFile = 'designation';
    $scope.caption = 'Designation';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.arrDepartmentData = [];
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.memberFirmId = localStorage.getItem('firmId');

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.init = function() {
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Name',type:2,column:'name',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Code',type:2,column:'code',sort:true,sort_value:2,width:"20%",align:"left"},
            {caption:'Department',type:2,column:'department',sort:true,sort_value:3,width:"20%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"30%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    }

    $scope.add = function() {
        if($scope.memberFirmId !== null) {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.id = null;
            $scope.bindScopeForm.order_no = localStorage.getItem('orderNo');
            $scope.getDepartmentList(0);
        }
    }

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            $controller('CommonFilterComponent', {$scope: $scope});
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.getDepartmentList(0);
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.memberFirmId !== null) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            saveForms.append('branch_master_id',$scope.memberFirmId);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    if($rootScope.btnGroup.status == 1) {
                        $state.go($scope.baseStateTo);
                    } else {
                        var orderNo = angular.copy($scope.bindScopeForm.order_no);
                        orderNo = (orderNo > 0) ? Number(orderNo) + 1: 1;
                        $scope.bindScopeForm = {};
                        $scope.formSubmission.$setPristine();
                        $scope.bindScopeForm.order_no = orderNo;
                        localStorage.setItem('orderNo',orderNo);
                    }
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    }
}]);
