'use strict';
radizoApp.controller('VehicleSaleController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', '$controller',function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, $controller) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'invoice_masters';
    $scope.exportFile = 'vehicle sale';
    $scope.caption = 'Vehicle Sale';
    $scope.bindScopeForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.vehicleType = 2;
    $scope.searchVehicleType = 2;
    $scope.vehicleStatus = [1];
    $scope.isVehicleBranch = 1;
    $scope.expenseRow = [];
    $scope.vehicleValidationType = 3;
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
        $scope.dataTablePath = $scope.statePath + '/index/5';
        $scope.userRights.cancel_invoice = 1;
        delete $scope.userRights.delete;
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Invoice No',type:2,column:'invoice_no',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Invoice Date',type:2,column:'invoice_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:true,sort_value:3,width:"10%",align:"left"},
            {caption:'Vehicle Code',type:2,column:'customer',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Payment Status',type:5,column:'',sort:false,sort_value:4,width:"10%",align:"left"},
            {caption:'Status',type:2,column:'vehicle_status',sort:false,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:6,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:5}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
        $controller('CommonFilterComponent', {$scope: $scope});
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $scope.arrGuarenteeData = [{id:1,name:'1st Year'},{id:2,name:'2nd Year'},{id:3,name:'3rd Year'},{id:4,name:'4th Year'},{id:5,name:'5th Year'}];
            $scope.bindScopeForm.guarantee = 1;
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.bindScopeForm.payment_mode = 1;
            $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
            $scope.bindScopeForm.invoice_date = AppUtilities.getDateFormat(new Date());
            $scope.getUniqueCode(2);
            $scope.getTitleList(0);
            $scope.getBankList(0);
            $scope.getCouponList(0);
            $scope.id = null;
        }
        AppUtilities.unblockUI();
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save_vehicle_invoice';
            var saveForms = new FormData($('#formSubmission')[0]);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('actual_amount',$scope.bindScopeForm.actual_amount);
            saveForms.append('vehicle_master_id',$scope.bindScopeForm.vehicle_master_id);
            saveForms.append('customer_master_id',$scope.bindScopeForm.customer_master_id);
            saveForms.append('invoice_json',JSON.stringify($scope.bindScopeForm));
            if(typeof $scope.bindScopeForm.shortFees !== "undefined") {
                saveForms.append('shortFees',$scope.bindScopeForm.shortFees);
            } else {
                saveForms.append('shortFees',0);
            }
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    };

    $scope.getTitleList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrTitleData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_title_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTitleData = angular.copy(response.data);
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response){
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
    };

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode) {
                    AppUtilities.handleResponse({status:0,'message':'Please define invoice code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleJobCode = '';
                AppUtilities.handleResponse({status:0,'message':'Please define invoice code setting!..'});
                $state.go($scope.baseStateTo, {}, { reload: true });
            });
    };

    $scope.cancelInvoice = function() {
        AppUtilities.confirmDeletion(2)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/cancel_invoice/5/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getBankList = function(flag = 0) {
        $scope.arrBankData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_bank_list';
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrBankData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.getCouponList = function(flag = 0,type = 1) {
        $scope.arrCouponData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_coupon_detail/'+$scope.memberFirmId+'/' + type;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrCouponData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/5/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    };
}]);
