'use strict';
radizoApp.controller('UsersController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities', function($rootScope, $scope, $state, AppService, AppUtilities) {
    console.log('login controller started!....');
    $scope.userLoginData = {};
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'users';
    $scope.submitBtn = {name:'Login <i class="m-icon-swapright m-icon-white"></i>',is_disabled :false};
    $scope.login = function() {
        if ($scope.loginForm.$valid) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Please Wait ...';
            $scope.submitBtn.is_disabled = true;
            $scope.userLoginData.from_type = 1;
            $scope.userLoginData.user_login = 1;
            var formdata = $scope.userLoginData;
            var path = $scope.statePath + '/login';
            AppService.postHttpRequest(path, formdata)
                .success(function(data) {
                    var params = { type: 1, message: data.message }
                    AppUtilities.toaster(params);
                    //console.log(data);
                    localStorage.setItem("userId", data.user_detail.id);
                    localStorage.setItem("fullName", data.user_detail.fullname);
                    localStorage.setItem("userType", data.user_detail.user_type);
                    localStorage.setItem("userTokenId", data.user_detail.token_id);
                    localStorage.setItem("userSessId", data.user_detail.sessid);
                    localStorage.setItem("roleId", data.user_detail.role_id);
                    localStorage.setItem('sidebarClosed',1);
                    AppUtilities.unblockUI();
                    $('.alert').removeClass('display-hide').removeClass('alert-danger');
                    $('.alert').addClass('alert-success');
                    $state.go('dashboard', {}, { reload: true });
                })
                .error(function(response) {
                    $('#SubmitBtn').attr('disabled', false);
                    $scope.submitBtn.name = 'Login <i class="m-icon-swapright m-icon-white"></i>';
                    $scope.submitBtn.is_disabled = true;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var params = { type: 2, message: 'Please Enter User Name & Password' }
            AppUtilities.toaster(params);
        }
    }

    $scope.changePassword = function() {
        if ($scope.userForms.$valid) {
            $('#updatePwd').attr('disabled', true);
            $('#updatePwd').html('Updating');
            AppUtilities.blockUI(1);
            var saveForms = $scope.userBindForm;
            var path = $scope.statePath + '/change_password';
            AppService.postHttpRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go('dashboard', null, { reload: true });
                })
                .error(function(response) {
                    $('#updatePwd').attr('disabled', false);
                    $('#updatePwd').html('Update');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        }
    }

}]);