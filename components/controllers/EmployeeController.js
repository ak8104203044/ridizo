'use strict';
radizoApp.controller('EmployeeController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', '$controller', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, $controller) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'employee_masters';
    $scope.exportFile = 'employee';
    $scope.caption = 'Employee';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.memberTab = 1;
    $scope.arrServiceGroupData = [];
    $scope.arrGender = ['Male', 'Female', 'Transgender'];
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Employee Name',type:2,column:'name',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Employee Code',type:2,column:'code',sort:true,sort_value:2,width:"10%",align:"left"},
            {caption:'Username',type:2,column:'username',sort:false,sort_value:2,width:"10%",align:"left"},
            {caption:'Department',type:2,column:'department',sort:true,sort_value:3,width:"20%",align:"left"},
            {caption:'Designation',type:2,column:'designation',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"10%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        if($scope.memberFirmId !== null) {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.getEmployeeCode();
            $scope.getCountryList(0);
            $scope.getTitleList(0);
            $scope.getDepartmentList(0);
            $scope.employeeRoleList(0);
            $scope.id = null;
        }
        AppUtilities.unblockUI();
    };

    $scope.nextTab = function() {
        if($scope.EmployeeInfoForm.$valid) {
            $scope.memberTab = 2;
        } else {
            var response = { status: 0, message: 'Please filled up manadatory fields !.' };
            AppUtilities.handleResponse(response);
        }
    };

    $scope.save = function() {
        if ($scope.EmployeeOtherInfoForm.$valid && $scope.EmployeeInfoForm.$valid && $scope.memberFirmId > 0) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#EmployeeInfoForm')[0]);
            var empForms = $('#EmployeeOtherInfoForm').serializeArray();
            angular.forEach(empForms,function(element){
                saveForms.append(element.name,element.value);
            });
            saveForms.append('branch_master_id',$scope.memberFirmId);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
                saveForms.append('old_photo',$scope.bindScopeForm.photo);
            }
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    if (response.is_send_mail == 1) {
                        AppService.getHttpRequest($rootScope.ADMIN_API_URL + '/system/send_mail/2/' + response.id);
                    }
                    $state.go($scope.baseStateTo);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            var response = { status: 0, message: 'Please filled up manadatory fields !.' };
            AppUtilities.handleResponse(response);
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            $controller('CommonFilterComponent', {$scope: $scope});
            AppService.getHttpRequest($scope.statePath + '/record/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.memberLogo = $scope.memberPath + GLOBAL_DIR.LOGODIR + '/' + response.data.logo;
                    $scope.memberPhoto = $scope.memberPath + GLOBAL_DIR.MEMBERDIR + '/' + response.data.photo;
                    $scope.employeePhto = $rootScope.IMAGE_URL + '/employee' + '/' + response.data.photo;
                    $scope.getCountryList(0);
                    $scope.getStateList(0);
                    $scope.getCityList(0);
                    $scope.getTitleList(0);
                    $scope.getDepartmentList(0);
                    $scope.getDesignationList(0);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    if (response.data.photo != '') {
                        $scope.employeePhto = $rootScope.IMAGE_URL + '/employee' + '/' + response.data.photo;
                    } else {
                        $scope.employeePhto = $rootScope.IMAGE_URL + '/default.png';
                    }
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/' + $scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, {reload:true});
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getEmployeeCode = function() {
        var firmId = localStorage.getItem('firmId');
        if (firmId > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + firmId + '/1';
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.employeeCode = angular.copy(response.code);
                })
                .error(function(response) {
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo, {}, { reload: true });
                    $scope.employeeCode = '';
                });
        } else {
            AppUtilities.handleResponse({ 'status': 0, 'message': 'Please select firm first !..' });
            $state.go($scope.baseStateTo, {}, { reload: true });
        }
    };

    $scope.employeeRoleList = function(flag = 0) {
        AppUtilities.blockUI(1,'#roleDiv');
        $scope.arrEmployeeRoleData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_employee_role_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrEmployeeRoleData = angular.copy(response.data);
                AppUtilities.unblockUI('#roleDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#roleDiv');
            });
    };

    $scope.uploads = function(element) {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/upload_files';
        var params = { getFile: element, old_file: $('#logo').val(), path: path };
        AppUtilities.uploadFiles(params)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.employeePhto = response.src;
                $('#photo').val(response.filename);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    };
}]);
