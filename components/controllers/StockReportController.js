'use strict';
radizoApp.controller('StockReportController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$window', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$window) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'stock_report';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.module = $state.current.data.module;
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.bindScopeForm = {};
    $scope.oneAtATime = false;
    $scope.status = {open: true,filter:false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';
    $scope.submitBtn = {name:'Submit',is_disabled :false};
    
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.generateReportType = function(flag = 1) {
        flag = (flag > 0) ? Number(flag) : 1;
        $window.close();
        console.log('report flag is :',flag);
        if(flag === 1) {
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.arrSortByData = [{id:1,name:'Product Name'},{id:2,name:'Product Code'},{id:3,name:'HSN Code'},{id:4,name:'Category Name'}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.getCategoryList();
            $scope.getBranchList();
            $scope.getTaxGroupList();
            $scope.getUnitList();
        } else if(flag === 2) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrSortByData = [{id:1,name:'Voucher No'},{id:2,name:'Voucher Date'},{id:3,name:'Supplier'}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getCategoryList();
            $scope.getBranchList();
            $scope.getSupplierList();
            $scope.getCompanyYearList();
        } else if(flag === 3 || flag === 4) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrPaymentStatusData = [{id:1,name:'Unpaid'},{id:2,name:'Partial Payment'},{id:3,name:'Paid'}];
            $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
            $scope.arrSortByData = [{id:1,name:'Invoice No'},{id:2,name:'Invoice Date'},{id:3,name: (flag ===2) ? 'Customer Name' : 'Supplier'}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            if(flag === 3) {
                $scope.getSupplierList();
            }
            $scope.getCategoryList();
            $scope.getBranchList();
            $scope.getCompanyYearList();
        } else {
        }
    }

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var path,caption,fileName;
            flag = (flag > 0) ? Number(flag) : 1;
            if(flag === 1) {
                localStorage.setItem('reportType',1);
                path = $scope.statePath + '/product_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Product Dynamic Report';
                fileName = 'stock/product_dynamic_report.html';
            } else if(flag === 2) {
                localStorage.setItem('reportType',2);
                path = $scope.statePath + '/purchase_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Purchase Report';
                fileName = 'stock/purchase_dynamic_report.html';
            } else if(flag == 3) {
                localStorage.setItem('reportType',2);
                path = $scope.statePath + '/purchase_order_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Purchase Order Report';
                fileName = 'stock/purchase_order_report.html';
            } else if(flag === 4) {
                localStorage.setItem('reportType',2);
                path = $scope.statePath + '/sale_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Sale Report';
                fileName = 'stock/sale_dynamic_report.html';
            } else {}
            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    if(typeof response.header !== "undefined" && response.header !== null) {
                        localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    } else {
                        localStorage.setItem('arrHeaderData',JSON.stringify([]));
                    }
                    if(typeof response.product !== "undefined" && response.product !== null) {
                        localStorage.setItem('arrProductData',JSON.stringify(response.product));
                    }
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    $scope.reportWindow = $window.open($scope.reportPath+'/report.html','StockReport','height=890,width=1280,resizable=1,location=no');
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.getBranchList = function() {
        $scope.arrBranchData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_branch_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBranchData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };

    $scope.getCompanyYearList = function() {
        $scope.arrCompnayYearData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_company_year_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCompnayYearData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };

    $scope.getCategoryList = function() {
        $scope.arrCategoryData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_category_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCategoryData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getProductList = function() {
        if($scope.bindScopeForm.category_master_id !== null && $scope.bindScopeForm.category_master_id > 0) {
            AppUtilities.blockUI(1);
            $scope.arrProductData = [];
            $scope.bindScopeForm.product_master_id = null;
            var path = $rootScope.ADMIN_API_URL + '/system/product_list/' + $scope.memberFirmId;
            var params = {'category_master_id':$scope.bindScopeForm.category_master_id}
            AppService.postHttpRequest(path,params)
                .success(function(response) {
                    $scope.arrProductData = angular.copy(response.data);
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                });
        } else {
            $scope.bindScopeForm.product_master_id = null;
            $scope.arrProductData = [];
        }
    }

    $scope.getSupplierList = function() {
        $scope.arrSupplierData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/supplier_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrSupplierData = angular.copy(response.data);
            }).error(function() {
            });
    }

    $scope.getTaxGroupList = function() {
        $scope.arrTaxGroupData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_tax_group_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTaxGroupData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getUnitList = function() {
        $scope.arrUnitData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_unit_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrUnitData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }
}]);
