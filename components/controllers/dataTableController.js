'use strict';
radizoApp.controller('dataTableController', ['$rootScope', '$scope', 'AppService','AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, AppService,AppUtilities, GLOBAL_DIR) {
    console.log('datatable controller called !');
    $scope.searchFilter = {};
    $scope.searchCollapsed = true;
    $scope.getFilterParam,$scope.filterSearchTxt,$scope.filterSearchId;
    $scope.dropdownToggle = { isopen: false};
    $scope.pagination = {current:1,itemPerPage:GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH,sortKey:0,reverse:true,entries:GLOBAL_DIR.PAGEDATA};

    $scope.initTable = function(page = 1) {
        AppUtilities.blockUI(1,'#datatable_ajax');
        if(sessionStorage.getItem($scope.module + 'filter') !== null) {
            $scope.searchFilter = JSON.parse(sessionStorage.getItem($scope.module+'filter'));
        } else {
            sessionStorage.clear();
        }
        var i;
        for(i in $scope.searchFilter) {
            if(i == "undefined") {
                delete $scope.searchFilter[i];
            } else if(typeof $scope.searchFilter[i] === "object") {
                delete $scope.searchFilter[i];
            } else {}
        }
        $scope.pagination.current = page;
        $scope.searchFilter.page = page;
        $scope.searchFilter.length = $scope.pagination.itemPerPage;
        $scope.searchFilter.sort_by = $scope.pagination.sortKey;
        $scope.searchFilter.sort_type = $scope.pagination.reverse;
        if(localStorage.getItem('firmId') !== null) {
            $scope.searchFilter.firm_id = localStorage.getItem('firmId');
        }

        if(localStorage.getItem('companyYearId') !== null) {
            $scope.searchFilter.company_year_id = localStorage.getItem('companyYearId');
        }

        if(localStorage.getItem('employeeId') !== null) {
            $scope.searchFilter.employee_master_id = localStorage.getItem('employeeId');
        }

        var path = (typeof $scope.dataTablePath !== "undefined" && $scope.dataTablePath !== null) ? $scope.dataTablePath : $scope.statePath + '/index';
        AppService.postHttpRequest(path,$scope.searchFilter)
                .success(function(response) {
                    $scope.searchCollapsed = true;
                    AppUtilities.unblockUI('#datatable_ajax');
                    $scope.pagination.records = angular.copy(response.records);
                    $scope.pagination.totalRecords = angular.copy(response.total);
                    $scope.start = angular.copy( response.start);
                    $scope.end = angular.copy(response.end);
                    if(typeof response.max_orderno !== "undefined") {
                        localStorage.setItem('orderNo',(response.max_orderno > 0) ? Number(response.max_orderno) : 1);
                    }
                }).error(function(response) {
                    $scope.pagination.records = [];
                    $scope.pagination.totalRecords = 0;
                    localStorage.setItem('orderNo',1);
                    AppUtilities.unblockUI('#datatable_ajax');
                    if(response.status == -1) {
                        AppUtilities.handleResponse(response);
                    }
                });
    };

    $scope.initExportTable = function(exportType = 1) {
        AppUtilities.blockUI(1);
        if(sessionStorage.getItem($scope.module + 'filter') !== null) {
            $scope.searchFilter = JSON.parse(sessionStorage.getItem($scope.module+'filter'));
        } else {
            sessionStorage.clear();
        }
        var i;
        for(i in $scope.searchFilter) {
            if(i == "undefined") {
                delete $scope.searchFilter[i];
            } else if(typeof $scope.searchFilter[i] === "object") {
                delete $scope.searchFilter[i];
            } else {}
        }

        $scope.searchFilter.sort_by = $scope.pagination.sortKey;
        $scope.searchFilter.sort_type = $scope.pagination.reverse;
        if(localStorage.getItem('firmId') !== null) {
            $scope.searchFilter.firm_id = localStorage.getItem('firmId');
        }

        if(localStorage.getItem('companyYearId') !== null) {
            $scope.searchFilter.company_year_id = localStorage.getItem('companyYearId');
        }

        if(localStorage.getItem('employeeId') !== null) {
            $scope.searchFilter.employee_master_id = localStorage.getItem('employeeId');
        }

        var path = (typeof $scope.exportPath !== "undefined" && $scope.exportPath !== null) ? $scope.exportPath : $scope.statePath + '/index/2';
        AppService.postHttpRequest(path,$scope.searchFilter)
                .success(function(response) {
                    console.log('records :', response);
                    AppUtilities.export(response.headers, response.records, $scope.exportFile + '_' + Date.now(), exportType);
                    AppUtilities.unblockUI();
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
    };

    $scope.sort = function(key) {
        $scope.pagination.reverse = ($scope.pagination.sortKey != key) ? true: !$scope.pagination.reverse;
        $scope.pagination.sortKey = key;
        $scope.initTable(1);
    };

    $scope.search = function() {
        if($scope.searchFilter.country_master_id === null) {
            $scope.searchFilter.searchText = '';
        }
        sessionStorage.setItem($scope.module + 'filter',JSON.stringify($scope.searchFilter));
        $scope.initTable(1);
    };

    $scope.reset = function() {
        $scope.searchFilter = {};
        sessionStorage.removeItem($scope.module + 'filter');
        $scope.initTable(1);
    };

    $scope.searchResult = function(search_type,loading,arrFilterData,getID,scopeType = 1) {
        scopeType = (scopeType > 0) ? Number(scopeType) : 1;
        $scope.getFilterParam = arrFilterData,
        $scope.filterSearchTxt = search_type,
        $scope.filterSearchId = getID;
        if(typeof $scope.searchFilter[search_type] !== null && $scope.searchFilter[search_type].length > 0) {
            $scope.searchFilter[loading] = 1;
            $scope.searchFilter[arrFilterData]  = [];
            $scope.getFilterParam = arrFilterData;
            var path,params = {};
            if(scopeType === 1) {
                params = {search_type:search_type,search_value:$scope.searchFilter[search_type]};
                path = $rootScope.ADMIN_API_URL + '/system/search_result';
            } else if(scopeType === 2) {
                path = $rootScope.ADMIN_API_URL + '/system/search_vehicle';
                params.vehicle_type = (typeof $scope.searchVehicleType !== "undefined" && $scope.searchVehicleType !== null) ? $scope.searchVehicleType : 1;
                if(typeof $scope.memberFirmId !== "undefined" && $scope.memberFirmId !== null) {
                    params.branch_master_id = $scope.memberFirmId;
                }
                params.search = $scope.searchFilter[search_type];
            } else if(scopeType === 3) {
                path = $rootScope.ADMIN_API_URL + '/system/search_customer';
                params.search_text = $scope.searchFilter[search_type];
                if(typeof $scope.memberFirmId !== "undefined" && $scope.memberFirmId !== null) {
                    params.branch_master_id = $scope.memberFirmId;
                }
            } else {
                params = {search_type:search_type,search_value:$scope.searchFilter[search_type]};
                path = $rootScope.ADMIN_API_URL + '/system/search_result';
            }

            if(localStorage.getItem('firmId') !== null) {
                params.branch_master_id = localStorage.getItem('firmId');
            }
            AppService.postHttpRequest(path,params)
                    .success(function(response) {
                        $scope.searchFilter[loading] = 0;
                        $scope.searchFilter[arrFilterData] = angular.copy(response.data);
                    }).error(function(response) {
                        $scope.searchFilter[getID] = null;
                        $scope.searchFilter[loading] = 0;
                    });
        } else {
            $scope.searchFilter[getID] = null;
            $scope.searchFilter[arrFilterData]  = [];
            $scope.searchFilter[loading] = 0;
        }
    };

    $scope.setValue = function(index,$event,search,arrFilterData,getID) {
        $scope.filterSearchTxt = '',$scope.filterSearchId ='';
        if(typeof $scope.searchFilter[arrFilterData][index] !== 'undefined' && $scope.searchFilter[arrFilterData][index] !== null) {
            if(typeof $scope.vehicleFrom !== "undefined" && $scope.vehicleFrom.length > 0) {
                $scope.searchFilter[getID] = $scope.searchFilter[arrFilterData][index].id;
                $scope.searchFilter[search] = $scope.searchFilter[arrFilterData][index].unique_no;
            } else {
                if(typeof $scope.searchFilter[arrFilterData][index][search] !== "undefined" && $scope.searchFilter[arrFilterData][index][search] !== null) {
                    $scope.searchFilter[getID] = $scope.searchFilter[arrFilterData][index].id;
                    $scope.searchFilter[search] = $scope.searchFilter[arrFilterData][index][search];
                } else {
                    $scope.searchFilter[getID] = $scope.searchFilter[arrFilterData][index].id;
                    $scope.searchFilter[search] = $scope.searchFilter[arrFilterData][index].name;
                }
            }
            $scope.filterSearchTxt = search;
            $scope.filterSearchId = getID;
            $scope.searchFilter[arrFilterData] = [];
        }
        $event.stopPropagation();
    };

    $scope.searchboxClicked = function($event) {
        if(typeof $scope.searchFilter[$scope.filterSearchId] === "undefined" || $scope.searchFilter[$scope.filterSearchId] === null) {
            $scope.searchFilter[$scope.filterSearchTxt] = '';
            $scope.searchFilter[$scope.getFilterParam] = [];
        }
        $event.stopPropagation();
    };
}]);
