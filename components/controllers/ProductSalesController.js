'use strict';
radizoApp.controller('ProductSalesController', ['$rootScope', '$scope', '$state', '$stateParams','$modal', 'AppService', 'AppUtilities', '$controller' ,'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams,$modal, AppService, AppUtilities,$controller, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'sale_masters';
    $scope.exportFile = 'sale';
    $scope.caption = 'Sale';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.saleRow = [{}];
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.arrTaxDetail = [];
    $scope.arrCustomerData = [];
    $scope.productDetail = {};
    $scope.tab = 1;
    $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.init = function() {
        $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
        $scope.getCustomerList();
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Invoice No',type:2,column:'unique_no',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Invoice Date',type:2,column:'invoice_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:true,sort_value:3,width:"10%",align:"left"},
            {caption:'Customer Name',type:2,column:'customer',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Status',type:5,column:'',sort:true,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left",payment_mode:true}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    }

    $scope.add = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $scope.id = null;
            $scope.invoice_date = AppUtilities.getDateFormat(new Date());
            $scope.getUniqueCode(2);
            $scope.getCustomerList();
        } else {
            AppUtilities.handleResponse({status:0,message:'Please select valid Branch & Company Year'});
            $state.go($scope.baseStateTo);
        }
    }

    $scope.nextTab = function() {
        console.log('here.....');
        if($scope.saleInvoiceForm.$valid) {
            $scope.tab = 2;
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    };

    $scope.edit = function(getId) {
        $scope.addRow = [];
        if (typeof getId !== 'undefined' && getId > 0) {
            $scope.getCustomerList(0);
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.addRow = angular.copy(response.product);
                    $scope.calculation();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            $('#record_save_btn').attr('disabled', true);
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
            AppUtilities.blockUI(1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id > 0) {
                saveForms.append('id',$scope.id);
            }
            saveForms.append('amount',$scope.productDetail.totalAmout);
            saveForms.append('item_detail',JSON.stringify($scope.addRow));
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.view = function(getId) {
        if (typeof getId !== 'undefined' && getId > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/1')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.addRow = angular.copy(response.product);
                    console.log('rows :', $scope.addRow);
                    $scope.calculation();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode) {
                    AppUtilities.handleResponse({status:0,'message':'Please define job code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleJobCode = '';
                AppUtilities.handleResponse({status:0,'message':'Please define job code setting!..'});
                $state.go($scope.baseStateTo, {}, { reload: true });
            });
    }

    $scope.getProductList = function(flag = 0) {
        $scope.arrProductData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        var path = $rootScope.ADMIN_API_URL + '/system/product_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrProductData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getItemDetail = function(row) {
        row.price = 0;
        row.tax = 0;
        row.quantity = 0;
        row.discount = 0;
        row.tax_detail =[];
        if(typeof row.product_master_id !== 'undefined' && row.product_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/product_detail/'+row.product_master_id+'/1';
            AppUtilities.blockUI(1);
            AppService.getHttpRequest(path)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    row.tax_detail = angular.copy(response.data.tax_detail);
                    row.price = angular.copy(response.data.price);
                    row.stockQty = Number(angular.copy(response.data.stock_qty));
                    if(row.stockQty > 0) {
                        row.quantity = 1;
                        row.qtyColor = 'green';
                    } else {
                        row.quantity = 0;
                        row.qtyColor = 'red';
                    }
                    row.item_price = angular.copy(response.data.price);
                    row.tax_name = angular.copy(response.data.tax_name);
                    $scope.productCalculation(row);
                }).error(function() {
                    AppUtilities.unblockUI();
                    $scope.calculation();
                });
        } else {
            $scope.productCalculation(row);
        }
    }

    $scope.productCalculation = function(rows) {
        $scope.isDuplicateProduct = 0;
        var isDuplicate = false;
        var sorted, i;
        rows.isDuplicate = 0;
        rows.total = 0;
        sorted = $scope.addRow.concat().sort(function(a, b) {
            if (a.product_master_id > b.product_master_id) return 1;
            if (a.product_master_id < b.product_master_id) return -1;
            return 0;
        });
        if (Object.keys($scope.addRow).length > 0) {
            for (i = 0; i < $scope.addRow.length; i++) {
                sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].product_master_id == sorted[i].product_master_id) || (sorted[i + 1] && sorted[i + 1].product_master_id == sorted[i].product_master_id));
                if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                    $scope.isDuplicateProduct = 1;
                    isDuplicate = true;
                } else {
                    sorted[i].isDuplicate =false;
                }
            }
        }

        if(rows.isDuplicate == false) {
            if(rows.stockQty <= 0) {
                rows.quantity = 0;
            } else {
                rows.quantity = (rows.stockQty < rows.quantity) ? rows.stockQty : rows.quantity
            }
            var totalPrice = 0;
            var itemPrice = 0;
            var quantity = (rows.quantity > 0) ? Number(rows.quantity) : 0;
            itemPrice = AppUtilities.numberFormat(rows.price,2);
            rows.discount = (rows.discount > 0) ? Number(rows.discount) : 0;
            var discountAmount = AppUtilities.numberFormat((itemPrice * rows.discount) /100,2);
            rows.discountAmount = discountAmount;
            itemPrice = AppUtilities.numberFormat(itemPrice - discountAmount,2);
            totalPrice = itemPrice * quantity;
            rows.total = AppUtilities.numberFormat(totalPrice,2);
            rows.taxes = [];
            if(typeof rows.tax_detail !== 'undefined' && Object.keys(rows.tax_detail).length > 0) {
                angular.forEach(rows.tax_detail,function(tax,index){
                    tax.price = itemPrice;
                    var taxAmount = $scope.calculateTax(tax);
                    rows.tax_detail[index]['tax_amount'] = taxAmount; 
                });
            }
            $scope.calculation();
        }
    }

    $scope.calculation = function() {
        $scope.arrTaxDetail = [];
        var totalDiscount = 0;
        var subTotalAmount = 0;
        var totalTax = 0;
        if($scope.addRow.length > 0) {
            angular.forEach($scope.addRow, function(rows,index) {
                var quantity = Number(rows.quantity);
                totalDiscount += rows.discountAmount;
                subTotalAmount += (typeof rows.total !== 'undefined') ? rows.total : 0;
                var itemTaxAmount = 0;
                if(typeof rows.tax_detail !== 'undefined' && Object.keys(rows.tax_detail).length > 0) {
                    angular.forEach(rows.tax_detail,function(tax,taxKey){
                        var itemTax = AppUtilities.numberFormat(tax.tax_amount * quantity,2);
                        totalTax += itemTax;
                        itemTaxAmount += itemTax;
                        if(typeof $scope.arrTaxDetail[taxKey] !== 'undefined') {
                            $scope.arrTaxDetail[taxKey]['price'] = AppUtilities.numberFormat($scope.arrTaxDetail[taxKey]['price'] + itemTax,2);
                        } else {
                            $scope.arrTaxDetail[taxKey] = {};
                            $scope.arrTaxDetail[taxKey]['name'] = tax.name;
                            $scope.arrTaxDetail[taxKey]['id'] = tax.id;
                            $scope.arrTaxDetail[taxKey]['price'] = itemTax;
                        }
                    });
                }
                rows.tax = AppUtilities.numberFormat(itemTaxAmount,2);
            });
            if(Object.keys($scope.arrTaxDetail).length > 0) {
                $scope.arrTaxDetail = Object.values($scope.arrTaxDetail);
            }
        }
        $scope.productDetail.totalTax = AppUtilities.numberFormat(totalTax,2);
        $scope.productDetail.totalDiscount = AppUtilities.numberFormat(totalDiscount,2);
        $scope.productDetail.subTotalAmount = AppUtilities.numberFormat(subTotalAmount,2);
        //var productTotalAmount = AppUtilities.numberFormat((subTotalAmount+totalTax),2);
        //var roundoff = Math.ceil(productTotalAmount);
        //console.log('roundoff :',roundoff);
        //$scope.productDetail.roundoff = roundoff;
        $scope.productDetail.totalAmout = AppUtilities.numberFormat((subTotalAmount+totalTax),2);
    }

    $scope.calculateTax = function(tax) {
        var taxAmount = 0;
        if(typeof tax !== 'undefined' && Object.keys(tax).length > 0) {
            var price = (tax.price > 0) ? AppUtilities.numberFormat(tax.price,2) : 0;
            var value = (tax.value > 0) ? AppUtilities.numberFormat(tax.value,2) : 0;
            var type = tax.type;
            if(type == 1) {
                value = (value > 100) ? 100 : value;
                taxAmount = AppUtilities.numberFormat((value * price) / 100,2);
            } else {
                value = (value > price) ? price : value;
                taxAmount = AppUtilities.numberFormat( (price - value),2);
            }
        }
        return taxAmount;
    }

    $scope.customerForm = function() {
        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/sale/customer_form.html';
        $scope.modalInstance = $modal.open({
            templateUrl: url,
            size: 'lg',
            scope: $scope,
            resolve:{
                arrCustomerData:function() {
                    return $scope.arrCustomerData;
                }
            }
        });
    }

    $scope.saveCustomer = function() {
        if ($scope.customerSubmission.$valid && $scope.memberFirmId > 0) {
            $('#update_btn').attr('disabled', true);
            $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
            var savePath = $rootScope.ADMIN_API_URL + '/' + 'customer_masters/save';
            var saveForms = new FormData($('#customerSubmission')[0]);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    $scope.modalInstance.close();
                    $scope.getCustomerList(0);
                    AppUtilities.handleResponse({status:1,message:'Customer added successfully !.'});
                })
                .error(function(response) {
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.getCustomerList = function(flag = 0) {
        $scope.arrCustomerData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_customer_list/'+$scope.memberFirmId;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrCustomerData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getBankList = function(flag = 0) {
        $scope.arrBankData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_bank_list';
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrBankData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.payment_form = function(id) {
        AppUtilities.blockUI(1);
        AppService.getHttpRequest($scope.statePath + '/payment_detail/' + id)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    $scope.getBankList();
                    $scope.payment_mode = 1;
                    $scope.sale_master_id = id;
                    var pdate = AppUtilities.getDateFormat(new Date());
                    $scope.payment_date = pdate;
                    $scope.payment_mode_date = pdate;
                    $scope.ptotalAmount = AppUtilities.numberFormat(response.data.amount,2);
                    $scope.partAmount = AppUtilities.numberFormat(response.data.part_amount,2);
                    $scope.totalDueAmount = AppUtilities.numberFormat(($scope.ptotalAmount - $scope.partAmount),2);
                    $scope.paid = $scope.totalDueAmount;
                    var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/sale/payment_form.html';
                    $scope.modalInstance = $modal.open({
                        animation:true,
                        templateUrl: url,
                        controller:'ProductSalesController',
                        backdrop:'static',
                        keyboard:false,
                        windowClass: 'modal',
                        size: 'lg',
                        scope: $scope
                    });
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
    }

    $scope.payment = function() {
        var total = AppUtilities.numberFormat($scope.totalDueAmount,2);
        var paid = AppUtilities.numberFormat($scope.paid,2);
        $scope.shortFees = 0;
        if(total > paid) {
            $scope.shortFees = AppUtilities.numberFormat(total - paid,2);
        } else {
            $scope.paid = total;
        }
    }

    $scope.savePayment = function() {
        if ($scope.paymentFormSubmission.$valid) {
            $('#update_btn').attr('disabled', true);
            $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
            var savePath = $scope.statePath + '/save_payment';
            var saveForms = new FormData($('#paymentFormSubmission')[0]);
            saveForms.append('total_amount',$scope.ptotalAmount);
            saveForms.append('id',$scope.sale_master_id);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    $scope.modalInstance.close();
                    $state.go($scope.baseStateTo, {}, { reload: true });
                })
                .error(function(response) {
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.priceValidator = function() {
        if($scope.part_amount > $scope.ptotalAmount) {
            return 'partial amount must be less than total amount';
        }
    }

    $scope.addRecord = function() {
        $scope.addRow.push({});
    }

    $scope.deleteRecord = function(rows,i) {
        var text = 'Do you want to delete this product ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                if($scope.id !== null && $scope.id > 0 && rows.sale_tran_id !== 'undefined' && rows.sale_tran_id > 0) {
                    AppUtilities.blockUI(1);
                    var path = $scope.statePath + '/delete_product/'+$scope.id+'/'+rows.sale_tran_id;
                    AppService.getHttpRequest(path)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            $scope.addRow.splice(i, 1);
                            $scope.calculation();
                        }).error(function() {
                            AppUtilities.unblockUI();
                            $scope.calculation();
                        });
                } else {
                    $scope.addRow.splice(i, 1);
                    $scope.calculation();
                }
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            AppUtilities.blockUI(1);
            var exportPath = $scope.statePath + '/export/';
            AppService.getHttpRequest(exportPath)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.export(response.header, response.data, $scope.exportFile + '_' + response.date, exportType);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    }
}]);