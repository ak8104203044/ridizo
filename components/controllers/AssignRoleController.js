'use strict';
radizoApp.controller('AssignRoleController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'user_role_trans';
    $scope.caption = 'Assign User Role';
    $scope.bindScopeForm = {};
    $scope.rights = localStorage.getItem('userRights');
    $scope.isMemberhide = 1;
    $scope.isMemberTypeManadatory = 0;
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.getMemberTypeList = function() {
        var path = $rootScope.ADMIN_API_URL + '/system/get_member_type_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrMemberTypeData = angular.copy(response.data);
            });
    }

    $scope.getRoleList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrMemberRoleData = [];
        $scope.arrUserTypeData = [];
        $scope.bindScopeForm.user_type_id = '';
        $scope.bindScopeForm.role_master_id = '';
        $scope.isMemberhide = 1;
        $scope.isMemberTypeManadatory = 0;
        $scope.showRole = 0;
        if (typeof $scope.bindScopeForm.user_type_tran_id !== 'undefined' && $scope.bindScopeForm.user_type_tran_id !== null) {
            var userTypeDetail = $scope.arrMemberTypeData.find(function(element) {
                return (element.id == $scope.bindScopeForm.user_type_tran_id);
            });
            if (typeof userTypeDetail !== 'undefined' && userTypeDetail.role_type == 4) {
                $scope.isMemberhide = 0;
            }
            var path = $rootScope.ADMIN_API_URL + '/system/get_member_role_list/'+$scope.bindScopeForm.user_type_tran_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    if (typeof userTypeDetail !== 'undefined' && userTypeDetail.role_type == 4) {
                        $scope.isMemberTypeManadatory = 1;
                        $scope.getUserTypeList();
                    }
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.arrMemberRoleData = angular.copy(response.data);
                })
                .error(function() {
                    $scope.arrMemberRoleData = [];
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                });
        }
        if (flag == 1) {
            AppUtilities.unblockUI();
        }
    }

    $scope.getUserTypeList = function() {
        $scope.arrUserTypeData = [];
        $scope.showRole = 0;
        if (typeof $scope.bindScopeForm.user_type_tran_id !== 'undefined' && $scope.bindScopeForm.user_type_tran_id !== null) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_user_type_list';
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrUserTypeData = angular.copy(response.data);
                })
                .error(function() {
                    $scope.arrUserTypeData = [];
                });
        }
    }

    $scope.changeMember = function() {
        $scope.isEmployee = 0;
        $scope.isMemberTypeManadatory = 1;
        if (typeof $scope.bindScopeForm.user_type_id !== 'undefined' && $scope.bindScopeForm.user_type_id !== null) {
            $scope.isMemberTypeManadatory = 0;
        }
    }

    $scope.search = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            var searchPath = $scope.statePath + '/search';
            var searchForms = {};
            $scope.isEmployee = 0;
            $scope.showRole = 0;
            var userTypeDetail = $scope.arrMemberTypeData.find(function(element) {
                return (element.id == $scope.bindScopeForm.user_type_tran_id);
            });
            searchForms.role_type = (typeof userTypeDetail !== 'undefined') ? userTypeDetail.role_type : '';
            searchForms.role_master_id = $scope.bindScopeForm.role_master_id;
            searchForms.user_member_type_id = $scope.bindScopeForm.user_type_tran_id;
            searchForms.user_type_id = (typeof $scope.bindScopeForm.user_type_id === 'undefined') ? '' : $scope.bindScopeForm.user_type_id;
            AppService.postHttpRequest(searchPath, searchForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.showRole = 1;
                    if (typeof userTypeDetail !== 'undefined' && userTypeDetail.role_type == 4) {
                        $scope.isEmployee = 1;
                    }
                    $scope.roleListData = response.data;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        }
    }

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            saveForms.append('role_master_id',$scope.bindScopeForm.role_master_id);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log(response);
                    AppUtilities.handleResponse(response);
                    $state.go('dashboard.assign-role', {}, { reload: true });
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Save');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Save');
                });
        }
    }
}]);