'use strict';
radizoApp.controller('DashboardController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities', function($rootScope, $scope, $state, AppService, AppUtilities) {
    console.log('dashboard here :- ', $state.current.name);
    $scope.userName = localStorage.getItem('fullName');
    $scope.userId = localStorage.getItem('userId');
    $scope.userType = localStorage.getItem('userType');
    $scope.roleId = localStorage.getItem('roleId');
    $scope.arrMemberData = [];
    $scope.selectedMember = '';
    $scope.previousMember = '';
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'dashboard';

    $scope.$watch("$viewContentLoaded",function(){
        var path = $rootScope.ADMIN_API_URL + '/system/delete_product_tmp_stock';
        AppService.getHttpRequest(path)
            .success(function(response) {
                console.log()
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                if(response.status == -1) {
                    AppUtilities.handleResponse(response);
                }
            });
    });

    $scope.init = function() {
        AppUtilities.blockUI(1);
        $scope.getUserRights();
        var path = $scope.statePath + '/today_job';
        if(localStorage.getItem('firmId') !== null) {
            path += '/' + localStorage.getItem('firmId');
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.todayJob = response.data;
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                if(response.status == -1) {
                    AppUtilities.handleResponse(response);
                }
            });
    }

    $scope.getUserRights = function() {
        var path = $scope.statePath + '/get_user_rights/' + $scope.userId + '/' + $scope.roleId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                if(Object.keys(response.data).length > 0) {
                    localStorage.setItem('userRights', JSON.stringify(response.data));
                } else {
                    localStorage.clear();
                    AppUtilities.handleResponse({status:0,message:'User rights is not assigned'});
                    $state.go('login');
                }
            })
            .error(function(response) {
                localStorage.clear();
                AppUtilities.handleResponse({status:0,message:'User rights is not assigned'});
                $state.go('login');
            });
    }

    $scope.companySettings = function() {
        var path = $rootScope.ADMIN_API_URL + '/system/company_settings/';
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (typeof response.company !== 'undefined' && Object.keys(response.company).length > 0) {
                    $scope.companyYearName = response.company.caption;
                    localStorage.setItem('companyYearId', response.company.id);
                }
                if (typeof response.member !== 'undefined' && Object.keys(response.member).length > 0) {
                    if ($scope.userType >= 3 && $scope.userType <= 4) {
                        localStorage.setItem('firmId', response.member.id);
                        localStorage.setItem('firmName', response.member.firm_name);
                        $scope.firmName = response.member.firm_name;
                        localStorage.setItem('employeeId',response.member.employee_id);
                        localStorage.setItem('isUser',1);
                        $scope.isUser = 1;
                    } else {
                        $scope.arrMemberData = response.member;
                        $rootScope.arrMemberDetail = response.member;
                        $scope.isUser = 0;
                        localStorage.setItem('isUser',0);
                        if (localStorage.getItem('firmId') != null && localStorage.getItem('firmId') > 0) {
                            $scope.selectedMember = localStorage.getItem('firmId');
                            $scope.previousMember = localStorage.getItem('firmId');
                        } else {
                            $scope.selectedMember = response.default_memer;
                            $scope.previousMember = response.default_memer;
                            localStorage.setItem('firmId', response.default_memer);
                        }
                    }

                }
            })
            .error(function(response) {
                if(response.status == -1) {
                    AppUtilities.handleResponse(response);
                }
            });
    }

    $scope.changeMember = function() {
        var text = 'Do you want to change firm ?';
        AppUtilities.confirmBox(text)
            .success(function(response) {
                var response = { 'status': 1, 'message': 'Firm changed successfully!.' };
                AppUtilities.handleResponse(response);
                localStorage.setItem('firmId', $scope.selectedMember);
                $state.go('dashboard', {}, { reload: 'dashboard' });
            })
            .error(function(response) {
                $scope.selectedMember = $scope.previousMember
                return false;
            });
    }

    $scope.sidebarToggle = function() {
        var sidebarClosed = 1;
        console.log('localstorage :',localStorage.getItem('sidebarClosed'));
        if(localStorage.getItem('sidebarClosed') !== null) {
            sidebarClosed = Number(localStorage.getItem('sidebarClosed'));
        }
        sidebarClosed = (sidebarClosed === 0) ? 1 : 0;
        var body = $('body');
        var sidebar = $('.page-sidebar');
        var sidebarMenu = $('.page-sidebar-menu');
        $(".sidebar-search", sidebar).removeClass("open");
        $rootScope.settings.layout.pageBodySolid = false;
        if(sidebarClosed) {
            body.removeClass("page-sidebar-closed");
            sidebarMenu.removeClass("page-sidebar-menu-closed");
            if ($.cookie) {
                $.cookie('sidebar_closed', '0');
            }
        } else {
            body.addClass("page-sidebar-closed");
            sidebarMenu.addClass("page-sidebar-menu-closed");
            if (body.hasClass("page-sidebar-fixed")) {
                sidebarMenu.trigger("mouseleave");
            }
            if ($.cookie) {
                $.cookie('sidebar_closed', '1');
            }
        }
        $(window).trigger('resize');
        localStorage.setItem('sidebarClosed',sidebarClosed);
    }

    $scope.logout = function() {
        var text = 'Do you want to Logout ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var path = $rootScope.ADMIN_API_URL + '/users/logout/';
                var userPostData = {
                    user_id: localStorage.getItem('userId'),
                    token_id: localStorage.getItem('userTokenId'),
                    sessid: localStorage.getItem('userSessId'),
                };
                AppService.postHttpRequest(path, userPostData)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        localStorage.clear();
                        AppUtilities.handleResponse(response);
                        $state.go('login');
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go('login');
                    });
            })
            .error(function(response) {
                return false;
            });
    }
}]);