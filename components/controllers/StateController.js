'use strict';
radizoApp.controller('StateController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller, GLOBAL_DIR) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'state_masters';
    $scope.exportFile = 'state';
    $scope.caption = 'State';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.arrCountryData = [];
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.submitBtn = {btn1:'Submit',btn2:'Submit & New',is_disabled :false,status:1};
    
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.init = function() {
        $scope.dataTablePath = $scope.statePath + '/index';
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Name',type:2,column:'name',sort:true,sort_value:1,width:"30%",align:"left"},
            {caption:'Code',type:2,column:'code',sort:true,sort_value:2,width:"30%",align:"left"},
            {caption:'Country',type:2,column:'country',sort:true,sort_value:3,width:"20%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"30%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.add = function() {
        $scope.id = null;
        $scope.bindScopeForm.order_no = localStorage.getItem('orderNo');
        $scope.getCountryList();
    };

    $scope.edit = function(getId) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            $scope.getCountryList();
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    if($rootScope.btnGroup.status == 1) {
                        $state.go($scope.baseStateTo);
                    } else {
                        var orderNo = angular.copy($scope.bindScopeForm.order_no);
                        orderNo = (orderNo > 0) ? Number(orderNo) + 1: 1;
                        $scope.bindScopeForm = {};
                        $scope.formSubmission.$setPristine();
                        $scope.bindScopeForm.order_no = orderNo;
                        localStorage.setItem('orderNo',orderNo);
                    }
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.view = function(getId) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getCountryList = function(flag = 0) {
        var path = $rootScope.ADMIN_API_URL + '/system/get_country_list';
        AppUtilities.blockUI(1,'#countryDiv');
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI('#countryDiv');
                $scope.arrCountryData = angular.copy(response.data);
            }).error(function() {
                AppUtilities.unblockUI('#countryDiv');
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
