'use strict';
radizoApp.controller('PurchaseOrderController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller','$modal','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller,$modal,GLOBAL_DIR) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'invoice_masters';
    $scope.exportFile = 'perfoma-invoice';
    $scope.caption = 'Perfoma Invoice';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.hideTable = 0;
    $scope.bindScopeForm.payment_mode = 1;
    $scope.searchPurchaseOrder = {};
    $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
    $scope.arrPaymentStatusData = [{id:0,name:"Pay Later"},{id:2,name:"Partial Payment"},{id:3,name:"Full Payment"}];
    $scope.submitBtn = {name:'Submit',is_disabled :false};
    $scope.message = GLOBAL_DIR.MESSAGE;

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.dataTablePath = $scope.statePath + '/index/1';
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Invoice No',type:2,column:'invoice_no',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Invoice Date',type:2,column:'invoice_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:true,sort_value:3,width:"15%",align:"left"},
            {caption:'Supplier',type:2,column:'customer',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Status',type:5,column:'',sort:true,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:6,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:1}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.getSupplierList(0);
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.id = null;
            $scope.purchaseWizard = 1;
            $scope.getSupplierList(0);
            $scope.getBankList(0);
        }
    };
    
    $scope.searchVoucher = function() {
        AppUtilities.blockUI(1);
        $scope.records = [];
        $scope.hideTable = 0;
        var path = $scope.statePath + '/search_purchase_order';
        $scope.searchPurchaseOrder.firm_id = $scope.memberFirmId;
        $scope.searchPurchaseOrder.company_year_id = $scope.companyYearId;
        AppService.postHttpRequest(path,$scope.searchPurchaseOrder)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.hideTable = 1;
                $scope.purchase_check = false;
                $scope.records = angular.copy(response.data);
                console.log('response : ',response);
            }).error(function(response) {
                AppUtilities.unblockUI();
                $scope.hideTable = 1;
                $scope.records = [];
            });
    };

    $scope.viewProductDetail = function(purchaseId = 0) {
        var path = $scope.statePath + '/purchase_product_detail/'+purchaseId+'/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.arrProductDetail = angular.copy(response.data);
                var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/purchase_order/product_detail.html';
                $scope.modalInstance = $modal.open({
                    templateUrl: url,
                    size: 'lg',
                    scope: $scope
                });
            }).error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    }

    $scope.wizardForm = function(tab = 1) {
        if(tab == 2) {
            var purchaseRecords = $scope.records.filter(function(element){
                return element.purchase_id == true;
            });
            if(typeof purchaseRecords !== "undefined" && purchaseRecords.length > 0) {
                var voucher;
                $scope.voucherRecords = {
                    id:[],
                    voucher_no:[],
                    supplier:[],
                    quantity:0
                };
                $scope.summary = {};
                for(voucher of purchaseRecords) {
                    if($scope.voucherRecords.id.indexOf(voucher.id) === -1) {
                        $scope.voucherRecords.id.push(voucher.id);
                    }
                    if($scope.voucherRecords.voucher_no.indexOf(voucher.voucher_no) === -1) {
                        $scope.voucherRecords.voucher_no.push(voucher.voucher_no);
                    }
                    if($scope.voucherRecords.supplier.indexOf(voucher.supplier) === -1) {
                        $scope.voucherRecords.supplier.push(voucher.supplier);
                    }
                    $scope.voucherRecords.quantity += Number(voucher.quantity);
                    $scope.voucherRecords.supplier_id = voucher.supplier_id;
                }
                AppUtilities.blockUI(1);
                var path = $scope.statePath + '/purchase_voucher_detail';
                var params = {
                    branch_master_id : $scope.memberFirmId,
                    id: $scope.voucherRecords.id
                };
                params.firm_id = $scope.memberFirmId;
                AppService.postHttpRequest(path,params)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            $scope.voucherRecords.products = angular.copy(response.data);
                            $scope.voucherRecords.invoice = response.invoice_detail.code;
                            $scope.bindScopeForm.invoice_date = AppUtilities.getDateFormat(new Date());
                            $scope.summary.qty = (response.qty > 0) ? Number(response.qty) : 0;
                            $scope.tab = tab;
                            $scope.purchaseWizard = tab;
                        }).error(function(response) {
                            AppUtilities.unblockUI();
                            AppUtilities.handleResponse(response);
                        });
            } else {
                AppUtilities.handleResponse({status:0,message:GLOBAL_DIR.MESSAGE.SELECT_ATLEAST_VALIDATION});
            }
        } else {
            $scope.bindScopeForm.invoice_date = AppUtilities.getDateFormat(new Date());
            $scope.purchaseWizard = tab;
        }
    };

    $scope.calculatePrice = function() {
        var purchasePrice = 0,totalPurchasePrice = 0;
        angular.forEach($scope.voucherRecords.products,function(product,index){
            if(typeof product.purchase_price !== "undefined" && product.purchase_price !== null) {
                purchasePrice =  AppUtilities.numberFormat(product.quantity * product.purchase_price,2);
                totalPurchasePrice += purchasePrice;
                $scope.voucherRecords.products[index]['subtotal'] = purchasePrice;
                $scope.voucherRecords.products[index]['total'] = purchasePrice;
            }
        });
        $scope.summary.purchase_price = AppUtilities.numberFormat(totalPurchasePrice,2);
        $scope.bindScopeForm.amount = AppUtilities.numberFormat(totalPurchasePrice,2);
        $scope.bindScopeForm.actual_amount = AppUtilities.numberFormat(totalPurchasePrice,2);
    };

    $scope.calculateAmt = function() {
        $scope.showShortFees = false;
        $scope.bindScopeForm.amount = ($scope.bindScopeForm.amount !== undefined) ? $scope.bindScopeForm.amount : "";
        if($scope.bindScopeForm.amount < $scope.bindScopeForm.actual_amount) {
            $scope.showShortFees = true;
            $scope.bindScopeForm.shortFees = AppUtilities.numberFormat($scope.bindScopeForm.actual_amount - $scope.bindScopeForm.amount,2);
        } else {
            $scope.bindScopeForm.amount = $scope.bindScopeForm.actual_amount;
            $scope.bindScopeForm.shortFees = 0;
        }
    };

    $scope.checkAll = function(behaviour) {
        $scope.records = $scope.records.map(function(element){
            element.purchase_id = behaviour;
            return element;
        });
    };

    $scope.save = function() {
        //if($scope.purchaseInvoiceForm.$valid) {
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var saleProduct = {
                product:$scope.voucherRecords.products,
                tax:[],
                voucher:{
                    supplier:$scope.voucherRecords.supplier,
                    supplier_id:$scope.voucherRecords.supplier_id,
                    id:$scope.voucherRecords.id,
                    quantity:$scope.voucherRecords.quantity
                },
                summary:{
                    subtotal:$scope.summary.purchase_price,
                    total:$scope.summary.purchase_price,
                    discount:0,
                    tax:0
                }
            };
            var savePath = $scope.statePath + '/save_purchase_invoice';
            var saveForms = new FormData($('#purchaseInvoiceForm')[0]);
            saveForms.append('short_Fees',(typeof $scope.bindScopeForm.shortFees !== "undefined") ? $scope.bindScopeForm.shortFees : 0);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('actual_amount',$scope.summary.purchase_price);
            saveForms.append('purchase_data',JSON.stringify(saleProduct));
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        /*} else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }*/
    };
    
    $scope.delete = function() {
        AppUtilities.confirmDeletion(2)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/cancel_invoice/1/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };
    
    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/1/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
