'use strict';
radizoApp.controller('BranchReportsController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities','$window', '$controller', function($rootScope, $scope, $state, AppService, AppUtilities,$window, $controller) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'branch_report';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.module = $state.current.data.module;
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.bindScopeForm = {};
    $scope.oneAtATime = false;
    $scope.status = {open: true,other:false,filter:false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.generateReportType = function(flag = 1) {
        if(flag === 1) {
            $scope.arrSortByData = [{id:1,name:"Member Code"},{id:2,name:"Name"},{id:3,name:"Country"},{id:4,name:"State"},{id:5,name:"City"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
        } else {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getBranchList(0);
            $scope.getCompanyYearList(0);
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
        }
    }

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            AppUtilities.blockUI(1);
            var path,caption,fileName;
            flag = (flag > 0) ? Number(flag) : 1;
            if(flag === 1) {
                path = $scope.statePath + '/member_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Member Dynamic Report';
                fileName = 'branch/member_dynamic_report.html';
                localStorage.setItem('reportType',1);
            } else if(flag === 2) {
                path = $scope.statePath + '/royalty_charge_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Royalty Charge Report';
                fileName = 'branch/royalty_charge_report.html';
                localStorage.setItem('reportType',2);
            }

            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    $scope.reportWindow = $window.open($scope.reportPath+'/report.html','StockReport','height=890,width=1280,resizable=1,location=no');
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

}]);
