'use strict';
radizoApp.controller('PurchaseController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'purchase_masters';
    $scope.exportFile = 'purchase';
    $scope.caption = 'Purchase';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.addRow = [];
    $scope.arrTaxDetail = [];
    $scope.arrProductData = [];
    $scope.productDetail = {};
    $scope.isSelectedProduct = false;
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.submitBtn = {name:'Submit',is_disabled :false};
    $scope.message = GLOBAL_DIR.MESSAGE;
    
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.getSupplierList(0);
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Voucher No',type:2,column:'voucher_no',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Date',type:2,column:'voucher_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Quantity',type:2,column:'quantity',sort:true,sort_value:3,width:"15%",align:"left"},
            {caption:'Supplier',type:2,column:'supplier',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $scope.id = null;
            $scope.bindScopeForm.voucher_date = AppUtilities.getDateFormat(new Date());
            $scope.getSupplierList(0);
        }
    };

    $scope.edit = function(getId = null) {
        $scope.addRow = [];
        if (getId !== null && getId != '') {
            $scope.getSupplierList(0);
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/1/'+$scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.quantity = "";
                    $scope.addRow = angular.copy(response.product);
                    $scope.totalQty = angular.copy(response.data.quantity);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.addRow.length > 0) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            saveForms.append('random',Math.random());
            saveForms.append('product_detail',JSON.stringify($scope.addRow));
            saveForms.append('total_qty',$scope.totalQty);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/2/'+$scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.addRow = angular.copy(response.product);
                    console.log('rows :', $scope.addRow);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getSupplierList = function(flag = 0) {
        if($scope.memberFirmId !== null) {
            $scope.arrSupplierData = [];
            flag = (typeof flag !== 'undefined') ? flag : 0;
            var path = $rootScope.ADMIN_API_URL + '/system/supplier_list/'+$scope.memberFirmId;
            if (flag == 1) {
                AppUtilities.blockUI(1);
            }
            AppService.getHttpRequest(path)
                .success(function(response) {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.arrSupplierData = angular.copy(response.data);
                }).error(function() {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                });
        }
    };

    $scope.searchProduct = function(searchValue) {
        $scope.isSelectedProduct = false;
        $scope.arrProductData = [];
        $scope.spinner = 1;
        var path = $rootScope.ADMIN_API_URL + '/system/product_list/'+$scope.memberFirmId;
        var params = {searchText:searchValue};
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrProductData = angular.copy(response.data);
            }).error(function() {
                $scope.spinner = 0;
            });
    };

    $scope.searchboxClicked = function($event) {
        $event.stopPropagation();
        $scope.arrProductData = [];
        if($scope.bindScopeForm.product === null) {
            $scope.bindScopeForm.quantity = '';
        }
    };

    $scope.getValue = function(index,$event) {
        if(typeof $scope.arrProductData[index] !== 'undefined' && $scope.arrProductData[index] !== null) {
            $scope.productDetail = angular.copy($scope.arrProductData[index]);
            $scope.bindScopeForm.product = $scope.arrProductData[index]['name'];
            $scope.arrProductData = [];
            $scope.isSelectedProduct = true;
        }
        $event.stopPropagation();
    };


    $scope.addProductDetail = function() {
        $scope.totalQty = 0;
        if($scope.isSelectedProduct === true && $scope.productDetail !== null && Object.keys($scope.productDetail).length > 0) {
            $scope.productDetail.quantity = Number($scope.bindScopeForm.quantity);
            var index = $scope.addRow.findIndex(function(element,index){
                return element.id == $scope.productDetail.id;
            });

            if(index !== -1) {
                $scope.addRow[index]['quantity'] = Number($scope.addRow[index]['quantity'] + $scope.productDetail.quantity); 
            } else {
                $scope.addRow.push($scope.productDetail);
            }

            $scope.totalQty = $scope.addRow.reduce(function(accumulator,item){
                return accumulator + Number(item.quantity);
            },0);
            $scope.productDetail = {};
            $scope.bindScopeForm.quantity = "";
            $scope.bindScopeForm.product = "";
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.VALID_PRODUCCT });
        }
    }

    $scope.deleteRecord = function(i) {
        $scope.addRow.splice(i, 1);
        $scope.totalQty = $scope.addRow.reduce(function(accumulator,item){
            return accumulator + Number(item.quantity);
        },0);
    };

    $scope.updateQuantity = function() {
        $scope.totalQty = $scope.addRow.reduce(function(accumulator,item){
            return accumulator + Number(item.quantity);
        },0);
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
