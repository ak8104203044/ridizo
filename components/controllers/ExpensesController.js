'use strict';
radizoApp.controller('ExpensesController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', '$controller','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, $controller,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'expense_masters';
    $scope.exportFile = 'expenses';
    $scope.caption = 'Expenses';
    $scope.bindScopeForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.addRow = [{}];
    $scope.expType = 2;
    $scope.vehicleValidationType = 2;
    $scope.vehicleType = 5;
    $scope.arrExpensesData = [];
    $scope.expenseHeadDetail = {};
    $scope.expenseRow = [];
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.arrExpenseTypeData = [{id:1,name:'Other'},{id:2,name:'Vehicle'}];
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Voucher No',type:2,column:'voucher',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Date',type:2,column:'date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Type',type:2,column:'type',sort:true,sort_value:3,width:"15%",align:"left"},
            {caption:'Vehicle',type:2,column:'vehicle',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null) {
            $scope.bindScopeForm.voucher_date = AppUtilities.getDateFormat(new Date());
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.expType = 1;
            $scope.id = null;
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save';
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                $scope.bindScopeForm.id = $scope.id;
            }
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.bindScopeForm.amount = $scope.expenseTotal;
            $scope.bindScopeForm.vehicle_master_id = $scope.bindScopeForm.vehicle_master_id;
            $scope.bindScopeForm.expense_detail = $scope.expenseRow;
            AppService.postHttpRequest(savePath, $scope.bindScopeForm)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.bindScopeForm = {};
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/'+$scope.memberFirmId+'/1')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $controller('CommonFilterComponent',{$scope:$scope});
                    $scope.bindScopeForm = angular.copy(response.data);
                    delete $scope.bindScopeForm.expense_json;
                    try {
                        $scope.expenseRow = JSON.parse(response.data.expense_json);
                    } catch(e) {
                        $scope.expenseRow = [];
                    }
                    $scope.expenseCalculation();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.view = function(getId) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/'+$scope.memberFirmId+'/2')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    try {
                        var expenses = JSON.parse(response.data.expense_json);
                        response.expense = expenses.map(function(element){
                            element.amount =  AppUtilities.numberFormat(element.amount);
                            return element;
                        });
                    } catch(e) {
                        response.expense = [];
                    }
                    $scope.totalAmount = AppUtilities.numberFormat(response.data.amount);
                    $scope.addRow = angular.copy(response.expense);
                    console.log($scope.addRow);
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.expenseType = function(type = 1) {
        if(type == 1) {
            $scope.bindScopeForm.vehicle_name = '';
            $scope.bindScopeForm.vehicle_master_id = null;
            $scope.expType = 1;
            if(typeof $scope.isVehicleExpense !== "undefined" && $scope.isVehicleExpense == 1) {
                if(typeof $scope.bindScopeForm.invoice_id !== "undefined" && $scope.bindScopeForm.invoice_id !== null) {
                    var expenseIndex = $scope.expenseRow.findIndex(function(element){
                        return (element.invoice_id == $scope.bindScopeForm.invoice_id);
                    });
                    if(expenseIndex >= 0) {
                        $scope.expenseRow.splice(expenseIndex,1);
                    }
                }
            }
        } else {
            $scope.expType = 2;
        }
    };

    $scope.validateForm = function() {
        if($scope.expenseGeneralForm.$valid) {
            $scope.tab = 2;
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.calculation = function(rows) {
        var sorted, i;
        $scope.isDuplicateExpense = 0;
        rows.isDuplicate = false;
        rows.price = (rows.price > 0) ? AppUtilities.numberFormat(rows.price,2) : 0;
        $scope.totalAmount = 0;
        var totalAmount = 0;
        sorted = $scope.addRow.concat().sort(function(a, b) {
            if (a.expense_head_master_id > b.expense_head_master_id) return 1;
            if (a.expense_head_master_id < b.expense_head_master_id) return -1;
            return 0;
        });
        if (Object.keys($scope.addRow).length > 0) {
            for (i = 0; i < $scope.addRow.length; i++) {
                sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].expense_head_master_id == sorted[i].expense_head_master_id) || (sorted[i + 1] && sorted[i + 1].expense_head_master_id == sorted[i].expense_head_master_id));
                if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                    $scope.isDuplicateExpense = 1;
                } else {
                    sorted[i].isDuplicate = false;
                }
            }
        }
        if(rows.isDuplicate == false) {
            if($scope.addRow.length > 0) {
                angular.forEach($scope.addRow, function(rows,index) {
                    var amount = (rows.price > 0) ? AppUtilities.numberFormat(rows.price,2) : 0;
                    totalAmount += amount;
                });
            }
        }
        $scope.totalAmount = AppUtilities.numberFormat(totalAmount,2);
    };

    $scope.addRecord = function() {
        $scope.addRow.push({});
    };

    $scope.deleteRecord = function(rows,i) {
        $scope.addRow.splice(i, 1);
        $scope.calculation(rows);
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
