'use strict';
radizoApp.controller('ExpenseReportController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities','$window', function($rootScope, $scope, $state, AppService, AppUtilities,$window) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'expense_report';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.module = $state.current.data.module;
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.bindScopeForm = {};
    $scope.oneAtATime = false;
    $scope.status = {open: true,other:false,filter:false};
    $scope.submitBtn = {name:'Submit',is_disabled :false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.generateReportType = function(flag = 1) {
        if(flag === 1) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrExpenseTypeData = [{id:1,name:"Other"},{id:2,name:"Vehicle"}];
            $scope.arrSortByData = [{id:1,name:"Voucher No"},{id:2,name:"Voucher Date"},{id:3,name:"Expense Type"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getBranchList();
            $scope.getCompanyYearList();
            $scope.getExpenseHeadList();
        } else {
        }
    };

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var path,caption,fileName;
            flag = (flag > 0) ? Number(flag) : 1;
            if(flag === 1) {
                localStorage.setItem('reportType',2);
                path = $scope.statePath + '/expense_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Expense Report';
                fileName = 'expense/expense_report.html';
            } else {}
            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    if(typeof response.expense !== "undefined" && response.expense !== null) {
                        localStorage.setItem('arrProductData',JSON.stringify(response.expense));
                    }
                    $scope.reportWindow = $window.open($scope.reportPath+'/report.html','ExpenseReport','height=890,width=1280,resizable=1,location=no');
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    };

    $scope.getBranchList = function() {
        $scope.arrBranchData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_branch_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBranchData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };

    $scope.getCompanyYearList = function() {
        $scope.arrCompnayYearData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_company_year_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCompnayYearData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };

    $scope.getExpenseHeadList = function() {
        $scope.arrExpenseHeadData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_expensehead_list/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrExpenseHeadData = angular.copy(response.data);
            }).error(function(response) {
            });
    };
}]);
