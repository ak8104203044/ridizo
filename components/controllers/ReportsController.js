'use strict';
var radizoApp = angular.module('radizoApp',[]);

radizoApp.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

radizoApp.controller('ReportsController', ['$rootScope', '$scope','$timeout','Excel',function($rootScope, $scope,$timeout,Excel) {
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.arrProductDetail = [];
    $scope.length = 0;
    $scope.reportType = 1;

    $scope.init = function() {
        $scope.fileName = localStorage.getItem('fileName');
        $scope.arrHeaderData = localStorage.getItem('arrHeaderData');
        $scope.arrReportData = localStorage.getItem('arrReportData');
        $scope.caption = localStorage.getItem('caption');
        if($scope.arrHeaderData !== null) {
            $scope.arrHeaderData = JSON.parse($scope.arrHeaderData);
            console.log('$scope.arrHeaderData :',$scope.arrHeaderData);
            $scope.length = Object.keys($scope.arrHeaderData).length;
        }
        
        if(localStorage.getItem('reportType') !== null) {
            $scope.reportType = localStorage.getItem('reportType');
        }

        console.log('$scope.reportType :',$scope.reportType);

        if(localStorage.getItem('arrProductData') !== null && $scope.reportType == 2) {
            $scope.arrProductDetail = JSON.parse(localStorage.getItem('arrProductData'));
        }

        if($scope.arrReportData !== null) {
            $scope.arrReportData = JSON.parse($scope.arrReportData);
        }
        console.log('$scope.arrProductDetail :',$scope.arrProductDetail);
        console.log('reports:',$scope.arrReportData);
    };

    $scope.getDateFormat = function(getDate, format) {
        format = (typeof format !== 'undefined') ? format : 'dd-mm-yyyy';
        var msec = Date.parse(getDate);
        var currentTime = new Date(msec);
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        month = (month < 9) ? ('0' + month) : month;
        day = (day < 9) ? ('0' + day) : day;
        var date;
        switch (format) {
            case 'dd-mm-yyyy':
                date = day + '-' + month + '-' + year;
                break;
            case 'dd/mm/yyyy':
                date = day + '/' + month + '/' + year;
                break;
            default:
                date = day + '-' + month + '-' + year;
                break;
        }
        return date;
    };

    $scope.exportType = function(exportType = 1) {
        try {
            if(exportType == 1) {
                var file = $scope.caption.replace(' ','-').toLowerCase();
                var fileName = file +'-'+$scope.getDateFormat(new Date())+'.xlsx';
                var getRecords = [];
                var getRecord,header,records;
                if($scope.reportType == 1) {
                    for (getRecord of $scope.arrReportData) {
                        records = {};
                        for(header of $scope.arrHeaderData) {
                            records[header['caption']] = getRecord[header.field];
                        }
                        getRecords.push(records);
                    }
                    alasql('SELECT * INTO XLSX("' + fileName + '",{headers:true}) FROM ?', [getRecords]);
                } else if($scope.reportType == 2) {
                    var exportHref = Excel.tableToExcel('#exportTableData','WireWorkbenchDataExport');
                    $timeout(function(){location.href = exportHref;},100);
                } else {}
            } else {
                
            }
        } catch(e) {
            console.log('error ',e);
        }
    };

    $scope.calculateLength = function(producdtLength,size) {
        $scope.captionSize = $scope.length + 2 + (producdtLength * size);
        $scope.productSize = (producdtLength * size);
    };
}]);
