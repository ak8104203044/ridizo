'use strict';
radizoApp.controller('ErrorLogsController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller',function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'error_logs';
    $scope.exportFile = 'errors-logs';
    $scope.caption = 'Error-Logs';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        statego = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(statego, { 'id': id });
    };

    $scope.init = function() {
        delete $scope.userRights.add;
        delete $scope.userRights.edit;
        $scope.dataTablePath = $scope.statePath + '/index/1';
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Controller',type:2,column:'controller',sort:true,sort_value:1,width:"10%",align:"left"},
            {caption:'Method',type:2,column:'method',sort:true,sort_value:2,width:"10%",align:"left"},
            {caption:'Request',type:2,column:'request',sort:false,sort_value:0,istextbox:1,width:"20%",align:"left"},
            {caption:'Description',type:2,column:'description',sort:false,sort_value:2,istextbox:1,width:"20%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                return false;
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    };
}]);
