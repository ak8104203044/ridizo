'use strict';
radizoApp.controller('EmployeeReportsController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$window', '$compile', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$window, $compile) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'employee_report';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.module = $state.current.data.module;
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.bindScopeForm = {};
    $scope.oneAtATime = false;
    $scope.status = {open: true,other:false,filter:false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.generateReportType = function(flag = 1) {
        if(flag === 1) {
            $scope.arrSortByData = [{id:1,name:"Employee Code"},{id:2,name:"Name"},{id:3,name:"Department"},{id:4,name:"Designation"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.getBranchList();
            $scope.getDepartmentList();
        } else {

        }
    }

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var path,caption,fileName;
            flag = (flag > 0) ? Number(flag) : 1;
            if(flag === 1) {
                path = $scope.statePath + '/employee_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Employee Dynamic Report';
                fileName = 'employee/dynamic_employee_report.html';
            }
            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    $scope.reportWindow = $window.open($scope.reportPath+'/report.html','StockReport','height=890,width=1280,resizable=1,location=no');
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.getBranchList = function() {
        $scope.arrBranchData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_branch_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBranchData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getDepartmentList = function(flag) {
        var path = $rootScope.ADMIN_API_URL + '/system/get_department_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrDepartmentData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getDesignationList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrDesignationData = [];
        if (typeof $scope.bindScopeForm.department_master_id !== 'undefined' && $scope.bindScopeForm.department_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_designation_list/'+$scope.memberFirmId + '/' +$scope.bindScopeForm.department_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrDesignationData = angular.copy(response.data);
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                })
                .error(function() {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.bindScopeForm.designation_master_id = '';
                });
        } else {
            if (flag == 1) {
                AppUtilities.unblockUI();
            }
            $scope.bindScopeForm.designation_master_id = '';
        }
    }
}]);
