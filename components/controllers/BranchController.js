'use strict';
radizoApp.controller('BranchController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'branch_masters';
    $scope.exportFile = 'member';
    $scope.caption = 'Franchise';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.memberTab = 1;
    $scope.arrServiceGroupData = [];
    $scope.memberPhoto = '';
    $scope.memberLogo = '';
    $scope.memberPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR + '/';
    $scope.arrGender = ['Male', 'Female', 'Transgender'];
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.init = function() {
        if(localStorage.getItem('arrMemberType') !== null) {
            $scope.arrSearchMemberTypeData = JSON.parse(localStorage.getItem('arrMemberType'));
        } else {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.getMemberTypeList(1);
        }
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Firm Name',type:2,column:'firm_name',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Member Code',type:2,column:'unique_no',sort:true,sort_value:2,width:"10%",align:"left"},
            {caption:'Member',type:2,column:'name',sort:true,sort_value:3,width:"10%",align:"left"},
            {caption:'UserName',type:2,column:'username',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Mobile No.',type:2,column:'mobile_no',sort:true,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.getMemberTypeList(0);
        $scope.getCountryList(0);
        $scope.getTitleList(0);
        $scope.getMemberCode(1);
        $scope.getRoyaltyChargeList(0);
        $scope.id = null;
        AppUtilities.unblockUI();
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.branchInformationForm.$valid) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#branchInformationForm')[0]);
            var branchForms = $('#formSubmission').serializeArray();
            angular.forEach(branchForms,function(element,key){
                saveForms.append(element.name,element.value);
            });
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
                saveForms.append('old_photo',$scope.bindScopeForm.photo);
                saveForms.append('old_logo',$scope.bindScopeForm.logo);
            }
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo,{},{reload:true});
                    if (response.is_send_mail == 1) {
                        AppService.getHttpRequest($rootScope.ADMIN_API_URL + '/system/send_mail/1/' + response.id);
                    }
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            $controller('CommonFilterComponent', {$scope: $scope});
            AppUtilities.blockUI(1);
            $scope.getCountryList(0);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.memberLogo = $scope.memberPath + GLOBAL_DIR.LOGODIR + '/' + response.data.logo;
                    $scope.memberPhoto = $scope.memberPath + GLOBAL_DIR.MEMBERDIR + '/' + response.data.photo;
                    $scope.getStateList(0);
                    $scope.getCityList(0);
                    $scope.getTitleList(0);
                    $scope.getMemberTypeList(0);
                    $scope.getRoyaltyChargeList(0);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.memberLogo = $scope.memberPath + GLOBAL_DIR.LOGODIR + '/' + response.data.logo;
                    $scope.memberPhoto = $scope.memberPath + GLOBAL_DIR.MEMBERDIR + '/' + response.data.photo;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: true });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.uploads = function(element, type) {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/upload_files';
        var params = { getFile: element, old_file: $('#logo').val(), path: path };
        AppUtilities.uploadFiles(params)
            .success(function(response) {
                AppUtilities.unblockUI();
                if (type == 1) {
                    $scope.memberPhoto = response.src;
                    $('#photo').val(response.filename);
                } else {
                    $scope.memberLogo = response.src;
                    $('#logo').val(response.filename);
                }
            }).error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    };

    $scope.nextTab = function() {
        if($scope.branchInformationForm.$valid) {
            $scope.memberTab = 2;
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
