'use strict';
radizoApp.controller('ProductStockController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, GLOBAL_DIR) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'product_stock';
    $scope.caption = 'Stock';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.index = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId) {
            AppService.getHttpRequest($scope.statePath + '/index/'+$scope.memberFirmId)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.productDetail = angular.copy(response.data);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
        }
    };

    $scope.add = function() {
        $scope.id = null;
        $scope.date = AppUtilities.getDateFormat(new Date());
        AppService.getHttpRequest($scope.statePath + '/index/'+$scope.memberFirmId+'/1')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.productDetail = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
    };

    $scope.save = function() {
        $('#record_save_btn').attr('disabled', true);
        $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
        AppUtilities.blockUI(1);
        var savePath = $scope.statePath + '/save';
        var saveForms = new FormData($('#formSubmission')[0]);
        saveForms.append('branch_master_id',$scope.memberFirmId);
        saveForms.append('company_year_master_id',$scope.companyYearId);
        AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $('#record_save_btn').attr('disabled', false);
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                    AppUtilities.handleResponse(response);
                });
    };
}]);