'use strict';
radizoApp.controller('VehicleRentController', ['$rootScope', '$scope', '$state', '$stateParams','$modal', 'AppService', 'AppUtilities', '$controller','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams,$modal, AppService, AppUtilities, $controller,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_rent_masters';
    $scope.exportFile = 'vehicle-rent';
    $scope.caption = 'Bike ON Rent';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.vehicleType = 1;
    $scope.searchVehicleType = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.arrStatusData = [{id:1,name:"Open"},{id:2,name:"Completed"}];
        $scope.arrFormTools = [{id:1,name:"Change Status",icon:"fa fa-desktop",method:"changeStatus()",title:"Change Status",class:"yellow"}];
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Booking Date',type:2,column:'booking_date',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'Vehicle No',type:2,column:'vehicle_no',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Customer Name',type:2,column:'customer',sort:true,sort_value:3,width:"20%",align:"left"},
            {caption:'Security Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Status',type:2,column:'status',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"15%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null) {
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.id = null;
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/'+$scope.memberFirmId+'/'+$scope.companyYearId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.bindScopeForm.customer_master_id !== null) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('vehicle_master_id',$scope.bindScopeForm.vehicle_master_id);
            saveForms.append('customer_master_id',$scope.bindScopeForm.customer_master_id);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId+'/'+$scope.memberFirmId+'/'+$scope.companyYearId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.changeStatus = function() {
        AppUtilities.selectionBox()
                    .success(function(getId){
                        $scope.vehicleRentId = getId;
                        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/vehicle_rent/change_status.html';
                        $scope.modalInstance = $modal.open({
                            templateUrl: url,
                            size: 'lg',
                            backdrop:'static',
                            keyboard:false,
                            windowClass: 'modal',
                            scope: $scope
                        });
                    }).error(function(){
                        return false;
                    });
    };

    $scope.save_status = function() {
        if(typeof $scope.vehicleRentId !== 'undefined' && $scope.vehicleRentId !== null && $scope.vehicleRentId != '') {
            if($scope.changeStatusForm.$valid) {
                $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
                $('#update_btn').attr('disabled', true);
                AppUtilities.blockUI(1);
                var savePath = $scope.statePath + '/save_status';
                var saveForms = new FormData($('#changeStatusForm')[0]);
                saveForms.append('id',$scope.vehicleRentId);
                AppService.postHttpFormRequest(savePath, saveForms)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $scope.modalInstance.close();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo,{},{reload:true});
                        $('#update_btn').attr('disabled', false);
                        $('#update_btn').html('UPDATE');
                    })
                    .error(function(response) {
                        $('#update_btn').attr('disabled', false);
                        $('#update_btn').html('UPDATE');
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            } else {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
            }
        } else {
            AppUtilities.handleResponse({status:0,message:GLOBAL_DIR.MESSAGE.VALID_DATA_VALIDATION});
            return false;
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.nextTab = function(newTab = 1) {
        $scope.tab = newTab;
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
