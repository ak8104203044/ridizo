'use strict';
radizoApp.controller('VehiclJobeReportsController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities','$controller','$window',function($rootScope, $scope, $state, AppService, AppUtilities,$controller,$window) {
    
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_job_report';
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.oneAtATime = false;
    $scope.status = {open: true,other:false,customer:false,filter:false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.bindScopeForm = {};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.generateReportType = function(flag = 1) {
        $controller('CommonFilterComponent', {$scope: $scope});
        console.log('flag :',flag);
        var today = AppUtilities.getDateFormat(new Date());
        $scope.bindScopeForm.from_date = today;
        $scope.bindScopeForm.end_date = today;
        $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
        $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
        if(flag === 1) {
            $scope.arrJobStatusData = [{id:0,name:"All Status"},{id:1,name:"Open"},{id:2,name:"Assign To Seninor Technician"},{id:3,name:"Assign To Junior Technician"},{id:4,name:"In Progress"},{id:5,name:"Hold"},{id:6,name:"Re-Assign"},{id:7,name:"Job Done"},{id:8,name:"Completed"}];
            $scope.arrVehicleStockType = [{id:2,name :'Yes'},{id:1,name:'No'}];
            $scope.arrPaymentStatusData = [{id:1,name:'Pending'},{id:2,name:'Partial'},{id:3,name:'Paid'}];
            $scope.arrSortByData = [{id:1,name:"Job Date"},{id:2,name:"Vehicle Type"},{id:3,name:"Manufacturer"},{id:4,name:"Vehicle Model"},{id:5,name:"Vehicle Code"},{id:6,name:"Customer Name"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.getBranchList();
            $scope.getVehicleTypeList();
            $scope.getCompanyYearList();
        }
    };

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var path,caption,fileName;
            if(flag === 1) {
                path = $scope.statePath + '/vehicle_job_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Dynamic Vehicle Job Report';
                fileName = 'vehicle_job/dynamic_vehicle_job_report.html';
            }
            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    console.log('response :',response);
                    localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    $scope.reportWindow =$window.open($scope.reportPath+'/report.html','StockReport','height=890,width=1280,resizable=1,location=no');
                    AppUtilities.unblockUI();
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                }).error(function(response) {
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        }
    };
}]);
