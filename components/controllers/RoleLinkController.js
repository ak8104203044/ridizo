'use strict';
radizoApp.controller('RoleLinkController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', '$controller', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, $controller) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'role_link_masters';
    $scope.exportFile = 'rolelink';
    $scope.caption = 'Role Link';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.routerRows = [{}];
    $scope.rights = localStorage.getItem('userRights');
    $scope.parentRoleLink = [];
    $scope.childAction = [{ id: 1, name: 'add', selected: false }, { id: 2, name: 'edit', selected: false }, { id: 3, name: 'view', selected: false }];
    $scope.arrEnableLink = [{ id: 1, name: '', selected: false }];
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id = 0) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        console.log('stateforward :',stateForward);
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.arrFormTools = [
            {id:1,name:"Save Role Links",icon:"fa fa-empire",method:"saveRole()",title:"Save Role Links",class:"blue-hoki"},
            {id:2,name:"Set Order",icon:"fa fa-desktop",method:"redirectState('setorder')",title:"Set Order",class:"yellow"}
        ];
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Name',type:2,column:'name',sort:true,sort_value:1,width:"20%",align:"left"},
            {caption:'State',type:2,column:'state',sort:true,sort_value:2,width:"20%",align:"left"},
            {caption:'Module',type:2,column:'module',sort:true,sort_value:3,width:"20%",align:"left"},
            {caption:'Parent',type:2,column:'parent',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"30%",align:"left"}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        $scope.id = null;
        $scope.getParentLink(0);
    };

    $scope.getParentLink = function(flag = 1) {
        AppUtilities.blockUI(1,'#parentRoleDiv');
        var path = $scope.statePath + '/parent_role_link';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.parentRoleLink = response.data;
                AppUtilities.unblockUI('#parentRoleDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#parentRoleDiv');
            });
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    $scope.bindScopeForm = angular.copy(response.data);
                    if (typeof $scope.bindScopeForm.base_files !== 'undefined' && $scope.bindScopeForm.base_files != '' && typeof $scope.bindScopeForm.base_files === 'string') {
                       try {
                            var arrBaseFiles = JSON.parse($scope.bindScopeForm.base_files);
                            $scope.bindScopeForm.base_files = $scope.generateFile(arrBaseFiles);
                       } catch(e) {
                            $scope.bindScopeForm.base_files = "";
                       }
                    }
                    try {
                        var arrActionData = JSON.parse(response.data.action);
                        var getData;
                        $scope.routerRows = [];
                        for(getData of arrActionData) {
                            $scope.routerRows.push({
                                router:getData.route,
                                param:getData.param,
                                files:$scope.generateFile(getData.files)
                            });
                        }
                    } catch(e) {
                        $scope.routerRows = [{}];
                    }
                    $scope.bindScopeForm.is_link = ($scope.bindScopeForm.is_link == 0) ? false : true;
                    try {
                        if (typeof $scope.bindScopeForm.action !== 'undefined' && $scope.bindScopeForm.action !== null && $scope.bindScopeForm.action != '') {
                            var getChildAction = JSON.parse($scope.bindScopeForm.action);
                            if (getChildAction.length > 0) {
                                if (getChildAction.indexOf('1') != -1) {
                                    $scope.childAction[0]['selected'] = true;
                                }
                                if (getChildAction.indexOf('2') != -1) {
                                    $scope.childAction[1]['selected'] = true;
                                }
                                if (getChildAction.indexOf('3') != -1) {
                                    $scope.childAction[2]['selected'] = true;
                                }
                            }
                        }
                    } catch(e) {
                        console.log('error',JSON.stringify(e));
                    }
                    if ($scope.bindScopeForm.is_link == 1) {
                        $scope.arrEnableLink[0]['selected'] = true;
                    }
                    $scope.getParentLink();
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            $state.go($scope.baseStateTo);
        }
    };

    $scope.generateFile = function(arrBaseFiles) {
        var files = "";
        var arrNewFiles = [];
        if(arrBaseFiles.length > 0) {
            var i;
            for(i = 0;i < arrBaseFiles.length;i++) {
                if(arrBaseFiles[i] != '') {
                    arrNewFiles.push(arrBaseFiles[i]);
                }
            }
            var totalLength = arrNewFiles.length - 1;
            for(i = 0;i < arrNewFiles.length;i++) {
                if (i == totalLength) {
                    files += arrBaseFiles[i];
                } else {
                    files += arrBaseFiles[i]+",\n";
                }
            }
        }
        return files.trim();
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            var routerForm = $('#routerForm').serializeArray();
            angular.forEach(routerForm,function(element) {
                saveForms.append(element.name,element.value);
            });
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    if ($scope.bindScopeForm.base_files != null && $scope.bindScopeForm.base_files != '') {
                        $scope.bindScopeForm.base_files = JSON.parse($scope.bindScopeForm.base_files);
                    }

                    try {
                        var arrActionData = JSON.parse(response.data.action);
                        var getData;
                        $scope.routerRows = [];
                        for(getData of arrActionData) {
                            $scope.routerRows.push({
                                router:getData.route,
                                param:getData.param,
                                files:$scope.generateFile(getData.files)
                            });
                        }
                    } catch(e) {
                        $scope.routerRows = [{}];
                    }
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            $state.go($scope.baseStateTo);
        }
    };

    $scope.order = function() {
        var path = $scope.statePath + '/order';
        AppService.getHttpRequest(path)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        console.log('data is :',response);
                        var roleRightsHtml = AppUtilities.setOrderRightsQuery(response.data);
                        $('#roleLinkHtml').html(roleRightsHtml);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    });
    };

    $scope.save_order = function() {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save_order';
            var saveForm = new FormData($('#formSubmission')[0]);
            AppService.postHttpFormRequest(savePath, saveForm)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.saveRole = function() {
        AppUtilities.confirmBox('Do you want to save the role link information into the router json ?')
            .success(function() {
                var path = $scope.statePath + '/save_rolelink_menu';
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            })
            .error(function(response) {
                return false;
            });
    };

    $scope.nextTab = function(newTab = 1) {
        $scope.tab = newTab;
    };

    $scope.addRows = function() {
        $scope.routerRows.push({});
    };

    $scope.deleteRow = function(i) {
        $scope.routerRows.splice(i, 1);
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            AppUtilities.blockUI(1);
            var exportPath = $scope.statePath + '/export/';
            AppService.getHttpRequest(exportPath)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('logs :', response.data);
                    AppUtilities.export(response.data, $scope.exportFile + '.xls', 1);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var response = { status: 0, message: 'Invalid Export Type' };
            AppUtilities.handleResponse(response);
        }
    };
}]);
