'use strict';
radizoApp.controller('SalesController', ['$rootScope', '$scope', '$state', '$stateParams','$modal', 'AppService', 'AppUtilities', '$controller','$window' ,'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams,$modal, AppService, AppUtilities,$controller,$window, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'invoice_masters';
    $scope.exportFile = 'sale';
    $scope.caption = 'Sale';
    $scope.bindScopeForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.arrTaxDetail = [];
    $scope.arrCustomerData = [];
    $scope.productDetail = {};
    $scope.productFilter = {};
    $scope.productRow = [];
    $scope.summary = {};
    $scope.arrProductId = [];
    $scope.productType = 1;
    $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
    $scope.submitBtn = {name:'Submit',is_disabled :false};
    $scope.message = GLOBAL_DIR.MESSAGE;

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.init = function() {
        $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
        $scope.getCustomerList(0);
        $scope.dataTablePath = $scope.statePath + '/index/2';
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Invoice No',type:2,column:'invoice_no',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Invoice Date',type:2,column:'invoice_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:true,sort_value:3,width:"10%",align:"left"},
            {caption:'Customer Name',type:2,column:'customer',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Status',type:5,column:'',sort:true,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:6,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:2}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.deleteTmpStock();
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.id = null;
            $scope.bindScopeForm.payment_mode = 1;
            $scope.bindScopeForm.date = AppUtilities.getDateFormat(new Date());
            $scope.getUniqueCode(2);
            $scope.getTaxGroupList(0);
            $scope.getBankList(0);
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.bindScopeForm.customer_master_id !== null) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save_sale_invoice';
            var saveForms = new FormData($('#formSubmission')[0]);
            var saleProduct = {
                product:$scope.productRow,
                tax:$scope.arrTaxDetail,
                summary:$scope.summary
            };
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('invoice_data',JSON.stringify(saleProduct));
            saveForms.append('actual_amount',$scope.summary.total);
            saveForms.append('customer_master_id',$scope.bindScopeForm.customer_master_id);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
        }
    };

    $scope.printInvoice = function(id) {
        AppUtilities.blockUI(1);
        var path = $scope.statePath + '/print_invoice/2/'+ id + '/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                try {
                    var receiptDetail = angular.copy(response.data);
                    var invoice = JSON.parse(receiptDetail.invoice_json);
                    delete receiptDetail.invoice_json;
                    receiptDetail = Object.assign({},receiptDetail,invoice);
                    localStorage.setItem('arrReceiptData',JSON.stringify(receiptDetail));
                    localStorage.setItem('caption','Invoice');
                    localStorage.setItem('fileName','sale_receipt.html');
                    var path =  $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/receipt/receipt.html';
                    $scope.reportWindow = $window.open(path,'SaleReceipt','height=890,width=1280,resizable=1,location=no');
                } catch(e) {
                    AppUtilities.handleResponse({status:0,message:e.toString()});
                }
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion(2)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/cancel_invoice/2/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };
    
    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode) {
                    AppUtilities.handleResponse({status:0,'message':'Please add invoice code from user setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleJobCode = '';
                AppUtilities.handleResponse({status:0,'message':'Please add invoice code from user setting!..'});
                $state.go($scope.baseStateTo, {}, { reload: true });
            });
    };

    $scope.getCustomerList = function(flag = 0) {
        $scope.arrCustomerData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_customer_list/'+$scope.memberFirmId;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrCustomerData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.deleteRecord = function(rows,i) {
        var text = 'Do you want to delete this product ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                $scope.deleteTmpStock(rows.product_variant_tran_id);
                $scope.productRow.splice(i, 1);
                $scope.calculation();
                AppUtilities.unblockUI();
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.deleteTmpStock = function(product_variant_tran_id = 0) {
        var path = $rootScope.ADMIN_API_URL + '/system/delete_product_tmp_stock/'+product_variant_tran_id;
        AppService.getHttpRequest(path)
            .success(function(response) {
                console.log()
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                if(response.status == -1) {
                    AppUtilities.handleResponse(response);
                }
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
