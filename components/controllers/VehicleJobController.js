'use strict';
radizoApp.controller('VehicleJobController', ['$rootScope', '$scope', '$state', '$stateParams', '$modal', 'AppService', 'AppUtilities','$controller','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, $modal, AppService, AppUtilities,$controller,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_job_masters';
    $scope.bindScopeForm = {};
    $scope.getVehicleDetail = {};
    $scope.bindScopeForm.in_house_vehicle = 2;
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.tab = 1;
    $scope.complainRow = [{}];
    $scope.scopeType = 2;
    $scope.vehicleValidationType = 1;
    $scope.productType = 2;
    $scope.vehicleType = 3;
    $scope.searchVehicleType = 3;
    $scope.searchFilter = {};
    $scope.productFilter = {};
    $scope.serviceFilter = {};
    $scope.summary = {};
    $scope.services = {};
    $scope.productRow = [];
    $scope.serviceRow = [];
    $scope.activeJobTab = 1;
    $scope.vehicleCouponDetail = {};
    $scope.jobAccordianTab = {first: true,second : false,third : false};
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function(jobType = 1) {
        jobType = Number(jobType);
        $scope.activeJobTab = jobType;
        $scope.arrJobStatusData = [{id:1,name:"Open"},{id:2,name:"Assign To Seninor Technician"},{id:3,name:"Assign To Junior Technician"},{id:4,name:"In Progress"}];
        $scope.searchFilter.job_status = 0;
        if(jobType === 1) {
            $scope.exportFile = 'add-job';
            $scope.caption = 'Add Job';
            $scope.dataTablePath = $scope.statePath + '/index/' + jobType;
            $scope.tableHeader = [
                {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
                {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
                {caption:'Job ID',type:2,column:'job_code',sort:true,sort_value:2,width:"15%",align:"left"},
                {caption:'Job Date',type:2,column:'date',sort:true,sort_value:1,width:"15%",align:"left"},
                {caption:'Vehicle No.',type:2,column:'vehicle',sort:true,sort_value:3,width:"10%",align:"left"},
                {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Status',type:2,column:'status',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:31}
            ];
            /** Initialize table grid **/
            $controller('dataTableController', {$scope: $scope});
            $scope.initTable(1);
            $scope.deleteTmpStock(0);
        } else if(jobType === 2) {
            $scope.arrJobStatusData = [{id:5,name:"Hold"},{id:6,name:"Re-Assign"},{id:7,name:"Job Done"}];
            $scope.exportFile = 'Inprogress Job';
            $scope.caption = 'inprogress-job';
            $scope.dataTablePath = $scope.statePath + '/index/' + jobType;
            $scope.tableHeader = [
                {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
                {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
                {caption:'Job ID',type:2,column:'job_code',sort:true,sort_value:2,width:"15%",align:"left"},
                {caption:'Job Date',type:2,column:'date',sort:true,sort_value:1,width:"15%",align:"left"},
                {caption:'Vehicle No.',type:2,column:'vehicle',sort:true,sort_value:3,width:"10%",align:"left"},
                {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Status',type:2,column:'status',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:32}
            ];
            /** Initialize table grid **/
            $controller('dataTableController', {$scope: $scope});
            $scope.initTable(1);
        } else if(jobType === 3) {
            $scope.exportFile = 'Job Complete';
            $scope.caption = 'job-complete';
            //$scope.arrJobStatusData = [{id:0,name:"All Status"},{id:1,name:"Open"},{id:2,name:"Assign To Seninor Technician"},{id:3,name:"Assign To Junior Technician"},{id:4,name:"In Progress"},{id:5,name:"Hold"},{id:6,name:"Re-Assign"},{id:7,name:"Job Done"},{id:8,name:"Completed"}];
            $scope.arrJobStatusData = [{id:8,name:"Completed"}];
            $scope.dataTablePath = $scope.statePath + '/index/' + jobType;
            delete $scope.userRights.add;
            delete $scope.userRights.edit;
            delete $scope.userRights.delete;
            $scope.tableHeader = [
                {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
                {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
                {caption:'Job Date',type:2,column:'date',sort:true,sort_value:1,width:"15%",align:"left"},
                {caption:'Job ID',type:2,column:'job_code',sort:true,sort_value:2,width:"15%",align:"left"},
                {caption:'Vehicle No.',type:2,column:'vehicle',sort:true,sort_value:3,width:"10%",align:"left"},
                {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Status',type:2,column:'status',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:33}
            ];
            /** Initialize table grid **/
            $controller('dataTableController', {$scope: $scope});
            $scope.initTable(1);
        } else if(jobType === 4) {
            $scope.exportFile = 'Job Invoice';
            $scope.caption = 'job-invoice';
            $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'invoice_masters';
            $scope.dataTablePath = $scope.statePath +'/index/3';
            $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
            delete $scope.userRights.add;
            delete $scope.userRights.edit;
            delete $scope.userRights.delete;
            $scope.userRights.cancel_invoice = 1;
            $scope.tableHeader = [
                {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
                {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
                {caption:'Job ID',type:2,column:'job_id',sort:true,sort_value:1,width:"15%",align:"left"},
                {caption:'Invoice No.',type:2,column:'invoice_no',sort:false,sort_value:2,width:"15%",align:"left"},
                {caption:'Invoice date',type:2,column:'invoice_date',sort:false,sort_value:2,width:"15%",align:"left"},
                {caption:'Vehicle No.',type:2,column:'vehicle',sort:true,sort_value:3,width:"10%",align:"left"},
                {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Payment Status',type:5,column:'payment_status',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Action',type:6,column:'',sort:false,sort_value:0,width:"10%",align:"left",btnType:3}
            ];
            /** Initialize table grid **/
            $controller('dataTableController', {$scope: $scope});
            $scope.initTable(1);
            $controller('CommonFilterComponent', {$scope: $scope});
        } else if(jobType === 5) {
            $scope.exportFile = 'View Job';
            $scope.caption = 'view-job';
            $scope.dataTablePath = $scope.statePath + '/index/5';
            delete $scope.userRights.add;
            delete $scope.userRights.delete;
            $scope.tableHeader = [
                {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
                {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
                {caption:'Job ID',type:2,column:'job_id',sort:true,sort_value:1,width:"15%",align:"left"},
                {caption:'Invoice No.',type:2,column:'invoice',sort:false,sort_value:2,width:"15%",align:"left"},
                {caption:'Vehicle No.',type:2,column:'vehicle',sort:true,sort_value:3,width:"10%",align:"left"},
                {caption:'Amount',type:2,column:'amount',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Payment Status',type:5,column:'payment_status',sort:false,sort_value:0,width:"10%",align:"left"},
                {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"10%",align:"left",btnType:3}
            ];
            /** Initialize table grid **/
            $controller('dataTableController', {$scope: $scope});
            $scope.initTable(1);
            $controller('CommonFilterComponent', {$scope: $scope});
        } else {}
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $scope.id = null;
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.bindScopeForm.job_date = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.completion_date = angular.copy($scope.bindScopeForm.job_date);
            $scope.getUniqueCode(3);
            $scope.checkListDetail();
        }
        AppUtilities.unblockUI();
    };

    $scope.formValidate = function(nextTab = 2) {
        if(nextTab == 2) {
            if($scope.vehicleJobGeneralForm.$valid) {
                $scope.tab = nextTab;
            } else {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
            }
        } else {
            $scope.tab = nextTab;
        }
    };

    $scope.save = function() {
        if($scope.vehicleJobGeneralForm.$valid) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#remarkForm')[0]);
            if($scope.id !== null && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('vehicle_master_id',$scope.bindScopeForm.vehicle_master_id);
            saveForms.append('in_house_vehicle',$scope.bindScopeForm.in_house_vehicle);
            if(localStorage.getItem('isUser') == 1) {
                saveForms.append('employee_master_id',localStorage.getItem('employeeId'));
            }
            var vehicleGeneralForm = $('#vehicleJobGeneralForm').serializeArray();
            var checklistForm =  $('#vehicleChecklistForm').serializeArray();
            angular.forEach(checklistForm,function(element) {
                saveForms.append(element.name,element.value);
            });
            angular.forEach(vehicleGeneralForm,function(element) {
                saveForms.append(element.name,element.value);
            });
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.getVehicleDetail = {};
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            $scope.bindScopeForm = {};
            $scope.checkListDetail();
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/edit/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    try {
                        var compain = JSON.parse(response.data.extra_information);
                        $scope.complainRow = (typeof compain.remark !== "undefined" && compain.remark.length > 0) ? compain.remark : [];
                    } catch(e) {
                        $scope.complainRow = [];
                    }
                    $scope.arrCheckListData = angular.copy(response.checklist);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/view/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.in_house_vehicle = ($scope.bindScopeForm.in_house_vehicle == true) ? 2 : 1;
                    try {
                        var vehicleJob = JSON.parse(response.data.job_json);
                        $scope.bindScopeForm.product = vehicleJob.product;
                        $scope.bindScopeForm.service = vehicleJob.service;
                    } catch(e) {
                        $scope.bindScopeForm.product = [];
                        $scope.bindScopeForm.service = [];
                    }
                    
                    $scope.bindScopeForm.checkList = response.checklist;
                    try {
                        var compalain = JSON.parse(response.data.extra_information);
                        $scope.complainRow = (typeof compalain.remark !== "undefined") ? compalain.remark : [];
                    } catch(e) {
                        $scope.complainRow = [{}];
                    }
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode || !$scope.memberFirmId || !$scope.companyYearId) {
                    if(type == 3) {
                        AppUtilities.handleResponse({status:0,'message':'Please add job code from user setting!..'});
                    } else {
                        AppUtilities.handleResponse({status:0,'message':'Please add invoice from user setting!..'});
                    }
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                if(response.status == 0) {
                    if(type == 3) {
                        AppUtilities.handleResponse({status:0,'message':'Please add job code from user setting!..'});
                    } else {
                        AppUtilities.handleResponse({status:0,'message':'Please add invoice from user setting!..'});
                    }
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
                $scope.vehicleJobCode = '';
            });
    };

    $scope.checkListDetail = function() {
        $scope.arrChecklistData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_checklist/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrChecklistData = response.data;
            })
            .error(function(response) {
                return false;
            });
    };

    $scope.changeJobStatus = function(jobId) {
        if(jobId !== null) {
            AppUtilities.blockUI(1);
            var path = $scope.statePath + '/view_status/' +jobId + '/' + $scope.memberFirmId+'/1';
            AppService.getHttpRequest(path)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.completion_date = AppUtilities.getDateFormat(new Date());
                    $scope.bindScopeForm.job_status =  Number($scope.bindScopeForm.job_status) + 1;
                    $scope.bindScopeForm.arrChecklistData = angular.copy(response.checklist);
                    console.log('$scope.bindScopeForm :',$scope.bindScopeForm);
                    $scope.getDepartmentList(0);
                    var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/general_form/job_status_form.html';
                    $modal.open({
                        templateUrl: url,
                        size: 'lg',
                        backdrop:'static',
                        keyboard:false,
                        windowClass: 'modal',
                        scope:$scope,
                        controller:function($scope,$modalInstance){
                            $scope.close = function() {
                                $modalInstance.close();
                            },
                            $scope.saveStatus = function() {
                                if($scope.jobStatusForm.$valid) {
                                    AppUtilities.blockUI(1);
                                    $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
                                    $('#update_btn').attr('disabled', true);
                                    var path = $scope.statePath + '/save_status';
                                    if(typeof $scope.bindScopeForm.employee_master_id !== "undefined" && $scope.bindScopeForm.employee_master_id !== null) {
                                        var employeeDetail = $scope.arrEmployeeData.find(function(element){
                                            return element.id == $scope.bindScopeForm.employee_master_id;
                                        });
                                        if(employeeDetail) {
                                            $scope.bindScopeForm.employee_name = employeeDetail.employee_name;
                                        }
                                    }
                                    $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
                                    AppService.postHttpRequest(path, $scope.bindScopeForm)
                                        .success(function(response) {
                                            AppUtilities.unblockUI();
                                            AppUtilities.handleResponse(response);
                                            $state.go($scope.baseStateTo);
                                            $scope.bindScopeForm = {};
                                            $modalInstance.close();
                                            $state.go($scope.baseStateTo, {}, { reload: true });
                                            $('#update_btn').attr('disabled', false);
                                            $('#update_btn').html('update');
                                        })
                                        .error(function(response) {
                                            $('#update_btn').attr('disabled', false);
                                            $('#update_btn').html('update');
                                            AppUtilities.unblockUI();
                                            AppUtilities.handleResponse(response);
                                        });
                                } else {
                                    AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
                                }
                            };
                        }
                    });
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
        }
    };

    $scope.getTaxGroupList = function(flag) {
        $scope.arrTaxGroupData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        var path = $rootScope.ADMIN_API_URL + '/system/get_tax_group_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTaxGroupData = angular.copy(response.data);
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.getDepartmentList = function(flag = 0) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrDepartmentData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_department_list/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrDepartmentData = angular.copy(response.data);
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.bindScopeForm.department_master_id = '';
            });

    };

    $scope.getDesignationList = function(flag = 0) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrDesignationData = [];
        $scope.bindScopeForm.employee_master_id = null;
        $scope.arrEmployeeData = [];
        if (typeof $scope.bindScopeForm.department_master_id !== 'undefined' && $scope.bindScopeForm.department_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_designation_list/'+ $scope.memberFirmId + '/' +$scope.bindScopeForm.department_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrDesignationData = angular.copy(response.data);
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                })
                .error(function() {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.bindScopeForm.designation_master_id = '';
                });
        } else {
            if (flag == 1) {
                AppUtilities.unblockUI();
            }
            $scope.bindScopeForm.designation_master_id = '';
        }
    };

    $scope.getEmployeeData = function() {
        $scope.arrEmployeeData = [];
        $scope.bindScopeForm.employee_master_id = null;
        if(typeof $scope.bindScopeForm.designation_master_id !== "undefined" && $scope.bindScopeForm.designation_master_id !== null) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_employee_list/'+$scope.memberFirmId+'/'+$scope.bindScopeForm.designation_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrEmployeeData = response.data;
                })
                .error(function(response) {
                    return false;
                });
        }
    };

    $scope.getJobPartDetail = function(id = 0,type = 2) {
        AppUtilities.blockUI(1);
        var path = $scope.statePath + '/view_status/' + id + '/' + $scope.memberFirmId + '/'+type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.bindScopeForm = angular.copy(response.data);
                $controller('CommonFilterComponent',{$scope:$scope});
                if(type == 2) {
                    $scope.getTaxGroupList(0);
                    $scope.vehicleCouponDetail = angular.copy(response.coupon);
                    if(response.data.job_json != '' && response.data.job_json !== null) {
                        try {
                            var vehicleJob = JSON.parse(response.data.job_json);
                            if(typeof vehicleJob.product !== "undefined" && vehicleJob.product !== null && vehicleJob.product != '') {
                                $scope.productRow = angular.copy(vehicleJob.product);
                            }
                            
                            if(typeof vehicleJob.service !== "undefined" && vehicleJob.service !== null && vehicleJob.service != '') {
                                $scope.serviceRow = angular.copy(vehicleJob.service);
                            }
                            
                            $scope.summary = angular.copy(vehicleJob.summary);
                            $scope.services = angular.copy(vehicleJob.service_detail);
                            $scope.calculateService();
                            $scope.calculation();
                        } catch(e) {
                            AppUtilities.handleResponse({status:0,message:e.toString()});
                        }
                    }
                } else if(type == 3) {
                    $scope.productType = 3;
                    if(response.data.job_json != '' && response.data.job_json !== null) {
                        try {
                            var vehicleJob = JSON.parse(response.data.job_json);
                            if(typeof vehicleJob.spare_product !== "undefined" && vehicleJob.spare_product !== null && vehicleJob.spare_product != '') {
                                $scope.productRow = angular.copy(vehicleJob.spare_product);
                            }
                        } catch(e) {
                            AppUtilities.handleResponse({status:0,message:e.toString()});
                        }
                    }
                } else {}
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                $state.go($scope.baseStateTo, {}, { reload: true });
                AppUtilities.handleResponse(response);
            });
    };

    $scope.saveVehicleProduct = function(trans_type = 2) {
        if ($scope.formSubmission.$valid) {
            AppUtilities.blockUI(1);
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            var savePath = $scope.statePath + '/save_product';
            var saveForms = new FormData();
            saveForms.append('id',$scope.bindScopeForm.id);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('trans_type',trans_type);
            if(trans_type == 2) {
                saveForms.append('discount',$scope.summary.discount);
                saveForms.append('coupon',$scope.services.coupon);
                saveForms.append('amount',AppUtilities.numberFormat($scope.summary.total+$scope.services.total,2));
                if(typeof $scope.services.coupon_detail !== "undefined" && $scope.services.coupon_detail.id !== "undefined" && $scope.services.coupon_detail.id !== null) {
                    saveForms.append('vehicle_coupon_tran_id',$scope.services.coupon_detail.id);
                    saveForms.append('expired',($scope.services.coupon_detail.expired == 1) ? 3 : 2);
                }
                var vehicleJobData = {
                    product:$scope.productRow,
                    tax:$scope.arrTaxDetail,
                    summary:$scope.summary,
                    service:$scope.serviceRow,
                    service_detail:$scope.services
                };
                saveForms.append('vehicle_job_json',JSON.stringify(vehicleJobData));
            } else if(trans_type == 1) {
                saveForms.append('spare_product',JSON.stringify($scope.productRow));
            }

            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    $scope.bindScopeForm = {};
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.deleteRecord = function(rows,i,type = 1) {
        var text = (type == 1) ? 'Do you want to delete this product ?' : 'Do you want to delete this service ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                if(type == 1) {
                    $scope.deleteTmpStock(rows.product_variant_tran_id);
                    $scope.productRow.splice(i, 1);
                    $scope.calculation();
                } else {
                    $scope.serviceRow.splice(i, 1);
                    $scope.calculateService();
                }
                if(typeof rows.vehicle_job_tran_id !== "undefined" && rows.vehicle_job_tran_id !== null) {
                    $scope.deleteProductStock(rows.vehicle_job_tran_id,type);
                }
                AppUtilities.unblockUI();
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.deleteProductStock = function(vehicle_job_tran_id,type = 1) {
        var saveForms = new FormData();
        if(type == 1 || type == 2) {
            var vehicleJobData = {
                product:$scope.productRow,
                tax:$scope.arrTaxDetail,
                summary:$scope.summary,
                service:$scope.serviceRow,
                service_detail:$scope.services
            };
            saveForms.append('discount',$scope.summary.discount);
            saveForms.append('coupon',$scope.services.coupon);
            saveForms.append('amount',AppUtilities.numberFormat($scope.summary.total+$scope.services.total,2));
            saveForms.append('vehicle_job_json',JSON.stringify(vehicleJobData));
        } else if(type == 3) {
            
        } else {}

        saveForms.append('type',type);
        saveForms.append('id',$scope.bindScopeForm.id);
        saveForms.append('vehicle_job_tran_id',vehicle_job_tran_id);
        
        var savePath = $scope.statePath + '/delete_product_stock';
        AppService.postHttpFormRequest(savePath, saveForms)
            .success(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    }

    $scope.deleteTmpStock = function(product_variant_tran_id = 0) {
        var path = $rootScope.ADMIN_API_URL + '/system/delete_product_tmp_stock/'+product_variant_tran_id;
        AppService.getHttpRequest(path)
            .success(function(response) {
                console.log(response);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                if(response.status == -1) {
                    AppUtilities.handleResponse(response);
                }
            });
    };

    $scope.generateInvoiceForm = function(id) {
        AppUtilities.blockUI(1);
        var path = $scope.statePath + '/view_status/' + id + '/' + $scope.memberFirmId + '/4';
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.getUniqueCode(2);
                $scope.getBankList(0);
                $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
                console.log('response is :',response.data);
                $scope.bindScopeForm = angular.copy(response.data);
                $scope.bindScopeForm.invoice_date = AppUtilities.getDateFormat(new Date());
                $scope.bindScopeForm.actual_amount = $scope.bindScopeForm.amount;
                $scope.bindScopeForm.payment_mode = 1;
                $scope.bindScopeForm.total_discount = $scope.bindScopeForm.discount;
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                $state.go($scope.baseStateTo, {}, { reload: true });
                AppUtilities.handleResponse(response);
            });
    };

    $scope.saveInvoice = function() {
        $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
        $scope.submitBtn.is_disabled = true;
        $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
        $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
        var savePath = $rootScope.ADMIN_API_URL + '/invoice_masters/save_vehiclejob_invoice';
        if(typeof $scope.bindScopeForm.payment_status !== "undefined" && $scope.bindScopeForm.payment_status == true) {
            $scope.bindScopeForm.paylater = 1;
        }
        AppService.postHttpRequest(savePath, $scope.bindScopeForm)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.getVehicleDetail = {};
                    $state.go($scope.baseStateTo);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
    };

    $scope.calculateAmt  = function() {
        $scope.showShortFees = false;
        $scope.bindScopeForm.amount = ($scope.bindScopeForm.amount !== undefined) ? $scope.bindScopeForm.amount : "";
        if($scope.bindScopeForm.amount < $scope.bindScopeForm.actual_amount) {
            $scope.showShortFees = true;
            $scope.bindScopeForm.shortFees = AppUtilities.numberFormat($scope.bindScopeForm.actual_amount - $scope.bindScopeForm.amount,2);
        } else {
            $scope.bindScopeForm.amount = $scope.bindScopeForm.actual_amount;
            $scope.bindScopeForm.shortFees = 0;
        }
    };

    $scope.calculateServiceDiscount = function() {
        $scope.showShortFees = false;
        $scope.bindScopeForm.shortFees = 0;
        $scope.bindScopeForm.total_discount = AppUtilities.numberFormat($scope.bindScopeForm.discount,2);
        $scope.bindScopeForm.actual_amount = ($scope.bindScopeForm.part_amount + $scope.bindScopeForm.service_amount + $scope.bindScopeForm.discount + $scope.bindScopeForm.coupon);
        $scope.bindScopeForm.amount  = $scope.bindScopeForm.actual_amount;
        var serviceDiscount = {};
        $scope.bindScopeForm.service_discount = ($scope.bindScopeForm.service_discount > 0) ? AppUtilities.numberFormat($scope.bindScopeForm.service_discount,2) : 0;
        $scope.bindScopeForm.service_discount = ($scope.bindScopeForm.service_discount > 100) ? 100 : $scope.bindScopeForm.service_discount;
        if($scope.bindScopeForm.service_discount > 0) {
            var discount = AppUtilities.numberFormat(($scope.bindScopeForm.service_amount * $scope.bindScopeForm.service_discount) / 100,2);
            $scope.bindScopeForm.total_discount = AppUtilities.numberFormat(discount + $scope.bindScopeForm.discount ,2);
            serviceDiscount = {
                price:$scope.bindScopeForm.service_discount,
                type:1,
                dis_type:2,
                name:'Service Discount',
                discount:discount
            };
            var amt = AppUtilities.numberFormat($scope.bindScopeForm.amount - discount,2);
            $scope.bindScopeForm.actual_amount = amt;
            $scope.bindScopeForm.amount = amt;
        }
        $scope.bindScopeForm.discount_detail =  serviceDiscount;
    };

    $scope.getBankList = function(flag = 0) {
        $scope.arrBankData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_bank_list';
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrBankData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/' + $scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.cancelInvoice = function() {
        AppUtilities.confirmDeletion(2)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/cancel_invoice/3/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.addComplainRow = function() {
        $scope.complainRow.push({});
    };

    $scope.deleteComplainRow = function(rows,i) {
        $scope.complainRow.splice(i, 1);
    };

    $scope.lockReceipt = function(id = 0) {
        var text = 'Do you want to locak the invoice ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var savePath = $scope.statePath + '/lock_invoice/'+id;
                AppService.getHttpRequest(savePath)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            console.log('response :',response);
                            AppUtilities.handleResponse(response);
                            $state.go($scope.baseStateTo, {}, { reload: true });
                        })
                        .error(function(response) {
                            AppUtilities.unblockUI();
                            AppUtilities.handleResponse(response);
                        });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            if($scope.activeJobTab === 4) {
                $scope.exportPath = $rootScope.ADMIN_API_URL+ '/invoice_masters/index/3/2';
            } else {
                $scope.exportPath = $scope.statePath + '/index/'+$scope.activeJobTab+'/2';
            }
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
