'use strict';
radizoApp.controller('VehicleJobController', ['$rootScope', '$scope', '$state', '$stateParams', '$modal', 'AppService', 'AppUtilities', 'GLOBAL_DIR','$filter', function($rootScope, $scope, $state, $stateParams, $modal, AppService, AppUtilities, GLOBAL_DIR,$filter) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_job_masters';
    $scope.exportFile = 'job';
    $scope.caption = 'Job';
    $scope.bindScopeForm = {};
    $scope.tmpVehicleForm = {};
    $scope.bindVehicleSearchForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.isVehicleShow = 0;
    $scope.isVehicleExists = 1;
    $scope.tmpVehicleForm.fuel_type = 1;
    $scope.tmpVehicleForm.vehicle_start_method = 1;
    $scope.getVehicleDetail = {};
    $scope.isVehicleAdd = 0;
    $scope.arrChecklistData = [];
    $scope.partRow = [{}];
    $scope.serviceRow = [{}];
    $scope.isDuplicateProduct = 0;
    $scope.qtyColor = '';
    $scope.isProductStkQty = 1;
    $scope.isDuplicateService = 0;
    $scope.isSearchJobRecords = 0;
    $scope.serviceDetail = {};
    $scope.productDetail = {};
    $scope.complainRow = [{}];
    $scope.arrPaymentTypeData = [{ id: 1, name: 'Cash' }, { id: 2, name: 'Credit Card' }, { id: 3, name: 'Debit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    console.log('baseto :',$scope.baseStateTo);
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.add = function() {
        AppUtilities.blockUI(1);
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $scope.bindScopeForm = {};
            $scope.bindScopeForm.job_date = AppUtilities.getDateFormat(new Date());
            $scope.id = null;
            $scope.getUniqueCode(3);
            $scope.checkListDetail();
            $scope.getProductList(0);
            $scope.getServiceList(0);
        } else {
            AppUtilities.handleResponse({status:0,message:'Please select valid Branch & Company Year'});
            $state.go($scope.baseStateTo);
        }
        AppUtilities.unblockUI();
    }

    $scope.save = function() {
        if($scope.vehicleJobGeneralForm.$valid && $scope.vehiclePartForm.$valid && $scope.vehicleServiceForm.$valid) {
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#remarkForm')[0]);
            if($scope.id !== null && $scope.id > 0) {
                saveForms.append('id',$scope.id);
            }
            if (typeof $scope.getVehicleDetail !== 'undefined' && typeof $scope.getVehicleDetail.id !== 'undefined' && $scope.getVehicleDetail.id > 0) {
                saveForms.append('vehicle_master_id',$scope.getVehicleDetail.id);
            }
            var total = AppUtilities.numberFormat($scope.productDetail.totalAmout + $scope.serviceDetail.totalAmount,2);
            saveForms.append('parts',JSON.stringify($scope.partRow));
            saveForms.append('service',JSON.stringify($scope.serviceRow));
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('customer_name',$scope.getVehicleDetail.name);
            saveForms.append('customer_mobile_no',$scope.getVehicleDetail.mobile_no);
            saveForms.append('customer_email_id',(typeof $scope.getVehicleDetail.email_id !== 'undefined') ? $scope.getVehicleDetail.email_id : '');
            saveForms.append('customer_address',$scope.getVehicleDetail.address);
            saveForms.append('total',total);
            var vehicleGeneralForm = $('#vehicleJobGeneralForm').serializeArray();
            var checklistForm =  $('#vehicleChecklistForm').serializeArray();
            angular.forEach(checklistForm,function(element) {
                saveForms.append(element.name,element.value);
            });
            angular.forEach(vehicleGeneralForm,function(element) {
                saveForms.append(element.name,element.value);
            });
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.getVehicleDetail = {};
                    $state.go($scope.baseStateTo);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.edit = function(getId) {
        if (getId !== null && getId > 0) {
            $scope.bindScopeForm = {};
            $scope.checkListDetail();
            $scope.getProductList(0);
            $scope.getServiceList(0);
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/edit/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.getVehicleDetail = {
                        name:$scope.bindScopeForm.customer_name,
                        mobile_no:$scope.bindScopeForm.customer_mobile_no,
                        address:$scope.bindScopeForm.customer_address,
                        email_id:$scope.bindScopeForm.customer_email_id,
                        id:$scope.bindScopeForm.vehicle_master_id
                    }
                    $scope.arrCheckListData = angular.copy(response.checklist);
                    $scope.partRow = angular.copy(response.parts);
                    try {
                        $scope.complainRow = JSON.parse(response.data.remark);
                    } catch(e) {
                        $scope.complainRow = [{}];
                    }
                    $scope.calculation();
                    $scope.serviceRow = angular.copy(response.service);
                    $scope.serviceCalculation();

                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.view = function(getId) {
        if (getId !== null && getId > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/view/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.parts = response.parts;
                    $scope.bindScopeForm.services = response.service;
                    $scope.bindScopeForm.checkList = response.checklist;
                    try {
                        $scope.complainRow = JSON.parse(response.data.remark);
                    } catch(e) {
                        $scope.complainRow = [{}];
                    }
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.job_action = function(getId, type) {
        if (typeof getId !== 'undefined' && getId > 0) {
            $scope.id = getId;
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/view_status/' + getId + '/' + $scope.memberFirmId + '/' + type)
                .success(function(response) {
                    $scope.bindScopeForm = response.data;
                    $scope.bindScopeForm.type = type;
                    $scope.bindScopeForm.checklist = response.checklist;
                    var url;
                    if (type == 1) {
                        $scope.employeeList();
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/change_status.html';
                    } else if (type == 2) {
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/complete.html';
                    } else {
                        $scope.getUniqueCode(2);
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/generate_invoice.html';
                    }
                    var popSize = (type == 1) ? 'lg' : 'lg';
                    $scope.modalInstance = $modal.open({
                        templateUrl: url,
                        size: popSize,
                        backdrop:'static',
                        keyboard:false,
                        windowClass: 'modal',
                        scope: $scope
                    });
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.save_status = function() {
        if ($scope.bindScopeForm.id > 0) {
            AppUtilities.blockUI(1);
            $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#update_btn').attr('disabled', true);
            var saveForms = new FormData($('#jobStatusForm')[0]);
            saveForms.append('firm_id',$scope.memberFirmId);
            saveForms.append('status',($scope.bindScopeForm.type == 1) ? 2 : 3);
            saveForms.append('id',$scope.bindScopeForm.id);
            var path = $scope.statePath + '/save_status';
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.modalInstance.close();
                    AppUtilities.handleResponse(response);
                    $scope.bindScopeForm = {};
                    $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                    $('#update_btn').text('UPDATE');
                    $('#update_btn').attr('disabled', false);
                })
                .error(function(response) {
                    $('#update_btn').text('UPDATE');
                    $('#update_btn').attr('disabled', false);
                    AppUtilities.handleResponse(response);
                    AppUtilities.unblockUI();
                    return false;
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.generate_invoide = function() {
        if ($scope.bindScopeForm.id > 0) {
            AppUtilities.blockUI(1);
            $('#generate_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#generate_btn').attr('disabled', true);
            var path = $scope.statePath + '/generate_invoice';
            var saveForms = new FormData($('#invoiceForm')[0]);
            saveForms.append('firm_id',$scope.memberFirmId);
            saveForms.append('id',$scope.bindScopeForm.id);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.modalInstance.close();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo + '-print_invoice', { id: $scope.bindScopeForm.id }, { reload: true });
                    $scope.bindScopeForm = {};
                    $('#generate_btn').text('UPDATE');
                    $('#generate_btn').attr('disabled', false);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    $('#generate_btn').text('UPDATE');
                    $('#generate_btn').attr('disabled', false);
                    AppUtilities.handleResponse(response);
                    return false;
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.print_invoice = function() {
        if (typeof $scope.id !== 'undefined' && $scope.id > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/print_invoice/' + $scope.id + '/' + $scope.memberFirmId)
                .success(function(response) {
                    $scope.bindScopeForm = angular.copy(response.data);
                    console.log($scope.bindScopeForm);
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/' + $scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    }
    
    $scope.employeeList = function() {
        $scope.arrEmployeeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_employee_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrEmployeeData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }
    
    $scope.searchVehicle = function() {
        $scope.getVehicleData = [];
        $scope.vehicleSearchDiv = 0;
        $scope.getVehicleDetail = {};
        $scope.getVehicleDetail.is_exists = 0;
        $scope.isVehicleExists = 0;
        var path = $rootScope.ADMIN_API_URL + '/system/search_vehicle';
        var params = { search: $scope.searchVehicleKey };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.isVehicleExists = 1;
                $scope.vehicleSearchDiv = 1;
                $scope.getVehicleData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.selectMatch = function(index) {
        if(typeof $scope.getVehicleData[index] !== 'undefined') {
            $scope.vehicleSearchDiv = 0;
            $scope.isVehicleShow = 1;
            $scope.searchVehicleKey = $scope.getVehicleData[index]['unique_no'];
            $scope.getVehicleDetail = $scope.getVehicleData[index];
            $scope.getVehicleDetail.is_exists = 1;
        } else {
            $scope.isVehicleShow = 0;
            $scope.getVehicleDetail = {};
        }
    }

    $scope.vehicleForm = function() {
        $scope.getVehicleDetail = {};
        $scope.searchVehicleKey = '';
        $scope.getTitleList(0);
        $scope.getVehicleTypeList(0);
        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/vehicle_form.html';
        $scope.modalInstance = $modal.open({
            templateUrl: url,
            size: 'lg',
            backdrop:'static',
            keyboard:false,
            windowClass: 'modal',
            controller:'VehicleJobController',
            scope:$scope,
            resolve:{
                scope:function(){
                    return $scope;
                }
            }
        });
    }

    $scope.saveVehicle = function() {
        if($scope.vehicleForm.$valid) {
            $scope.vehicleSearchDiv = 0;
            $scope.isVehicleShow = 1;
            //$scope.viewVehicle();
            //$scope.modalInstance.dismiss($scope.getVehicleDetail);
            //console.log('detail is :',$scope.getVehicleDetail);
            $('#update_btn').attr('disabled', true);
            $('#update_btn').html('Processing....');
            var savePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_masters'+ '/save';
            var saveForms = new FormData($('#vehicleForm')[0]);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id' ,$scope.companyYearId);
            saveForms.append('vehicle_type',2);
            saveForms.append('registration_date',AppUtilities.getDateFormat(new Date()));
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.modalInstance.close();
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    AppUtilities.handleResponse({status:1,message:'Vehicle added successfully'});
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.viewVehicle = function() {
        $scope.vehicles = {};
        var vehicleForm = $('#vehicleForm').serializeArray();
        angular.forEach(vehicleForm,function(element){
            $scope.vehicles[element.name] = element.value;
        });

        var manufacturerData = $scope.arrManufacturerData.find(function(element) { return element.mtranid == $scope.vehicles.manufacturer_tran_id });
        if (typeof manufacturerData !== 'undefined') {
            $scope.vehicles.manufacturer = manufacturerData.name;
        }

        var vehicleTypeData = $scope.arrVehicleTypeData.find(function(element) { return element.id == $scope.vehicles.vehicle_type_master_id });
        if (typeof vehicleTypeData !== 'undefined') {
            $scope.vehicles.vehicle_type = vehicleTypeData.name;
        }
        
        var vehicleModelData = $scope.arrVehicleModelData.find(function(element) { return element.id == $scope.vehicles.vehicle_model_master_id });
        if (typeof vehicleModelData !== 'undefined') {
            $scope.vehicles.model = vehicleModelData.name;
        }

        $scope.vehicles.name = $scope.vehicles.owner_first_name + ' ' + (typeof $scope.vehicles.owner_middle_name !== 'undefined' ? $scope.vehicles.owner_middle_name : '') + ' ' + (typeof $scope.vehicles.owner_last_name !== 'undefined' ? $scope.vehicles.owner_last_name : '');
        console.log('infor',$scope.vehicles);
        $scope.getVehicleDetail = angular.copy($scope.vehicles);
        console.log('infor',$scope.getVehicleDetail);
    }

    $scope.getVehicleTypeList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrVehicleTypeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_type_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrVehicleTypeData = response.data;
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getManufacturerList = function(flag,isReload) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        isReload = (typeof isReload !== 'undefined') ? isReload : 0;
        $scope.arrManufacturerData = [];
        if(isReload == 0) {
            $scope.arrVehicleModelData = [];
            $scope.tmpVehicleForm.vehicle_model_master_id = '';
        }
        
        if(typeof $scope.tmpVehicleForm.vehicle_type_master_id !== 'undefined' && $scope.tmpVehicleForm.vehicle_type_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_manufacturer_vehicle_list/'+$scope.tmpVehicleForm.vehicle_type_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrManufacturerData = response.data;
                })
                .error(function(response) {
                    $scope.tmpVehicleForm.manufacturer_tran_id = '';
                });
        } else {
            $scope.tmpVehicleForm.manufacturer_tran_id = '';
        }
    }

    $scope.getVehicleModelList = function() {
        $scope.arrVehicleModelData = [];
        if (typeof $scope.tmpVehicleForm.manufacturer_tran_id !== 'undefined' && $scope.tmpVehicleForm.manufacturer_tran_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_model_list/'+$scope.tmpVehicleForm.manufacturer_tran_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrVehicleModelData = response.data;
                })
                .error(function() {
                    $scope.tmpVehicleForm.vehicle_model_master_id = '';
                })
        } else {
            $scope.tmpVehicleForm.vehicle_model_master_id = '';
        }
    }

    $scope.getTitleList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrTitleData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_title_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTitleData = angular.copy(response.data);
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response){
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
    }

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode || !$scope.memberFirmId || !$scope.companyYearId) {
                    AppUtilities.handleResponse({status:0,'message':'Please define job code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                if(response.status == 0) {
                    AppUtilities.handleResponse({status:0,'message':'Please define job code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
                $scope.vehicleJobCode = '';
            });
    }

    $scope.checkListDetail = function() {
        $scope.arrChecklistData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_checklist/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrChecklistData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.getProductList = function(flag = 0) {
        $scope.arrProductData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        var path = $rootScope.ADMIN_API_URL + '/system/product_list/'+$scope.memberFirmId+'/1';
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrProductData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getItemDetail = function(row) {
        row.price = 0;
        row.tax = 0;
        row.quantity = 0;
        row.discount = 0;
        row.tax_detail =[];
        if(typeof row.product_master_id !== 'undefined' && row.product_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/product_detail/'+row.product_master_id+'/1';
            AppUtilities.blockUI(1);
            AppService.getHttpRequest(path)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    row.tax_detail = angular.copy(response.data.tax_detail);
                    row.price = angular.copy(response.data.price);
                    row.stockQty = Number(angular.copy(response.data.stock_qty));
                    if(row.stockQty > 0) {
                        row.quantity = 1;
                        row.qtyColor = 'green';
                    } else {
                        row.quantity = 0;
                        row.qtyColor = 'red';
                    }
                    row.item_price = angular.copy(response.data.price);
                    row.tax_name = angular.copy(response.data.tax_name);
                    $scope.productCalculation(row);
                }).error(function() {
                    AppUtilities.unblockUI();
                    $scope.calculation();
                });
        } else {
            $scope.productCalculation(row);
        }
    }

    $scope.productCalculation = function(rows) {
        rows.isDuplicate = false;
        rows.total = 0;
        $scope.isDuplicateProduct = 0;
        var sorted, i;
        sorted = $scope.partRow.concat().sort(function(a, b) {
            if (a.product_master_id > b.product_master_id) return 1;
            if (a.product_master_id < b.product_master_id) return -1;
            return 0;
        });
        if (Object.keys($scope.partRow).length > 0) {
            for (i = 0; i < $scope.partRow.length; i++) {
                sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].product_master_id == sorted[i].product_master_id) || (sorted[i + 1] && sorted[i + 1].product_master_id == sorted[i].product_master_id));
                if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                    $scope.isDuplicateProduct = 1;
                } else {
                    sorted[i].isDuplicate =false;
                }
            }
        }

        if(rows.isDuplicate == false) {
            if(rows.stockQty <= 0) {
                rows.quantity = 0;
            } else {
                rows.quantity = (rows.stockQty < rows.quantity) ? rows.stockQty : rows.quantity
            }
            var totalPrice = 0;
            var itemPrice = 0;
            var quantity = (rows.quantity > 0) ? Number(rows.quantity) : 0;
            itemPrice = AppUtilities.numberFormat(rows.price,2);
            rows.discount = (rows.discount > 0) ? Number(rows.discount) : 0;
            var discountAmount = AppUtilities.numberFormat((itemPrice * rows.discount) /100,2);
            rows.discountAmount = discountAmount;
            itemPrice = AppUtilities.numberFormat(itemPrice - discountAmount,2);
            totalPrice = itemPrice * quantity;
            rows.total = AppUtilities.numberFormat(totalPrice,2);
            rows.taxes = [];
            if(typeof rows.tax_detail !== 'undefined' && Object.keys(rows.tax_detail).length > 0) {
                angular.forEach(rows.tax_detail,function(tax,index){
                    tax.price = itemPrice;
                    var taxAmount = $scope.calculateTax(tax);
                    rows.tax_detail[index]['tax_amount'] = taxAmount; 
                });
            }
            $scope.calculation();
        }
    }

    $scope.calculation = function() {
        $scope.arrTaxDetail = [];
        var totalDiscount = 0;
        var subTotalAmount = 0;
        var totalTax = 0;
        var totalQty = 0;
        $scope.isProductStkQty = 1;
        if($scope.partRow.length > 0) {
            angular.forEach($scope.partRow, function(rows,index) {
                var quantity = Number(rows.quantity);
                totalQty += quantity;
                totalDiscount += rows.discountAmount;
                subTotalAmount += (typeof rows.total !== 'undefined') ? rows.total : 0;
                var itemTaxAmount = 0;
                if(rows.stockQty <=0) {
                    $scope.isProductStkQty = 0;
                }
                if(typeof rows.tax_detail !== 'undefined' && Object.keys(rows.tax_detail).length > 0) {
                    angular.forEach(rows.tax_detail,function(tax,taxKey){
                        var itemTax = AppUtilities.numberFormat(tax.tax_amount * quantity,2);
                        totalTax += itemTax;
                        itemTaxAmount += itemTax;
                        if(typeof $scope.arrTaxDetail[taxKey] !== 'undefined') {
                            $scope.arrTaxDetail[taxKey]['price'] = AppUtilities.numberFormat($scope.arrTaxDetail[taxKey]['price'] + itemTax,2);
                        } else {
                            $scope.arrTaxDetail[taxKey] = {};
                            $scope.arrTaxDetail[taxKey]['name'] = tax.name;
                            $scope.arrTaxDetail[taxKey]['id'] = tax.id;
                            $scope.arrTaxDetail[taxKey]['price'] = itemTax;
                        }
                    });
                }
                rows.tax = AppUtilities.numberFormat(itemTaxAmount,2);
            });
            if(Object.keys($scope.arrTaxDetail).length > 0) {
                $scope.arrTaxDetail = Object.values($scope.arrTaxDetail);
            }
        }
        $scope.productDetail.totalQty = totalQty;
        $scope.productDetail.totalTax = AppUtilities.numberFormat(totalTax,2);
        $scope.productDetail.totalDiscount = AppUtilities.numberFormat(totalDiscount,2);
        $scope.productDetail.subTotalAmount = AppUtilities.numberFormat(subTotalAmount,2);
        $scope.productDetail.totalAmout = AppUtilities.numberFormat((subTotalAmount+totalTax),2);
    }

    $scope.calculateTax = function(tax) {
        var taxAmount = 0;
        if(typeof tax !== 'undefined' && Object.keys(tax).length > 0) {
            var price = (tax.price > 0) ? AppUtilities.numberFormat(tax.price,2) : 0;
            var value = (tax.value > 0) ? AppUtilities.numberFormat(tax.value,2) : 0;
            var type = tax.type;
            if(type == 1) {
                value = (value > 100) ? 100 : value;
                taxAmount = AppUtilities.numberFormat((value * price) / 100,2);
            } else {
                value = (value > price) ? price : value;
                taxAmount = AppUtilities.numberFormat( (price - value),2);
            }
        }
        return taxAmount;
    }

    $scope.partAddRow = function() {
        $scope.partRow.push({});
    }

    $scope.partDeleteRow = function(rows,i) {
        var text = 'Do you want to delete this product ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                if($scope.id !== null && $scope.id > 0 && typeof rows.vehicle_job_tran_id !== 'undefined' && rows.vehicle_job_tran_id > 0) {
                    AppUtilities.blockUI(1);
                    var path = $scope.statePath + '/delete_product/'+$scope.id+'/'+rows.vehicle_job_tran_id;
                    AppService.getHttpRequest(path)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            $scope.partRow.splice(i, 1);
                            $scope.calculation();
                            AppUtilities.handleResponse(response);
                        }).error(function() {
                            AppUtilities.unblockUI();
                            $scope.calculation();
                        });
                } else {
                    $scope.partRow.splice(i, 1);
                    $scope.calculation();
                }
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.getServiceList = function(flag = 0) {
        $scope.arrServiceData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        var path = $rootScope.ADMIN_API_URL + '/system/service_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrServiceData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.serviceProcess = function(serviceRow,flag) {
        var sorted, i;
        serviceRow.isDuplicate = false;
        sorted = $scope.serviceRow.concat().sort(function(a, b) {
            if (a.service_master_id > b.service_master_id) return 1;
            if (a.service_master_id < b.service_master_id) return -1;
            return 0;
        });
        for (i = 0; i < $scope.serviceRow.length; i++) {
            sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].service_master_id == sorted[i].service_master_id) || (sorted[i + 1] && sorted[i + 1].service_master_id == sorted[i].service_master_id));
            if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                $scope.isDuplicateService = 1;
            } else {
                serviceRow.isDuplicate = false;
            }
        }

        if(serviceRow.isDuplicate == false) {
            if(flag == 0) {
                var serviceDetail = $scope.arrServiceData.find(function(element){
                    return element.id == serviceRow.service_master_id;
                });
                if(typeof serviceDetail !== 'undefined') {
                    serviceRow.price = AppUtilities.numberFormat(serviceDetail.price,2);
                }
            }
            $scope.serviceCalculation();
        }
    }

    $scope.serviceCalculation = function() {
        var totalAmount = 0;
        if($scope.serviceRow.length > 0) {
            angular.forEach($scope.serviceRow, function(rows,index) {
                var amount = (rows.price > 0) ? AppUtilities.numberFormat(rows.price,2) : 0;
                totalAmount += amount;
            });
        }
        $scope.serviceDetail.totalAmount = AppUtilities.numberFormat(totalAmount,2);
    }

    $scope.serviceAddRow = function() {
        $scope.serviceRow.push({});
    }

    $scope.serviceDeleteRow = function(rows,i) {
        var text = 'Do you want to delete this service ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                if($scope.id !== null && $scope.id > 0 && typeof rows.vehicle_job_service_tran_id !== 'undefined' && rows.vehicle_job_service_tran_id > 0) {
                    AppUtilities.blockUI(1);
                    var path = $scope.statePath + '/delete_service/'+$scope.id+'/'+rows.vehicle_job_service_tran_id;
                    AppService.getHttpRequest(path)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            $scope.serviceRow.splice(i, 1);
                            $scope.serviceCalculation();
                            AppUtilities.handleResponse(response);
                        }).error(function() {
                            AppUtilities.unblockUI();
                            $scope.calculation();
                        });
                } else {
                    $scope.serviceRow.splice(i, 1);
                    $scope.serviceCalculation();
                }
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.addComplainRow = function() {
        $scope.complainRow.push({});
    }

    $scope.deleteComplainRow = function(rows,i) {
        $scope.complainRow.splice(i, 1);
    }

    $scope.formValidate = function(nextTab = 1) {
        if(nextTab == 2) {
            if($scope.vehicleJobGeneralForm.$valid) {
                $scope.tab = nextTab;
            } else {
                var response = { status: 0, message: 'Please filled up manadatory fields !.' };
                AppUtilities.handleResponse(response);
            }
        } else if(nextTab == 3) {
            if($scope.vehiclePartForm.$valid) {
                if($scope.partRow.length == 0) {
                    var response = { status: 0, message: 'Please select atleast one parts !.' };
                    AppUtilities.handleResponse(response);
                } else {
                    $scope.tab = nextTab;
                }
            } else {
                var response = { status: 0, message: 'Please filled up manadatory fields !.' };
                AppUtilities.handleResponse(response);
            }
        } else if(nextTab == 4) {
            if($scope.vehicleServiceForm.$valid) {
                if($scope.serviceRow.length == 0) {
                    var response = { status: 0, message: 'Please select atleast one service !.' };
                    AppUtilities.handleResponse(response);
                } else {
                    $scope.tab = nextTab;
                }
            } else {
                var response = { status: 0, message: 'Please filled up manadatory fields !.' };
                AppUtilities.handleResponse(response);
            }
        } else {
            $scope.tab = nextTab;
        }
    }

    $scope.vehicleJobSearch = function() {
        var getdate = AppUtilities.getDateFormat(new Date());;
        $scope.bindVehicleSearchForm.from_date = getdate;
        $scope.bindVehicleSearchForm.end_date = getdate;
    }

    $scope.viewJobList = function() {
        AppUtilities.blockUI(1);
        $scope.arrJobList = [];
        $scope.isSearchJobRecords = 0;
        var path = $scope.statePath + '/view_job_list/' + $scope.memberFirmId+'/'+$scope.companyYearId;
        var params  = $scope.bindVehicleSearchForm;
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrJobList = angular.copy(response.data);
                $scope.isSearchJobRecords = 1;
                $('#searchDiv').slideToggle();
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                $scope.isSearchJobRecords = 1;
                AppUtilities.unblockUI();
                return false;
            });
    }

    $scope.getPaymentDetail = function(getId = 0) {
        if(getId > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/payment_detail/' +$scope.memberFirmId+'/'+$scope.companyYearId+'/'+getId)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $scope.getBankList();
                        console.log(response);
                        $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
                        var getDate = AppUtilities.getDateFormat(new Date());
                        $scope.payment_date = getDate;
                        $scope.payment_mode_date = getDate;
                        $scope.vehicle_job_master_id = getId;
                        $scope.payment_mode = 1;
                        $scope.total = response.data.total;
                        $scope.paid = response.data.total;
                        $scope.partial = response.data.partial;
                        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/payment_form.html';
                        $scope.modalInstance = $modal.open({
                            animation:true,
                            templateUrl: url,
                            controller:'VehicleJobController',
                            backdrop:'static',
                            keyboard:false,
                            windowClass: 'modal',
                            size: 'lg',
                            scope: $scope
                        });
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                    });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please select valid data!.' });
        }
    }
    
    $scope.payment = function() {
        var total = AppUtilities.numberFormat($scope.total,2);
        var paid = AppUtilities.numberFormat($scope.paid,2);
        $scope.shortFees = 0;
        if(total > paid) {
            $scope.shortFees = AppUtilities.numberFormat(total - paid,2);
        } else {
            $scope.paid = total;
        }
    }

    $scope.getBankList = function(flag = 0) {
        $scope.arrBankData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_bank_list';
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrBankData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.savePayment = function() {
        if ($scope.paymentFormSubmission.$valid) {
            $('#update_btn').attr('disabled', true);
            $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
            var savePath = $scope.statePath + '/save_payment';
            var saveForms = new FormData($('#paymentFormSubmission')[0]);
            saveForms.append('id',$scope.vehicle_job_master_id);
            saveForms.append('short_fees',$scope.shortFees);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    $scope.modalInstance.close();
                    $scope.viewJobList();
                })
                .error(function(response) {
                    $('#update_btn').attr('disabled', false);
                    $('#update_btn').html('Save');
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

}]);