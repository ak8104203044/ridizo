'use strict';
radizoApp.controller('SettingsController', ['$rootScope', '$scope', '$state', '$controller', 'AppService', 'AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, $state, $controller, AppService, AppUtilities, GLOBAL_DIR) {
    console.log('settings controller called ........');
    $scope.generalSettingTab = 1;
    $scope.bindScopeForm = {};
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'setting';
    $scope.bindScopeForm.logoPath = '';
    $scope.ImagePath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR + '/' + GLOBAL_DIR.LOGODIR + '/';
    $scope.settingLogoPath = '';
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.currentFirmId = localStorage.getItem('firmId');
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }
    
    $scope.generalSetting = function() {
        AppUtilities.blockUI(1);
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.getCountryList(0);
        var path = $scope.statePath + '/general_settings';
        AppService.getHttpRequest(path)
            .success(function(response) {
                response.data.forEach(function(data, key) {
                    $scope.bindScopeForm[data.field_name] = data.field_value;
                });
                $scope.settingLogoPath = $scope.ImagePath + $scope.bindScopeForm.logo;
                $('#logo').val($scope.bindScopeForm.logo);
                $scope.getStateList(0);
                $scope.getCityList(0);
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
            });
    }

    $scope.uploads = function(element) {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/upload_files';
        var params = { getFile: element, old_file: $('#logo').val(), path: path };
        AppUtilities.uploadFiles(params)
            .success(function(response) {
                AppUtilities.unblockUI();
                $scope.settingLogoPath = response.src;
                $('#logo').val(response.filename);
                $scope.bindScopeForm.logo = response.filename;
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    }

    $scope.saveGeneralSetting = function() {
        if ($scope.formSubmission.$valid) {
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            AppUtilities.blockUI(1);
            var saveForms = new FormData($('#formSubmission')[0]);
            var path = $scope.statePath + '/save_general_settings';
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go('dashboard', null, { reload: true });
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    }

    $scope.userSetting = function() {
        if($scope.currentFirmId !== null) {
            AppUtilities.blockUI(1);
            var path = $scope.statePath + '/user_settings/' + $scope.currentFirmId;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.bindScopeForm = response.data;
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                });
        } else {
            AppUtilities.handleResponse({status:0,message:GLOBAL_DIR.MESSAGE.VALID_MEMBER});
        }
    }

    $scope.saveUserSetting = function() {
        if ($scope.formSubmission.$valid && $scope.currentFirmId != null) {
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            AppUtilities.blockUI(1);
            var saveForms = new FormData($('#formSubmission')[0]);
            if (typeof $scope.bindScopeForm.id !== 'undefined' && $scope.bindScopeForm.id !== null) {
                saveForms.append('id',$scope.bindScopeForm.id);
            }
            saveForms.append('branch_master_id',localStorage.getItem('firmId'));
            var path = $scope.statePath + '/save_user_settings';
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    console.log(response);
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go('dashboard', null, { reload: true });
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    }
}]);
