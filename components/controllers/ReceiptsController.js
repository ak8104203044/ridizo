'use strict';
var radizoApp = angular.module('radizoApp',[]);
radizoApp.controller('ReceiptsController', ['$rootScope', '$scope', function($rootScope, $scope) {
    $scope.arrReceiptData = [];
    $scope.length = 0;
    $scope.subtotal = 0;
    $scope.init = function() {
        $scope.fileName = localStorage.getItem('fileName');
        $scope.arrReceiptData = localStorage.getItem('arrReceiptData');
        $scope.caption = localStorage.getItem('caption');
        if($scope.arrReceiptData !== null) {
            $scope.arrReceiptData = JSON.parse($scope.arrReceiptData);
        }
        console.log('receipts:',$scope.arrReceiptData);
    };

    $scope.calculateAmt = function(Amt = 0) {
        Amt = Number(Amt);
        $scope.subtotal += Amt;
    }

}]);