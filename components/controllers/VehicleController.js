'use strict';
radizoApp.controller('VehicleController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', '$controller','$modal','$window','GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, $controller,$modal,$window,GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_masters';
    $scope.exportFile = 'vehicle';
    $scope.caption = 'Vehicle';
    $scope.bindScopeForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.isVehicleShow = 0;
    $scope.vehicleType = 6;
    $scope.searchVehicleType = 6;
    $scope.arrVehicleSpecificationData = [];
    $scope.getImagePath = $rootScope.IMAGE_URL + '/' + GLOBAL_DIR.VEHICLEDIR + '/';
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.imageRow = [{}];
    $scope.options = {language: 'en',allowedContent: true,entities: false};
    
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.init = function() {
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Vehicle Code',type:2,column:'unique_no',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Vehicle No',type:2,column:'vehicle_number',sort:true,sort_value:2,width:"10%",align:"left"},
            {caption:'Make',type:2,column:'model',sort:true,sort_value:3,width:"15%",align:"left"},
            {caption:'Purchase Price',type:2,column:'purchase',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Grade',type:2,column:'grade',sort:false,sort_value:0,width:"10%",align:"left"},
            {caption:'Action',type:3,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:6}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $scope.initTable(1);
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.bindScopeForm.registration_date = AppUtilities.getDateFormat(new Date());
        $scope.getVehicleCode();
        $scope.getVehicleTypeList(0);
        $scope.getCountryList(0);
        $scope.getTitleList(0);
        $scope.getVariantList(0);
        $scope.getGradeList(0);
        $scope.id = null;
        AppUtilities.unblockUI();
    };

    $scope.save = function() {
        if ($scope.vehicleBasicForm.$valid && $scope.vehicleOwnerForm.$valid && $scope.memberFirmId > 0) {
            AppUtilities.blockUI(1);
            AppUtilities.btnBehaviour($rootScope.btnGroup,1);
            var savePath = $scope.statePath + '/save';
            var saveForms = new FormData($('#vehicleBasicForm')[0]);
            if (typeof $scope.id !== 'undefined' && $scope.id !== null && $scope.id != '') {
                saveForms.append('id',$scope.id);
            }
            var ownerforms = $('#vehicleOwnerForm').serializeArray();
            angular.forEach(ownerforms,function(element){
                saveForms.append(element.name,element.value);
            });
            var spforms = $('#vehicleSpecForm').serializeArray();
            angular.forEach(spforms,function(element){
                saveForms.append(element.name,element.value);
            });
            var imgforms = $('#vehicleImageForm').serializeArray();
            angular.forEach(imgforms,function(element){
                saveForms.append(element.name,element.value);
            });
            saveForms.append('other_detail',($scope.bindScopeForm.other_detail !== null ? $scope.bindScopeForm.other_detail :''));
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('company_year_master_id' ,$scope.companyYearId);
            saveForms.append('vehicle_from',1);
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    AppUtilities.btnBehaviour($rootScope.btnGroup,2);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.edit = function(getId = null) {
        if (getId !== null && getId != '') {
            $controller('CommonFilterComponent', {$scope: $scope});
            AppUtilities.blockUI(1);
            $scope.getCountryList(0);
            AppService.getHttpRequest($scope.statePath + '/record/'+$scope.memberFirmId + '/' + getId + '/1')
                .success(function(response) {
                    //console.log(response);
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.imageRow = angular.copy(response.image);
                    $scope.bindScopeForm.specification = angular.copy(response.specification);
                    $scope.bindScopeForm.manufacturer_tran_id = response.data.manufacturer_tran_id;
                    $scope.employeeCode = response.data.unique_no;
                    $scope.getVehicleTypeList(0);
                    $scope.getManufacturerList(0,1);
                    $scope.getVehicleModelList(0,1);
                    $scope.vehicleSpecificationList();
                    $scope.getStateList(0);
                    $scope.getCityList(0);
                    $scope.getTitleList(0);
                    $scope.getVariantList(0);
                    $scope.getGradeList(0);
                    AppUtilities.unblockUI();
                }).error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.view = function(getId = null) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/'+ $scope.memberFirmId + '/'  + getId + '/2')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/' + $scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getVehicleCode = function() {
        var path = $rootScope.ADMIN_API_URL + '/system/vehicle_unique_code/';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.vehicleCode = response.code;
                if ($scope.companyYearId > 0 && $scope.memberFirmId > 0 && response.code != null && response.code != '') {
                    $scope.isVehicleShow = 1;
                } else {
                    AppUtilities.handleResponse({status:0,'message':'Please add vehicle code from user setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleCode = '';
                AppUtilities.handleResponse(response);
                $state.go($scope.baseStateTo, {}, { reload: true });
            });
    };

    $scope.uploads = function(element) {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/upload_files';
        var params = { getFile: element, old_file: '', path: path };
        AppUtilities.uploadFiles(params)
            .success(function(response) {
                AppUtilities.unblockUI();
                $('#preview_' + element.id).attr('src', response.src);
                $('#upload_image_' + element.id).val(response.filename);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    };

    $scope.validateForm = function(tab = 1) {
        if(tab == 2) {
            if($scope.vehicleBasicForm.$valid) {
                $scope.tab = tab;
            } else {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
            }
        } else if(tab == 3) {
            if($scope.vehicleOwnerForm.$valid) {
                $scope.tab = tab;
            } else {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
            }
        } else {}
    };

    $scope.addRow = function() {
        $scope.imageRow.push({});
    };

    $scope.deleteRow = function(i) {
        $scope.imageRow.splice(i, 1);
    };

    $scope.vehicleSaleForm = function(id = null) {
        if(id !== null && id != '') {
            $scope.submitBtn = {name:'Save',is_disabled :false};
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/'+ $scope.memberFirmId + '/'  + id + '/3')
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.total_amount = AppUtilities.numberFormat($scope.bindScopeForm.purchase_price + $scope.bindScopeForm.expense,2,'.','');
                    var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/vehicle/sale_form.html';
                    $modal.open({
                        templateUrl: url,
                        size: 'lg',
                        backdrop:'static',
                        keyboard:false,
                        windowClass: 'modal',
                        scope:$scope,
                        controller:function($scope,$modalInstance) {
                            $scope.saveVehicleSale = function() {
                                if($scope.vehicleSaleForm.$valid && $scope.memberFirmId > 0 && $scope.companyYearId > 0) {
                                    $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
                                    $scope.submitBtn.is_disabled = true;
                                    var savePath = $scope.statePath + '/save_vehicle_sale';
                                    AppService.postHttpRequest(savePath, $scope.bindScopeForm)
                                        .success(function(response) {
                                            AppUtilities.unblockUI();
                                            $modalInstance.close();
                                            AppUtilities.handleResponse({status:1,message:'Vehicle sale price added successfully!.'});
                                            $state.go($scope.baseStateTo, {}, { reload: true });
                                            $scope.submitBtn.name = 'Save';
                                            $scope.submitBtn.is_disabled = false;
                                        }).error(function(response) {
                                            AppUtilities.unblockUI();
                                            AppUtilities.handleResponse(response);
                                            $scope.submitBtn.name = 'Save';
                                            $scope.submitBtn.is_disabled = false;
                                        });
                                } else {
                                    AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
                                }
                            };
                            $scope.cancelModal = function() {
                                $modalInstance.close();
                            }
                        }
                    });
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_RESPONSE });
            $state.go($scope.baseStateTo);
        }
    };

    $scope.generateCertificateForm = function(records) {
        if(records !== null) {
            $controller('CommonFilterComponent', {$scope: $scope});
            $scope.getGradeList(0);
            //$scope.getCouponList(0);
            $scope.bindScopeForm = records;
            var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/vehicle/generate_certificate_form.html';
            $modal.open({
                templateUrl: url,
                size: 'lg',
                backdrop:'static',
                keyboard:false,
                windowClass: 'modal',
                scope:$scope,
                controller:function($scope,$modalInstance) {
                    $scope.saveCertificate = function() {
                        if($scope.vehicleCertificateForm.$valid && $scope.memberFirmId > 0 && $scope.companyYearId > 0) {
                            $('#update_btn').attr('disabled', true);
                            $('#update_btn').html('Processing....');
                            var savePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_masters'+ '/save_certificate';
                            var saveForms = new FormData($('#vehicleCertificateForm')[0]);
                            saveForms.append('branch_master_id',$scope.memberFirmId);
                            saveForms.append('company_year_master_id' ,$scope.companyYearId);
                            saveForms.append('id' ,$scope.bindScopeForm.id);
                            AppService.postHttpFormRequest(savePath, saveForms)
                                .success(function(response) {
                                    AppUtilities.unblockUI();
                                    $modalInstance.close();
                                    $('#update_btn').attr('disabled', false);
                                    $('#update_btn').html('Save');
                                    AppUtilities.handleResponse({status:1,message:'Vehicle added successfully'});
                                    $state.go($scope.baseStateTo, {}, { reload: true });
                                })
                                .error(function(response) {
                                    AppUtilities.unblockUI();
                                    AppUtilities.handleResponse(response);
                                    $('#update_btn').attr('disabled', false);
                                    $('#update_btn').html('Save');
                                });
                        } else {
                            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
                        }
                    };
                    $scope.cancelModal = function() {
                        $modalInstance.close();
                    }
                }
            });
        }
    };

    $scope.viewCertificate = function(rows) {
        if(typeof $scope.reportWindow !== 'undefined') {
            $scope.reportWindow.close();
        }
        localStorage.setItem('arrHeaderData',JSON.stringify([]));
        localStorage.setItem('arrReportData',JSON.stringify(rows));
        localStorage.setItem('caption','Vehicle Certificate');
        localStorage.setItem('fileName','vehicle/certificate.html');
        var path = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports/report.html';
        $scope.reportWindow = $window.open(path,'Certificate Report','height=890,width=1280,resizable=1,location=no');
    };

    $scope.deleteCertificate = function(id = 0) {
        var text = 'Do you want to delete certificate ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var savePath = $scope.statePath + '/delete_certificate/'+id+'/'+$scope.memberFirmId;
                AppService.getHttpRequest(savePath)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            console.log('response :',response);
                            AppUtilities.handleResponse(response);
                            $state.go($scope.baseStateTo, {}, { reload: true });
                        })
                        .error(function(response) {
                            AppUtilities.unblockUI();
                            AppUtilities.handleResponse(response);
                        });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
