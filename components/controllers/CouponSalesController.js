'use strict';
radizoApp.controller('CouponSalesController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities','$controller', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities,$controller, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'invoice_masters';
    $scope.exportFile = 'Coupon-sale';
    $scope.caption = 'Coupon Sale';
    $scope.bindScopeForm = {};
    $scope.tmpVehicleForm = {};
    $scope.id = $stateParams.id;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.module = $state.current.data.module;
    $scope.couponDetail = {};
    $scope.vehicleType = 4;
    $scope.searchVehicleType = 4;
    $scope.scopeType  = 2;
    $scope.submitBtn = {name:'Submit',is_disabled :false};

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.RIGHTS });
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.init = function() {
        $scope.dataTablePath = $scope.statePath + '/index/4';
        $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
        $scope.tableHeader = [
            {caption:'',type:1,column:'',sort:false,sort_value:0,width:"5%",align:"center"},
            {caption:'S.No.',type:2,column:'count',sort:false,sort_value:0,width:"5%",align:"left"},
            {caption:'Invoice No',type:2,column:'invoice_no',sort:true,sort_value:1,width:"15%",align:"left"},
            {caption:'Invoice Date',type:2,column:'invoice_date',sort:true,sort_value:2,width:"15%",align:"left"},
            {caption:'Amount',type:2,column:'amount',sort:true,sort_value:3,width:"10%",align:"left"},
            {caption:'Vehicle Code',type:2,column:'customer',sort:true,sort_value:4,width:"20%",align:"left"},
            {caption:'Status',type:5,column:'',sort:true,sort_value:4,width:"10%",align:"left"},
            {caption:'Action',type:6,column:'',sort:false,sort_value:0,width:"20%",align:"left",btnType:4}
        ];
        /** Initialize table grid **/
        $controller('dataTableController', {$scope: $scope});
        $controller('CommonFilterComponent', {$scope: $scope});
        $scope.initTable(1);
    };

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id }, { reload: statego });
    };

    $scope.add = function() {
        if($scope.memberFirmId !== null && $scope.companyYearId !== null) {
            $controller('CommonFilterComponent',{$scope:$scope});
            $scope.id = null;
            $scope.bindScopeForm.payment_mode = 1;
            $scope.bindScopeForm.date = AppUtilities.getDateFormat(new Date());
            $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
            $scope.getUniqueCode(2);
            $scope.getBankList(0);
            $scope.getCouponList(0,2);
        }
    };

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.bindScopeForm.vehicle_master_id > 0) {
            AppUtilities.blockUI(1);
            $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
            $scope.submitBtn.is_disabled = true;
            var savePath = $scope.statePath + '/save_coupon_invoice';
            var saveForms = new FormData($('#formSubmission')[0]);
            saveForms.append('branch_master_id',$scope.memberFirmId);
            saveForms.append('vehicle_master_id',$scope.bindScopeForm.vehicle_master_id);
            saveForms.append('company_year_master_id',$scope.companyYearId);
            saveForms.append('actual_amount',$scope.bindScopeForm.actual_amount);
            saveForms.append('invoice_json',JSON.stringify($scope.couponDetail));
            AppService.postHttpFormRequest(savePath, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $state.go($scope.baseStateTo);
                    AppUtilities.handleResponse(response);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.submitBtn.name = 'Submit';
                    $scope.submitBtn.is_disabled = false;
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.MANDATORY_FIELDS });
        }
    };

    $scope.delete = function() {
        AppUtilities.confirmDeletion(2)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/cancel_invoice/4/'+$scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
                return false;
            });
    };

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode) {
                    AppUtilities.handleResponse({status:0,'message':'Please add job code from user setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleJobCode = '';
                AppUtilities.handleResponse({status:0,'message':response.message});
                $state.go($scope.baseStateTo, {}, { reload: true });
            });
    };

    $scope.getCouponDetail = function() {
        $scope.bindScopeForm.amount = 0;
        $scope.bindScopeForm.actual_amount = 0;
        if(typeof $scope.bindScopeForm.coupon_master_id !== "undefined" && $scope.bindScopeForm.coupon_master_id !== null) {
            $scope.bindScopeForm.amount = 0;
            var couponDetail = $scope.arrCouponData.find(function(element) {
                return element.id == $scope.bindScopeForm.coupon_master_id;
            });
            if(couponDetail) {
                $scope.couponDetail.product = [];
                $scope.couponDetail.product.push({
                    name:couponDetail.name,
                    code:couponDetail.code,
                    quantity:1,
                    price:couponDetail.amount,
                    subtotal:couponDetail.amount,
                    total:couponDetail.amount,
                    tax:0,
                    discount:0
                });
                $scope.couponDetail.summary = {
                    subtotal : couponDetail.amount,
                    total:couponDetail.amount,
                    discount:0,
                    tax:0
                };
                $scope.couponDetail.product
                $scope.couponDetail.tax = [];
                $scope.bindScopeForm.actual_amount = Number(couponDetail.amount);
                $scope.bindScopeForm.amount = Number(couponDetail.amount);
            }
        }
    };

    $scope.calculateAmt  = function(type = 1) {
        if(type == 1) {
            $scope.showShortFees = false;
            $scope.bindScopeForm.amount = ($scope.bindScopeForm.amount !== undefined) ? $scope.bindScopeForm.amount : "";
            if($scope.bindScopeForm.amount < $scope.bindScopeForm.actual_amount) {
                $scope.showShortFees = true;
                $scope.bindScopeForm.shortFees = AppUtilities.numberFormat($scope.bindScopeForm.actual_amount - $scope.bindScopeForm.amount,2);
            } else {
                $scope.bindScopeForm.amount = $scope.bindScopeForm.actual_amount;
                $scope.bindScopeForm.shortFees = 0;
            }
        } else {
            var total = AppUtilities.numberFormat($scope.totalDueAmount,2);
            var paid = AppUtilities.numberFormat($scope.paid,2);
            $scope.shortFees = 0;
            if(total > paid) {
                $scope.shortFees = AppUtilities.numberFormat(total - paid,2);
            } else {
                $scope.paid = total;
            }
        }
    };

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            $scope.exportPath = $scope.statePath + '/index/4/2';
            $scope.initExportTable(exportType);
        } else {
            AppUtilities.handleResponse({ status: 0, message: GLOBAL_DIR.MESSAGE.INVALID_EXPORT });
        }
    };
}]);
