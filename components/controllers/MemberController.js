'use strict';
radizoApp.controller('MemberController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, GLOBAL_DIR) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'user_member_masters';
    $scope.exportFile = 'member';
    $scope.caption = 'Franchise';
    $scope.baseStateTo = 'dashboard.member';
    $scope.bindScopeForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.memberTab = 1;
    $scope.arrServiceGroupData = [];
    $scope.memberPhoto = '';
    $scope.memberLogo = '';
    $scope.memberPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR + '/';
    $scope.arrGender = ['Male', 'Female', 'Transgender'];
    $scope.rights = localStorage.getItem('userRights');
    if (typeof $scope.rights === 'string' && $scope.rights != '') {
        $scope.rights = JSON.parse($scope.rights);
        $scope.userRights = (typeof $scope.rights.service !== 'undefined') ? $scope.rights.service : {};
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = $scope.baseStateTo + '-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    };

    $scope.add = function() {
        AppUtilities.blockUI(1);
        $scope.getCountryList(0);
        $scope.getTitleList(0);
        $scope.getMemberTypeList(0);
        $scope.getMemberCode();
        $scope.id = null;
        AppUtilities.unblockUI();
    }

    $scope.save = function(getId) {
        if ($scope.formSubmission.$valid) {
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var savePath = $scope.statePath + '/save';
            var saveForms = AppUtilities.safeConvertObject('formSubmission');
            if (typeof getId !== 'undefined' && getId > 0) {
                saveForms.id = getId;
                saveForms.old_photo = $scope.bindScopeForm.photo;
                saveForms.old_logo = $scope.bindScopeForm.logo;
            }
            saveForms.title_master_id = (typeof $scope.bindScopeForm.title_master_id !== 'undefined') ? $scope.bindScopeForm.title_master_id : '';
            saveForms.country_master_id = (typeof $scope.bindScopeForm.country_master_id !== 'undefined') ? $scope.bindScopeForm.country_master_id : '';
            saveForms.state_master_id = (typeof $scope.bindScopeForm.state_master_id !== 'undefined') ? $scope.bindScopeForm.state_master_id : '';
            saveForms.city_master_id = (typeof $scope.bindScopeForm.city_master_id !== 'undefined') ? $scope.bindScopeForm.city_master_id : '';
            saveForms.member_type_tran_id = (typeof $scope.bindScopeForm.member_type_tran_id !== 'undefined') ? $scope.bindScopeForm.member_type_tran_id : '';
            AppService.postHttpRequest(savePath, saveForms)
                .success(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    $state.go($scope.baseStateTo, {}, { reload: true });
                    if (response.is_send_mail == 1) {
                        AppService.getHttpRequest($rootScope.ADMIN_API_URL + '/system/send_mail/1/' + response.id);
                    }
                    AppUtilities.handleResponse(response);
                })
                .error(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var response = { status: 0, message: 'Please filled up manadatory fields !.' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.edit = function(getId) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = response.data;
                    $scope.memberLogo = $scope.memberPath + GLOBAL_DIR.LOGODIR + '/' + response.data.logo;
                    $scope.memberPhoto = $scope.memberPath + GLOBAL_DIR.MEMBERDIR + '/' + response.data.photo;
                    $scope.getCountryList(0);
                    $scope.getStateList(0);
                    $scope.getCityList(0);
                    $scope.getTitleList(0);
                    $scope.getMemberTypeList(0);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.view = function(getId) {
        if (getId !== null && getId != '') {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/record/' + getId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = response.data;
                    $scope.memberLogo = $scope.memberPath + GLOBAL_DIR.LOGODIR + '/' + response.data.logo;
                    $scope.memberPhoto = $scope.memberPath + GLOBAL_DIR.MEMBERDIR + '/' + response.data.photo;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid response type !.' });
            $state.go($scope.baseStateTo);
        }
    }

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete';
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: true });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.getCountryList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrCountryData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'country' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrCountryData = response.data;
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getStateList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrStateData = [];
        $scope.arrCityData = [];
        if (typeof $scope.bindScopeForm.country_master_id !== 'undefined') {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'state', 'country_master_id': $scope.bindScopeForm.country_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    $scope.arrStateData = response.data;
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                })
                .error(function(response) {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.bindScopeForm.state_master_id = '';
                    $scope.bindScopeForm.city_master_id = '';
                });
        } else {
            if (flag == 1) {
                AppUtilities.unblockUI();
            }
            $scope.bindScopeForm.state_master_id = '';
            $scope.bindScopeForm.city_master_id = '';
        }
    }

    $scope.getCityList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrCityData = [];
        if (typeof $scope.bindScopeForm.state_master_id !== 'undefined') {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'city', 'state_master_id': $scope.bindScopeForm.state_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    $scope.arrCityData = response.data;
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                })
                .error(function(response) {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.bindScopeForm.city_master_id = '';
                });
        } else {
            if (flag == 1) {
                AppUtilities.unblockUI();
            }
            $scope.bindScopeForm.city_master_id = '';
        }
    }

    $scope.getTitleList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrTitleData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'title' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrTitleData = response.data;
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getMemberTypeList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrMemberTypeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'franchise' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrMemberTypeData = response.data;
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
    }

    $scope.getMemberCode = function() {
        $scope.memberCode = null;
        var path = $rootScope.ADMIN_API_URL + '/system/member_unique_no';
        AppService.getHttpRequest(path)
            .success(function(response) {
                if(response.code != '') {
                    $scope.memberCode = response.code;
                } else {
                    AppUtilities.handleResponse({status:0,'message':'Please define firm code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                AppUtilities.handleResponse(response);
                $state.go($scope.baseStateTo, {}, { reload: true });
                $scope.memberCode = null;
            });

    }

    $scope.uploads = function(element, type) {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/upload_files';
        var params = { getFile: element, old_file: $('#logo').val(), path: path };
        AppUtilities.uploadFiles(params)
            .success(function(response) {
                AppUtilities.unblockUI();
                if (type == 1) {
                    $scope.memberPhoto = response.src;
                    $('#photo').val(response.filename);
                } else {
                    $scope.memberLogo = response.src;
                    $('#logo').val(response.filename);
                }
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    }

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            AppUtilities.blockUI(1);
            var exportPath = $scope.statePath + '/export/';
            AppService.getHttpRequest(exportPath)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.export(response.header, response.data, $scope.exportFile + '_' + response.date, exportType);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Invalid Export Type' });
        }
    }
}]);