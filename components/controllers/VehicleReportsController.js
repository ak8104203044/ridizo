'use strict';
radizoApp.controller('VehicleReportsController', ['$rootScope', '$scope', '$state', 'AppService', 'AppUtilities', '$window', function($rootScope, $scope,$state, AppService, AppUtilities, $window) {
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_report';
    $scope.rights = localStorage.getItem('userRights');
    $scope.baseStateTo = $state.current.data.stateName;
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.module = $state.current.data.module;
    $scope.arrHeaderData = [];
    $scope.arrReportData = [];
    $scope.bindScopeForm = {};
    $scope.oneAtATime = false;
    $scope.status = {open: true,other:false,customer:false,filter:false};
    $scope.reportPath = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/reports';

    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        try {
            $scope.rights = JSON.parse($scope.rights);
            $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
            if (Object.keys($scope.userRights).length <= 0) {
                var response = { status: 0, message: 'You don\'t have rights to access this location' };
                AppUtilities.handleResponse(response);
                $state.go('dashboard', {}, { reload: true });
            }
        } catch(e) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }

    $scope.generateReportType = function(flag = 1) {
        if(flag === 1) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrSortByData = [{id:1,name:"Booking Date"},{id:2,name:"Vehicle Type"},{id:3,name:"Manufacturer"},{id:4,name:"Vehicle Model"},{id:5,name:"Vehicle Code"},{id:6,name:"Customer Name"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getBranchList();
            $scope.getVehicleTypeList();
            $scope.getCompanyYearList();
        } else if(flag === 2) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrSortByData = [{id:1,name:"Loan Date"},{id:2,name:"Vehicle Type"},{id:3,name:"Manufacturer"},{id:4,name:"Vehicle Model"},{id:5,name:"Vehicle Code"},{id:6,name:"Customer Name"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getBranchList();
            $scope.getVehicleTypeList();
            $scope.getCompanyYearList();
        } else if(flag === 3) {
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrSortByData = [{id:1,name:"Registration Date"},{id:2,name:"Vehicle Type"},{id:3,name:"Manufacturer"},{id:4,name:"Vehicle Model"},{id:5,name:"Vehicle Code"},{id:6,name:"Customer Name"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.getBranchList();
            $scope.getVehicleTypeList();
        } else if(flag === 4) {
            $scope.arrPaymentTypeData = [{id:1,name:"Unpaid"},{id:2,name:'Partial Payment'},{id:3,name:'Full Payment'}];
            var today = AppUtilities.getDateFormat(new Date());
            $scope.bindScopeForm.from_date = today;
            $scope.bindScopeForm.end_date = today;
            $scope.arrSortByData = [{id:1,name:"Sale Date"},{id:2,name:"Vehicle Type"},{id:3,name:"Manufacturer"},{id:4,name:"Vehicle Model"},{id:5,name:"Vehicle Code"},{id:6,name:"Customer Name"}];
            $scope.arrSortTypeData = [{id:1,name:'ASC'},{id:2,name:'DESC'}];
            $scope.bindScopeForm.sort_by = 1;
            $scope.bindScopeForm.sort_type = 1;
            $scope.bindScopeForm.branch_master_id = $scope.memberFirmId;
            $scope.bindScopeForm.company_year_master_id = $scope.companyYearId;
            $scope.getBranchList();
            $scope.getVehicleTypeList();
            $scope.getCompanyYearList();
        } else {}
    }

    $scope.generateReport = function(flag = 1) {
        if ($scope.reportFormSubmission.$valid) {
            if(typeof $scope.reportWindow !== 'undefined') {
                $scope.reportWindow.close();
            }
            $('#record_save_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var path,caption,fileName;
            flag = (flag > 0) ? Number(flag) : 1;
            if(flag === 1) {
                path = $scope.statePath + '/bike_rent_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Bike Rent Dynamic Report';
                fileName = 'vehicle/bike_rent_dynamic_report.html';
            } else if(flag === 2) {
                path = $scope.statePath + '/bike_finance_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Bike Finance Dynamic Report';
                fileName = 'vehicle/bike_finance_dynamic_report.html';
            } else if(flag === 3) {
                path = $scope.statePath + '/vehicle_dynamic_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Vehicle Dynamic Report';
                fileName = 'vehicle/vehicle_dynamic_report.html';
            } else if(flag === 4){
                path = $scope.statePath + '/dynamic_vehicle_sale_report';
                caption = (typeof $scope.bindScopeForm.caption !== 'undefined' && $scope.bindScopeForm.caption !== null) ? $scope.bindScopeForm.caption : 'Vehicle Dynamic Report';
                fileName = 'vehicle/dynamic_vehicle_sale_report.html';
            } else {}
            var saveForms = new FormData($('#reportFormSubmission')[0]);
            AppService.postHttpFormRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    console.log('response :',response);
                    localStorage.setItem('arrHeaderData',JSON.stringify(response.header));
                    localStorage.setItem('arrReportData',JSON.stringify(response.data));
                    localStorage.setItem('caption',caption);
                    localStorage.setItem('fileName',fileName);
                    $scope.reportWindow =$window.open($scope.reportPath+'/report.html','StockReport','height=890,width=1280,resizable=1,location=no');
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $('#record_save_btn').attr('disabled', false);
                    $('#record_save_btn').html('Submit');
                });
        } else {
            AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
        }
    }

    $scope.getBranchList = function() {
        $scope.arrBranchData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_branch_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBranchData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getCompanyYearList = function() {
        $scope.arrCompnayYearData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_company_year_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCompnayYearData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getVehicleTypeList = function() {
        $scope.arrVehicleTypeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_type_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrVehicleTypeData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    }

    $scope.getManufacturerList = function(flag = 0) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        $scope.arrManufacturerData = [];
        $scope.arrVehicleModelData = [];
        $scope.bindScopeForm.vehicle_model_master_id = '';
        $scope.bindScopeForm.manufacturer_tran_id = '';
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        if(typeof $scope.bindScopeForm.vehicle_type_master_id !== 'undefined' && $scope.bindScopeForm.vehicle_type_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_manufacturer_vehicle_list/'+$scope.bindScopeForm.vehicle_type_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrManufacturerData = angular.copy(response.data);
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                }).error(function(response) {
                    $scope.bindScopeForm.manufacturer_tran_id = '';
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                });
        } else {
            $scope.bindScopeForm.manufacturer_tran_id = '';
            if (flag == 1) {
                AppUtilities.unblockUI();
            }
        }
    }

    $scope.getVehicleModelList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrVehicleModelData = [];
        if (typeof $scope.bindScopeForm.manufacturer_tran_id !== 'undefined' && $scope.bindScopeForm.manufacturer_tran_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_model_list/'+$scope.bindScopeForm.manufacturer_tran_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrVehicleModelData = angular.copy(response.data);
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                })
                .error(function() {
                    if (flag == 1) {
                        AppUtilities.unblockUI();
                    }
                    $scope.bindScopeForm.vehicle_model_master_id = '';
                })
        } else {
            if (flag == 1) {
                AppUtilities.blockUI(1);
            }
            $scope.bindScopeForm.vehicle_model_master_id = '';
        }
    }
}]);
