'use strict';
radizoApp.controller('AssignRoleRightsController', ['$rootScope', '$scope', '$state', '$stateParams', 'AppService', 'AppUtilities', 'GLOBAL_DIR', function($rootScope, $scope, $state, $stateParams, AppService, AppUtilities, GLOBAL_DIR) {
    console.log('assign role rights controller called ........');
    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'role_link_operation_rights_trans';
    $scope.caption = 'Assign Role Rights';
    $scope.bindScopeForm = {};
    $scope.baseStateTo = $state.current.name.split('-')[0];
    if (typeof $scope.rights === 'string' && $scope.rights !== null && $scope.baseStateTo !== null) {
        $scope.rights = JSON.parse($scope.rights);
        $scope.module = $scope.baseStateTo.replace('dashboard.','');
        $scope.userRights = (typeof $scope.rights[$scope.module] !== 'undefined') ? $scope.rights[$scope.module] : {};
        if (Object.keys($scope.userRights).length <= 0) {
            var response = { status: 0, message: 'You don\'t have rights to access this location' };
            AppUtilities.handleResponse(response);
            $state.go('dashboard', {}, { reload: true });
        }
    }
    $scope.index = function() {
        var path = $scope.statePath + '/get_role_list/' + localStorage.getItem('userType');
        AppUtilities.blockUI(1);
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.parentRole = response.role;
                $scope.parentModule = response.module;
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
            });
    }

    $scope.search = function() {
        AppUtilities.blockUI(1);
        var searchPath = $scope.statePath + '/search_modules';
        var searchForms = {};
        searchForms.module_id = (typeof $scope.bindScopeForm.module_id === 'undefined') ? '' : $scope.bindScopeForm.module_id;
        searchForms.role_master_id = (typeof $scope.bindScopeForm.role_master_id === 'undefined') ? '' : $scope.bindScopeForm.role_master_id;
        AppService.postHttpRequest(searchPath, searchForms)
            .success(function(response) {
                AppUtilities.unblockUI();
                console.log(response);
                var roleRightsHtml = AppUtilities.roleRightsQuery(response.modules, response.operation, response.rights);
                $('#roleLinkHtml').html(roleRightsHtml);
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    }

    $scope.save = function() {
        AppUtilities.blockUI(1);
        $('#save_record').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...');
        $('#save_record').attr('disabled', true);
        var saveData = new FormData($('#formSubmission')[0]);
        var searchPath = $scope.statePath + '/save';
        AppService.postHttpFormRequest(searchPath, saveData)
            .success(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
                $('#save_record').attr('disabled', false);
                $('#save_record').html('Save');
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
                $('#save_record').attr('disabled', false);
                $('#save_record').html('Save');
            });
        return false;
    }

    $scope.index();
}]);