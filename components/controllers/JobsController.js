'use strict';
radizoApp.controller('JobsController', ['$rootScope', '$scope', '$state', '$stateParams', '$modal', 'AppService', 'AppUtilities', 'GLOBAL_DIR','$filter', function($rootScope, $scope, $state, $stateParams, $modal, AppService, AppUtilities, GLOBAL_DIR,$filter) {

    $scope.statePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_job_masters';
    //$scope.item = $filter('date')('2019-02-13', "dd/MM/yyyy");
    $scope.exportFile = 'job';
    $scope.caption = 'Job';
    $scope.bindScopeForm = {};
    $scope.tmpVehicleForm = {};
    $scope.path = $scope.statePath + '/index';
    $scope.pageLength = GLOBAL_DIR.DEFAULT_TABLEGRID_LENGTH;
    $scope.id = $stateParams.id;
    $scope.tab = 1;
    $scope.rights = localStorage.getItem('userRights');
    $scope.memberFirmId = localStorage.getItem('firmId');
    $scope.companyYearId = localStorage.getItem('companyYearId');
    $scope.isVehicleShow = 0;
    $scope.tmpVehicleForm.fuel_type = 1;
    $scope.tmpVehicleForm.vehicle_start_method = 1;
    $scope.getVehicleDetail = {};
    $scope.isVehicleAdd = 0;
    $scope.arrChecklistData = [];
    $scope.partRow = [{}];
    $scope.serviceRow = [{}];
    $scope.partObj = {};
    $scope.serviceObj = {};
    $scope.isDuplicateParts = 0;
    $scope.isDuplicateService = 0;
    $scope.isSearchJobRecords = 0;
    if (typeof $rootScope.baseStateTo === 'undefined') {
        $rootScope.baseStateTo = 'dashboard.job';
    }

    $scope.arrPaymentTypeData = [{ id: 1, name: 'Cash' }, { id: 2, name: 'Credit Card' }, { id: 3, name: 'Debit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
    if (typeof $scope.rights === 'string' && $scope.rights != '') {
        $scope.rights = JSON.parse($scope.rights);
        $scope.userRights = (typeof $scope.rights.vehicle !== 'undefined') ? $scope.rights.vehicle : {};
    }

    /** page redirect function **/
    $scope.redirectState = function(statego, id) {
        var stateForward = 'dashboard.job-' + statego;
        id = (typeof id !== 'undefined') ? id : null;
        $state.go(stateForward, { 'id': id });
    }

    $scope.add = function() {
        $scope.bindScopeForm = {};
        AppUtilities.blockUI(1);
        $scope.id = null;
        $scope.getUniqueCode(3);
        $scope.checkListDetail();
        $scope.getUnitList();
        $scope.getPartGroupList(0);
        $scope.getServiceGroupList(0);
        AppUtilities.unblockUI();
    }

    $scope.getUniqueCode = function(type) {
        var path = $rootScope.ADMIN_API_URL + '/system/user_setting_unique_no/' + $scope.memberFirmId + '/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.uniqueCode = response.code;
                if(!$scope.uniqueCode || !$scope.memberFirmId || !$scope.companyYearId) {
                    AppUtilities.handleResponse({status:0,'message':'Please define job code setting!..'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
            })
            .error(function(response) {
                $scope.vehicleJobCode = '';
            });
    }

    $scope.showVehicleModel = function() {
        $('#vehicleModel').modal();
        $scope.getVehicleTypeList(0);
    }

    $scope.searchVehicle = function() {
        $scope.getVehicleData = [];
        $scope.vehicleSearchDiv = 0;
        $scope.getVehicleDetail = {};
        $scope.getVehicleDetail.is_exists = 0;
        console.log('search text :',$scope.searchVehicleKey);
        var path = $rootScope.ADMIN_API_URL + '/system/search_vehicle';
        var params = { search: $scope.searchVehicleKey };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.vehicleSearchDiv = 1;
                $scope.getVehicleData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.selectMatch = function(index) {
        if(typeof $scope.getVehicleData[index] !== 'undefined') {
            $scope.vehicleSearchDiv = 0;
            $scope.isVehicleShow = 1;
            $scope.searchVehicleKey = $scope.getVehicleData[index]['unique_no'];
            $scope.getVehicleDetail = $scope.getVehicleData[index];
            $scope.bindScopeForm.customer_mobile_no = $scope.getVehicleDetail.mobile_no;
            $scope.bindScopeForm.customer_address = $scope.getVehicleDetail.address;
            $scope.bindScopeForm.customer_name = $scope.getVehicleDetail.name;
            $scope.getVehicleDetail.is_exists = 1;
        } else {
            $scope.isVehicleShow = 0;
            $scope.getVehicleDetail = {};
        }
    }

    $scope.saveVehicle = function() {
        $scope.tmpVehicleForm.fuel_type = $("input[name='fule_type']:checked").val();
        $scope.tmpVehicleForm.vehicle_start_method = $("input[name='vehicle_start_method']:checked").val();
        $scope.tmpVehicleForm.is_exists = 0;
        $scope.isVehicleShow = 1;

        var manufacturerData = $scope.arrManufacturerData.find(function(element) { return element.mtranid == $scope.tmpVehicleForm.manufacturer_tran_id });
        if (typeof manufacturerData !== 'undefined') {
            $scope.tmpVehicleForm.manufacturer = manufacturerData.name;
        }

        var vehicleTypeData = $scope.arrVehicleTypeData.find(function(element) { return element.id == $scope.tmpVehicleForm.vehicle_type_master_id });

        if (typeof vehicleTypeData !== 'undefined') {
            $scope.tmpVehicleForm.vehicle_type = vehicleTypeData.name;
        }
        
        var vehicleModelData = $scope.arrVehicleModelData.find(function(element) { return element.id == $scope.tmpVehicleForm.vehicle_model_master_id });
        if (typeof vehicleModelData !== 'undefined') {
            $scope.tmpVehicleForm.model = vehicleModelData.name;
        }
        $scope.isVehicleAdd = 1;
        $scope.tmpVehicleForm.name = $scope.tmpVehicleForm.first_name + ' ' + (typeof $scope.tmpVehicleForm.middle_name !== 'undefined' ? $scope.tmpVehicleForm.middle_name : '') + ' ' + (typeof $scope.tmpVehicleForm.last_name !== 'undefined' ? $scope.tmpVehicleForm.last_name : '');
        $scope.getVehicleDetail = angular.copy($scope.tmpVehicleForm);
        $scope.bindScopeForm.customer_mobile_no = $scope.getVehicleDetail.mobile_no;
        $scope.bindScopeForm.customer_address = $scope.getVehicleDetail.address;
        $scope.bindScopeForm.customer_name = $scope.getVehicleDetail.name;
        $scope.searchVehicleKey = '';
        $('#vehicleModel').modal('hide');
    }

    $scope.getVehicleTypeList = function(flag) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        $scope.arrVehicleTypeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'vehicle_type' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrVehicleTypeData = response.data;
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            })
            .error(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    }

    $scope.getManufacturerList = function(flag,isReload) {
        flag = (typeof flag !== 'undefined') ? flag : 0;
        isReload = (typeof isReload !== 'undefined') ? isReload : 0;
        $scope.arrManufacturerData = [];
        if(isReload == 0) {
            $scope.arrVehicleModelData = [];
            $scope.tmpVehicleForm.vehicle_model_master_id = '';
        }
        
        if(typeof $scope.tmpVehicleForm.vehicle_type_master_id !== 'undefined' && $scope.tmpVehicleForm.vehicle_type_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'manufacturer_vehicle',id:$scope.tmpVehicleForm.vehicle_type_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    $scope.arrManufacturerData = response.data;
                })
                .error(function(response) {
                    $scope.tmpVehicleForm.manufacturer_tran_id = '';
                });
        } else {
            $scope.tmpVehicleForm.manufacturer_tran_id = '';
        }
    }

    $scope.getVehicleModelList = function() {
        $scope.arrVehicleModelData = [];
        if (typeof $scope.tmpVehicleForm.manufacturer_tran_id !== 'undefined' && $scope.tmpVehicleForm.manufacturer_tran_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'vehicle_model', manufacturer_tran_id: $scope.tmpVehicleForm.manufacturer_tran_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    $scope.arrVehicleModelData = response.data;
                })
                .error(function() {
                    $scope.tmpVehicleForm.vehicle_model_master_id = '';
                })
        } else {
            $scope.tmpVehicleForm.vehicle_model_master_id = '';
        }
    }

    $scope.checkListDetail = function() {
        $scope.arrChecklistData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'checklist' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrChecklistData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.getUnitList = function() {
        $scope.arrUnitData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'unit' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrUnitData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.getPartGroupList = function() {
        $scope.arrPartGroupData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'part_group' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrPartGroupData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.getPartList = function(getRow) {
        getRow.arrPartData = [];
        getRow.part_master_id = "";
        getRow.base_price = "";
        getRow.tax = "";
        getRow.total = "";
        if (typeof getRow.part_group_master_id !== 'undefined') {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'part', id: getRow.part_group_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    getRow.arrPartData = response.data;
                    $scope.calculatePartTotal();
                })
                .error(function(response) {

                });
        }
    }

    $scope.getPartCalculation = function(getRow) {
        var isDuplicate = false;
        getRow.base_price = "";
        getRow.tax = "";
        getRow.total = "";
        getRow.isDuplicate = 0;
        $scope.isDuplicateParts = 0;
        var sorted, i;
        sorted = $scope.partRow.concat().sort(function(a, b) {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
        });
        if ($scope.partRow.length > 0) {
            for (i = 0; i < $scope.partRow.length; i++) {
                sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].part_master_id == sorted[i].part_master_id) || (sorted[i + 1] && sorted[i + 1].part_master_id == sorted[i].part_master_id));
                if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                    $scope.isDuplicateParts = 1;
                    isDuplicate = true;
                }
            }
        }
        if (isDuplicate === false) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'part_calculation', id: getRow.part_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    getRow.arrPartCalculationData = response.data[0];
                    $scope.partBasicCalculation(getRow);
                })
                .error(function(response) {

                });
        }
    }

    $scope.partBasicCalculation = function(getRow) {
        if (typeof getRow.arrPartCalculationData !== 'undefined' && Object.keys(getRow.arrPartCalculationData).length > 0) {
            var quantity = Number(getRow.quantity);
            var basePrice = Number(getRow.arrPartCalculationData.price);
            var price = basePrice * quantity;
            var totalTax = Number(getRow.arrPartCalculationData.tax) * quantity;
            var totalPrice = price + totalTax;
            getRow.base_price = basePrice;
            getRow.tax = totalTax;
            getRow.total = totalPrice;
        }
        $scope.calculatePartTotal();
    }

    $scope.updateUnit = function() {
        $scope.bindScopeForm.unit_master_id = [];
        if (typeof $scope.partRow !== 'undefined' && Object.keys($scope.partRow).length > 0) {
            angular.forEach($scope.partRow, function(parts, index) {
                $scope.bindScopeForm.unit_master_id.push(parts.unit_master_id);
            });
        }
    }

    $scope.calculatePartTotal = function() {
        $scope.bindScopeForm.part_group_master_id = [];
        $scope.bindScopeForm.part_master_id = [];
        $scope.bindScopeForm.unit_master_id = [];
        $scope.bindScopeForm.quantity = [];
        $scope.bindScopeForm.base_price = [];
        $scope.bindScopeForm.tax = [];
        $scope.bindScopeForm.base_tax = [];
        $scope.bindScopeForm.total_price = [];
        if (typeof $scope.partRow !== 'undefined' && Object.keys($scope.partRow).length > 0) {
            var grandTotalQuantity = 0,
                grandTotalBasePrice = 0,
                grandTotalPrice = 0,
                grandTotalTax = 0;
            angular.forEach($scope.partRow, function(parts, index) {
                $scope.bindScopeForm.part_group_master_id.push(parts.part_group_master_id);
                $scope.bindScopeForm.part_master_id.push(parts.part_master_id);
                $scope.bindScopeForm.unit_master_id.push(parts.unit_master_id);
                if (typeof parts.arrPartCalculationData !== 'undefined' && Object.keys(parts.arrPartCalculationData).length > 0) {
                    var quantity = Number(parts.quantity);
                    var basePrice = AppUtilities.numberFormat(parts.arrPartCalculationData.price, 2);
                    var price = AppUtilities.numberFormat(basePrice * quantity, 2);
                    var totalTax = AppUtilities.numberFormat(Number(parts.arrPartCalculationData.tax) * quantity, 2);
                    var totalPrice = AppUtilities.numberFormat(price + totalTax, 2);
                    $scope.bindScopeForm.quantity.push(quantity);
                    $scope.bindScopeForm.base_price.push(basePrice);
                    $scope.bindScopeForm.tax.push(totalTax);
                    $scope.bindScopeForm.total_price.push(totalPrice);
                    $scope.bindScopeForm.base_tax.push(AppUtilities.numberFormat(parts.arrPartCalculationData.tax, 2));
                    grandTotalQuantity += quantity;
                    grandTotalBasePrice += basePrice;
                    grandTotalTax += totalTax;
                    grandTotalPrice += totalPrice;
                }
            });
            $scope.partObj.totalQuantity = grandTotalQuantity;
            $scope.partObj.totalBasePrice = AppUtilities.numberFormat(grandTotalBasePrice, 2);
            $scope.partObj.totalTax = AppUtilities.numberFormat(grandTotalTax, 2);
            $scope.partObj.totalPrice = AppUtilities.numberFormat(grandTotalPrice, 2);
        } else {
            $scope.partObj.totalQuantity = 0;
            $scope.partObj.totalBasePrice = 0;
            $scope.partObj.totalTax = 0;
            $scope.partObj.totalPrice = 0;
        }
        $scope.bindScopeForm.part_total_price = $scope.partObj.totalPrice;
        $scope.bindScopeForm.part_total_tax = $scope.partObj.totalTax;
    }

    $scope.getServiceGroupList = function() {
        $scope.arrServiceGroupData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'service_group' };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrServiceGroupData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.getServiceList = function(serviceRow) {
        serviceRow.arrPartData = [];
        serviceRow.service_master_id = "";
        serviceRow.base_price = "";
        serviceRow.total = "";
        if (typeof serviceRow.service_group_master_id !== 'undefined') {
            var path = $rootScope.ADMIN_API_URL + '/system/get_response';
            var params = { name: 'service', id: serviceRow.service_group_master_id };
            AppService.postHttpRequest(path, params)
                .success(function(response) {
                    serviceRow.arrServiceData = response.data;
                })
                .error(function(response) {
                    return false;
                });
        }
    }

    $scope.serviceCalculation = function(serviceRow) {
        var isDuplicate = false;
        serviceRow.base_price = 0;
        serviceRow.total = 0;
        $scope.isDuplicateService = 0;
        var sorted, i;
        sorted = $scope.serviceRow.concat().sort(function(a, b) {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
        });
        for (i = 0; i < $scope.serviceRow.length; i++) {
            sorted[i].isDuplicate = ((sorted[i - 1] && sorted[i - 1].service_master_id == sorted[i].service_master_id) || (sorted[i + 1] && sorted[i + 1].service_master_id == sorted[i].service_master_id));
            if (typeof sorted[i].isDuplicate !== 'undefined' && sorted[i].isDuplicate == true) {
                $scope.isDuplicateService = 1;
                isDuplicate = true;
            }
        }

        if (isDuplicate === false) {
            if (typeof serviceRow.arrServiceData !== 'undefined' && Object.keys(serviceRow.arrServiceData).length > 0) {
                var serviceRecord = serviceRow.arrServiceData.find(function(element) {
                    return (element.id == serviceRow.service_master_id);
                });
                if (typeof serviceRecord !== 'undefined') {
                    var quantity = 1;
                    var basePrice = AppUtilities.numberFormat(serviceRecord.price, 2);
                    var price = AppUtilities.numberFormat(basePrice * quantity, 2);
                    var totalPrice = price;
                    serviceRow.base_price = basePrice;
                    serviceRow.total = totalPrice;
                } else {
                    serviceRow.base_price = 0;
                    serviceRow.total = 0;
                }
            }
            $scope.serviceCartCalculation();
        }
    }

    $scope.serviceCartCalculation = function() {
        $scope.bindScopeForm.service_group_master_id = [];
        $scope.bindScopeForm.service_master_id = [];
        $scope.bindScopeForm.service_quantity = [];
        $scope.bindScopeForm.service_base_price = [];
        $scope.bindScopeForm.service_price = [];
        var totalQuantity = 0,
            basePrice = 0,
            totalPrice = 0;
        if (typeof $scope.serviceRow !== 'undefined' && Object.keys($scope.serviceRow).length > 0) {
            angular.forEach($scope.serviceRow, function(service, index) {
                if (typeof service.arrServiceData !== 'undefined' && Object.keys(service.arrServiceData).length > 0) {
                    var serviceRecord = service.arrServiceData.find(function(element) {
                        return (element.id == service.service_master_id);
                    });
                    $scope.bindScopeForm.service_group_master_id.push(service.service_group_master_id);
                    $scope.bindScopeForm.service_master_id.push(service.service_master_id);
                    if (typeof serviceRecord !== 'undefined') {
                        var quantity = 1;
                        var price = AppUtilities.numberFormat(serviceRecord.price, 2);
                        var ServiceTotalPrice = AppUtilities.numberFormat(price * quantity, 2);
                        totalQuantity += AppUtilities.numberFormat(quantity);
                        basePrice += price;
                        totalPrice += ServiceTotalPrice;
                        $scope.bindScopeForm.service_quantity.push(quantity);
                        $scope.bindScopeForm.service_base_price.push(price);
                        $scope.bindScopeForm.service_price.push(ServiceTotalPrice);
                    }
                }
            });
        }
        $scope.serviceObj.totalBasePrice = basePrice;
        $scope.serviceObj.totalPrice = totalPrice;
        $scope.serviceObj.totalQuantity = totalQuantity;
        $scope.bindScopeForm.service_total_price = totalPrice;
        $scope.bindScopeForm.service_total_tax = 0;
    }

    $scope.save = function() {
        if ($scope.formSubmission.$valid && $scope.memberFirmId > 0 && $scope.companyYearId > 0) {
            $('#record_save_btn').attr('disabled', true);
            AppUtilities.blockUI(1);
            var savePath = $scope.statePath + '/save';
            //var saveForms = AppUtilities.safeConvertObject('formSubmission');
            var saveForms = Object.assign({}, $scope.bindScopeForm);
            if ($scope.id !== null && $scope.id > 0) {
                saveForms.id = $scope.id;
            } else {
                if (typeof $scope.getVehicleDetail !== 'undefined' && typeof $scope.getVehicleDetail.id !== 'undefined' && $scope.getVehicleDetail.id > 0) {
                    saveForms.vehicle_master_id = $scope.getVehicleDetail.id;
                    saveForms.vehicle_number = $scope.getVehicleDetail.vehicle_number;
                } else {
                    saveForms = Object.assign({}, saveForms, $scope.getVehicleDetail);
                }
            }
            saveForms.user_member_master_id = $scope.memberFirmId;
            saveForms.arr_checklist = [];
            var checklist = document.getElementsByName('arr_checklist[]');
            if (checklist.length > 0) {
                for (var i = 0; i < checklist.length; i++) {
                    if (checklist[i].checked == true) {
                        saveForms.arr_checklist.push(checklist[i].value);
                    }
                }
            }
            saveForms.company_year_master_id = $scope.companyYearId;
            AppService.postHttpRequest(savePath, saveForms)
                .success(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $scope.getVehicleDetail = {};
                    $state.go($scope.baseStateTo);
                })
                .error(function(response) {
                    $('#record_save_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        }
    }

    $scope.edit = function(getId) {
        if (getId !== null && getId > 0) {
            $scope.bindScopeForm = {};
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/edit/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.checkListDetail();
                    $scope.getUnitList();
                    $scope.getPartGroupList();
                    $scope.getServiceGroupList();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.partRow = response.parts;
                    $scope.calculatePartTotal();
                    $scope.serviceRow = response.service;
                    $scope.bindScopeForm.checkList = response.checklist;
                    console.log($scope.bindScopeForm.checkList);
                    $scope.serviceCartCalculation();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.view = function(getId) {
        if (getId !== null && getId > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/view/' + getId + '/' + $scope.memberFirmId)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.bindScopeForm = angular.copy(response.data);
                    $scope.bindScopeForm.parts = response.parts;
                    $scope.bindScopeForm.services = response.service;
                    $scope.bindScopeForm.checkList = response.checklist;
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.change_status_form = function(getId, type) {
        if (typeof getId !== 'undefined' && getId > 0) {
            $scope.id = getId;
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/view_status/' + getId + '/' + $scope.memberFirmId + '/' + type)
                .success(function(response) {
                    $scope.bindScopeForm = response.data;
                    $scope.bindScopeForm.type = type;
                    $scope.bindScopeForm.checklist = response.checklist;
                    $scope.bindScopeForm.checklist = response.checklist;
                    var url;
                    if (type == 1) {
                        $scope.employeeList();
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/change_status.html';
                    } else if (type == 2) {
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/complete.html';
                    } else {
                        $scope.getUniqueCode(2);
                        url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/job/generate_invoice.html';
                    }
                    var popSize = (type == 1) ? 'lg' : 'lg';
                    $scope.modalInstance = $modal.open({
                        templateUrl: url,
                        size: popSize,
                        scope: $scope
                    });
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.save_status = function() {
        if ($scope.bindScopeForm.id > 0) {
            AppUtilities.blockUI(1);
            $('#update_btn').attr('disabled', true);
            $('#update_btn').text('UPDATING....');
            var saveForms;
            $scope.bindScopeForm.status = ($scope.bindScopeForm.type == 1) ? 2 : 3;
            $scope.bindScopeForm.firm_id = $scope.memberFirmId;
            if ($scope.bindScopeForm.type == 2) {
                saveForms = AppUtilities.safeConvertObject('jobStatusForm');
                saveForms = Object.assign({}, saveForms, $scope.bindScopeForm);
            } else {
                saveForms = $scope.bindScopeForm;
            }
            var path = $scope.statePath + '/save_status';
            AppService.postHttpRequest(path, saveForms)
                .success(function(response) {
                    $('#update_btn').text('UPDATE');
                    $('#update_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    $scope.modalInstance.close();
                    AppUtilities.handleResponse(response);
                    $scope.bindScopeForm = {};
                    $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                })
                .error(function(response) {
                    $('#update_btn').text('UPDATE');
                    $('#update_btn').attr('disabled', false);
                    AppUtilities.unblockUI();
                    return false;
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.generate_invoide = function() {
        if ($scope.bindScopeForm.id > 0) {
            AppUtilities.blockUI(1);
            var saveForms = angular.copy($scope.bindScopeForm);
            saveForms.firm_id = $scope.memberFirmId;;
            console.log('save forms:', saveForms);
            var path = $scope.statePath + '/generate_invoice';
            AppService.postHttpRequest(path, saveForms)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.modalInstance.close();
                    AppUtilities.handleResponse(response);
                    $state.go('dashboard.job-print-invoice', { id: $scope.bindScopeForm.id }, { reload: true });
                    $scope.bindScopeForm = {};
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    return false;
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.print_invoice = function() {
        if (typeof $scope.id !== 'undefined' && $scope.id > 0) {
            AppUtilities.blockUI(1);
            AppService.getHttpRequest($scope.statePath + '/print_invoice/' + $scope.id + '/' + $scope.memberFirmId)
                .success(function(response) {
                    $scope.bindScopeForm = response.data;
                    $scope.firmLogo = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR + '/' + GLOBAL_DIR.LOGODIR + '/' + response.data.header.logo;
                    console.log($scope.bindScopeForm);
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
        } else {
            var response = { 'status': 0, 'message': 'invalid response' };
            AppUtilities.handleResponse(response);
            $state.go($scope.baseStateTo);
        }
    }

    $scope.open_job = function() {
        $rootScope.jobStatusId = 1;
        $rootScope.baseStateTo = 'dashboard.job';
    }

    $scope.job_inprogress = function() {
        $rootScope.jobStatusId = 2;
        $rootScope.baseStateTo = 'dashboard.job_inprogress';
    }

    $scope.job_complete = function() {
        $rootScope.jobStatusId = 3;
        $rootScope.baseStateTo = 'dashboard.job_complete';
    }

    $scope.job_view_list = function() {
        $rootScope.jobStatusId = 4;
        $rootScope.baseStateTo = 'dashboard.job_view';
    }

    $scope.employeeList = function() {
        $scope.arrEmployeeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_response';
        var params = { name: 'employee_list', 'firm_id': $scope.memberFirmId };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                $scope.arrEmployeeData = response.data;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.delete = function() {
        AppUtilities.confirmDeletion()
            .success(function(data) {
                AppUtilities.blockUI(1);
                var deleteObj = { 'id': data };
                var deletePath = $scope.statePath + '/delete/' + $scope.memberFirmId;
                AppService.postHttpRequest(deletePath, deleteObj)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $state.go($scope.baseStateTo, {}, { reload: $scope.baseStateTo });
                        AppUtilities.handleResponse(response);
                    })
                    .error(function(response) {
                        AppUtilities.unblockUI();
                        AppUtilities.handleResponse(response);
                    });
            }).error(function(response) {
                console.log(response);
            });
    }

    $scope.viewJobList = function() {
        $scope.arrJobList = [];
        $scope.isSearchJobRecords = 0;
        var path = $scope.statePath + '/view_job_list/' + $scope.memberFirmId;
        var params = { 
                        job_code: (typeof $scope.bindScopeForm.job_code !== 'undefined' && $scope.bindScopeForm.job_code !== null) ? $scope.bindScopeForm.job_code : '',
                        vehicle_code: (typeof $scope.bindScopeForm.vehicle_code !== 'undefined' && $scope.bindScopeForm.vehicle_code !== null) ? $scope.bindScopeForm.vehicle_code : '',
                        vehicle_no: (typeof $scope.bindScopeForm.vehicle_no !== 'undefined' && $scope.bindScopeForm.vehicle_no !== null) ? $scope.bindScopeForm.vehicle_no : '',
                        mobile_no: (typeof $scope.bindScopeForm.mobile_no !== 'undefined' && $scope.bindScopeForm.mobile_no !== null) ? $scope.bindScopeForm.mobile_no : ''
                    };
        AppService.postHttpRequest(path, params)
            .success(function(response) {
                console.log(response);
                $scope.arrJobList = angular.copy(response.data);
                $scope.isSearchJobRecords = 1;
            })
            .error(function(response) {
                return false;
            });
    }

    $scope.calculateAmt = function(job,partAmt = 0,serviceAmt = 0) {
        job.totalAmount = Number(partAmt) + Number(serviceAmt);
    }

    $scope.export = function(exportType) {
        if (exportType !== null && exportType != '') {
            AppUtilities.blockUI(1);
            var exportPath = $scope.statePath + '/export/' + $scope.memberFirmId + '/' + $scope.companyYearId + '/' + $rootScope.jobStatusId;
            AppService.getHttpRequest(exportPath)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.export(response.header, response.data, $scope.exportFile + '_' + response.date, exportType);
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                });
        } else {
            var response = { status: 0, message: 'Invalid Export Type' };
            AppUtilities.handleResponse(response);
        }
    }

    $scope.partAddRow = function() {
        $scope.partRow.push({});
    }

    $scope.partDeleteRow = function(i) {
        $scope.partRow.splice(i, 1);
        $scope.calculatePartTotal();
    }

    $scope.serviceAddRow = function() {
        $scope.serviceRow.push({});
    }

    $scope.serviceDeleteRow = function(i) {
        $scope.serviceRow.splice(i, 1);
        $scope.serviceCartCalculation();
    }
}]);