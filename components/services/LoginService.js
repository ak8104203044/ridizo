'use strict';
radizoApp.service('LoginService', function($q, $http, appContant) {
    var loginService = {};
    loginService.validateLogin = function(formData) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        $http({
            url: appContant.API_PATH.login,
            cache: true,
            method: 'POST',
            data: JSON.stringify(formData),
            dataType: 'json',
            timeout: 10000
        }).success(function(data, status, headers, config) {
            console.log(data);
            if (data.status === 1) {
                deferred.resolve(data);
            } else {
                if (typeof data.message !== 'undefined') {
                    deferred.reject(data.message);
                } else {
                    deferred.reject('Internal server error, please try again later !.');
                }
            }
        }).error(function(data, status, headers, config) {
            console.log('config', config);
            var message = 'Internal server error, please try again later';
            deferred.reject(message);
        });
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };
    return loginService;
});