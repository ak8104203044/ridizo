'use strict';
radizoApp.service('DashboardService', function($q, $http, appContant) {
    var dashboardService = {};
    dashboardService.sidebarMenu = function(path) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        $http.get(path)
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject('sidebar menu is not intialized');
            });
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };

    dashboardService.postRequest = function(path, formData) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        $http({
            url: path,
            cache: true,
            method: 'POST',
            data: formData,
            dataType: 'json',
            timeout: 10000,
            headers: {
                'radizo-sessid': localStorage.getItem('userSessId'),
                'radizo-token-id': localStorage.getItem('userTokenId')
            }
        }).success(function(data, status, headers, config) {
            console.log(data);
            if (data.status === 1) {
                deferred.resolve(data);
            } else {
                var params = (typeof data.status !== 'undefined') ? data : { message: 'Internal server error, please try again later !.', status: 0 };
                deferred.reject(params);
            }
        }).error(function(data, status, headers, config) {
            var message = (typeof data.message !== 'undefined') ? data.message : 'Internal server error, please try again later !.';
            deferred.reject({ 'status': 0, 'message': message });
        });
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };
    return dashboardService;
});