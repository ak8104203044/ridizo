'use strict';
radizoApp.service('AppService', function($q, $http,ERROR_MESSAGE) {
    return {
        getHttpRequest: function(path) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            $http.get(path)
                .success(function(data) {
                    console.log('success :', data.status);
                    if (data.status === 1) {
                        deferred.resolve(data);
                    } else {
                        var params = (typeof data.status !== 'undefined') ? data : { message: ERROR_MESSAGE.SERVER, status: 0 };
                        deferred.reject(params);
                    }
                })
                .error(function(data) {
                    deferred.reject({ 'status': 0, 'message': 'invalid response' });
                });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        },
        postHttpRequest: function(path, formData) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            if (typeof formData !== 'string') {
                formData = JSON.stringify(formData);
            }
            $http({
                url: path,
                cache: false,
                method: 'POST',
                data: formData,
                dataType: 'json',
                timeout: 30000
            }).success(function(data, status, headers, config) {
                console.log('success :', data.status);
                if (data.status === 1) {
                    deferred.resolve(data);
                } else {
                    var params = (typeof data.status !== 'undefined') ? data : { message: ERROR_MESSAGE.SERVER, status: 0 };
                    deferred.reject(params);
                }
            }).error(function(data, status, headers, config) {
                var message = ERROR_MESSAGE.SERVER;
                deferred.reject({ 'status': 0, 'message': message });
            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        },
        postHttpFormRequest: function(path, formData) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            $http({
                url: path,
                cache: false,
                method: 'POST',
                data: formData,
                transformRequest: angular.identity,
                timeout: 30000,
                headers: {
                    'Content-Type': undefined,
                    'Process-Data': false
                }
            }).success(function(data, status, headers, config) {
                console.log('success :', data.status);
                if (data.status === 1) {
                    deferred.resolve(data);
                } else {
                    var params = (typeof data.status !== 'undefined') ? data : { message: ERROR_MESSAGE.SERVER, status: 0 };
                    deferred.reject(params);
                }
            }).error(function(data, status, headers, config) {
                var message = ERROR_MESSAGE.SERVER;
                deferred.reject({ 'status': 0, 'message': message });
            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    };
});