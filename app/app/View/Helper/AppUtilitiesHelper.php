<?php
App::uses('AppHelper', 'View/Helper');

class AppUtilitiesHelper extends AppHelper {
    public $helpers = array('Html','Form');

 	public function __construct(View $view, $settings = array()) {
        parent::__construct($view, $settings);
    }

    /**
     * This function is used to create button according to user role rights
     * @action => edit,add,delete,view etc.
     * @caption => button caption
     * @controller => controller name
     * @method => method name
     * @id => auto generate primary key
     * @title => title tooltip name
     */
    public function roleActionType($action,$caption,$id = null,$title = null) {
        $action = strtolower($action);
        switch($action) {
            case 'add':
                        $method = (!empty($method)) ? $method : 'add';
                        $title = (!empty($title)) ? $title : 'Add Record';
                        return $this->Html->link(" <i class='fa fa-plus'></i>&nbsp;".__($caption,true),'javascript:void(0);',array('class'=>'btn btn-xs default green tooltips','escape' => false,'data-container'=>'body','data-placement'=>'top','data-original-title' => __($title, true),'onclick' => "CommonComponent.add();"));
                        break;
            case 'edit':
                        $method = (!empty($method)) ? $method : 'edit';
                        $title = (!empty($title)) ? $title : 'Edit Record';
                        return $this->Html->link("<i class='fa fa-edit'></i>&nbsp;".__($caption,true), 'javascript:void(0);',array('class'=>'btn btn-xs default blue tooltips','escape' => false,'data-container'=>'body','data-placement'=>'top','data-original-title' => __($title, true),'onclick' => "CommonComponent.action('edit','".$id."');"));
                        break;
            case 'view':
                        $method = (!empty($method)) ? $method : 'view';
                        $title = (!empty($title)) ? $title : 'view Record';
                        return $this->Html->link("<i class='fa fa-eye'></i>&nbsp;".__($caption,true),'javascript:void(0);',array('class'=>'btn btn-xs default tooltips','escape' => false,'data-container'=>'body','data-placement'=>'top','data-original-title' => __($title, true),'onclick' => "CommonComponent.action('view','".$id."');"));
                        break;
            default:
                        return true;
                        break;
        }
    }

    /**
     * This function is used to change date format
     */
    public function date_format($getDate,$format = 'yyyy-mm-dd') {
        if(isset($getDate)) {
            $convertDateFormat = null;
            if(strpos($getDate,'-') !== false) {
                $date = str_replace('/','-',$getDate);
            } else {
                $date = str_replace('/','/',$getDate);
            }
            try {
                $dateObj = new DateTime($date);
            } catch (Exception $e) {
                echo $e->getMessage();
                exit(1);
            }
            if(isset($date) && !empty($date)) {
                switch($format) {
                case 'yyyy-mm-dd': $convertDateFormat = $dateObj->format('Y-m-d');
                                    break;
                case 'mm-dd-yyyy': $convertDateFormat = $dateObj->format('m-d-Y');
                                    break;  
                case 'dd-mm-yyyy': $convertDateFormat = $dateObj->format('d-m-Y');
                                    break;
                case 'dd/mm/yyyy': $convertDateFormat = $dateObj->format('d/m/Y');
                                    break;
                default : $convertDateFormat = $dateObj->format('Y-m-d');
                        break;
            }
          }
          return $convertDateFormat;
        }
    }
}
?>