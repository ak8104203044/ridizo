<?php
App::uses('Model', 'Model');
class VehicleMaster extends AppModel{
    public $name='VehicleMaster';
    public $validate=array(
                            'vehicle_number'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter vehicle number'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, vehicle number already exists'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select member first'
                                ),
                                'firm_validate'=>array(
                                    'rule'=>array('validateMember'),
                                    'message'=>'Invalid firmID,please select valid firmID'
                                )
                            ),
                            'purchase_price'=>array(
                                'purchase_price_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please purchase price'
                                )
                            ),
                            'purchase_date'=>array(
                                'purchase_date_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please purchase date'
                                )
                            ),
                            'registration_date'=>array(
                                'registration_date_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please registration date'
                                )
                            ),
                            'manufacturer_tran_id'=>array(
                                'manufacturer_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select manufacturer'
                                )
                            ),
                            'vehicle_model_master_id'=>array(
                                'vehicle_model_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle model'
                                )
                            ),
                            'fule_type'=>array(
                                'fule_type_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select fuel type'
                                )
                            ),
                            'vehicle_start_method'=>array(
                                'vehicle_start_method_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle start method'
                                )
                            ),
                            'purchase_from'=>array(
                                'purchase_from_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter purchase from'
                                )
                            ),
                            'purchase_address'=>array(
                                'purchase_address_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter purchase address'
                                )
                            ),
                            /*'owner_title_master_id'=>array(
                                'owner_title_master_id_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select title'
                                )
                            ),*/
                            'owner_first_name'=>array(
                                'owner_first_name_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter owner first name'
                                )
                            ),
                            'owner_mobile_no'=>array(
                                'owner_mobile_no_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please owner mobile no.'
                                ),
                                'mobile_no_numeric'=>array(
                                    'rule'=>'numeric',
                                    'message'=>'Mobile no. should be numeric only'
                                ),
                                'mobileno_minlength'=>array(
                                    'rule'=>array('minLength','10'),
                                    'message'=>'Please enter 10 digits mobile no.',
                                )
                            ),
                            'owner_email_id'=>array(
                                'email_validate'=>array(
                                    'rule'=>array('email'),
                                    'message'=>'Please enter valid email id',
                                    'allowEmpty'=>true
                                )
                            ),
                            'country_master_id'=>array(
                                'country_master_id_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select country'
                                )
                            ),
                            'state_master_id'=>array(
                                'state_master_id_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select state'
                                )
                            ),
                            'city_master_id'=>array(
                                'city_master_id_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select city'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'is_active' => 1,'vehicle_status' => 1,'vehicle_number' => trim($this->data[$this->alias]['vehicle_number']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function validateMember() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        return ($count > 0);
    }

    public function validateSoldVehicle($vehicleNumber = '') {
        $arrVehicleRecords = array();
        if(!empty($vehicleNumber)) {
            $conditions = array('status' => 1,'is_active' => 1,'vehicle_status' => 2,'vehicle_number' => trim($vehicleNumber));
            $options = array('fields' => array('id'),'conditions' => $conditions);
            $arrVehicleRecords = $this->find('first',$options);
        }
        return $arrVehicleRecords;
    }
}
?>