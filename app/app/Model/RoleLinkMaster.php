<?php
App::uses('Model', 'Model');
class RoleLinkMaster extends AppModel{
    public $name='RoleLinkMaster';
    public $actsAs = array(
                            'Tree' => array(
                                'scope' => array('RoleLinkMaster.status' => 1,'RoleLinkMaster.is_show' => 1)
                            )
                    );
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'state'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter state'
                                )
                            ),
                            'template_url'=>array(
                                'template_validation'=>array(
                                    'rule'=>array('customRoleValidation','template_url'),
                                    'message'=>'Please enter template url'
                                )
                            ),
                            'view'=>array(
                                'view_validation'=>array(
                                    'rule'=>array('customRoleValidation','view'),
                                    'message'=>'Please enter view'
                                )
                            ),
                            'controller'=>array(
                                'controller_validation'=>array(
                                    'rule'=>array('customRoleValidation','controller'),
                                    'message'=>'Please enter controller'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }

        if(isset($this->data[$this->alias]['parent_id']) && !empty($this->data[$this->alias]['parent_id'])){
			$conditions["parent_id"] =$this->data[$this->alias]['parent_id'];
        }
        
        $options = array(
                        'fields' => array('id'),
                        'conditions' => $conditions
                    );
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no']);
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
				$conditions["id <>"] =$this->data[$this->alias]['id'];
            }

            if(isset($this->data[$this->alias]['parent_id']) && !empty($this->data[$this->alias]['parent_id'])){
                $conditions["parent_id"] = $this->data[$this->alias]['parent_id'];
            } else {
                $conditions["parent_id"] = '';
            }

			$options = array(
                            'fields' => array('id'),
                            'conditions' => $conditions
                        );
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else{
			return true;
		}
    }

    public function customRoleValidation($request,$fields) {
        if(isset($this->data[$this->alias]['is_link']) && !empty($this->data[$this->alias]['is_link'])) {
            if(empty($request[$fields])) {
                return false;
            }
            return true;
        } else {
            return true;
        }
    }
}
?>