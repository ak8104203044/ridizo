<?php
App::uses('Model', 'Model');
class UserSetting extends AppModel {
	public  $name ='UserSetting';
    public  $validate = array(
                            'branch_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select valid Member ID'
                                ),
                                'user_check' => array(
                                    'rule'=>array('validateMember'),
                                    'message'=>'member ID not exists,Please select valid member ID !.'
                                )
                            ),
                            'employee_code'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter valid employee code'
                                )
                            ),
                            'employee_code_digit'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter employee code total digit'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','employee_code_digit'),
                                    'message'=>'Please enter valid employee code total digit'
                                )
                            ),
                            'employee_code_start_from'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter employee code start from'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','employee_code_start_from'),
                                    'message'=>'Please enter valid employee code start from'
                                )
                            ),
                            'invoice_code'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice code'
                                ),
                            ),
                            'invoice_code_digit'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice code total digit'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','invoice_code_digit'),
                                    'message'=>'Please enter valid invoice code total digit'
                                )
                            ),
                            'invoice_code_start_from'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice code start from'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','invoice_code_start_from'),
                                    'message'=>'Please enter valid invoice code start from'
                                )
                            ),
                            'invoice_code_session'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select invoice financial year'
                                )
                            ),
                            'jobcard_code'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter jobcard code'
                                )
                            ),
                            'jobcard_code_digit'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter jobcard code total digit'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','jobcard_code_digit'),
                                    'message'=>'Please enter valid jobcard code total digit'
                                )
                            ),
                            'jobcard_code_start_from'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter jobcard code start from'
                                ),
                                'numeric_value'=>array(
                                    'rule'=>array('validateNumric','jobcard_code_start_from'),
                                    'message'=>'Please enter valid jobcard code start from'
                                )
                            ),
                            'jobcard_code_session'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select job card financial year'
                                )
                            )
                        );
    public function validateNumric($request,$fields) {
        if(isset($request[$fields]) && !empty($request[$fields])) {
            return true;
        } else {
            return false;
        }
    }

    public function validateMember() {
        $options = array('fields' => array('id'),'conditions' => array('status' => 1,'id' =>$this->data[$this->alias]['branch_master_id']));
        $arrMemberData = ClassRegistry::init('BranchMaster')->find('first',$options);
        if(!empty($arrMemberData)) {
            return true;
        } else {
            return false;
        }
    }
}
?>