<?php
App::uses('Model', 'Model');
class VehicleSaleMaster extends AppModel{
    public $name='VehicleSaleMaster';
    public $recursive = -1;
    public $validate =  array(
                            'invoice_date'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice date'
                                )
                            ),
                            'title_master_id'=>array(
                                'title_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select title'
                                )
                            ),
                            'first_name'=>array(
                                'firstname_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter first name'
                                )
                            ),
                            'mobile_no'=>array(
                                'mobile_no_notblank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no'
                                )
                            ),
                            'expense_json'=>array(
                                'expense_json_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select atleast one expense head'
                                )
                            ),
                            'payment_mode'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select payment mode'
                                )
                            ),
                            'vehicle_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle'
                                ),
                                'validate_vehicle'=>array(
                                    'rule'=>array('validateVehicle'),
                                    'message'=>'Please select valid vehicle'
                                ),
                                'duplicate_vehicle' => array(
                                    'rule' => array('validateDuplicateVehicle'),
                                    'message' => 'This vehicle is sold out'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct Branch'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select company year'
                                ),
                                'valid_company'=>array(
                                    'rule'=>array('validateCompanyYear'),
                                    'message'=>'Please select valid company year'
                                )
                            )
                    );

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYear() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function validateVehicle() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['vehicle_master_id']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('VehicleMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateDuplicateVehicle() {
        $conditions = array('status' => 1,'vehicle_master_id' => trim($this->data[$this->alias]['vehicle_master_id']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return $count == 0;
    }
}
?>