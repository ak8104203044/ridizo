<?php
App::uses('Model', 'Model');
class CustomerMaster extends AppModel{
    public $name='CustomerMaster';
    public $recursive = -1;
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                )
                            ),
                            'mobile_no'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('validateMobile'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, mobile number already exists'
                                )
                            )
                    );
    
    public function validateMobile() {
        $conditions = array('status' => 1,'mobile_no' => trim($this->data[$this->alias]['mobile_no']),'branch_master_id' => $this->data[$this->alias]['branch_master_id']);
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }
}
?>