<?php
App::uses('Model', 'Model');
class UserRoleTran extends AppModel {
	public  $name ='UserRoleTran';
    public  $validate = array(
                            'role_type'=>array(
                                'role_type_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select user type'
                                ),
                            ),
                            'user_member_type_id'=>array(
                                'user_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select user type'
                                )
                            ),
                            'role_master_id'=>array(
                                'role_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select role'
                                )
                            )
                        );
}
?>