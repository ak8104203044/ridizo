<?php
App::uses('Model', 'Model');
class User extends AppModel{
	public $name='User';
	public $validate=array(
                        'username'=>array(
                            'username_must_to_be_blank'=>array(
                                'rule'=>array('notBlank'),
                                'required'=>true,
                                'message'=>'Please Enter User Name'
                            )
                        ),
                        'password'=>array(
                            'password_must_to_be_blank'=>array(
                                'rule'=>array('notBlank'),
                                'required'=>true,
                                'message'=>'Please Enter Password'
                            )	
                        )
                    );
}
?>