<?php
App::uses('Model', 'Model');
class VehicleCouponMaster extends AppModel{
    public $name='VehicleCouponMaster';
    public $arrServiceDetail = array();
    public $validate =  array(
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid Branch(s)'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'coupon_master_id'=>array(
                                'notblank_coupon'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid coupon'
                                ),
                                'validate_coupon' => array(
                                    'rule' => 'validateCoupon',
                                    'message' => 'Coupon is invalid, please select valid coupon !.'
                                )
                            ),
                            'vehicle_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle'
                                )
                            ),
                            'customer_master_id'=>array(
                                'customer_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select customer'
                                )
                            ),
                            'date'=>array(
                                'invoice_date_notblank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice date'
                                )
                            ),
                            'amount'=>array(
                                'value_validate'=>array(
                                    'rule'=>array('validateAmount'),
                                    'message'=>'Please enter valid amount'
                                )
                            )
                    );
    
    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCoupon() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['coupon_master_id']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id','service'),'conditions' => $conditions);
        $arrCouponData = ClassRegistry::init('CouponMaster')->find('first',$options);
        if(count($arrCouponData) > 0) {
            $this->arrServiceDetail = (!empty($arrCouponData['CouponMaster']['service'])) ? json_decode($arrCouponData['CouponMaster']['service'],true) : array();
            return true;
        } else {
            return false;
        }
    }

    public function validateAmount() {
        if(isset($this->data[$this->alias]['amount'])) {
            if($this->data[$this->alias]['amount'] <= 0) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}
?>