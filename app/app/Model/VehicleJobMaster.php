<?php
App::uses('Model', 'Model');
class VehicleJobMaster extends AppModel{
    public $name = 'VehicleJobMaster';
    public $validate =  array(
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select current company year'
                                ),
                                'validate_company_year' => array(
                                    'rule' => array('validateCompanyYearId'),
                                    'message' => 'Please select valid company year ID'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select member first'
                                ),
                                'firm_validate'=>array(
                                    'rule'=>array('validateMember'),
                                    'message'=>'Invalid firmID,please select valid firmID'
                                )
                            ),
                            'vehicle_master_id'=>array(
                                'vehicle_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle'
                                ),
                                'vehicle_validate'=>array(
                                    'rule'=>array('validateVehicle'),
                                    'message'=>'Please select valid vehicle'
                                )
                            ),
                            'product_quantity'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateQty'),
                                    'message'=>'Please enter valid quantity'
                                ),
                                'validate_qty' => array(
                                    'rule' => 'validateStockQty',
                                    'message' => 'product quantity is less than or equal to stock quantity'
                                )
                            ),
                            'product_price'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateAmount'),
                                    'message'=>'Please enter valid product price'
                                )
                            )
                            
                    );
    
    public function validateMember() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        return ($count > 0);
    }

    public function validateCompanyYearId() {
        $conditions = array('status' => 1,'is_current' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        return ($count > 0);
    }

    public function validateVehicle() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['vehicle_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('VehicleMaster')->find('count',$options);
        return ($count > 0);
    }

    public function validateQty() {
        if(isset($this->data[$this->alias]['product_quantity']) && count($this->data[$this->alias]['product_quantity']) > 0) {
            foreach($this->data[$this->alias]['product_quantity'] as $key => $quantity) {
                if($quantity <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validateStockQty() {
        if(isset($this->data[$this->alias]['product_master_id']) && count($this->data[$this->alias]['product_master_id']) > 0) {
            $arrProductDetail = array();
            foreach($this->data[$this->alias]['product_master_id'] as $key => $productId) {
                if(isset($this->data[$this->alias]['product_quantity'][$key])) {
                    $quantity = intval($this->data[$this->alias]['product_quantity'][$key]);
                    if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])) {
                        $query = "SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_stock_trans WHERE `status` = 1 AND product_master_id = '".$productId."'
                                UNION ALL
                                SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_consumption_trans WHERE `status` = 1 AND product_master_id = '".$productId."' AND vehicle_job_master_id !='".$this->data[$this->alias]['id']."'";
                    } else {
                        $query = "SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_stock_trans WHERE `status` = 1 AND product_master_id = '".$productId."'
                                UNION ALL
                                SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_consumption_trans WHERE `status` = 1 AND product_master_id = '".$productId."'";
                    }
                    $result = ClassRegistry::init('VehicleJobMaster')->query($query,false);
                    if(!empty($result)) {
                        $available = $result[0][0]['quantity'];
                        $consumption = $result[1][0]['quantity'];
                        $stock = ($available > $consumption) ? ($available - $consumption) : 0;
                        if($quantity > $stock) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function validateAmount() {
        if(isset($this->data[$this->alias]['product_price']) && count($this->data[$this->alias]['product_price']) > 0) {
            foreach($this->data[$this->alias]['product_price'] as $key => $price) {
                if($price <= 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
?>