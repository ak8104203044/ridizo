<?php
App::uses('AppModel','Model');
class ErrorLog extends AppModel{
    public $name = 'ErrorLog';

    public function saveErrorLog($logs) {
        try{
            $arrErrorLog['ErrorLog']['user_id'] = $logs['user_id'];
            $arrErrorLog['ErrorLog']['controller'] = $logs['controller'];
            $arrErrorLog['ErrorLog']['method'] = $logs['method'];
            $arrErrorLog['ErrorLog']['description'] = $logs['description'];
            if(isset($logs['request']) && !empty($logs['request'])) {
                $arrErrorLog['ErrorLog']['request'] = json_encode($logs['request']);
            }
            $this->create();
            $this->save($arrErrorLog);
        } catch(Exception $e) {
            echo $e;exit;
        }
    }
}
?>
