<?php
App::uses('Model', 'Model');
class PurchaseMaster extends AppModel{
    public $name='PurchaseMaster';
    public $recursive = -1;
    public $validate=array(
                            'voucher_no'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter po order no'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('validateVoucherNo'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, po number already exists'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select company year'
                                ),
                                'valid_company'=>array(
                                    'rule'=>array('validateCompanyYear'),
                                    'message'=>'Please select valid company year'
                                )
                            ),
                            'supplier_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selct supplier'
                                )
                            ),
                            'voucher_date'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter purchase date'
                                )
                            ),
                            'quantity'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateQty'),
                                    'message'=>'Please enter valid quantity'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct Branch'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            )
                    );
    
    public function validateVoucherNo() {
        $conditions = array('status' => 1,'voucher_no' => trim($this->data[$this->alias]['voucher_no']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function validateQty() {
        if(isset($this->data[$this->alias]['quantity']) && count($this->data[$this->alias]['quantity']) > 0) {
            foreach($this->data[$this->alias]['quantity'] as $key => $quantity) {
                if($quantity <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYear() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
?>