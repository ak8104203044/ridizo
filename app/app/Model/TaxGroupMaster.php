<?php
App::uses('Model', 'Model');
class TaxGroupMaster extends AppModel{
    public $name='TaxGroupMaster';
    public $virtualFields = array('taxlist' => 'GROUP_CONCAT(TaxGroupTran.tax_master_id)','tax_name' => 'GROUP_CONCAT(TaxMaster.name)');
    public $validate=array(
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid Branch(s)'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'tax_master_id'=>array(
                                'tax_not_blank'=>array(
                                    'rule'=>array('taxValidate'),
                                    'message'=>'Please select atleast one tax.'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no'],'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
				$conditions["id <>"] =$this->data[$this->alias]['id'];
			}
			$options = array('fields' => array('id'),'conditions' => $conditions);
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else{
			return true;
		}
    }

    public function taxValidate() {
        if(!isset($this->data[$this->alias]['tax_master_id']) || empty($this->data[$this->alias]['tax_master_id'])) {
            return false;
        }
        return true;
    }
}
?>