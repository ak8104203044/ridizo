<?php
App::uses('Model', 'Model');
class SupplierMaster extends AppModel{
	public $name = 'SupplierMaster';
    public $validate =  array(
                            'firm_name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                )
                            ),
                            'first_name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                )
                            ),
                            'mobile_no' => array(
                                'mobile_no_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('validateMobile'),
                                    'required'=>'create',
                                    'message'=>'Mobile no already exists'
                                )
                            ),
                            'email_id' => array(
                                'email_unique'=>array(
                                    'rule'=>array('validateEmail'),
                                    'required'=>'create',
                                    'message'=>'EmailID already exists'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid Branch(s)'
                                )
                            )
                        );
    
    public function validateMobile() {
        $conditions = array('status' => 1,'mobile_no' => trim($this->data[$this->alias]['mobile_no']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] = $this->data[$this->alias]['id'];
        }
        $conditions['branch_master_id'] = $this->data[$this->alias]['branch_master_id'];
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function validateEmail() {
        if(!empty($this->data[$this->alias]['email_id'])) {
            $conditions = array('status' => 1,'email_id' => trim($this->data[$this->alias]['email_id']));
            if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
                $conditions["id <>"] = $this->data[$this->alias]['id'];
            }
            $conditions['branch_master_id'] = $this->data[$this->alias]['branch_master_id'];
            $options = array('fields' => array('id'),'conditions' => $conditions);
            $count = $this->find('count',$options);
            return ($count == 0);
        }
        return true;
    }
}
?>