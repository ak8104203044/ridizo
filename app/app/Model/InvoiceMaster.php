<?php
App::uses('Model', 'Model');
class InvoiceMaster extends AppModel{
    public $name='InvoiceMaster';
    public $recursive = -1;
    public $validate=array(
                            'invoice_date'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter invoice date'
                                )
                            ),
                            'payment_mode'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select payment mode'
                                )
                            ),
                            'purchase_price'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validatePurchasePrice'),
                                    'message'=>'Please enter valid purchase price'
                                )
                            ),
                            'sale_price'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateSalePrice'),
                                    'message'=>'Please enter valid sale price'
                                )
                            ),
                            'unit_price'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateSalePrice'),
                                    'message'=>'Please enter valid unit price'
                                )
                            ),
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct Branch'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select company year'
                                ),
                                'valid_company'=>array(
                                    'rule'=>array('validateCompanyYear'),
                                    'message'=>'Please select valid company year'
                                )
                            )
                    );

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYear() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validatePurchasePrice() {
        if(isset($this->data[$this->alias]['purchase_price']) && count($this->data[$this->alias]['purchase_price']) > 0) {
            foreach($this->data[$this->alias]['purchase_price'] as $key => $quantity) {
                if($quantity <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validateSalePrice() {
        if(isset($this->data[$this->alias]['sale_price']) && count($this->data[$this->alias]['sale_price']) > 0) {
            foreach($this->data[$this->alias]['sale_price'] as $key => $quantity) {
                if($quantity <= 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
?>