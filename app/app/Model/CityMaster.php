<?php
App::uses('Model', 'Model');
class CityMaster extends AppModel{
    public $name='CityMaster';
    public $virtualFields = array('state' => 'StateMaster.name');
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'country_master_id'=>array(
                                'country_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selct country'
                                )
                            ),
                            'state_master_id'=>array(
                                'state_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selct state'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']),'state_master_id' => $this->data[$this->alias]['state_master_id']);
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }

        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no'],'state_master_id' => $this->data[$this->alias]['state_master_id']);
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
				$conditions["id <>"] =$this->data[$this->alias]['id'];
			}
			$options = array('fields' => array('id'),'conditions' => $conditions);
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else{
			return true;
		}
    }
}
?>