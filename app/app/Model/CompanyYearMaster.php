<?php
App::uses('Model', 'Model');
class CompanyYearMaster extends AppModel{
	public $name='CompanyYearMaster';
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'start_date'=>array(
                                'start_date_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter start date'
                                ),
                                'start_date_unique'=>array(
                                    'rule'=>array('getDateValidation','start_date'),
                                    'required'=>'create',
                                    'message'=>'Session start date is greater than previous session date range.'
                                )
                            ),
                            'end_date'=>array(
                                'end_date_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter end date'
                                ),
                                'end_date_unique'=>array(
                                    'rule'=>array('getDateValidation','end_date'),
                                    'required'=>'create',
                                    'message'=>'Session end date is greater than previous session date range.'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }

        $options = array(
                        'fields' => array('id'),
                        'conditions' => $conditions
                    );
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function getDateValidation($request,$fields) {
        $date = date('Y-m-d',strtotime($request[$fields]));
		$conditions = array('status' => 1,'AND' => array("? BETWEEN start_date AND end_date" => array($date)));
		if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])) {
			$conditions["id <>"] =$this->data[$this->alias]['id'];
		}
		$options = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
		$count = $this->find('count',$options);
		return ($count == 0);
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no']);
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
				$conditions["id <>"] =$this->data[$this->alias]['id'];
			}
			$options = array(
                            'fields' => array('id'),
                            'conditions' => $conditions
                        );
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else{
			return true;
		}
    }
}
?>