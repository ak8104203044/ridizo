<?php
App::uses('Model', 'Model');
class GeneralSetting extends AppModel {
	public  $name ='GeneralSetting';
    public  $validate = array(
                            'firm_name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter firm name'
                                )
                            ),
                            'pan_number'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter pan number'
                                )
                            ),
                            'contact_person'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter contact person'
                                )
                            ),
                            'mobile_no'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no.'
                                )
                            ),
                            'email_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter email id'
                                )
                            ),
                            'country_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select country'
                                )
                            ),
                            'state_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select state'
                                )
                            ),
                            'city_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select city'
                                )
                            ),
                            'franchise_unique_string'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter franchise unique string'
                                )
                            ),
                            'franchise_total_digit'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter franchise total digit'
                                )
                            ),
                            'franchise_digit_start_from'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter franchise digit start from'
                                )
                            ),
                            'vehicle_unique_string'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter vehicle unique string'
                                )
                            ),
                            'vehicle_total_digit'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter vehicle total digit'
                                )
                            ),
                            'vehicle_digit_start_from'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter vehicle digit start from'
                                )
                            )
                        );
    
}
?>