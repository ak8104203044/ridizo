<?php
App::uses('Model', 'Model');
class UserMemberMaster extends AppModel {
	public  $name ='UserMemberMaster';
    public  $validate = array(
                            'firm_name'=>array(
                                'firm_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter firm name'
                                ),
                                'firm_must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, firm name already exists'
                                )
                            ),
                            'first_name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter first name'
                                )
                            ),
                            'title_master_id'=>array(
                                'title_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select title'
                                )
                            ),
                            'gender'=>array(
                                'gender_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select gender'
                                )
                            ),
                            'mobile_no'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no.'
                                ),
                                'mobile_no_numeric'=>array(
                                    'rule'=>'numeric',
                                    'message'=>'Mobile no. should be numeric only'
                                ),
                                'mobileno_minlength'=>array(
                                    'rule'=>array('minLength','10'),
                                    'message'=>'Please enter 10 digits mobile no.',
                                ),
                                'mobile_unique' => array(
                                    'rule' => 'mobileValidate',
                                    'message' => 'Mobile No. already exists,Please add different mobile no !.'
                                )
                            ),
                            'email_id'=>array(
                                'email_validate'=>array(
                                    'rule'=>array('email'),
                                    'message'=>'Please enter valid email id',
                                    'allowEmpty'=>true
                                ),
                                'emailid_unique' => array(
                                    'rule' => 'emailValidate',
                                    'message' => 'EmailID already exists,Please add different emailID !.'
                                )
                            ),
                            'country_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select country'
                                )
                            ),
                            'state_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select state'
                                )
                            ),
                            'city_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select city'
                                )
                            ),
                            'member_type_tran_id'=>array(
                                'member_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select member type'
                                )
                            ),
                            'pincode'=>array(
                                'pincode_numeric'=>array(
                                    'rule'=>'numeric',
                                    'message'=>'Please enter pincode numeric only',
                                    'allowEmpty'=>true
                                ),
                                'pincode_minlength'=>array(
                                    'rule'=>array('minLength','6'),
                                    'message'=>'Please enter 6 digits pincode ',
                                    'allowEmpty'=>true
                                )
                            )
                        );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'firm_name' => trim($this->data[$this->alias]['firm_name']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] = $this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function mobileValidate() {
        $conditions = array('status' => 1,'mobile_no' => trim($this->data[$this->alias]['mobile_no']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] = $this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function emailValidate() {
        if(!empty($this->data[$this->alias]['email_id'])) {
            $conditions = array('status' => 1,'email_id' => trim($this->data[$this->alias]['email_id']));
            if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
                $conditions["id <>"] = $this->data[$this->alias]['id'];
            }
            $options = array('fields' => array('id'),'conditions' => $conditions);
            $count = $this->find('count',$options);
            return ($count == 0);
        }
        return true;
    }
}
?>