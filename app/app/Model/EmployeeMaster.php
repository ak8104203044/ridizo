<?php
App::uses('Model', 'Model');
class EmployeeMaster extends AppModel {
	public  $name ='EmployeeMaster';
    public  $validate = array(
                            'first_name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter first name'
                                )
                            ),
                            'title_master_id'=>array(
                                'title_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select title'
                                )
                            ),
                            'branch_master_id' => array(
                                'firm_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select valid firmID'
                                ),
                                'firm_validate'=>array(
                                    'rule'=>array('validateMember'),
                                    'message'=>'Invalid firmID,please select valid firmID'
                                )
                            ),
                            'gender'=>array(
                                'gender_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select gender'
                                )
                            ),
                            'mobile_no'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no.'
                                ),
                                'mobile_no_numeric'=>array(
                                    'rule'=>'numeric',
                                    'message'=>'Mobile no. should be numeric only'
                                ),
                                'mobileno_minlength'=>array(
                                    'rule'=>array('minLength','10'),
                                    'message'=>'Please enter 10 digits mobile no.',
                                ),
                                'mobile_unique' => array(
                                    'rule' => 'mobileValidate',
                                    'message' => 'Mobile No. already exists,Please add different mobile no !.'
                                )
                            ),
                            'email_id'=>array(
                                'email_validate'=>array(
                                    'rule'=>array('email'),
                                    'message'=>'Please enter valid email id',
                                    'allowEmpty'=>true
                                ),
                                'emailid_unique' => array(
                                    'rule' => 'emailValidate',
                                    'message' => 'EmailID already exists,Please add different emailID !.'
                                )
                            ),
                            'country_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select country'
                                )
                            ),
                            'state_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select state'
                                )
                            ),
                            'city_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select city'
                                )
                            ),
                            'department_master_id'=>array(
                                'department_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select department'
                                )
                            ),
                            'designation_master_id'=>array(
                                'designation_not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select designation'
                                )
                            ),
                            'pincode'=>array(
                                'pincode_numeric'=>array(
                                    'rule'=>'numeric',
                                    'message'=>'Please enter pincode numeric only',
                                    'allowEmpty'=>true
                                ),
                                'pincode_minlength'=>array(
                                    'rule'=>array('minLength','6'),
                                    'message'=>'Please enter 6 digits pincode ',
                                    'allowEmpty'=>true
                                )
                            ),
                        );

    public function validateMember() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        return ($count > 0);
    }

    public function mobileValidate() {
        $conditions = array('status' => 1,'mobile_no' => trim($this->data[$this->alias]['mobile_no']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] = $this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function emailValidate() {
        if(!empty($this->data[$this->alias]['email_id'])) {
            $conditions = array('status' => 1,'email_id' => trim($this->data[$this->alias]['email_id']));
            if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
                $conditions["id <>"] = $this->data[$this->alias]['id'];
            }
            $options = array('fields' => array('id'),'conditions' => $conditions);
            $count = $this->find('count',$options);
            return ($count == 0);
        }
        return true;
    }

}
?>