<?php
App::uses('Model', 'Model');
class VehicleRentMaster extends AppModel{
    public $name = 'VehicleRentMaster';
    public $validate = array(
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid Branch(s)'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select current company year'
                                ),
                                'validate_company_year' => array(
                                    'rule' => array('validateCompanyYearId'),
                                    'message' => 'Please select valid company year ID'
                                )
                            ),
                            'vehicle_master_id'=>array(
                                'vehicle_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle'
                                ),
                                'vehicle_validate'=>array(
                                    'rule'=>array('validateVehicle'),
                                    'message'=>'Please select valid vehicle'
                                ),
                                'duplicate_vehicle' => array(
                                    'rule' => 'duplicateVehicle',
                                    'message' => 'Duplicate entry, vehicle already exists'
                                )
                            ),
                            'first_name'=>array(
                                'first_name_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter first name'
                                )
                            ),
                            'mobile_no'=>array(
                                'mobile_no_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no'
                                )
                            ),
                            'c_address'=>array(
                                'mobile_no_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter current address'
                                )
                            )
                    );

    public function validateVehicle() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['vehicle_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('VehicleMaster')->find('count',$options);
        return ($count > 0);
    }
    
    public function duplicateVehicle() {
        $conditions = array('status' => 1,'rent_status' => 1,'vehicle_master_id' => trim($this->data[$this->alias]['vehicle_master_id']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']),'company_year_master_id' => trim($this->data[$this->alias]['company_year_master_id']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYearId() {
        $conditions = array('status' => 1,'is_current' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        return ($count > 0);
    }
}
?>