<?php
App::uses('Model', 'Model');
class SaleMaster extends AppModel{
    public $name='SaleMaster';
    public $recursive = -1;
    public $validate=array(
                            'customer_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select customer'
                                )
                            ),
                            'invoice_date'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter date'
                                )
                            ),
                            'branch_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select branch'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select company year'
                                ),
                                'valid_company'=>array(
                                    'rule'=>array('validateCompanyYear'),
                                    'message'=>'Please select valid company year'
                                )
                            ),
                            'quantity'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateQty'),
                                    'message'=>'Please enter valid quantity'
                                ),
                                'validate_qty' => array(
                                    'rule' => 'validateStockQty',
                                    'message' => 'product quantity is less than or equal to stock quantity'
                                )
                            ),
                            'price'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('validateAmount'),
                                    'message'=>'Please enter valid price'
                                )
                            )
                    );

    public function validateQty() {
        if(isset($this->data[$this->alias]['quantity']) && count($this->data[$this->alias]['quantity']) > 0) {
            foreach($this->data[$this->alias]['quantity'] as $key => $quantity) {
                if($quantity <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validateStockQty() {
        if(isset($this->data[$this->alias]['product_master_id']) && count($this->data[$this->alias]['product_master_id']) > 0) {
            $arrProductDetail = array();
            foreach($this->data[$this->alias]['product_master_id'] as $key => $productId) {
                if(isset($this->data[$this->alias]['quantity'][$key])) {
                    $quantity = intval($this->data[$this->alias]['quantity'][$key]);
                    if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])) {
                        $query = "SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_stock_trans WHERE `status` = 1 AND product_master_id = '".$productId."'
                                    UNION ALL
                                    SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_consumption_trans WHERE `status` = 1 AND product_master_id = '".$productId."' AND sale_master_id !='".$this->data[$this->alias]['id']."'";
                    } else {
                        $query = "SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_stock_trans WHERE `status` = 1 AND product_master_id = '".$productId."'
                                UNION ALL
                                SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_consumption_trans WHERE `status` = 1 AND product_master_id = '".$productId."'";
                    }
                    $result = ClassRegistry::init('SaleMaster')->query($query,false);
                    if(!empty($result)) {
                        $available = $result[0][0]['quantity'];
                        $consumption = $result[1][0]['quantity'];
                        $stock = ($available > $consumption) ? ($available - $consumption) : 0;
                        if($quantity > $stock) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function validateAmount() {
        if(isset($this->data[$this->alias]['price']) && count($this->data[$this->alias]['price']) > 0) {
            foreach($this->data[$this->alias]['price'] as $key => $price) {
                if($price <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYear() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
?>