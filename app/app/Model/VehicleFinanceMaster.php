<?php
App::uses('Model', 'Model');
class VehicleFinanceMaster extends AppModel{
    public $name = 'VehicleFinanceMaster';
    public $validate = array(
                            'branch_master_id'=>array(
                                'branch_validate'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please selecct valid Branch(s)'
                                ),
                                'valid_branch' => array(
                                    'rule' => 'validateBranch',
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'company_year_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select current company year'
                                ),
                                'validate_company_year' => array(
                                    'rule' => array('validateCompanyYearId'),
                                    'message' => 'Please select valid company year ID'
                                )
                            ),
                            'vehicle_master_id'=>array(
                                'vehicle_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please select vehicle'
                                ),
                                'vehicle_validate'=>array(
                                    'rule'=>array('validateVehicle'),
                                    'message'=>'Please select valid vehicle'
                                )
                            ),
                            'date_of_loan'=>array(
                                'dateofloan_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter date of loan'
                                )
                            ),
                            'loan_tenure'=>array(
                                'loan_tenure_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter loan tenure'
                                )
                            ),
                            'loan_amount'=>array(
                                'loanamount_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter loan amount'
                                )
                            ),
                            'emi_amount'=>array(
                                'emi_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter emi amount'
                                )
                            ),
                            'down_payment'=>array(
                                'down_payment_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter down payment'
                                )
                            ),
                            'first_name'=>array(
                                'first_name_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter first name'
                                )
                            ),
                            'mobile_no'=>array(
                                'mobile_no_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter mobile no'
                                )
                            ),
                            'c_address'=>array(
                                'mobile_no_required'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter current address'
                                )
                            )
                    );

    public function validateVehicle() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['vehicle_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('VehicleMaster')->find('count',$options);
        return ($count > 0);
    }

    public function validateBranch() {
        $conditions = array('status' => 1,'id' => trim($this->data[$this->alias]['branch_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('BranchMaster')->find('count',$options);
        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateCompanyYearId() {
        $conditions = array('status' => 1,'is_current' => 1,'id' => trim($this->data[$this->alias]['company_year_master_id']));
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = ClassRegistry::init('CompanyYearMaster')->find('count',$options);
        return ($count > 0);
    }
}
?>