<?php
App::uses('Model', 'Model');
class ManufacturerMaster extends AppModel{
    public $name='ManufacturerMaster';
    public $virtualFields = array('vehicle_type_id' => 'GROUP_CONCAT(ManufacturerTran.vehicle_type_master_id)','vehicle_type_name' => 'GROUP_CONCAT(VehicleTypeMaster.name)','vehicle_type' => 'VehicleTypeMaster.name','vehicle_type_master_id' => 'VehicleTypeMaster.id');
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'vehicle_type_master_id'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('vehicleTypeValidate'),
                                    'message'=>'Please select vehicle type'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }

        $options = array(
                        'fields' => array('id'),
                        'conditions' => $conditions
                    );
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function vehicleTypeValidate() {
        if(isset($this->data[$this->alias]['vehicle_type_master_id']) && count($this->data[$this->alias]['vehicle_type_master_id']) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no']);
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
				$conditions["id <>"] =$this->data[$this->alias]['id'];
			}
			$options = array(
                            'fields' => array('id'),
                            'conditions' => $conditions
                        );
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else{
			return true;
		}
    }
}
?>