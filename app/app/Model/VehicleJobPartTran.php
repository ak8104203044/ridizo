<?php
App::uses('Model', 'Model');
class VehicleJobPartTran extends AppModel{
    public $name = 'VehicleJobPartTran';
    public $recursive = -1;
    public $arrInsertIds;

    public function afterSave($created,$options = array()) {
        $productId = $this->data[$this->alias]['product_master_id'];
        if($this->getInsertID() > 0) {
            $this->arrInsertIds[$productId] = $this->getInsertID();
        } else {
            $this->arrInsertIds[$productId] = $this->data[$this->alias]['id'];
        }
    }
}
?>