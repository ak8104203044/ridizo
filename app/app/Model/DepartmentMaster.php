<?php
App::uses('Model', 'Model');
class DepartmentMaster extends AppModel{
	public $name='DepartmentMaster';
    public $validate=array(
                            'name'=>array(
                                'not_blank'=>array(
                                    'rule'=>array('notBlank'),
                                    'message'=>'Please enter name'
                                ),
                                'must_be_unique'=>array(
                                    'rule'=>array('nameValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, name already exists'
                                )
                            ),
                            'branch_master_id' => array(
                                'not_blank' => array(
                                    'rule' =>array('notBlank'),
                                    'message' => 'Please select valid branch'
                                )
                            ),
                            'order_no'=>array(
                                'order_number_unique'=>array(
                                    'rule'=>array('orderNoValidate'),
                                    'required'=>'create',
                                    'message'=>'Duplicate entry, order no. already exists'
                                )
                            )
                    );
    
    public function nameValidate() {
        $conditions = array('status' => 1,'name' => trim($this->data[$this->alias]['name']),'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
        if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])){
            $conditions["id <>"] =$this->data[$this->alias]['id'];
        }
        $options = array('fields' => array('id'),'conditions' => $conditions);
        $count = $this->find('count',$options);
        return ($count == 0);
    }

    public function orderNoValidate() {
        if(isset($this->data[$this->alias]['order_no']) && !empty($this->data[$this->alias]['order_no'])) {
            $conditions = array('status' => 1,'order_no' => $this->data[$this->alias]['order_no'],'branch_master_id' => trim($this->data[$this->alias]['branch_master_id']));
			if(isset($this->data[$this->alias]['id']) && !empty($this->data[$this->alias]['id'])) {
				$conditions["id <>"] =$this->data[$this->alias]['id'];
			}
			$options = array('fields' => array('id'),'conditions' => $conditions);
			$orderCount = $this->find('count',$options);
			return ($orderCount == 0);
		} else {
			return true;
		}
    }
}
?>