/*Table structure for table `bank_masters` */
SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE IF NOT EXISTS `bank_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `bank_key` (`name`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `branch_masters` */

CREATE TABLE IF NOT EXISTS `branch_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firm_name` varchar(120) DEFAULT NULL,
  `title_master_id` int(11) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `gst_number` varchar(60) DEFAULT NULL,
  `parcard_no` varchar(20) DEFAULT NULL,
  `mobile_no` varchar(14) DEFAULT NULL,
  `alternate_mobile_no` varchar(14) DEFAULT NULL,
  `email_id` varchar(120) DEFAULT NULL,
  `alternate_email_id` varchar(120) DEFAULT NULL,
  `logo` varchar(36) DEFAULT NULL,
  `correspondance_address` text,
  `permanent_address` text,
  `city_master_id` int(11) DEFAULT NULL,
  `state_master_id` int(11) DEFAULT NULL,
  `country_master_id` int(11) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `type_tran_id` int(11) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `branch_ref_key1` (`city_master_id`),
  KEY `branch_ref_key2` (`state_master_id`),
  KEY `branch_ref_key3` (`country_master_id`),
  KEY `branch_ref_key4` (`title_master_id`),
  KEY `branch_ref_key5` (`type_tran_id`),
  KEY `branch_keys` (`firm_name`),
  CONSTRAINT `branch_ref_key1` FOREIGN KEY (`city_master_id`) REFERENCES `city_masters` (`id`),
  CONSTRAINT `branch_ref_key2` FOREIGN KEY (`state_master_id`) REFERENCES `state_masters` (`id`),
  CONSTRAINT `branch_ref_key3` FOREIGN KEY (`country_master_id`) REFERENCES `country_masters` (`id`),
  CONSTRAINT `branch_ref_key4` FOREIGN KEY (`title_master_id`) REFERENCES `title_masters` (`id`),
  CONSTRAINT `branch_ref_key5` FOREIGN KEY (`type_tran_id`) REFERENCES `type_trans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `city_masters` */

CREATE TABLE IF NOT EXISTS `city_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `state_master_id` int(11) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `city_name_key` (`name`),
  KEY `city_state_key` (`state_master_id`),
  CONSTRAINT `city_ref_key1` FOREIGN KEY (`state_master_id`) REFERENCES `state_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `company_year_masters` */

CREATE TABLE IF NOT EXISTS `company_year_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `caption` varchar(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `is_current` tinyint(1) DEFAULT '0' COMMENT '1=>process,0=>not process',
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `cpyear_key1` (`name`),
  KEY `cpyear_key2` (`caption`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `country_masters` */

CREATE TABLE IF NOT EXISTS `country_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `country_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `department_masters` */

CREATE TABLE IF NOT EXISTS `department_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `department_key` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `designation_masters` */

CREATE TABLE IF NOT EXISTS `designation_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `department_master_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `designation_key1` (`name`),
  KEY `designation_key2` (`department_master_id`),
  CONSTRAINT `designation_ref_key` FOREIGN KEY (`department_master_id`) REFERENCES `department_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `document_masters` */

CREATE TABLE IF NOT EXISTS `document_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `document_key1` (`name`),
  KEY `document_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `employee_masters` */

CREATE TABLE IF NOT EXISTS `employee_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `unique_no` varchar(30) DEFAULT NULL,
  `max_unique_no` int(10) DEFAULT NULL,
  `title_master_id` int(11) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `gender` tinyint(2) DEFAULT '1' COMMENT '1 => Male,2 => Female,3 => Transgender',
  `mobile_no` varchar(14) DEFAULT NULL,
  `alternate_mobile_no` varchar(14) DEFAULT NULL,
  `email_id` varchar(200) DEFAULT NULL,
  `alternate_email_id` varchar(200) DEFAULT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `aadhar_number` varchar(12) DEFAULT NULL,
  `pan_number` varchar(12) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `country_master_id` int(11) DEFAULT NULL,
  `state_master_id` int(11) DEFAULT NULL,
  `city_master_id` int(11) DEFAULT NULL,
  `user_member_master_id` int(11) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `c_address` text,
  `p_address` text,
  `photo` varchar(36) DEFAULT NULL,
  `main_user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `employee_key` (`unique_no`,`first_name`),
  KEY `employee_ref_key1` (`title_master_id`),
  KEY `employee_ref_key2` (`country_master_id`),
  KEY `employee_ref_key3` (`state_master_id`),
  KEY `employee_ref_key4` (`city_master_id`),
  KEY `employee_ref_key6` (`user_member_master_id`),
  KEY `employee_ref_key5` (`user_id`),
  KEY `employee_ref_key7` (`main_user_id`),
  CONSTRAINT `employee_ref_key1` FOREIGN KEY (`title_master_id`) REFERENCES `title_masters` (`id`),
  CONSTRAINT `employee_ref_key2` FOREIGN KEY (`country_master_id`) REFERENCES `country_masters` (`id`),
  CONSTRAINT `employee_ref_key3` FOREIGN KEY (`state_master_id`) REFERENCES `state_masters` (`id`),
  CONSTRAINT `employee_ref_key4` FOREIGN KEY (`city_master_id`) REFERENCES `city_masters` (`id`),
  CONSTRAINT `employee_ref_key5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `employee_ref_key6` FOREIGN KEY (`user_member_master_id`) REFERENCES `user_member_masters` (`id`),
  CONSTRAINT `employee_ref_key7` FOREIGN KEY (`main_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `employee_trans` */

CREATE TABLE IF NOT EXISTS `employee_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_master_id` int(11) DEFAULT NULL,
  `designation_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `employee_tra_ref_key1` (`employee_master_id`),
  KEY `employee_tra_ref_key2` (`designation_master_id`),
  CONSTRAINT `employee_tra_ref_key1` FOREIGN KEY (`employee_master_id`) REFERENCES `employee_masters` (`id`),
  CONSTRAINT `employee_tra_ref_key2` FOREIGN KEY (`designation_master_id`) REFERENCES `designation_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `error_logs` */

CREATE TABLE IF NOT EXISTS `error_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `method` varchar(40) DEFAULT NULL,
  `request` text,
  `description` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `error_log_ref_key` (`user_id`),
  CONSTRAINT `error_log_ref_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `general_settings` */

CREATE TABLE IF NOT EXISTS `general_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(160) DEFAULT NULL,
  `field_value` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `general_setting_key` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `inspection_masters` */

CREATE TABLE IF NOT EXISTS `inspection_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `code` varchar(80) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>in-active',
  PRIMARY KEY (`id`),
  KEY `inspection_key` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `manufacturer_masters` */

CREATE TABLE IF NOT EXISTS `manufacturer_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `manufacturer_key1` (`name`),
  KEY `manufacturer_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `manufacturer_trans` */

CREATE TABLE IF NOT EXISTS `manufacturer_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_type_master_id` int(11) DEFAULT NULL,
  `manufacturer_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `manu_tran_ref_key1` (`vehicle_type_master_id`),
  KEY `manu_tran_ref_key2` (`manufacturer_master_id`),
  CONSTRAINT `manu_tran_ref_key1` FOREIGN KEY (`vehicle_type_master_id`) REFERENCES `vehicle_type_masters` (`id`),
  CONSTRAINT `manu_tran_ref_key2` FOREIGN KEY (`manufacturer_master_id`) REFERENCES `manufacturer_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `part_group_masters` */

CREATE TABLE IF NOT EXISTS `part_group_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>activve,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `partgroup_key1` (`name`),
  KEY `partgroup_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `part_masters` */

CREATE TABLE IF NOT EXISTS `part_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(80) DEFAULT NULL,
  `hsn_code` varchar(80) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `is_taxable` tinyint(1) DEFAULT '0' COMMENT '1=>yes,0=>no',
  `part_group_master_id` int(11) DEFAULT NULL,
  `tax_group_master_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `part_ref_key1` (`part_group_master_id`),
  KEY `part_ref_key2` (`tax_group_master_id`),
  KEY `part_keys` (`name`,`code`),
  CONSTRAINT `part_ref_key1` FOREIGN KEY (`part_group_master_id`) REFERENCES `part_group_masters` (`id`),
  CONSTRAINT `part_ref_key2` FOREIGN KEY (`tax_group_master_id`) REFERENCES `tax_group_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `role_link_masters` */

CREATE TABLE IF NOT EXISTS `role_link_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `folder_name` varchar(100) DEFAULT NULL,
  `view` varchar(80) DEFAULT NULL,
  `controller` varchar(60) DEFAULT NULL,
  `service` varchar(60) DEFAULT NULL,
  `is_link` tinyint(2) DEFAULT '0' COMMENT '1=>link,0=>not link',
  `icon` varchar(20) DEFAULT NULL,
  `base_files` text,
  `action` varchar(28) DEFAULT NULL,
  `action_files` text,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(5) DEFAULT NULL,
  `rght` int(5) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '1=>show,0=>hidden',
  PRIMARY KEY (`id`),
  KEY `role_link_key` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Table structure for table `role_link_operation_rights_trans` */

CREATE TABLE IF NOT EXISTS `role_link_operation_rights_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_master_id` int(11) DEFAULT NULL,
  `role_link_master_id` int(11) DEFAULT NULL,
  `type_tran_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `role_link_op_ref_key1` (`role_master_id`),
  KEY `role_link_op_ref_key2` (`role_link_master_id`),
  KEY `role_link_op_ref_key3` (`type_tran_id`),
  CONSTRAINT `role_link_op_ref_key1` FOREIGN KEY (`role_master_id`) REFERENCES `role_masters` (`id`),
  CONSTRAINT `role_link_op_ref_key2` FOREIGN KEY (`role_link_master_id`) REFERENCES `role_link_masters` (`id`),
  CONSTRAINT `role_link_op_ref_key3` FOREIGN KEY (`type_tran_id`) REFERENCES `type_trans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `role_masters` */

CREATE TABLE IF NOT EXISTS `role_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `type_tran_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` int(5) DEFAULT NULL,
  `is_default` tinyint(2) DEFAULT '0',
  `is_show` tinyint(2) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `role_keys` (`name`,`code`,`type_tran_id`),
  KEY `role_ref_key` (`type_tran_id`),
  CONSTRAINT `role_ref_key` FOREIGN KEY (`type_tran_id`) REFERENCES `type_trans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `service_group_masters` */

CREATE TABLE IF NOT EXISTS `service_group_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `service_keys` (`name`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `service_masters` */

CREATE TABLE IF NOT EXISTS `service_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT '0',
  `service_group_master_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `service_key1` (`name`),
  KEY `service_key2` (`code`),
  KEY `service_ref_key` (`service_group_master_id`),
  CONSTRAINT `service_ref_key` FOREIGN KEY (`service_group_master_id`) REFERENCES `service_group_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `state_masters` */

CREATE TABLE IF NOT EXISTS `state_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `country_master_id` int(11) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `state_name` (`name`),
  KEY `state_country_key` (`country_master_id`),
  CONSTRAINT `state_ref_key` FOREIGN KEY (`country_master_id`) REFERENCES `country_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tax_group_masters` */

CREATE TABLE IF NOT EXISTS `tax_group_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `taxgroup_key1` (`name`),
  KEY `taxgroup_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tax_group_trans` */

CREATE TABLE IF NOT EXISTS `tax_group_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_master_id` int(11) DEFAULT NULL,
  `tax_group_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `tax_tra_ref_key1` (`tax_master_id`),
  KEY `tax_tra_ref_key2` (`tax_group_master_id`),
  CONSTRAINT `tax_tra_ref_key1` FOREIGN KEY (`tax_master_id`) REFERENCES `tax_masters` (`id`),
  CONSTRAINT `tax_tra_ref_key2` FOREIGN KEY (`tax_group_master_id`) REFERENCES `tax_group_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tax_masters` */

CREATE TABLE IF NOT EXISTS `tax_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '1' COMMENT '1=>Percentage,2=>Fixed',
  `value` decimal(10,2) DEFAULT '0.00',
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `tax_key1` (`name`),
  KEY `tax_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `title_masters` */

CREATE TABLE IF NOT EXISTS `title_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `shortname` varchar(60) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '1' COMMENT '1=>male,2=>female,3=>both',
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `title_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `type_masters` */

CREATE TABLE IF NOT EXISTS `type_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `shortname` varchar(60) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `type_keys` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `type_trans` */

CREATE TABLE IF NOT EXISTS `type_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `shortname` varchar(30) DEFAULT NULL,
  `type_master_id` int(11) DEFAULT NULL,
  `role_type` tinyint(2) DEFAULT '0' COMMENT '1=>Developer,2=>Super Admin,3=>Franchise,4=>Employee',
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `typetran_key` (`name`),
  KEY `typetran_key2` (`type_master_id`,`role_type`),
  CONSTRAINT `type_tran_ref_key` FOREIGN KEY (`type_master_id`) REFERENCES `type_masters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Table structure for table `unit_masters` */

CREATE TABLE IF NOT EXISTS `unit_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `unit_key1` (`name`),
  KEY `unit_key2` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_logged_trans` */

CREATE TABLE IF NOT EXISTS `user_logged_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `from_type` tinyint(1) DEFAULT '1' COMMENT '1=>web,2=>app',
  `sessid` varchar(60) DEFAULT NULL,
  `token_id` varchar(150) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,2=>inactive',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_logged_tran_ref_key` (`user_id`),
  KEY `user_logged_key1` (`sessid`),
  KEY `user_logged_key2` (`token_id`),
  CONSTRAINT `user_logged_tran_ref_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_member_masters` */
CREATE TABLE IF NOT EXISTS `user_member_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_no` varchar(40) DEFAULT NULL,
  `max_unique_no` int(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title_master_id` int(11) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `first_name` varchar(60) DEFAULT NULL,
  `middle_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `firm_name` varchar(160) DEFAULT NULL,
  `gender` tinyint(2) DEFAULT '1' COMMENT '1=>male,2=>female,3=>transgender',
  `gst_number` varchar(30) DEFAULT NULL,
  `pan_number` varchar(18) DEFAULT NULL,
  `mobile_no` varchar(14) DEFAULT NULL,
  `alternate_mobile_no` varchar(14) DEFAULT NULL,
  `email_id` varchar(140) DEFAULT NULL,
  `alternate_email_id` varchar(140) DEFAULT NULL,
  `country_master_id` int(11) DEFAULT NULL,
  `state_master_id` int(11) DEFAULT NULL,
  `city_master_id` int(11) DEFAULT NULL,
  `member_type_tran_id` int(11) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `c_address` text,
  `p_address` text,
  `logo` varchar(36) DEFAULT NULL,
  `photo` varchar(36) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `user_member_ref_key1` (`title_master_id`),
  KEY `user_member_ref_key2` (`country_master_id`),
  KEY `user_member_ref_key3` (`state_master_id`),
  KEY `user_member_ref_key4` (`city_master_id`),
  KEY `user_member_ref_key5` (`member_type_tran_id`),
  KEY `user_member_ref_key6` (`user_id`),
  KEY `name_key` (`first_name`,`middle_name`,`last_name`,`unique_no`,`max_unique_no`,`firm_name`),
  CONSTRAINT `user_member_ref_key1` FOREIGN KEY (`title_master_id`) REFERENCES `title_masters` (`id`),
  CONSTRAINT `user_member_ref_key2` FOREIGN KEY (`country_master_id`) REFERENCES `country_masters` (`id`),
  CONSTRAINT `user_member_ref_key3` FOREIGN KEY (`state_master_id`) REFERENCES `state_masters` (`id`),
  CONSTRAINT `user_member_ref_key4` FOREIGN KEY (`city_master_id`) REFERENCES `city_masters` (`id`),
  CONSTRAINT `user_member_ref_key5` FOREIGN KEY (`member_type_tran_id`) REFERENCES `type_trans` (`id`),
  CONSTRAINT `user_member_ref_key6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_role_trans` */

CREATE TABLE IF NOT EXISTS `user_role_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `user_roletran_ref_key1` (`role_master_id`),
  KEY `user_roletran_ref_key2` (`user_id`),
  CONSTRAINT `user_roletran_ref_key1` FOREIGN KEY (`role_master_id`) REFERENCES `role_masters` (`id`),
  CONSTRAINT `user_roletran_ref_key2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_settings` */

CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_code` varchar(100) DEFAULT NULL,
  `employee_code_digit` int(8) DEFAULT NULL,
  `employee_code_start_from` int(11) DEFAULT NULL,
  `invoice_code` varchar(100) DEFAULT NULL,
  `invoice_code_digit` int(8) DEFAULT NULL,
  `invoice_code_start_from` int(11) DEFAULT NULL,
  `jobcard_code` varchar(100) DEFAULT NULL,
  `jobcard_code_digit` int(8) DEFAULT NULL,
  `jobcard_code_start_from` int(11) DEFAULT NULL,
  `user_member_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `user_setting_key` (`user_member_master_id`),
  CONSTRAINT `user_setting_ref_key` FOREIGN KEY (`user_member_master_id`) REFERENCES `user_member_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_tokens` */

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `from_type` tinyint(1) DEFAULT '1' COMMENT '1=>web,2=>app',
  `token` varchar(150) DEFAULT NULL,
  `sessid` varchar(60) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `token_user_id` (`user_id`),
  KEY `auth_token` (`token`),
  CONSTRAINT `user_token_ref_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users` */
CREATE TABLE IF NOT EXISTS `users`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `encrypt` varchar(120) DEFAULT NULL,
  `fullname` varchar(60) DEFAULT NULL,
  `user_type` tinyint(2) DEFAULT '3' COMMENT '1=>Developer,2=>Super Admin,3=>Member,4=>Employee',
  `is_active` tinyint(2) DEFAULT '1' COMMENT '0 for inactive , 1 for active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=> No ,0 => Yes',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_key` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `variant_masters` */

CREATE TABLE IF NOT EXISTS `variant_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `color_code` varchar(30) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>removed',
  PRIMARY KEY (`id`),
  KEY `color_keys` (`name`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_expense_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_expense_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_master_id` int(11) DEFAULT NULL,
  `vehicle_expense_type_tran_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_exp_ref_key1` (`vehicle_master_id`),
  KEY `vehicle_exp_ref_key2` (`vehicle_expense_type_tran_id`),
  CONSTRAINT `vehicle_exp_ref_key1` FOREIGN KEY (`vehicle_master_id`) REFERENCES `vehicle_masters` (`id`),
  CONSTRAINT `vehicle_exp_ref_key2` FOREIGN KEY (`vehicle_expense_type_tran_id`) REFERENCES `type_trans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_image_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_image_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_master_id` int(11) DEFAULT NULL,
  `image` varchar(36) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_image_ref_key` (`vehicle_master_id`),
  CONSTRAINT `vehicle_image_ref_key` FOREIGN KEY (`vehicle_master_id`) REFERENCES `vehicle_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_job_checklist_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_job_checklist_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_job_master_id` int(11) DEFAULT NULL,
  `inspection_master_id` int(11) DEFAULT NULL,
  `review` tinyint(2) DEFAULT '1' COMMENT '1=>good,2=>average,3=>bad',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `job_checklist_ref_key1` (`vehicle_job_master_id`),
  KEY `job_checklist_ref_key2` (`inspection_master_id`),
  CONSTRAINT `job_checklist_ref_key1` FOREIGN KEY (`vehicle_job_master_id`) REFERENCES `vehicle_job_masters` (`id`),
  CONSTRAINT `job_checklist_ref_key2` FOREIGN KEY (`inspection_master_id`) REFERENCES `inspection_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_job_masters` */

CREATE TABLE IF NOT EXISTS `vehicle_job_masters` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `unique_no` VARCHAR(100) DEFAULT NULL,
  `max_unique_no` INT(10) DEFAULT NULL,
  `invoice_no` VARCHAR(100) DEFAULT NULL,
  `max_invoice_no` INT(10) DEFAULT NULL,
  `job_date` DATETIME DEFAULT NULL,
  `customer_name` VARCHAR(120) DEFAULT NULL,
  `customer_mobile_no` VARCHAR(14) DEFAULT NULL,
  `customer_email_id` VARCHAR(200) DEFAULT NULL,
  `customer_address` TEXT,
  `part_total_tax` DECIMAL(10,2) DEFAULT '0.00',
  `part_total_price` DECIMAL(10,2) DEFAULT '0.00',
  `service_total_tax` DECIMAL(10,2) DEFAULT '0.00',
  `service_total_price` DECIMAL(10,2) DEFAULT '0.00',
  `job_status` TINYINT(2) DEFAULT '1' COMMENT '1=>open,2=>inprogress,3=>completed',
  `job_status_date` DATETIME DEFAULT NULL,
  `history` TEXT COMMENT 'job status history',
  `remark` TEXT,
  `is_paid` TINYINT(1) DEFAULT '0' COMMENT '0=>due,1=> paid',
  `payment_date` DATETIME DEFAULT NULL,
  `payment_mode` TINYINT(2) DEFAULT '1' COMMENT '1=>Cash,2=>Credit Card,3=>Debit Card,4=>Net Banking,5=>UPI',
  `payment_remark` TEXT,
  `service_km` varchar(20) DEFAULT NULL,
  `next_service_km` varchar(20) DEFAULT NULL,
  `is_release` TINYINT(1) DEFAULT '0' COMMENT '0=>working,1=>release from garage',
  `release_date` DATETIME DEFAULT NULL,
  `vehicle_master_id` INT(11) DEFAULT NULL,
  `employee_master_id` INT(11) DEFAULT NULL,
  `company_year_master_id` INT(11) DEFAULT NULL,
  `user_member_master_id` INT(11) DEFAULT NULL,
  `user_id` INT(11) DEFAULT NULL,
  `created` DATETIME DEFAULT NULL,
  `modified` DATETIME DEFAULT NULL,
  `status` TINYINT(1) DEFAULT '1' COMMENT '1=>active,0=>deleted',
  PRIMARY KEY (`id`),
  KEY `vehicle_job_ref_key1` (`vehicle_master_id`),
  KEY `vehicle_job_ref_key2` (`employee_master_id`),
  KEY `vehicle_job_ref_key3` (`company_year_master_id`),
  KEY `vehicle_job_ref_key4` (`user_member_master_id`),
  KEY `vehicle_job_ref_key5` (`user_id`),
  KEY `vehicle_sort_key` (`unique_no`,`vehicle_master_id`,`company_year_master_id`,`user_member_master_id`),
  CONSTRAINT `vehicle_job_ref_key1` FOREIGN KEY (`vehicle_master_id`) REFERENCES `vehicle_masters` (`id`),
  CONSTRAINT `vehicle_job_ref_key2` FOREIGN KEY (`employee_master_id`) REFERENCES `employee_masters` (`id`),
  CONSTRAINT `vehicle_job_ref_key3` FOREIGN KEY (`company_year_master_id`) REFERENCES `company_year_masters` (`id`),
  CONSTRAINT `vehicle_job_ref_key4` FOREIGN KEY (`user_member_master_id`) REFERENCES `user_member_masters` (`id`),
  CONSTRAINT `vehicle_job_ref_key5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_job_part_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_job_part_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_job_master_id` int(11) DEFAULT NULL,
  `part_group_master_id` int(11) DEFAULT NULL,
  `part_master_id` int(11) DEFAULT NULL,
  `unit_master_id` int(11) DEFAULT NULL,
  `base_price` decimal(10,2) DEFAULT '0.00',
  `quantity` int(4) DEFAULT '0',
  `base_tax` decimal(10,2) DEFAULT '0.00',
  `total_tax` decimal(10,2) DEFAULT '0.00',
  `total_price` decimal(10,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_job_part_ref_key1` (`vehicle_job_master_id`),
  KEY `vehicle_job_part_ref_key2` (`part_master_id`),
  KEY `vehicle_job_part_ref_key3` (`unit_master_id`),
  KEY `vehicle_job_part_ref_key4` (`part_group_master_id`),
  CONSTRAINT `vehicle_job_part_ref_key1` FOREIGN KEY (`vehicle_job_master_id`) REFERENCES `vehicle_job_masters` (`id`),
  CONSTRAINT `vehicle_job_part_ref_key2` FOREIGN KEY (`part_master_id`) REFERENCES `part_masters` (`id`),
  CONSTRAINT `vehicle_job_part_ref_key3` FOREIGN KEY (`unit_master_id`) REFERENCES `unit_masters` (`id`),
  CONSTRAINT `vehicle_job_part_ref_key4` FOREIGN KEY (`part_group_master_id`) REFERENCES `part_group_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_job_service_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_job_service_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_job_master_id` int(11) DEFAULT NULL,
  `service_group_master_id` int(11) DEFAULT NULL,
  `service_master_id` int(11) DEFAULT NULL,
  `base_price` decimal(10,2) DEFAULT '0.00',
  `quantity` int(4) DEFAULT '0',
  `total_price` decimal(10,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `veh_job_service_ref_key1` (`vehicle_job_master_id`),
  KEY `veh_job_service_ref_key2` (`service_master_id`),
  KEY `veh_job_service_ref_key3` (`service_group_master_id`),
  CONSTRAINT `veh_job_service_ref_key1` FOREIGN KEY (`vehicle_job_master_id`) REFERENCES `vehicle_job_masters` (`id`),
  CONSTRAINT `veh_job_service_ref_key2` FOREIGN KEY (`service_master_id`) REFERENCES `service_masters` (`id`),
  CONSTRAINT `veh_job_service_ref_key3` FOREIGN KEY (`service_group_master_id`) REFERENCES `service_group_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_masters` */

CREATE TABLE IF NOT EXISTS `vehicle_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_no` varchar(100) DEFAULT NULL,
  `max_unique_no` int(10) DEFAULT NULL,
  `vehicle_number` varchar(30) DEFAULT NULL,
  `vehicle_model_master_id` int(11) DEFAULT NULL,
  `manufacturer_year` varchar(8) DEFAULT NULL,
  `engine_number` varchar(40) DEFAULT NULL,
  `chasis_number` varchar(50) DEFAULT NULL,
  `fule_type` tinyint(2) DEFAULT '1' COMMENT '1 => Patrol,2=>Diesel,3=>CNG',
  `vehicle_start_method` tinyint(2) DEFAULT '1' COMMENT '1=>Self,2=>Kick,3=>Both',
  `variant_master_id` int(11) DEFAULT NULL,
  `vehicle_type` tinyint(2) DEFAULT '1' COMMENT '1=>Sell,2=>Service',
  `purchase_price` decimal(10,2) DEFAULT '0.00',
  `onroad_price` decimal(10,2) DEFAULT '0.00',
  `profit` decimal(10,2) DEFAULT '0.00',
  `purchase_from` varchar(160) DEFAULT NULL,
  `purchase_address` text,
  `owner_title_master_id` int(11) DEFAULT NULL,
  `owner_first_name` varchar(60) DEFAULT NULL,
  `owner_middle_name` varchar(60) DEFAULT NULL,
  `owner_last_name` varchar(60) DEFAULT NULL,
  `owner_mobile_no` varchar(14) DEFAULT NULL,
  `owner_email_id` varchar(160) DEFAULT NULL,
  `owner_profession` varchar(160) DEFAULT NULL,
  `owner_address` text,
  `owner_country_master_id` int(11) DEFAULT NULL,
  `owner_state_master_id` int(11) DEFAULT NULL,
  `owner_city_master_id` int(11) DEFAULT NULL,
  `owner_remark` text,
  `user_member_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_year_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_number_key` (`unique_no`,`vehicle_number`),
  KEY `vehicle_ref_key1` (`vehicle_model_master_id`),
  KEY `vehicle_ref_key2` (`owner_title_master_id`),
  KEY `vehicle_ref_key3` (`user_member_master_id`),
  KEY `vehicle_ref_key4` (`user_id`),
  KEY `vehicle_ref_key5` (`variant_master_id`),
  KEY `vehicle_ref_key6` (`owner_country_master_id`),
  KEY `vehicle_ref_key7` (`owner_state_master_id`),
  KEY `vehicle_ref_key8` (`owner_city_master_id`),
  KEY `vehicle_ref_key9` (`company_year_master_id`),
  CONSTRAINT `vehicle_ref_key1` FOREIGN KEY (`vehicle_model_master_id`) REFERENCES `vehicle_model_masters` (`id`),
  CONSTRAINT `vehicle_ref_key2` FOREIGN KEY (`owner_title_master_id`) REFERENCES `title_masters` (`id`),
  CONSTRAINT `vehicle_ref_key3` FOREIGN KEY (`user_member_master_id`) REFERENCES `user_member_masters` (`id`),
  CONSTRAINT `vehicle_ref_key4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `vehicle_ref_key5` FOREIGN KEY (`variant_master_id`) REFERENCES `variant_masters` (`id`),
  CONSTRAINT `vehicle_ref_key6` FOREIGN KEY (`owner_country_master_id`) REFERENCES `country_masters` (`id`),
  CONSTRAINT `vehicle_ref_key7` FOREIGN KEY (`owner_state_master_id`) REFERENCES `state_masters` (`id`),
  CONSTRAINT `vehicle_ref_key8` FOREIGN KEY (`owner_city_master_id`) REFERENCES `city_masters` (`id`),
  CONSTRAINT `vehicle_ref_key9` FOREIGN KEY (`company_year_master_id`) REFERENCES `company_year_masters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_model_masters` */

CREATE TABLE IF NOT EXISTS `vehicle_model_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `manufacturer_tran_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` tinyint(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_model_key1` (`name`),
  KEY `vehicle_model_key2` (`code`),
  KEY `model_ref_key1` (`manufacturer_tran_id`),
  CONSTRAINT `model_ref_key1` FOREIGN KEY (`manufacturer_tran_id`) REFERENCES `manufacturer_trans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_specification_masters` */

CREATE TABLE IF NOT EXISTS `vehicle_specification_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(80) DEFAULT NULL,
  `vehicle_type_master_id` int(11) DEFAULT NULL,
  `description` text,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,2=>inactive',
  PRIMARY KEY (`id`),
  KEY `vehicle_specification_key` (`name`,`code`),
  KEY `specification_ref_key` (`vehicle_type_master_id`),
  CONSTRAINT `specification_ref_key` FOREIGN KEY (`vehicle_type_master_id`) REFERENCES `vehicle_type_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_specification_trans` */

CREATE TABLE IF NOT EXISTS `vehicle_specification_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_master_id` int(11) DEFAULT NULL,
  `vehicle_specification_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`),
  KEY `spe_tran_key1` (`vehicle_master_id`),
  KEY `spe_tran_key2` (`vehicle_specification_master_id`),
  CONSTRAINT `vehicle_sp_ref_key1` FOREIGN KEY (`vehicle_master_id`) REFERENCES `vehicle_masters` (`id`),
  CONSTRAINT `vehicle_sp_ref_key2` FOREIGN KEY (`vehicle_specification_master_id`) REFERENCES `vehicle_specification_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_type_masters` */

CREATE TABLE IF NOT EXISTS `vehicle_type_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `order_no` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1=>active,0=>inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS = 1;
