<?php
App::uses('AppController','Controller');
class ProductMastersController extends AppController {
    public $name = 'ProductMasters';
    public $layout = false;
    public $uses = array('ProductMaster','ProductImageTran','ProductVariantTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('ProductMaster.status' => 1,'ProductMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['ProductMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['ProductMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }
                
                if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                    $conditions['ProductMaster.category_master_id'] = trim($this->request->data['category_master_id']);
                }

                if(isset($this->request->data['type']) && !empty($this->request->data['type'])) {
                    $conditions['ProductMaster.type'] = trim($this->request->data['type']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('CategoryMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('CategoryMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('CategoryMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('CategoryMaster.order_no ASC');
                }

                $joins  =   array(
                                array(
                                    'table' => 'category_masters',
                                    'alias' => 'CategoryMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id','CategoryMaster.status' => 1)
                                )
                            );
                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'joins' => $joins,'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->ProductMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','type','category_name','order_no','trans_exists'),
                                    'joins' => $joins,
                                    'conditions' => $conditions,
                                    'group' => 'ProductMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->ProductMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ProductMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['ProductMaster']['name'];
                        $records[$key]['code'] = $tableDetails['ProductMaster']['code'];
                        $records[$key]['type'] = ($tableDetails['ProductMaster']['type'] == 1) ? '<div class = "btn btn-xs green">New Product</div>' : '<div class ="btn btn-xs blue">Scrape Product</div>';
                        $records[$key]['category'] = $tableDetails['ProductMaster']['category_name'];
                        $records[$key]['order_no'] = $tableDetails['ProductMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails['ProductMaster']['trans_exists'];
                        $maxOrderNo = ($tableDetails['ProductMaster']['order_no'] > $maxOrderNo) ? $tableDetails['ProductMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','category'=>'Category');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['ProductMaster'] = $this->request->data;
                #pr($arrSaveRecords['ProductMaster']);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['ProductMaster']['id']) && !empty($arrSaveRecords['ProductMaster']['id'])) {
                    $arrSaveRecords['ProductMaster']['id'] = $this->decryption($arrSaveRecords['ProductMaster']['id']);
                }
                $this->ProductMaster->set($arrSaveRecords);
                if($this->ProductMaster->validates()) {
                    $arrProductImageData = $arrDeleteImageTran =  array();
                    if(isset($arrSaveRecords['ProductMaster']['product_image']) && count($arrSaveRecords['ProductMaster']['product_image']) >0 ){
                        $arrProductImageData = $arrSaveRecords['ProductMaster']['product_image'];
                    }
                    $arrSaveRecords['ProductMaster']['is_taxable'] = 0;
                    if(!empty($arrSaveRecords['ProductMaster']['tax_group_master_id'])) {
                        $arrSaveRecords['ProductMaster']['is_taxable'] = 1;
                    }
                    $dataSource = $this->ProductMaster->getDataSource();
                    $fieldList = array('id','name','code','hsn_code','stock_type','sale_price','purchase_price','unit_master_id','is_taxable','category_master_id','type','tax_group_master_id','branch_master_id','warrenty','description','order_no');
                    $arrProductVariantTranData = array();
                    try {
                        $dataSource->begin();
                        $arrSaveRecords['ProductMaster']['sale_price'] = number_format($arrSaveRecords['ProductMaster']['sale_price'],2,'.','');
                        if(!isset($arrSaveRecords['ProductMaster']['id']) || empty($arrSaveRecords['ProductMaster']['id'])) {
                            $this->ProductMaster->create();
                        } else {
                            $options = array('fields' => array('image','id'),'conditions' => array('status' => 1,'product_master_id' => $arrSaveRecords['ProductMaster']['id']));
                            $arrImageTran = $this->ProductImageTran->find('list',$options);
                            if(!empty($arrImageTran)) {
                                $arrDeleteImageTran = array_diff(array_keys($arrImageTran),$arrProductImageData);
                            }
                            $options = array('fields' => array('id'),'conditions' => array('product_master_id' => $arrSaveRecords['ProductMaster']['id'],'branch_master_id' => $arrSaveRecords['ProductMaster']['branch_master_id'],'price' => $arrSaveRecords['ProductMaster']['sale_price'],'status' => 1));
                            $arrProductVariantData = $this->ProductVariantTran->find('first',$options);
                            if(count($arrProductVariantData) > 0) {
                                $arrProductVariantTranData['ProductVariantTran']['id'] = $arrProductVariantData['ProductVariantTran']['id'];
                            }
                        }

                        if(!$this->ProductMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $isNew = 0;
                        if(!isset($arrSaveRecords['ProductMaster']['id'])) {
                            $isNew = 1;
                            $arrSaveRecords['ProductMaster']['id'] = $this->ProductMaster->getInsertID();
                        }

                        $updateFields['ProductVariantTran.stock_price'] = 0;
                        $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                        $updateParams['ProductVariantTran.product_master_id'] = $arrSaveRecords['ProductMaster']['id'];
                        if(isset($arrProductVariantTranData['ProductVariantTran']['id'])) {
                            $updateParams['NOT']['ProductVariantTran.id'] = $arrProductVariantTranData['ProductVariantTran']['id'];
                        }
                        if(!$this->ProductVariantTran->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        unset($updateFields,$updateParams);

                        if(!isset($arrProductVariantTranData['ProductVariantTran']['id'])) {
                            $this->ProductVariantTran->create();
                            $arrProductVariantTranData['ProductVariantTran']['date'] = date('Y-m-d');
                        }
                        $arrProductVariantTranData['ProductVariantTran']['stock_price'] = 1;
                        $arrProductVariantTranData['ProductVariantTran']['product_master_id'] = $arrSaveRecords['ProductMaster']['id'];
                        $arrProductVariantTranData['ProductVariantTran']['price'] = $arrSaveRecords['ProductMaster']['sale_price'];
                        $arrProductVariantTranData['ProductVariantTran']['branch_master_id'] = $arrSaveRecords['ProductMaster']['branch_master_id'] ;
                        $this->ProductVariantTran->save($arrProductVariantTranData);

                        if(!empty($arrDeleteImageTran)) {
                            $updateFields['ProductImageTran.status'] = 0;
                            $updateFields['ProductImageTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['ProductImageTran.image'] = $arrDeleteImageTran;
                            if(!$this->ProductImageTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }

                        if(count($arrProductImageData) > 0) {
                            $arrProductImageTran = array();
                            $i = 0;
                            $dir = Configure::read('UPLOAD_PRODUCT_IMAGE_DIR');
                            if(!file_exists($dir)) {
                                mkdir($dir,0777,true);
                            }
                            foreach($arrProductImageData as $key => $imageName) {
                                if(!empty($imageName)) {
                                    if(isset($arrImageTran[$imageName])) {
                                        $arrProductImageTran[$i]['ProductImageTran']['id'] = $arrImageTran[$imageName];
                                    }
                                    $arrProductImageTran[$i]['ProductImageTran']['image'] = $imageName;
                                    $arrProductImageTran[$i]['ProductImageTran']['branch_master_id'] = $arrSaveRecords['ProductMaster']['branch_master_id'];
                                    $arrProductImageTran[$i]['ProductImageTran']['product_master_id'] = $arrSaveRecords['ProductMaster']['id'];
                                    $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$imageName;
                                    if(@file_exists($tmpPath)) {
                                        $saveImagePath = $dir.'/'.$imageName;
                                        @copy($tmpPath,$saveImagePath);
                                        @unlink($tmpPath);
                                    }
                                    ++$i;
                                }
                            }
                            if(count($arrProductImageTran) > 0) {
                                if(!$this->ProductImageTran->saveAll($arrProductImageTran)) {
                                    throw new Exception(__('Record could not saved properly, please try again later!',true));
                                }
                            }
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if($isNew == 0) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'part_group_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->ProductMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'product_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$type = 0,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId)) {
                    $type = (int) $type;
                    $id = $this->decryption(trim($id));
                    $joins = array();
                    
                    if($type === 1) {
                        $fields = array('name','code','hsn_code','description','stock_type','sale_price','purchase_price','unit','category_name','tax_group','warrenty','type','order_no');
                        $joins = array(
                                        array(
                                            'table' => 'category_masters',
                                            'alias' => 'CategoryMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id','CategoryMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'tax_group_masters',
                                            'alias' => 'TaxGroupMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('ProductMaster.tax_group_master_id = TaxGroupMaster.id','TaxGroupMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'unit_masters',
                                            'alias' => 'UnitMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('ProductMaster.unit_master_id = UnitMaster.id','UnitMaster.status' => 1)
                                        )
                                );
                    } else {
                        $fields = array('name','code','hsn_code','description','stock_type','sale_price','unit_master_id','purchase_price','category_master_id','tax_group_master_id','warrenty','type','order_no');
                    }
                    $options = array(
                            'fields' => $fields,
                            'joins' => $joins,
                            'conditions' => array('ProductMaster.status' => 1,'ProductMaster.id' => $id,'ProductMaster.branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->ProductMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $options = array('fields' => array('id','image'),'conditions' => array('product_master_id' => $id,'status' => 1,'branch_master_id' => $firmId));
                            $arrVehicleImageData = $this->ProductImageTran->find('all',$options);
                            $arrVehicleImageData = Hash::combine($arrVehicleImageData,'{n}.ProductImageTran.id','{n}.ProductImageTran');
                            if(!empty($arrVehicleImageData)) {
                                $arrVehicleImageData = array_values($arrVehicleImageData);
                            }
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['ProductMaster'],'image' => $arrVehicleImageData);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'type' => $type),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('id','id'),'conditions' => array('status'=> 1,'id' => $this->request->data['id'],'trans_exists' => 1));
                    $arrRecords = $this->ProductMaster->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->ProductMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['ProductMaster.status'] = 0;
                            $updateFields['ProductMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ProductMaster.id'] = $arrDeleteRecords;
                            if(!$this->ProductMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateParams,$updateFields,$arrDeleteRecords);

                            $updateFields['ProductVariantTran.status'] = 0;
                            $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ProductVariantTran.product_master_id'] = $this->request->data['id'];
                            if(!$this->ProductVariantTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
