<?php
App::uses('AppController','Controller');
class InvoiceMastersController extends AppController {
    public $name = 'InvoiceMasters';
    public $layout = false;
    public $uses = array('InvoiceMaster','InvoiceTran','InvoiceTaxTran','ProductStockTran','PurchaseMaster','ProductMaster','ProductVariantTran','ProductVariantStockTran','TmpProductVariantStockTran','VehicleCouponMaster','VehicleCouponTran','VehicleJobMaster','VehicleSaleMaster','VehicleMaster','RoyaltyChargeTran','VehicleSaleDocumentTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function index($type = 1,$dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $type = (int) $type;
                $dataType = (int) $dataType;
                $arrVehicleId = array();
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $conditions = array('InvoiceMaster.branch_master_id' => $firmId,'InvoiceMaster.company_year_master_id' => $companyYearId,'InvoiceMaster.status' => 1,'InvoiceMaster.type' => $type);
                if(isset($this->request->data['invoice_no']) && !empty($this->request->data['invoice_no'])) {
                    $conditions['InvoiceMaster.invoice_no LIKE'] = '%'.trim($this->request->data['invoice_no']).'%';
                }

                if(isset($this->request->data['vehicle_master_id']) && !empty($this->request->data['vehicle_master_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_master_id'])] = trim($this->request->data['vehicle_master_id']);
                }

                if(isset($this->request->data['vehicle_number_id']) && !empty($this->request->data['vehicle_number_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_number_id'])] = trim($this->request->data['vehicle_number_id']);
                }

                if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                    $conditions['VehicleMaster.vehicle_model_master_id'] = trim($this->request->data['vehicle_model_master_id']);
                }

                if(isset($this->request->data['vehicle_engine_id']) && !empty($this->request->data['vehicle_engine_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_engine_id'])] = trim($this->request->data['vehicle_engine_id']);
                }

                if(isset($this->request->data['vehicle_chasis_id']) && !empty($this->request->data['vehicle_chasis_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_chasis_id'])] = trim($this->request->data['vehicle_chasis_id']);
                }

                if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
                    $conditions['InvoiceMaster.invoice_date >='] = $this->AppUtilities->date_format($this->request->data['start_date'],'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['InvoiceMaster.invoice_date <='] = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                }

                if(isset($this->request->data['customer_master_id']) && !empty($this->request->data['customer_master_id'])) {
                    $conditions['InvoiceMaster.customer_master_id'] = trim($this->request->data['customer_master_id']);
                }
                if(!empty($arrVehicleId)) {
                    $conditions['InvoiceMaster.vehicle_master_id'] = array_values($arrVehicleId);
                }
                #pr($conditions);

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('InvoiceMaster.max_invoice_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('InvoiceMaster.invoice_date '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('InvoiceMaster.amount '.$sortyType);
                                break;
                        case 4:
                                if($type === 1) {
                                    $orderBy = array('SM.first_name '.$sortyType);
                                } else if($type ==  2) {
                                    $orderBy = array('CustomerMaster.first_name '.$sortyType);
                                } else if($type == 4) {
                                    $orderBy = array('VehicleMaster.max_unique_no '.$sortyType);
                                } else {
                                    $orderBy = array('InvoiceMaster.max_invoice_no '.$sortyType);
                                }
                                break;
                        default:
                                $orderBy = array('InvoiceMaster.max_invoice_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('InvoiceMaster.max_invoice_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->InvoiceMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $joins = array();
                $type = (int) $type;
                if($type === 1) {
                    $headers = array('count'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Supplier Name','p_status' => 'Status');
                    $tableName = 'SM';
                    $fields = array("CONCAT_WS(' ',TM.name,SM.first_name,SM.middle_name,SM.last_name) AS customer");
                    $joins[] =array(
                                    'table' => 'supplier_masters',
                                    'alias' => 'SM',
                                    'type' => 'LEFT',
                                    'conditions' => array('InvoiceMaster.customer_master_id = SM.id','SM.status' => 1)
                                );
                    $joins[] = array(
                                    'table' => 'title_masters',
                                    'alias' => 'TM',
                                    'type' => 'LEFT',
                                    'conditions' => array('SM.title_master_id = TM.id','TM.status' => 1)
                                );
                } else if($type === 2) {
                    $headers = array('count'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Customer Name','p_status' => 'Status');
                    $tableName = 'CustomerMaster';
                    $fields = array("CONCAT_WS(' ',TM.name,CustomerMaster.first_name,CustomerMaster.middle_name,CustomerMaster.last_name) AS customer");
                    $joins[] = array(
                                    'table' => 'customer_masters',
                                    'alias' => 'CustomerMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('InvoiceMaster.customer_master_id = CustomerMaster.id','CustomerMaster.status' => 1)
                                );
                    $joins[] = array(
                                    'table' => 'title_masters',
                                    'alias' => 'TM',
                                    'type' => 'LEFT',
                                    'conditions' => array('CustomerMaster.title_master_id = TM.id','TM.status' => 1)
                                );
                } else if($type === 3) {
                    $headers = array('count'=>'S.No','job_id'=>'Job ID','invoice_no' => 'Invoice No','invoice_date'=>'Invoice Date','vehicle'=>'Vehicle No','amount' => 'Amount','p_status' => 'Payment Status');
                    $tableName = '';
                    $fields = array('VJM.unique_no','VJM.job_date','VJM.job_time','VJM.vehicle_number','VJM.vehicle_code');
                    $joins[] = array(
                                    'table' => 'vehicle_jobs',
                                    'alias' => 'VJM',
                                    'type' => 'INNER',
                                    'conditions' => array('InvoiceMaster.customer_master_id = VJM.id','VJM.status' => 1)
                                );
                } else if($type === 4) {
                    $fields = array('InvoiceMaster.invoice_json');
                    $headers = array('count'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Vehicle Code','p_status' => 'Status');
                } else if($type === 5) {
                    $headers = array('count'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Vehicle Code','p_status' => 'Status');
                    $tableName = 'VehicleMaster';
                    $fields = array('VehicleMaster.unique_no','VehicleMaster.vehicle_number','VSM.vehicle_status','VSM.agent_name');
                    $joins[] = array(
                                    'table' => 'vehicle_sale_masters',
                                    'alias' => 'VSM',
                                    'type' => 'INNER',
                                    'conditions' => array('InvoiceMaster.id = VSM.invoice_master_id','VSM.status' => 1)
                                );
                    $joins[] = array(
                                    'table' => 'vehicle_masters',
                                    'alias' => 'VehicleMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('VSM.vehicle_master_id = VehicleMaster.id','VehicleMaster.status' => 1)
                                );
                } else {
                    $fields = $headers = array();
                }
                $fields = array_merge(array('id','invoice_no','invoice_date','amount','is_lock_invoice','payment_status'),$fields);
                $tableOptions = array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'group' => array('InvoiceMaster.id'),
                    'order' => $orderBy
                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->InvoiceMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['InvoiceMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['invoice_no'] = $tableDetails['InvoiceMaster']['invoice_no'];
                        $records[$key]['amount'] = $tableDetails['InvoiceMaster']['amount'];
                        if(isset($tableDetails[0]['customer'])) {
                            $records[$key]['customer'] = $tableDetails[0]['customer'];
                        } else if($type === 4 && isset($tableDetails['InvoiceMaster']['invoice_json']) && !empty($tableDetails['InvoiceMaster']['invoice_json'])) {
                            $arrInvoiceData = json_decode($tableDetails['InvoiceMaster']['invoice_json'],true);
                            if($dataType === 1) {
                                $records[$key]['customer'] = "<div><b>".$arrInvoiceData['customer']['unique_no']."</b></div><div><b>".$arrInvoiceData['customer']['vehicle_number']."</b></div>";
                            } else {
                                $records[$key]['customer'] = $arrInvoiceData['customer']['unique_no']."\n".$arrInvoiceData['customer']['vehicle_number'];
                            }
                        } else if($type === 5) {
                            if($dataType === 1) {
                                $records[$key]['customer'] = "<div><b>".$tableDetails['VehicleMaster']['unique_no']."</b></div><div><b>".$tableDetails['VehicleMaster']['vehicle_number']."</b></div>";
                            } else {
                                $records[$key]['customer'] = $tableDetails['VehicleMaster']['unique_no']."\n".$tableDetails['VehicleMaster']['vehicle_number'];
                            }
                        } else{}
                        if(isset($tableDetails['VJM']['unique_no'])) {
                            if($dataType === 1) {
                                $records[$key]['job_id']  = '<b>'.$tableDetails['VJM']['unique_no'].'</b><div>'.date('d-m-Y H:i:s',strtotime($tableDetails['VJM']['job_date'].' '.$tableDetails['VJM']['job_time'])).'</div>';
                                $records[$key]['vehicle'] = '<b>'.$tableDetails['VJM']['vehicle_number'].'</b><div>'.$tableDetails['VJM']['vehicle_code'].'</div>';
                            } else {
                                $records[$key]['job_id']  = $tableDetails['VJM']['unique_no']."\n \n".date('d-m-Y H:i:s',strtotime($tableDetails['VJM']['job_date'].' '.$tableDetails['VJM']['job_time']));
                                $records[$key]['vehicle'] = $tableDetails['VJM']['vehicle_number']."\n \n".$tableDetails['VJM']['vehicle_code'];
                            }
                        } else if(isset($tableDetails['VSM']['vehicle_status'])) {
                            $vehicleStatus = (int) $tableDetails['VSM']['vehicle_status'];
                            $btn = '';
                            if($vehicleStatus === 0) {
                                $btn = '<div class ="btn btn-xs btn-warning">Pending</div>';
                            } else if($vehicleStatus === 1) {
                                $btn = '<div class ="btn btn-xs btn-primary">Assigned Agent</div>';
                            } else if($vehicleStatus === 2) {
                                $btn = '<div class ="btn btn-xs yellow">In progress</div>';
                            } else if($vehicleStatus === 3) {
                                $btn = '<div class ="btn btn-xs red">HOLD</div>';
                            } else if($vehicleStatus === 4) {
                                $btn = '<div class ="btn btn-xs blue">Completed</div>';
                            } else {}

                            if(!empty($tableDetails['VSM']['agent_name'])) {
                                $btn.= '<div class ="agent">'.$tableDetails['VSM']['agent_name'].'<div>';
                            }
                            $records[$key]['vehicle_status'] = $btn;
                            $records[$key]['status'] = $vehicleStatus;
                        } else {}
                        $records[$key]['payment_type'] = (int) $tableDetails['InvoiceMaster']['payment_status'];
                        $pstatus = '';
                        if($records[$key]['payment_type'] === 0) {
                            $pstatus = 'PENDING';
                        } else if($records[$key]['payment_type'] === 1) {
                            $pstatus = 'PARTIAL PAID';
                        } else if($records[$key]['payment_type'] === 2) {
                            $pstatus = 'PAID';
                        }
                        $records[$key]['p_status'] = $pstatus;
                        $records[$key]['invoice_date'] = $this->AppUtilities->date_format($tableDetails['InvoiceMaster']['invoice_date'],'dd-mm-yyyy');
                        if(isset($records[$key]['status']) && $records[$key]['status'] > 3) {
                            $records[$key]['is_exists'] = 1;
                        } else {
                            $records[$key]['is_exists'] = $tableDetails['InvoiceMaster']['is_lock_invoice'];
                        }
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_purchase_order() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $conditions = array('PurchaseMaster.status' => 1,'PurchaseMaster.generate_invoice' => 0,'PurchaseMaster.branch_master_id' => $firmId,'PurchaseMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['voucher_no']) && !empty($this->request->data['voucher_no'])) {
                    $conditions['PurchaseMaster.voucher_no'] = explode(',',$this->request->data['voucher_no']);
                }

                if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
                    $conditions['PurchaseMaster.voucher_date >='] = $this->AppUtilities->date_format(trim($this->request->data['start_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['PurchaseMaster.voucher_date <='] = $this->AppUtilities->date_format(trim($this->request->data['end_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['supplier_master_id']) && !empty($this->request->data['supplier_master_id'])) {
                    $conditions['PurchaseMaster.supplier_master_id'] = $this->request->data['supplier_master_id'];
                }
                $tableOptions = array(
                    'fields' => array('id','voucher_no','voucher_date','quantity',"CONCAT_WS(' ',TM.name,SM.first_name,SM.middle_name,SM.last_name) AS supplier",'SM.id'),
                    'joins' => array(
                        array(
                            'table' => 'supplier_masters',
                            'alias' => 'SM',
                            'type' => 'LEFT',
                            'conditions' => array('PurchaseMaster.supplier_master_id = SM.id','SM.status' => 1)
                        ),
                        array(
                            'table' => 'title_masters',
                            'alias' => 'TM',
                            'type' => 'LEFT',
                            'conditions' => array('SM.title_master_id = TM.id','TM.status' => 1)
                        )
                    ),
                    'conditions' => $conditions,
                    'group' => array('PurchaseMaster.id'),
                    'recursive' => -1
                );
                $arrTableData = $this->PurchaseMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $arrRecords = array();
                    foreach($arrTableData as $key => $data) {
                        $arrRecords[$key]['id'] = $this->encryption($data['PurchaseMaster']['id']);
                        $arrRecords[$key]['voucher_no'] = $data['PurchaseMaster']['voucher_no'];
                        $arrRecords[$key]['voucher_date'] = $this->AppUtilities->date_format($data['PurchaseMaster']['voucher_date'],'yyyy-mm-dd');
                        $arrRecords[$key]['quantity'] = $data['PurchaseMaster']['quantity'];
                        $arrRecords[$key]['supplier'] = $data[0]['supplier'];
                        $arrRecords[$key]['supplier_id'] = $data['SM']['id'];
                    }
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function purchase_voucher_detail() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && isset($this->request->data['branch_master_id'])) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array(
                        'fields' => array('id','name','code','hsn_code','PVT.id','PVT.price as previous_price','SUM(PT.quantity) AS quantity'),
                        'joins' => array(
                            array(
                                'table' => 'product_variant_trans',
                                'alias' =>'PVT',
                                'type' => 'INNER',
                                'conditions' => array('ProductMaster.id = PVT.product_master_id','PVT.status' => 1)
                            ),
                            array(
                                'table' => 'purchase_trans',
                                'alias' =>'PT',
                                'type' => 'INNER',
                                'conditions' => array('PVT.id = PT.product_variant_tran_id','PT.status' => 1)
                            )
                        ),
                        'conditions' => array(
                            'ProductMaster.branch_master_id' => $this->request->data['branch_master_id'],
                            'PT.branch_master_id' => $this->request->data['branch_master_id'],
                            'PT.purchase_master_id' => $this->request->data['id'],
                            'ProductMaster.status' => 1
                        ),
                        'group' => array('PVT.id'),
                        'order' => array('ProductMaster.order_no')
                    );
                    $arrProductDetail = $this->ProductMaster->find('all',$options);
                    if(count($arrProductDetail) > 0) {
                        $arrRecords = array();
                        $totalQty = 0;
                        foreach($arrProductDetail as $key => $product) {
                            $arrRecords[$key]['id'] = $product['ProductMaster']['id'];
                            $arrRecords[$key]['product_variant_tran_id'] = $product['PVT']['id'];
                            $arrRecords[$key]['name'] = $product['ProductMaster']['name'];
                            $arrRecords[$key]['code'] = $product['ProductMaster']['code'];
                            $arrRecords[$key]['hsn_code'] = $product['ProductMaster']['hsn_code'];
                            $arrRecords[$key]['previous_price'] = (float) $product['PVT']['previous_price'];
                            $arrRecords[$key]['quantity'] = $product[0]['quantity'];
                            $totalQty += $product[0]['quantity'];
                        }
                        $arrUserSettingData = $this->AppUtilities->getUserSettingCode($this->request->data['branch_master_id'],2);
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords,'invoice_detail' => $arrUserSettingData,'qty' => (int)$totalQty);
                        $statusCode = 200;
                    } else {
                        $statusCode = 200;
                        $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function purchase_product_detail($id = 0,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($branchId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                                'fields' => array('id','name','code','hsn_code','SUM(PT.quantity) AS quantity'),
                                'joins' => array(
                                    array(
                                        'table' => 'product_variant_trans',
                                        'alias' =>'PVT',
                                        'type' => 'INNER',
                                        'conditions' => array('ProductMaster.id = PVT.product_master_id','PVT.status' => 1)
                                    ),
                                    array(
                                        'table' => 'purchase_trans',
                                        'alias' =>'PT',
                                        'type' => 'INNER',
                                        'conditions' => array('PVT.id = PT.product_variant_tran_id','PT.status' => 1)
                                    )
                                ),
                                'conditions' => array(
                                    'ProductMaster.branch_master_id' => $branchId,
                                    'PT.branch_master_id' => $branchId,
                                    'PT.purchase_master_id' => $id,
                                    'ProductMaster.status' => 1
                                ),
                                'group' => array('PVT.id'),
                                'order' => array('ProductMaster.order_no')
                            );
                    $arrProductDetail = $this->ProductMaster->find('all',$options);
                    if(count($arrProductDetail) > 0) {
                        $arrRecords = array();
                        foreach($arrProductDetail as $key => $product) {
                            $arrRecords[$key]['id'] = $product['ProductMaster']['id'];
                            $arrRecords[$key]['name'] = $product['ProductMaster']['name'];
                            $arrRecords[$key]['code'] = $product['ProductMaster']['code'];
                            $arrRecords[$key]['hsn_code'] = $product['ProductMaster']['hsn_code'];
                            $arrRecords[$key]['quantity'] = $product[0]['quantity'];
                        }
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                        $statusCode = 200;
                    } else {
                        $statusCode = 200;
                        $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_id' => $branchId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /***
     * This function is used generate invoice of purchase order
     */
    public function save_purchase_invoice() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(count($this->request->data) > 0) {
                    #pr($this->request->data);exit;
                    $arrValidateData['InvoiceMaster'] = $this->request->data;
                    $arrValidateData['InvoiceMaster']['branch_master_id'] = isset($this->request->data['branch_master_id']) ? $this->request->data['branch_master_id'] : '';
                    $arrValidateData['InvoiceMaster']['company_year_master_id'] = isset($this->request->data['company_year_master_id']) ? $this->request->data['company_year_master_id'] : '';
                    $arrProdctDetail = $arrVoucherDetail = array();
                    $supplierId = 0;
                    if(isset($arrValidateData['InvoiceMaster']['purchase_data']) && !empty($arrValidateData['InvoiceMaster']['purchase_data'])) {
                        $arrPurchaseData = json_decode($arrValidateData['InvoiceMaster']['purchase_data'],true);
                        $arrVoucherDetail = $this->bulk_decryption($arrPurchaseData['voucher']['id']);
                        $arrProdctDetail = $arrPurchaseData['product'];
                        $supplierId = $arrPurchaseData['voucher']['supplier_id'];
                        $arrValidateData['InvoiceMaster']['purchase_price'] = array_column($arrProdctDetail,'purchase_price');
                        $arrValidateData['InvoiceMaster']['sale_price'] = array_column($arrProdctDetail,'sale_price');
                        unset($arrValidateData['InvoiceMaster']['purchase_data'],$arrPurchaseData);
                    }
                    #pr($arrVoucherDetail);exit;
                    $this->InvoiceMaster->set($arrValidateData);
                    if($this->InvoiceMaster->validates()) {
                        $dataSource = $this->InvoiceMaster->getDataSource();
                        unset($arrValidateData);
                        $arrSaveInvoiceData = array();
                        try {
                            $dataSource->begin();
                            $firmId = $this->request->data['branch_master_id'];
                            $tableName = 'AutoInvoice'.$firmId.'Code';
                            $arrSaveInvoiceData[$tableName]['name'] = $this->request->data['invoice_date'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrSaveInvoiceData)) {
                                throw new Exception(__('Auto Invoice code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_no'] = $arrCodeData['code'];
                            $arrSaveInvoiceData['InvoiceMaster']['max_invoice_no'] = $arrCodeData['maxNumber'];
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_date'] = $this->AppUtilities->date_format($this->request->data['invoice_date'],'yyyy-mm-dd');
                            $arrSaveInvoiceData['InvoiceMaster']['amount'] = $this->request->data['actual_amount'];
                            $arrSaveInvoiceData['InvoiceMaster']['received_date'] = $this->AppUtilities->date_format($this->request->data['invoice_date'],'yyyy-mm-dd');
                            $arrSaveInvoiceData['InvoiceMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                            $arrSaveInvoiceData['InvoiceMaster']['type'] = 1;
                            $arrSaveInvoiceData['InvoiceMaster']['customer_master_id'] = $supplierId;
                            if(isset($this->request->data['paylater']) && !empty($this->request->data['paylater'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 0;
                            } else {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                                $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                                $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                                if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                                }
                                if($this->request->data['actual_amount'] > $this->request->data['amount']) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 1;
                                    $arrSaveInvoiceData['InvoiceMaster']['part_amount'] = number_format($this->request->data['amount'],2,'.','');
                                } else {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                                }
                                $history['payment_mode'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode'];
                                $history['bank_master_id'] = $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'];
                                $history['payment_reference_no'] = $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'];
                                $history['payment_status'] = $arrSaveInvoiceData['InvoiceMaster']['payment_status'];
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['part_amount'])) {
                                    $history['part_amount'] = $arrSaveInvoiceData['InvoiceMaster']['part_amount'];
                                }
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'])) {
                                    $history['payment_mode_date'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'];
                                }
                                $arrHistory[] = $history;
                                $arrSaveInvoiceData['InvoiceMaster']['history'] = json_encode($arrHistory);
                                unset($arrHistory);
                            }
                            if(isset($this->request->data['purchase_data']) && !empty($this->request->data['purchase_data'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['invoice_json'] = $this->request->data['purchase_data'];
                            }
                            if(!empty($arrVoucherDetail)) {
                                $arrSaveInvoiceData['InvoiceMaster']['reference_ids'] = json_encode($arrVoucherDetail);
                            }
                            $arrSaveInvoiceData['InvoiceMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            if(isset($this->request->data['is_lock_invoice'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['is_lock_invoice'] = 1;
                            }
                            $fieldList = array('id','invoice_no','max_invoice_no','type','invoice_date','amount','part_amount','history','is_lock_invoice','received_date','payment_status','customer_master_id','payment_mode','bank_master_id','remark','payment_reference_no','payment_mode_date','reference_ids','invoice_json','branch_master_id','company_year_master_id');
                            $this->InvoiceMaster->create();
                            #pr($arrSaveInvoiceData);
                            $this->InvoiceMaster->save($arrSaveInvoiceData,array('validate' => false,'fieldList' => $fieldList));
                            $maxInvoiceId = $this->InvoiceMaster->getInsertID();
                            unset($arrSaveInvoiceData);
                            #pr($arrProdctDetail);
                            if(count($arrProdctDetail) > 0) {
                                $arrSaveInvoicTranData = $arrSaveProductData = $arrUpdateProductVaraintData = $arrSaveProductVariantData = $arrProductStockVariantData = array();
                                $arrProductId = array_column($arrProdctDetail,'id');
                                $counter = 0;
                                $options = array(
                                            'fields' => array('product_master_id','price','id'),
                                            'conditions' => array('product_master_id' => $arrProductId,'branch_master_id' => $this->request->data['branch_master_id'],'status' => 1)
                                        );
                                $arrProductVariantDetail = $this->ProductVariantTran->find('all',$options);
                                if(count($arrProductVariantDetail) > 0) {
                                    $arrProductVariantDetail = Hash::combine($arrProductVariantDetail,'{n}.ProductVariantTran.price','{n}.ProductVariantTran.id','{n}.ProductVariantTran.product_master_id');
                                }

                                $arrSaveProductVariantStockData = array();
                                foreach($arrProdctDetail as $key => $product) {
                                    $options = array('fields' => array('id','reference_master_id','quantity'),'conditions' => array('reference_master_id' => $arrVoucherDetail,'product_variant_tran_id' => $product['product_variant_tran_id'],'branch_master_id' => $this->request->data['branch_master_id'],'status' => 1,'type' => 2,'from_type' => 1));
                                    $arrProductStockTranData = $this->ProductVariantStockTran->find('all',$options);
                                    if(count($arrProductStockTranData) > 0) {
                                        $salePrice = number_format($product['sale_price'],2,'.','');
                                        $prevPrice = number_format($product['previous_price'],2,'.','');
                                        if($salePrice != $prevPrice) {
                                            if(isset($arrProductVariantDetail[$product['id']][$salePrice])) {
                                                $productVariantId = $arrProductVariantDetail[$product['id']][$salePrice];
                                            } else {
                                                $arrSaveProductVariantData = array();
                                                $arrSaveProductVariantData['ProductVariantTran']['product_master_id'] = $product['id'];
                                                $arrSaveProductVariantData['ProductVariantTran']['price'] = $salePrice;
                                                $arrSaveProductVariantData['ProductVariantTran']['stock_price'] = 1;
                                                $arrSaveProductVariantData['ProductVariantTran']['date'] = date('Y-m-d');
                                                $arrSaveProductVariantData['ProductVariantTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                                $fieldList = array('id','product_master_id','price','stock_price','branch_master_id');
                                                $this->ProductVariantTran->create();
                                                $this->ProductVariantTran->save($arrSaveProductVariantData,array('fieldList' => $fieldList));
                                                $productVariantId = $this->ProductVariantTran->getInsertID();
                                                unset($arrSaveProductVariantData);
                                            }

                                            foreach($arrProductStockTranData as $istkey => $stock) {
                                                $stockQty = (int)$stock['ProductVariantStockTran']['quantity'];
                                                $consumedOptions = array(
                                                    'fields' => array('SUM(ABS(quantity)) AS quantity'),
                                                    'conditions' => array('parent_id' => $stock['ProductVariantStockTran']['id'],'branch_master_id' => $this->request->data['branch_master_id'],'status' => 1,'stock_type' => 2,'tran_type' => 3,'type' => 1)
                                                );
                                                $arrProductStockConsumedData = $this->ProductVariantStockTran->find('first',$consumedOptions);
                                                if($arrProductStockConsumedData[0]['quantity'] > 0) {
                                                    $consumedQty = (int) abs($arrProductStockConsumedData[0]['quantity']);
                                                    $availableQty = intval($stockQty - $consumedQty);
                                                    $productStockData = array();
                                                    $productStockData['ProductVariantStockTran']['id'] = $stock['ProductVariantStockTran']['id'];
                                                    $productStockData['ProductVariantStockTran']['quantity'] = $consumedQty;
                                                    $productStockData['ProductVariantStockTran']['type'] = 1;
                                                    $productStockData['ProductVariantStockTran']['date'] = date('Y-m-d H:i:s');
                                                    $productStockData['ProductVariantStockTran']['invoice_master_id'] = $maxInvoiceId;
                                                    $arrSaveProductVariantStockData[] = $productStockData;
                                                    unset($productStockData);
                                                    if($availableQty > 0) {
                                                        $productStockData = array();
                                                        $productStockData['ProductVariantStockTran']['product_variant_tran_id'] = $productVariantId;
                                                        $productStockData['ProductVariantStockTran']['old_product_variant_tran_id'] = $product['product_variant_tran_id'];
                                                        $productStockData['ProductVariantStockTran']['reference_master_id'] = $stock['ProductVariantStockTran']['reference_master_id'];
                                                        $productStockData['ProductVariantStockTran']['type'] = 1;
                                                        $productStockData['ProductVariantStockTran']['date'] = date('Y-m-d H:i:s');
                                                        $productStockData['ProductVariantStockTran']['user_id'] = $this->Session->read('sessUserId');
                                                        $productStockData['ProductVariantStockTran']['stock_type'] = 1;
                                                        $productStockData['ProductVariantStockTran']['from_type'] = 1;
                                                        $productStockData['ProductVariantStockTran']['quantity'] = $availableQty;
                                                        $productStockData['ProductVariantStockTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                                        $productStockData['ProductVariantStockTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                                                        $productStockData['ProductVariantStockTran']['invoice_master_id'] = $maxInvoiceId;
                                                        $arrSaveProductVariantStockData[] = $productStockData;
                                                        unset($productStockData);
                                                    }
                                                } else {
                                                    $productStockData = array();
                                                    $productStockData['ProductVariantStockTran']['id'] = $stock['ProductVariantStockTran']['id'];
                                                    $productStockData['ProductVariantStockTran']['date'] = date('Y-m-d H:i:s');
                                                    $productStockData['ProductVariantStockTran']['product_variant_tran_id'] = $productVariantId;
                                                    $productStockData['ProductVariantStockTran']['type'] = 1;
                                                    $productStockData['ProductVariantStockTran']['invoice_master_id'] = $maxInvoiceId;
                                                    $arrSaveProductVariantStockData[] = $productStockData;
                                                    unset($productStockData);
                                                }
                                            }
                                        } else {
                                            $productVariantId = $product['product_variant_tran_id'];
                                        }

                                        $arrUpdateProductVaraintData[$product['id']] = $productVariantId;

                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['invoice_master_id'] = $maxInvoiceId;
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['reference_tran_id'] = $productVariantId;
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['unit_price'] = $product['purchase_price'];
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['sale_price'] = $product['sale_price'];
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['quantity'] = $product['quantity'];
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['subtotal'] = number_format($product['subtotal'],2,'.','');
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['total'] = number_format($product['total'],2,'.','');
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                        ++$counter;
                                    }
                                }
                                #pr($arrSaveProductVariantStockData);
                                #pr($arrSaveInvoicTranData);
                                #exit;
                                $fieldList = array('id','invoice_master_id','reference_tran_id','unit_price','sale_price','quantity','subtotal','total','branch_master_id');
                                $this->InvoiceTran->saveAll($arrSaveInvoicTranData,array('fieldList' => $fieldList));
                                unset($arrSaveInvoicTranData);

                                if(count($arrUpdateProductVaraintData) > 0) {
                                    $updateFields['ProductVariantTran.stock_price'] = 0;
                                    $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['ProductVariantTran.branch_master_id'] = $this->request->data['branch_master_id'];
                                    $updateParams['ProductVariantTran.product_master_id'] = array_keys($arrUpdateProductVaraintData);
                                    $this->ProductVariantTran->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $updateFields['ProductVariantTran.stock_price'] = 1;
                                    $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['ProductVariantTran.branch_master_id'] = $this->request->data['branch_master_id'];
                                    $updateParams['ProductVariantTran.id'] = array_values($arrUpdateProductVaraintData);
                                    $updateParams['ProductVariantTran.product_master_id'] = array_keys($arrUpdateProductVaraintData);
                                    $this->ProductVariantTran->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams,$arrUpdateProductVaraintData);
                                }

                                if(count($arrSaveProductVariantStockData) > 0) {
                                    if(!$this->ProductVariantStockTran->saveAll($arrSaveProductVariantStockData)){
                                        throw new Exception(__('Product variant stock record could not updated, please try again later !',true));
                                    }
                                }
                            }

                            if(!empty($arrVoucherDetail)) {
                                $updateFields = $updateParams = array();
                                $updateFields['PurchaseMaster.invoice_master_id'] = $maxInvoiceId;
                                $updateFields['PurchaseMaster.generate_invoice'] = 1;
                                $updateFields['PurchaseMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['PurchaseMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                                $updateParams['PurchaseMaster.id'] = $arrVoucherDetail;
                                $this->PurchaseMaster->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);
                            }
                            unset($arrValidateData,$arrProdctDetail,$arrVoucherDetail);
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('SAVED_INVOICE',true),'id' => $maxInvoiceId);
                        }  catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                        }
                    } else {
                        $validationErrros = Set::flatten($this->InvoiceMaster->validationErrors);
                        $message = $this->AppUtilities->displayMessage($validationErrros);
                        $response = array('status' => 0,'message' => __($message,true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /***\
     * 
     */
    public function save_sale_invoice() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(count($this->request->data)) {
                    #pr($this->request->data);exit;
                    $arrValidateData = array();
                    $arrValidateData['InvoiceMaster'] = $this->request->data;
                    $arrValidateData['InvoiceMaster']['branch_master_id'] = isset($this->request->data['branch_master_id']) ? $this->request->data['branch_master_id'] : '';
                    $arrValidateData['InvoiceMaster']['company_year_master_id'] = isset($this->request->data['company_year_master_id']) ? $this->request->data['company_year_master_id'] : '';
                    $arrProdctDetail = array();
                    if(isset($arrValidateData['InvoiceMaster']['invoice_data']) && !empty($arrValidateData['InvoiceMaster']['invoice_data'])) {
                        $arrInvoice = json_decode($arrValidateData['InvoiceMaster']['invoice_data'],true);
                        $arrProdctDetail = $arrInvoice['product'];
                        $arrValidateData['InvoiceMaster']['unit_price'] = array_column($arrProdctDetail,'price');
                        unset($arrInvoice);
                    }
                    #pr($arrValidateData);exit;
                    $this->InvoiceMaster->set($arrValidateData);
                    if($this->InvoiceMaster->validates()) {
                        unset($arrValidateData);
                        $dataSource = $this->InvoiceMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $firmId = $this->request->data['branch_master_id'];
                            $tableName = 'AutoInvoice'.$firmId.'Code';
                            $arrSaveInvoiceData[$tableName]['name'] = $this->request->data['customer_master_id'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrSaveInvoiceData)) {
                                throw new Exception(__('Auto Invoice code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                            $invoiceDate = $this->AppUtilities->date_format($this->request->data['date'],'yyyy-mm-dd');
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_no'] = $arrCodeData['code'];
                            $arrSaveInvoiceData['InvoiceMaster']['max_invoice_no'] = $arrCodeData['maxNumber'];
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_date'] = $invoiceDate;
                            $arrSaveInvoiceData['InvoiceMaster']['amount'] = $this->request->data['actual_amount'];
                            $arrSaveInvoiceData['InvoiceMaster']['received_date'] = $invoiceDate;
                            $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                            $arrSaveInvoiceData['InvoiceMaster']['customer_master_id'] = $this->request->data['customer_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                            $arrSaveInvoiceData['InvoiceMaster']['is_lock_invoice'] = (isset($this->request->data['is_lock_invoice'])) ? 1 : 0;
                            $arrSaveInvoiceData['InvoiceMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['type'] = 2;
                            if(isset($this->request->data['invoice_data'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['invoice_json'] = $this->request->data['invoice_data'];
                            }

                            if(isset($this->request->data['paylater']) && !empty($this->request->data['paylater'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 0;
                            } else {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                                $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                                $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                                if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                                }
                                if($this->request->data['actual_amount'] > $this->request->data['amount']) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 1;
                                    $arrSaveInvoiceData['InvoiceMaster']['part_amount'] = number_format($this->request->data['amount'],2,'.','');
                                } else {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                                }
                                $history['payment_mode'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode'];
                                $history['bank_master_id'] = $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'];
                                $history['payment_reference_no'] = $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'];
                                $history['payment_status'] = $arrSaveInvoiceData['InvoiceMaster']['payment_status'];
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'])) {
                                    $history['payment_mode_date'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'];
                                }
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['part_amount'])) {
                                    $history['part_amount'] = $arrSaveInvoiceData['InvoiceMaster']['part_amount'];
                                }
                                $arrHistory[] = $history;
                                $arrSaveInvoiceData['InvoiceMaster']['history'] = json_encode($arrHistory);
                                unset($arrHistory);
                            }
                            
                            $fieldList = array('id','invoice_no','max_invoice_no','type','invoice_date','amount','part_amount','history','is_lock_invoice','received_date','payment_status','customer_master_id','payment_mode','bank_master_id','remark','payment_reference_no','payment_mode_date','reference_ids','invoice_json','branch_master_id','company_year_master_id');
                            $this->InvoiceMaster->create();
                            $this->InvoiceMaster->save($arrSaveInvoiceData,array('validate' => false,'fieldList' => $fieldList));
                            #pr($arrSaveInvoiceData);
                            $maxInvoiceId = $this->InvoiceMaster->getInsertID();
                            unset($arrSaveInvoiceData);
                            #pr($arrProdctDetail);exit;
                            if(count($arrProdctDetail) > 0) {
                                $counter = 0;
                                $arrSaveInvoicTranData = array();
                                foreach($arrProdctDetail as $key => $product) {
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['invoice_master_id'] = $maxInvoiceId;
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['reference_tran_id'] = $product['product_variant_tran_id'];
                                    if(isset($product['tax_group_master_id']) && !empty($product['tax_group_master_id'])) {
                                        $arrSaveInvoicTranData[$counter]['InvoiceTran']['tax_group_master_id'] = $product['tax_group_master_id'];
                                    }
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['unit_price'] = number_format($product['price'],2,'.','');
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['quantity'] = (int) $product['quantity'];
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['tax'] = (isset($product['item_tax'])) ? $product['item_tax'] : '0';
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['discount'] = (isset($product['discount'])) ? $product['discount'] : '0';
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['discount_price'] = (isset($product['discount_amount'])) ? $product['discount_amount'] : '0';
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['subtotal'] = number_format($product['subtotal'],2,'.','');
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['total'] = number_format($product['total'],2,'.','');
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                    $arrSaveInvoicTranData[$counter]['InvoiceTran']['tax_json'] = json_encode($product['tax']);
                                    ++$counter;
                                }

                                #pr($arrSaveInvoicTranData);
                                #pr($arrProductConsumptionTranData);exit;
                                $fieldList = array('id','invoice_master_id','reference_tran_id','tax_group_master_id','unit_price','quantity','tax','discount','discount_price','tax_json','subtotal','total','branch_master_id');
                                $this->InvoiceTran->saveAll($arrSaveInvoicTranData,array('fieldList' => $fieldList));
                                unset($arrSaveInvoicTranData,$fieldList);

                                $userId = $this->Session->read('sessUserId');
                                $branchId = $this->request->data['branch_master_id'];
                                $companyYearId = $this->request->data['company_year_master_id'];
                                $date = date('Y-m-d H:i:s');
                                $stockQuery = "INSERT INTO product_variant_stock_trans (`date`,`invoice_master_id`,`product_variant_tran_id`,`old_product_variant_tran_id`,`parent_id`,`quantity`,`type`,`stock_type`,`tran_type`,`from_type`,`branch_master_id`,`company_year_master_id`,`user_id`,`created`,`modified`)
                                                SELECT '".$date."','".$maxInvoiceId."',product_variant_tran_id,product_variant_tran_id,product_variant_stock_tran_id,quantity,1,2,3,2,'".$branchId."','".$companyYearId."','".$userId."',NOW(),NOW()
                                                FROM tmp_product_variant_stock_trans WHERE user_id = '".$userId."'";
                                $this->ProductVariantStockTran->query($stockQuery,false);
                                unset($arrProductConsumptionTranData,$fieldList);
                            }
                            $this->loadModel('TmpProductVariantStockTran');
                            $this->TmpProductVariantStockTran->deleteAll(array('user_id' => $this->Session->read('sessUserId'),'branch_master_id' => $this->request->data['branch_master_id']),false);
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('SAVED_INVOICE',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                        }
                    } else {
                        $validationErrros = Set::flatten($this->InvoiceMaster->validationErrors);
                        $message = $this->AppUtilities->displayMessage($validationErrros);
                        $response = array('status' => 0,'message' => __($message,true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_id' => $branchId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /**
     * This function is used to save vehicle coupon and generate also invoice
     */
    public function save_coupon_invoice() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrValidateData['VehicleCouponMaster'] = $this->request->data;
                $this->VehicleCouponMaster->set($arrValidateData);
                if($this->VehicleCouponMaster->validates()) {
                    unset($arrValidateData);
                    $dataSource = $this->VehicleCouponMaster->getDataSource();
                    try{
                        $dataSource->begin();
                        $arrSaveVehicleCouponData  = $arrSaveVehicleCouponTranData = $arrSaveInvoiceData = $arrSaveInvoiceTranData = array();
                        $arrSaveVehicleCouponData['VehicleCouponMaster']['date'] = $this->AppUtilities->date_format($this->request->data['date'],'yyyy-mm-dd');
                        $arrSaveVehicleCouponData['VehicleCouponMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                        $arrSaveVehicleCouponData['VehicleCouponMaster']['coupon_master_id'] = $this->request->data['coupon_master_id'];
                        $arrSaveVehicleCouponData['VehicleCouponMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                        #pr($arrSaveVehicleCouponData);exit;
                        $this->VehicleCouponMaster->create();
                        $fieldList = array('id','date','vehicle_master_id','coupon_master_id','branch_master_id');
                        if(!$this->VehicleCouponMaster->save($arrSaveVehicleCouponData,array('fieldList' => $fieldList,'validate' => false))) {
                            throw new Exception(__('vehicle_coupon_masters could not saved properly, please try again later!',true));
                        }
                        $maxVehicleCouponId = $this->VehicleCouponMaster->getInsertID();
                        unset($arrSaveVehicleCouponData,$fieldList);
                        if(isset($this->VehicleCouponMaster->arrServiceDetail) && !empty($this->VehicleCouponMaster->arrServiceDetail)) {
                            $currentDate = date('Y-m-d');
                            $count = 0;
                            foreach($this->VehicleCouponMaster->arrServiceDetail as $key => $service) {
                                $date = date('Y-m-d',strtotime('+'.intval($service['month']).' months'.$currentDate));
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['name'] =  $service['name'];
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['date'] =  $date;
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['km'] =  $service['km'];
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['vehicle_coupon_master_id'] =  $maxVehicleCouponId;
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['order_no'] =  $count + 1;
                                $currentDate = $date;
                                ++$count;
                            }
                            #pr($arrSaveVehicleCouponTranData);exit;
                            $fieldList = array('id','name','date','km','order_no','vehicle_coupon_master_id','branch_master_id');
                            if(!$this->VehicleCouponTran->saveAll($arrSaveVehicleCouponTranData,array('fieldList' => $fieldList))) {
                                throw new Exception(__('vehicle_coupon_trans could not saved properly, please try again later!',true));
                            }
                            unset($arrSaveVehicleCouponTranData,$fieldList);
                        }
                        $firmId = $this->request->data['branch_master_id'];
                        $tableName = 'AutoInvoice'.$firmId.'Code';
                        $arrSaveAutoData = array();
                        $arrSaveAutoData[$tableName]['name'] = $this->request->data['date'];
                        $this->loadModel($tableName);
                        $this->$tableName->create();
                        if(!$this->$tableName->save($arrSaveAutoData)) {
                            throw new Exception(__('Auto Invoice code could not saved',true));
                        }
                        unset($arrSaveAutoData);
                        $maxNumber = $this->$tableName->getInsertID();
                        $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                        $arrSaveInvoiceData['InvoiceMaster']['invoice_no'] = $arrCodeData['code'];
                        $arrSaveInvoiceData['InvoiceMaster']['max_invoice_no'] = $arrCodeData['maxNumber'];
                        $arrSaveInvoiceData['InvoiceMaster']['invoice_date'] = $this->AppUtilities->date_format($this->request->data['date'],'yyyy-mm-dd');
                        $arrSaveInvoiceData['InvoiceMaster']['amount'] = $this->request->data['actual_amount'];
                        $arrSaveInvoiceData['InvoiceMaster']['received_date'] = $this->AppUtilities->date_format($this->request->data['date'],'yyyy-mm-dd');
                        $arrSaveInvoiceData['InvoiceMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                        $arrSaveInvoiceData['InvoiceMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                        $arrSaveInvoiceData['InvoiceMaster']['customer_master_id'] = $maxVehicleCouponId;
                        $arrSaveInvoiceData['InvoiceMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                        $arrSaveInvoiceData['InvoiceMaster']['type'] = 4;
                        $arrSaveInvoiceData['InvoiceMaster']['reference_ids'] = json_encode(array($maxVehicleCouponId));
                        if(isset($this->request->data['paylater']) && !empty($this->request->data['paylater'])) {
                            $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 0;
                        } else {
                            $arrSaveInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                            $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                            $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                            if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                            }
                            if($this->request->data['actual_amount'] > $this->request->data['amount']) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 1;
                                $arrSaveInvoiceData['InvoiceMaster']['part_amount'] = number_format($this->request->data['amount'],2,'.','');
                            } else {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                            }
                            $history['payment_mode'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode'];
                            $history['bank_master_id'] = $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'];
                            $history['payment_reference_no'] = $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'];
                            $history['payment_status'] = $arrSaveInvoiceData['InvoiceMaster']['payment_status'];
                            if(isset($arrSaveInvoiceData['InvoiceMaster']['part_amount'])) {
                                $history['part_amount'] = $arrSaveInvoiceData['InvoiceMaster']['part_amount'];
                            }
                            if(isset($arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'])) {
                                $history['payment_mode_date'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'];
                            }
                            $arrHistory[] = $history;
                            $arrSaveInvoiceData['InvoiceMaster']['history'] = json_encode($arrHistory);
                            unset($arrHistory);
                        }
                        if(isset($this->request->data['invoice_json'])) {
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_json'] = $this->request->data['invoice_json'];
                        }
                        #pr($arrSaveInvoiceData);
                        $fieldList = array('id','invoice_no','max_invoice_no','type','invoice_date','amount','part_amount','history','is_lock_invoice','received_date','payment_status','vehicle_master_id','customer_master_id','payment_mode','bank_master_id','remark','payment_reference_no','payment_mode_date','reference_ids','invoice_json','branch_master_id','company_year_master_id');
                        if(!$this->InvoiceMaster->save($arrSaveInvoiceData,array('fieldList' => $fieldList))) {
                            throw new Exception(__('invoice_master table could not saved properly, please try again later!',true));
                        }
                        unset($arrSaveInvoiceData,$fieldList);
                        $maxInvoiceId = $this->InvoiceMaster->getInsertID();
                        $arrSaveInvoiceTranData['InvoiceTran']['invoice_master_id'] = $maxInvoiceId;
                        $arrSaveInvoiceTranData['InvoiceTran']['reference_tran_id'] = $this->request->data['coupon_master_id'];
                        $arrSaveInvoiceTranData['InvoiceTran']['unit_price'] = $this->request->data['actual_amount'];
                        $arrSaveInvoiceTranData['InvoiceTran']['quantity'] = 1;
                        $arrSaveInvoiceTranData['InvoiceTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                        $arrSaveInvoiceTranData['InvoiceTran']['subtotal'] = $this->request->data['actual_amount'];
                        $arrSaveInvoiceTranData['InvoiceTran']['total'] = $this->request->data['actual_amount'];
                        #pr($arrSaveInvoiceTranData);
                        $fieldList = array('id','invoice_master_id','reference_tran_id','unit_price','quantity','subtotal','total','type','branch_master_id');
                        if(!$this->InvoiceTran->save($arrSaveInvoiceTranData,array('fieldList' => $fieldList))) {
                            throw new Exception(__('invoice_trans table could not saved, Please try again later !.',true));
                        }
                        unset($arrSaveInvoiceTranData,$fieldList);
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('SAVED_INVOICE',true));
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleCouponMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_vehicle_invoice() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $this->request->data['vehicle_master_id'] = isset($this->request->data['vehicle_master_id']) ? $this->request->data['vehicle_master_id'] : '';
                $this->request->data['company_year_master_id'] = isset($this->request->data['company_year_master_id']) ? $this->request->data['company_year_master_id'] : '';
                $this->request->data['branch_master_id'] = isset($this->request->data['branch_master_id']) ? $this->request->data['branch_master_id'] : '';
                $arrValidateData['VehicleSaleMaster'] = $this->request->data;
                $this->VehicleSaleMaster->set($arrValidateData);
                if($this->VehicleSaleMaster->validates()) {
                    unset($arrValidateData);
                    #pr($this->request->data);exit;
                    $options = array('fields' => array('purchase_price'),'conditions' => array('status' => 1,'vehicle_status' => 1,'id' => $this->request->data['vehicle_master_id']));
                    $arrVehicleData = $this->VehicleMaster->find('first',$options);
                    if(count($arrVehicleData) > 0) {
                        $dataSource = $this->InvoiceMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $arrSaveVehicleSaleData = $arrSaveInvoiceData = $arrInvoiceCodeData = array();
                            $date = $this->AppUtilities->date_format($this->request->data['invoice_date'],'yyyy-mm-dd');
                            $firmId = $this->request->data['branch_master_id'];
                            $tableName = 'AutoInvoice'.$firmId.'Code';
                            $arrInvoiceCodeData[$tableName]['name'] = $this->request->data['vehicle_master_id'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrInvoiceCodeData)) {
                                throw new Exception(__('Auto Invoice code could not saved',true));
                            }
                            unset($arrInvoiceCodeData);
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_no'] = $arrCodeData['code'];
                            $arrSaveInvoiceData['InvoiceMaster']['max_invoice_no'] = $arrCodeData['maxNumber'];
                            $arrSaveInvoiceData['InvoiceMaster']['received_date'] = $date;
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_date'] = $date;
                            $arrSaveInvoiceData['InvoiceMaster']['type'] = 5;
                            $arrSaveInvoiceData['InvoiceMaster']['amount'] = number_format($this->request->data['actual_amount'],2,'.','');
                            $arrSaveInvoiceData['InvoiceMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['customer_master_id'] = $this->request->data['customer_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['reference_ids'] = json_encode(array($this->request->data['vehicle_master_id']));
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_json'] = (!empty($this->request->data['invoice_json'])) ? $this->request->data['invoice_json'] : "{}";
                            if(isset($this->request->data['paylater']) && !empty($this->request->data['paylater'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 0;
                            } else {
                                if(isset($this->request->data['is_lock_invoice'])) {
                                    $arrSaveInvoiceData['InvoiceMaster']['is_lock_invoice'] = $this->request->data['is_lock_invoice'];
                                }
                                $arrSaveInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                                $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                                $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                                if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                                }
                                if(isset($this->request->data['shortFees']) && $this->request->data['shortFees'] > 0) {
                                    $history['short_fees'] = number_format($this->request->data['shortFees'],2,'.','');
                                    $paymentType = 1;
                                    $arrSaveInvoiceData['InvoiceMaster']['part_amount'] = number_format($this->request->data['amount'],2,'.','');
                                } else {
                                    $paymentType = 2;
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                                }
                                $history['payment_status'] = $paymentType;
                                $history['amount'] = number_format($this->request->data['amount'],2,'.','');
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = $paymentType;
                                $history['payment_mode'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode'];
                                $history['bank_master_id'] = $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'];
                                $history['payment_reference_no'] = $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'];
                                $history['payment_status'] = $arrSaveInvoiceData['InvoiceMaster']['payment_status'];
                                $history['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'])) {
                                    $history['payment_mode_date'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'];
                                }
                                $arrHistory[] = $history;
                                $arrSaveInvoiceData['InvoiceMaster']['history'] = json_encode($arrHistory);
                                unset($arrHistory);
                            }
                            #pr($arrSaveInvoiceData);
                            $fieldList = array('id','invoice_no','max_invoice_no','amount','discount','coupon','type','invoice_date','part_amount','history','is_lock_invoice','received_date','payment_status','vehicle_master_id','customer_master_id','payment_mode','bank_master_id','remark','payment_reference_no','payment_mode_date','reference_ids','invoice_json','branch_master_id','company_year_master_id');
                            $this->InvoiceMaster->create();
                            if(!$this->InvoiceMaster->save($arrSaveInvoiceData,array('validate' => false,'fieldList' => $fieldList))){
                                throw new Exception(__('Vehicle Job invoice could not saved properly, please try again later !.',true));
                            }
                            $maxInvoiceId = $this->InvoiceMaster->getInsertID();
                            unset($arrSaveInvoiceData);

                            $arrSaveVehicleSaleData['VehicleSaleMaster']['date'] = $date;
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['invoice_master_id'] = $maxInvoiceId;
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['customer_master_id'] = $this->request->data['customer_master_id'];
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['guarantee'] = (int)$this->request->data['guarantee'];
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['guarantee_date'] = date('Y-m-d',strtotime('+ '.$this->request->data['guarantee'].' year'));
                            $arrSaveVehicleSaleData['VehicleSaleMaster']['coupon_master_id'] = $this->request->data['coupon_master_id'];
                            #pr($arrSaveVehicleSaleData);
                            $fieldList = array('date','guarantee','guarantee_date','coupon_master_id','invoice_master_id','customer_master_id','vehicle_master_id','branch_master_id','company_year_master_id','title_master_id','first_name','middle_name','last_name','mobile_no','email_id','adhaar_no','pancard_no','address');
                            if(!$this->VehicleSaleMaster->save($arrSaveVehicleSaleData,array('fieldList' => $fieldList))) {
                                throw new Exception(__('Vehicle Sale record could not saved !.',true));
                            }
                            unset($arrSaveVehicleSaleData);

                            if(isset($this->request->data['coupon_master_id']) && !empty($this->request->data['coupon_master_id'])) {
                                $arrSaveVehicleCouponData = array();
                                $arrSaveVehicleCouponData['VehicleCouponMaster']['date'] = $date;
                                $arrSaveVehicleCouponData['VehicleCouponMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                                $arrSaveVehicleCouponData['VehicleCouponMaster']['coupon_master_id'] = $this->request->data['coupon_master_id'];
                                $arrSaveVehicleCouponData['VehicleCouponMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                                $arrSaveVehicleCouponData['VehicleCouponMaster']['type'] = 2;
                                #pr($arrSaveVehicleCouponData);
                                $this->VehicleCouponMaster->create();
                                $fieldList = array('id','date','type','vehicle_master_id','coupon_master_id','branch_master_id');
                                if(!$this->VehicleCouponMaster->save($arrSaveVehicleCouponData,array('fieldList' => $fieldList))) {
                                    throw new Exception(__('vehicle_coupon_masters could not saved properly, please try again later!',true));
                                }
                                $maxVehicleCouponId = $this->VehicleCouponMaster->getInsertID();
                                $arrValidateCouponData['VehicleCouponMaster'] = $this->request->data;
                                $this->VehicleCouponMaster->set($arrValidateCouponData);
                                $this->VehicleCouponMaster->validateCoupon();
                                if(isset($this->VehicleCouponMaster->arrServiceDetail) && !empty($this->VehicleCouponMaster->arrServiceDetail)) {
                                    $currentDate = date('Y-m-d');
                                    $count = 0;
                                    foreach($this->VehicleCouponMaster->arrServiceDetail as $key => $service) {
                                        $date = date('Y-m-d',strtotime('+'.intval($service['month']).' months'.$currentDate));
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['name'] =  $service['name'];
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['date'] =  $date;
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['km'] =  $service['km'];
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['vehicle_coupon_master_id'] =  $maxVehicleCouponId;
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                        $arrSaveVehicleCouponTranData[$count]['VehicleCouponTran']['order_no'] =  $count + 1;
                                        $currentDate = $date;
                                        ++$count;
                                    }
                                    #pr($arrSaveVehicleCouponTranData);
                                    $fieldList = array('id','name','date','km','order_no','vehicle_coupon_master_id','branch_master_id');
                                    if(!$this->VehicleCouponTran->saveAll($arrSaveVehicleCouponTranData,array('fieldList' => $fieldList))) {
                                        throw new Exception(__('vehicle_coupon_trans could not saved properly, please try again later!',true));
                                    }
                                    unset($arrSaveVehicleCouponTranData,$fieldList);
                                }
                            }

                            if(!empty($this->request->data['expense_json'])) {
                                $arrSaveInvoiceTranData = array();
                                $count = 0;
                                $arrExpenseDetail  = json_decode($this->request->data['expense_json'],true);
                                if(count($arrExpenseDetail) > 0) {
                                    foreach($arrExpenseDetail as $key => $expense) {
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['invoice_master_id'] = $maxInvoiceId;
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['reference_tran_id'] = $expense['id'];
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['unit_price'] = number_format($expense['amount'],2,'.','');
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['quantity'] = 1;
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['type'] = 4;
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['subtotal'] = number_format($expense['amount'],2,'.','');
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['total'] = number_format($expense['amount'],2,'.','');
                                        $arrSaveInvoiceTranData[$count]['InvoiceTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                        ++$count;
                                    }
                                    #pr($arrSaveInvoiceTranData);
                                    $fieldList = array('invoice_master_id','reference_tran_id','unit_price','quantity','type','subtotal','total','branch_master_id');
                                    if(!$this->InvoiceTran->saveAll($arrSaveInvoiceTranData,array('fieldList' => $fieldList))) {
                                        throw new Exception(__('Invoice trans could not saved properly, please try again later !.',true));
                                    }
                                    unset($arrSaveInvoiceTranData);
                                }
                            }
                            
                            $arrSaveVehicleData = array();
                            $arrSaveVehicleData['VehicleMaster']['id'] = $this->request->data['vehicle_master_id'];
                            $arrSaveVehicleData['VehicleMaster']['vehicle_status'] = 2;
                            $arrSaveVehicleData['VehicleMaster']['customer_master_id'] = $this->request->data['customer_master_id'];
                            $arrSaveVehicleData['VehicleMaster']['profit'] = number_format($this->request->data['actual_amount'] - $arrVehicleData['VehicleMaster']['purchase_price'],2,'.','');
                            #pr($arrSaveVehicleData);
                            if(!$this->VehicleMaster->save($arrSaveVehicleData,array('validate' => false))) {
                                throw new Exception(__('Invoice trans could not saved properly, please try again later !.',true));
                            }
                            unset($arrSaveVehicleData);
                            $arrChargeDetail = $this->AppUtilities->getRoyaltyChargeDetail($this->request->data['branch_master_id'],1,$this->request->data['actual_amount']);
                            if(count($arrChargeDetail) > 0) {
                                if($arrChargeDetail['amount'] > 0) {
                                    $arrRoyaltyChargeTranData = array();
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['date'] = date('Y-m-d');
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['invoice_master_id'] = $maxInvoiceId;
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['royalty_charge_master_id'] = $arrChargeDetail['id'];
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['amount'] = $arrChargeDetail['amount'];
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['type'] = 1;
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                    $arrRoyaltyChargeTranData['RoyaltyChargeTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                                    #pr($arrRoyaltyChargeTranData);
                                    $this->RoyaltyChargeTran->create();
                                    $fieldList = array('date','invoice_master_id','royalty_charge_master_id','amount','type','branch_master_id','company_year_master_id');
                                    if(!$this->RoyaltyChargeTran->save($arrRoyaltyChargeTranData,array('fieldList' => $fieldList))) {
                                        throw new Exception(__('Member royalty charge could not saved properly, please try again later !.',true));
                                    }
                                }
                            }
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('SAVED_INVOICE',true),'id' => $maxInvoiceId);
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleSaleMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_vehicle_sale_status() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $this->request->data['id'] = (!empty($this->request->data['id'])) ? $this->decryption($this->request->data['id']) : 0;
                $options = array('fields' => array('id','branch_master_id','vehicle_status_json'),'conditions' => array('status' => 1,'invoice_master_id' => $this->request->data['id']));
                $arrVehicleSaleRecords = $this->VehicleSaleMaster->find('first',$options);
                if(count($arrVehicleSaleRecords) > 0) {
                    $dataSource = $this->VehicleSaleMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $arrSaveVehicleSaleRecords = $arrStatusData = array();
                        $vehicleStatusDate = date('Y-m-d H:i:s');
                        $arrVehicleSaleStatusData = array();
                        if(!empty($arrVehicleSaleRecords['VehicleSaleMaster']['vehicle_status_json'])) {
                            $arrVehicleSaleStatusData = json_decode($arrVehicleSaleRecords['VehicleSaleMaster']['vehicle_status_json'],true);
                        } 
                        $arrStatusData['vehicle_status'] = $this->request->data['vehicle_status'];
                        if(isset($this->request->data['estimated_delivery_date'])) {
                            $arrStatusData['estimated_delivery_date'] = $this->AppUtilities->date_format($this->request->data['estimated_delivery_date'],'yyyy-mm-dd');
                        }
                        if(isset($this->request->data['submission_date'])) {
                            $arrStatusData['submission_date'] = $this->AppUtilities->date_format($this->request->data['submission_date'],'yyyy-mm-dd');
                        }
                        $arrStatusData['vehicle_status_date'] = $vehicleStatusDate;
                        if(isset($this->request->data['agent_master_id'])) {
                            $arrStatusData['agent_master_id'] = $this->request->data['agent_master_id'];
                            $arrStatusData['agent_amount'] = $this->request->data['agent_amount'];
                            $arrStatusData['agent_name'] = $this->request->data['agent_name'];
                        }
                        if(isset($this->request->data['remark'])) {
                            $arrStatusData['remark'] = $this->request->data['remark'];
                        }
                        
                        $arrVehicleSaleStatusData[] = $arrStatusData;
                        $arrSaveVehicleSaleRecords['VehicleSaleMaster']['id'] = $arrVehicleSaleRecords['VehicleSaleMaster']['id'];
                        $arrSaveVehicleSaleRecords['VehicleSaleMaster']['vehicle_status'] = $this->request->data['vehicle_status'];
                        $arrSaveVehicleSaleRecords['VehicleSaleMaster']['vehicle_status_date'] = $vehicleStatusDate;
                        if(isset($arrStatusData['estimated_delivery_date'])) {
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['estimated_delivery_date'] = $arrStatusData['estimated_delivery_date'];
                        }
                        if(isset($arrStatusData['submission_date'])) {
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['submission_date'] = $arrStatusData['submission_date'];
                        }
                        $arrSaveVehicleSaleRecords['VehicleSaleMaster']['vehicle_status'] = $this->request->data['vehicle_status'];
                        if(isset($this->request->data['agent_master_id'])) {
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['agent_master_id'] = $this->request->data['agent_master_id'];
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['agent_amount'] = $this->request->data['agent_amount'];
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['agent_name'] = $this->request->data['agent_name'];
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['agent_code'] = 'RDZ000'.$arrVehicleSaleRecords['VehicleSaleMaster']['id'];
                        }
                        if(isset($this->request->data['remark'])) {
                            $arrSaveVehicleSaleRecords['VehicleSaleMaster']['remark'] = $this->request->data['remark'];
                        }
                        $arrSaveVehicleSaleRecords['VehicleSaleMaster']['vehicle_status_json'] = json_encode($arrVehicleSaleStatusData);
                        #pr($arrSaveVehicleSaleRecords);
                        if(!$this->VehicleSaleMaster->saveAll($arrSaveVehicleSaleRecords)) {
                            throw new Exception(__('Vehicle Document could not saved properly, please try again later !.',true));
                        }

                        if(isset($this->request->data['document_master_id']) && !empty($this->request->data['document_master_id'])) {
                            $options = array('fields' => array('document_master_id','id'),'conditions' => array('vehicle_sale_master_id' => $arrVehicleSaleRecords['VehicleSaleMaster']['id'],'status' => 1));
                            $arrVehicleSaleDocumentData = $this->VehicleSaleDocumentTran->find('list',$options);
                            $arrSaveDocumentData = array();
                            $count = 0;
                            foreach($this->request->data['document_master_id'] as $key => $documentId) {
                                if(isset($arrVehicleSaleDocumentData[$documentId])) {
                                    $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['id'] = $arrVehicleSaleDocumentData[$documentId];
                                }
                                if(isset($this->request->data['expiry_date'][$documentId]) && !empty($this->request->data['expiry_date'][$documentId])) {
                                    $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['expiry_date'] = $this->AppUtilities->date_format($this->request->data['expiry_date'][$documentId],'yyyy-mm-dd');
                                }
                                if(isset($this->request->data['document_remark'][$documentId]) && !empty($this->request->data['document_remark'][$documentId])) {
                                    $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['remark'] = $this->request->data['document_remark'][$documentId];
                                }
                                $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['vehicle_sale_master_id'] = $arrVehicleSaleRecords['VehicleSaleMaster']['id'];
                                $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['document_master_id'] = $documentId;
                                $arrSaveDocumentData[$count]['VehicleSaleDocumentTran']['branch_master_id'] = $arrVehicleSaleRecords['VehicleSaleMaster']['branch_master_id'];
                                ++$count;
                            }
                            #pr($arrSaveDocumentData);exit;
                            if(!$this->VehicleSaleDocumentTran->saveAll($arrSaveDocumentData)) {
                                throw new Exception(__('Vehicle Document could not saved properly, please try again later !.',true));
                            }
                        }
                        unset($arrSaveVehicleSaleRecords,$arrSaveDocumentData,$arrVehicleSaleStatusData);
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('Vehicle status is updated successfully !',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],$this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function payment_detail($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $id = $this->decryption(trim($id));
                $options = array('fields' => array('id','amount','part_amount'),'conditions' => array('status' => 1,'id' => $id,'payment_status' => array(0,1)));
                $arrRecords = $this->InvoiceMaster->find('first',$options);
                if(count($arrRecords) > 0) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['InvoiceMaster']);
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_payment() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $this->request->data['id'] = (isset($this->request->data['id']) && !empty($this->request->data['id'])) ? $this->decryption(trim($this->request->data['id'])) : 0;
                $options = array('fields' => array('id','amount','part_amount','history'),'conditions' => array('status' => 1,'id' => $this->request->data['id'],'payment_status !=' => 2));
                $arrRecords = $this->InvoiceMaster->find('first',$options);
                #pr($arrRecords);
                #pr($this->request->data);exit;
                if(count($arrRecords) > 0) {
                    $dataSource = $this->InvoiceMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $arrPaymentDetail = (!empty($arrRecords['InvoiceMaster']['history'])) ? json_decode($arrRecords['InvoiceMaster']['history'],true) : array();
                        $totalAmount = number_format($arrRecords['InvoiceMaster']['amount'],2,'.','');
                        $paid = number_format($this->request->data['paid'] + $arrRecords['InvoiceMaster']['part_amount'],2,'.','');
                        if($totalAmount > $paid) {
                            $paymentType = 1;
                            $partAmount = $paid;
                            $shortFees = number_format($totalAmount - $paid,2,'.','');;
                        } else {
                            $partAmount = 0;
                            $shortFees = 0;
                            $paymentType = 2;
                        }
                        $count = count($arrPaymentDetail);
                        $arrPaymentDetail[$count]['payment_status'] = $paymentType;
                        $arrPaymentDetail[$count]['payment_mode'] = $this->request->data['payment_mode'];
                        $arrPaymentDetail[$count]['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                        $arrPaymentDetail[$count]['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                        $arrPaymentDetail[$count]['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                        $arrPaymentDetail[$count]['amount'] = number_format($this->request->data['paid'],2,'.','');
                        $arrPaymentDetail[$count]['short_fees'] =  $shortFees;
                        $arrPaymentDetail[$count]['payment_date'] = $this->AppUtilities->date_format($this->request->data['payment_date'],'yyyy-mm-dd');
                        if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                            $arrPaymentDetail[$count]['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                        }

                        $arrInvoiceData = array();
                        $arrInvoiceData['InvoiceMaster']['id'] = $arrRecords['InvoiceMaster']['id'];
                        $arrInvoiceData['InvoiceMaster']['payment_status'] = $paymentType;
                        $arrInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                        $arrInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                        $arrInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                        $arrInvoiceData['InvoiceMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                        $arrInvoiceData['InvoiceMaster']['part_amount'] = $partAmount;
                        $arrInvoiceData['InvoiceMaster']['payment_date'] = $this->AppUtilities->date_format($this->request->data['payment_date'],'yyyy-mm-dd');
                        if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                            $arrInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                        }
                        $arrInvoiceData['InvoiceMaster']['history'] = json_encode($arrPaymentDetail);
                        #pr($arrInvoiceData);exit;
                        $this->InvoiceMaster->save($arrInvoiceData,array('validate' => false));
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('payment is added successfully',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],$this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /***
     * This function is used to lock receipt and donot cancelled anyone
     */
    public function lock_invoice($id = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array('fields' => array('id'),'conditions' => array('status' => 1,'id' => $id,'is_lock_invoice' => 0));
                    $arrRecords = $this->InvoiceMaster->find('first',$options);
                    if(count($arrRecords) > 0) {
                        $dataSource = $this->InvoiceMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $updateFields = $updateParams = array();
                            $updateFields['InvoiceMaster.is_lock_invoice'] = 1;
                            $updateFields['InvoiceMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['InvoiceMaster.id'] = $id;
                            $this->InvoiceMaster->updateAll($updateFields,$updateParams);
                            unset($updateFields,$updateParams);
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('Invoice is locked successfully !.',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],$this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('Invoice already locked',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function print_invoice($type = 1,$id = null,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($branchId)) {
                    $id = $this->decryption(trim($id));
                    $type = (int)$type;
                    $joins = $fields = array();
                    if($type === 1) {
                        $fields = array('SM.name','SM.mobile_no','SM.email_id','SM.address');
                        $joins[] =array(
                                        'table' => 'suppliers',
                                        'alias' => 'SM',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.customer_master_id = SM.id','SM.status' => 1)
                                    );
                    } else if($type === 2) {
                        $fields = array('SM.name','SM.mobile_no','SM.email_id','SM.permanent_address','SM.residential_address as address');
                        $joins[] = array(
                                        'table' => 'customers',
                                        'alias' => 'SM',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.customer_master_id = SM.id','SM.status' => 1)
                                    );
                    } else if($type === 3) {
                        $fields = array('VJ.customer_name AS name','VJ.customer_mobile_no AS mobile_no','VJ.customer_email_id AS email_id','VJ.customer_address AS address','VJ.in_house_vehicle','VJ.vehicle_code','VJ.vehicle_number','amount','discount','coupon');
                        $joins[] = array(
                                        'table' => 'vehicle_jobs',
                                        'alias' => 'VJ',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.customer_master_id = VJ.id','VJ.status' => 1)
                                    );
                    } else if($type === 5) {
                        $fields = array("Customer.name as name",'Customer.mobile_no','Customer.email_id','Customer.residential_address AS address');
                        $joins[] = array(
                                        'table' => 'vehicle_sale_masters',
                                        'alias' => 'VSM',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.id = VSM.invoice_master_id','VSM.status' => 1)
                                    );
                        $joins[] = array(
                                        'table' => 'customers',
                                        'alias' => 'Customer',
                                        'type' => 'INNER',
                                        'conditions' => array('VSM.customer_master_id = Customer.id','Customer.status' => 1)
                                    );
                    } else {}
                    $options = array(

                                    'fields' => array_merge(array('id','invoice_no','invoice_date','amount','payment_status','payment_mode','invoice_json'),$fields),
                                    'joins' => $joins,
                                    'conditions' => array('InvoiceMaster.id' => $id,'InvoiceMaster.branch_master_id' => $branchId,'InvoiceMaster.status' => 1)
                                );
                    $arrRecords = $this->InvoiceMaster->find('first',$options);
                    if(count($arrRecords) > 0) {
                        if($type === 1 || $type === 2) {
                            $arrRecords['InvoiceMaster']['name'] = $arrRecords['SM']['name'];
                            $arrRecords['InvoiceMaster']['mobile_no'] = $arrRecords['SM']['mobile_no'];
                            $arrRecords['InvoiceMaster']['email_id'] = $arrRecords['SM']['email_id'];
                            $arrRecords['InvoiceMaster']['address'] = $arrRecords['SM']['address'];
                            if(isset($arrRecords['SM']['permanent_address'])) {
                                $arrRecords['InvoiceMaster']['address2'] = $arrRecords['SM']['permanent_address'];
                            }
                        } else if($type === 3) {
                            $arrRecords['InvoiceMaster']['name'] = $arrRecords['VJ']['name'];
                            $arrRecords['InvoiceMaster']['mobile_no'] = $arrRecords['VJ']['mobile_no'];
                            $arrRecords['InvoiceMaster']['email_id'] = $arrRecords['VJ']['email_id'];
                            $arrRecords['InvoiceMaster']['address'] = $arrRecords['VJ']['address'];
                            $arrRecords['InvoiceMaster']['vehicle_code'] = $arrRecords['VJ']['vehicle_code'];
                            $arrRecords['InvoiceMaster']['vehicle_number'] = $arrRecords['VJ']['vehicle_number'];
                            $arrRecords['InvoiceMaster']['inhouse_vehicle'] = ($arrRecords['VJ']['in_house_vehicle'] == 1) ? 'YES' : 'NO';
                        } else if($type === 5) {
                            $arrRecords['InvoiceMaster']['name'] = $arrRecords['Customer']['name'];
                            $arrRecords['InvoiceMaster']['mobile_no'] = $arrRecords['Customer']['mobile_no'];
                            $arrRecords['InvoiceMaster']['email_id'] = $arrRecords['Customer']['email_id'];
                            $arrRecords['InvoiceMaster']['address'] = $arrRecords['Customer']['address'];
                        } else {}
                        $arrRecords['InvoiceMaster']['inwords'] = $this->AppUtilities->getNumberFormat($arrRecords['InvoiceMaster']['amount']);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['InvoiceMaster']);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /**
     * This function is used to cancel invoice
     * $type default 1 | 1 => purchase, 2 => sales, 3 => job,4 => coupon
     */
    public function cancel_invoice($type = 1,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && !empty($this->request->data['id']) && !empty($branchId)) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('id','id'),'conditions' => array('status'=> 1,'is_lock_invoice' => 1,'id' => $this->request->data['id'],'branch_master_id' => $branchId));
                    $arrRecords = $this->InvoiceMaster->find('list',$options);
                    $arrCancelInvoiceRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrCancelInvoiceRecords) > 0) {
                        $type = (int) $type;
                        $dataSource = $this->InvoiceMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $updateFields = $updateParams = array();
                            $updateFields['InvoiceMaster.status'] = 0;
                            $updateFields['InvoiceMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['InvoiceMaster.id'] = $arrCancelInvoiceRecords;
                            $this->InvoiceMaster->updateAll($updateFields,$updateParams);
                            unset($updateFields,$updateParams);

                            $updateFields = $updateParams = array();
                            $updateFields['InvoiceTran.status'] = 0;
                            $updateFields['InvoiceTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['InvoiceTran.invoice_master_id'] = $arrCancelInvoiceRecords;
                            $this->InvoiceTran->updateAll($updateFields,$updateParams);
                            unset($updateFields,$updateParams);

                            if($type === 1) {
                                /** cancel purchase invoice records **/
                                $options = array(
                                                'fields' => array('id','old_product_variant_tran_id'),
                                                'conditions' => array('status' => 1,'stock_type' => 1,'from_type' => 1,'invoice_master_id' => $arrCancelInvoiceRecords)
                                            );
                                $arrProductVariantStockData = $this->ProductVariantStockTran->find('list',$options);
                                if(!empty($arrProductVariantStockData)) {
                                    $arrSaveProductVariantStockData = array();
                                    $counter = 0;
                                    foreach($arrProductVariantStockData as $id => $variantId) {
                                        $arrSaveProductVariantStockData[$counter]['ProductVariantStockTran']['id'] = $id;
                                        $arrSaveProductVariantStockData[$counter]['ProductVariantStockTran']['invoice_master_id'] = null;
                                        $arrSaveProductVariantStockData[$counter]['ProductVariantStockTran']['type'] = 2;
                                        $arrSaveProductVariantStockData[$counter]['ProductVariantStockTran']['product_variant_tran_id'] = $variantId;
                                        ++$counter;
                                    }
                                    $this->ProductVariantStockTran->saveAll($arrSaveProductVariantStockData);

                                    $options = array(
                                                    'fields' => array('product_master_id','id'),
                                                    'conditions' => array('status' => 1,'id' => array_values($arrProductVariantStockData))
                                                );
                                    $arrProductVariantData = $this->ProductVariantTran->find('list',$options);
                                    if(!empty($arrProductVariantData)) {
                                        $updateFields = $updateParams = array();
                                        $updateFields['ProductVariantTran.stock_price'] = 0;
                                        $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['ProductVariantTran.product_master_id'] = array_keys($arrProductVariantData);
                                        $this->ProductVariantTran->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams);

                                        $updateFields = $updateParams = array();
                                        $updateFields['ProductVariantTran.stock_price'] = 1;
                                        $updateFields['ProductVariantTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['ProductVariantTran.id'] = array_values($arrProductVariantStockData);
                                        $this->ProductVariantTran->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams);
                                    }
                                }
                                $updateFields = $updateParams = array();
                                $updateFields['PurchaseMaster.generate_invoice'] = 0;
                                $updateFields['PurchaseMaster.invoice_master_id'] = null;
                                $updateFields['PurchaseMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['PurchaseMaster.invoice_master_id'] = $arrCancelInvoiceRecords;
                                $this->PurchaseMaster->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);
                            } else if($type === 2) {
                                /** cancel sale invoice records **/
                                $updateFields = $updateParams = array();
                                $updateFields['ProductVariantStockTran.status'] = 0;
                                $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['ProductVariantStockTran.invoice_master_id'] = array_values($arrCancelInvoiceRecords);
                                if(!$this->ProductVariantStockTran->updateAll($updateFields,$updateParams)) {
                                    throw new Exception(__('Product stock could not saved properly !.',true));
                                }
                                unset($updateFields,$updateParams);
                            } else if($type === 3) {
                                /** cancel vehicle job invoice records **/
                                $updateFields = $updateParams = array();
                                $options = array('fields' => array('id','customer_master_id'),'conditions' => array('id' => $arrCancelInvoiceRecords,'branch_master_id' => $branchId));
                                $arrInvoiceRecords = $this->InvoiceMaster->find('list',$options);
                                if(!empty($arrInvoiceRecords)) {
                                    $updateFields['ProductVariantStockTran.invoice_master_id'] = NULL;
                                    $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['ProductVariantStockTran.invoice_master_id'] = $arrCancelInvoiceRecords;
                                    $this->ProductVariantStockTran->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $updateFields['VehicleJobMaster.is_generate_invoice'] = 0;
                                    $updateFields['VehicleJobMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['VehicleJobMaster.id'] = array_values($arrInvoiceRecords);
                                    $this->VehicleJobMaster->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $updateFields['RoyaltyChargeTran.status'] = 0;
                                    $updateFields['RoyaltyChargeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['RoyaltyChargeTran.invoice_master_id'] = array_values($arrCancelInvoiceRecords);
                                    $this->RoyaltyChargeTran->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);
                                    
                                }
                            } else if($type === 4) {
                                /** cancel coupon invoice records **/
                                $options = array('fields' => array('reference_ids'),'conditions' => array('id' => $arrCancelInvoiceRecords,'branch_master_id' => $branchId));
                                $arrInvoiceRecords = $this->InvoiceMaster->find('all',$options);
                                if(count($arrInvoiceRecords) > 0) {
                                    $arrVehicleCodeId = array();
                                    foreach($arrInvoiceRecords as $key => $records) {
                                        if(!empty($records['InvoiceMaster']['reference_ids'])) {
                                            $arrVehicleCodeId = array_merge($arrVehicleCodeId,json_decode($records['InvoiceMaster']['reference_ids'],true));
                                        }
                                    }
                                    if(count($arrVehicleCodeId) > 0) {
                                        $updateFields = $updateParams = array();
                                        $updateFields['VehicleCouponMaster.status'] = 0;
                                        $updateFields['VehicleCouponMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['VehicleCouponMaster.id'] = $arrVehicleCodeId;
                                        $this->VehicleCouponMaster->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams);

                                        $updateFields = $updateParams = array();
                                        $updateFields['VehicleCouponTran.status'] = 0;
                                        $updateFields['VehicleCouponTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['VehicleCouponTran.vehicle_coupon_master_id'] = $arrVehicleCodeId;
                                        $this->VehicleCouponTran->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams);
                                    }
                                }
                            } else if($type === 5) {
                                /** cancel vehicle sale invoice records **/
                                $options = array('fields' => array('id','reference_ids'),'conditions' => array('id' => $arrCancelInvoiceRecords,'branch_master_id' => $branchId));
                                $records = $this->InvoiceMaster->find('all',$options);
                                if(count($records) > 0) {
                                    $arrVehicleId = array();
                                    foreach($records as $key => $records) {
                                        if(!empty($records['InvoiceMaster']['reference_ids'])) {
                                            $arrVehicleId = array_merge($arrVehicleId,json_decode($records['InvoiceMaster']['reference_ids'],true));
                                        }
                                    }
                                    $updateFields = $updateParams = array();
                                    $updateFields['VehicleSaleMaster.status'] = 0;
                                    $updateFields['VehicleSaleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['VehicleSaleMaster.vehicle_master_id'] = $arrVehicleId;
                                    $updateParams['VehicleSaleMaster.invoice_master_id'] = $arrCancelInvoiceRecords;
                                    $this->VehicleSaleMaster->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $updateFields = $updateParams = array();
                                    $updateFields['RoyaltyChargeTran.status'] = 0;
                                    $updateFields['RoyaltyChargeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['RoyaltyChargeTran.invoice_master_id'] = array_values($arrCancelInvoiceRecords);
                                    $this->RoyaltyChargeTran->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $updateFields = $updateParams = array();
                                    $updateFields['VehicleMaster.profit'] = 0;
                                    $updateFields['VehicleMaster.vehicle_status'] = 1;
                                    $updateFields['VehicleMaster.customer_master_id'] = null;
                                    $updateFields['VehicleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                    $updateParams['VehicleMaster.id'] = $arrVehicleId;
                                    $this->VehicleMaster->updateAll($updateFields,$updateParams);
                                    unset($updateFields,$updateParams);

                                    $options = array('fields' => array('id','vehicle_master_id'),'conditions' => array('vehicle_master_id' => $arrVehicleId,'branch_master_id' => $branchId));
                                    $records = $this->VehicleCouponMaster->find('list',$options);
                                    if(!empty($records)) {
                                        $arrVehicleCouponMasterId = array_keys($records);
                                        $updateFields = $updateParams = array();
                                        $updateFields['VehicleCouponMaster.status'] = 0;
                                        $updateFields['VehicleCouponMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['VehicleCouponMaster.id'] = $arrVehicleCouponMasterId;
                                        $this->VehicleCouponMaster->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams);

                                        $updateFields = $updateParams = array();
                                        $updateFields['VehicleCouponTran.status'] = 0;
                                        $updateFields['VehicleCouponTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                        $updateParams['VehicleCouponTran.vehicle_coupon_master_id'] = $arrVehicleCouponMasterId;
                                        $this->VehicleCouponTran->updateAll($updateFields,$updateParams);
                                        unset($updateFields,$updateParams,$arrVehicleCouponMasterId,$records);
                                    }
                                }
                            }else {}
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_INVOICE',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_id' => $branchId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_vehiclejob_invoice() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                    $options = array('fields' => array('id','discount','coupon','vehicle_master_id','job_json','in_house_vehicle'),'conditions' => array('id' => $this->request->data['id'],'is_generate_invoice' => 0,'status' => 1));
                    $arrVehicleJobData = $this->VehicleJobMaster->find('first',$options);
                    #pr($arrVehicleJobData);exit;
                    if(count($arrVehicleJobData) > 0) {
                        $dataSource = $this->VehicleJobMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $firmId = $this->request->data['branch_master_id'];
                            $tableName = 'AutoInvoice'.$firmId.'Code';
                            $arrSaveInvoiceCodeData = array();
                            $arrSaveInvoiceCodeData[$tableName]['name'] = $this->request->data['id'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrSaveInvoiceCodeData)) {
                                throw new Exception(__('Auto Invoice code could not saved',true));
                            }
                            unset($arrSaveInvoiceCodeData);
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                            $invoiceDate = $this->AppUtilities->date_format($this->request->data['invoice_date'],'yyyy-mm-dd');
                            $arrSaveInvoiceData = array();
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_no'] = $arrCodeData['code'];
                            $arrSaveInvoiceData['InvoiceMaster']['max_invoice_no'] = $arrCodeData['maxNumber'];
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_date'] = $invoiceDate;
                            $arrSaveInvoiceData['InvoiceMaster']['received_date'] = $invoiceDate;
                            $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                            $arrSaveInvoiceData['InvoiceMaster']['reference_ids'] = json_encode(array($this->request->data['id']));
                            $arrSaveInvoiceData['InvoiceMaster']['amount'] = $this->request->data['actual_amount'];
                            $arrSaveInvoiceData['InvoiceMaster']['discount'] = isset($this->request->data['total_discount']) ? $this->request->data['total_discount'] : 0;
                            $arrSaveInvoiceData['InvoiceMaster']['coupon'] = isset($this->request->data['coupon']) ? $this->request->data['coupon'] : 0;
                            $arrSaveInvoiceData['InvoiceMaster']['customer_master_id'] = $this->request->data['id'];
                            $arrSaveInvoiceData['InvoiceMaster']['vehicle_master_id'] = $arrVehicleJobData['VehicleJobMaster']['vehicle_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                            $arrSaveInvoiceData['InvoiceMaster']['is_lock_invoice'] = (isset($this->request->data['is_lock_invoice'])) ? 1 : 0;
                            $arrSaveInvoiceData['InvoiceMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrSaveInvoiceData['InvoiceMaster']['type'] = 3;
                            $arrJobDetail = (!empty($arrVehicleJobData['VehicleJobMaster']['job_json'])) ? json_decode($arrVehicleJobData['VehicleJobMaster']['job_json'],true) : array();
                            if(isset($this->request->data['discount_detail']) && !empty($this->request->data['discount_detail'])) {
                                $arrJobDetail['discount_detail'] = $this->request->data['discount_detail'];
                            }
                            $arrSaveInvoiceData['InvoiceMaster']['invoice_json'] = json_encode($arrJobDetail);
                            if(isset($this->request->data['paylater']) && !empty($this->request->data['paylater'])) {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 0;
                            } else {
                                $arrSaveInvoiceData['InvoiceMaster']['payment_mode'] = $this->request->data['payment_mode'];
                                $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                                $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                                if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                                }
                                if(isset($this->request->data['shortFees']) && $this->request->data['shortFees'] > 0) {
                                    $history['short_fees'] = number_format($this->request->data['shortFees'],2,'.','');
                                    $paymentType = 1;
                                    $arrSaveInvoiceData['InvoiceMaster']['part_amount'] = number_format($this->request->data['amount'],2,'.','');
                                } else {
                                    $paymentType = 2;
                                    $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = 2;
                                }
                                $history['payment_status'] = $paymentType;
                                $history['amount'] = number_format($this->request->data['amount'],2,'.','');
                                $arrSaveInvoiceData['InvoiceMaster']['payment_status'] = $paymentType;
                                $history['payment_mode'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode'];
                                $history['bank_master_id'] = $arrSaveInvoiceData['InvoiceMaster']['bank_master_id'];
                                $history['payment_reference_no'] = $arrSaveInvoiceData['InvoiceMaster']['payment_reference_no'];
                                $history['payment_status'] = $arrSaveInvoiceData['InvoiceMaster']['payment_status'];
                                $history['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                                if(isset($arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'])) {
                                    $history['payment_mode_date'] = $arrSaveInvoiceData['InvoiceMaster']['payment_mode_date'];
                                }
                                $arrHistory[] = $history;
                                $arrSaveInvoiceData['InvoiceMaster']['history'] = json_encode($arrHistory);
                                unset($arrHistory);
                            }
                            #pr($arrSaveInvoiceData);exit;
                            $fieldList = array('id','invoice_no','max_invoice_no','amount','discount','coupon','type','invoice_date','part_amount','history','is_lock_invoice','received_date','payment_status','vehicle_master_id','customer_master_id','payment_mode','bank_master_id','remark','payment_reference_no','payment_mode_date','reference_ids','invoice_json','branch_master_id','company_year_master_id');
                            $this->InvoiceMaster->create();
                            if(!$this->InvoiceMaster->save($arrSaveInvoiceData,array('validate' => false,'fieldList' => $fieldList))){
                                throw new Exception(__('Vehicle Job invoice could not saved properly, please try again later !.',true));
                            }
                            #pr($arrSaveInvoiceData);
                            $maxInvoiceId = $this->InvoiceMaster->getInsertID();
                            unset($arrSaveInvoiceData);

                            $arrUpdateVehicleJobData = array();
                            $arrUpdateVehicleJobData['VehicleJobMaster']['is_generate_invoice'] = 1;
                            $arrUpdateVehicleJobData['VehicleJobMaster']['id'] = $this->request->data['id'];
                            if(!$this->VehicleJobMaster->save($arrUpdateVehicleJobData,array('validate' => false))) {
                                throw new Exception(__('Vehicle Job invoice could not saved properly, please try again later !.',true));
                            }

                            $updateFields['ProductVariantStockTran.invoice_master_id'] = $maxInvoiceId;
                            $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['ProductVariantStockTran.reference_master_id'] = $this->request->data['id'];
                            $updateParams['ProductVariantStockTran.from_type'] = 3;
                            $this->ProductVariantStockTran->updateAll($updateFields,$updateParams);
                            unset($updateFields,$updateParams);

                            $query = "INSERT INTO invoice_trans(invoice_master_id,reference_tran_id,tax_group_master_id,unit_price,quantity,tax,discount,discount_price,`type`,tax_json,branch_master_id,created,modified)
                                        SELECT '".$maxInvoiceId."',reference_tran_id,tax_group_master_id,unit_price,quantity,tax,discount,discount_price,`type`,
                                        tax_json,branch_master_id,NOW(),NOW()
                                        FROM vehicle_job_trans
                                        WHERE vehicle_job_trans.`vehicle_job_master_id` = '".$this->request->data['id']."' AND `status` = 1 AND `branch_master_id` = '".$this->request->data['branch_master_id']."'
                                        AND vehicle_job_trans.`product_type` = 1;";
                            $this->InvoiceMaster->query($query,false);

                            if($arrVehicleJobData['VehicleJobMaster']['in_house_vehicle'] == 0) {
                                $arrChargeDetail = $this->AppUtilities->getRoyaltyChargeDetail($this->request->data['branch_master_id'],2,$this->request->data['actual_amount']);
                                if(count($arrChargeDetail) > 0) {
                                    if($arrChargeDetail['amount'] > 0) {
                                        $arrRoyaltyChargeTranData = array();
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['date'] = date('Y-m-d');
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['invoice_master_id'] = $maxInvoiceId;
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['royalty_charge_master_id'] = $arrChargeDetail['id'];
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['amount'] = $arrChargeDetail['amount'];
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['type'] = 2;
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                        $arrRoyaltyChargeTranData['RoyaltyChargeTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                                        $this->RoyaltyChargeTran->create();
                                        $fieldList = array('date','invoice_master_id','royalty_charge_master_id','amount','type','branch_master_id','company_year_master_id');
                                        if(!$this->RoyaltyChargeTran->save($arrRoyaltyChargeTranData,array('fieldList' => $fieldList))) {
                                            throw new Exception(__('Member royalty charge could not saved properly, please try again later !.',true));
                                        }
                                    }
                                }
                            }
                            $dataSource->commit();
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('SAVED_INVOICE',true),'id' => $maxInvoiceId);
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        }
        catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_tmp_stock() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['branch_master_id']) && isset($this->request->data['id']) && isset($this->request->data['quantity']) && isset($this->request->data['company_year_master_id'])) {
                    $this->request->data['trans_type'] = (isset($this->request->data['trans_type'])) ? $this->request->data['trans_type'] : 2;
                    $dataSource = $this->TmpProductVariantStockTran->getDataSource();
                    try {
                        $dataSource->begin();
                        if($this->request->data['trans_type'] == 2) {
                            $options = array(
                                'fields' => array('id','product_stock_tran_id','SUM(quantity) AS quantity'),
                                'conditions' => array('branch_master_id' => $this->request->data['branch_master_id'],'id' => $this->request->data['id']),
                                'group' => array('ProductStockTran.product_stock_tran_id'),
                                'having' => array('quantity > 0'),
                                'order' => array('ProductStockTran.product_stock_tran_id ASC')
                            );
                            $arrProductStockData = $this->ProductStockTran->find('all',$options);
                            if(count($arrProductStockData) > 0) {
                                $quantity = (int) $this->request->data['quantity'];
                                $arrSaveTmpProductStockData = array();
                                $count = 0;
                                foreach($arrProductStockData as $key => $productStock) {
                                    $pstock = (int) $productStock[0]['quantity'];
                                    if($quantity > 0) {
                                        if($quantity > $pstock) {
                                            $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['quantity'] = -$pstock;
                                            $quantity = $quantity - $pstock;
                                        } else {
                                            $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['quantity'] = -$quantity;
                                            $quantity = 0;
                                        }
                                        $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['product_variant_stock_tran_id'] = $productStock['ProductStockTran']['product_stock_tran_id'];
                                        $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['product_variant_tran_id'] = $this->request->data['id'];
                                        $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                        $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                                        $arrSaveTmpProductStockData[$count]['TmpProductVariantStockTran']['user_id'] = $this->Session->read('sessUserId');
                                        ++$count;
                                    } else {
                                        break;
                                    }
                                }
                                $this->TmpProductVariantStockTran->saveAll($arrSaveTmpProductStockData);
                            }
                        } else if($this->request->data['trans_type'] == 1) {
                            $quantity = (int) $this->request->data['quantity'];
                            $arrSaveTmpProductStockData['TmpProductVariantStockTran']['quantity'] = $quantity;
                            $arrSaveTmpProductStockData['TmpProductVariantStockTran']['product_variant_tran_id'] = $this->request->data['id'];
                            $arrSaveTmpProductStockData['TmpProductVariantStockTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                            $arrSaveTmpProductStockData['TmpProductVariantStockTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrSaveTmpProductStockData['TmpProductVariantStockTran']['user_id'] = $this->Session->read('sessUserId');
                            $this->TmpProductVariantStockTran->save($arrSaveTmpProductStockData);
                        } else {}
                        
                        $dataSource->commit();
                        unset($arrSaveTmpProductStockData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('temporary stock added',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export($exportType = 1,$type = 1,$branchId = 0,$companyYearId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($branchId) && !empty($companyYearId)) {
                    $conditions = array('InvoiceMaster.branch_master_id' => $branchId,'InvoiceMaster.company_year_master_id' => $companyYearId,'InvoiceMaster.status' => 1,'InvoiceMaster.type' => $type);
                    $joins = array();
                    $type = (int) $type;
                    if($type === 1) {
                        $headers = array('sno'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Supplier Name','status' => 'Status');
                        $fields = array('SM.name');
                        $joins[] =array(
                                        'table' => 'suppliers',
                                        'alias' => 'SM',
                                        'type' => 'LEFT',
                                        'conditions' => array('InvoiceMaster.customer_master_id = SM.id','SM.status' => 1)
                                    );
                    } else if($type === 2) {
                        $headers = array('sno'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Customer Name','status' => 'Status');
                        $fields = array('SM.name');
                        $joins[] = array(
                                        'table' => 'customers',
                                        'alias' => 'SM',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.customer_master_id = SM.id','SM.status' => 1)
                                    );
                    } else if($type === 3) {
                        $headers = array('sno'=>'S.No','job_code'=>'Job ID','invoice_no' => 'Invoice No','invoice_date'=>'Invoice Date','vehicle_no'=>'Vehicle No','amount' => 'Amount','status' => 'Payment Status');
                        $fields = array('VJM.unique_no','VJM.job_date','VJM.job_time','VJM.vehicle_number','VJM.vehicle_code');
                        $joins[] = array(
                                        'table' => 'vehicle_jobs',
                                        'alias' => 'VJM',
                                        'type' => 'INNER',
                                        'conditions' => array('InvoiceMaster.customer_master_id = VJM.id','VJM.status' => 1)
                                    );
                    } else if($type === 4) {
                        $headers = array('sno'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Vehicle Code','status' => 'Status');
                        $fields = array('InvoiceMaster.invoice_json');
                    } else if($type === 5) {
                        $headers = array('sno'=>'S.No','invoice_no'=>'Invoice No','invoice_date'=>'Invoice Date','amount'=>'Amount','customer' => 'Vehicle Code','status' => 'Status');
                        $fields = array('InvoiceMaster.invoice_json');
                    } else {
                        $fields = array();
                    }
                    $fields = array_merge(array('id','invoice_no','invoice_date','amount','is_lock_invoice','payment_status'),$fields);
                    $tableOptions = array(
                        'fields' => $fields,
                        'joins' => $joins,
                        'conditions' => $conditions,
                        'group' => array('InvoiceMaster.id')
                    );
                    $arrTableData = $this->InvoiceMaster->find('all',$tableOptions);
                    if(count($arrTableData) > 0) {
                        $arrTableColumnValues = array();
                        foreach($arrTableData as $key => $tableDetails) {
                            if($tableDetails['InvoiceMaster']['payment_status'] == 0) {
                                $status = 'Unpaid';
                            } else if($tableDetails['InvoiceMaster']['payment_status'] == 1) {
                                $status = 'Partial Payment';
                            } else {
                                $status = 'Paid';
                            }

                            if(isset($tableDetails['SM']['name'])) {
                                $name = $tableDetails['SM']['name'];
                                $arrTableColumnValues[$key]['customer'] = $name;
                            } else if($type === 4 && isset($tableDetails['InvoiceMaster']['invoice_json']) && !empty($tableDetails['InvoiceMaster']['invoice_json'])) {
                                $arrInvoiceData = json_decode($tableDetails['InvoiceMaster']['invoice_json'],true);
                                $name = $arrInvoiceData['unique_no']."\n".$arrInvoiceData['customer']['vehicle_number'];
                                $arrTableColumnValues[$key]['customer'] = $name;
                            } else if($type === 5) {
                                $records[$key]['customer'] = "<div><b>".$tableDetails['VehicleMaster']['unique_no']."</b></div><div><b>".$tableDetails['VehicleMaster']['vehicle_number']."</b></div>";
                            } else {}

                            $arrTableColumnValues[$key]['sno'] = $key + 1;
                            $arrTableColumnValues[$key]['invoice_no'] = $tableDetails['InvoiceMaster']['invoice_no'];
                            $arrTableColumnValues[$key]['invoice_date'] = $this->AppUtilities->date_format($tableDetails['InvoiceMaster']['invoice_date'],'dd-mm-yyyy');
                            $arrTableColumnValues[$key]['amount'] = $tableDetails['InvoiceMaster']['amount'];
                            $arrTableColumnValues[$key]['status'] = $status;
                        }
                        $statusCode = 200;
                        $response = array('status' => 1,'date' => date('Y-m-d-H:i:s'),'message' => __('RECORD_FETCHED',true),'data' => $arrTableColumnValues,'header' => $headers);
                    } else {
                        $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('type' => $type,'branch_master_id' => $branchId,'company_year_master_id' => $companyYearId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
