<?php
App::uses('AppController','Controller');
class VehicleMastersController extends AppController {
    public $name = 'VehicleMasters';
    public $layout = false;
    public $uses = array('Vehicle','ErrorLog','ManufacturerTran','VehicleGradeTran','VehicleMaster','VehicleImageTran','VehicleSpecificationTran','VehicleCouponTran','VehicleCouponMaster','VehicleCertificateTran','CustomerMaster');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : '-1';
                $conditions = array('Vehicle.vehicle_from' => 1,'Vehicle.branch_master_id' => $firmId);
                $arrVehicleId = array();
                if(isset($this->request->data['vehicle_master_id']) && !empty($this->request->data['vehicle_master_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_master_id'])] = trim($this->request->data['vehicle_master_id']);
                }

                if(isset($this->request->data['vehicle_number_id']) && !empty($this->request->data['vehicle_number_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_number_id'])] = trim($this->request->data['vehicle_number_id']);
                }

                if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                    $conditions['Vehicle.vehicle_model_master_id'] = trim($this->request->data['vehicle_model_master_id']);
                }

                if(isset($this->request->data['vehicle_engine_id']) && !empty($this->request->data['vehicle_engine_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_engine_id'])] = trim($this->request->data['vehicle_engine_id']);
                }

                if(isset($this->request->data['vehicle_chasis_id']) && !empty($this->request->data['vehicle_chasis_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_chasis_id'])] = trim($this->request->data['vehicle_chasis_id']);
                }

                if(!empty($arrVehicleId)) {
                    $conditions['Vehicle.id'] = array_values($arrVehicleId);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('Vehicle.max_unique_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('Vehicle.vehicle_number '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('Vehicle.vehicle_model_name '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('Vehicle.engine_number '.$sortyType);
                                break;
                        default:
                                $orderBy = array('Vehicle.max_unique_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('Vehicle.max_unique_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->Vehicle->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','unique_no','vehicle_number','vehicle_model_name','engine_number','purchase_price','grade','vehicle_status','generate_certificate'),
                                    'conditions' => $conditions,
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->Vehicle->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['Vehicle']['id']);
                        $vehicleCode = $tableDetails['Vehicle']['unique_no'];
                        if($tableDetails['Vehicle']['generate_certificate'] == 1) {
                            $vehicleCode .= '<div class ="certificate">Ridizo Certified</div>';
                        }
                        if($tableDetails['Vehicle']['vehicle_status'] == 2) {
                            $vehicleCode .= '<div class ="sold-out">Sold Out</div>';
                        }
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['unique_no'] = $vehicleCode;
                        $records[$key]['vehicle_number'] = $tableDetails['Vehicle']['vehicle_number'];
                        $records[$key]['model'] = $tableDetails['Vehicle']['vehicle_model_name'];
                        $records[$key]['engine'] = $tableDetails['Vehicle']['engine_number'];
                        $records[$key]['purchase'] = $tableDetails['Vehicle']['purchase_price'];
                        $records[$key]['generate_certificate'] = (int) $tableDetails['Vehicle']['generate_certificate'];
                        $records[$key]['grade'] = '<div class = "grade">'.$tableDetails['Vehicle']['grade'].'</div>';
                        $records[$key]['grade_value'] = strtoupper($tableDetails['Vehicle']['grade']);
                        $records[$key]['is_exists'] = ($tableDetails['Vehicle']['generate_certificate'] == 1 || $tableDetails['Vehicle']['vehicle_status'] == 2) ? 1 : 0;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','unique_no'=>'Vehicle Code','vehicle_number'=>'Vehicle Number','model' => 'Vehicle Model','purchase' => 'Purchase Price','grade_value' =>'Grade');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Excepton $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                usleep(rand(1,100));
                $arrSaveRecords['VehicleMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['VehicleMaster']['id']) && !empty($arrSaveRecords['VehicleMaster']['id'])) {
                    $arrSaveRecords['VehicleMaster']['id'] = $this->decryption($arrSaveRecords['VehicleMaster']['id']);
                }
                #pr($arrSaveRecords);exit;
                $this->VehicleMaster->set($arrSaveRecords);
                $isShowMessage = 0;
                if($this->VehicleMaster->validates()) {
                    #pr($arrSaveRecords['VehicleMaster']);exit;
                    $dataSource = $this->VehicleMaster->getDataSource();
                    try{
                        $dataSource->begin();
                        $arrSpecificationData = $arrVehicleImageData = $arrDeleteSpecificationData = $arrDeleteImageTran = array();
                        $arrVehicleGradeTranId = 0;
                        if(isset($arrSaveRecords['VehicleMaster']['arr_specification']) && count($arrSaveRecords['VehicleMaster']['arr_specification']) > 0) {
                            $arrSpecificationData = $arrSaveRecords['VehicleMaster']['arr_specification'];
                        }
                        if(isset($arrSaveRecords['VehicleMaster']['vehicle_image']) && count($arrSaveRecords['VehicleMaster']['vehicle_image']) >0 ){
                            $arrVehicleImageData = $arrSaveRecords['VehicleMaster']['vehicle_image'];
                        }
                        if(isset($arrSaveRecords['VehicleMaster']['id']) && !empty($arrSaveRecords['VehicleMaster']['id'])) {
                            $vehicleMasterId = $arrSaveRecords['VehicleMaster']['id'];
                            $options = array('fields' => array('vehicle_specification_master_id','id'),'conditions' => array('status' => 1,'vehicle_master_id' =>$vehicleMasterId));
                            $arrVehicleSpecificationTran = $this->VehicleSpecificationTran->find('list',$options);
                            if(!empty($arrVehicleSpecificationTran)) {
                                $arrDeleteSpecificationData = array_diff(array_keys($arrVehicleSpecificationTran),$arrSpecificationData);
                            }
    
                            $options = array('fields' => array('image','id'),'conditions' => array('status' => 1,'vehicle_master_id' => $vehicleMasterId));
                            $arrImageTran = $this->VehicleImageTran->find('list',$options);
                            if(!empty($arrImageTran)) {
                                $arrDeleteImageTran = array_diff(array_keys($arrImageTran),$arrVehicleImageData);
                            }

                            $options = array('fields' => array('id'),'conditions' => array('status' => 1,'is_active' => 1,'vehicle_master_id' => $vehicleMasterId));
                            $arrVehicleGradeData = $this->VehicleGradeTran->find('first',$options);
                            if(!empty($arrVehicleGradeData)) {
                                $arrVehicleGradeTranId = $arrVehicleGradeData['VehicleGradeTran']['id'];
                            }
                        } else {
                            $tableName = 'AutoVehicleCode';
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            $arrVehicleCode['AutoVehicleCode']['name'] = $arrSaveRecords['VehicleMaster']['vehicle_number'];
                            if(!$this->$tableName->save($arrVehicleCode)) {
                                throw new Exception(__('Auto Vehicle Code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrVehicleCodeData = $this->AppUtilities->getVehicleUniqueNo($maxNumber);
                            if(!empty($arrVehicleCodeData)) {
                                $arrSaveRecords['VehicleMaster']['unique_no'] = $arrVehicleCodeData['code'];
                                $arrSaveRecords['VehicleMaster']['max_unique_no'] = $arrVehicleCodeData['maxNumber'];
                            }
                            $options = array('fields' => array('id'),'conditions' => array('unique_no' => $arrSaveRecords['VehicleMaster']['unique_no']));
                            $validMember = $this->VehicleMaster->find('count',$options);
                            if($validMember > 0) {
                                $isShowMessage = 1;
                                throw new Exception(__('Vehicle Code already exists !...',true));
                            }
                            unset($options);
                            $arrSaveRecords['VehicleMaster']['user_id'] = $this->Session->read('sessUserId');
                            $this->VehicleMaster->create();
                        }
                        
                        if($arrSaveRecords['VehicleMaster']['vehicle_from'] == 2) {
                            $arrCustomerDetail = array();
                            $options = array(
                                            'fields' => array('id'),
                                            'conditions' => array('status' => 1,'mobile_no' => trim($arrSaveRecords['VehicleMaster']['owner_mobile_no']),'first_name' => trim($arrSaveRecords['VehicleMaster']['owner_first_name']))
                                        );
                            $arrCustomerData = $this->CustomerMaster->find('first',$options);
                            if(count($arrCustomerData) > 0) {
                                $arrSaveRecords['VehicleMaster']['customer_master_id'] = $arrCustomerData['CustomerMaster']['id'];
                            } else {
                                $fieldList = array('id','title_master_id','first_name','middle_name','last_name','mobile_no','email_id','adhaar_no','pancard_no','residential_address','permanent_address','branch_master_id');
                                $arrSaveCustomerRecords = array();
                                $arrSaveCustomerRecords['CustomerMaster']['title_master_id'] = $arrSaveRecords['VehicleMaster']['owner_title_master_id'];
                                $arrSaveCustomerRecords['CustomerMaster']['first_name'] = $arrSaveRecords['VehicleMaster']['owner_first_name'];
                                $arrSaveCustomerRecords['CustomerMaster']['middle_name'] = $arrSaveRecords['VehicleMaster']['owner_middle_name'];
                                $arrSaveCustomerRecords['CustomerMaster']['last_name'] = $arrSaveRecords['VehicleMaster']['owner_last_name'];
                                $arrSaveCustomerRecords['CustomerMaster']['mobile_no'] = $arrSaveRecords['VehicleMaster']['owner_mobile_no'];
                                $arrSaveCustomerRecords['CustomerMaster']['email_id'] = $arrSaveRecords['VehicleMaster']['owner_email_id'];
                                $arrSaveCustomerRecords['CustomerMaster']['residential_address'] = $arrSaveRecords['VehicleMaster']['owner_address'];
                                $arrSaveCustomerRecords['CustomerMaster']['branch_master_id'] = $arrSaveRecords['VehicleMaster']['branch_master_id'];
                                $this->CustomerMaster->set($arrSaveCustomerRecords);
                                if($this->CustomerMaster->validates()) {
                                    if(!$this->CustomerMaster->save($arrSaveCustomerRecords,array('fieldList' => $fieldList))) {
                                        $isShowMessage = 1;
                                        throw new Exception(__('Record could not saved properly, please try again later!',true));
                                    }
                                    $arrSaveRecords['VehicleMaster']['customer_master_id'] = $this->CustomerMaster->getInsertID();
                                } else {
                                    $validationErrros = Set::flatten($this->CustomerMaster->validationErrors);
                                    $message = $this->AppUtilities->displayMessage($validationErrros);
                                    throw new Exception(__($message,true));
                                }
                                unset($arrSaveCustomerRecords);
                            }
                        }
                        #pr($arrSaveRecords);exit;
                        $arrSaveRecords['VehicleMaster']['registration_date'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleMaster']['registration_date'],'yyyy-mm-dd');
                        if(isset($arrSaveRecords['VehicleMaster']['purchase_date']) && !empty($arrSaveRecords['VehicleMaster']['purchase_date'])) {
                            $arrSaveRecords['VehicleMaster']['purchase_date'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleMaster']['purchase_date'],'yyyy-mm-dd');
                        }
                        $fieldList = array('id','unique_no','max_unique_no','vehicle_type','vehicle_from','purchase_price','purchase_date','company_year_master_id','vehicle_number','registration_date','vehicle_model_master_id','manufacturer_year','engine_number','chasis_number','fuel_type','vehicle_start_method','variant_master_id','other_detail','purchase_from','purchase_address','customer_master_id','owner_title_master_id','owner_first_name','owner_middle_name','owner_last_name','owner_mobile_no','owner_email_id','owner_profession','owner_address','owner_remark','branch_master_id','user_id','owner_country_master_id','owner_state_master_id','owner_city_master_id');
                        if(!$this->VehicleMaster->save($arrSaveRecords,array('fieldList' => $fieldList,'validate' => false))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        
                        if(!isset($arrSaveRecords['VehicleMaster']['id'])) {
                            $vehicleMasterId = $this->VehicleMaster->getInsertID();
                        }
                        
                        if(!empty($arrVehicleGradeTranId)) {
                            $arrSaveVehicleGradeData['VehicleGradeTran']['id'] = $arrVehicleGradeTranId;
                        } else {
                            $this->VehicleGradeTran->create();
                        }

                        if(isset($arrSaveRecords['VehicleMaster']['grade_master_id']) && !empty($arrSaveRecords['VehicleMaster']['grade_master_id'])) {
                            $arrSaveVehicleGradeData['VehicleGradeTran']['vehicle_master_id'] = $vehicleMasterId;
                            $arrSaveVehicleGradeData['VehicleGradeTran']['grade_master_id'] = $arrSaveRecords['VehicleMaster']['grade_master_id'];
                            if(!$this->VehicleGradeTran->save($arrSaveVehicleGradeData)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($arrSaveVehicleGradeData);
                        }
                        
                        if(count($arrSpecificationData) > 0) {
                            $arrVehicleSpecificationData = array();
                            $i = 0;
                            foreach($arrSpecificationData as $key => $specificationId) {
                                if(isset($arrVehicleSpecificationTran[$specificationId])) {
                                    $arrVehicleSpecificationData[$i]['VehicleSpecificationTran']['id'] = $arrVehicleSpecificationTran[$specificationId];
                                }
                                $arrVehicleSpecificationData[$i]['VehicleSpecificationTran']['vehicle_specification_master_id'] = $specificationId;
                                $arrVehicleSpecificationData[$i]['VehicleSpecificationTran']['vehicle_master_id'] = $vehicleMasterId;
                                $arrVehicleSpecificationData[$i]['VehicleSpecificationTran']['branch_master_id'] = $arrSaveRecords['VehicleMaster']['branch_master_id'];
                                $i++;
                            }
                            if(!$this->VehicleSpecificationTran->saveAll($arrVehicleSpecificationData)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                        }
                        /** delete specification data when never used **/
                        if(count($arrDeleteSpecificationData) > 0) {
                            $updateFields['VehicleSpecificationTran.status'] = 0;
                            $updateFields['VehicleSpecificationTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['VehicleSpecificationTran.vehicle_specification_master_id'] = $arrDeleteSpecificationData;
                            $updateParams['VehicleSpecificationTran.branch_master_id'] = $arrSaveRecords['VehicleMaster']['branch_master_id'];
                            if(!$this->VehicleSpecificationTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }
                        
                        $arrVehicleRecords = $this->VehicleMaster->validateSoldVehicle($arrSaveRecords['VehicleMaster']['vehicle_number']);
                        if(count($arrVehicleRecords) > 0) {
                            $updateFields = $updateParams = array();
                            $updateFields['VehicleMaster.is_active'] = 0;
                            $updateFields['VehicleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['VehicleMaster.id'] = $arrVehicleRecords['VehicleMaster']['id'];
                            if(!$this->VehicleMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }

                        if(count($arrVehicleImageData) >0 ) {
                            $arrVehicleImageTran = array();
                            $i = 0;
                            foreach($arrVehicleImageData as $key => $imageName) {
                                if(!empty($imageName)) {
                                    if(isset($arrImageTran[$imageName])) {
                                        $arrVehicleImageTran[$i]['VehicleImageTran']['id'] = $arrImageTran[$imageName];
                                    }
                                    $arrVehicleImageTran[$i]['VehicleImageTran']['image'] = $imageName;
                                    $arrVehicleImageTran[$i]['VehicleImageTran']['vehicle_master_id'] = $vehicleMasterId;
                                    $arrVehicleImageTran[$i]['VehicleImageTran']['branch_master_id'] = $arrSaveRecords['VehicleMaster']['branch_master_id'];
                                    $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$imageName;
                                    if(@file_exists($tmpPath)) {
                                        $saveImagePath = Configure::read('UPLOAD_VEHICLE_IMAGE_DIR').'/'.$imageName;
                                        @copy($tmpPath,$saveImagePath);
                                        @unlink($tmpPath);
                                    }
                                    ++$i;
                                }
                            }
                            if(!$this->VehicleImageTran->saveAll($arrVehicleImageTran)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                        }
    
                        /** delete vehicle image data when never used **/
                        if(count($arrDeleteImageTran) > 0) {
                            $updateFields['VehicleImageTran.status'] = 0;
                            $updateFields['VehicleImageTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['VehicleImageTran.image'] = $arrDeleteImageTran;
                            $updateParams['VehicleImageTran.branch_master_id'] = $arrSaveRecords['VehicleMaster']['branch_master_id'];
                            if(!$this->VehicleImageTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }
    
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true),'id' => $vehicleMasterId,'vehicle_code' => $arrSaveRecords['VehicleMaster']['unique_no']);
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        if($isShowMessage == 1) {
                            $response = array('status' => 0,'message' => __($e->getMessage(),true));
                        } else {
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_certificate() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                    #pr($this->request->data);exit;
                    $this->request->data['id'] = $this->decryption(trim($this->request->data['id']));
                    $options = array('fields' => array('id'),'conditions' => array('id' => $this->request->data['id'],'status' => 1,'generate_certificate' => 0));
                    $arrVehicleData = $this->VehicleMaster->find('first',$options);
                    if(count($arrVehicleData) > 0) {
                        $dataSource = $this->VehicleMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $arrSaveVehicleCertificateData = $arrSaveVehicleData = $arrSaveVehicleGradeData = array();

                            $this->VehicleCertificateTran->create();
                            $arrSaveVehicleCertificateData['VehicleCertificateTran']['date'] = date('Y-m-d');
                            $arrSaveVehicleCertificateData['VehicleCertificateTran']['vehicle_master_id'] = $this->request->data['id'];
                            if(isset($this->request->data['coupon_master_id'])) {
                                $arrSaveVehicleCertificateData['VehicleCertificateTran']['coupon_master_id'] = $this->request->data['coupon_master_id'];
                            }
                            $arrSaveVehicleCertificateData['VehicleCertificateTran']['grade_master_id'] = $this->request->data['grade_master_id'];
                            $arrSaveVehicleCertificateData['VehicleCertificateTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                            #pr($arrSaveVehicleCertificateData);exit;
                            $fieldList = array('id','date','vehicle_master_id','coupon_master_id','grade_master_id','branch_master_id');
                            if(!$this->VehicleCertificateTran->save($arrSaveVehicleCertificateData,array('fieldList' => $fieldList))) {
                                throw new Exception(__('Vehicle certificate record could not saved properly, please try again later!',true));
                            }
                            unset($arrSaveVehicleCertificateData,$fieldList);
                            
                            $updateFields['VehicleGradeTran.is_active'] = 0;
                            $updateFields['VehicleGradeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleGradeTran.vehicle_master_id'] = $this->request->data['id'];
                            if(!$this->VehicleGradeTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);

                            $this->VehicleGradeTran->create();
                            $arrSaveVehicleGradeData['VehicleGradeTran']['vehicle_master_id'] = $this->request->data['id'];
                            $arrSaveVehicleGradeData['VehicleGradeTran']['grade_master_id'] = $this->request->data['grade_master_id'];
                            #pr($arrSaveVehicleGradeData);
                            if(!$this->VehicleGradeTran->save($arrSaveVehicleGradeData)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }

                            $arrSaveVehicleData['VehicleMaster']['generate_certificate'] = 1;
                            $arrSaveVehicleData['VehicleMaster']['id'] = $this->request->data['id'];
                            #pr($arrSaveVehicleData);exit;
                            if(!$this->VehicleMaster->save($arrSaveVehicleData,array('validate' =>false))) {
                                throw new Exception(__('Vehicle record could not saved properly, please try again later!',true));
                            }
                            unset($arrSaveVehicleData);
                            $dataSource->commit();
                            $statusCode =200;
                            $response = array('status' => 1,'message' => __('SAVED_CERTIFICATE',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete_certificate($id = 0,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($branchId)) {
                    $id = $this->decryption(trim($id));
                    $this->loadModel('VehicleCertificateTran');
                    $options = array('fields' => array('vehicle_master_id','grade_master_id','coupon_master_id'),'conditions' => array('status' => 1,'vehicle_master_id' => $id,'branch_master_id' => $branchId));
                    $arrVehicleCertificateData = $this->VehicleCertificateTran->find('first',$options);
                    if(count($arrVehicleCertificateData) > 0) {
                        $dataSource = $this->VehicleCertificateTran->getDataSource();
                        try {
                            $dataSource->begin();
                            $updateFields['VehicleCertificateTran.status'] = 0;
                            $updateFields['VehicleCertificateTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleCertificateTran.vehicle_master_id'] = $id;
                            if(!$this->VehicleCertificateTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);

                            $updateFields['VehicleGradeTran.status'] = 0;
                            $updateFields['VehicleGradeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleGradeTran.vehicle_master_id'] = $arrVehicleCertificateData['VehicleCertificateTran']['vehicle_master_id'];
                            $updateParams['VehicleGradeTran.grade_master_id'] = $arrVehicleCertificateData['VehicleCertificateTran']['grade_master_id'];
                            if(!$this->VehicleGradeTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);

                            $updateFields['VehicleGradeTran.is_active'] = 1;
                            $updateFields['VehicleGradeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleGradeTran.vehicle_master_id'] = $arrVehicleCertificateData['VehicleCertificateTran']['vehicle_master_id'];
                            $updateParams['VehicleGradeTran.status'] = 1;
                            if(!$this->VehicleGradeTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);

                            $updateFields['VehicleMaster.generate_certificate'] = 0;
                            $updateFields['VehicleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleMaster.id'] = $arrVehicleCertificateData['VehicleCertificateTran']['vehicle_master_id'];
                            if(!$this->VehicleMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $dataSource->commit();
                            $statusCode =200;
                            $response = array('status' => 1,'message' => __('DELETED_CERTIFICATE',true));
                        }  catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_master_id' => $branchId),'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_master_id' => $branchId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    /**
     * $type 1 for edit ,2 => view , 3 => view sale form
     */
    public function record($firmId = 0,$id = 0,$type = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $type = (int) $type;
                    try{
                        $id = $this->decryption(trim($id));
                        if($type === 1) {
                            $options = array(
                                'fields' => array(
                                    'id','unique_no','max_unique_no','vehicle_number','purchase_price','purchase_date','vehicle_type','vehicle_model_master_id','manufacturer_year','engine_number','chasis_number','fuel_type','vehicle_start_method','variant_master_id','registration_date',
                                    'purchase_from','purchase_address','owner_title_master_id AS title_master_id','owner_first_name','owner_middle_name','owner_last_name','owner_mobile_no','owner_email_id','owner_profession','owner_address','other_detail','grade_master_id',
                                    'owner_remark','owner_country_master_id AS country_master_id','owner_state_master_id AS state_master_id','owner_city_master_id AS city_master_id','manufacturer_tran_id','manufacturer_name','vehicle_type_name','variant_name','vehicle_model_name','vehicle_type_id as vehicle_type_master_id'
                                ),
                                'conditions' => array('Vehicle.status' => 1,'Vehicle.vehicle_status' => 1,'Vehicle.vehicle_type' => array(2,3),'Vehicle.id' => $id,'Vehicle.branch_master_id' => $firmId),
                                'recursive' => -1
                            );
                            $arrRecords = $this->Vehicle->find('first',$options);
                            if(count($arrRecords) > 0) {
                                $arrRecords['Vehicle']['id'] = $this->encryption($arrRecords['Vehicle']['id']);
                                $statusCode = 200;
                                $options = array('fields' => array('vehicle_specification_master_id','id'),'conditions' => array('vehicle_master_id' => $id,'status' => 1,'branch_master_id' => $firmId));
                                $arrVehicleSpecificationData = $this->VehicleSpecificationTran->find('list',$options);
                                $options = array('fields' => array('id','image'),'conditions' => array('vehicle_master_id' => $id,'status' => 1,'branch_master_id' => $firmId));
                                $arrVehicleImageData = $this->VehicleImageTran->find('all',$options);
                                $arrVehicleImageData = Hash::combine($arrVehicleImageData,'{n}.VehicleImageTran.id','{n}.VehicleImageTran');
                                $arrRecords['Vehicle']['registration_date'] = $this->AppUtilities->date_format($arrRecords['Vehicle']['registration_date'],'dd-mm-yyyy');
                                $arrRecords['Vehicle']['specification'] = $arrVehicleSpecificationData;
                                $arrImages = (count($arrVehicleImageData) > 0) ? array_values($arrVehicleImageData) : array();
                                $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['Vehicle'],'specification' => $arrVehicleSpecificationData,'image' => $arrImages);
                            } else {
                                $response = array('status' => 0,'message' => __('NO_RECORD',true));
                            }
                        } else if($type === 2) {
                            $options = array(
                                'fields' =>array(
                                    'id','unique_no','max_unique_no','vehicle_number','purchase_price','purchase_date','vehicle_model_master_id','manufacturer_year','engine_number','chasis_number','fuel_type','vehicle_start_method','variant_master_id',
                                    'purchase_from','purchase_address','owner_title_master_id','owner_first_name','owner_middle_name','owner_last_name','owner_mobile_no','owner_email_id','owner_profession','owner_address',
                                    'owner_remark','owner_country_master_id','owner_state_master_id','owner_city_master_id','manufacturer_tran_id','manufacturer_name','vehicle_type_name','variant_name','vehicle_model_name','vehicle_type_id as vehicle_type_master_id',
                                    'CountryMaster.name','StateMaster.name','CityMaster.name','registration_date','other_detail','grade','vehicle_type'
                                ),
                                'joins' => array(
                                    array(
                                        'table' => 'country_masters',
                                        'alias' =>'CountryMaster',
                                        'type' =>'LEFT',
                                        'conditions' => array('Vehicle.owner_country_master_id = CountryMaster.id','CountryMaster.status' => 1)
                                    ),
                                    array(
                                        'table' => 'state_masters',
                                        'alias' =>'StateMaster',
                                        'type' =>'LEFT',
                                        'conditions' => array('Vehicle.owner_state_master_id = StateMaster.id','StateMaster.status' => 1)
                                    ),
                                    array(
                                        'table' => 'city_masters',
                                        'alias' =>'CityMaster',
                                        'type' =>'LEFT',
                                        'conditions' => array('Vehicle.owner_city_master_id = CityMaster.id','CityMaster.status' => 1)
                                    )
                                ),
                                'conditions' => array('Vehicle.status' => 1,'Vehicle.vehicle_type' => array(2,3),'Vehicle.id' => $id,'Vehicle.branch_master_id' => $firmId),
                                'recursive' => -1
                            );
                            $arrRecords = $this->Vehicle->find('first',$options);
                            if(count($arrRecords) > 0) {
                                $statusCode = 200;
                                $arrRecords['Vehicle']['id'] = $this->encryption($arrRecords['Vehicle']['id']);
                                $arrRecords['Vehicle']['country'] = $arrRecords['CountryMaster']['name'];
                                $arrRecords['Vehicle']['state'] = $arrRecords['StateMaster']['name'];
                                $arrRecords['Vehicle']['city'] = $arrRecords['CityMaster']['name'];
                                $arrRecords['Vehicle']['registration_date'] = $this->AppUtilities->date_format($arrRecords['Vehicle']['registration_date'],'dd-mm-yyyy');
                                $arrRecords['Vehicle']['purchase_date'] = $this->AppUtilities->date_format($arrRecords['Vehicle']['purchase_date'],'dd-mm-yyyy');
                                $options = array(
                                                    'fields' => array('VehicleSpecificationMaster.id','VehicleSpecificationMaster.name'),
                                                    'joins' => array(
                                                        array(
                                                            'table' => 'vehicle_specification_masters',
                                                            'alias' =>'VehicleSpecificationMaster',
                                                            'type' =>'INNER',
                                                            'conditions' => array('VehicleSpecificationTran.vehicle_specification_master_id = VehicleSpecificationMaster.id','VehicleSpecificationMaster.status' => 1)
                                                        )
                                                    ),
                                                    'conditions' => array('VehicleSpecificationTran.vehicle_master_id' => $id,'VehicleSpecificationTran.status' => 1)
                                                );
                                $arrVehicleSpecificationData = $this->VehicleSpecificationTran->find('all',$options);
                                $arrVehicleSpecificationData = Hash::combine($arrVehicleSpecificationData,'{n}.VehicleSpecificationMaster.id','{n}.VehicleSpecificationMaster');
                                $options = array('fields' => array('id','image'),'conditions' => array('vehicle_master_id' => $id,'status' => 1));
                                $arrVehicleImageData = $this->VehicleImageTran->find('list',$options);
                                $arrRecords['Vehicle']['specification'] = $arrVehicleSpecificationData;
                                $arrRecords['Vehicle']['images'] = (count($arrVehicleImageData) > 0) ? array_values($arrVehicleImageData) : array();
                                $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['Vehicle']);
                            } else {
                                $response = array('status' => 0,'message' => __('NO_RECORD',true));
                            }
                        } else if($type === 3) {
                            $options = array(
                                'fields' => array('id','unique_no','max_unique_no','vehicle_number','sale_price','purchase_price','SUM(EM.amount) AS expense'),
                                'joins' => array(
                                    array(
                                        'table' => 'expense_masters',
                                        'alias' => 'EM',
                                        'type' => 'LEFT',
                                        'conditions' => array('Vehicle.id = EM.vehicle_master_id','EM.status' => 1,'EM.expense_type' => 2)
                                    )
                                ),
                                'conditions' => array('Vehicle.status' => 1,'Vehicle.vehicle_status' => 1,'Vehicle.vehicle_type' => array(2,3),'Vehicle.id' => $id,'Vehicle.branch_master_id' => $firmId),
                                'recursive' => -1
                            );
                            $arrRecords = $this->Vehicle->find('first',$options);
                            if(count($arrRecords) > 0) {
                                $arrRecords['Vehicle']['id'] = $this->encryption($arrRecords['Vehicle']['id']);
                                $arrRecords['Vehicle']['expense'] = (float) $arrRecords[0]['expense'];
                                $arrRecords['Vehicle']['purchase_price'] = (float) $arrRecords['Vehicle']['purchase_price'];
                                $arrRecords['Vehicle']['previous_sale_price'] = (float) $arrRecords['Vehicle']['sale_price'];
                                $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['Vehicle']);
                            } else {
                                $response = array('status' => 0,'message' => __('NO_RECORD',true));
                            }
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } catch(Exception $e) {
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'branch_master_id' => $firmId),'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_vehicle_sale() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && isset($this->request->data['sale_price']) && $this->request->data['sale_price'] > 0 && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                    $options = array('fields' => array('id'),'conditions' => array('id' => $this->request->data['id'],'status' => 1));
                    $arrRecords = $this->VehicleMaster->find('first',$options);
                    if(count($arrRecords) > 0) {
                        $dataSource = $this->VehicleMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['VehicleMaster.sale_price'] = number_format($this->request->data['sale_price'],2,'.','');
                            $updateFields['VehicleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleMaster.id'] = $this->request->data['id'];
                            if(!$this->VehicleMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('SALE_PRICE_ADDED',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->VehicleMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['VehicleMaster.status'] = 0;
                        $updateFields['VehicleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['VehicleMaster.id'] = $this->request->data['id'];
                        if(!$this->VehicleMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        unset($updateParams,$updateFields);
                        $updateFields['VehicleSpecificationTran.status'] = 0;
                        $updateFields['VehicleSpecificationTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['VehicleSpecificationTran.vehicle_master_id'] = $this->request->data['id'];
                        $updateParams['VehicleSpecificationTran.status != '] = 0;
                        if(!$this->VehicleSpecificationTran->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        unset($updateParams,$updateFields);
                        $updateFields['VehicleImageTran.status'] = 0;
                        $updateFields['VehicleImageTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['VehicleImageTran.vehicle_master_id'] = $this->request->data['id'];
                        $updateParams['VehicleImageTran.status != '] = 0;
                        if(!$this->VehicleImageTran->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
