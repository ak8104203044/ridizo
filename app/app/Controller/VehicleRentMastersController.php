<?php
App::uses('AppController','Controller');
class VehicleRentMastersController extends AppController {
    public $name = 'VehicleRentMasters';
    public $layout = false;
    public $uses = array('VehicleRentMaster','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $conditions = array('VehicleRentMaster.status' => 1,'VehicleRentMaster.branch_master_id' => $firmId,'VehicleRentMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['booking_date']) && !empty($this->request->data['booking_date'])) {
                    $conditions['VehicleRentMaster.booking_date'] = $this->AppUtilities->date_format(trim($this->request->data['booking_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['vehicle_master_id']) && !empty($this->request->data['vehicle_master_id'])) {
                    $conditions['VehicleRentMaster.vehicle_master_id'] = trim($this->request->data['vehicle_master_id']);
                }

                if(isset($this->request->data['first_name']) && !empty($this->request->data['first_name'])) {
                    $conditions['VehicleRentMaster.first_name LIKE'] = '%'.trim($this->request->data['first_name']).'%';
                }

                if(isset($this->request->data['last_name']) && !empty($this->request->data['last_name'])) {
                    $conditions['VehicleRentMaster.last_name LIKE'] = '%'.trim($this->request->data['last_name']).'%';
                }

                if(isset($this->request->data['status']) && !empty($this->request->data['status'])) {
                    $conditions['VehicleRentMaster.rent_status'] = intval($this->request->data['status']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('VehicleRentMaster.booking_date '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('VM.vehicle_number '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('VehicleRentMaster.first_name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('VehicleRentMaster.booking_date '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('VehicleRentMaster.booking_date ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'vehicle_masters',
                                                    'alias' => 'VM',
                                                    'type' => 'INNER',
                                                    'conditions' => array('VehicleRentMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                                )
                                            ),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->VehicleRentMaster->find('count',$tableCountOptions);
                    $displayLength = isset($this->request->data['length'])?intval($this->request->data['length']):0;
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','booking_date','booking_time','rent_status','security_amount','CM.name','VM.unique_no','VM.vehicle_number'),
                                    'joins' => array(
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'INNER',
                                            'conditions' => array('VehicleRentMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                        ),
                                        array(
                                            'table' => 'customers',
                                            'alias' => 'CM',
                                            'type' => 'INNER',
                                            'conditions' => array('VehicleRentMaster.customer_master_id = CM.id','CM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'VehicleRentMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->VehicleRentMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['VehicleRentMaster']['id']);
                        $bookingDate = date('d-M-Y h:i:s A',strtotime($tableDetails['VehicleRentMaster']['booking_date'].' '.$tableDetails['VehicleRentMaster']['booking_time']));
                        $bstatus = '';
                        if($tableDetails['VehicleRentMaster']['rent_status'] == 1) {
                            $bstatus = 'OPEN';
                            $rentStatus = '<div class="btn btn-xs btn-warning">Open</div>';
                        } else {
                            $bstatus = 'COMPLETED';
                            $rentStatus = '<div class="btn btn-xs green">Completed</div>';
                        }
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['vehicle_no'] = $tableDetails['VM']['vehicle_number'];
                        $records[$key]['booking_date'] = $bookingDate;
                        $records[$key]['customer'] = $tableDetails['CM']['name'];
                        $records[$key]['amount'] = $tableDetails['VehicleRentMaster']['security_amount'];
                        $records[$key]['status'] = $rentStatus;
                        $records[$key]['rent_status'] = $bstatus;
                        $records[$key]['is_exists'] = ((int)$tableDetails['VehicleRentMaster']['rent_status'] === 1) ? 0 : 1;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','booking_date'=>'Booking Date','vehicle_no'=>'Vehicle Number','customer'=>'Customer','amount' => 'Security Amount','rent_status' => 'Status');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['VehicleRentMaster'] = $this->request->data;
                if(isset($arrSaveRecords['VehicleRentMaster']['id']) && !empty($arrSaveRecords['VehicleRentMaster']['id'])) {
                    $arrSaveRecords['VehicleRentMaster']['id'] = $this->decryption($arrSaveRecords['VehicleRentMaster']['id']);
                }
                $this->VehicleRentMaster->set($arrSaveRecords);
                if($this->VehicleRentMaster->validates()) {
                    $dataSource = $this->VehicleRentMaster->getDataSource();
                    $fieldList = array('id','booking_date','booking_time','estimated_return_date','estimated_return_time','security_amount','estimate_rent_amount','customer_master_id','vehicle_master_id','company_year_master_id','branch_master_id');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['VehicleRentMaster']['id']) || empty($arrSaveRecords['VehicleRentMaster']['id'])) {
                            $this->VehicleRentMaster->create();
                        }
                        $arrSaveRecords['VehicleRentMaster']['booking_date'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleRentMaster']['booking_date'],'yyyy-mm-dd');
                        $arrSaveRecords['VehicleRentMaster']['booking_time'] = date('H:i:s',strtotime($arrSaveRecords['VehicleRentMaster']['booking_time']));
                        $arrSaveRecords['VehicleRentMaster']['estimated_return_date'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleRentMaster']['estimated_return_date'],'yyyy-mm-dd');
                        $arrSaveRecords['VehicleRentMaster']['estimated_return_time'] = date('H:i:s',strtotime($arrSaveRecords['VehicleRentMaster']['estimated_return_time']));
                        #pr($arrSaveRecords);exit;
                        if(!$this->VehicleRentMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleRentMaster']['id']) && !empty($arrSaveRecords['VehicleRentMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                        unset($this->request->data);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleRentMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_status() {
        $response = array('status' => 0,'message' => 'Invalid response');
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $this->request->data['id'] = isset($this->request->data['id']) ? $this->decryption(trim($this->request->data['id'])) : '';
                $options = array('fields' => array('id'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                $records = $this->VehicleRentMaster->find('first',$options);
                if(count($records) > 0) {
                    $dataSource = $this->VehicleRentMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $arrSaveRecords = array();
                        $arrSaveRecords['VehicleRentMaster']['id'] = $this->request->data['id'];
                        $returnDate = $this->AppUtilities->date_format($this->request->data['return_date'],'yyyy-mm-dd').' '.date('H:i:s',strtotime($this->request->data['return_time']));
                        $arrSaveRecords['VehicleRentMaster']['return_date'] = $returnDate;
                        $arrSaveRecords['VehicleRentMaster']['rent_status'] = 2;
                        #pr($arrSaveRecords);exit;
                        if(!$this->VehicleRentMaster->save($arrSaveRecords,array('validate' => false))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($arrSaveRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('Status is updated successfully!.',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'save_status','request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'save_status','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    public function record($id = null,$firmId = 0,$companyYearId = 0) {
        $response = array('status' => 0,'message' => 'Invalid response');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId) && !empty($companyYearId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('id','booking_date','booking_time','estimated_return_date','estimated_return_time','customer_master_id','security_amount','estimate_rent_amount','customer_master_id','CM.name','CM.mobile_no','CM.email_id','CM.residential_address','CM.permanent_address','vehicle_master_id','VM.unique_no','VM.vehicle_number'),
                            'joins' => array(
                                array(
                                    'table' => 'vehicle_masters',
                                    'alias' => 'VM',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleRentMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                ),
                                array(
                                    'table' => 'customers',
                                    'alias' => 'CM',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleRentMaster.customer_master_id = CM.id','CM.status' => 1)
                                )
                            ),
                            'conditions' => array('VehicleRentMaster.status' => 1,'VehicleRentMaster.id' => $id,'VehicleRentMaster.branch_master_id' => $firmId,'VehicleRentMaster.company_year_master_id' => $companyYearId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->VehicleRentMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            $arrRecords['VehicleRentMaster']['customer'] = $arrRecords['CM']['name'];
                            $arrRecords['VehicleRentMaster']['email_id'] = $arrRecords['CM']['email_id'];
                            $arrRecords['VehicleRentMaster']['mobile_no'] = $arrRecords['CM']['mobile_no'];
                            $arrRecords['VehicleRentMaster']['add1'] = $arrRecords['CM']['residential_address'];
                            $arrRecords['VehicleRentMaster']['add2'] = $arrRecords['CM']['permanent_address'];
                            $arrRecords['VehicleRentMaster']['vehicle_code'] = $arrRecords['VM']['unique_no'];
                            $arrRecords['VehicleRentMaster']['vehicle_number'] = $arrRecords['VM']['vehicle_number'];
                            $arrRecords['VehicleRentMaster']['booking_date'] = $this->AppUtilities->date_format($arrRecords['VehicleRentMaster']['booking_date'],'dd-mm-yyyy');
                            $arrRecords['VehicleRentMaster']['booking_time'] = date('g:i:s A',strtotime($arrRecords['VehicleRentMaster']['booking_time']));
                            $arrRecords['VehicleRentMaster']['estimated_return_date'] = $this->AppUtilities->date_format($arrRecords['VehicleRentMaster']['estimated_return_date'],'dd-mm-yyyy');
                            $arrRecords['VehicleRentMaster']['estimated_return_time'] = date('g:i:s A',strtotime($arrRecords['VehicleRentMaster']['estimated_return_time']));
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleRentMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0 && !empty($firmId)) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('id'),'conditions' => array('status'=> 1,'rent_status' => 2,'id' => $this->request->data['id'],'branch_master_id' => $firmId));
                    $arrRecords = $this->VehicleRentMaster->find('list',$options);
                    #pr($arrRecords);exit;
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->VehicleRentMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['VehicleRentMaster.status'] = -1;
                            $updateFields['VehicleRentMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleRentMaster.id'] = $arrDeleteRecords;
                            $updateParams['VehicleRentMaster.branch_master_id'] = $firmId;
                            if(!$this->VehicleRentMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_rent_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
