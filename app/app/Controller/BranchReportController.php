<?php
App::uses('AppController','Controller');

class BranchReportController extends AppController {
    public $name = 'BranchReport';
    public $uses = array('ErrorLog','BranchMaster');
    public $components = array('AppUtilities');

    public function member_dynamic_report() {
        $response = array('status' => 0,'message' => 'Invalid response type');
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['branch']) && !empty($this->request->data['branch'])) {
                    $count = 0;
                    if(isset($this->request->data['branch'][0]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.unique_no';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Member Code';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][0]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][1]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.firm_name';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'firm_name';
                        $arrFieldData[$count]['caption'] = 'Firm Name';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][1]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][2]['field'])) {
                        $arrFieldData[$count]['columnName'] = "CONCAT_WS(' ',TitleMaster.name,BranchMaster.first_name,BranchMaster.middle_name,BranchMaster.last_name) as name";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Name';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][2]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][3]['field'])) {
                        $arrFieldData[$count]['columnName'] = "IF(BranchMaster.gender = 1,'Male','Female') AS gender";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'gender';
                        $arrFieldData[$count]['caption'] = 'Gender';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][3]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][4]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.mobile_no';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][4]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][5]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.alternate_mobile_no';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'alternate_mobile_no';
                        $arrFieldData[$count]['caption'] = 'Alternate Mobile No';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][5]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][6]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.email_id';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][6]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][7]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.alternate_email_id';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'alternate_email_id';
                        $arrFieldData[$count]['caption'] = 'Alternate Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][7]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][8]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'CountryMaster.name';
                        $arrFieldData[$count]['tableName'] = 'CountryMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Country';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][8]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][9]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'StateMaster.name';
                        $arrFieldData[$count]['tableName'] = 'StateMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'State';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][9]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][10]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'CityMaster.name';
                        $arrFieldData[$count]['tableName'] = 'CityMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'City';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][10]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][11]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'TypeTran.name';
                        $arrFieldData[$count]['tableName'] = 'TypeTran';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Member Type';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][11]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][12]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.pan_number';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'pan_number';
                        $arrFieldData[$count]['caption'] = 'PAN No';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][12]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][13]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.gst_number';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'gst_number';
                        $arrFieldData[$count]['caption'] = 'GST No';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][13]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][14]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.c_address';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'c_address';
                        $arrFieldData[$count]['caption'] = 'Residential Address';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][14]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][15]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.p_address';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'p_address';
                        $arrFieldData[$count]['caption'] = 'Permanent Address';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][15]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][16]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'BranchMaster.pincode';
                        $arrFieldData[$count]['tableName'] = 'BranchMaster';
                        $arrFieldData[$count]['fieldName'] = 'pincode';
                        $arrFieldData[$count]['caption'] = 'Pincode';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][16]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['branch'][17]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'RoyaltyChargeMaster.name';
                        $arrFieldData[$count]['tableName'] = 'RoyaltyChargeMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Royalty Charge';
                        $arrFieldData[$count]['order'] = $this->request->data['branch'][17]['order'];
                        ++$count;
                    }
                }
                
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                    }
                    $conditions = array('BranchMaster.status' => 1);
                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'BranchMaster.max_unique_no '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'BranchMaster.first_name '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'CountryMaster.name '.$sortyType);
                                    break;
                            case 4:
                                    $orderBy = array('order' => 'StateMaster.name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'BranchMaster.max_unique_no '.$sortyType);
                                    break;
                        }
                    }
                    $options = array(
                        'fields' => array_column($arrFieldData,'columnName'),
                        'joins' => array(
                            array(
                                'table' => 'type_trans',
                                'alias' => 'TypeTran',
                                'type' => 'INNER',
                                'conditions' => array('BranchMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1)
                            ),
                            array(
                                'table' => 'country_masters',
                                'alias' => 'CountryMaster',
                                'type' => 'LEFT',
                                'conditions' => array('BranchMaster.country_master_id = CountryMaster.id','CountryMaster.status' => 1)
                            ),
                            array(
                                'table' => 'state_masters',
                                'alias' => 'StateMaster',
                                'type' => 'LEFT',
                                'conditions' => array('BranchMaster.state_master_id = StateMaster.id','StateMaster.status' => 1)
                            ),
                            array(
                                'table' => 'city_masters',
                                'alias' => 'CityMaster',
                                'type' => 'LEFT',
                                'conditions' => array('BranchMaster.city_master_id = CityMaster.id','CityMaster.status' => 1)
                            ),
                            array(
                                'table' => 'title_masters',
                                'alias' => 'TitleMaster',
                                'type' => 'LEFT',
                                'conditions' => array('BranchMaster.title_master_id = TitleMaster.id','TitleMaster.status' => 1)
                            ),
                            array(
                                'table' => 'royalty_charge_masters',
                                'alias' => 'RoyaltyChargeMaster',
                                'type' => 'INNER',
                                'conditions' => array('BranchMaster.royalty_charge_master_id = RoyaltyChargeMaster.id','RoyaltyChargeMaster.status' => 1)
                            )
                        ),
                        'conditions' => $conditions,
                        'recursive' => -1,
                        'group' => array('BranchMaster.id')
                    );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->BranchMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select atleast one field !.',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function royalty_charge_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $conditions = array('BranchMaster.status' => 1);
                if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                    $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                    $conditions['RoyaltyChargeTran.date >='] = $fromDate;
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                    $conditions['RoyaltyChargeTran.date <='] = $endDate;
                }

                if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                    $conditions['BranchMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                }

                if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                    $conditions['RoyaltyChargeTran.company_year_master_id'] = $this->request->data['company_year_master_id'];
                }

                $options = array(
                                'fields' => array('id','firm_name','DATE_FORMAT(RoyaltyChargeTran.date,"%d-%m-%Y") as date','RoyaltyChargeTran.amount',"IF(RoyaltyChargeTran.type = 1,'Vehicle Sale','Service') AS type"),
                                'joins' => array(
                                    array(
                                        'table' => 'royalty_charge_trans',
                                        'alias' => 'RoyaltyChargeTran',
                                        'conditions' => array('BranchMaster.id = RoyaltyChargeTran.branch_master_id','RoyaltyChargeTran.status' => 1)
                                    )
                                ),
                                'conditions' => $conditions,
                                'order' => array('RoyaltyChargeTran.date')
                            );
                $arrRecords = $this->BranchMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrContentData = array();
                    foreach($arrRecords as $key => $data) {
                        $id = $data['BranchMaster']['id'];
                        $name = $data['BranchMaster']['firm_name'];
                        $arrContentData[$id]['id'] = $id;
                        $arrContentData[$id]['name'] = $name;
                        if(isset($arrContentData[$id]['total'])) {
                            $arrContentData[$id]['total'] = number_format($arrContentData[$id]['total'] + $data['RoyaltyChargeTran']['amount'],2,'.','');
                        } else {
                            $arrContentData[$id]['total'] = number_format($data['RoyaltyChargeTran']['amount'],2,'.','');
                        }
                        $arrContentData[$id]['detail'][] = array('date' => $data[0]['date'],'type' => $data[0]['type'],'amount' => $data['RoyaltyChargeTran']['amount']);
                    }
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => array_values($arrContentData),'header' => array());
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
