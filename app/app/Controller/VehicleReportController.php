<?php
App::uses('AppController','Controller');
class VehicleReportController extends AppController {
    public $name = 'VehicleReport';
    public $layout = false;
    public $uses = array('Vehicle','VehicleRentMaster','VehicleFinanceMaster','ExpenseMaster','VehicleSaleMaster','ErrorLog');
    public $components = array('AppUtilities');

    public function index() {
        exit;
    }

    public function bike_rent_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['vehicle']) && !empty($this->request->data['vehicle'])) {
                    $count = 0;
                    if(isset($this->request->data['vehicle'][0]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleRentMaster.booking_date,'%d-%m-%Y') AS booking_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'booking_date';
                        $arrFieldData[$count]['caption'] = 'Booking Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][0]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][1]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleRentMaster.booking_time';
                        $arrFieldData[$count]['tableName'] = 'VehicleRentMaster';
                        $arrFieldData[$count]['fieldName'] = 'booking_time';
                        $arrFieldData[$count]['caption'] = 'Booking Time';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][1]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][2]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_type_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_type_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Type';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][2]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][3]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.manufacturer_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                        $arrFieldData[$count]['caption'] = 'Manufacturer';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][3]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][4]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_model_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Model';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][4]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][5]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.unique_no';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Vehicle Code';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][5]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][6]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                        $arrFieldData[$count]['caption'] = 'Vehicle No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][6]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][7]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.engine_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'engine_number';
                        $arrFieldData[$count]['caption'] = 'Engine No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][7]['order'];
                        ++$count;
                    }
                    if(isset($this->request->data['vehicle'][8]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.chasis_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'chasis_number';
                        $arrFieldData[$count]['caption'] = 'Chasis No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][8]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][9]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleRentMaster.security_amount';
                        $arrFieldData[$count]['tableName'] = 'VehicleRentMaster';
                        $arrFieldData[$count]['fieldName'] = 'security_amount';
                        $arrFieldData[$count]['caption'] = 'Security Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][9]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][10]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleRentMaster.estimate_rent_amount';
                        $arrFieldData[$count]['tableName'] = 'VehicleRentMaster';
                        $arrFieldData[$count]['fieldName'] = 'estimate_rent_amount';
                        $arrFieldData[$count]['caption'] = 'Estimate Rent Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][10]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][11]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleRentMaster.estimated_return_date';
                        $arrFieldData[$count]['tableName'] = 'VehicleRentMaster';
                        $arrFieldData[$count]['fieldName'] = 'estimated_return_date';
                        $arrFieldData[$count]['caption'] = 'Estimate Return Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][11]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][12]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleRentMaster.estimated_return_time';
                        $arrFieldData[$count]['tableName'] = 'VehicleRentMaster';
                        $arrFieldData[$count]['fieldName'] = 'estimated_return_time';
                        $arrFieldData[$count]['caption'] = 'Estimate Return Time';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][12]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][13]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.name";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Customer Name';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][13]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][14]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.mobile_no';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][14]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][15]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.email_id';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][15]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][16]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.residential_address';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'residential_address';
                        $arrFieldData[$count]['caption'] = 'Residential Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][16]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][17]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.permanent_address';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'permanent_address';
                        $arrFieldData[$count]['caption'] = 'Permanent Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][17]['order'];
                        ++$count;
                    }
                }
                
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    #pr($arrFieldData);exit;
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                    }
                    $conditions = array('VehicleRentMaster.status' => 1);
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $conditions['VehicleRentMaster.booking_date >='] = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                    }

                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $conditions['VehicleRentMaster.booking_date <='] = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                    }

                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['VehicleRentMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                        $conditions['VehicleRentMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                    }
                    
                    if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                        $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                    }
                    
                    if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                        $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                    }

                    if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                        $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                    }

                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'VehicleRentMaster.booking_date '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'Vehicle.vehicle_type_name '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'Vehicle.manufacturer_name '.$sortyType);
                                    break;
                            case 4:
                                    $orderBy = array('order' => 'Vehicle.vehicle_model_name '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'Vehicle.max_unique_no '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'Customer.first_name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'VehicleRentMaster.booking_date '.$sortyType);
                                    break;
                        }
                    }
                    $options = array(
                        'fields' => array_column($arrFieldData,'columnName'),
                        'joins' => array(
                            array(
                                'table' => 'vehicles',
                                'alias' => 'Vehicle',
                                'type' => 'INNER',
                                'conditions' => array('VehicleRentMaster.vehicle_master_id = Vehicle.id','Vehicle.status' => 1)
                            ),
                            array(
                                'table' => 'customers',
                                'alias' => 'Customer',
                                'type' => 'INNER',
                                'conditions' => array('VehicleRentMaster.customer_master_id = Customer.id','Customer.status' => 1)
                            )
                        ),
                        'conditions' => $conditions,
                        'recursive' => -1,
                        'group' => array('VehicleRentMaster.id')
                    );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->VehicleRentMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function bike_finance_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['vehicle']) && !empty($this->request->data['vehicle'])) {
                    $count = 0;
                    if(isset($this->request->data['vehicle'][0]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleFinanceMaster.date_of_loan,'%d-%m-%Y') AS loan_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'loan_date';
                        $arrFieldData[$count]['caption'] = 'Loan Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][0]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][1]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_type_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_type_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Type';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][1]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][2]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.manufacturer_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                        $arrFieldData[$count]['caption'] = 'Manufacturer';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][2]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][3]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_model_name';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Model';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][3]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][4]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.unique_no';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Vehicle Code';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][4]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][5]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.vehicle_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                        $arrFieldData[$count]['caption'] = 'Vehicle No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][5]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][6]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.engine_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'engine_number';
                        $arrFieldData[$count]['caption'] = 'Engine No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][6]['order'];
                        ++$count;
                    }
                    if(isset($this->request->data['vehicle'][7]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Vehicle.chasis_number';
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'chasis_number';
                        $arrFieldData[$count]['caption'] = 'Chasis No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][7]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][8]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleFinanceMaster.loan_amount';
                        $arrFieldData[$count]['tableName'] = 'VehicleFinanceMaster';
                        $arrFieldData[$count]['fieldName'] = 'loan_amount';
                        $arrFieldData[$count]['caption'] = 'Loan Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][8]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][9]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleFinanceMaster.loan_tenure';
                        $arrFieldData[$count]['tableName'] = 'VehicleFinanceMaster';
                        $arrFieldData[$count]['fieldName'] = 'loan_tenure';
                        $arrFieldData[$count]['caption'] = 'Loan Tenure';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][9]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][10]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleFinanceMaster.emi_amount';
                        $arrFieldData[$count]['tableName'] = 'VehicleFinanceMaster';
                        $arrFieldData[$count]['fieldName'] = 'emi_amount';
                        $arrFieldData[$count]['caption'] = 'EMI Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][10]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][11]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'VehicleFinanceMaster.down_payment';
                        $arrFieldData[$count]['tableName'] = 'VehicleFinanceMaster';
                        $arrFieldData[$count]['fieldName'] = 'down_payment';
                        $arrFieldData[$count]['caption'] = 'Down Payment';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][11]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][12]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.name";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Customer Name';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][12]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][13]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.mobile_no';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][13]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][14]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.email_id';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][14]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][15]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.residential_address';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'residential_address';
                        $arrFieldData[$count]['caption'] = 'Residential Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][15]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['vehicle'][16]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'Customer.permanent_address';
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'permanent_address';
                        $arrFieldData[$count]['caption'] = 'Permanent Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][16]['order'];
                        ++$count;
                    }
                }
                
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    #pr($arrFieldData);exit;
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                    }
                    
                    $conditions = array('VehicleFinanceMaster.status' => 1);
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $conditions['VehicleFinanceMaster.date_of_loan >='] = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                    }

                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $conditions['VehicleFinanceMaster.date_of_loan <='] = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                    }

                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['VehicleFinanceMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                        $conditions['VehicleFinanceMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                    }
                    
                    if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                        $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                    }
                    
                    if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                        $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                    }

                    if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                        $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                    }

                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'VehicleFinanceMaster.date_of_loan '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'Vehicle.vehicle_type_name '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'Vehicle.manufacturer_name '.$sortyType);
                                    break;
                            case 4:
                                    $orderBy = array('order' => 'Vehicle.vehicle_model_name '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'Vehicle.max_unique_no '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'Customer.first_name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'VehicleFinanceMaster.date_of_loan '.$sortyType);
                                    break;
                        }
                    }
                    $options = array(
                        'fields' => array_column($arrFieldData,'columnName'),
                        'joins' => array(
                            array(
                                'table' => 'vehicles',
                                'alias' => 'Vehicle',
                                'type' => 'INNER',
                                'conditions' => array('VehicleFinanceMaster.vehicle_master_id = Vehicle.id','Vehicle.status' => 1)
                            ),
                            array(
                                'table' => 'customers',
                                'alias' => 'Customer',
                                'type' => 'INNER',
                                'conditions' => array('VehicleFinanceMaster.customer_master_id = Customer.id','Customer.status' => 1)
                            )
                        ),
                        'conditions' => $conditions,
                        'recursive' => -1,
                        'group' => array('VehicleFinanceMaster.id')
                    );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->VehicleFinanceMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function vehicle_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if(isset($this->request->data['vehicle']) && !empty($this->request->data['vehicle'])) {
                $fcount = $count = 0;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "DATE_FORMAT(Vehicle.registration_date,'%d-%m-%Y') AS registration_date";
                    $arrFieldData[$count]['tableName'] = 0;
                    $arrFieldData[$count]['fieldName'] = 'registration_date';
                    $arrFieldData[$count]['caption'] = 'Registration Date';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.unique_no";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'unique_no';
                    $arrFieldData[$count]['caption'] = 'Vehicle Code';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_type_name";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'vehicle_type_name';
                    $arrFieldData[$count]['caption'] = 'Vehicle Type';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.manufacturer_name";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                    $arrFieldData[$count]['caption'] = 'Manufacturer';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_model_name";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                    $arrFieldData[$count]['caption'] = 'Vehicle Model';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_number";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                    $arrFieldData[$count]['caption'] = 'Vehicle No.';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.manufacturer_year";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'manufacturer_year';
                    $arrFieldData[$count]['caption'] = 'Manufacturer Year';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.engine_number";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'engine_number';
                    $arrFieldData[$count]['caption'] = 'Engine No.';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.chasis_number";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'chasis_number';
                    $arrFieldData[$count]['caption'] = 'Chasis No.';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "VariantMaster.name AS variant_color";
                    $arrFieldData[$count]['tableName'] = 'VariantMaster';
                    $arrFieldData[$count]['fieldName'] = 'variant_color';
                    $arrFieldData[$count]['caption'] = 'Colour';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "IF(Vehicle.vehicle_type = 1,'Sell','Rent') AS stock_type";
                    $arrFieldData[$count]['tableName'] = 0;
                    $arrFieldData[$count]['fieldName'] = 'stock_type';
                    $arrFieldData[$count]['caption'] = 'Stock Type';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "(CASE Vehicle.fuel_type WHEN 1 THEN 'Patrol' WHEN 2 THEN 'Diesel' ELSE 'CNG' END) AS fuel_type";
                    $arrFieldData[$count]['tableName'] = 0;
                    $arrFieldData[$count]['fieldName'] = 'fuel_type';
                    $arrFieldData[$count]['caption'] = 'Fuel Type';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "(CASE Vehicle.vehicle_start_method WHEN 1 THEN 'Self' WHEN 2 THEN 'Kick' ELSE 'Both' END) AS vehicle_start_method";
                    $arrFieldData[$count]['tableName'] = 0;
                    $arrFieldData[$count]['fieldName'] = 'vehicle_start_method';
                    $arrFieldData[$count]['caption'] = 'Vehicle Start Method';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                $isVehicleExpense = 0;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.id as expense";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'expense';
                    $arrFieldData[$count]['caption'] = 'Expense';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    $isVehicleExpense = 1;
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.purchase_price";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'purchase_price';
                    $arrFieldData[$count]['caption'] = 'Purchase Price';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.profit";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'profit';
                    $arrFieldData[$count]['caption'] = 'Profit';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "CONCAT_WS(' ',TitleMaster.name,Vehicle.owner_first_name,Vehicle.owner_middle_name,Vehicle.owner_last_name) AS fullname";
                    $arrFieldData[$count]['tableName'] = 0;
                    $arrFieldData[$count]['fieldName'] = 'fullname';
                    $arrFieldData[$count]['caption'] = 'Current Owner';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.owner_mobile_no";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'owner_mobile_no';
                    $arrFieldData[$count]['caption'] = 'Modile No.';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.owner_email_id";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'owner_email_id';
                    $arrFieldData[$count]['caption'] = 'Email ID';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.owner_profession";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'owner_profession';
                    $arrFieldData[$count]['caption'] = 'Profession';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.owner_address";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'owner_address';
                    $arrFieldData[$count]['caption'] = 'Address';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "CountryMaster.name AS country";
                    $arrFieldData[$count]['tableName'] = 'CountryMaster';
                    $arrFieldData[$count]['fieldName'] = 'country';
                    $arrFieldData[$count]['caption'] = 'Vehicle Code';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "StateMaster.name AS state";
                    $arrFieldData[$count]['tableName'] = 'StateMaster';
                    $arrFieldData[$count]['fieldName'] = 'state';
                    $arrFieldData[$count]['caption'] = 'State';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "CityMaster.name AS city";
                    $arrFieldData[$count]['tableName'] = 'CityMaster';
                    $arrFieldData[$count]['fieldName'] = 'city';
                    $arrFieldData[$count]['caption'] = 'City';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }
                ++$fcount;
                if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                    $arrFieldData[$count]['columnName'] = "Vehicle.owner_remark";
                    $arrFieldData[$count]['tableName'] = 'Vehicle';
                    $arrFieldData[$count]['fieldName'] = 'owner_remark';
                    $arrFieldData[$count]['caption'] = 'Remark';
                    $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                    ++$count;
                }

                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    #pr($arrFieldData);exit;
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[$key]['field'] = $header['fieldName'];
                        $arrHeaderData[$key]['caption'] = $header['caption'];
                    }
                    
                    $conditions = array('Vehicle.status' => 1,'Vehicle.vehicle_type' => array(2,3));
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                        $conditions['Vehicle.registration_date >='] = $fromDate;
                    }

                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                        $conditions['Vehicle.registration_date <='] = $endDate;
                    }

                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['Vehicle.branch_master_id'] = $this->request->data['branch_master_id']; 
                    }

                    if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                        $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                    }
                    
                    if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                        $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                    }

                    if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                        $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                    }
                    $conditions['Vehicle.vehicle_from'] = 1;
                    #pr($conditions);exit;
                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'Vehicle.registration_date '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'Vehicle.vehicle_type_name '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'Vehicle.manufacturer_name '.$sortyType);
                                    break;
                            case 4:
                                    $orderBy = array('order' => 'Vehicle.vehicle_model_name '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'Vehicle.max_unique_no '.$sortyType);
                                    break;
                            case 5:
                                    $orderBy = array('order' => 'VehicleRentMaster.first_name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'Vehicle.registration_date '.$sortyType);
                                    break;
                        }
                    } else {
                        $orderBy = array('order' => 'Vehicle.registration_date ASC');
                    }
                    $options = array(
                        'fields' => array_column($arrFieldData,'columnName'),
                        'joins' => array(
                            array(
                                'table' => 'variant_masters',
                                'alias' => 'VariantMaster',
                                'type' => 'LEFT',
                                'conditions' => array('Vehicle.variant_master_id = VariantMaster.id','VariantMaster.status' => 1)
                            ),
                            array(
                                'table' => 'title_masters',
                                'alias' => 'TitleMaster',
                                'type' => 'LEFT',
                                'conditions' => array('Vehicle.owner_title_master_id = TitleMaster.id','TitleMaster.status' => 1)
                            ),
                            array(
                                'table' => 'country_masters',
                                'alias' => 'CountryMaster',
                                'type' => 'LEFT',
                                'conditions' => array('Vehicle.owner_country_master_id = CountryMaster.id','CountryMaster.status' => 1)
                            ),
                            array(
                                'table' => 'state_masters',
                                'alias' => 'StateMaster',
                                'type' => 'LEFT',
                                'conditions' => array('Vehicle.owner_state_master_id = StateMaster.id','StateMaster.status' => 1)
                            ),
                            array(
                                'table' => 'city_masters',
                                'alias' => 'CityMaster',
                                'type' => 'LEFT',
                                'conditions' => array('Vehicle.owner_city_master_id = CityMaster.id','CityMaster.status' => 1)
                            )
                        ),
                        'conditions' => $conditions,
                    );
                    #pr($conditions);
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->Vehicle->find('all',$options);
                    if(count($arrRecords) > 0) {
                        if($isVehicleExpense == 1) {
                            $arrExpenseDetail = array();
                            $arrVehicleId = Hash::combine($arrRecords,'{n}.Vehicle.expense','{n}.Vehicle.expense');
                            $options = array(
                                            'fields' => array('vehicle_master_id','EHM.id','EHM.name','ET.price'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'expense_trans',
                                                    'alias' => 'ET',
                                                    'conditions' => array('ExpenseMaster.id = ET.expense_master_id','ET.status' => 1)
                                                ),
                                                array(
                                                    'table' => 'expense_head_masters',
                                                    'alias' => 'EHM',
                                                    'conditions' => array('ET.expense_head_master_id = EHM.id','EHM.status' => 1)
                                                )
                                            ),
                                            'conditions' => array(
                                                'ExpenseMaster.status' => 1,'ExpenseMaster.vehicle_master_id' => $arrVehicleId,'ExpenseMaster.branch_master_id' => $this->request->data['branch_master_id']
                                            ),
                                            'order' => array('EHM.order_no')
                                        );
                            $arrVehicleExpenseData = $this->ExpenseMaster->find('all',$options);
                            if(count($arrVehicleExpenseData) > 0) {
                                foreach($arrVehicleExpenseData as $key => $expenseHead) {
                                    $arrExpenseDetail[$expenseHead['EHM']['id']]['field'] = $expenseHead['EHM']['id'];
                                    $arrExpenseDetail[$expenseHead['EHM']['id']]['caption'] = $expenseHead['EHM']['name'];
                                    $arrVehicleExpenseHeadData[$expenseHead['ExpenseMaster']['vehicle_master_id']][$expenseHead['EHM']['id']] = $expenseHead['ET']['price'];
                                }
                                $arrFeeHead = array();
                                foreach($arrHeaderData as $key => $value) {
                                    if($value['field'] == 'expense') {
                                        foreach($arrExpenseDetail as $key => $value1) {
                                            $arrFeeHead[] = $value1;
                                        }
                                    } else {
                                        $arrFeeHead[] = $value;
                                    }
                                }
                                $arrHeaderData = $arrFeeHead;
                                unset($arrFeeHead);
                            }
                        }
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                            if(isset($data['Vehicle']['expense']) && isset($arrVehicleExpenseHeadData[$data['Vehicle']['expense']])) {
                                $arrContentData[$key] = $arrContentData[$key] + $arrVehicleExpenseHeadData[$data['Vehicle']['expense']];
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function dynamic_vehicle_sale_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['vehicle']) && !empty($this->request->data['vehicle'])) {
                    $fcount = $count = 0;
                    $arrFieldData = $arrHeaderData = array();
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleSaleMaster.date,'%d-%m-%Y') AS date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'date';
                        $arrFieldData[$count]['caption'] = 'Invoice Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "InvoiceMaster.invoice_no";
                        $arrFieldData[$count]['tableName'] = 'InvoiceMaster';
                        $arrFieldData[$count]['fieldName'] = 'invoice_no';
                        $arrFieldData[$count]['caption'] = 'Invoice No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "InvoiceMaster.amount";
                        $arrFieldData[$count]['tableName'] = 'InvoiceMaster';
                        $arrFieldData[$count]['fieldName'] = 'amount';
                        $arrFieldData[$count]['caption'] = 'Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_type_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_type_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Type';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.manufacturer_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                        $arrFieldData[$count]['caption'] = 'Manufacturer';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_model_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Model';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.unique_no";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Vehicle Code';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                        $arrFieldData[$count]['caption'] = 'Vehicle No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.engine_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'engine_number';
                        $arrFieldData[$count]['caption'] = 'Engine No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.chasis_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'chasis_number';
                        $arrFieldData[$count]['caption'] = 'Chasis No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleSaleMaster.guarantee";
                        $arrFieldData[$count]['tableName'] = 'VehicleSaleMaster';
                        $arrFieldData[$count]['fieldName'] = 'guarantee';
                        $arrFieldData[$count]['caption'] = 'Guarantee';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "CouponMaster.name";
                        $arrFieldData[$count]['tableName'] = 'CouponMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Coupon';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] ="Customer.name as fullname";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'fullname';
                        $arrFieldData[$count]['caption'] = 'Customer Name';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.mobile_no";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Customer Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.email_id";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Customer Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.adhaar_no";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'adhaar_no';
                        $arrFieldData[$count]['caption'] = 'Adhaar No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.pancard_no";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'pancard_no';
                        $arrFieldData[$count]['caption'] = 'Pancard No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Customer.permanent_address";
                        $arrFieldData[$count]['tableName'] = 'Customer';
                        $arrFieldData[$count]['fieldName'] = 'address';
                        $arrFieldData[$count]['caption'] = 'Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle'][$fcount]['order'];
                        ++$count;
                    }
                    if(count($arrFieldData) > 0) {
                        uasort($arrFieldData,function($a,$b){
                            if($a['order'] == 0){
                                return 1;
                            }
                            if($b['order'] == 0){
                                return -1;
                            }
                            return ($a['order'] < $b['order']) ? -1 : 1;
                        });
                        #pr($arrFieldData);exit;
                        foreach($arrFieldData as $key => $header) {
                            $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                        }
                        
                        $conditions = array('VehicleSaleMaster.status' => 1);
                        if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                            $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                            $conditions['VehicleSaleMaster.date >='] = $fromDate;
                        }
    
                        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                            $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                            $conditions['VehicleSaleMaster.date <='] = $endDate;
                        }
    
                        if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                            $conditions['VehicleSaleMaster.branch_master_id'] = $this->request->data['branch_master_id']; 
                        }
                        
                        if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                            $conditions['InvoiceMaster.company_year_master_id'] = $this->request->data['company_year_master_id']; 
                        }

                        if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                            $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                        }
                        
                        if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                            $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                        }
    
                        if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                            $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                        }

                        if(isset($this->request->data['payment_status']) && !empty($this->request->data['payment_status'])) {
                            $conditions['InvoiceMaster.payment_status'] = intval($this->request->data['payment_status'] - 1);
                        }

                        if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                            $sortBy = (int) $this->request->data['sort_by'];
                            $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                            switch($sortBy) {
                                case 1:
                                        $orderBy = array('order' => 'VehicleSaleMaster.date '.$sortyType);
                                        break;
                                case 2:
                                        $orderBy = array('order' => 'Vehicle.vehicle_type_name '.$sortyType);
                                        break;
                                case 3:
                                        $orderBy = array('order' => 'Vehicle.manufacturer_name '.$sortyType);
                                        break;
                                case 4:
                                        $orderBy = array('order' => 'Vehicle.vehicle_model_name '.$sortyType);
                                        break;
                                case 5:
                                        $orderBy = array('order' => 'Vehicle.max_unique_no '.$sortyType);
                                        break;
                                case 5:
                                        $orderBy = array('order' => 'VehicleRentMaster.first_name '.$sortyType);
                                        break;
                                default:
                                        $orderBy = array('order' => 'VehicleSaleMaster.date '.$sortyType);
                                        break;
                            }
                        } else {
                            $orderBy = array('order' => 'VehicleSaleMaster.date ASC');
                        }
                        $options = array(
                            'fields' => array_column($arrFieldData,'columnName'),
                            'joins' => array(
                                array(
                                    'table' => 'customers',
                                    'alias' => 'Customer',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleSaleMaster.customer_master_id = Customer.id','Customer.status' => 1)
                                ),
                                array(
                                    'table' => 'invoice_masters',
                                    'alias' => 'InvoiceMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleSaleMaster.invoice_master_id = InvoiceMaster.id','InvoiceMaster.status' => 1)
                                ),
                                array(
                                    'table' => 'vehicles',
                                    'alias' => 'Vehicle',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleSaleMaster.vehicle_master_id = Vehicle.id','Vehicle.status' => 1)
                                ),
                                array(
                                    'table' => 'coupon_masters',
                                    'alias' => 'CouponMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('VehicleSaleMaster.coupon_master_id = CouponMaster.id','CouponMaster.status' => 1)
                                )
                            ),
                            'conditions' => $conditions,
                        );
                        #pr($conditions);
                        $options = array_merge($options,$orderBy);
                        #pr($options);exit;
                        $arrRecords = $this->VehicleSaleMaster->find('all',$options);
                        if(count($arrRecords) > 0) {
                            $arrContentData = array();
                            foreach($arrRecords as $key => $data) {
                                foreach($arrFieldData as $index => $fields) {
                                    if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                        $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                    }
                                }
                            }
                            unset($arrFieldData);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
