<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $method = array('POST','GET','PUT');
    public $uses = array('UserLoggedTran');
    public $components = array('Session','RequestHandler');

    public function bodyResponse($response,$statusCode = 200) {
        $this->autoRender = false;
        $statusCode = (int) $statusCode;
        $this->response->type('json');
        $outputBuffer = ob_get_contents();
        if(strlen($outputBuffer) > 0 || is_null($response)) {
            ob_clean();
            $response = array(
                            'status' => 0,
                            'message' => 'Unexpected API output. Contact Developer',
                            'error' => $outputBuffer
                        );
            $this->loadModel('ErrorLog');
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => json_encode($response));
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
        } else if($statusCode === 400) {
            $this->loadModel('ErrorLog');
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => json_encode($response));
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
        } else {}
        $response['statusCode'] = $statusCode;
        $this->response->body(json_encode($response));
    }

    public function httpHeaderValidate() {
        $response = array();
        if(!in_array($_SERVER['REQUEST_METHOD'],$this->method)) {
            $response = array('status' => 0 ,'message' => 'invalid requet method');
        } else {
            if(isset($this->request->data['user_login']) && $this->request->data['user_login'] == 1) {
                $appAccessToken = isset($_SERVER['HTTP_APP_ACCESS_TOKEN']) ? $_SERVER['HTTP_APP_ACCESS_TOKEN'] : '';
                $appScretKey = isset($_SERVER['HTTP_APP_SECRET_KEY']) ? $_SERVER['HTTP_APP_SECRET_KEY'] : '';
                $appApiKey = isset($_SERVER['HTTP_APP_API_KEY']) ? $_SERVER['HTTP_APP_API_KEY'] : '';
                if($appApiKey != Configure::read('SERVER_API_KEY') || $appScretKey != Configure::read('SERVER_SECRET_KEY') || $appAccessToken != Configure::read('SERVER_ACCESS_TOKEN')) {
                    $response = array('status' => 0 ,'message' => 'Invalid Header Authentication');
                } 
            } else {
                $sessId = isset($_SERVER['HTTP_APP_SESSID']) ? $_SERVER['HTTP_APP_SESSID'] : '';
                $tokenId = isset($_SERVER['HTTP_APP_TOKEN_ID']) ? $_SERVER['HTTP_APP_TOKEN_ID'] : '';
                $options = array('fields' => array('user_id'),'conditions' => array('status' => 1,'sessid' => $sessId,'token_id' => $tokenId));
                $arrUserData = $this->UserLoggedTran->find('first',$options);
                if(!empty($arrUserData)) {
                    $this->Session->write('sessUserId',$arrUserData['UserLoggedTran']['user_id']);
                } else {
                    $response = array('status' => -1 ,'message' => 'Invalid Header Authentication');
                }
            }
        }
        return $response;
    }
    
    public function beforeFilter() {
        ob_start();
        //header('Content-Type: application/json');
        $this->response->disableCache();
        if($this->request->is("options")){
            $this->response->header('Access-Control-Allow-Origin','*');
            $this->response->header('Access-Control-Allow-Methods','*');
            $this->response->header('Access-Control-Allow-Headers','Content-Type, Authorization');
            $this->response->send();
            $this->_stop();
        }
        
        if($this->request->params['controller'] !== 'table_schema') {
            $data = $this->request->input('json_decode', 'true');
            if(!empty($data)) {
                $this->request->data = $data;
            }
            $response = $this->httpHeaderValidate();
            if(!empty($response)) {
                $this->bodyResponse($response);
            }
        }
    }

    public function safe_b64encode($string = '') {
        $data = base64_encode($string);
        $data = str_replace(['+','/','='],['-','_',''],$data);
        return $data;
    }

    public function safe_b64decode($string = '') {
        $data = str_replace(['-','_'],['+','/'],$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encryption($value = null) {
        if(!$value) return false;
        $iv_size = openssl_cipher_iv_length('aes-256-cbc');
        $iv = openssl_random_pseudo_bytes($iv_size);
        $crypttext = openssl_encrypt($value, 'aes-256-cbc', '63297932632423432@#$ridizoApp', OPENSSL_RAW_DATA, $iv);
        return $this->safe_b64encode($iv.$crypttext); 
    }

    public function decryption($value = null) {
        if(!$value) return false;
        $crypttext = $this->safe_b64decode($value);
        $iv_size = openssl_cipher_iv_length('aes-256-cbc');
        $iv = substr($crypttext, 0, $iv_size);
        $crypttext = substr($crypttext, $iv_size);
        if(!$crypttext) return false;
        $decrypttext = openssl_decrypt($crypttext, 'aes-256-cbc', '63297932632423432@#$ridizoApp', OPENSSL_RAW_DATA, $iv);
        return rtrim($decrypttext);
    }

    public function bulk_decryption($data = array()) {
        $arrDecryptedId = array();
        if(count($data) > 0) {
            foreach($data as $key => $id) {
                $arrDecryptedId[] = $this->decryption($id);
            }
        }
        return $arrDecryptedId;
    }

    public function encrypt_algo($message = null) {
        $encryption = $message;
        if(!empty($message)) {
            $algorithm = "AES-256-CBC";
            $key = '64357565483780636948548935453454';
            $EncryptionKey = base64_decode($key);
            $InitializationVector  = openssl_random_pseudo_bytes(openssl_cipher_iv_length($algorithm));
            $EncryptedText = openssl_encrypt($message, $algorithm, $EncryptionKey, 0, $InitializationVector);
            $encryption =  base64_encode($EncryptedText . '::' . $InitializationVector);
        }
        return $encryption;
    }

    public function decrypt_algo($encryption = null) {
        $decryption = $encryption;
        if(!empty($encryption)) {
            $algorithm = "AES-256-CBC";
            $key = '64357565483780636948548935453454';
            $EncryptionKey = base64_decode($key);
            list($encryptedData, $InitializationVector ) = array_pad(explode('::', base64_decode($encryption), 2), 2, null);
            $decryption =  openssl_decrypt($encryptedData, $algorithm, $EncryptionKey, 0, $InitializationVector);
        }
        return $decryption;
    }

    public function test() {
        $this->UserManagement = $this->Components->load('UserManagement');
        $this->UserManagement->getUserRights(1);
        exit;
        $string = bin2hex(openssl_random_pseudo_bytes(60));
        echo "length :".strlen($string)," ->",$string;exit;
        exit;
        echo md5(uniqid('',true));exit;
        $encrypt = $this->encryption(1);
        echo "encryption : ",$encrypt,"<br>";
        echo "decryption :",$this->decryption($encrypt);
        exit;
    }

    public function email_test() {
        $this->Email = $this->Components->load('Email');
        $config['from_email'] = 'ak8104203044@gmail.com';
        $config['subject'] = 'testing email';
        $config['content'] = 'content here';
        $this->Email->sendEmail($config);
        exit;
    }
}
