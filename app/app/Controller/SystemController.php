<?php
App::uses('AppController','Controller');

class SystemController extends AppController {
    public $layout = false;
    public $uses = array('User','CountryMaster','Customer','CouponMaster','GradeMaster','DocumentMaster','ProductStockTran','BankMaster','RoyaltyChargeMaster','ExpenseHeadMaster','ErrorLog','CustomerMaster','SupplierMaster','StateMaster','Employee','EmployeeMaster','ServiceMaster','UnitMaster','CategoryMaster','ProductMaster','VehicleSpecificationMaster','CityMaster','VariantMaster','TaxMaster','TaxGroupMaster','VehicleTypeMaster','ManufacturerMaster','ServiceGroupMaster','DepartmentMaster','TypeTran','TitleMaster','GeneralSetting','CompanyYearMaster','BranchMaster','DesignationMaster','RoleMaster','VehicleModelMaster','InspectionMaster','Vehicle','AgentMaster','RoleLinkMaster');
    public $components = array('AppUtilities','UserManagement','RequestHandler');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function get_country_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->CountryMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.CountryMaster.id','{n}.CountryMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_state_list($countryId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $conditions = array('status' => 1);
                if($countryId > 0) {
                    $conditions['StateMaster.country_master_id'] = $countryId;
                } else {
                    $conditions['StateMaster.country_master_id >'] = 0;
                }
                $options = array('fields' => array('id','name'),'conditions' => $conditions,'order' => array('order_no'));
                $arrRecords = $this->StateMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.StateMaster.id','{n}.StateMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_city_list($stateId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $conditions = array('CityMaster.status' => 1);
                if($stateId > 0) {
                    $conditions['CityMaster.state_master_id'] = $stateId;
                } else {
                    $conditions['CityMaster.state_master_id >'] = 0;
                }
                $options = array('fields' => array('id','name'),'conditions' => $conditions,'order' => array('order_no'));
                $arrRecords = $this->CityMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.CityMaster.id','{n}.CityMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_tax_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('order_no'));
                    $arrRecords = $this->TaxMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.TaxMaster.id','{n}.TaxMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_BRANCH',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_unit_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->UnitMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.UnitMaster.id','{n}.UnitMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function get_service_group_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('order_no'));
                    $arrRecords = $this->ServiceGroupMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.ServiceGroupMaster.id','{n}.ServiceGroupMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select valid branch',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function service_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(!empty($firmId)) {
                    $conditions = array('ServiceMaster.status' => 1,'ServiceMaster.branch_master_id' => $firmId);
                    if(isset($this->request->data['searchText']) && !empty($this->request->data['searchText'])) {
                        $conditions['ServiceMaster.name LIKE'] =  '%'.trim($this->request->data['searchText']).'%';
                    }
                    if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                        $conditions['NOT']['ServiceMaster.id'] =  $this->request->data['id'];
                    }
                    $options = array('fields' => array('id','name','code','price'),'conditions' => $conditions,'order' => array('order_no'));
                    $arrRecords = $this->ServiceMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.ServiceMaster.id','{n}.ServiceMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select valid branch',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_vehicle_type_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->VehicleTypeMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.VehicleTypeMaster.id','{n}.VehicleTypeMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_variant_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name','color_code'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->VariantMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.VariantMaster.id','{n}.VariantMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_manufacturer_vehicle_list($vehicleTypeId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($vehicleTypeId)) {
                    $this->ManufacturerMaster->virtualFields = array('mtranid' => 'ManufacturerTran.id');
                    $options = array(
                                    'fields' => array('id','name','mtranid','ManufacturerTran.id'),
                                    'joins' => array(
                                        array(
                                            'table' => 'manufacturer_trans',
                                            'alias' => 'ManufacturerTran',
                                            'type' => 'INNER',
                                            'conditions' => array('ManufacturerMaster.id = ManufacturerTran.manufacturer_master_id','ManufacturerTran.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('ManufacturerMaster.status' => 1,'ManufacturerTran.vehicle_type_master_id' => $vehicleTypeId),
                                    'order' => array('ManufacturerMaster.order_no')
                                );
                    $arrRecords = $this->ManufacturerMaster->find('all',$options);
                    $arrRecords = Hash::combine($arrRecords,'{n}.ManufacturerTran.id','{n}.ManufacturerMaster');
                    if(count($arrRecords) > 0) {
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_tax_group_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('order_no'));
                    $arrRecords = $this->TaxGroupMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.TaxGroupMaster.id','{n}.TaxGroupMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select valid branch',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_department_list($branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $branchId),'order' => array('order_no'));
                $arrRecords = $this->DepartmentMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.DepartmentMaster.id','{n}.DepartmentMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_designation_list($branchId = 0,$departmentId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($departmentId)) {
                    $conditions = array('DesignationMaster.status' => 1,'DesignationMaster.department_master_id' => $departmentId,'DesignationMaster.branch_master_id' => $branchId);
                    $options = array('fields' => array('id','name'),'conditions' => $conditions,'order' => array('order_no'));
                    $arrRecords = $this->DesignationMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.DesignationMaster.id','{n}.DesignationMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_member_type_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name','role_type'),'conditions' => array('status' => 1,'type_master_id' => Configure::read('MEMBER_TYPE_MASTER_ID'),'role_type' => array(Configure::read('EMPLOYEE_ROLE_TYPE'),Configure::read('MEMBER_ROLE_TYPE'))),'order' => array('order_no'));
                $arrRecords = $this->TypeTran->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.TypeTran.id','{n}.TypeTran');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_franchise_type_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'type_master_id' => Configure::read('MEMBER_TYPE_MASTER_ID'),'role_type' => Configure::read('MEMBER_ROLE_TYPE')),'order' => array('order_no'));
                $arrRecords = $this->TypeTran->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.TypeTran.id','{n}.TypeTran');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_title_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->TitleMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.TitleMaster.id','{n}.TitleMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_member_role_list($memberTypeId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($memberTypeId)) {
                    $options = array(
                        'fields' => array('id','name'),
                        'joins' => array(
                            array(
                                'table' => 'type_trans',
                                'alias' => 'TypeTran',
                                'type' => 'INNER',
                                'conditions' => array('RoleMaster.type_tran_id = TypeTran.id','TypeTran.status' => 1)
                            )
                        ),
                        'conditions' => array('TypeTran.id' => $memberTypeId,'TypeTran.type_master_id' => Configure::read('MEMBER_TYPE_MASTER_ID'),'RoleMaster.status' => 1),
                        'order' => array('RoleMaster.order_no')
                    );
                    $arrRecords = $this->RoleMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.RoleMaster.id','{n}.RoleMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('member_type_id' => $memberTypeId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_employee_role_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array(
                    'fields' => array('id','name'),
                    'joins' => array(
                        array(
                            'table' => 'type_trans',
                            'alias' => 'TypeTran',
                            'type' => 'INNER',
                            'conditions' => array('RoleMaster.type_tran_id = TypeTran.id','TypeTran.status' => 1)
                        )
                    ),
                    'conditions' => array('TypeTran.role_type' => Configure::read('EMPLOYEE_ROLE_TYPE'),'TypeTran.type_master_id' => Configure::read('MEMBER_TYPE_MASTER_ID'),'RoleMaster.status' => 1),
                    'order' => array('RoleMaster.order_no')
                );
                $arrRecords = $this->RoleMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.RoleMaster.id','{n}.RoleMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_user_type_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','firm_name','unique_no'),'conditions' => array('status' => 1),'order' => array('max_unique_no') );
                $arrRecords = $this->BranchMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.BranchMaster.id','{n}.BranchMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_vehicle_model_list($manufacturerTranId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($manufacturerTranId)) {
                    $options = array(
                        'fields' => array('id','name'),
                        'conditions' => array('status' => 1,'manufacturer_tran_id' => $manufacturerTranId),
                        'order' => array('order_no')
                    );
                    $arrRecords = $this->VehicleModelMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.VehicleModelMaster.id','{n}.VehicleModelMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_employee_list($firmId = 0,$designationId) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $conditions = array('status' => 1,'branch_master_id' => $firmId);
                    if(!empty($designationId)) {
                        $conditions['designation_master_id'] = $designationId;
                    }
                    $options = array('fields' => array('id','employee_name'),'conditions' => $conditions,'order' => array('unique_no'));
                    $arrRecords = $this->Employee->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.Employee.id','{n}.Employee');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_vehicle_specification_list($vehicleTypeId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($vehicleTypeId)) {
                    $options = array(
                        'fields' => array('id','name'),
                        'conditions' => array('VehicleSpecificationMaster.vehicle_type_master_id' => $vehicleTypeId,'VehicleSpecificationMaster.status' => 1),
                        'group' => array('VehicleSpecificationMaster.id'),
                        'order' => array('IFNULL(VehicleSpecificationMaster.order_no,9999)')
                    );
                    $arrRecords = $this->VehicleSpecificationMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.VehicleSpecificationMaster.id','{n}.VehicleSpecificationMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function get_checklist($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                    $arrRecords = $this->InspectionMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.InspectionMaster.id','{n}.InspectionMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select valid branch',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_payment_method_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'type_master_id' => Configure::read('PAYMENT_METHOD')),'order' => array('order_no'));
                $arrRecords = $this->TypeTran->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.TypeTran.id','{n}.TypeTran');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_member_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','firm_name','unique_no'),'conditions' => array('status' => 1),'order' => 'max_unique_no');
                $arrRecords = $this->BranchMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.BranchMaster.id','{n}.BranchMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function get_royalty_charge_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => 'order_no');
                $arrRecords = $this->RoyaltyChargeMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.RoyaltyChargeMaster.id','{n}.RoyaltyChargeMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_coupon_detail($branchId = 0,$serviceType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($branchId)) {
                    $options = array('fields' => array('id','name','amount'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'service_type' => $serviceType),'order' => 'order_no');
                    $arrRecords = $this->CouponMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.CouponMaster.id','{n}.CouponMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_grade_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => 'order_no');
                $arrRecords = $this->GradeMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.GradeMaster.id','{n}.GradeMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_expense_head() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['search']) && !empty($this->request->data['search']) && isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                    $conditions = array('status' => 1,'name LIKE' => '%'.trim($this->request->data['search']).'%','branch_master_id' => $this->request->data['branch_master_id']);
                    if(isset($this->request->data['expense_head_master_id']) && !empty($this->request->data['expense_head_master_id'])) {
                        $conditions['NOT']['id'] = $this->request->data['expense_head_master_id'];
                    }
                    $options = array('fields' => array('id','name','code'),'conditions' => $conditions,'order' => array('order_no'));
                    $arrRecords = $this->ExpenseHeadMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.ExpenseHeadMaster.id','{n}.ExpenseHeadMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please enter any word to search',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_vehicle_service_detail($vehicleId = 0,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($vehicleId) && !empty($branchId)) {
                    $this->loadModel('ExpenseVehicleTran');
                    $options = array('fields' => array('id','invoice_master_id'),'conditions' => array('status' => 1,'branch_master_id' => $branchId));
                    $arrVehicleExpenseData = $this->ExpenseVehicleTran->find('list',$options);
                    $conditions = array('IM.type' => 3,'VehicleJobMaster.vehicle_master_id' => $vehicleId,'IM.branch_master_id' => $branchId);
                    if(!empty($arrVehicleExpenseData)) {
                        $conditions['NOT']['IM.id'] = array_values($arrVehicleExpenseData);
                    }
                    $this->loadModel('VehicleJobMaster');
                    $options = array(
                                    'fields' => array('SUM(IM.amount) AS amount','GROUP_CONCAT(DISTINCT IM.id) as invoice_id'),
                                    'joins' => array(
                                        array(
                                            'table' => 'invoice_masters',
                                            'alias' => 'IM',
                                            'type' => 'INNER',
                                            'conditions' => array('VehicleJobMaster.id = IM.customer_master_id','IM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions
                                );
                    $arrRecords = $this->VehicleJobMaster->find('first',$options);
                    if($arrRecords[0]['amount'] > 0) {
                        $options = array('fields' => array('id','name','code','description'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'type' => 2));
                        $arrVehicleExpenseData = $this->ExpenseHeadMaster->find('first',$options);
                        if(!empty($arrVehicleExpenseData)) {
                            $statusCode = 200;
                            $arrExpenseRecords['id'] = $arrVehicleExpenseData['ExpenseHeadMaster']['id'];
                            $arrExpenseRecords['name'] = $arrVehicleExpenseData['ExpenseHeadMaster']['name'];
                            $arrExpenseRecords['code'] = $arrVehicleExpenseData['ExpenseHeadMaster']['code'];
                            $arrExpenseRecords['description'] = $arrVehicleExpenseData['ExpenseHeadMaster']['description'];
                            $arrExpenseRecords['invoice_id'] = $arrRecords[0]['invoice_id'];
                            $arrExpenseRecords['amount'] = (float) $arrRecords[0]['amount'];
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrExpenseRecords);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_vehicle_expense_detail($vehicleId = 0,$branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($vehicleId) && !empty($branchId)) {
                    $this->loadModel('ExpenseMaster');
                    $options = array(
                                    'fields' => array('EHM.id','EHM.name','EHM.code','ET.description','SUM(ET.price) AS amount'),
                                    'joins' => array(
                                        array(
                                            'table'=> 'expense_trans',
                                            'alias' => 'ET',
                                            'type' =>'INNER',
                                            'conditions' => array('ExpenseMaster.id =ET.expense_master_id','ET.status' => 1)
                                        ),
                                        array(
                                            'table'=> 'expense_head_masters',
                                            'alias' => 'EHM',
                                            'type' =>'INNER',
                                            'conditions' => array('ET.expense_head_master_id =EHM.id','EHM.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('ExpenseMaster.status' => 1,'ExpenseMaster.branch_master_id' => $branchId,'ExpenseMaster.vehicle_master_id' => $vehicleId),
                                    'group' => array('EHM.id'),
                                    'order' => array('EHM.order_no')
                                );
                    $arrVehicleExpenseData = $this->ExpenseMaster->find('all',$options);
                    if(count($arrVehicleExpenseData) > 0) {
                        $arrExpenseHeadData = array();
                        $totalAmount = 0;
                        foreach($arrVehicleExpenseData as $key => $expense) {
                            $arrExpenseHeadData[$expense['EHM']['id']]['id'] = $expense['EHM']['id'];
                            $arrExpenseHeadData[$expense['EHM']['id']]['name'] = $expense['EHM']['name'];
                            $arrExpenseHeadData[$expense['EHM']['id']]['code'] = $expense['EHM']['code'];
                            $arrExpenseHeadData[$expense['EHM']['id']]['description'] = $expense['ET']['description'];
                            $arrExpenseHeadData[$expense['EHM']['id']]['amount'] = (float) number_format($expense[0]['amount'],2,'.','');
                            $totalAmount += $expense[0]['amount'];
                        }
                        $arrRecords['total'] = (float) number_format($totalAmount,2,'.','');
                        $arrRecords['expense_head'] = array_values($arrExpenseHeadData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_agent_list($branchId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($branchId)) {
                    $options = array(
                                'fields' => array('id',"CONCAT_WS(' ',TM.name,first_name,middle_name,last_name) AS name",'mobile_no'),
                                'joins' => array(
                                    array(
                                        'table' => 'title_masters',
                                        'alias' => 'TM',
                                        'conditions' => array('AgentMaster.title_master_id = TM.id','TM.status' => 1)
                                    )
                                ),
                                'conditions' => array('AgentMaster.status' => 1,'branch_master_id' => $branchId),'order' => array('first_name')
                            );
                    $arrRecords = $this->AgentMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrAgentData = array();
                        foreach($arrRecords as $key => $agent) {
                            $arrAgentData[$key]['id'] = $agent['AgentMaster']['id'];
                            $arrAgentData[$key]['name'] = $agent[0]['name'].' ('.$agent['AgentMaster']['mobile_no'].')';
                        }
                        
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrAgentData);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_document_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->DocumentMaster->find('all',$options);
                if(count($arrRecords) > 0) {
                    $arrRecords = Hash::combine($arrRecords,'{n}.DocumentMaster.id','{n}.DocumentMaster');
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function common() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(count($arrRecords) > 0) {
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_category_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get') && !empty($firmId)) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('order_no'));
                $arrRecords = $this->CategoryMaster->find('all',$options);
                $arrRecords = Hash::combine($arrRecords,'{n}.CategoryMaster.id','{n}.CategoryMaster');
                if(count($arrRecords) > 0) {
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function product_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(!empty($firmId)) {
                    $conditions = array('ProductMaster.status' => 1,'ProductMaster.branch_master_id' => $firmId,'CategoryMaster.branch_master_id' => $firmId,'CategoryMaster.status' => 1);
                    if(isset($this->request->data['searchText']) && !empty($this->request->data['searchText'])) {
                        $conditions['ProductMaster.name LIKE'] =  '%'.trim($this->request->data['searchText']).'%';
                    }

                    if(isset($this->request->data['type']) && !empty($this->request->data['type'])) {
                        $conditions['ProductMaster.type'] =  $this->request->data['type'];
                    }

                    if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                        $conditions['ProductMaster.category_master_id'] =  $this->request->data['category_master_id'];
                    }

                    $this->ProductMaster->virtualFields = array('product_variant_tran_id' => 'ProductVariantTran.id');
                    $options = array(
                                    'fields' => array('id','name','code','hsn_code','sale_price','product_variant_tran_id','is_taxable'),
                                    'joins' => array(
                                        array(
                                            'table' => 'category_masters',
                                            'alias' => 'CategoryMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id')
                                        ),
                                        array(
                                            'table' => 'product_variant_trans',
                                            'alias' => 'ProductVariantTran',
                                            'type' => 'INNER',
                                            'conditions' => array('ProductMaster.id = ProductVariantTran.product_master_id')
                                        )
                                    ),
                                    'conditions' => $conditions,'order' => array('ProductMaster.order_no')
                                );
                    $arrRecords = $this->ProductMaster->find('all',$options);
                    $arrRecords = Hash::combine($arrRecords,'{n}.ProductMaster.id','{n}.ProductMaster');
                    if(count($arrRecords) > 0) {
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_BRANCH',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function product_stock_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(!empty($firmId)) {
                    $conditions = array('ProductStockTran.branch_master_id' => $firmId);
                    if(isset($this->request->data['searchText']) && !empty($this->request->data['searchText'])) {
                        $conditions['ProductStockTran.name LIKE'] =  '%'.trim($this->request->data['searchText']).'%';
                    }

                    if(isset($this->request->data['stock_type']) && !empty($this->request->data['stock_type'])) {
                        $conditions['ProductStockTran.stock_type'] =  $this->request->data['stock_type'];
                    }

                    if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                        $conditions['NOT']['ProductStockTran.id'] =  $this->request->data['id'];
                    }

                    $options = array(
                        'fields' => array('name','code','product_id','hsn_code','id','price','IFNULL(SUM(quantity),0) AS stock','tax_group_master_id'),
                        'conditions' => $conditions,
                        'group' => 'ProductStockTran.id',
                        'having' => 'stock > 0',
                        'order' => array('ProductStockTran.date ASC')
                    );
                    $arrProductRecords = $this->ProductStockTran->find('all',$options);
                    if(count($arrProductRecords) > 0) {
                        $arrRecords = array();
                        $count = 0;
                        $arrProductId = array();
                        foreach($arrProductRecords as $key => $product) {
                            if(!isset($arrProductId[$product['ProductStockTran']['product_id']])) {
                                $arrRecords[$count]['name'] = $product['ProductStockTran']['name'];
                                $arrRecords[$count]['code'] = $product['ProductStockTran']['code'];
                                $arrRecords[$count]['hsn_code'] = $product['ProductStockTran']['hsn_code'];
                                $arrRecords[$count]['price'] = (float) $product['ProductStockTran']['price'];
                                $arrRecords[$count]['product_variant_tran_id'] = $product['ProductStockTran']['id'];
                                $arrRecords[$count]['tax_group_master_id'] = $product['ProductStockTran']['tax_group_master_id'];
                                $arrRecords[$count]['stock'] = (int) $product[0]['stock'];
                                $arrProductId[$product['ProductStockTran']['product_id']] = $product['ProductStockTran']['product_id'];
                                ++$count;
                            }
                        }
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_BRANCH',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function product_tax_detail($firmId = 0,$taxGroupId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId) && !empty($taxGroupId)) {
                    $options = array(
                                    'fields' => array('id','name','type','value'),
                                    'joins' => array(
                                        array(
                                            'table' => 'tax_group_trans',
                                            'alias' => 'TGT',
                                            'type' => 'INNER',
                                            'conditions' => array('TaxMaster.id = TGT.tax_master_id','TGT.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('TaxMaster.branch_master_id' => $firmId,'TGT.tax_group_master_id' => $taxGroupId),
                                    'order' => 'TaxMaster.order_no'
                                );
                    $arrRecords = $this->TaxMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.TaxMaster.id','{n}.TaxMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_BRANCH',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function supplier_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    //$this->SupplierMaster->virtualFields = array('name' => "CONCAT_WS(' ',TM.name,SupplierMaster.first_name,SupplierMaster.middle_name,SupplierMaster.last_name)"); 
                    /* 
                     joins' => array(
                        array(
                            'table' => 'title_masters',
                            'alias' => 'TM',
                            'type' => 'LEFT',
                            'conditions' => array('SupplierMaster.title_master_id = TM.id','TM.status' => 1)
                        )
                    ),
                    */
                    $options = array(
                                    'fields' => array('id',"firm_name as name"),
                                    'conditions' => array('SupplierMaster.status' => 1,'SupplierMaster.branch_master_id' => $firmId),
                                    'order' => array('SupplierMaster.id')
                                );
                    $arrRecords = $this->SupplierMaster->find('all',$options);
                    $arrRecords = Hash::combine($arrRecords,'{n}.SupplierMaster.id','{n}.SupplierMaster');
                    if(count($arrRecords) > 0) {
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_customer_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('id'));
                    $arrRecords = $this->Customer->find('all',$options);
                    $arrRecords = Hash::combine($arrRecords,'{n}.Customer.id','{n}.Customer');
                    if(count($arrRecords) > 0) {
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select valid branch',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_customer() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['branch_master_id']) && isset($this->request->data['search_text']) && !empty($this->request->data['search_text']) && !empty($this->request->data['branch_master_id'])) {
                    $branchId = (int) $this->request->data['branch_master_id'];
                    $searchText = addslashes(trim($this->request->data['search_text']));
                    $query = "SELECT `id`,`name`,`email_id`,`mobile_no` FROM customers WHERE branch_master_id = $branchId AND first_name LIKE '%".$searchText."%'
                            UNION
                            SELECT `id`,`name`,`email_id`,`mobile_no` FROM customers WHERE branch_master_id = $branchId AND middle_name LIKE '%".$searchText."%'
                            UNION
                            SELECT `id`,`name`,`email_id`,`mobile_no` FROM customers WHERE branch_master_id = $branchId AND last_name LIKE '%".$searchText."%'
                            UNION
                            SELECT `id`,`name`,`email_id`,`mobile_no` FROM customers WHERE branch_master_id = $branchId AND mobile_no LIKE '%".$searchText."%'
                            UNION 
                            SELECT `id`,`name`,`email_id`,`mobile_no` FROM customers WHERE branch_master_id = $branchId AND email_id LIKE '%".$searchText."%';";
                    $arrCustomerRecords = $this->User->query($query,false);
                    if(count($arrCustomerRecords) > 0) {
                        $arrRecords = array();
                        foreach($arrCustomerRecords as $key => $data) {
                            $arrRecords[$key]['id'] = $data[0]['id'];
                            $arrRecords[$key]['name'] = $data[0]['name'];
                            $arrRecords[$key]['mobile_no'] = $data[0]['mobile_no'];
                            $arrRecords[$key]['email_id'] = $data[0]['email_id'];
                        }
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please enter any keyword to continue ..',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function get_expensehead_list($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'branch_master_id' => $firmId),'order' => array('order_no'));
                    $arrRecords = $this->ExpenseHeadMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrRecords = Hash::combine($arrRecords,'{n}.ExpenseHeadMaster.id','{n}.ExpenseHeadMaster');
                        $arrRecords = array_values($arrRecords);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('firm_id' => $firmId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_bank_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','name'),'conditions' => array('status' => 1),'order' => array('id'));
                $arrRecords = $this->BankMaster->find('all',$options);
                $arrRecords = Hash::combine($arrRecords,'{n}.BankMaster.id','{n}.BankMaster');
                if(count($arrRecords) > 0) {
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_branch_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','firm_name AS name'),'conditions' => array('status' => 1),'order' => array('id'));
                $arrRecords = $this->BranchMaster->find('all',$options);
                $arrRecords = Hash::combine($arrRecords,'{n}.BranchMaster.id','{n}.BranchMaster');
                if(count($arrRecords) > 0) {
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_company_year_list() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','caption AS name'),'conditions' => array('status' => 1),'order' => array('order_no'));
                $arrRecords = $this->CompanyYearMaster->find('all',$options);
                $arrRecords = Hash::combine($arrRecords,'{n}.CompanyYearMaster.id','{n}.CompanyYearMaster');
                if(count($arrRecords) > 0) {
                    $arrRecords = array_values($arrRecords);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function product_detail($id = null,$type = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id)) {
                    $options = array('fields' => array('id','tax_group_master_id','sale_price'),'conditions' => array('status' => 1,'id' => $id),'order' => array('order_no'));
                    $arrPartRecrods = $this->ProductMaster->find('first',$options);
                    if(!empty($arrPartRecrods)) {
                        $stockQty = 0;
                        if($type == 1) {
                            $stockQty = $this->AppUtilities->getProductStockQty($id);
                        }
                        $statusCode = 200;
                        $arrTaxDetail = array();
                        $price = number_format($arrPartRecrods['ProductMaster']['sale_price'],2,'.','');
                        $arrRecords['price'] = $price;
                        $arrRecords['stock_qty'] = $stockQty;
                        $taxPrice = 0;
                        $taxName = '';
                        if(!empty($arrPartRecrods['ProductMaster']['tax_group_master_id']) && $arrPartRecrods['ProductMaster']['tax_group_master_id'] > 0) {
                            $taxGroupId = (int) $arrPartRecrods['ProductMaster']['tax_group_master_id'];
                            $options = array(
                                'fields' => array('TaxGroupMaster.name','TaxMaster.type','TaxMaster.id','TaxMaster.name','TaxMaster.value'),
                                'joins' => array(
                                    array(
                                        'table' =>'tax_group_trans',
                                        'alias' =>'TaxGroupTran',
                                        'type' => 'INNER',
                                        'conditions' => array('TaxGroupMaster.id = TaxGroupTran.tax_group_master_id','TaxGroupTran.status' => 1)
                                    ),
                                    array(
                                        'table' =>'tax_masters',
                                        'alias' =>'TaxMaster',
                                        'type' => 'INNER',
                                        'conditions' => array('TaxMaster.id = TaxGroupTran.tax_master_id','TaxMaster.status' => 1)
                                    )
                                ),
                                'conditions' => array('TaxGroupMaster.status' => 1,'TaxGroupMaster.id' => $taxGroupId)
                            );
                            $arrTaxGroupData = $this->TaxGroupMaster->find('all',$options);
                            if(count($arrTaxGroupData) > 0) {
                                $taxName = $arrTaxGroupData[0]['TaxGroupMaster']['name'];
                                $arrTaxDetail = Hash::combine($arrTaxGroupData,'{n}.TaxMaster.id','{n}.TaxMaster');
                                $arrTaxDetail = array_values($arrTaxDetail);
                            }
                        }
                        $arrRecords['tax_detail'] = $arrTaxDetail;
                        $arrRecords['tax_name'] = $taxName;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function upload_files() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                    if($_FILES['file']['error'] == 0) {
                        $imageName = md5(uniqid('',true));
                        $path = Configure::read('UPLOAD_TMP_DIR').'/';
                        $fileExt = strtolower(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION));
                        $arrExtension = array('jpg','jpeg','png','gif');
                        $fileLocation = $path.$imageName.'.'.$fileExt;
                        $fileName = $imageName.'.'.$fileExt;
                        if(in_array($fileExt,$arrExtension)) {
                            $flag = 1;
                            if($_FILES['file']['size'] > 300000) {
                                $this->__compress_image($_FILES['file']['tmp_name'],$fileLocation,75);
                            } else {
                                if(!@move_uploaded_file($_FILES['file']['tmp_name'],$fileLocation)) {
                                    $flag = 0;
                                }
                            }
                            if($flag === 1) {
                                if(isset($this->request->data['old_filename']) && !empty($this->request->data['old_filename'])) {
                                    @unlink($path.$this->request->data['old_filename']);
                                }
                                $statusCode = 200;
                                $response = array('status' => 1,'message' => __('File uploaded successfully !.',true),'file' => $fileName );
                            } else {
                                $response = array('status' => 0,'message' => __('Sorry, there was an error uploading your file.',true));
                            }
                        } else {
                            $response = array('status' => 0,'message' => __('Please upload valid file format (*.jpg,*.png,*.gif)',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('Error occured while uploading file',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    private function __compress_image($source_url,$destination_url,$quality) {
        $info = getimagesize($source_url); 
        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source_url); 
        } else if ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source_url);
        } else if ($info['mime'] == 'image/png')  {
            $image = imagecreatefrompng($source_url); 
        } else {}
        imagejpeg($image, $destination_url, $quality);
        return $destination_url;
    }

    public function member_unique_no() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $arrMemberData = $this->AppUtilities->generateMemberUniqueNo();
                if(!empty($arrMemberData)) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'code' => $arrMemberData['code']);
                } else {
                    $response = array('status' => 0,'message' => __('Please define member code setting',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function company_settings() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get') && $this->Session->check('sessUserId')) {
                $options = array('fields' => array('id','user_type'),'conditions' => array('id' => $this->Session->read('sessUserId'),'status' => 1));
                $arrUserData = $this->User->find('first',$options);
                if(!empty($arrUserData)) {
                    $userType = (int)$arrUserData['User']['user_type'];
                    $options = array('fields' => array('id','caption'),'conditions' => array('status' => 1 ,'is_current' => 1));
                    $arrCompanyYearData = $this->CompanyYearMaster->find('first',$options);
                    $companyData = (!empty($arrCompanyYearData)) ? $arrCompanyYearData['CompanyYearMaster'] : (object) array();
    
                    if(in_array($userType,array(3,4))) {
                        $arrMemberDetail = $this->UserManagement->getUserMemberDetail($arrUserData['User']['id'],$arrUserData['User']['user_type']);
                        if(!empty($arrMemberDetail)) {
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'member' => $arrMemberDetail,'company' => $companyData,'default_memer' => '');
                        } else {
                            $response = array('status' => -1,'message' => __('INVALID_RESPONSE',true));
                        }
                    } else {
                        $options = array('fields' => array('id','firm_name','unique_no'),'conditions' => array('status' => 1 ));
                        $arrMemberData = $this->BranchMaster->find('all',$options);
                        $defaultMemberId = '';
                        if(!empty($arrMemberData)) {
                            $defaultMemberId = $arrMemberData[0]['BranchMaster']['id'];
                            $arrMemberData = Hash::combine($arrMemberData,'{n}.BranchMaster.id','{n}.BranchMaster');
                            $arrMemberData = array_values($arrMemberData);
                        }
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'member' => $arrMemberData,'company' => $companyData,'default_memer' => $defaultMemberId);
                    }
                } else {
                    $response = array('status' => -1,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function user_setting_unique_no($firmId = null,$type = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $arrUserSettingData = $this->AppUtilities->getUserSettingCode($firmId,$type);
                    if(!empty($arrUserSettingData)) {
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'code' => $arrUserSettingData['code']);
                    } else {
                        $response = array('status' => 0,'message' => __('Please add job code from user setting !..',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('invalid Member ID',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function vehicle_unique_code() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $arrRecords = $this->AppUtilities->getVehicleUniqueNo();
                if(!empty($arrRecords)) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'code' => $arrRecords['code']);
                } else {
                    $response = array('status' => 0,'message' => __('Please add vehicle code first',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_vehicle() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['search']) && isset($this->request->data['vehicle_type']) && !empty($this->request->data['vehicle_type']) && !empty($this->request->data['search'])) {
                    $vehicleKeyword = addslashes(trim($this->request->data['search']));
                    $vehicleType = (int) $this->request->data['vehicle_type'];
                    $branchId = (isset($this->request->data['branch_master_id'])) ? (int) $this->request->data['branch_master_id'] : 0;
                    if($vehicleType === 1) {
                        $conditions = " AND vehicles.vehicle_status = 1 AND vehicles.vehicle_type = 3 AND vehicles.vehicle_from = 1 AND vehicles.branch_master_id = ".$branchId;
                    } else if($vehicleType === 2) {
                        $conditions = " AND vehicles.vehicle_status = 1 AND vehicles.vehicle_type = 2 AND vehicles.vehicle_from = 1 AND vehicles.branch_master_id = ".$branchId;
                    } else if($vehicleType === 3) {
                        $conditions = " AND ( (vehicles.vehicle_from = 1 AND vehicles.vehicle_status IN (1, 2) AND vehicles.branch_master_id = '".$branchId."') OR (vehicles.vehicle_from = 1 AND vehicles.vehicle_status = 2 AND vehicles.branch_master_id <> '".$branchId."') OR (vehicles.vehicle_from = 2  AND vehicles.vehicle_status = 1))";
                    } else if($vehicleType === 4) {
                        $conditions = " AND vehicles.vehicle_status = 1 AND vehicles.vehicle_type = 1 AND vehicles.vehicle_from = 2";
                    } else if($vehicleType === 5) {
                        $conditions = " AND vehicles.vehicle_status = 1 AND vehicles.vehicle_type IN(2,3) AND vehicles.vehicle_from = 1 AND vehicles.branch_master_id = ".$branchId;
                    } else if($vehicleType === 6) {
                        $conditions = " AND vehicles.vehicle_from = 1 AND vehicles.branch_master_id = ".$branchId;
                    } else if($vehicleType === 7) {
                        $conditions = " AND vehicles.vehicle_type = 2 AND vehicles.vehicle_from = 1 AND vehicles.branch_master_id = ".$branchId;
                    } else {
                        $conditions = "";
                    }

                    $conditions .= " AND vehicles.is_active = 1";
                    $query = "SELECT id,unique_no,vehicle_number,manufacturer_year,engine_number,chasis_number,vehicle_model_name,manufacturer_name,vehicle_type_name,
                              cust_name,cust_mobile_no,cust_email_id,cust_address,vehicle_from,vehicle_status,purchase_price,sale_price
                              FROM vehicles WHERE status = 1 AND vehicle_number LIKE '%".$vehicleKeyword."%' $conditions
                              UNION
                              SELECT id,unique_no,vehicle_number,manufacturer_year,engine_number,chasis_number,vehicle_model_name,manufacturer_name,vehicle_type_name,
                              cust_name,cust_mobile_no,cust_email_id,cust_address,vehicle_from,vehicle_status,purchase_price,sale_price
                              FROM vehicles WHERE status = 1 AND unique_no LIKE '%".$vehicleKeyword."%' $conditions
                              UNION
                              SELECT id,unique_no,vehicle_number,manufacturer_year,engine_number,chasis_number,vehicle_model_name,manufacturer_name,vehicle_type_name,
                              cust_name,cust_mobile_no,cust_email_id,cust_address,vehicle_from,vehicle_status,purchase_price,sale_price
                              FROM vehicles WHERE status = 1 AND engine_number LIKE '%".$vehicleKeyword."%' $conditions
                              UNION
                              SELECT id,unique_no,vehicle_number,manufacturer_year,engine_number,chasis_number,vehicle_model_name,manufacturer_name,vehicle_type_name,
                              cust_name,cust_mobile_no,cust_email_id,cust_address,vehicle_from,vehicle_status,purchase_price,sale_price
                              FROM vehicles WHERE status = 1 AND chasis_number LIKE '%".$vehicleKeyword."%' $conditions";
                    #echo $query;exit;
                    $result = $this->User->query($query,false);
                    if(!empty($result)) {
                        $vehicleRecords = array();
                        foreach($result as $key => $vehicle) {
                            $vehicleRecords[$key]['id'] = $vehicle[0]['id'];
                            $vehicleRecords[$key]['vehicle_number'] = $vehicle[0]['vehicle_number'];
                            $vehicleRecords[$key]['unique_no'] = $vehicle[0]['unique_no'];
                            $vehicleRecords[$key]['engine_number'] = $vehicle[0]['engine_number'];
                            $vehicleRecords[$key]['chasis_number'] = $vehicle[0]['chasis_number'];
                            $vehicleRecords[$key]['name'] = $vehicle[0]['cust_name'];
                            $vehicleRecords[$key]['mobile_no'] = $vehicle[0]['cust_mobile_no'];
                            $vehicleRecords[$key]['email_id'] = $vehicle[0]['cust_email_id'];
                            $vehicleRecords[$key]['address'] = $vehicle[0]['cust_address'];
                            $vehicleRecords[$key]['model'] = $vehicle[0]['vehicle_model_name'];
                            $vehicleRecords[$key]['manufacturer'] = $vehicle[0]['manufacturer_name'];
                            $vehicleRecords[$key]['vehicle_type'] = $vehicle[0]['vehicle_type_name'];
                            $vehicleRecords[$key]['purchase_price'] = (float)$vehicle[0]['purchase_price'];
                            $vehicleRecords[$key]['sale_price'] = (float)$vehicle[0]['sale_price'];
                            $vehicleRecords[$key]['vehicle_from'] = (int)$vehicle[0]['vehicle_from'];
                            $vehicleRecords[$key]['vehicle_status'] = (int)$vehicle[0]['vehicle_status'];
                            if($vehicle[0]['vehicle_from'] == 2 && $vehicle[0]['vehicle_status'] == 1) {
                                $inhouse = 0;
                            } else if($vehicle[0]['vehicle_from'] == 1 && $vehicle[0]['vehicle_status'] == 1) {
                                $inhouse = 1;
                            } else if($vehicle[0]['vehicle_from'] == 1 && $vehicle[0]['vehicle_status'] == 2) {
                                $inhouse = 0;
                            } else {
                                $inhouse = 0;
                            }
                            $vehicleRecords[$key]['in_house_vehicle'] = (int)$inhouse;
                        }
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $vehicleRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('Please enter any word to search',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function send_mail($userType = 1,$id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id)) {
                    $userType = (int)$userType;
                    if($userType === 1) {
                        $options = array(
                                    'fields' => array('User.username','User.encrypt','email_id','first_name','middle_name','last_name','TitleMaster.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'users',
                                            'alias' => 'User',
                                            'type' => 'INNER',
                                            'conditions' => array('BranchMaster.user_id = User.id','User.status' => 1)
                                        ),
                                        array(
                                            'table' => 'title_masters',
                                            'alias' => 'TitleMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('BranchMaster.title_master_id = TitleMaster.id','TitleMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('BranchMaster.id' => $id,'BranchMaster.status' => 1)
                                );
                        $arrUserMemberData = $this->BranchMaster->find('first',$options);
                        if(!empty($arrUserMemberData)) {
                            $this->Email = $this->Components->load('Email');
                            $userName = $arrUserMemberData['User']['username'];
                            $pwd = $this->decrypt_algo($arrUserMemberData['User']['encrypt']);
                            $name = $arrUserMemberData['TitleMaster']['name'].' '.$arrUserMemberData['BranchMaster']['first_name'].' '.$arrUserMemberData['BranchMaster']['middle_name'].' '.$arrUserMemberData['BranchMaster']['last_name'];
                            $config['from_email'] = $arrUserMemberData['BranchMaster']['email_id'];
                            $config['subject'] = 'Radizo Franchise Registration';
                            $config['content'] = '<div>
                                                    <div style="font-size:24px;font-weight:bold;margin-bottom:10px;">Thank you for Registration</div>
                                                    <div style="font-size:18px;font-weight:bold;margin-bottom:10px;">
                                                        Dear '.$name.' <br>Your registration has been done successfully!.
                                                    </div>
                                                    <div style="margin-bottom:10px;">
                                                        You can manage your account on the go by clicking on
                                                    </div>
                                                    <div style="margin-bottom:10px;">
                                                        for Admin login your user id and password mention below:	
                                                    </div>
                                                    <div>
                                                        User ID : <b>'.$userName.'</b><br/>
                                                        Password : <b>'.$pwd.'</b>
                                                    </div>
                                                </div>';
                            $this->Email->sendEmail($config);
                            $response = array('status' => 1,'message' => __('mail sent successfully!.',true));
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else if($userType === 2) {
                        $options = array(
                            'fields' => array('User.username','User.encrypt','email_id','first_name','middle_name','last_name','TitleMaster.name'),
                            'joins' => array(
                                array(
                                    'table' => 'users',
                                    'alias' => 'User',
                                    'type' => 'INNER',
                                    'conditions' => array('EmployeeMaster.user_id = User.id','User.status' => 1)
                                ),
                                array(
                                    'table' => 'title_masters',
                                    'alias' => 'TitleMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('EmployeeMaster.title_master_id = TitleMaster.id','TitleMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('EmployeeMaster.id' => $id,'EmployeeMaster.status' => 1)
                        );
                        $arrEmployeeData = $this->EmployeeMaster->find('first',$options);
                        if(!empty($arrEmployeeData)) {
                            if(!empty($arrEmployeeData['EmployeeMaster']['email_id'])) {
                                $this->Email = $this->Components->load('Email');
                                $userName = $arrEmployeeData['User']['username'];
                                $pwd = $this->decrypt_algo($arrEmployeeData['User']['encrypt']);
                                $name = $arrEmployeeData['TitleMaster']['name'].' '.$arrEmployeeData['EmployeeMaster']['first_name'].' '.$arrEmployeeData['EmployeeMaster']['middle_name'].' '.$arrEmployeeData['EmployeeMaster']['last_name'];
                                $config['from_email'] = $arrEmployeeData['EmployeeMaster']['email_id'];
                                $config['subject'] = 'Radizo Employee Registration';
                                $config['content'] = '<div>
                                                        <div style="font-size:24px;font-weight:bold;margin-bottom:10px;">Thank you for Registration</div>
                                                        <div style="font-size:18px;font-weight:bold;margin-bottom:10px;">
                                                            Dear '.$name.' <br>Your registration has been done successfully!.
                                                        </div>
                                                        <div style="margin-bottom:10px;">
                                                            You can manage your account on the go by clicking on
                                                        </div>
                                                        <div style="margin-bottom:10px;">
                                                            for Admin login your user id and password mention below:	
                                                        </div>
                                                        <div>
                                                            User ID : <b>'.$userName.'</b><br/>
                                                            Password : <b>'.$pwd.'</b>
                                                        </div>
                                                    </div>';
                                $this->Email->sendEmail($config);
                            }
                            $response = array('status' => 1,'message' => __('Mail sent successfully!.',true));
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id'=>$id,'user_type' => $userType),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function report_header($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $arrRecords = $this->AppUtilities->getReportHeaderDetail($firmId);
                    if(!empty($arrRecords)) {
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array(),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_result() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['search_type']) && !empty($this->request->data['search_type'])) {
                    $searchType = (string) $this->request->data['search_type'];
                    $searchValue = (string) trim($this->request->data['search_value']);
                    $arrRecords = $options =  array();
                    $model = '';
                    switch($searchType) {
                        case 'country':
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    $model = 'CountryMaster';
                                    break;
                        case 'state':
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    $model = 'StateMaster';
                                    break;
                        case 'vehicle_type':
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    $model = 'VehicleTypeMaster';
                                    break;
                        case 'manufacturer':
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    $model = 'ManufacturerMaster';
                                    break;
                        case 'vehicle_code':
                                    $branchId = (isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) ? $this->request->data['branch_master_id'] : 0;
                                    $model = 'Vehicle';
                                    $options = array('fields' => array('id','unique_no as name'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'unique_no LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'vehicle_number':
                                    $branchId = (isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) ? $this->request->data['branch_master_id'] : 0;
                                    $model = 'Vehicle';
                                    $options = array('fields' => array('id','vehicle_number as name'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'vehicle_number LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'vehicle_model':
                                    $model = 'VehicleModelMaster';
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'engine_no':
                                    $branchId = (isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) ? $this->request->data['branch_master_id'] : 0;
                                    $model = 'Vehicle';
                                    $options = array('fields' => array('id','engine_number as name'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'engine_number LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'chasis_no':
                                    $branchId = (isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) ? $this->request->data['branch_master_id'] : 0;
                                    $model = 'Vehicle';
                                    $options = array('fields' => array('id','chasis_number as name'),'conditions' => array('status' => 1,'branch_master_id' => $branchId,'chasis_number LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'department':
                                    $model = 'DepartmentMaster';
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'designation':
                                    $model = 'DesignationMaster';
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    break;
                        case 'parent_link':
                                    $model = 'RoleLinkMaster';
                                    $options = array('fields' => array('id','name'),'conditions' => array('status' => 1,'name LIKE' => '%'.$searchValue.'%'));
                                    break;
                        default:
                                    break;
                    }
                    if(count($options) > 0) {
                        $arrRecords = $this->{$model}->find('all',$options);
                        if(count($arrRecords) > 0) {
                            $arrRecords = Hash::combine($arrRecords,'{n}.'.$model.'.id','{n}.'.$model);
                            $arrRecords = array_values($arrRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete_product_tmp_stock($id = 0) {
        try {
            if($this->request->is('get')) {
                $this->loadModel('TmpProductVariantStockTran');
                $dataSource = $this->TmpProductVariantStockTran->getDataSource();
                try {
                    $dataSource->begin();
                    $arrDeleteParams = array('user_id' => $this->Session->read('sessUserId'));
                    if(!empty($id)) {
                        $arrDeleteParams['product_variant_tran_id'] = $id; 
                    }
                    $this->TmpProductVariantStockTran->deleteAll($arrDeleteParams,false);
                    $dataSource->commit();
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('Tmp product deleted !.',true));
                } catch(Exception $e) {
                    $dataSource->rollback();
                    $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                    $this->ErrorLog->saveErrorLog($arrErrorLogs);
                    $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
