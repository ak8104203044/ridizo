<?php
App::uses('AppController','Controller');
class VehicleFinanceMastersController extends AppController {
    public $name = 'VehicleFinanceMasters';
    public $layout = false;
    public $uses = array('VehicleFinanceMaster','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '0';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '0';
                $conditions = array('VehicleFinanceMaster.status' => 1,'VehicleFinanceMaster.branch_master_id' => $firmId,'VehicleFinanceMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['loan_date']) && !empty($this->request->data['loan_date'])) {
                    $conditions['VehicleRentMaster.date_of_loan'] = $this->AppUtilities->date_format(trim($this->request->data['loan_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['vehicle_master_id']) && !empty($this->request->data['vehicle_master_id'])) {
                    $conditions['VehicleRentMaster.vehicle_master_id'] = trim($this->request->data['vehicle_master_id']);
                }

                if(isset($this->request->data['first_name']) && !empty($this->request->data['first_name'])) {
                    $conditions['VehicleRentMaster.first_name LIKE'] = '%'.trim($this->request->data['first_name']).'%';
                }

                if(isset($this->request->data['last_name']) && !empty($this->request->data['last_name'])) {
                    $conditions['VehicleRentMaster.last_name LIKE'] = '%'.trim($this->request->data['last_name']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('VehicleFinanceMaster.date_of_loan '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('VM.vehicle_number '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('VehicleFinanceMaster.first_name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('VehicleFinanceMaster.date_of_loan '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('VehicleFinanceMaster.date_of_loan ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'vehicle_masters',
                                                    'alias' => 'VM',
                                                    'type' => 'INNER',
                                                    'conditions' => array('VehicleFinanceMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                                )
                                            ),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->VehicleFinanceMaster->find('count',$tableCountOptions);
                    $displayLength = isset($this->request->data['length'])?intval($this->request->data['length']):0;
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','date_of_loan','loan_tenure','loan_amount','CM.name','VM.unique_no','VM.vehicle_number'),
                                    'joins' => array(
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'INNER',
                                            'conditions' => array('VehicleFinanceMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                        ),
                                        array(
                                            'table' => 'customers',
                                            'alias' => 'CM',
                                            'type' => 'INNER',
                                            'conditions' => array('VehicleFinanceMaster.customer_master_id = CM.id','CM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'VehicleFinanceMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->VehicleFinanceMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['VehicleFinanceMaster']['id']);
                        $customerName = $tableDetails['CM']['name'];
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['loan_date'] = date('d-M-Y h:i:s A',strtotime($tableDetails['VehicleFinanceMaster']['date_of_loan']));
                        $records[$key]['vehicle'] = $tableDetails['VM']['vehicle_number'];
                        $records[$key]['customer'] = $customerName;
                        $records[$key]['amount'] = $tableDetails['VehicleFinanceMaster']['loan_amount'];
                        $records[$key]['tenure'] = $tableDetails['VehicleFinanceMaster']['loan_tenure'];
                        $records[$key]['is_exists'] = 0;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','loan_date'=>'Loan Date','vehicle'=>'Vehicle Number','customer'=>'Customer','amount' => 'Loan Amount','tenure' => 'Tenure');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['VehicleFinanceMaster'] = $this->request->data;
                if(isset($arrSaveRecords['VehicleFinanceMaster']['id']) && !empty($arrSaveRecords['VehicleFinanceMaster']['id'])) {
                    $arrSaveRecords['VehicleFinanceMaster']['id'] = $this->decryption($arrSaveRecords['VehicleFinanceMaster']['id']);
                }
                $this->VehicleFinanceMaster->set($arrSaveRecords);
                if($this->VehicleFinanceMaster->validates()) {
                    $dataSource = $this->VehicleFinanceMaster->getDataSource();
                    $fieldList = array('id','date_of_loan','loan_tenure','loan_amount','emi_amount','down_payment','vehicle_master_id','customer_master_id','company_year_master_id','branch_master_id');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['VehicleFinanceMaster']['id']) || empty($arrSaveRecords['VehicleFinanceMaster']['id'])) {
                            $this->VehicleFinanceMaster->create();
                        }
                        $arrSaveRecords['VehicleFinanceMaster']['date_of_loan'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleFinanceMaster']['date_of_loan'],'yyyy-mm-dd');
                        #pr($arrSaveRecords);exit;
                        if(!$this->VehicleFinanceMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleFinanceMaster']['id']) && !empty($arrSaveRecords['VehicleFinanceMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                        unset($this->request->data);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleFinanceMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$firmId = 0,$companyYearId = 0) {
        $response = array('status' => 0,'message' => 'Invalid response');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId) && !empty($companyYearId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('id','date_of_loan','loan_tenure','loan_amount','emi_amount','down_payment','vehicle_master_id','customer_master_id','CM.name','CM.mobile_no','CM.email_id','CM.residential_address','CM.permanent_address','VM.unique_no','VM.vehicle_number'),
                            'joins' => array(
                                array(
                                    'table' => 'vehicle_masters',
                                    'alias' => 'VM',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleFinanceMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                ),
                                array(
                                    'table' => 'customers',
                                    'alias' => 'CM',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleFinanceMaster.customer_master_id = CM.id','CM.status' => 1)
                                )
                            ),
                            'conditions' => array('VehicleFinanceMaster.status' => 1,'VehicleFinanceMaster.id' => $id,'VehicleFinanceMaster.branch_master_id' => $firmId,'VehicleFinanceMaster.company_year_master_id' => $companyYearId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->VehicleFinanceMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            $arrRecords['VehicleFinanceMaster']['customer'] = $arrRecords['CM']['name'];
                            $arrRecords['VehicleFinanceMaster']['email_id'] = $arrRecords['CM']['email_id'];
                            $arrRecords['VehicleFinanceMaster']['mobile_no'] = $arrRecords['CM']['mobile_no'];
                            $arrRecords['VehicleFinanceMaster']['add1'] = $arrRecords['CM']['residential_address'];
                            $arrRecords['VehicleFinanceMaster']['add2'] = $arrRecords['CM']['permanent_address'];
                            $arrRecords['VehicleFinanceMaster']['vehicle_code'] = $arrRecords['VM']['unique_no'];
                            $arrRecords['VehicleFinanceMaster']['vehicle_number'] = $arrRecords['VM']['vehicle_number'];
                            $arrRecords['VehicleFinanceMaster']['date_of_loan'] = $this->AppUtilities->date_format($arrRecords['VehicleFinanceMaster']['date_of_loan'],'dd-mm-yyyy');
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleFinanceMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0 && !empty($firmId)) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->VehicleFinanceMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['VehicleFinanceMaster.status'] = -1;
                        $updateFields['VehicleFinanceMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['VehicleFinanceMaster.id'] = $this->request->data['id'];
                        $updateParams['VehicleFinanceMaster.branch_master_id'] = $firmId;
                        if(!$this->VehicleFinanceMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_finance_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
