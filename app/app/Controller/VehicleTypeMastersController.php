<?php
App::uses('AppController','Controller');
class VehicleTypeMastersController extends AppController {
    public $name = 'VehicleTypeMasters';
    public $layout = false;
    public $uses = array('VehicleTypeMaster','ManufacturerTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('VehicleTypeMaster.status' => 1);
                $arrSearchFields = array(1 =>'VehicleTypeMaster.name',2 => 'VehicleTypeMaster.code');
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['VehicleTypeMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['VehicleTypeMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('VehicleTypeMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('VehicleTypeMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('VehicleTypeMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('VehicleTypeMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->VehicleTypeMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','COUNT(MFT.id) AS count'),
                                    'joins' =>array(
                                        array(
                                            'table' => 'manufacturer_trans',
                                            'alias' => 'MFT',
                                            'type' => 'LEFT',
                                            'conditions' => array('VehicleTypeMaster.id = MFT.vehicle_type_master_id','MFT.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'VehicleTypeMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->VehicleTypeMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['VehicleTypeMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['VehicleTypeMaster']['name'];
                        $records[$key]['code'] = $tableDetails['VehicleTypeMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['VehicleTypeMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['VehicleTypeMaster']['order_no'] > $maxOrderNo) ? $tableDetails['VehicleTypeMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_type_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['VehicleTypeMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['VehicleTypeMaster']['id']) && !empty($arrSaveRecords['VehicleTypeMaster']['id'])) {
                    $arrSaveRecords['VehicleTypeMaster']['id'] = $this->decryption($arrSaveRecords['VehicleTypeMaster']['id']);
                }
                $this->VehicleTypeMaster->set($arrSaveRecords);
                if($this->VehicleTypeMaster->validates()) {
                    $dataSource = $this->VehicleTypeMaster->getDataSource();
                    $fieldList = array('id','name','code','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['VehicleTypeMaster']['id']) || empty($arrSaveRecords['VehicleTypeMaster']['id'])) {
                            $this->VehicleTypeMaster->create();
                        }
                        if(!$this->VehicleTypeMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleTypeMaster']['id']) && !empty($arrSaveRecords['VehicleTypeMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_type_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleTypeMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_type_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','order_no'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->VehicleTypeMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleTypeMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_type_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('vehicle_type_master_id'),'conditions' => array('status'=> 1,'vehicle_type_master_id' => $this->request->data['id']));
                    $arrRecords = $this->ManufacturerTran->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->VehicleTypeMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['VehicleTypeMaster.status'] = 0;
                            $updateFields['VehicleTypeMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleTypeMaster.id'] = $arrDeleteRecords;
                            if(!$this->VehicleTypeMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'country_masters','method' => 'delete','description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'vehicle_type_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
