<?php
App::uses('AppController','Controller');
class CompanyYearMastersController extends AppController {
    public $name = 'CompanyYearMasters';
    public $layout = false;
    public $uses = array('CompanyYearMaster','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('CompanyYearMaster.status' => 1);
                if(isset($this->request->data['caption']) && !empty($this->request->data['caption'])) {
                    $conditions['CompanyYearMaster.caption LIKE'] = '%'.trim($this->request->data['caption']).'%';
                }

                if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
                    $conditions['CompanyYearMaster.start_date <= '] = $this->AppUtilities->date_format(trim($this->request->data['start_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['CompanyYearMaster.end_date >='] = $this->AppUtilities->date_format(trim($this->request->data['end_date']),'yyyy-mm-dd');
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('CompanyYearMaster.caption '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('CompanyYearMaster.start_date '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('CompanyYearMaster.end_date '.$sortyType);
                                break;
                        default:
                                $orderBy = array('CompanyYearMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('CompanyYearMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->CompanyYearMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','caption','start_date','end_date','order_no','is_current','COUNT(VJM.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'product_variant_stock_trans',
                                            'alias' => 'VJM',
                                            'type' => 'LEFT',
                                            'conditions' => array('CompanyYearMaster.id = VJM.company_year_master_id','VJM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' =>'CompanyYearMaster.id',
                                    'order' => $orderBy
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->CompanyYearMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['CompanyYearMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['caption'] = $tableDetails['CompanyYearMaster']['caption'];
                        $records[$key]['start_date'] = $this->AppUtilities->date_format($tableDetails['CompanyYearMaster']['start_date'],'dd-mm-yyyy');
                        $records[$key]['end_date'] = $this->AppUtilities->date_format($tableDetails['CompanyYearMaster']['end_date'],'dd-mm-yyyy');
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $records[$key]['order_no'] = $tableDetails['CompanyYearMaster']['order_no'];
                        $records[$key]['current'] = $tableDetails['CompanyYearMaster']['is_current'];
                        $records[$key]['current_status'] = ($tableDetails['CompanyYearMaster']['is_current'] == 1) ? 'Active' :'In-Active';
                        $maxOrderNo = ($tableDetails['CompanyYearMaster']['order_no'] > $maxOrderNo) ? $tableDetails['CompanyYearMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','caption'=>'Caption','start_date' => 'Start Date','end_date' => 'End Date','current_status' => 'Status','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['CompanyYearMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['CompanyYearMaster']['id']) && !empty($arrSaveRecords['CompanyYearMaster']['id'])) {
                    $arrSaveRecords['CompanyYearMaster']['id'] = $this->decryption($arrSaveRecords['CompanyYearMaster']['id']);
                }
                $this->CompanyYearMaster->set($arrSaveRecords);
                if($this->CompanyYearMaster->validates()) {
                    $dataSource = $this->CompanyYearMaster->getDataSource();
                    if(!isset($arrSaveRecords['CompanyYearMaster']['id'])) {
                        $count = $this->CompanyYearMaster->find('count',array('conditions' => array('status' => 1)));
                        if($count <= 0) {
                            $arrSaveRecords['CompanyYearMaster']['is_current'] = 1;
                        }
                    }
                    $fieldList = array('id','name','caption','start_date','is_current','end_date','description','order_no');
                    $arrSaveRecords['CompanyYearMaster']['start_date'] = $this->AppUtilities->date_format($arrSaveRecords['CompanyYearMaster']['start_date']);
                    $arrSaveRecords['CompanyYearMaster']['end_date'] = $this->AppUtilities->date_format($arrSaveRecords['CompanyYearMaster']['end_date']);
                    $arrSaveRecords['CompanyYearMaster']['caption'] = $this->AppUtilities->date_format($arrSaveRecords['CompanyYearMaster']['start_date'],'yyyy').'-'.$this->AppUtilities->date_format($arrSaveRecords['CompanyYearMaster']['end_date'],'yyyy');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['CompanyYearMaster']['id']) || empty($arrSaveRecords['CompanyYearMaster']['id'])) {
                            $this->CompanyYearMaster->create();
                        }
                        if(!$this->CompanyYearMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['CompanyYearMaster']['id']) && !empty($arrSaveRecords['CompanyYearMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->CompanyYearMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function change_status() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                    $options = array('fields' => array('id'),'conditions' => array('status' => 1 ,'id != ' =>$this->request->data['id']));
                    $count = $this->CompanyYearMaster->find('count',$options);
                    if($count > 0) {
                        $dataSource = $this->CompanyYearMaster->getDataSource();
                        try{
                            $dataSource->begin();
                            $updateFields['CompanyYearMaster.is_current'] = 0;
                            $updateFields['CompanyYearMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['CompanyYearMaster.id != '] = $this->request->data['id'];
                            if(!$this->CompanyYearMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not changed properly, please try again later!',true));
                            }
    
                            unset($updateFields,$updateParams);
    
                            $updateFields['CompanyYearMaster.is_current'] = 1;
                            $updateFields['CompanyYearMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['CompanyYearMaster.id'] = $this->request->data['id'];
                            if(!$this->CompanyYearMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not changed properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('Financial year activated successfully !.',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('Financial year could not deactivated ',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','caption','start_date','end_date','order_no'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->CompanyYearMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $arrRecords['CompanyYearMaster']['start_date'] = $this->AppUtilities->date_format($arrRecords['CompanyYearMaster']['start_date'],'dd-mm-yyyy');
                            $arrRecords['CompanyYearMaster']['end_date'] = $this->AppUtilities->date_format($arrRecords['CompanyYearMaster']['end_date'],'dd-mm-yyyy');
                            $response = array('status' => 1,'message' => __('record_fetched',true),'data' => $arrRecords['CompanyYearMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $getId = implode(',',$this->request->data['id']);
                    $query = "SELECT company_year_master_id FROM product_variant_stock_trans WHERE `status` = 1 AND company_year_master_id IN(".$getId.")
                              UNION
                              SELECT company_year_master_id FROM purchase_masters WHERE `status` = 1 AND company_year_master_id IN(".$getId.")";
                    $result = $this->CompanyYearMaster->query($query,false);
                    if(!empty($result)) {
                        $arrExistsRecords = array();
                        foreach($result as $key => $val) {
                            $arrExistsRecords[] = $val[0]['company_year_master_id'];
                        }
                        $arrRecords = array_diff($this->request->data['id'],$arrExistsRecords);
                    } else {
                        $arrRecords = $this->request->data['id'];
                    }
                    if(count($arrRecords) > 0) {
                        $dataSource = $this->CompanyYearMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['CompanyYearMaster.status'] = 0;
                            $updateFields['CompanyYearMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['CompanyYearMaster.id'] = $arrRecords;
                            if(!$this->CompanyYearMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
