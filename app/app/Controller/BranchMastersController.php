<?php
App::uses('AppController','Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class BranchMastersController extends AppController {
    public $name = 'BranchMasters';
    public $layout = false;
    public $uses = array('BranchMaster','ErrorLog','User','UserRoleTran','ExpenseHeadMaster','AutoMemberCode','RoleMaster');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('BranchMaster.status' => 1);
                if(isset($this->request->data['member_type_tran_id']) && !empty($this->request->data['member_type_tran_id'])) {
                    $conditions['BranchMaster.member_type_tran_id'] = trim($this->request->data['member_type_tran_id']);
                }
                
                if(isset($this->request->data['unique_no']) && !empty($this->request->data['unique_no'])) {
                    $conditions['BranchMaster.unique_no LIKE'] = '%'.trim($this->request->data['unique_no']).'%';
                }

                if(isset($this->request->data['firm_name']) && !empty($this->request->data['firm_name'])) {
                    $conditions['BranchMaster.firm_name LIKE'] = '%'.trim($this->request->data['firm_name']).'%';
                }

                if(isset($this->request->data['mobile_no']) && !empty($this->request->data['mobile_no'])) {
                    $conditions['BranchMaster.mobile_no LIKE'] = '%'.trim($this->request->data['mobile_no']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('BranchMaster.firm_name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('BranchMaster.max_unique_no '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('TypeTran.name '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('BranchMaster.mobile_no '.$sortyType);
                                break;
                        default:
                                $orderBy = array('BranchMaster.firm_name '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('BranchMaster.firm_name ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'type_trans',
                                                    'alias' => 'TypeTran',
                                                    'type' => 'INNER',
                                                    'conditions' => array('BranchMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                                )
                                            ),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->BranchMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','User.username','firm_name','TypeTran.name','first_name','middle_name','last_name','unique_no','mobile_no','COUNT(VM.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'type_trans',
                                            'alias' => 'TypeTran',
                                            'type' => 'INNER',
                                            'conditions' => array('BranchMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                        ),
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'LEFT',
                                            'conditions' => array('BranchMaster.id = VM.branch_master_id','VM.status' => 1)
                                        ),
                                        array(
                                            'table' => 'users',
                                            'alias' => 'User',
                                            'type' => 'INNER',
                                            'conditions' => array('BranchMaster.user_id = User.id','User.status' => 1,'TypeTran.role_type' => 3)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'BranchMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->BranchMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['BranchMaster']['id']);
                        $name = $tableDetails['TypeTran']['name'];
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['firm_name'] = $tableDetails['BranchMaster']['firm_name'];
                        $records[$key]['username'] = $tableDetails['User']['username'];
                        $records[$key]['unique_no'] = $tableDetails['BranchMaster']['unique_no'];
                        $records[$key]['name'] = $name;
                        $records[$key]['mobile_no'] = $tableDetails['BranchMaster']['mobile_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','firm_name'=>'Firm Name','unique_no'=>'Member Code','username'=>'UserName','mobile_no' => 'Mobile No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        $isShowMessage = 0;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['BranchMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['BranchMaster']['id']) && !empty($arrSaveRecords['BranchMaster']['id'])) {
                    $arrSaveRecords['BranchMaster']['id'] = $this->decryption($arrSaveRecords['BranchMaster']['id']);
                }
                $arrSaveDefaultExpensesHeadData = array();
                $this->BranchMaster->set($arrSaveRecords);
                if($this->BranchMaster->validates()) {
                    #pr($arrSaveRecords);exit;
                    $dataSource = $this->BranchMaster->getDataSource();
                    $fieldList = array('id','firm_name','registration_date','first_name','middle_name','last_name','unique_no','max_unique_no','user_id','title_master_id','gender','gst_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','member_type_tran_id','c_address','p_address','logo','royalty_charge_master_id','photo');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['BranchMaster']['id']) || empty($arrSaveRecords['BranchMaster']['id'])) {
                            $arrSaveMemberCode['AutoMemberCode']['name'] = $arrSaveRecords['BranchMaster']['first_name'];
                            $this->AutoMemberCode->create();
                            if(!$this->AutoMemberCode->save($arrSaveMemberCode)) {
                                throw new Exception(__('Auto member data could not saved',true));
                            }
                            $maxNumber = $this->AutoMemberCode->getInsertID();
                            $arrMemberCode = $this->AppUtilities->getMemberUniqueNo($maxNumber);
                            $userName = 'RADIZO0000'.$maxNumber;
                            if(!empty($arrMemberCode)) {
                                $arrSaveRecords['BranchMaster']['unique_no'] = $arrMemberCode['code'];
                                $arrSaveRecords['BranchMaster']['max_unique_no'] = $arrMemberCode['maxNumber'];
                                $replaceData = array('/' => '','-' => '','%' => '');
                                $userName = strtr($arrMemberCode['code'],$replaceData);
                            }
                            $options = array('fields' => array('id'),'conditions' => array('unique_no' => $arrSaveRecords['BranchMaster']['unique_no']));
                            $validMember = $this->BranchMaster->find('count',$options);
                            if($validMember > 0) {
                                $isShowMessage = 1;
                                throw new Exception(__('Member Code already exists !...',true));
                            }
                            unset($options);
                            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                            $arrUserData['User']['username'] = $userName;
                            $arrUserData['User']['password'] = $passwordHasher->hash($arrSaveRecords['BranchMaster']['mobile_no']);
                            $arrUserData['User']['encrypt'] = $this->encrypt_algo($arrSaveRecords['BranchMaster']['mobile_no']);
                            $arrUserData['User']['fullname'] = $arrSaveRecords['BranchMaster']['first_name'].' '.$arrSaveRecords['BranchMaster']['middle_name'].' '.$arrSaveRecords['BranchMaster']['last_name'];
                            $this->User->create();
                            if(!$this->User->save($arrUserData,array('validate'=>false))) {
                                throw new Exception(__('UserName already exists!',true));
                            }
                            $userId = $this->User->getInsertID();
                            $arrSaveRecords['BranchMaster']['user_id'] = $userId;
                            $options = array('fields' => array('id'),'conditions' => array('type_tran_id' => $arrSaveRecords['BranchMaster']['member_type_tran_id'],'status' => 1));
                            $arrRoleData = $this->RoleMaster->find('first',$options);
                            if(!empty($arrRoleData)) {
                                $arrUserRoleTran['UserRoleTran']['role_master_id'] = $arrRoleData['RoleMaster']['id'];
                                $arrUserRoleTran['UserRoleTran']['user_id'] = $userId;
                                $this->UserRoleTran->create();
                                if(!$this->UserRoleTran->save($arrUserRoleTran)) {
                                    throw new Exception(__('User role tran could not saved properly,Please try again later !.',true));
                                }
                            }

                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['name'] = 'Vehicle Service Expenses';
                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['code'] = 'Vehicle Service Expenses';
                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['description'] = 'Vehicle Service Expenses';
                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['type'] = 2;
                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['order_no'] = 1;

                            $this->BranchMaster->create();
                        } else {
                            if(empty($arrSaveRecords['BranchMaster']['photo'])) {
                                $arrSaveRecords['BranchMaster']['photo'] = $arrSaveRecords['BranchMaster']['old_photo'];
                                unset($arrSaveRecords['BranchMaster']['old_photo']);
                            }
                            if(empty($arrSaveRecords['BranchMaster']['logo'])) {
                                $arrSaveRecords['BranchMaster']['logo'] = $arrSaveRecords['BranchMaster']['old_logo'];
                                unset($arrSaveRecords['BranchMaster']['old_logo']);
                            }
                        }
                        
                        $arrSaveRecords['BranchMaster']['registration_date'] = date('Y-m-d');
                        if(!$this->BranchMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $memberId = '';
                        if(!isset($arrSaveRecords['BranchMaster']['id'])) {
                            $memberId = $this->BranchMaster->getInsertID();
                        }

                        if(count($arrSaveDefaultExpensesHeadData) > 0 && !empty($memberId)) {
                            $arrSaveDefaultExpensesHeadData['ExpenseHeadMaster']['branch_master_id'] = $memberId;
                            $this->ExpenseHeadMaster->create();
                            if(!$this->ExpenseHeadMaster->save($arrSaveDefaultExpensesHeadData,array('validate' => false))) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                        }

                        $dataSource->commit();
                        if(isset($arrSaveRecords['BranchMaster']['photo']) && !empty($arrSaveRecords['BranchMaster']['photo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$arrSaveRecords['BranchMaster']['photo'];
                            $saveImagePath = Configure::read('UPLOAD_MEMBER_IMAGE_DIR').'/'.$arrSaveRecords['BranchMaster']['photo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                            if(isset($arrSaveRecords['BranchMaster']['old_photo']) && !empty($arrSaveRecords['BranchMaster']['old_photo'])) {
                                $files =  Configure::read('UPLOAD_MEMBER_IMAGE_DIR').'/'.$arrSaveRecords['BranchMaster']['old_photo'];
                                if(file_exists($files)) {
                                    @unlink($files);
                                }
                            }
                        }
                        if(isset($arrSaveRecords['BranchMaster']['logo']) && !empty($arrSaveRecords['BranchMaster']['logo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$arrSaveRecords['BranchMaster']['logo'];
                            $saveImagePath = Configure::read('UPLOAD_SETTING_LOGO_DIR').'/'.$arrSaveRecords['BranchMaster']['logo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                            if(isset($arrSaveRecords['BranchMaster']['old_logo']) && !empty($arrSaveRecords['BranchMaster']['old_logo'])) {
                                $files =  Configure::read('UPLOAD_SETTING_LOGO_DIR').'/'.$arrSaveRecords['BranchMaster']['old_logo'];
                                if(file_exists($files)) {
                                    @unlink($files);
                                }
                            }
                        }
    
                        $statusCode = 200;
                        if(isset($arrSaveRecords['BranchMaster']['id']) && !empty($arrSaveRecords['BranchMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true),'is_send_mail' => 0);
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true),'is_send_mail' => 1,'id' =>$memberId);
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        if($isShowMessage == 1) {
                            $response = array('status' => 0,'message' => __($e->getMessage(),true));
                        } else {
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    }
                } else {
                    $validationErrros = Set::flatten($this->BranchMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('id','TypeTran.name','CountryMaster.name','StateMaster.name','CityMaster.name','registration_date','firm_name','first_name','middle_name','last_name','unique_no','max_unique_no','user_id','title_master_id','gender','gst_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','member_type_tran_id','c_address','p_address','logo','photo','royalty_charge_master_id','RCM.name'),
                            'joins' => array(
                                array(
                                    'table' => 'type_trans',
                                    'alias' => 'TypeTran',
                                    'type' => 'INNER',
                                    'conditions' => array('BranchMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'country_masters',
                                    'alias' => 'CountryMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('BranchMaster.country_master_id = CountryMaster.id','CountryMaster.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'state_masters',
                                    'alias' => 'StateMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('BranchMaster.state_master_id = StateMaster.id','StateMaster.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'city_masters',
                                    'alias' => 'CityMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('BranchMaster.city_master_id = CityMaster.id','CityMaster.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'royalty_charge_masters',
                                    'alias' => 'RCM',
                                    'type' => 'LEFT',
                                    'conditions' => array('BranchMaster.royalty_charge_master_id = RCM.id','RCM.status' => 1)
                                ),
                            ),
                            'conditions' => array('BranchMaster.status' => 1,'BranchMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->BranchMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            $arrRecords['BranchMaster']['member_type'] = $arrRecords['TypeTran']['name'];
                            $arrRecords['BranchMaster']['country'] = $arrRecords['CountryMaster']['name'];
                            $arrRecords['BranchMaster']['state'] = $arrRecords['StateMaster']['name'];
                            $arrRecords['BranchMaster']['city'] = $arrRecords['CityMaster']['name'];
                            $arrRecords['BranchMaster']['royalty'] = $arrRecords['RCM']['name'];
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['BranchMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->BranchMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $options = array('fields' => array('id','user_id'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                        $arrUserData = $this->BranchMaster->find('list',$options);
                        if(!empty($arrUserData)) {
                            $arrUserId = array_values($arrUserData);
                            $updateFields = $updateParams = array();
                            $updateFields['User.status'] = 0;
                            $updateFields['User.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['User.id'] = $arrUserId;
                            if(!$this->User->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $updateFields = $updateParams = array();
                            $updateFields['UserRoleTran.status'] = 0;
                            $updateFields['UserRoleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['UserRoleTran.user_id'] = $arrUserId;
                            if(!$this->UserRoleTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }
                        $updateFields = $updateParams = array();
                        $updateFields['BranchMaster.status'] = 0;
                        $updateFields['BranchMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['BranchMaster.id'] = $this->request->data['id'];
                        if(!$this->BranchMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
