<?php
App::uses('AppController','Controller');
class RoleMastersController extends AppController {
    public $name = 'RoleMasters';
    public $layout = false;
    public $uses = array('RoleMaster','ErrorLog','UserRoleTran');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('RoleMaster.status' => 1,'RoleMaster.is_show' => 1);
                $arrSearchFields = array(1 =>'RoleMaster.name',2 => 'RoleMaster.code',3 => 'TypeTran.name');
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['RoleMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['RoleMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                if(isset($this->request->data['type_tran_id']) && !empty($this->request->data['type_tran_id'])) {
                    $conditions['RoleMaster.type_tran_id'] = trim($this->request->data['type_tran_id']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('RoleMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('RoleMaster.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('RoleMaster.member_name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('RoleMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('RoleMaster.order_no ASC');
                }

                $joins = array(
                            array(
                                'table' => 'type_trans',
                                'alias' => 'TypeTran',
                                'type' => 'INNER',
                                'conditions' => array('RoleMaster.type_tran_id = TypeTran.id','TypeTran.status' => 1)
                            )
                        );
                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => $joins,
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->RoleMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $joins[] = array(
                            'table' => 'user_role_trans',
                            'alias' => 'UserRoleTran',
                            'type' => 'LEFT',
                            'conditions' => array('RoleMaster.id = UserRoleTran.role_master_id','UserRoleTran.status' => 1)
                        );
                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','is_default','member_name','COUNT(UserRoleTran.id) AS count'),
                                    'joins' => $joins,
                                    'conditions' => $conditions,
                                    'group' => 'RoleMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->RoleMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['RoleMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['RoleMaster']['name'];
                        $records[$key]['code'] = $tableDetails['RoleMaster']['code'];
                        $records[$key]['member'] = $tableDetails['RoleMaster']['member_name'];
                        $records[$key]['order_no'] = $tableDetails['RoleMaster']['order_no'];
                        $records[$key]['is_exists'] = ($tableDetails[0]['count'] == 0 && $tableDetails['RoleMaster']['is_default'] == 0) ? 0 : 1;
                        $maxOrderNo = ($tableDetails['RoleMaster']['order_no'] > $maxOrderNo) ? $tableDetails['RoleMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','member' => 'Member','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['RoleMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['RoleMaster']['id']) && !empty($arrSaveRecords['RoleMaster']['id'])) {
                    $arrSaveRecords['RoleMaster']['id'] = $this->decryption($arrSaveRecords['RoleMaster']['id']);
                }
                $this->RoleMaster->set($arrSaveRecords);
                if($this->RoleMaster->validates()) {
                    $dataSource = $this->RoleMaster->getDataSource();
                    $fieldList = array('id','name','code','order_no','description','type_tran_id');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['RoleMaster']['id']) || empty($arrSaveRecords['RoleMaster']['id'])) {
                            $this->RoleMaster->create();
                        }
                        if(!$this->RoleMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['RoleMaster']['id']) && !empty($arrSaveRecords['RoleMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->RoleMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','order_no','description','type_tran_id'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->RoleMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['RoleMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $arrRoleId = array();
                    $options = array('fields' => array('id','role_master_id'),'conditions' => array('UserRoleTran.status' => 1,'UserRoleTran.role_master_id' => $this->request->data['id']));
                    $arrUserRoleData = $this->UserRoleTran->find('list',$options);
                    if(count($arrUserRoleData) > 0) {
                        $arrRoleId = array_diff($this->request->data['id'],array_values($arrUserRoleData));
                    } else {
                        $arrRoleId = $this->request->data['id'];
                    }
                    if(count($arrRoleId) > 0) {
                        $dataSource = $this->RoleMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['RoleMaster.status'] = 0;
                            $updateFields['RoleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['RoleMaster.id'] = $arrRoleId;
                            if(!$this->RoleMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 1,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
