<?php
App::uses('AppController','Controller');
class SettingController extends AppController {
    public $name = 'Setting';
    public $layout = false;
    public $uses = array('GeneralSetting','VehicleMaster','SaleMaster','ErrorLog','VehicleJobMaster','BranchMaster','UserSetting','EmployeeMaster');
    public $generalSettings = array('firm_name','pan_number','gst_number','contact_person','mobile_no','alternate_mobile_no','email_id','alternate_email_id','residential_address','permanent_address','country_master_id','state_master_id','city_master_id','logo','website_url','franchise_unique_string','franchise_total_digit','franchise_digit_start_from','vehicle_unique_string','vehicle_total_digit','vehicle_digit_start_from');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function general_settings() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array('fields' => array('id','field_name','field_value'),'conditions' => array('status' => 1,'field_name != ' => ''));
                $arrGeneralSettingData = $this->GeneralSetting->find('all',$options);
                if(count($arrGeneralSettingData) > 0) {
                    $statusCode = 200;
                    $arrGeneralSettingData = Hash::combine($arrGeneralSettingData,'{n}.GeneralSetting.id','{n}.GeneralSetting');
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => array_values($arrGeneralSettingData));
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_general_settings() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrGeneralSettings['GeneralSetting'] = $this->request->data;
                unset($this->request->data);
                #pr($arrGeneralSettings);exit;
                $this->GeneralSetting->set($arrGeneralSettings);
                if($this->GeneralSetting->validates()) {
                    $options = array('fields' => array('id','field_name','field_value'),'conditions' => array('status' => 1));
                    $arrGeneralSettingData = $this->GeneralSetting->find('all',$options);
                    $arrGeneralSettingData = Hash::combine($arrGeneralSettingData,'{n}.GeneralSetting.field_name','{n}.GeneralSetting');
                    $options = array('fields' => array('id'),'conditions' => array('status' => 1));
                    $memberCount = $this->BranchMaster->find('count',$options);

                    $options = array('fields' => array('id'),'conditions' => array('status' => 1));
                    $vehicleCount = $this->VehicleMaster->find('count',$options);

                    $counter = 0;
                    $oldFile = '';
                    if(!empty($arrGeneralSettings['GeneralSetting']['logo'])) {
                        $fileExt = strtolower(pathinfo($arrGeneralSettings['GeneralSetting']['logo'],PATHINFO_EXTENSION));
                        $oldFile = $arrGeneralSettings['GeneralSetting']['logo'];
                        $arrGeneralSettings['GeneralSetting']['logo'] = $this->Session->read('sessUserId').'_logo.'.$fileExt;
                    }

                    if($memberCount > 0) {
                        if(isset($arrGeneralSettingData['franchise_unique_string']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['franchise_unique_string']);
                        }
                        if(isset($arrGeneralSettingData['franchise_total_digit']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['franchise_total_digit']);
                        }
                        if(isset($arrGeneralSettingData['franchise_digit_start_from']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['franchise_digit_start_from']);
                        }
                    } else {
                        if(stripos($arrGeneralSettings['GeneralSetting']['franchise_unique_string'],'%AUTO_INCREMENT_DIGIT%') === false) {
                            $arrGeneralSettings['GeneralSetting']['franchise_unique_string'] = $arrGeneralSettings['GeneralSetting']['franchise_unique_string'].'%AUTO_INCREMENT_DIGIT%';
                        }
                    }
                    #pr($arrGeneralSettings);exit;

                    if($vehicleCount > 0) {
                        if(isset($arrGeneralSettingData['vehicle_digit_start_from']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['vehicle_digit_start_from']);
                        }
                        if(isset($arrGeneralSettingData['vehicle_unique_string']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['vehicle_unique_string']);
                        }
                        if(isset($arrGeneralSettingData['vehicle_total_digit']['field_value'])) {
                            unset($arrGeneralSettings['GeneralSetting']['vehicle_total_digit']);
                        }
                    } else {
                        if(stripos($arrGeneralSettings['GeneralSetting']['vehicle_unique_string'],'%AUTO_INCREMENT_DIGIT%') === false) {
                            $arrGeneralSettings['GeneralSetting']['vehicle_unique_string'] = $arrGeneralSettings['GeneralSetting']['vehicle_unique_string'].'%AUTO_INCREMENT_DIGIT%';
                        }
                    }

                    foreach($arrGeneralSettings['GeneralSetting'] as $fieldName => $fieldValue) {
                        if(isset($arrGeneralSettingData[$fieldName])) {
                            $arrSaveGeneralSettings[$counter]['GeneralSetting']['id'] = $arrGeneralSettingData[$fieldName]['id'];
                        }
                        $arrSaveGeneralSettings[$counter]['GeneralSetting']['field_name'] = $fieldName; 
                        $arrSaveGeneralSettings[$counter]['GeneralSetting']['field_value'] = $fieldValue; 
                        ++$counter;
                    }

                    $dataSource = $this->GeneralSetting->getDataSource();
                    $fieldList = array('id','field_name','fieldvalue');
                    try{
                        $dataSource->begin();
                        if($memberCount <= 0) {
                            $arrGeneralSettings['GeneralSetting']['franchise_digit_start_from'] = ($arrGeneralSettings['GeneralSetting']['franchise_digit_start_from'] > 0) ? intval($arrGeneralSettings['GeneralSetting']['franchise_digit_start_from']) : 1;
                            $tableName = 'auto_member_codes';
                            $start = $arrGeneralSettings['GeneralSetting']['franchise_digit_start_from'];
                            $query ="DROP TABLE IF EXISTS $tableName; 
                                    CREATE TABLE IF NOT EXISTS `$tableName`(
                                        `id` INT(20) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(40),
                                        `created` DATETIME,
                                        `modified` DATETIME,
                                        PRIMARY KEY (`id`)
                                    ) ENGINE=INNODB AUTO_INCREMENT=$start DEFAULT CHARSET=latin1;";
                            if(!$this->GeneralSetting->query($query,false)) {
                                throw new Exception(__('could not reset auto increment value properly, please try again later!',true));	
                            }
                        }
                        if($vehicleCount <= 0) {
                            $tableName = 'auto_vehicle_codes';
                            $arrGeneralSettings['GeneralSetting']['vehicle_digit_start_from'] = ($arrGeneralSettings['GeneralSetting']['vehicle_digit_start_from'] > 0) ? $arrGeneralSettings['GeneralSetting']['vehicle_digit_start_from'] : 1;
                            $start = $arrGeneralSettings['GeneralSetting']['vehicle_digit_start_from'];
                            $query ="DROP TABLE IF EXISTS $tableName; 
                                    CREATE TABLE IF NOT EXISTS `$tableName`(
                                        `id` INT(20) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(40),
                                        `created` DATETIME,
                                        `modified` DATETIME,
                                        PRIMARY KEY (`id`)
                                    ) ENGINE=INNODB AUTO_INCREMENT=$start DEFAULT CHARSET=latin1;";
                            if(!$this->GeneralSetting->query($query,false)) {
                                throw new Exception(__('could not reset auto increment value properly, please try again later!',true));	
                            }
                        }
                        $this->GeneralSetting->saveAll($arrSaveGeneralSettings,array('validate' => false));
                        $dataSource->commit();
                        if(!empty($arrGeneralSettings['GeneralSetting']['logo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$oldFile;
                            $saveImagePath = Configure::read('UPLOAD_SETTING_LOGO_DIR').'/'.$arrGeneralSettings['GeneralSetting']['logo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                        }
                        unset($arrGeneralSettings,$arrSaveGeneralSettings);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('General Setting saved successfully !.',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrGeneralSettings,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->GeneralSetting->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function user_settings($firmId = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($firmId)) {
                    $options = array('fields' => array('id','employee_code','employee_code_digit','employee_code_start_from','invoice_code','invoice_code_digit','invoice_code_start_from','jobcard_code','jobcard_code_digit','jobcard_code_start_from'),'conditions' => array('status' => 1,'branch_master_id ' => $firmId));
                    $arrUserSettings = $this->UserSetting->find('first',$options);
                    if(count($arrUserSettings) > 0) {
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrUserSettings['UserSetting']);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('invalid member ID',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_user_settings() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrUserSettings['UserSetting'] = $this->request->data;
                #pr($arrUserSettings);exit;
                unset($this->request->data);
                $arrUserSettings['UserSetting']['branch_master_id'] = isset($arrUserSettings['UserSetting']['branch_master_id']) ? $arrUserSettings['UserSetting']['branch_master_id'] : '';
                $this->UserSetting->set($arrUserSettings);
                if($this->UserSetting->validates()) {
                    $options = array('fields' => array('id'),'conditions' => array('status' => 1,'branch_master_id' => $arrUserSettings['UserSetting']['branch_master_id']));
                    $employeeCount = $this->EmployeeMaster->find('count',$options);

                    $options = array('fields' => array('id'),'conditions' => array('status' => 1,'branch_master_id' => $arrUserSettings['UserSetting']['branch_master_id']));
                    $jobcardCount = $this->VehicleJobMaster->find('count',$options);
                    if($jobcardCount <=0) {
                        $options = array('fields' => array('id'),'conditions' => array('status' => 1,'branch_master_id' => $arrUserSettings['UserSetting']['branch_master_id']));
                        $jobcardCount = $this->SaleMaster->find('count',$options);
                    }
                    $dataSource = $this->UserSetting->getDataSource();
                    $fieldList = array('id','branch_master_id','employee_code','employee_code_digit','employee_code_start_from','invoice_code','invoice_code_digit','invoice_code_start_from','invoice_code_session','jobcard_code','jobcard_code_digit','jobcard_code_start_from','jobcard_code_session');
                    try{
                        $dataSource->begin();
                        $firmId = $arrUserSettings['UserSetting']['branch_master_id'];
                        if($employeeCount <= 0) {
                            if(stripos($arrUserSettings['UserSetting']['employee_code'],'%AUTO_INCREMENT_DIGIT%') === false) {
                                $arrUserSettings['UserSetting']['employee_code'] = $arrUserSettings['UserSetting']['employee_code'].'%AUTO_INCREMENT_DIGIT%';
                            }
                            $tableName = 'auto_employee'.$firmId.'_codes';
                            $start = ($arrUserSettings['UserSetting']['employee_code_start_from'] > 0) ?  intval($arrUserSettings['UserSetting']['employee_code_start_from']) : 1;
                            $query ="DROP TABLE IF EXISTS $tableName; 
                                    CREATE TABLE IF NOT EXISTS `$tableName`(
                                        `id` INT(20) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(40),
                                        `created` DATETIME,
                                        `modified` DATETIME,
                                        PRIMARY KEY (`id`)
                                    ) ENGINE=INNODB AUTO_INCREMENT=$start DEFAULT CHARSET=latin1;";
                            if(!$this->GeneralSetting->query($query,false)) {
                                throw new Exception(__('could not reset auto increment value properly, please try again later!',true));	
                            }
                        } else {
                            unset($arrUserSettings['UserSetting']['employee_code']);
                            unset($arrUserSettings['UserSetting']['employee_code_digit']);
                            unset($arrUserSettings['UserSetting']['employee_code_start_from']);
                        }

                        if($jobcardCount <= 0) {
                            if(stripos($arrUserSettings['UserSetting']['invoice_code'],'%AUTO_INCREMENT_DIGIT%') === false) {
                                $arrUserSettings['UserSetting']['invoice_code'] = $arrUserSettings['UserSetting']['invoice_code'].'%AUTO_INCREMENT_DIGIT%';
                            }
                            $tableName = 'auto_invoice'.$firmId.'_codes';
                            $start = ($arrUserSettings['UserSetting']['invoice_code_start_from'] > 0) ?  intval($arrUserSettings['UserSetting']['invoice_code_start_from']) : 1;
                            $query ="DROP TABLE IF EXISTS $tableName; 
                                    CREATE TABLE IF NOT EXISTS `$tableName`(
                                        `id` INT(20) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(40),
                                        `created` DATETIME,
                                        `modified` DATETIME,
                                        PRIMARY KEY (`id`)
                                    ) ENGINE=INNODB AUTO_INCREMENT=$start DEFAULT CHARSET=latin1;";
                            if(!$this->GeneralSetting->query($query,false)) {
                                throw new Exception(__('could not reset auto increment value properly, please try again later!',true));	
                            }

                            if(stripos($arrUserSettings['UserSetting']['jobcard_code'],'%AUTO_INCREMENT_DIGIT%') === false) {
                                $arrUserSettings['UserSetting']['jobcard_code'] = $arrUserSettings['UserSetting']['jobcard_code'].'%AUTO_INCREMENT_DIGIT%';
                            }
                            $tableName = 'auto_job'.$firmId.'_codes';
                            $start = ($arrUserSettings['UserSetting']['jobcard_code_start_from'] > 0) ?  intval($arrUserSettings['UserSetting']['jobcard_code_start_from']) : 1;
                            $query ="DROP TABLE IF EXISTS $tableName; 
                                    CREATE TABLE IF NOT EXISTS `$tableName`(
                                        `id` INT(20) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(40),
                                        `created` DATETIME,
                                        `modified` DATETIME,
                                        PRIMARY KEY (`id`)
                                    ) ENGINE=INNODB AUTO_INCREMENT=$start DEFAULT CHARSET=latin1;";
                            if(!$this->GeneralSetting->query($query,false)) {
                                throw new Exception(__('could not reset auto increment value properly, please try again later!',true));	
                            }
                        } else {
                            unset($arrUserSettings['UserSetting']['jobcard_code']);
                            unset($arrUserSettings['UserSetting']['jobcard_code_digit']);
                            unset($arrUserSettings['UserSetting']['jobcard_code_start_from']);
                            unset($arrUserSettings['UserSetting']['invoice_code']);
                            unset($arrUserSettings['UserSetting']['invoice_code_start_from']);
                            unset($arrUserSettings['UserSetting']['invoice_code_digit']);
                        }
                        
                        $this->UserSetting->save($arrUserSettings,array('validate' => false));
                        $dataSource->commit();
                        unset($arrUserSettings);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('User Setting saved successfully !.',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrUserSettings,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->UserSetting->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
