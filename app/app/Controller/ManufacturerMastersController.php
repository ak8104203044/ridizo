<?php
App::uses('AppController','Controller');
class ManufacturerMastersController extends AppController {
    public $name = 'ManufacturerMasters';
    public $layout = false;
    public $uses = array('ManufacturerMaster','VehicleModel','ErrorLog','ManufacturerTran');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('ManufacturerMaster.status' => 1);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['ManufacturerMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['ManufacturerMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('ManufacturerMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('ManufacturerMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ManufacturerMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('ManufacturerMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->ManufacturerMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','COUNT(VM.id) AS count'),
                                    'joins' =>array(
                                        array(
                                            'table' => 'vehicle_models',
                                            'alias' => 'VM',
                                            'type' => 'LEFT',
                                            'conditions' => array('ManufacturerMaster.id = VM.manufacturer_id','VM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'ManufacturerMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->ManufacturerMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ManufacturerMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['ManufacturerMaster']['name'];
                        $records[$key]['code'] = $tableDetails['ManufacturerMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['ManufacturerMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['ManufacturerMaster']['order_no'] > $maxOrderNo) ? $tableDetails['ManufacturerMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['ManufacturerMaster'] = $this->request->data;
                #pr($arrSaveRecords['ManufacturerMaster']);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['ManufacturerMaster']['id']) && !empty($arrSaveRecords['ManufacturerMaster']['id'])) {
                    $arrSaveRecords['ManufacturerMaster']['id'] = $this->decryption($arrSaveRecords['ManufacturerMaster']['id']);
                }
                $this->ManufacturerMaster->set($arrSaveRecords);
                if($this->ManufacturerMaster->validates()) {
                    $dataSource = $this->ManufacturerMaster->getDataSource();
                    $fieldList = array('id','name','code','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['ManufacturerMaster']['id']) || empty($arrSaveRecords['ManufacturerMaster']['id'])) {
                            $this->ManufacturerMaster->create();
                        }
    
                        if(!$this->ManufacturerMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        if(isset($arrSaveRecords['ManufacturerMaster']['id']) && !empty($arrSaveRecords['ManufacturerMaster']['id'])) {
                            $updateFields['ManufacturerTran.status'] = 0;
                            $updateFields['ManufacturerTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ManufacturerTran.manufacturer_master_id'] = $arrSaveRecords['ManufacturerMaster']['id'];
                            if(!$this->ManufacturerTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Manufacturer trans could not deleted properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $manufacturerId = $arrSaveRecords['ManufacturerMaster']['id'];
                        } else {
                            $manufacturerId = $this->ManufacturerMaster->getInsertID();
                        }
                        
                        $arrVehicleTypeTranData = array();
                        if(isset($arrSaveRecords['ManufacturerMaster']['vehicle_type_master_id'][0]) && !empty($arrSaveRecords['ManufacturerMaster']['vehicle_type_master_id'][0])) {
                            $arrSaveRecords['ManufacturerMaster']['vehicle_type_master_id'] = explode(',',$arrSaveRecords['ManufacturerMaster']['vehicle_type_master_id'][0]);
                            $count = 0;
                            foreach($arrSaveRecords['ManufacturerMaster']['vehicle_type_master_id'] as $key => $taxId) {
                                $arrVehicleTypeTranData[$count]['ManufacturerTran'] = array('manufacturer_master_id' => $manufacturerId,'vehicle_type_master_id' => $taxId);
                                ++$count;
                            }
                        }
    
                        if(!empty($arrVehicleTypeTranData)) {
                            if(!$this->ManufacturerTran->saveAll($arrVehicleTypeTranData,array('fieldList' => array('manufacturer_master_id','vehicle_type_master_id')))) {
                                throw new Exception(__('ManufacturerTran could not saved properly, please try again later!',true));
                            }
                        }
    
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['ManufacturerMaster']['id']) && !empty($arrSaveRecords['ManufacturerMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->ManufacturerMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','order_no','vehicle_type_id','vehicle_type_name'),
                            'joins' => array(
                                array(
                                    'table' => 'manufacturer_trans',
                                    'alias' => 'ManufacturerTran',
                                    'type' => 'INNER',
                                    'conditions' => array('ManufacturerMaster.id = ManufacturerTran.manufacturer_master_id','ManufacturerTran.status' => 1)
                                ),
                                array(
                                    'table' => 'vehicle_type_masters',
                                    'alias' => 'VehicleTypeMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('ManufacturerTran.vehicle_type_master_id = VehicleTypeMaster.id','VehicleTypeMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('ManufacturerMaster.status' => 1,'ManufacturerMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->ManufacturerMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['ManufacturerMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('manufacturer_id'),'conditions' => array('status'=> 1,'manufacturer_id' => $this->request->data['id']));
                    $arrRecords = $this->VehicleModel->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->ManufacturerMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['ManufacturerMaster.status'] = 0;
                            $updateFields['ManufacturerMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ManufacturerMaster.id'] = $arrDeleteRecords;
                            if(!$this->ManufacturerMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateParams,$updateFields);
                            $updateFields['ManufacturerTran.status'] = 0;
                            $updateFields['ManufacturerTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ManufacturerTran.manufacturer_master_id'] = $arrDeleteRecords;
                            if(!$this->ManufacturerTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Manufacturer trans could not deleted properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'manufacturer_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
