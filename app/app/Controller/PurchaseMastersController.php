<?php
App::uses('AppController','Controller');
class PurchaseMastersController extends AppController {
    public $name = 'PurchaseMasters';
    public $layout = false;
    public $uses = array('PurchaseMaster','PurchaseTran','PurchaseTaxTran','ProductVariantStockTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $conditions = array('PurchaseMaster.status' => 1,'PurchaseMaster.branch_master_id' => $firmId,'PurchaseMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['voucher_no']) && !empty($this->request->data['voucher_no'])) {
                    $conditions['PurchaseMaster.voucher_no LIKE'] = '%'.trim($this->request->data['voucher_no']).'%';
                }

                if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
                    $conditions['PurchaseMaster.voucher_date >='] = $this->AppUtilities->date_format(trim($this->request->data['start_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['PurchaseMaster.voucher_date <='] = $this->AppUtilities->date_format(trim($this->request->data['end_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['supplier_master_id']) && !empty($this->request->data['supplier_master_id'])) {
                    $conditions['PurchaseMaster.supplier_master_id'] = trim($this->request->data['supplier_master_id']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('PurchaseMaster.voucher_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('PurchaseMaster.voucher_date '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('PurchaseMaster.quantity '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('PurchaseMaster.supplier_master_id '.$sortyType);
                                break;
                        default:
                                $orderBy = array('PurchaseMaster.voucher_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('PurchaseMaster.voucher_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->PurchaseMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','voucher_no','voucher_date','quantity',"CONCAT_WS(' ',TM.name,SM.first_name,SM.middle_name,SM.last_name) AS supplier",'generate_invoice'),
                                    'joins' => array(
                                        array(
                                            'table' => 'supplier_masters',
                                            'alias' => 'SM',
                                            'type' => 'LEFT',
                                            'conditions' => array('PurchaseMaster.supplier_master_id = SM.id','SM.status' => 1)
                                        ),
                                        array(
                                            'table' => 'title_masters',
                                            'alias' => 'TM',
                                            'type' => 'LEFT',
                                            'conditions' => array('SM.title_master_id = TM.id','TM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('PurchaseMaster.id'),
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->PurchaseMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['PurchaseMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['voucher_no'] = $tableDetails['PurchaseMaster']['voucher_no'];
                        $records[$key]['voucher_date'] = $this->AppUtilities->date_format($tableDetails['PurchaseMaster']['voucher_date'],'dd-mm-yyyy');
                        $records[$key]['quantity'] = $tableDetails['PurchaseMaster']['quantity'];
                        $records[$key]['supplier'] = $tableDetails[0]['supplier'];
                        $records[$key]['is_exists'] = $tableDetails['PurchaseMaster']['generate_invoice'];
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','voucher_no'=>'Voucher No','voucher_date'=>'Date','quantity'=>'Quantity','supplier' => 'Supplier');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['PurchaseMaster'] = $this->request->data;
                if(isset($arrSaveRecords['PurchaseMaster']['id']) && !empty($arrSaveRecords['PurchaseMaster']['id'])) {
                    $arrSaveRecords['PurchaseMaster']['id'] = $this->decryption($arrSaveRecords['PurchaseMaster']['id']);
                }
                $arrSaveRecords['PurchaseMaster']['branch_master_id'] = (isset($arrSaveRecords['PurchaseMaster']['branch_master_id']) && $arrSaveRecords['PurchaseMaster']['branch_master_id'] > 0) ? $arrSaveRecords['PurchaseMaster']['branch_master_id'] : '';
                $arrProductTranData = $arrProdctDetail = array();
                if(isset($arrSaveRecords['PurchaseMaster']['product_detail']) && !empty($arrSaveRecords['PurchaseMaster']['product_detail'])) {
                    $arrProdctDetail = json_decode($arrSaveRecords['PurchaseMaster']['product_detail'],true);
                    $arrSaveRecords['PurchaseMaster']['quantity'] = array_column($arrProdctDetail,'quantity');
                    unset($arrSaveRecords['PurchaseMaster']['product_detail']);
                }
                #pr($arrProdctDetail);
                #pr($arrSaveRecords);exit;
                $this->PurchaseMaster->set($arrSaveRecords);
                if($this->PurchaseMaster->validates()) {
                    unset($arrSaveRecords['PurchaseMaster']['quantity']);
                    $arrSaveRecords['PurchaseMaster']['quantity'] = $arrSaveRecords['PurchaseMaster']['total_qty'];
                    unset($arrSaveRecords['PurchaseMaster']['total_qty'],$arrSaveRecords['PurchaseMaster']['random']);
                    $dataSource = $this->PurchaseMaster->getDataSource();
                    $fieldList = array('id','voucher_no','voucher_date','quantity','company_year_master_id','supplier_master_id','branch_master_id','description');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['PurchaseMaster']['id']) || empty($arrSaveRecords['PurchaseMaster']['id'])) {
                            $this->PurchaseMaster->create();
                        } else {
                            $options = array(
                                            'fields' => array('product_variant_tran_id','id'),
                                            'conditions' => array('reference_master_id' => $arrSaveRecords['PurchaseMaster']['id'],'type' => 2,'tran_type' => 1,'branch_master_id' => $arrSaveRecords['PurchaseMaster']['branch_master_id'],'status' => 1)
                                        );
                            $arrProductStockData = $this->ProductVariantStockTran->find('list',$options);
                        }
                        $arrSaveRecords['PurchaseMaster']['voucher_date'] = $this->AppUtilities->date_format($arrSaveRecords['PurchaseMaster']['voucher_date'],'yyyy-mm-dd');
                        #pr($arrSaveRecords);
                        if(!$this->PurchaseMaster->save($arrSaveRecords,array('validate' => false,'fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        
                        if(!isset($arrSaveRecords['PurchaseMaster']['id'])) {
                            $arrSaveRecords['PurchaseMaster']['id'] = $this->PurchaseMaster->getInsertID();
                        }

                        if(isset($arrProdctDetail) && count($arrProdctDetail) > 0) {
                            $arrPurchaseProductTranData = $arrProductStockTranData = array();
                            $arrProductId = array();
                            $count = 0;
                            $taxcount = 0;
                            foreach($arrProdctDetail as $key => $product) {
                                if(isset($product['purchase_tran_id']) && !empty($product['purchase_tran_id'])) {
                                    $arrPurchaseProductTranData[$count]['PurchaseTran']['id'] = $product['purchase_tran_id'];
                                }
                                $arrProductId[$product['product_variant_tran_id']] = $product['product_variant_tran_id'];
                                $arrPurchaseProductTranData[$count]['PurchaseTran']['purchase_master_id'] = $arrSaveRecords['PurchaseMaster']['id'];
                                $arrPurchaseProductTranData[$count]['PurchaseTran']['product_variant_tran_id'] = $product['product_variant_tran_id'];
                                $arrPurchaseProductTranData[$count]['PurchaseTran']['quantity'] = $product['quantity'];
                                $arrPurchaseProductTranData[$count]['PurchaseTran']['temp_price'] = $product['sale_price'];
                                $arrPurchaseProductTranData[$count]['PurchaseTran']['branch_master_id'] = $arrSaveRecords['PurchaseMaster']['branch_master_id'];
                                
                                if(isset($arrProductStockData[$product['product_variant_tran_id']])) {
                                    $arrProductStockTranData[$count]['ProductVariantStockTran']['id'] = $arrProductStockData[$product['product_variant_tran_id']];
                                }
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['date'] = date('Y-m-d H:i:s');
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['reference_master_id'] = $arrSaveRecords['PurchaseMaster']['id'];
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['company_year_master_id'] = $arrSaveRecords['PurchaseMaster']['company_year_master_id'];
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['product_variant_tran_id'] = $product['product_variant_tran_id'];
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['old_product_variant_tran_id'] = $product['product_variant_tran_id'];
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['quantity'] = (int)$product['quantity'];
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['type'] = 2;
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['from_type'] = 1;
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['user_id'] = $this->Session->read('sessUserId');
                                $arrProductStockTranData[$count]['ProductVariantStockTran']['branch_master_id'] = $arrSaveRecords['PurchaseMaster']['branch_master_id'];
                                ++$count;
                            }
                            #pr($arrPurchaseProductTranData);
                            #pr($arrProductStockTranData);exit;

                            if(isset($arrSaveRecords['PurchaseMaster']['id']) && !empty($arrSaveRecords['PurchaseMaster']['id']) && !empty($arrProductId)) {
                                $updateFields['PurchaseTran.status'] = 0;
                                $updateFields['PurchaseTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['PurchaseTran.purchase_master_id'] = $arrSaveRecords['PurchaseMaster']['id'];
                                $updateParams['NOT']['PurchaseTran.product_variant_tran_id'] = $arrProductId;
                                $this->PurchaseTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);

                                $updateFields['ProductVariantStockTran.status'] = 0;
                                $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['ProductVariantStockTran.reference_master_id'] = $arrSaveRecords['PurchaseMaster']['id'];
                                $updateParams['ProductVariantStockTran.type'] = 2;
                                $updateParams['NOT']['ProductVariantStockTran.product_variant_tran_id'] = $arrProductId;
                                $this->ProductVariantStockTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);
                            }

                            $this->PurchaseTran->saveAll($arrPurchaseProductTranData);
                            if(count($arrProductStockTranData) > 0) {
                                $this->ProductVariantStockTran->saveAll($arrProductStockTranData);
                            }
                        }
                        
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['PurchaseMaster']['id']) && !empty($arrSaveRecords['PurchaseMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->PurchaseMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = 0,$type = 1,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId)) {
                    $type = (int) $type;
                    $id = $this->decryption(trim($id));
                    if($type === 2) {
                        $options = array(
                            'fields' => array('id','voucher_no','voucher_date','quantity','supplier_master_id','description',"CONCAT_WS(' ',TM.name,SupplierMaster.first_name,SupplierMaster.middle_name,SupplierMaster.last_name) AS supplier",'SupplierMaster.address','SupplierMaster.mobile_no','SupplierMaster.email_id'),
                            'joins' => array(
                                array(
                                    'table' => 'supplier_masters',
                                    'alias' => 'SupplierMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('PurchaseMaster.supplier_master_id = SupplierMaster.id','SupplierMaster.status' => 1)
                                ),
                                array(
                                    'table' => 'title_masters',
                                    'alias' => 'TM',
                                    'type' => 'LEFT',
                                    'conditions' => array('SupplierMaster.title_master_id = TM.id','TM.status' => 1)
                                )
                            ),
                            'conditions' => array('PurchaseMaster.status' => 1,'PurchaseMaster.id' => $id,'PurchaseMaster.branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                    } else {
                        $options = array(
                            'fields' => array('id','voucher_no','voucher_date','quantity','supplier_master_id','description'),
                            'conditions' => array('status' => 1,'id' => $id,'PurchaseMaster.branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                    }
                    $arrRecords = $this->PurchaseMaster->find('first',$options);
                    if(count($arrRecords) > 0) {
                        $arrProductTranData = array();
                        $this->loadModel('ProductMaster');
                        $options = array(
                                        'fields' => array('PVT.id','ProductMaster.id','name','sale_price','code','hsn_code','PT.id','PT.quantity'),
                                        'joins' => array(
                                            array(
                                                'table' => 'product_variant_trans',
                                                'alias' => 'PVT',
                                                'type' => 'INNER',
                                                'conditions' => array('ProductMaster.id = PVT.product_master_id','PVT.status' => 1)
                                            ),
                                            array(
                                                'table' => 'purchase_trans',
                                                'alias' => 'PT',
                                                'type' => 'INNER',
                                                'conditions' => array('PVT.id = PT.product_variant_tran_id','PT.status' => 1)
                                            )
                                        ),
                                        'conditions' => array('ProductMaster.status' => 1,'PT.purchase_master_id' => $id)
                                    );
                        $arrPurchaseTranData = $this->ProductMaster->find('all',$options);
                        if(!empty($arrPurchaseTranData)) {
                            foreach($arrPurchaseTranData as $key => $data) {
                                $arrProductTranData[$data['PT']['id']]['name'] = $data['ProductMaster']['name'];
                                $arrProductTranData[$data['PT']['id']]['code'] = $data['ProductMaster']['code'];
                                $arrProductTranData[$data['PT']['id']]['hsn_code'] = $data['ProductMaster']['hsn_code'];
                                $arrProductTranData[$data['PT']['id']]['quantity'] = (int)$data['PT']['quantity'];
                                $arrProductTranData[$data['PT']['id']]['purchase_tran_id'] = $data['PT']['id'];
                                $arrProductTranData[$data['PT']['id']]['id'] = $data['ProductMaster']['id'];
                                $arrProductTranData[$data['PT']['id']]['sale_price'] = $data['ProductMaster']['sale_price'];
                                $arrProductTranData[$data['PT']['id']]['product_variant_tran_id'] = $data['PVT']['id'];
                            }
                        }
                        $statusCode = 200;
                        $arrProductTranData = array_values($arrProductTranData);
                        $arrRecords['PurchaseMaster']['voucher_date'] = $this->AppUtilities->date_format($arrRecords['PurchaseMaster']['voucher_date'],'dd-mm-yyyy');
                        if($type === 2) {
                            $arrRecords['PurchaseMaster']['supplier_name'] = $arrRecords[0]['supplier'];
                            $arrRecords['PurchaseMaster']['mobile_no'] = $arrRecords['SupplierMaster']['mobile_no'];
                            $arrRecords['PurchaseMaster']['email_id'] = $arrRecords['SupplierMaster']['email_id'];
                            $arrRecords['PurchaseMaster']['address'] = $arrRecords['SupplierMaster']['address'];
                        }
                        $arrRecords['PurchaseMaster']['quantity'] = (int)$arrRecords['PurchaseMaster']['quantity'];
                        unset($this->request->data);
                        $response = array('status' => 1,'message' => __('record_fetched',true),'data' => $arrRecords['PurchaseMaster'],'product' => $arrProductTranData);
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->PurchaseMaster->getDataSource();
                    try {
                        $dataSource->begin();	
                        $updateFields['PurchaseMaster.status'] = 0;
                        $updateFields['PurchaseMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['PurchaseMaster.id'] = $this->request->data['id'];
                        $this->PurchaseMaster->updateAll($updateFields,$updateParams);

                        unset($updateFields,$updateParams);
                        $updateFields['PurchaseTran.status'] = 0;
                        $updateFields['PurchaseTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['PurchaseTran.purchase_master_id'] = $this->request->data['id'];
                        $this->PurchaseTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $updateFields['ProductVariantStockTran.status'] = 0;
                        $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ProductVariantStockTran.reference_master_id'] = $this->request->data['id'];
                        $updateParams['ProductVariantStockTran.type'] = 2;
                        $this->ProductVariantStockTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
