<?php
App::uses('AppController','Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class UserMemberMastersController extends AppController {
    public $name = 'UserMemberMasters';
    public $layout = false;
    public $uses = array('UserMemberMaster','ErrorLog','User','UserRoleTran','AutoMemberCode','RoleMaster');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($userId = null,$module = null) {
        try {
            $userRights = array();
            if(!empty($userId)) {
                $userRights = Cache::read('userLinkRights'.$userId,'long');
            }
            $this->autoRender = false;
            $this->response->type('json');
            $response = array('status' => 0,'message' => 'invalid');
            $statusCode = 400;
            if($this->request->is('post')) {
                $conditions = array('UserMemberMaster.status' => 1);
                $arrSearchFields = array(1 =>'TypeTran.name',2 => 'UserMemberMaster.unique_no',3 => 'UserMemberMaster.mobile_no');
                if(isset($this->request->data['searchFields']) && isset($this->request->data['searchText']) && !empty($this->request->data['searchFields']) && !empty($this->request->data['searchText'])){
                    $conditions[$arrSearchFields[$this->request->data['searchFields']]. ' LIKE'] = '%'.trim($this->request->data['searchText']).'%';
                }
                $tableCountOptions = array(
                                        'fields' => array('id'),
                                        'joins' => array(
                                            array(
                                                'table' => 'type_trans',
                                                'alias' => 'TypeTran',
                                                'type' => 'INNER',
                                                'conditions' => array('UserMemberMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                            )
                                        ),
                                        'conditions' => $conditions,
                                        'recursive' => -1
                                    );
                $totalRecords = $this->UserMemberMaster->find('count',$tableCountOptions);
                $displayLength = isset($this->request->data['length'])?intval($this->request->data['length']):0;
                $displayLength = ($displayLength < 0) ? $totalRecords : $displayLength; 
                $displayStart = isset($this->request->data['start'])?intval($this->request->data['start']):0;
                $draw = isset($this->request->data['draw'])?intval($this->request->data['draw']):'';
                $end = $displayStart + $displayLength;
                $end = $end > $totalRecords ? $totalRecords : $end;
                $arrSortColumn = array(2 => 'UserMemberMaster.firm_name',3 => 'UserMemberMaster.unique_no',4 => 'TypeTran.name',5 => 'UserMemberMaster.mobile_no');
                if(isset($this->request->data['order'][0]['column']) && isset($arrSortColumn[$this->request->data['order'][0]['column']])) {
                    $orderCondition = $arrSortColumn[$this->request->data['order'][0]['column']].' '.strtoupper($this->request->data['order'][0]['dir']);
                } else {
                    $orderCondition = "UserMemberMaster.name ASC";
                }
                $this->AppUtilities = (new View($this))->loadHelper('AppUtilities');
                $tableOptions = array(
                                    'fields' => array('id','firm_name','TypeTran.name','first_name','middle_name','last_name','unique_no','mobile_no','COUNT(VM.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'type_trans',
                                            'alias' => 'TypeTran',
                                            'type' => 'INNER',
                                            'conditions' => array('UserMemberMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                        ),
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'LEFT',
                                            'conditions' => array('UserMemberMaster.id = VM.user_member_master_id','VM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'UserMemberMaster.id',
                                    'limit' => $displayLength,
                                    'offset' => $displayStart,
                                    'order' => $orderCondition,
                                    'recursive' => -1
                                );
                $arrTableData = $this->UserMemberMaster->find('all',$tableOptions);
                $isEditRights = $isViewRights = false;
                if(!empty($module)) {
                    if(isset($userRights[$module]['edit']) && !empty($userRights[$module]['edit'])) {
                        $isEditRights = true;
                    }
                    if(isset($userRights[$module]['view']) && !empty($userRights[$module]['view'])) {
                        $isViewRights = true;
                    }
                }
                $records = array('draw' => $draw,'recordsTotal' => $totalRecords,'recordsFiltered' => $totalRecords,'data' => array());
                if($totalRecords > 0) {
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['UserMemberMaster']['id']);
                        $action = '';
                        if($isEditRights === true && $tableDetails[0]['count'] <= 0) {
                            $action = $this->AppUtilities->roleActionType('edit','Edit',$encryption,'Edit Record')." ";
                        }
                        if($isViewRights === true) {
                            $action.= $this->AppUtilities->roleActionType('view','View',$encryption,'View Record');
                        }
                        //$name = $tableDetails['TitleMaster']['name'].' '.$tableDetails['UserMemberMaster']['first_name'].' '.$tableDetails['UserMemberMaster']['middle_name'].' '.$tableDetails['UserMemberMaster']['last_name'];
                        $name = $tableDetails['TypeTran']['name'];
                        $checkbox = '<input type="checkbox" class="selectAllCheckData" name="id[]" value="'.$encryption.'">';
                        if($tableDetails[0]['count'] > 0) {
                            $checkbox = '&nbsp;&nbsp;--';
                        }
                        $records["data"][$key] =array(
                                                   $checkbox,
                                                    ++$displayStart,
                                                    $tableDetails['UserMemberMaster']['firm_name'],
                                                    $tableDetails['UserMemberMaster']['unique_no'],
                                                    $name,
                                                    $tableDetails['UserMemberMaster']['mobile_no'],
                                                    $action,
                                                );
                    }
                    $records["draw"] = $draw;
                    $records["recordsTotal"] = $totalRecords;
                    $records["recordsFiltered"] = $totalRecords;	
                    $response = $records;
                } else {
                    $response = $records;
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $json = json_encode($response);
        $this->response->body($json);
    }

    public function save() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        $isShowMessage = 0;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['UserMemberMaster'] = $this->request->data;
                unset($this->request->data);
                
                if(isset($arrSaveRecords['UserMemberMaster']['id']) && !empty($arrSaveRecords['UserMemberMaster']['id'])) {
                    $arrSaveRecords['UserMemberMaster']['id'] = $this->decryption($arrSaveRecords['UserMemberMaster']['id']);
                }
                #pr($arrSaveRecords);
                $this->UserMemberMaster->set($arrSaveRecords);
                if($this->UserMemberMaster->validates()) {
                    #pr($arrSaveRecords);exit;
                    $dataSource = $this->UserMemberMaster->getDataSource();
                    $fieldList = array('id','firm_name','registration_date','first_name','middle_name','last_name','unique_no','max_unique_no','user_id','title_master_id','gender','gst_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','member_type_tran_id','c_address','p_address','logo','photo');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['UserMemberMaster']['id']) || empty($arrSaveRecords['UserMemberMaster']['id'])) {
                            $arrSaveMemberCode['AutoMemberCode']['name'] = $arrSaveRecords['UserMemberMaster']['first_name'];
                            $this->AutoMemberCode->create();
                            if(!$this->AutoMemberCode->save($arrSaveMemberCode)) {
                                throw new Exception(__('Auto member data could not saved',true));
                            }
                            $maxNumber = $this->AutoMemberCode->getInsertID();
                            $arrMemberCode = $this->AppUtilities->getMemberUniqueNo($maxNumber);
                            $userName = 'RADIZO0000'.$maxNumber;
                            if(!empty($arrMemberCode)) {
                                $arrSaveRecords['UserMemberMaster']['unique_no'] = $arrMemberCode['code'];
                                $arrSaveRecords['UserMemberMaster']['max_unique_no'] = $arrMemberCode['maxNumber'];
                                $replaceData = array('/' => '','-' => '','%' => '');
                                $userName = strtr($arrMemberCode['code'],$replaceData);
                            }
                            $options = array('fields' => array('id'),'conditions' => array('unique_no' => $arrSaveRecords['UserMemberMaster']['unique_no']));
                            $validMember = $this->UserMemberMaster->find('count',$options);
                            if($validMember > 0) {
                                $isShowMessage = 1;
                                throw new Exception(__('Member Code already exists !...',true));
                            }
                            unset($options);
                            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                            $arrUserData['User']['username'] = $userName;
                            $arrUserData['User']['password'] = $passwordHasher->hash($arrSaveRecords['UserMemberMaster']['mobile_no']);
                            $arrUserData['User']['encrypt'] = $this->encrypt_algo($arrSaveRecords['UserMemberMaster']['mobile_no']);
                            $arrUserData['User']['fullname'] = $arrSaveRecords['UserMemberMaster']['first_name'].' '.$arrSaveRecords['UserMemberMaster']['middle_name'].' '.$arrSaveRecords['UserMemberMaster']['last_name'];
                            $this->User->create();
                            if(!$this->User->save($arrUserData,array('validate'=>false))) {
                                throw new Exception(__('UserName already exists!',true));
                            }
                            $userId = $this->User->getInsertID();
                            $arrSaveRecords['UserMemberMaster']['user_id'] = $userId;
                            $options = array('fields' => array('id'),'conditions' => array('type_tran_id' => $arrSaveRecords['UserMemberMaster']['member_type_tran_id'],'status' => 1));
                            $arrRoleData = $this->RoleMaster->find('first',$options);
                            if(!empty($arrRoleData)) {
                                $arrUserRoleTran['UserRoleTran']['role_master_id'] = $arrRoleData['RoleMaster']['id'];
                                $arrUserRoleTran['UserRoleTran']['user_id'] = $userId;
                                $this->UserRoleTran->create();
                                if(!$this->UserRoleTran->save($arrUserRoleTran)) {
                                    throw new Exception(__('User role tran could not saved properly,Please try again later !.',true));
                                }
                            }
                            $this->UserMemberMaster->create();
                        } else {
                            if(empty($arrSaveRecords['UserMemberMaster']['photo'])) {
                                $arrSaveRecords['UserMemberMaster']['photo'] = $arrSaveRecords['UserMemberMaster']['old_photo'];
                                unset($arrSaveRecords['UserMemberMaster']['old_photo']);
                            }
                            if(empty($arrSaveRecords['UserMemberMaster']['logo'])) {
                                $arrSaveRecords['UserMemberMaster']['logo'] = $arrSaveRecords['UserMemberMaster']['old_logo'];
                                unset($arrSaveRecords['UserMemberMaster']['old_logo']);
                            }
                        }
                        
                        $arrSaveRecords['UserMemberMaster']['registration_date'] = date('Y-m-d');
                        if(!$this->UserMemberMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $memberId = '';
                        if(!isset($arrSaveRecords['UserMemberMaster']['id'])) {
                            $memberId = $this->UserMemberMaster->getInsertID();
                        }
                        $dataSource->commit();
                        if(isset($arrSaveRecords['UserMemberMaster']['photo']) && !empty($arrSaveRecords['UserMemberMaster']['photo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$arrSaveRecords['UserMemberMaster']['photo'];
                            $saveImagePath = Configure::read('UPLOAD_MEMBER_IMAGE_DIR').'/'.$arrSaveRecords['UserMemberMaster']['photo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                            if(isset($arrSaveRecords['UserMemberMaster']['old_photo']) && !empty($arrSaveRecords['UserMemberMaster']['old_photo'])) {
                                $files =  Configure::read('UPLOAD_MEMBER_IMAGE_DIR').'/'.$arrSaveRecords['UserMemberMaster']['old_photo'];
                                if(file_exists($files)) {
                                    @unlink($files);
                                }
                            }
                        }
                        if(isset($arrSaveRecords['UserMemberMaster']['logo']) && !empty($arrSaveRecords['UserMemberMaster']['logo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$arrSaveRecords['UserMemberMaster']['logo'];
                            $saveImagePath = Configure::read('UPLOAD_SETTING_LOGO_DIR').'/'.$arrSaveRecords['UserMemberMaster']['logo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                            if(isset($arrSaveRecords['UserMemberMaster']['old_logo']) && !empty($arrSaveRecords['UserMemberMaster']['old_logo'])) {
                                $files =  Configure::read('UPLOAD_SETTING_LOGO_DIR').'/'.$arrSaveRecords['UserMemberMaster']['old_logo'];
                                if(file_exists($files)) {
                                    @unlink($files);
                                }
                            }
                        }
    
                        $statusCode = 200;
                        if(isset($arrSaveRecords['UserMemberMaster']['id']) && !empty($arrSaveRecords['UserMemberMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('updated_record',true),'is_send_mail' => 0);
                        } else {
                            $response = array('status' => 1,'message' => __('saved_record',true),'is_send_mail' => 1,'id' =>$memberId);
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        if($isShowMessage == 1) {
                            $response = array('status' => 0,'message' => __($e->getMessage(),true));
                        } else {
                            $response = array('status' => 0,'message' => __('internal_server_error',true));
                        }
                    }
                } else {
                    $validationErrros = Set::flatten($this->UserMemberMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('id','TypeTran.name','CountryMaster.name','StateMaster.name','CityMaster.name','registration_date','firm_name','first_name','middle_name','last_name','unique_no','max_unique_no','user_id','title_master_id','gender','gst_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','member_type_tran_id','c_address','p_address','logo','photo'),
                            'joins' => array(
                                array(
                                    'table' => 'type_trans',
                                    'alias' => 'TypeTran',
                                    'type' => 'INNER',
                                    'conditions' => array('UserMemberMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'country_masters',
                                    'alias' => 'CountryMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('UserMemberMaster.country_master_id = CountryMaster.id','CountryMaster.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'state_masters',
                                    'alias' => 'StateMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('UserMemberMaster.state_master_id = StateMaster.id','StateMaster.status' => 1,'TypeTran.role_type' => 3)
                                ),
                                array(
                                    'table' => 'city_masters',
                                    'alias' => 'CityMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('UserMemberMaster.city_master_id = CityMaster.id','CityMaster.status' => 1,'TypeTran.role_type' => 3)
                                )
                            ),
                            'conditions' => array('UserMemberMaster.status' => 1,'UserMemberMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->UserMemberMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            $arrRecords['UserMemberMaster']['member_type'] = $arrRecords['TypeTran']['name'];
                            $arrRecords['UserMemberMaster']['country'] = $arrRecords['CountryMaster']['name'];
                            $arrRecords['UserMemberMaster']['state'] = $arrRecords['StateMaster']['name'];
                            $arrRecords['UserMemberMaster']['city'] = $arrRecords['CityMaster']['name'];
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('record_fetched',true),'data' => $arrRecords['UserMemberMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('no_record',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('invalid_response',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $dataSource = $this->UserMemberMaster->getDataSource();
                    try{
                        $dataSource->begin();
                        $options = array('fields' => array('id','user_id'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                        $arrUserData = $this->UserMemberMaster->find('list',$options);
                        if(!empty($arrUserData)) {
                            $arrUserId = array_values($arrUserData);
                            $updateFields = $updateParams = array();
                            $updateFields['User.status'] = 0;
                            $updateFields['User.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['User.id'] = $arrUserId;
                            if(!$this->User->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $updateFields = $updateParams = array();
                            $updateFields['UserRoleTran.status'] = 0;
                            $updateFields['UserRoleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['UserRoleTran.user_id'] = $arrUserId;
                            if(!$this->UserRoleTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }
                        $updateFields = $updateParams = array();
                        $updateFields['UserMemberMaster.status'] = 0;
                        $updateFields['UserMemberMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['UserMemberMaster.id'] = $this->request->data['id'];
                        if(!$this->UserMemberMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('deleted_record',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('internal_server_error',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('invalid_response',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $exportOptions = array(
                                    'fields' => array('id','firm_name','unique_no','mobile_no','TypeTran.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'type_trans',
                                            'alias' => 'TypeTran',
                                            'type' => 'INNER',
                                            'conditions' => array('UserMemberMaster.member_type_tran_id = TypeTran.id','TypeTran.status' => 1,'TypeTran.role_type' => 3)
                                        )
                                    ),
                                    'conditions' => array('UserMemberMaster.status' => 1),
                                    'recursive' => -1
                                );
                $arrExportData = $this->UserMemberMaster->find('all',$exportOptions);
                if(count($arrExportData) > 0) {
                    $statusCode = 200;
                    $counter = 0;
                    $arrTableColumnValues = array();
                    foreach($arrExportData as $key => $exportDetails) {
                        ++$counter;
                        $arrTableColumnValues[$key]['sno'] = $counter.'.';
                        $arrTableColumnValues[$key]['firm_name'] = $exportDetails['UserMemberMaster']['firm_name'];
                        $arrTableColumnValues[$key]['unique_code'] = $exportDetails['UserMemberMaster']['unique_no'];
                        $arrTableColumnValues[$key]['member'] = $exportDetails['TypeTran']['name'];
                        $arrTableColumnValues[$key]['mobileno'] = $exportDetails['UserMemberMaster']['mobile_no'];
                    }
                    $headers = array('sno'=>'S.No','firm_name'=>'Firm Name','unique_code'=>'Unique Code','member'=>'Member','mobileno' => 'Mobile No');
                    $response = array('status' => 1,'date' => date('Y-m-d-H:i:s'),'message' => __('record_fetched',true),'data' => $arrTableColumnValues,'header' => $headers);
                } else {
                    $response = array('status' => 0,'message' => __('no_record',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'user_member_masters','method' => 'export','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>