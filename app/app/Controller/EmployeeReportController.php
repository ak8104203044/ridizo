<?php
App::uses('AppController','Controller');

class EmployeeReportController extends AppController {
    public $name = 'EmployeeReport';
    public $uses = array('ErrorLog','EmployeeMaster');

    public function employee_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['employee']) && !empty($this->request->data['employee'])) {
                    $count = 0;
                    if(isset($this->request->data['employee'][0]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.unique_no';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Employee Code';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][0]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][1]['field'])) {
                        $arrFieldData[$count]['columnName'] = "CONCAT_WS(' ',TitleMaster.name,EmployeeMaster.first_name,EmployeeMaster.middle_name,EmployeeMaster.last_name) as fullname";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'fullname';
                        $arrFieldData[$count]['caption'] = 'Name';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][1]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][2]['field'])) {
                        $arrFieldData[$count]['columnName'] = "IF(EmployeeMaster.gender = 1,'Male','Female') AS gender";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'gender';
                        $arrFieldData[$count]['caption'] = 'Gender';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][2]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][3]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.mobile_no';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][3]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][4]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.alternate_mobile_no';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'alternate_mobile_no';
                        $arrFieldData[$count]['caption'] = 'Alternate Mobile No';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][4]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][5]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.email_id';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][5]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][6]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.alternate_email_id';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'alternate_email_id';
                        $arrFieldData[$count]['caption'] = 'Alternate Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][6]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][7]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'CountryMaster.name';
                        $arrFieldData[$count]['tableName'] = 'CountryMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Country';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][7]['order'];
                        ++$count;
                    }
                    if(isset($this->request->data['employee'][8]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'StateMaster.name';
                        $arrFieldData[$count]['tableName'] = 'StateMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'State';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][8]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][9]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'CityMaster.name';
                        $arrFieldData[$count]['tableName'] = 'CityMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'City';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][9]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][10]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.date_of_joining';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'date_of_joining';
                        $arrFieldData[$count]['caption'] = 'Date of Joining';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][10]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][11]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'DepartmentMaster.name';
                        $arrFieldData[$count]['tableName'] = 'DepartmentMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Department';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][11]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][12]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'DesignationMaster.name';
                        $arrFieldData[$count]['tableName'] = 'DesignationMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Designation';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][12]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][13]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.qualification';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'qualification';
                        $arrFieldData[$count]['caption'] = 'Qualification';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][13]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][14]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.pan_number';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'pan_number';
                        $arrFieldData[$count]['caption'] = 'PAN No.';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][14]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][15]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.aadhar_number';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'aadhar_number';
                        $arrFieldData[$count]['caption'] = 'Permanent Address';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][15]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][16]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.c_address';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'c_address';
                        $arrFieldData[$count]['caption'] = 'Residential Address';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][16]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['employee'][17]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'EmployeeMaster.p_address';
                        $arrFieldData[$count]['tableName'] = 'EmployeeMaster';
                        $arrFieldData[$count]['fieldName'] = 'p_address';
                        $arrFieldData[$count]['caption'] = 'Permanent Address';
                        $arrFieldData[$count]['order'] = $this->request->data['employee'][17]['order'];
                        ++$count;
                    }
                }
                
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    #pr($arrFieldData);exit;
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[$key]['field'] = $header['fieldName'];
                        $arrHeaderData[$key]['caption'] = $header['caption'];
                    }
                    $conditions = array('EmployeeMaster.status' => 1);
                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['EmployeeMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['department_master_id']) && !empty($this->request->data['department_master_id'])) {
                        $conditions['DepartmentMaster.id'] = $this->request->data['department_master_id'];
                    }
                    
                    if(isset($this->request->data['designation_master_id']) && !empty($this->request->data['designation_master_id'])) {
                        $conditions['DesignationMaster.id'] = $this->request->data['designation_master_id'];
                    }
                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'EmployeeMaster.max_unique_no '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'EmployeeMaster.first_name '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'DepartmentMaster.name '.$sortyType);
                                    break;
                            case 4:
                                    $orderBy = array('order' => 'DesignationMaster.name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'EmployeeMaster.max_unique_no '.$sortyType);
                                    break;
                        }
                    }
                    $options = array(
                        'fields' => array_column($arrFieldData,'columnName'),
                        'joins' => array(
                            array(
                                'table' => 'employee_trans',
                                'alias' => 'EmployeeTran',
                                'type' => 'INNER',
                                'conditions' => array('EmployeeMaster.id = EmployeeTran.employee_master_id','EmployeeTran.status' => 1)
                            ),
                            array(
                                'table' => 'designation_masters',
                                'alias' => 'DesignationMaster',
                                'type' => 'INNER',
                                'conditions' => array('EmployeeTran.designation_master_id = DesignationMaster.id','DesignationMaster.status' => 1)
                            ),
                            array(
                                'table' => 'department_masters',
                                'alias' => 'DepartmentMaster',
                                'type' => 'INNER',
                                'conditions' => array('DesignationMaster.department_master_id = DepartmentMaster.id','DepartmentMaster.status' => 1)
                            ),
                            array(
                                'table' => 'country_masters',
                                'alias' => 'CountryMaster',
                                'type' => 'LEFT',
                                'conditions' => array('EmployeeMaster.country_master_id = CountryMaster.id','CountryMaster.status' => 1)
                            ),
                            array(
                                'table' => 'state_masters',
                                'alias' => 'StateMaster',
                                'type' => 'LEFT',
                                'conditions' => array('EmployeeMaster.state_master_id = StateMaster.id','StateMaster.status' => 1)
                            ),
                            array(
                                'table' => 'city_masters',
                                'alias' => 'CityMaster',
                                'type' => 'LEFT',
                                'conditions' => array('EmployeeMaster.city_master_id = CityMaster.id','CityMaster.status' => 1)
                            ),
                            array(
                                'table' => 'title_masters',
                                'alias' => 'TitleMaster',
                                'type' => 'LEFT',
                                'conditions' => array('EmployeeMaster.title_master_id = TitleMaster.id','TitleMaster.status' => 1)
                            )
                        ),
                        'conditions' => $conditions,
                        'recursive' => -1,
                        'group' => array('EmployeeMaster.id')
                    );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->EmployeeMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select atleast one field !.',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>