<?php
App::uses('AppController','Controller');
class DesignationMastersController extends AppController {
    public $name = 'DesignationMasters';
    public $layout = false;
    public $uses = array('DesignationMaster','EmployeeTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : 0;
                $conditions = array('DesignationMaster.branch_master_id' => $firmId,'DesignationMaster.status' => 1);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['DesignationMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['DesignationMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                if(isset($this->request->data['department_master_id']) && !empty($this->request->data['department_master_id'])) {
                    $conditions['DesignationMaster.department_master_id'] = trim($this->request->data['department_master_id']);
                }
                
                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('DesignationMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('DesignationMaster.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('DepartmentMaster.name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('DesignationMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('DesignationMaster.order_no ASC');
                }
                
                $joins =    array(
                                array(
                                    'table' => 'department_masters',
                                    'alias' => 'DepartmentMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('DepartmentMaster.id = DesignationMaster.department_master_id','DesignationMaster.status' => 1)
                                )
                            );

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'joins' => $joins,'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->DesignationMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $joins[] = array(
                                'table' => 'employee_trans',
                                'alias' => 'ET',
                                'type' => 'LEFT',
                                'conditions' => array('DesignationMaster.id = ET.designation_master_id','ET.status' => 1)
                            );
                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','department_name','COUNT(ET.id) AS count'),
                                    'joins' => $joins,
                                    'conditions' => $conditions,
                                    'group' => 'DesignationMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->DesignationMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['DesignationMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['DesignationMaster']['name'];
                        $records[$key]['code'] = $tableDetails['DesignationMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['DesignationMaster']['order_no'];
                        $records[$key]['department'] = $tableDetails['DesignationMaster']['department_name'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['DesignationMaster']['order_no'] > $maxOrderNo) ? $tableDetails['DesignationMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','department' => 'Department','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $this->request->data['branch_master_id'] = (isset($this->request->data['branch_master_id'])) ? $this->request->data['branch_master_id'] : '';
                $arrSaveRecords['DesignationMaster'] = $this->request->data;
                //pr($arrSaveRecords['DesignationMaster']);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['DesignationMaster']['id']) && !empty($arrSaveRecords['DesignationMaster']['id'])) {
                    $arrSaveRecords['DesignationMaster']['id'] = $this->decryption($arrSaveRecords['DesignationMaster']['id']);
                }
                $this->DesignationMaster->set($arrSaveRecords);
                if($this->DesignationMaster->validates()) {
                    $dataSource = $this->DesignationMaster->getDataSource();
                    $fieldList = array('id','name','code','department_master_id','description','branch_master_id','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['DesignationMaster']['id']) || empty($arrSaveRecords['DesignationMaster']['id'])) {
                            $this->DesignationMaster->create();
                        }
                        if(!$this->DesignationMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['DesignationMaster']['id']) && !empty($arrSaveRecords['DesignationMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->DesignationMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','department_master_id','department_name','order_no'),
                            'joins' => array(
                                array(
                                    'table' => 'department_masters',
                                    'alias' => 'DepartmentMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('DepartmentMaster.id = DesignationMaster.department_master_id','DesignationMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('DesignationMaster.status' => 1,'DesignationMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->DesignationMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['DesignationMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('designation_master_id'),'conditions' => array('status'=> 1,'designation_master_id' => $this->request->data['id']));
                    $arrRecords = $this->EmployeeTran->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->DesignationMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['DesignationMaster.status'] = 0;
                            $updateFields['DesignationMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['DesignationMaster.id'] = $arrDeleteRecords;
                            if(!$this->DesignationMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
