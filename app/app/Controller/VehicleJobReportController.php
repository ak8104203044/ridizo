<?php
App::uses('AppController','Controller');
class VehicleJobReportController extends AppController {
    public $name = 'VehicleJobReport';
    public $layout = false;
    public $uses = array('VehicleJobMaster','ErrorLog');
    public $components = array('AppUtilities');
    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function vehicle_job_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['vehicle_job']) && !empty($this->request->data['vehicle_job'])) {
                    $fcount = $count = 0;
                    $arrFieldData = $arrHeaderData = array();
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.unique_no";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Job Code';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleJobMaster.job_date,'%d-%m-%Y') AS job_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'job_date';
                        $arrFieldData[$count]['caption'] = 'Job Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "InvoiceMaster.invoice_no";
                        $arrFieldData[$count]['tableName'] = 'InvoiceMaster';
                        $arrFieldData[$count]['fieldName'] = 'invoice_no';
                        $arrFieldData[$count]['caption'] = 'Invoice No';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(InvoiceMaster.invoice_date,'%d-%m-%Y') AS invoice_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'invoice_date';
                        $arrFieldData[$count]['caption'] = 'Invoice Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "(CASE VehicleJobMaster.job_status WHEN 1 THEN 'OPEN' WHEN 2 THEN 'Assign To Seninor Technician' WHEN 3 THEN 'Assign To Junior Technician' WHEN 4 THEN 'In Progress' WHEN 5 THEN 'Hold' WHEN 6 THEN 'Re-Assign' WHEN 7 THEN 'Job Done' ELSE 'Completed' END) AS job_status";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'job_status';
                        $arrFieldData[$count]['caption'] = 'Job Status';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.manufacturer_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                        $arrFieldData[$count]['caption'] = 'Manufacturer';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_model_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Model';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.unique_no";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Vehicle Code';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                        $arrFieldData[$count]['caption'] = 'Vehicle No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.engine_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'engine_number';
                        $arrFieldData[$count]['caption'] = 'Engine No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.chasis_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'chasis_number';
                        $arrFieldData[$count]['caption'] = 'Chasis No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "IF(VehicleJobMaster.in_house_vehicle = 1, 'Yes','No') AS in_house_vehicle";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'in_house_vehicle';
                        $arrFieldData[$count]['caption'] = 'In House Vehicle';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.service_km";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'service_km';
                        $arrFieldData[$count]['caption'] = 'Service K.M';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.next_service_km";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'next_service_km';
                        $arrFieldData[$count]['caption'] = 'Next Service K.M';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleJobMaster.next_service_date,'%d-%m-%Y') AS next_service_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'next_service_date';
                        $arrFieldData[$count]['caption'] = 'Next Service Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(VehicleJobMaster.completion_date,'%d-%m-%Y') AS completion_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'completion_date';
                        $arrFieldData[$count]['caption'] = 'Completion Date';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.amount";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'amount';
                        $arrFieldData[$count]['caption'] = 'Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.discount";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'discount';
                        $arrFieldData[$count]['caption'] = 'Discount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "CouponMaster.name";
                        $arrFieldData[$count]['tableName'] = 'CouponMaster';
                        $arrFieldData[$count]['fieldName'] = 'coupon';
                        $arrFieldData[$count]['caption'] = 'Coupon Name';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.coupon";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'coupon';
                        $arrFieldData[$count]['caption'] = 'Coupon Discount';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "(CASE InvoiceMaster.payment_status WHEN 0 THEN 'Pending' WHEN 1 THEN 'Partial' ELSE 'Paid' END) AS payment_status";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'payment_status';
                        $arrFieldData[$count]['caption'] = 'Payment Status';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.customer_name";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'customer_name';
                        $arrFieldData[$count]['caption'] = 'Customer Name';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.customer_mobile_no";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'customer_mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.customer_email_id";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'customer_email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['vehicle_job'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "VehicleJobMaster.customer_address";
                        $arrFieldData[$count]['tableName'] = 'VehicleJobMaster';
                        $arrFieldData[$count]['fieldName'] = 'customer_address';
                        $arrFieldData[$count]['caption'] = 'Address';
                        $arrFieldData[$count]['order'] = $this->request->data['vehicle_job'][$fcount]['order'];
                        ++$count;
                    }
                    if(count($arrFieldData) > 0) {
                        uasort($arrFieldData,function($a,$b){
                            if($a['order'] == 0){
                                return 1;
                            }
                            if($b['order'] == 0){
                                return -1;
                            }
                            return ($a['order'] < $b['order']) ? -1 : 1;
                        });
                        foreach($arrFieldData as $key => $header) {
                            $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                        }
                        $conditions = array('VehicleJobMaster.status' => 1);
                        if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                            $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                            $conditions['VehicleJobMaster.job_date >='] = $fromDate;
                        }

                        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                            $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                            $conditions['VehicleJobMaster.job_date <='] = $endDate;
                        }

                        if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                            $conditions['VehicleJobMaster.branch_master_id'] = $this->request->data['branch_master_id']; 
                        }

                        if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                            $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                        }
                        
                        if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                            $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                        }

                        if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                            $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                        }

                        if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                            $conditions['VehicleJobMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                        }

                        if(isset($this->request->data['job_status']) && !empty($this->request->data['job_status'])) {
                            $conditions['VehicleJobMaster.job_status'] = $this->request->data['job_status'];
                        }

                        if(isset($this->request->data['inhouse_vehicle']) && !empty($this->request->data['inhouse_vehicle'])) {
                            $conditions['VehicleJobMaster.in_house_vehicle'] = intval($this->request->data['inhouse_vehicle']) - 1;
                        }

                        if(isset($this->request->data['payment_status']) && !empty($this->request->data['payment_status'])) {
                            $conditions['InvoiceMaster.payment_status'] = intval($this->request->data['payment_status']) - 1;
                        }
                        if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                            $sortBy = (int) $this->request->data['sort_by'];
                            $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                            switch($sortBy) {
                                case 1:
                                        $orderBy = array('order' => 'VehicleJobMaster.job_date '.$sortyType);
                                        break;
                                case 2:
                                        $orderBy = array('order' => 'Vehicle.vehicle_type_name '.$sortyType);
                                        break;
                                case 3:
                                        $orderBy = array('order' => 'Vehicle.manufacturer_name '.$sortyType);
                                        break;
                                case 4:
                                        $orderBy = array('order' => 'Vehicle.vehicle_model_name '.$sortyType);
                                        break;
                                case 5:
                                        $orderBy = array('order' => 'Vehicle.max_unique_no '.$sortyType);
                                        break;
                                case 5:
                                        $orderBy = array('order' => 'VehicleJobMaster.customer_name '.$sortyType);
                                        break;
                                default:
                                        $orderBy = array('order' => 'VehicleJobMaster.job_date '.$sortyType);
                                        break;
                            }
                        } else {
                            $orderBy = array('order' => 'VehicleJobMaster.job_date ASC');
                        }
                        $options = array(
                                        'fields' => array_column($arrFieldData,'columnName'),
                                        'joins' => array(
                                                array(
                                                    'table' => 'vehicles',
                                                    'alias' => 'Vehicle',
                                                    'type' => 'INNER',
                                                    'conditions' => array('VehicleJobMaster.vehicle_master_id = Vehicle.id','Vehicle.status' => 1)
                                                ),
                                                array(
                                                    'table' => 'invoice_masters',
                                                    'alias' => 'InvoiceMaster',
                                                    'type' => 'LEFT',
                                                    'conditions' => array('VehicleJobMaster.id = InvoiceMaster.customer_master_id','InvoiceMaster.status' => 1)
                                                ),
                                                array(
                                                    'table' => 'vehicle_coupon_trans',
                                                    'alias' => 'VCT',
                                                    'type' => 'LEFT',
                                                    'conditions' => array('VehicleJobMaster.vehicle_coupon_tran_id = VCT.id','VCT.status' => 1)
                                                ),
                                                array(
                                                    'table' => 'vehicle_coupon_masters',
                                                    'alias' => 'VCM',
                                                    'type' => 'LEFT',
                                                    'conditions' => array('VCT.vehicle_coupon_master_id = VCM.id','VCM.status' => 1)
                                                ),
                                                array(
                                                    'table' => 'coupon_masters',
                                                    'alias' => 'CouponMaster',
                                                    'type' => 'LEFT',
                                                    'conditions' => array('VCM.coupon_master_id = CouponMaster.id','CouponMaster.status' => 1)
                                                )
                                            ),
                                            'conditions' => $conditions,
                                    );
                        $options = array_merge($options,$orderBy);
                        $arrRecords = $this->VehicleJobMaster->find('all',$options);
                        if(count($arrRecords) > 0) {
                            $arrContentData = array();
                            foreach($arrRecords as $key => $data) {
                                foreach($arrFieldData as $index => $fields) {
                                    if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                        $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                    }
                                }
                            }
                            unset($arrFieldData);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true)); 
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
