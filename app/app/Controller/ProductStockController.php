<?php
App::uses('AppController','Controller');
class ProductStockController extends AppController {
    public $layout = false;
    public $uses = array('ProductMaster','ProductVariantStockTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($firmId = 0,$type = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($type == 0) {
                $options = array(
                    'fields' => array('ProductMaster.name','CategoryMaster.name','ProductVariantTran.price','ProductVariantTran.id','ProductMaster.code','ProductMaster.hsn_code','UnitMaster.name','ProductVariantStockTran.quantity'),
                    'joins' => array(
                        array(
                            'table' => 'category_masters',
                            'alias' => 'CategoryMaster',
                            'type' =>'INNER',
                            'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id','CategoryMaster.status' => 1,'CategoryMaster.branch_master_id' => $firmId)
                        ),
                        array(
                            'table' => 'unit_masters',
                            'alias' => 'UnitMaster',
                            'type' =>'LEFT',
                            'conditions' => array('ProductMaster.unit_master_id = UnitMaster.id','UnitMaster.status' => 1)
                        ),
                        array(
                            'table' => 'product_variant_trans',
                            'alias' => 'ProductVariantTran',
                            'type' =>'INNER',
                            'conditions' => array('ProductMaster.id = ProductVariantTran.product_master_id','ProductVariantTran.status' => 1)
                        ),
                        array(
                            'table' => 'product_variant_stock_trans',
                            'alias' => 'ProductVariantStockTran',
                            'type' =>'INNER',
                            'conditions' => array('ProductVariantTran.id = ProductVariantStockTran.product_variant_tran_id','ProductVariantStockTran.status' => 1)
                        )
                    ),
                    'conditions' => array('ProductMaster.branch_master_id' => $firmId,'ProductMaster.status' => 1),
                    'order' => 'ProductMaster.order_no'
                );
                $arrProductData = $this->ProductMaster->find('all',$options);
                if(count($arrProductData) > 0) {
                    $arrProduct = array();
                    foreach($arrProductData as $key => $product) {
                        $id = $product['ProductVariantTran']['id'];
                        if(isset($arrProduct[$id])) {
                            if($product['ProductVariantStockTran']['quantity'] > 0) {
                                $arrProduct[$id]['stock'] += intval($product['ProductVariantStockTran']['quantity']);
                            } else {
                                if(isset($arrProduct[$id]['consumed'])) {
                                    $arrProduct[$id]['consumed'] += abs($product['ProductVariantStockTran']['quantity']);
                                } else {
                                    $arrProduct[$id]['consumed'] = abs($product['ProductVariantStockTran']['quantity']);
                                }
                            }
                        } else {
                            $arrProduct[$id]['product'] = $product['ProductMaster']['name'];
                            $arrProduct[$id]['id'] = $product['ProductVariantTran']['id'];
                            $arrProduct[$id]['price'] = $product['ProductVariantTran']['price'];
                            $arrProduct[$id]['code'] = $product['ProductMaster']['code'];
                            $arrProduct[$id]['hsn_code'] = $product['ProductMaster']['hsn_code'];
                            $arrProduct[$id]['category'] = $product['CategoryMaster']['name'];
                            $arrProduct[$id]['unit'] = $product['UnitMaster']['name'];
                            if($product['ProductVariantStockTran']['quantity'] > 0) {
                                $arrProduct[$id]['stock'] = intval($product['ProductVariantStockTran']['quantity']);
                            } else {
                                $arrProduct[$id]['consumed'] = abs($product['ProductVariantStockTran']['quantity']);
                            }
                        }
                    }
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => array_values($arrProduct));
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $options = array(
                    'fields' => array('ProductMaster.name','CategoryMaster.name','ProductVariantTran.price','ProductVariantTran.id','ProductMaster.code','ProductMaster.hsn_code','UnitMaster.name'),
                    'joins' => array(
                        array(
                            'table' => 'category_masters',
                            'alias' => 'CategoryMaster',
                            'type' =>'INNER',
                            'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id','CategoryMaster.status' => 1,'CategoryMaster.branch_master_id' => $firmId)
                        ),
                        array(
                            'table' => 'unit_masters',
                            'alias' => 'UnitMaster',
                            'type' =>'LEFT',
                            'conditions' => array('ProductMaster.unit_master_id = UnitMaster.id','UnitMaster.status' => 1)
                        ),
                        array(
                            'table' => 'product_variant_trans',
                            'alias' => 'ProductVariantTran',
                            'type' =>'INNER',
                            'conditions' => array('ProductMaster.id = ProductVariantTran.product_master_id','ProductVariantTran.status' => 1)
                        )
                    ),
                    'conditions' => array('ProductMaster.branch_master_id' => $firmId,'ProductMaster.status' => 1,'ProductVariantTran.stock_price' => 1,'ProductMaster.type' => 1),
                    'order' => 'ProductMaster.order_no'
                );
                $arrProductData = $this->ProductMaster->find('all',$options);
                if(count($arrProductData) > 0) {
                    $arrProduct = array();
                    foreach($arrProductData as $key => $product) {
                        $id = $product['ProductVariantTran']['id'];
                        $arrProduct[$id]['product'] = $product['ProductMaster']['name'];
                        $arrProduct[$id]['id'] = $product['ProductVariantTran']['id'];
                        $arrProduct[$id]['price'] = $product['ProductVariantTran']['price'];
                        $arrProduct[$id]['code'] = $product['ProductMaster']['code'];
                        $arrProduct[$id]['hsn_code'] = $product['ProductMaster']['hsn_code'];
                        $arrProduct[$id]['category'] = $product['CategoryMaster']['name'];
                        $arrProduct[$id]['unit'] = $product['UnitMaster']['name'];
                    }
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => array_values($arrProduct));
                } else {
                    $statusCode = 200;
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'product_stock','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['stock']) && count($this->request->data['stock']) > 0) {
                    $arrProductStockData = array();
                    $count = 0;
                    $branchId = $this->request->data['branch_master_id'];
                    foreach($this->request->data['stock'] as $productId => $stock) {
                        if($stock > 0) {
                            $arrProductStockData[$count]['ProductVariantStockTran']['product_variant_tran_id'] = $productId;
                            $arrProductStockData[$count]['ProductVariantStockTran']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                            $arrProductStockData[$count]['ProductVariantStockTran']['branch_master_id'] = $branchId;
                            $arrProductStockData[$count]['ProductVariantStockTran']['tran_type'] = 2;
                            $arrProductStockData[$count]['ProductVariantStockTran']['from_type'] = 4;
                            $arrProductStockData[$count]['ProductVariantStockTran']['date'] = date('Y-m-d');
                            $arrProductStockData[$count]['ProductVariantStockTran']['quantity'] = intval($stock);
                            ++$count;
                        }
                    }
                    $this->ProductVariantStockTran->saveAll($arrProductStockData);
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('Opening stock is added successfully',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'product_stock','method' => 'save','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>