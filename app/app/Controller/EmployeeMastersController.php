<?php
App::uses('AppController','Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class EmployeeMastersController extends AppController {
    public $name = 'EmployeeMasters';
    public $layout = false;
    public $uses = array('EmployeeMaster','Employee','EmployeeTran','ErrorLog','User','UserRoleTran','RoleMaster');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : '';
                $conditions = array('Employee.status' => 1,'Employee.branch_master_id' => $firmId);
                if(isset($this->request->data['first_name']) && !empty($this->request->data['first_name'])) {
                    $conditions['Employee.first_name LIKE'] = '%'.trim($this->request->data['first_name']).'%';
                }

                if(isset($this->request->data['last_name']) && !empty($this->request->data['last_name'])) {
                    $conditions['Employee.last_name LIKE'] = '%'.trim($this->request->data['last_name']).'%';
                }

                if(isset($this->request->data['unique_no']) && !empty($this->request->data['unique_no'])) {
                    $conditions['Employee.unique_no LIKE'] = '%'.trim($this->request->data['unique_no']).'%';
                }

                if(isset($this->request->data['department_master_id']) && !empty($this->request->data['department_master_id'])) {
                    $conditions['Employee.department_master_id'] = trim($this->request->data['department_master_id']);
                }

                if(isset($this->request->data['designation_master_id']) && !empty($this->request->data['designation_master_id'])) {
                    $conditions['Employee.designation_master_id'] = trim($this->request->data['designation_master_id']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('Employee.first_name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('Employee.unique_no '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('Employee.department '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('Employee.designation '.$sortyType);
                                break;
                        default:
                                $orderBy = array('Employee.first_name '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('Employee.first_name ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->Employee->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','employee_name','designation','department','unique_no','mobile_no','username'),
                                    'conditions' => $conditions,
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->Employee->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['Employee']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['Employee']['employee_name'];
                        $records[$key]['code'] = $tableDetails['Employee']['unique_no'];
                        $records[$key]['department'] = $tableDetails['Employee']['department'];
                        $records[$key]['designation'] = $tableDetails['Employee']['designation'];
                        $records[$key]['username'] = $tableDetails['Employee']['username'];
                        $records[$key]['is_exists'] = 0;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Employee Name','code'=>'Unique Code','username' => 'UserName','department'=>'Department','designation' => 'Designation');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['EmployeeMaster'] = $this->request->data;
                #pr($arrSaveRecords);
                if(!isset($arrSaveRecords['EmployeeMaster']['branch_master_id']) || empty($arrSaveRecords['EmployeeMaster']['branch_master_id'])) {
                    $arrSaveRecords['EmployeeMaster']['branch_master_id'] = '';
                }
                if(isset($arrSaveRecords['EmployeeMaster']['id']) && !empty($arrSaveRecords['EmployeeMaster']['id'])) {
                    $arrSaveRecords['EmployeeMaster']['id'] = $this->decryption($arrSaveRecords['EmployeeMaster']['id']);
                }
    
                $this->EmployeeMaster->set($arrSaveRecords);
                if($this->EmployeeMaster->validates()) {
                    unset($this->request->data);
                    #pr($arrSaveRecords);exit;
                    if(!empty($arrSaveRecords['EmployeeMaster']['date_of_joining'])) {
                        $arrSaveRecords['EmployeeMaster']['date_of_joining'] = $this->AppUtilities->date_format($arrSaveRecords['EmployeeMaster']['date_of_joining'],'yyyy-mm-dd');
                    }
                    #pr($arrSaveRecords);exit;
                    $dataSource = $this->EmployeeMaster->getDataSource();
                    $isEmployeeTranExists = 0;
                    $fieldList = array('id','user_id','main_user_id','unique_no','max_unique_no','first_name','middle_name','last_name','qualification','designation_master_id','branch_master_id','title_master_id','gender','aadhar_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','date_of_joining','c_address','p_address','photo');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['EmployeeMaster']['id']) || empty($arrSaveRecords['EmployeeMaster']['id'])) {
                            $tableName = 'AutoEmployee'.$arrSaveRecords['EmployeeMaster']['branch_master_id'].'Code';
                            $arrSaveMemberCode[$tableName]['name'] = $arrSaveRecords['EmployeeMaster']['first_name'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrSaveMemberCode)) {
                                throw new Exception(__('Auto Employee code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrMemberCodeData = $this->AppUtilities->getUserSettingCode($arrSaveRecords['EmployeeMaster']['branch_master_id'],1,$maxNumber);
                            $userName = 'RADIZO0000'.$maxNumber;
                            if(!empty($arrMemberCodeData)) {
                                $arrSaveRecords['EmployeeMaster']['unique_no'] = $arrMemberCodeData['code'];
                                $arrSaveRecords['EmployeeMaster']['max_unique_no'] = $arrMemberCodeData['maxNumber'];
                                $replaceData = array('/' => '','-' => '','%' => '');
                                $userName = substr(strtoupper($arrSaveRecords['EmployeeMaster']['first_name']),0,4).substr($arrSaveRecords['EmployeeMaster']['mobile_no'],0,3).$arrMemberCodeData['maxNumber'].substr($arrSaveRecords['EmployeeMaster']['mobile_no'],5,3);
                            }
                            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                            $arrUserData['User']['username'] = $userName;
                            $arrUserData['User']['password'] = $passwordHasher->hash($arrSaveRecords['EmployeeMaster']['mobile_no']);
                            $arrUserData['User']['encrypt'] = $this->encrypt_algo($arrSaveRecords['EmployeeMaster']['mobile_no']);
                            $arrUserData['User']['fullname'] = $arrSaveRecords['EmployeeMaster']['first_name'].' '.$arrSaveRecords['EmployeeMaster']['middle_name'].' '.$arrSaveRecords['EmployeeMaster']['last_name'];
                            $arrUserData['User']['user_type'] = Configure::read('EMPLOYEE_ROLE_TYPE');
                            $this->User->create();
                            if(!$this->User->save($arrUserData,array('validate'=>false))) {
                                throw new Exception(__('Member user could not saved properly,Please try again later !.',true));
                            }
                            $userId = $this->User->getInsertID();
                            $arrSaveRecords['EmployeeMaster']['user_id'] = $userId;
                            if(isset($arrSaveRecords['EmployeeMaster']['role_master_id']) && !empty($arrSaveRecords['EmployeeMaster']['role_master_id'])) {
                                $arrUserRoleTran['UserRoleTran']['role_master_id'] = $arrSaveRecords['EmployeeMaster']['role_master_id'];
                                $arrUserRoleTran['UserRoleTran']['user_id'] = $userId;
                                $this->UserRoleTran->create();
                                if(!$this->UserRoleTran->save($arrUserRoleTran)) {
                                    throw new Exception(__('User role tran could not saved properly,Please try again later !.',true));
                                }
                            }
                            $this->EmployeeMaster->create();
                        } else {
                            if(empty($arrSaveRecords['EmployeeMaster']['photo'])) {
                                $arrSaveRecords['EmployeeMaster']['photo'] = $arrSaveRecords['EmployeeMaster']['old_photo'];
                                unset($arrSaveRecords['EmployeeMaster']['old_photo']);
                            }
                            $options = array('fields' => array('id'),'conditions' =>array('status' => 1,'employee_master_id' => $arrSaveRecords['EmployeeMaster']['id'],'designation_master_id' => $arrSaveRecords['EmployeeMaster']['designation_master_id']));
                            $arrEmployeeTran = $this->EmployeeTran->find('first',$options);
                            if(empty($arrEmployeeTran)) {
                                $updateFields['EmployeeTran.status'] = 0;
                                $updateFields['EmployeeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['EmployeeTran.employee_master_id'] = $arrSaveRecords['EmployeeMaster']['id'];
                                $updateParams['EmployeeTran.status'] = 1;
                                $this->EmployeeTran->updateAll($updateFields,$updateParams);
                                unset($updateParams,$updateFields);
                            } else {
                                $isEmployeeTranExists = 1;
                            }
                        }
                        $arrSaveRecords['EmployeeMaster']['main_user_id'] = $this->Session->read('sessUserId');
                        if(!$this->EmployeeMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        if($isEmployeeTranExists == 0 && !empty($arrSaveRecords['EmployeeMaster']['designation_master_id'])) {
                            $arrEmployeeTran['EmployeeTran']['designation_master_id'] = $arrSaveRecords['EmployeeMaster']['designation_master_id'];
                            $arrEmployeeTran['EmployeeTran']['branch_master_id'] = $arrSaveRecords['EmployeeMaster']['branch_master_id'];
                            if(isset($arrSaveRecords['EmployeeMaster']['id']) && !empty($arrSaveRecords['EmployeeMaster']['id'])) {
                                $arrEmployeeTran['EmployeeTran']['employee_master_id'] = $arrSaveRecords['EmployeeMaster']['id'];
                            } else {
                                $arrEmployeeTran['EmployeeTran']['employee_master_id'] = $this->EmployeeMaster->getInsertID();
                            }
                            $this->EmployeeTran->create();
                            $this->EmployeeTran->save($arrEmployeeTran);
                        }
    
                        $dataSource->commit();
                        if(isset($arrSaveRecords['EmployeeMaster']['photo']) && !empty($arrSaveRecords['EmployeeMaster']['photo'])) {
                            $tmpPath = Configure::read('UPLOAD_TMP_DIR').'/'.$arrSaveRecords['EmployeeMaster']['photo'];
                            $saveImagePath = Configure::read('UPLOAD_EMPLOYEE_IMAGE_DIR').'/'.$arrSaveRecords['EmployeeMaster']['photo'];
                            if(file_exists($tmpPath) && copy($tmpPath,$saveImagePath)) {
                                @unlink($tmpPath);
                            }
                            if(isset($arrSaveRecords['EmployeeMaster']['old_photo']) && !empty($arrSaveRecords['EmployeeMaster']['old_photo'])) {
                                $files =  Configure::read('UPLOAD_EMPLOYEE_IMAGE_DIR').'/'.$arrSaveRecords['EmployeeMaster']['old_photo'];
                                if(file_exists($files)) {
                                    @unlink($files);
                                }
                            }
                        }
                        
                        $statusCode = 200;
                        if(isset($arrSaveRecords['EmployeeMaster']['id']) && !empty($arrSaveRecords['EmployeeMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true),'is_send_mail' => 0);
                        } else {
                            if(!empty($arrSaveRecords['EmployeeMaster']['email_id'])) {
                                $response = array('status' => 1,'message' => __('SAVED_RECORD',true),'is_send_mail' => 1,'id' => $this->EmployeeMaster->getInsertID());
                            } else {
                                $response = array('status' => 1,'message' => __('SAVED_RECORD',true),'is_send_mail' => 0);
                            }
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->EmployeeMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$firmId = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('id','unique_no','max_unique_no','employee_name','first_name','middle_name','last_name','qualification','designation_master_id','branch_master_id','title_master_id','gender','aadhar_number','pan_number','mobile_no','alternate_mobile_no','email_id','alternate_email_id','country_master_id','state_master_id','city_master_id','pincode','date_of_joining','c_address','p_address','photo','department','designation','country','state','city','designation_master_id','department_master_id'),
                            'conditions' => array('Employee.status' => 1,'Employee.id' => $id,'Employee.branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->Employee->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['Employee']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete($firmId = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && !empty($firmId) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->EmployeeMaster->getDataSource();
                    try{
                        $dataSource->begin();
                        $options = array('fields' => array('id','user_id'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                        $arrUserData = $this->EmployeeMaster->find('list',$options);
                        if(!empty($arrUserData)) {
                            $arrUserId = array_values($arrUserData);
                            $updateFields = $updateParams = array();
                            $updateFields['User.status'] = 0;
                            $updateFields['User.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['User.id'] = $arrUserId;
                            if(!$this->User->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $updateFields = $updateParams = array();
                            $updateFields['UserRoleTran.status'] = 0;
                            $updateFields['UserRoleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['UserRoleTran.user_id'] = $arrUserId;
                            if(!$this->UserRoleTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }
                        $updateFields = $updateParams = array();
                        $updateFields['EmployeeMaster.status'] = 0;
                        $updateFields['EmployeeMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['EmployeeMaster.id'] = $this->request->data['id'];
                        if(!$this->EmployeeMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        unset($updateFields,$updateParams);
                        $updateFields = $updateParams = array();
                        $updateFields['EmployeeTran.status'] = 0;
                        $updateFields['EmployeeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['EmployeeTran.employee_master_id'] = $this->request->data['id'];
                        if(!$this->EmployeeTran->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
