<?php
App::uses('AppController','Controller');
class ServiceMastersController extends AppController {
    public $name = 'ServiceMasters';
    public $layout = false;
    public $uses = array('ServiceMaster','VehicleJobTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('ServiceMaster.status' => 1,'ServiceMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['ServiceMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['ServiceMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                if(isset($this->request->data['service_group_master_id']) && !empty($this->request->data['service_group_master_id'])) {
                    $conditions['ServiceMaster.service_group_master_id'] = trim($this->request->data['service_group_master_id']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('ServiceMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('ServiceMaster.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('ServiceGroupMaster.name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ServiceMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('ServiceMaster.order_no ASC');
                }

                $joins  =   array(
                                array(
                                    'table' => 'service_group_masters',
                                    'alias' => 'ServiceGroupMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('ServiceGroupMaster.id = ServiceMaster.service_group_master_id','ServiceMaster.status' => 1)
                                )
                            );
                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => $joins,
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->ServiceMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $joins[] = array(
                                'table' => 'vehicle_job_trans',
                                'alias' => 'VJT',
                                'type' => 'LEFT',
                                'conditions' => array('ServiceMaster.id = VJT.reference_tran_id','VJT.status' => 1,'VJT.type' => 2)
                            );
                $tableOptions = array(
                                    'fields' => array('id','name','code','price','order_no','servicegroup_name','COUNT(VJT.id) AS count'),
                                    'joins' => $joins,
                                    'conditions' => $conditions,
                                    'group' => array('ServiceMaster.id'),
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->ServiceMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ServiceMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['ServiceMaster']['name'];
                        $records[$key]['code'] = $tableDetails['ServiceMaster']['code'];
                        $records[$key]['price'] = $tableDetails['ServiceMaster']['price'];
                        $records[$key]['service'] = $tableDetails['ServiceMaster']['servicegroup_name'];
                        $records[$key]['order_no'] = $tableDetails['ServiceMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['ServiceMaster']['order_no'] > $maxOrderNo) ? $tableDetails['ServiceMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','service' => 'Service Group','price' => 'Price','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['ServiceMaster'] = $this->request->data;
                //pr($arrSaveRecords['ServiceMaster']);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['ServiceMaster']['id']) && !empty($arrSaveRecords['ServiceMaster']['id'])) {
                    $arrSaveRecords['ServiceMaster']['id'] = $this->decryption($arrSaveRecords['ServiceMaster']['id']);
                }
                $arrSaveRecords['ServiceMaster']['branch_master_id'] = isset($arrSaveRecords['ServiceMaster']['branch_master_id']) ? $arrSaveRecords['ServiceMaster']['branch_master_id'] : '';
                $this->ServiceMaster->set($arrSaveRecords);
                if($this->ServiceMaster->validates()) {
                    $dataSource = $this->ServiceMaster->getDataSource();
                    $fieldList = array('id','name','code','price','branch_master_id','service_group_master_id','description','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['ServiceMaster']['id']) || empty($arrSaveRecords['ServiceMaster']['id'])) {
                            $this->ServiceMaster->create();
                        }
                        if(!$this->ServiceMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['ServiceMaster']['id']) && !empty($arrSaveRecords['ServiceMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->ServiceMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','price','description','service_group_master_id','servicegroup_name','order_no'),
                            'joins' => array(
                                array(
                                    'table' => 'service_group_masters',
                                    'alias' => 'ServiceGroupMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('ServiceGroupMaster.id = ServiceMaster.service_group_master_id','ServiceMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('ServiceMaster.status' => 1,'ServiceMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->ServiceMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['ServiceMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('reference_tran_id'),'conditions' => array('status'=> 1,'type' => 2,'reference_tran_id' => $this->request->data['id']));
                    $arrRecords = $this->VehicleJobTran->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->ServiceMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['ServiceMaster.status'] = 0;
                            $updateFields['ServiceMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ServiceMaster.id'] = $arrDeleteRecords;
                            if(!$this->ServiceMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
