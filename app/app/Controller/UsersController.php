<?php

App::uses('AppController','Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class UsersController extends AppController {
    public $name = 'Users';
    public $components = array('UserManagement','AppUtilities','Session','RequestHandler');
    public $uses = array('User','ErrorLog');
    public $layout = false;

    public function beforeFilter() {
        parent::beforeFilter();
    }
    public function index() {
        echo md5('123456');exit;
        $statusCode = 400;
        $response = array('status' => -1,'message' => 'Invalid method authentication');
        $this->bodyResponse($response,$statusCode);
    }

    public function login() {
        $response = array('status' => 0 ,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $userRequest['User'] = $this->request->data;
                unset($this->request->data);
                $this->User->set($userRequest);
                if($this->User->validates()) {
                    
                    $options = array(
                                    'fields' => array('id','username','password','fullname','user_type','UserRoleTran.role_master_id'),
                                    'joins' => array(
                                        array(
                                            'table' => 'user_role_trans',
                                            'alias' => 'UserRoleTran',
                                            'type' =>'LEFT',
                                            'conditions' => array('User.id = UserRoleTran.user_id','UserRoleTran.status' =>1)
                                        )
                                    ),
                                    'conditions' => array('User.status' => 1,'User.is_active' => 1 ,'User.username' => $userRequest['User']['username'])
                                );
                    $arrUserDetail = $this->User->find('first',$options);
                    if(count($arrUserDetail) > 0) {
                        $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                        $userPassword = $passwordHasher->hash($userRequest['User']['password']);
                        $password = $arrUserDetail['User']['password'];
                        if($userPassword == $password) {
                            if(!empty($arrUserDetail['UserRoleTran']['role_master_id'])) {
                                $arrUserTokenData = $this->UserManagement->saveUserLogs($arrUserDetail['User']['id'],$userRequest['User']['from_type']);
                                if(!empty($arrUserTokenData)) {
                                    $statusCode = 200;
                                    $arrUserDetail['User']['token_id'] = $arrUserTokenData['token_id'];
                                    $arrUserDetail['User']['sessid'] = $arrUserTokenData['sessid'];
                                    $memberId = $memberFirmName = '';
                                    $arrUserDetail['User']['role_id'] = $arrUserDetail['UserRoleTran']['role_master_id'];
                                    $arrUserDetail['User']['firm_id'] = '';
                                    $arrUserDetail['User']['firm_name'] = '';
                                    $arrUserDetail['User']['user_type'] = (int) $arrUserDetail['User']['user_type'];
                                    $response = array('status' => 1, 'message' => __('LOGIN_SUCCESS',true),'user_detail' => $arrUserDetail['User']);
                                } else {
                                    $response = array('status' => 0, 'message' => __('User logged could not saved,please try again later !..',true));
                                }
                            } else {
                                $response = array('status' => 0,'message' => __('UNAUTHORIZED_ACCESS',true));
                            }
                        } else {
                            $response = array('status' => 0,'message' => __('INVALID_PASSWORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('INVALID_USERNAME',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->User->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>'','controller' => 'users','method' => 'login','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function logout() {
        $response = array('status' => 0 ,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        if($this->request->is('post')) {
            if(!empty($this->request->data['user_id']) && !empty($this->request->data['sessid'])) {
                $userId = $this->request->data['user_id'];
                $sessId = $this->request->data['sessid'];
                $tokenId = $this->request->data['token_id'];
                $this->UserManagement->logoutUserLogs($userId,$sessId);
                $statusCode = 200;
                Cache::delete('userLinkRights'.$userId,'long');
                $response = array('status' => 1, 'message' => __('LOGOUT_SUCCESS',true));
            } else {
                $response = array('status' => 0,'message' =>__('INVALID_PARAMS',true));
            }
        } else {
            $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function change_password() {
        $response = array('status' => 0 ,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        if($this->request->is('post')) {
            if(isset($this->request->data['password']) && !empty($this->request->data['password'])) {
                $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                $this->request->data['password'] = $passwordHasher->hash($this->request->data['password']);
                $dataSource = $this->User->getDataSource();
                $arrSaveRecords['User']['password'] = $this->request->data['password'];
                $arrSaveRecords['User']['id'] = $this->Session->read('sessUserId');
                try{
                    $dataSource->begin();
                    if(!$this->User->save($arrSaveRecords,array('validate' => false))) {
                        throw new Exception(__('Record could not saved properly, please try again later!',true));
                    }
                    $dataSource->commit();
                    $response = array('status' => 1,'message' => __('Password changed successfully !.',true));
                } catch(Exception $e) {
                    $dataSource->rollback();
                    $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'users','method' => 'change_password','request' => $this->request->data,'description' => $e);
                    $this->ErrorLog->saveErrorLog($arrErrorLogs);
                    $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
            }
        } else {
            $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>