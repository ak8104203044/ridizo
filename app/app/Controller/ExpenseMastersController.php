<?php
App::uses('AppController','Controller');
class ExpenseMastersController extends AppController {
    public $name = 'ExpenseMasters';
    public $layout = false;
    public $uses = array('ExpenseMaster','ExpenseTran','ExpenseVehicleTran','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('ExpenseMaster.status' => 1,'ExpenseMaster.branch_master_id' => $firmId,'ExpenseMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['type']) && !empty($this->request->data['type'])) {
                    $conditions['ExpenseMaster.expense_type'] = trim($this->request->data['type']);
                }

                if(isset($this->request->data['voucher_no']) && !empty($this->request->data['voucher_no'])) {
                    $conditions['ExpenseMaster.voucher_no LIKE'] = '%'.trim($this->request->data['voucher_no']).'%';
                }

                if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                    $conditions['ExpenseMaster.voucher_date >='] = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['ExpenseMaster.voucher_date <='] = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                }

                $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                $totalRecords = $this->ExpenseMaster->find('count',$tableCountOptions);
                $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                $start = ($page - 1) * $length;
                $end = ($start + $length);
                $end = ($end > $totalRecords) ? $totalRecords : $end;
                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('ExpenseMaster.voucher_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('ExpenseMaster.voucher_date '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('ExpenseMaster.expense_type '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ExpenseMaster.voucher_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('ExpenseMaster.voucher_no ASC');
                }

                $tableOptions = array(
                    'fields' => array('id','voucher_no','voucher_date','expense_type','amount','VM.unique_no'),
                    'joins' => array(
                        array(
                            'table' => 'vehicle_masters',
                            'alias' => 'VM',
                            'type' => 'LEFT',
                            'conditions' => array('ExpenseMaster.vehicle_master_id = VM.id','VM.status' => 1)
                        )
                    ),
                    'conditions' => $conditions,
                    'group' => array('ExpenseMaster.id'),
                    'limit' => $length,
                    'offset' => $start,
                    'order' => $orderBy,
                    'recursive' => -1
                );
                $arrTableData = $this->ExpenseMaster->find('all',$tableOptions);
                if($totalRecords > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ExpenseMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['voucher'] = $tableDetails['ExpenseMaster']['voucher_no'];
                        $records[$key]['vehicle'] = $tableDetails['VM']['unique_no'];
                        $records[$key]['date'] = $this->AppUtilities->date_format($tableDetails['ExpenseMaster']['voucher_date'],'dd-mm-yyyy');
                        $records[$key]['amount'] = $tableDetails['ExpenseMaster']['amount'];
                        $records[$key]['type'] = ($tableDetails['ExpenseMaster']['expense_type'] == 1) ? 'Other' : 'Vehicle';
                        $records[$key]['is_exists'] = 0;
                    }
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    
    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                }
                
                $this->request->data['voucher_no'] = isset($this->request->data['voucher_no']) ? $this->request->data['voucher_no'] : '';
                $this->request->data['voucher_date'] = isset($this->request->data['voucher_date']) ? $this->request->data['voucher_date'] : '';
                $this->request->data['branch_master_id'] = isset($this->request->data['branch_master_id']) ? $this->request->data['branch_master_id'] : '';
                $this->request->data['company_year_master_id'] = isset($this->request->data['company_year_master_id']) ? $this->request->data['company_year_master_id'] : '';
                $this->request->data['vehicle_master_id'] = isset($this->request->data['vehicle_master_id']) ? $this->request->data['vehicle_master_id'] : '';
                $this->request->data['amount'] = isset($this->request->data['amount']) ? $this->request->data['amount'] : '';
                $arrValidateData['ExpenseMaster'] = $this->request->data;
                $this->ExpenseMaster->set($arrValidateData);
                if($this->ExpenseMaster->validates()) {
                    unset($arrValidateData);
                    $dataSource = $this->ExpenseMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $arrSaveExpenseData['ExpenseMaster']['voucher_no'] = $this->request->data['voucher_no'];
                        $arrSaveExpenseData['ExpenseMaster']['voucher_date'] = $this->AppUtilities->date_format($this->request->data['voucher_date'],'yyyy-mm-dd');
                        $arrSaveExpenseData['ExpenseMaster']['description'] = (isset($this->request->data['description'])) ? $this->request->data['description'] : '';
                        $arrSaveExpenseData['ExpenseMaster']['branch_master_id'] = $this->request->data['branch_master_id'];
                        $arrSaveExpenseData['ExpenseMaster']['company_year_master_id'] = $this->request->data['company_year_master_id'];
                        if(!empty($this->request->data['vehicle_master_id'])) {
                            $arrSaveExpenseData['ExpenseMaster']['expense_type'] = 2;
                            $arrSaveExpenseData['ExpenseMaster']['vehicle_master_id'] = $this->request->data['vehicle_master_id'];
                        } else {
                            $arrSaveExpenseData['ExpenseMaster']['expense_type'] = 1;
                        }
                        
                        if(isset($this->request->data['expense_detail']) && !empty($this->request->data['expense_detail'])) {
                            $arrSaveExpenseData['ExpenseMaster']['expense_json'] = json_encode($this->request->data['expense_detail']);
                        }

                        $arrSaveExpenseData['ExpenseMaster']['amount'] = $this->request->data['amount'];
                        $arrDeleteExpenseHeadData = array();
                        if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                            $arrExpenseHeadDetail = array();
                            if(isset($this->request->data['expense_detail']) && !empty($this->request->data['expense_detail'])) {
                                $arrExpenseHeadDetail = array_column($this->request->data['expense_detail'],'id');
                            }
                            $arrSaveExpenseData['ExpenseMaster']['id'] = $this->request->data['id'];
                            $options = array(
                                'fields' => array('expense_head_master_id','id'),
                                'conditions' => array('ExpenseTran.expense_master_id' => $arrSaveExpenseData['ExpenseMaster']['id'],'ExpenseTran.branch_master_id'=> $this->request->data['branch_master_id'],'ExpenseTran.status' => 1)
                            );
                            $arrExpenseHeadData = $this->ExpenseTran->find('list',$options);
                            if(!empty($arrExpenseHeadData) && !empty($arrExpenseHeadDetail)) {
                                $arrDeleteExpenseHeadData = array_diff(array_keys($arrExpenseHeadData),$arrExpenseHeadDetail);
                            }
                        } else {
                            $this->ExpenseMaster->create();
                        }
                        #pr($arrSaveExpenseData);exit;
                        $fieldList = array('id','voucher_no','voucher_date','amount','expense_json','vehicle_master_id','expense_type','branch_master_id','company_year_master_id','description');
                        $this->ExpenseMaster->save($arrSaveExpenseData,array('validate' => false,'fieldList' => $fieldList));
                        if(!isset($this->request->data['id'])) {
                            $arrSaveExpenseData['ExpenseMaster']['id'] = $this->ExpenseMaster->getInsertID();
                        }

                        #pr($arrDeleteExpenseHeadData);
                        if(isset($this->request->data['expense_detail']) && count($this->request->data['expense_detail']) > 0) {
                            $arrExpenseTranData = array();
                            $count = 0;
                            foreach($this->request->data['expense_detail'] as $key => $expenseHead) {
                                if(isset($arrExpenseHeadData[$expenseHead['id']])) {
                                    $arrExpenseTranData[$count]['ExpenseTran']['id'] = $arrExpenseHeadData[$expenseHead['id']];
                                }
                                $arrExpenseTranData[$count]['ExpenseTran']['expense_master_id'] = $arrSaveExpenseData['ExpenseMaster']['id'];
                                $arrExpenseTranData[$count]['ExpenseTran']['expense_head_master_id'] = $expenseHead['id'];
                                $arrExpenseTranData[$count]['ExpenseTran']['description'] = (isset($expenseHead['description'])) ? $expenseHead['description'] : '';
                                $arrExpenseTranData[$count]['ExpenseTran']['price'] = number_format($expenseHead['amount'],2,'.','');
                                $arrExpenseTranData[$count]['ExpenseTran']['total'] = number_format($expenseHead['amount'],2,'.','');
                                $arrExpenseTranData[$count]['ExpenseTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                ++$count;
                            }
                            #pr($arrExpenseTranData);exit;
                            $fieldList = array('id','expense_master_id','expense_head_master_id','description','price','total','branch_master_id');
                            $this->ExpenseTran->saveAll($arrExpenseTranData,array('fieldList' => $fieldList));
                            unset($arrExpenseTranData);
                        }

                        if(count($arrDeleteExpenseHeadData) > 0) {
                            $updateFields['ExpenseTran.status'] = 0;
                            $updateFields['ExpenseTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['ExpenseTran.expense_head_master_id'] = $arrDeleteExpenseHeadData;
                            $updateParams['ExpenseTran.branch_master_id'] = $this->request->data['branch_master_id'];
                            if(!$this->ExpenseTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                        }

                        if(isset($this->request->data['invoice_id']) && !empty($this->request->data['invoice_id'])) {
                            $arrInvoiceData = explode(',',$this->request->data['invoice_id']);
                            $arrExpenseVehicleTranData = array();
                            $count = 0;
                            foreach($arrInvoiceData as $key => $invoiceId) {
                                $arrExpenseVehicleTranData[$count]['ExpenseVehicleTran']['expense_master_id'] = $arrSaveExpenseData['ExpenseMaster']['id'];
                                $arrExpenseVehicleTranData[$count]['ExpenseVehicleTran']['invoice_master_id'] = $invoiceId;
                                $arrExpenseVehicleTranData[$count]['ExpenseVehicleTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                                ++$count;
                            }
                            $this->ExpenseVehicleTran->saveAll($arrExpenseVehicleTranData);
                            unset($arrExpenseVehicleTranData);
                        }

                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($this->request->data['id']) && !empty($this->request->data['id'])) {
                            $response = array('status' => 1,'message' => __('updated_record',true));
                        } else {
                            $response = array('status' => 1,'message' => __('saved_record',true));
                        }
                        unset($arrSaveExpenseData);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'expense_masters','method' => 'save','request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->ExpenseMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$firmId = 0,$type = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($firmId)) {
                    $id = $this->decryption($id);
                    $type = (int)$type;
                    $options = array(
                        'fields' => array('id','voucher_no','voucher_date','amount','expense_json','vehicle_master_id','expense_type','description','VM.unique_no','VM.vehicle_number'),
                        'joins' => array(
                            array(
                                'table' => 'vehicle_masters',
                                'alias' => 'VM',
                                'type' => 'LEFT',
                                'conditions' => array('ExpenseMaster.vehicle_master_id = VM.id','VM.status' => 1)
                            )
                        ),
                        'conditions' => array('ExpenseMaster.status' => 1,'ExpenseMaster.id' => $id,'ExpenseMaster.branch_master_id' => $firmId),
                        'recursive' => -1
                    );
                    $arrRecords = $this->ExpenseMaster->find('first',$options);
                    if(!empty($arrRecords)) {
                        $statusCode = 200;
                        $arrRecords['ExpenseMaster']['id'] = $this->encryption($arrRecords['ExpenseMaster']['id']);
                        $arrRecords['ExpenseMaster']['inwords'] = $this->AppUtilities->getNumberFormat($arrRecords['ExpenseMaster']['amount']);
                        $arrRecords['ExpenseMaster']['voucher_date'] = $this->AppUtilities->date_format($arrRecords['ExpenseMaster']['voucher_date'],'dd-mm-yyyy');
                        $arrRecords['ExpenseMaster']['vehicle_number'] = $arrRecords['VM']['vehicle_number'];
                        $arrRecords['ExpenseMaster']['vehicle_code'] = $arrRecords['VM']['unique_no'];
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['ExpenseMaster']);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->ExpenseMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['ExpenseMaster.status'] = -1;
                        $updateFields['ExpenseMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ExpenseMaster.id'] = $this->request->data['id'];
                        $this->ExpenseMaster->updateAll($updateFields,$updateParams);

                        unset($updateFields,$updateParams);
                        $updateFields['ExpenseTran.status'] = 0;
                        $updateFields['ExpenseTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ExpenseTran.expense_master_id'] = $this->request->data['id'];
                        $this->ExpenseTran->updateAll($updateFields,$updateParams);

                        unset($updateFields,$updateParams);

                        $updateFields['ExpenseVehicleTran.status'] = 0;
                        $updateFields['ExpenseVehicleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ExpenseVehicleTran.expense_master_id'] = $this->request->data['id'];
                        $this->ExpenseVehicleTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('deleted_record',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export($exporType = 1,$firmId = 0,$companyYearId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get') && !empty($firmId) && !empty($companyYearId)) {
                $exportOptions = array(
                                    'fields' => array('id','voucher_no','voucher_date','amount','VM.unique_no'),
                                    'joins' => array(
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'INNER',
                                            'conditions' => array('ExpenseMaster.vehicle_master_id = VM.id','VM.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('ExpenseMaster.status' => 1,'ExpenseMaster.branch_master_id' => $firmId,'ExpenseMaster.company_year_master_id' => $companyYearId),
                                    'order' => 'ExpenseMaster.voucher_no ASC',
                                    'recursive' => -1
                                );
                $arrExportData = $this->ExpenseMaster->find('all',$exportOptions);
                if(count($arrExportData) > 0) {
                    $statusCode = 200;
                    $counter = 0;
                    $arrTableColumnValues = array();
                    foreach($arrExportData as $key => $exportDetails) {
                        ++$counter;
                        $arrTableColumnValues[$key]['sno'] = $counter.'.';
                        $arrTableColumnValues[$key]['voucher_no'] = $exportDetails['ExpenseMaster']['voucher_no'];
                        $arrTableColumnValues[$key]['date'] = $this->AppUtilities->date_format($exportDetails['ExpenseMaster']['voucher_date'],'dd-mm-yyyy');
                        $arrTableColumnValues[$key]['amount'] = $exportDetails['ExpenseMaster']['amount'];
                        $arrTableColumnValues[$key]['vehicle'] = $exportDetails['VM']['unique_no'];
                    }
                    $headers = array('sno'=>'S.No','voucher_no'=>'Voucher No','date'=>'Date','amount'=>'Amount','vehicle' => 'Vehicle');
                    $response = array('status' => 1,'date' => date('Y-m-d-H:i:s'),'message' => __('RECORD_FETCHED',true),'data' => $arrTableColumnValues,'header' => $headers);
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('export_type' => $exporType,'firm_id' => $firmId,'company_year_id' => $companyYearId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
