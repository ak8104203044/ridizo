<?php
App::uses('AppController','Controller');
class CityMastersController extends AppController {
    public $name = 'CityMasters';
    public $layout = false;
    public $uses = array('CityMaster','BranchMaster','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('CityMaster.status' => 1);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['CityMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['CityMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                if(isset($this->request->data['state_master_id']) && !empty($this->request->data['state_master_id'])) {
                    $conditions['CityMaster.state_master_id'] = trim($this->request->data['state_master_id']);
                }
                
                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('CityMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('CityMaster.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('StateMaster.name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('IFNULL(CityMaster.order_no,999) '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('IFNULL(CityMaster.order_no,999) ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array(
                                            'fields' => array('id'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'state_masters',
                                                    'alias' => 'StateMaster',
                                                    'type' => 'INNER',
                                                    'conditions' => array('CityMaster.state_master_id = StateMaster.id','StateMaster.status' => 1)
                                                )
                                            ),
                                            'conditions' => $conditions,
                                            'recursive' => -1
                                        );
                    $totalRecords = $this->CityMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','StateMaster.name','IF(COUNT(UMM.id) > 0 || COUNT(EM.id) > 0,1,0) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'state_masters',
                                            'alias' => 'StateMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('CityMaster.state_master_id = StateMaster.id','StateMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'branch_masters',
                                            'alias' => 'UMM',
                                            'type' => 'LEFT',
                                            'conditions' => array('CityMaster.id = UMM.city_master_id','UMM.status' => 1)
                                        ),
                                        array(
                                            'table' => 'employee_masters',
                                            'alias' => 'EM',
                                            'type' => 'LEFT',
                                            'conditions' => array('CityMaster.id = EM.city_master_id','EM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('CityMaster.id'),
                                    'order' => $orderBy
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->CityMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['CityMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['CityMaster']['name'];
                        $records[$key]['code'] = $tableDetails['CityMaster']['code'];
                        $records[$key]['state'] = $tableDetails['StateMaster']['name'];
                        $records[$key]['order_no'] = $tableDetails['CityMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['CityMaster']['order_no'] > $maxOrderNo) ? $tableDetails['CityMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','state'=>'State','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['CityMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['CityMaster']['id']) && !empty($arrSaveRecords['CityMaster']['id'])) {
                    $arrSaveRecords['CityMaster']['id'] = $this->decryption($arrSaveRecords['CityMaster']['id']);
                }
                $this->CityMaster->set($arrSaveRecords);
                if($this->CityMaster->validates()) {
                    $dataSource = $this->CityMaster->getDataSource();
                    $fieldList = array('id','name','code','state_master_id','country_master_id','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['CityMaster']['id']) || empty($arrSaveRecords['CityMaster']['id'])) {
                            $this->CityMaster->create();
                        }
                        $this->CityMaster->save($arrSaveRecords,array('fieldList' => $fieldList));
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['CityMaster']['id']) && !empty($arrSaveRecords['CityMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->CityMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','order_no','state','state_master_id'),
                            'joins' => array(
                                array(
                                    'table' => 'state_masters',
                                    'alias' => 'StateMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('CityMaster.state_master_id = StateMaster.id','StateMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('CityMaster.status' => 1,'CityMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->CityMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['CityMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('city_master_id'),'conditions' => array('status'=> 1,'city_master_id' => $this->request->data['id']));
                    $arrRecords = $this->BranchMaster->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->CityMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $updateFields['CityMaster.status'] = 0;
                            $updateFields['CityMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['CityMaster.id'] = $arrDeleteRecords;
                            $this->CityMaster->updateAll($updateFields,$updateParams);
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
