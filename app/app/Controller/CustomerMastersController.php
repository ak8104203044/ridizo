<?php
App::uses('AppController','Controller');
class CustomerMastersController extends AppController {
    public $name = 'CustomerMasters';
    public $layout = false;
    public $uses = array('CustomerMaster','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($userId = null,$module = null) {
        try{
            $userRights = array();
            if(!empty($userId)) {
                $userRights = Cache::read('userLinkRights'.$userId,'long');
            }
            $this->autoRender = false;
            $this->response->type('json');
            $response = array('status' => 0,'message' => 'invalid');
            $statusCode = 400;
            if($this->request->is('post')) {
                $conditions = array('CustomerMaster.status' => 1);
                $arrSearchFields = array(1 =>'CustomerMaster.name',2 => 'CustomerMaster.code');
                if(isset($this->request->data['searchFields']) && isset($arrSearchFields[$this->request->data['searchFields']]) && isset($this->request->data['searchText']) && !empty($this->request->data['searchFields']) && !empty($this->request->data['searchText'])){
                    $conditions[$arrSearchFields[$this->request->data['searchFields']]. ' LIKE'] = '%'.trim($this->request->data['searchText']).'%';
                }
                $tableCountOptions = array(
                                        'fields' => array('id'),
                                        'conditions' => $conditions,
                                        'recursive' => -1
                                    );
                $totalRecords = $this->CustomerMaster->find('count',$tableCountOptions);
                $displayLength = isset($this->request->data['length'])?intval($this->request->data['length']):0;
                $displayLength = ($displayLength < 0) ? $totalRecords : $displayLength; 
                $displayStart = isset($this->request->data['start'])?intval($this->request->data['start']):0;
                $draw = isset($this->request->data['draw'])?intval($this->request->data['draw']):'';
                $end = $displayStart + $displayLength;
                $end = $end > $totalRecords ? $totalRecords : $end;
                $arrSortColumn = array(2 => 'CustomerMaster.name',3 => 'CustomerMaster.code');
                if(isset($this->request->data['order'][0]['column']) && isset($arrSortColumn[$this->request->data['order'][0]['column']])) {
                    $orderCondition = $arrSortColumn[$this->request->data['order'][0]['column']].' '.strtoupper($this->request->data['order'][0]['dir']);
                } else {
                    $orderCondition = "IFNULL(CustomerMaster.order_no,9999) ASC";
                }
                $this->AppUtilities = (new View($this))->loadHelper('AppUtilities');
                $tableOptions = array(
                                    'fields' => array('id','name','code','COUNT(StateMaster.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'state_masters',
                                            'alias' => 'StateMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('CustomerMaster.id = StateMaster.country_master_id','StateMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('CustomerMaster.id'),
                                    'limit' => $displayLength,
                                    'offset' => $displayStart,
                                    'order' => $orderCondition,
                                    'recursive' => -1
                                );
                $arrTableData = $this->CustomerMaster->find('all',$tableOptions);
                $isEditRights = $isViewRights = false;
                if(!empty($module)) {
                    if(isset($userRights[$module]['edit']) && !empty($userRights[$module]['edit'])) {
                        $isEditRights = true;
                    }
                    if(isset($userRights[$module]['view']) && !empty($userRights[$module]['view'])) {
                        $isViewRights = true;
                    }
                }
                $records = array('draw' => $draw,'recordsTotal' => $totalRecords,'recordsFiltered' => $totalRecords,'data' => array());
                if($totalRecords > 0) {
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['CustomerMaster']['id']);
                        $action = '';
                        if($isEditRights === true && $tableDetails[0]['count'] <= 0) {
                            $action = $this->AppUtilities->roleActionType('edit','Edit',$encryption,'Edit Record')." ";
                        }
                        if($isViewRights === true) {
                            $action.= $this->AppUtilities->roleActionType('view','View',$encryption,'View Record');
                        }
                        $checkbox = '<input type="checkbox" class="selectAllCheckData" name="id[]" value="'.$encryption.'">';
                        if($tableDetails[0]['count'] > 0) {
                            $checkbox = '&nbsp;&nbsp;--';
                        }
                        $records["data"][$key] =array(
                                                    $checkbox,
                                                    ++$displayStart,
                                                    $tableDetails['CustomerMaster']['name'],
                                                    $tableDetails['CustomerMaster']['code'],
                                                    $action,
                                                );
                    }
                    $records["draw"] = $draw;
                    $records["recordsTotal"] = $totalRecords;
                    $records["recordsFiltered"] = $totalRecords;	
                    $response = $records;
                } else {
                    $response = $records;
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'customer_masters','method' => 'index','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $json = json_encode($response);
        $this->response->body($json);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['CustomerMaster'] = $this->request->data;
                #pr($arrSaveRecords);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['CustomerMaster']['id']) && !empty($arrSaveRecords['CustomerMaster']['id'])) {
                    $arrSaveRecords['CustomerMaster']['id'] = $this->decryption($arrSaveRecords['CustomerMaster']['id']);
                }
                $this->CustomerMaster->set($arrSaveRecords);
                if($this->CustomerMaster->validates()) {
                    $dataSource = $this->CustomerMaster->getDataSource();
                    $fieldList = array('id','title_master_id','first_name','middle_name','last_name','mobile_no','email_id','adhaar_no','pancard_no','residential_address','permanent_address','branch_master_id');
                    try {
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['CustomerMaster']['id']) || empty($arrSaveRecords['CustomerMaster']['id'])) {
                            $this->CustomerMaster->create();
                        }
                        if(!$this->CustomerMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $maxCustomerId = $this->CustomerMaster->getInsertID();
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['CustomerMaster']['id']) && !empty($arrSaveRecords['CustomerMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true),'id' => $maxCustomerId);
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->CustomerMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','order_no'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->CustomerMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('record_fetched',true),'data' => $arrRecords['CustomerMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('no_record',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'customer_masters','method' => 'record','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $dataSource = $this->CustomerMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['CustomerMaster.status'] = 0;
                        $updateFields['CustomerMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['CustomerMaster.id'] = $this->request->data['id'];
                        if(!$this->CustomerMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('deleted_record',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'customer_masters','method' => 'delete','description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'customer_masters','method' => 'delete','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export($exporType = 1) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                $exportOptions = array(
                                    'fields' => array('id','name','code','order_no'),
                                    'conditions' => array('CustomerMaster.status' => 1),
                                    'order' => 'IFNULL(CustomerMaster.order_no,9999) ASC',
                                    'recursive' => -1
                                );
                $arrExportData = $this->CustomerMaster->find('all',$exportOptions);
                if(count($arrExportData) > 0) {
                    $statusCode = 200;
                    $counter = 0;
                    $arrTableColumnValues = array();
                    foreach($arrExportData as $key => $exportDetails) {
                        ++$counter;
                        $arrTableColumnValues[$key]['sno'] = $counter.'.';
                        $arrTableColumnValues[$key]['name'] = $exportDetails['CustomerMaster']['name'];
                        $arrTableColumnValues[$key]['code'] = $exportDetails['CustomerMaster']['code'];
                        $arrTableColumnValues[$key]['order_no'] = $exportDetails['CustomerMaster']['order_no'];
                    }
                    $headers = array('sno'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                    $response = array('status' => 1,'date' => date('Y-m-d-H:i:s'),'message' => __('record_fetched',true),'data' => $arrTableColumnValues,'header' => $headers);
                } else {
                    $response = array('status' => 0,'message' => __('no_record',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'customer_masters','method' => 'export','request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>