<?php
App::uses('AppController','Controller');

class StockReportController extends AppController {
    public $name = 'StockReport';
    public $uses = array('ErrorLog','ProductMaster','PurchaseMaster','InvoiceMaster');
    public $components = array('AppUtilities');

    public function product_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['stock']) && !empty($this->request->data['stock'])) {
                    $count = 0;
                    if(isset($this->request->data['stock'][0]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'CategoryMaster.name';
                        $arrFieldData[$count]['tableName'] = 'CategoryMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Category';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][0]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][1]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'ProductMaster.name';
                        $arrFieldData[$count]['tableName'] = 'ProductMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Product';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][1]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][2]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'ProductMaster.code';
                        $arrFieldData[$count]['tableName'] = 'ProductMaster';
                        $arrFieldData[$count]['fieldName'] = 'code';
                        $arrFieldData[$count]['caption'] = 'Product Code';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][2]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][3]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'ProductMaster.hsn_code';
                        $arrFieldData[$count]['tableName'] = 'ProductMaster';
                        $arrFieldData[$count]['fieldName'] = 'hsn_code';
                        $arrFieldData[$count]['caption'] = 'HSN Code';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][3]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][4]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'ProductMaster.purchase_price';
                        $arrFieldData[$count]['tableName'] = 'ProductMaster';
                        $arrFieldData[$count]['fieldName'] = 'purchase_price';
                        $arrFieldData[$count]['caption'] = 'Cost Price';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][4]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][5]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'ProductMaster.sale_price';
                        $arrFieldData[$count]['tableName'] = 'ProductMaster';
                        $arrFieldData[$count]['fieldName'] = 'sale_price';
                        $arrFieldData[$count]['caption'] = 'Sale Price';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][5]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][6]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'TaxGroupMaster.name';
                        $arrFieldData[$count]['tableName'] = 'TaxGroupMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Tax Group';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][6]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][7]['field'])) {
                        $arrFieldData[$count]['columnName'] = 'UnitMaster.name';
                        $arrFieldData[$count]['tableName'] = 'UnitMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Unit';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][7]['order'];
                        ++$count;
                    }
                }
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                    }
                    $conditions = array('ProductMaster.status' => 1);
                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['ProductMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                        $conditions['CategoryMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                        $conditions['ProductMaster.category_master_id'] = $this->request->data['category_master_id'];
                    }

                    if(isset($this->request->data['tax_group_master_id']) && !empty($this->request->data['tax_group_master_id'])) {
                        $conditions['ProductMaster.tax_group_master_id'] = $this->request->data['tax_group_master_id'];
                    }

                    if(isset($this->request->data['unit_master_id']) && !empty($this->request->data['unit_master_id'])) {
                        $conditions['ProductMaster.unit_master_id'] = $this->request->data['unit_master_id'];
                    }

                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'ProductMaster.name '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'ProductMaster.code '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'ProductMaster.hsn_code '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'CategoryMaster.name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'ProductMaster.name '.$sortyType);
                                    break;
                        }
                    }

                    $options = array(
                                    'fields' => array_column($arrFieldData,'columnName'),
                                    'joins' => array(
                                        array(
                                            'table' => 'category_masters',
                                            'alias' => 'CategoryMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('ProductMaster.category_master_id = CategoryMaster.id','CategoryMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'tax_group_masters',
                                            'alias' => 'TaxGroupMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('ProductMaster.tax_group_master_id = TaxGroupMaster.id','TaxGroupMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'unit_masters',
                                            'alias' => 'UnitMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('ProductMaster.unit_master_id = UnitMaster.id','UnitMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'recursive' => -1,
                                    'group' => array('ProductMaster.id')
                                );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->ProductMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = array();
                        foreach($arrRecords as $key => $data) {
                            foreach($arrFieldData as $index => $fields) {
                                if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                    $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                }
                            }
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function purchase_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['stock']) && !empty($this->request->data['stock'])) {
                    $fcount = $count = 0;
                    $arrFieldData = $arrHeaderData = array();
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'voucher_no';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['caption'] = 'Voucher No.';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'voucher_date';
                        $arrFieldData[$count]['caption'] = 'Voucher Date';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'firm_name';
                        $arrFieldData[$count]['caption'] = 'Supplier';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    if(count($arrFieldData) > 0) {
                        uasort($arrFieldData,function($a,$b){
                            if($a['order'] == 0){
                                return 1;
                            }
                            if($b['order'] == 0){
                                return -1;
                            }
                            return ($a['order'] < $b['order']) ? -1 : 1;
                        });
                        foreach($arrFieldData as $key => $header) {
                            $arrHeaderData[] = $header;
                        }
                        $conditions = array('ProductMaster.status' => 1);
                        if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                            $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                            $conditions['PurchaseMaster.voucher_date >='] = $fromDate;
                        }

                        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                            $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                            $conditions['PurchaseMaster.voucher_date <='] = $endDate;
                        }

                        if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                            $conditions['PurchaseMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                        }

                        if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                            $conditions['PurchaseMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                        }
                        
                        if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                            $conditions['ProductMaster.category_master_id'] = $this->request->data['category_master_id'];
                        }

                        if(isset($this->request->data['product_master_id']) && !empty($this->request->data['product_master_id'])) {
                            $conditions['ProductMaster.id'] = $this->request->data['product_master_id'];
                        }

                        if(isset($this->request->data['supplier_master_id']) && !empty($this->request->data['supplier_master_id'])) {
                            $conditions['PurchaseMaster.supplier_master_id'] = $this->request->data['supplier_master_id'];
                        }
                        
                        $orderBy =  array();
                        if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                            $sortBy = (int) $this->request->data['sort_by'];
                            $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                            switch($sortBy) {
                                case 1:
                                        $orderBy = array('order' => 'PurchaseMaster.voucher_no '.$sortyType);
                                        break;
                                case 2:
                                        $orderBy = array('order' => 'PurchaseMaster.voucher_date '.$sortyType);
                                        break;
                                case 3:
                                        $orderBy = array('order' => 'SupplierMaster.firm_name '.$sortyType);
                                        break;
                                default:
                                        $orderBy = array('order' => 'PurchaseMaster.voucher_no '.$sortyType);
                                        break;
                            }
                        }
                        $fields = array('PurchaseMaster.id','SupplierMaster.firm_name','PurchaseMaster.voucher_no','DATE_FORMAT(PurchaseMaster.voucher_date,"%d-%m-%Y") as voucher_date','ProductMaster.id','ProductMaster.name','PurchaseTran.quantity');
                        $options = array(
                                        'fields' => $fields,
                                        'joins' => array(
                                            array(
                                                'table' => 'purchase_trans',
                                                'alias' => 'PurchaseTran',
                                                'type' => 'INNER',
                                                'conditions' => array('PurchaseMaster.id = PurchaseTran.purchase_master_id','PurchaseTran.status' => 1)
                                            ),
                                            array(
                                                'table' => 'product_variant_trans',
                                                'alias' => 'PVT',
                                                'type' => 'INNER',
                                                'conditions' => array('PurchaseTran.product_variant_tran_id = PVT.id','PVT.status' => 1)
                                            ),
                                            array(
                                                'table' => 'product_masters',
                                                'alias' => 'ProductMaster',
                                                'type' => 'INNER',
                                                'conditions' => array('ProductMaster.id = PVT.product_master_id','ProductMaster.status' => 1)
                                            ),
                                            array(
                                                'table' => 'supplier_masters',
                                                'alias' => 'SupplierMaster',
                                                'type' => 'LEFT',
                                                'conditions' => array('PurchaseMaster.supplier_master_id = SupplierMaster.id','SupplierMaster.status' => 1)
                                            )
                                        ),
                                        'conditions' => $conditions,
                                        'recursive' => -1
                                    );
                        $options = array_merge($options,$orderBy);
                        $arrRecords = $this->PurchaseMaster->find('all',$options);
                        if(count($arrRecords) > 0) {
                            $arrContentData = $arrProductDetail = array();
                            foreach($arrRecords as $key => $data) {
                                $purchaseId = $data['PurchaseMaster']['id'];
                                $productId = $data['ProductMaster']['id'];
                                $arrContentData[$purchaseId]['firm_name'] = $data['SupplierMaster']['firm_name'];
                                $arrContentData[$purchaseId]['voucher_no'] = $data['PurchaseMaster']['voucher_no'];
                                $arrContentData[$purchaseId]['voucher_date'] = $data[0]['voucher_date'];
                                if(isset($arrContentData[$purchaseId]['total_qty'] )) {
                                    $arrContentData[$purchaseId]['total_qty']  += $data['PurchaseTran']['quantity'];
                                } else {
                                    $arrContentData[$purchaseId]['total_qty']  = $data['PurchaseTran']['quantity'];
                                }
                                $arrContentData[$purchaseId]['detail'][$productId] = $data['PurchaseTran']['quantity'];
                                $arrProductDetail[$productId]['id'] = $productId;
                                $arrProductDetail[$productId]['name'] = $data['ProductMaster']['name'];
                            }
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => array_values($arrContentData),'product' => array_values($arrProductDetail));
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function purchase_order_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if(isset($this->request->data['stock']) && !empty($this->request->data['stock'])) {
                $fcount = $count = 0;
                    $arrFieldData = $arrHeaderData = array();
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'invoice_no';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['caption'] = 'Invoice No.';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'invoice_date';
                        $arrFieldData[$count]['caption'] = 'Invoice Date';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'firm_name';
                        $arrFieldData[$count]['caption'] = 'Supplier';
                        $arrFieldData[$count]['cols'] = 1;
                        $arrFieldData[$count]['rows'] = 2;
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    if(count($arrFieldData) > 0) {
                        uasort($arrFieldData,function($a,$b){
                            if($a['order'] == 0){
                                return 1;
                            }
                            if($b['order'] == 0){
                                return -1;
                            }
                            return ($a['order'] < $b['order']) ? -1 : 1;
                        });
                        foreach($arrFieldData as $key => $header) {
                            $arrHeaderData[] = $header;
                        }
                        $conditions = array('ProductMaster.status' => 1,'InvoiceMaster.type' => 1);
                        if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                            $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                            $conditions['InvoiceMaster.invoice_date >='] = $fromDate;
                        }

                        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                            $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                            $conditions['InvoiceMaster.invoice_date <='] = $endDate;
                        }

                        if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                            $conditions['InvoiceMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                        }

                        if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                            $conditions['InvoiceMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                        }
                        
                        if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                            $conditions['ProductMaster.category_master_id'] = $this->request->data['category_master_id'];
                        }

                        if(isset($this->request->data['product_master_id']) && !empty($this->request->data['product_master_id'])) {
                            $conditions['ProductMaster.id'] = $this->request->data['product_master_id'];
                        }

                        if(isset($this->request->data['supplier_master_id']) && !empty($this->request->data['supplier_master_id'])) {
                            $conditions['InvoiceMaster.customer_master_id'] = $this->request->data['supplier_master_id'];
                        }
                        $orderBy =  array();
                        if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                            $sortBy = (int) $this->request->data['sort_by'];
                            $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                            switch($sortBy) {
                                case 1:
                                        $orderBy = array('order' => 'InvoiceMaster.invoice_no '.$sortyType);
                                        break;
                                case 2:
                                        $orderBy = array('order' => 'InvoiceMaster.invoice_date '.$sortyType);
                                        break;
                                case 3:
                                        $orderBy = array('order' => 'SupplierMaster.firm_name '.$sortyType);
                                        break;
                                default:
                                        $orderBy = array('order' => 'InvoiceMaster.invoice_no '.$sortyType);
                                        break;
                            }
                        }
                        $fields = array('InvoiceMaster.id','SupplierMaster.firm_name','InvoiceMaster.invoice_no','DATE_FORMAT(InvoiceMaster.invoice_date,"%d-%m-%Y") as invoice_date','ProductMaster.id','ProductMaster.name','InvoiceTran.quantity','InvoiceTran.sale_price','InvoiceTran.unit_price AS purchase_price','InvoiceTran.total');
                        $options = array(
                                        'fields' => $fields,
                                        'joins' => array(
                                            array(
                                                'table' => 'invoice_trans',
                                                'alias' => 'InvoiceTran',
                                                'type' => 'INNER',
                                                'conditions' => array('InvoiceMaster.id = InvoiceTran.invoice_master_id','InvoiceTran.status' => 1)
                                            ),
                                            array(
                                                'table' => 'product_variant_trans',
                                                'alias' => 'PVT',
                                                'type' => 'INNER',
                                                'conditions' => array('InvoiceTran.reference_tran_id = PVT.id','PVT.status' => 1)
                                            ),
                                            array(
                                                'table' => 'product_masters',
                                                'alias' => 'ProductMaster',
                                                'type' => 'INNER',
                                                'conditions' => array('ProductMaster.id = PVT.product_master_id','ProductMaster.status' => 1)
                                            ),
                                            array(
                                                'table' => 'supplier_masters',
                                                'alias' => 'SupplierMaster',
                                                'type' => 'INNER',
                                                'conditions' => array('InvoiceMaster.customer_master_id = SupplierMaster.id','SupplierMaster.status' => 1)
                                            )
                                        ),
                                        'conditions' => $conditions,
                                        'recursive' => -1
                                    );
                        $options = array_merge($options,$orderBy);
                        $arrRecords = $this->InvoiceMaster->find('all',$options);
                        #pr($arrRecords);exit;
                        if(count($arrRecords) > 0) {
                            $arrContentData = $arrProductDetail = array();
                            foreach($arrRecords as $key => $data) {
                                $purchaseId = $data['InvoiceMaster']['id'];
                                $productId = $data['ProductMaster']['id'];
                                $arrContentData[$purchaseId]['firm_name'] = $data['SupplierMaster']['firm_name'];
                                $arrContentData[$purchaseId]['invoice_no'] = $data['InvoiceMaster']['invoice_no'];
                                $arrContentData[$purchaseId]['invoice_date'] = $data[0]['invoice_date'];
                                $arrContentData[$purchaseId]['total'] = $data['InvoiceTran']['total'];
                                $arrContentData[$purchaseId]['detail'][$productId]['quantity'] = $data['InvoiceTran']['quantity'];
                                $arrContentData[$purchaseId]['detail'][$productId]['sale_price'] = $data['InvoiceTran']['sale_price'];
                                $arrContentData[$purchaseId]['detail'][$productId]['purchase_price'] = $data['InvoiceTran']['purchase_price'];
                                $arrContentData[$purchaseId]['detail'][$productId]['total'] = $data['InvoiceTran']['total'];
                                $arrProductDetail[$productId]['id'] = $productId;
                                $arrProductDetail[$productId]['name'] = $data['ProductMaster']['name'];
                            }
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => array_values($arrContentData),'product' => array_values($arrProductDetail));
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                    }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function sale_dynamic_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
       try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['stock']) && !empty($this->request->data['stock'])) {
                    $count = $fcount = 0;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'invoice_no';
                        $arrFieldData[$count]['caption'] = 'Invoice No';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'invoice_date';
                        $arrFieldData[$count]['caption'] = 'Invoice Date';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'tax';
                        $arrFieldData[$count]['caption'] = 'Tax';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'discount_price';
                        $arrFieldData[$count]['caption'] = 'Discount';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'payment_status';
                        $arrFieldData[$count]['caption'] = 'Payment Status';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                    
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'payment_mode';
                        $arrFieldData[$count]['caption'] = 'Product Mode';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'mobile_no';
                        $arrFieldData[$count]['caption'] = 'Customer Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'email_id';
                        $arrFieldData[$count]['caption'] = 'Customer Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['stock'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'residential_address';
                        $arrFieldData[$count]['caption'] = 'Customer Address';
                        $arrFieldData[$count]['order'] = $this->request->data['stock'][$fcount]['order'];
                        ++$count;
                    }
                }

                #pr($arrFieldData);exit;
                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = $header;
                    }

                    $conditions = array('ProductMaster.status' => 1,'InvoiceMaster.type' => 2);
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                        $conditions['InvoiceMaster.invoice_date >='] = $fromDate;
                    }

                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                        $conditions['InvoiceMaster.invoice_date <='] = $endDate;
                    }
                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['InvoiceMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                        $conditions['InvoiceMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                    }
                    
                    if(isset($this->request->data['category_master_id']) && !empty($this->request->data['category_master_id'])) {
                        $conditions['ProductMaster.category_master_id'] = $this->request->data['category_master_id'];
                    }

                    if(isset($this->request->data['product_master_id']) && !empty($this->request->data['product_master_id'])) {
                        $conditions['ProductMaster.id'] = $this->request->data['product_master_id'];
                    }

                    if(isset($this->request->data['payment_type']) &&  !empty($this->request->data['payment_type'])) {
                        $conditions['InvoiceMaster.payment_type'] = intval($this->request->data['payment_type'] - 1);
                    }

                    if(isset($this->request->data['payment_mode']) && !empty($this->request->data['payment_mode'])) {
                        $conditions['InvoiceMaster.payment_mode'] = $this->request->data['payment_mode'];
                    }

                    $orderBy =  array();

                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'InvoiceMaster.max_invoice_no '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'InvoiceMaster.invoice_date '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'Customer.name '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'InvoiceMaster.max_invoice_no '.$sortyType);
                                    break;
                        }
                    }

                    $options = array(
                                    'fields' => array(
                                                    'InvoiceMaster.invoice_no','DATE_FORMAT(InvoiceMaster.invoice_date,"%d-%m-%Y") as invoice_date','InvoiceMaster.id',
                                                    'Customer.name','Customer.mobile_no','Customer.email_id','Customer.residential_address','ProductMaster.id','ProductMaster.name',
                                                    "(CASE InvoiceMaster.payment_status WHEN 0 THEN 'UNPAID' WHEN 1 THEN 'PARTIAL PAYMENT' ELSE 'PAID' END) AS payment_status",
                                                    "(CASE InvoiceMaster.payment_mode WHEN 0 THEN 'N/A' WHEN 1 THEN 'Cash' WHEN 2 THEN 'Cheque' WHEN 3 THEN 'Credit/Debit' WHEN 4 THEN 'Netbanking' ELSE 'UPI' END) AS payment_mode",
                                                    'InvoiceTran.quantity','InvoiceTran.tax','InvoiceTran.unit_price','InvoiceTran.total','InvoiceTran.discount_price'
                                                ),
                                    'joins' => array(
                                        array(
                                            'table' => 'invoice_trans',
                                            'alias' => 'InvoiceTran',
                                            'type' => 'INNER',
                                            'conditions' => array('InvoiceMaster.id = InvoiceTran.invoice_master_id','InvoiceTran.status' => 1)
                                        ),
                                        array(
                                            'table' => 'product_variant_trans',
                                            'alias' => 'PVT',
                                            'type' => 'INNER',
                                            'conditions' => array('InvoiceTran.reference_tran_id = PVT.id','PVT.status' => 1)
                                        ),
                                        array(
                                            'table' => 'product_masters',
                                            'alias' => 'ProductMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('ProductMaster.id = PVT.product_master_id','ProductMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'customers',
                                            'alias' => 'Customer',
                                            'type' => 'LEFT',
                                            'conditions' => array('InvoiceMaster.customer_master_id = Customer.id','Customer.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'recursive' => -1
                                );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->InvoiceMaster->find('all',$options);
                    #pr($arrRecords);exit;
                    if(count($arrRecords) > 0) {
                        $arrContentData = $arrProductDetail = array();
                        foreach($arrRecords as $key => $data) {
                            $invoiceId = $data['InvoiceMaster']['id'];
                            $productId = $data['ProductMaster']['id'];
                            $arrContentData[$invoiceId]['invoice_no'] = $data['InvoiceMaster']['invoice_no'];
                            $arrContentData[$invoiceId]['invoice_date'] = $data[0]['invoice_date'];
                            $arrContentData[$invoiceId]['name'] = $data['Customer']['name'];
                            $arrContentData[$invoiceId]['mobile_no'] = $data['Customer']['mobile_no'];
                            $arrContentData[$invoiceId]['email_id'] = $data['Customer']['email_id'];
                            $arrContentData[$invoiceId]['residential_address'] = $data['Customer']['residential_address'];
                            $arrContentData[$invoiceId]['payment_status'] = $data[0]['payment_status'];
                            $arrContentData[$invoiceId]['payment_mode'] = $data[0]['payment_mode'];
                            if(isset($arrContentData[$invoiceId]['tax'])) {
                                $arrContentData[$invoiceId]['tax'] += number_format( $data['InvoiceTran']['tax'],2,'.','');
                            } else {
                                $arrContentData[$invoiceId]['tax'] = number_format( $data['InvoiceTran']['tax'],2,'.','');
                            }
                            if(isset($arrContentData[$invoiceId]['discount_price'])) {
                                $arrContentData[$invoiceId]['discount_price'] += number_format( $data['InvoiceTran']['discount_price'],2,'.','');
                            } else {
                                $arrContentData[$invoiceId]['discount_price'] = number_format( $data['InvoiceTran']['discount_price'],2,'.','');
                            }
                            if(isset($arrContentData[$invoiceId]['total'])) {
                                $arrContentData[$invoiceId]['total'] += number_format( $data['InvoiceTran']['total'],2,'.','');
                            } else {
                                $arrContentData[$invoiceId]['total'] = number_format( $data['InvoiceTran']['total'],2,'.','');
                            }
                            $arrContentData[$invoiceId]['detail'][$productId]['quantity'] = $data['InvoiceTran']['quantity'];
                            $arrContentData[$invoiceId]['detail'][$productId]['price'] = $data['InvoiceTran']['unit_price'];
                            $arrProductDetail[$productId]['id'] = $productId;
                            $arrProductDetail[$productId]['name'] = $data['ProductMaster']['name'];
                        }
                        unset($arrFieldData);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => array_values($arrContentData),'product' => array_values($arrProductDetail));
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('Please select atleast one field !.',true)); 
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
