<?php
App::uses('AppController','Controller');

class DashboardController extends AppController{
    public $name = 'Dashboard';
    public $uses = array('RoleLinkMaster','ErrorLog','User');
    public $components = array('UserManagement');

    public function sidebar() {
        $response = array('status' => 0 ,'message' => __('invalid_response',true));
        $statusCode = 400;
        if($this->request->is('post')) {
            $statusCode = 200;
            $arrRoleLinks = $this->RoleLinkMaster->find('threaded');
            $response = array('status' => 1,'message' => 'Sidebar menu fetched successfully !.','sidebar_menu' => $arrRoleLinks);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function today_job($branchId = 0) {
        $response = array('status' => 0,'message' => 'invalid');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $conditions = "";
                if(!empty($branchId)) {
                    $conditions = " AND branch_master_id = '".$branchId."'";
                }
                $date = date('Y-m-d');
                $query = "SELECT '1' AS `job_status`,COUNT(id) AS `count` FROM vehicle_job_masters WHERE `status` = 1 AND job_status <= 3 AND job_date = '".$date."' ".$conditions."
                            UNION ALL
                            SELECT '2' AS `job_status`,COUNT(id) AS `count` FROM vehicle_job_masters WHERE `status`= 1 AND job_status = 4 AND job_date = '".$date."' ".$conditions."
                            UNION ALL
                            SELECT '3' AS `job_status`,COUNT(id) AS `count` FROM vehicle_job_masters WHERE `status` = 1 AND job_status = 8 AND is_generate_invoice = 0 AND job_date = '".$date."' ".$conditions."
                            UNION ALL
                            SELECT '4' AS `job_status`,COUNT(id) AS `count` FROM vehicle_job_masters WHERE `status` = 1 AND job_status >= 8 AND is_generate_invoice = 1 AND job_date = '".$date."' ".$conditions."";
                $result = $this->User->query($query,false);
                if(count($result) > 0) {
                    $statusCode = 200;
                    $arrJob['open'] = $result[0][0]['count'];
                    $arrJob['inprogress'] = $result[1][0]['count'];
                    $arrJob['complete'] = $result[2][0]['count'];
                    $arrJob['invoice'] = $result[3][0]['count'];
                    $response = array('status' => 1,'message' => __('record_fetched',true),'data' => $arrJob);
                } else {
                    $response = array('status' => 0,'message' => __('no_record',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_response',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'dashboard','method' => 'today_job','request' => '','description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function get_user_rights($userId = null,$roleId = null) {
        $response = array('status' => 0 ,'message' => __('invalid_response',true));
        $statusCode = 400;
        try {
            if(!empty($roleId)) {
                $arrUserRights = $this->UserManagement->getUserRights($userId,$roleId);
                if(!empty($arrUserRights)) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' =>__('record_fetched',true),'data' => $arrUserRights);
                } else {
                    $response = array('status' => 0,'message' =>__('user rights could not fetched properly,please try again later!...'),'data' => $arrUserRights);
                }
            } else {
                $response = array('status' => 0,'message' => __('Invalid Role',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>'','controller' => 'users','method' => 'get_user_rights','request' => array('role_id' => $roleId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('internal_server_error',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
