<?php
App::uses('AppController','Controller');
class SupplierMastersController extends AppController {
    public $name = 'SupplierMasters';
    public $layout = false;
    public $uses = array('SupplierMaster','PurchaseMaster','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('SupplierMaster.status' => 1,'SupplierMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['firm_name']) && !empty($this->request->data['firm_name'])) {
                    $conditions['SupplierMaster.firm_name LIKE'] = '%'.trim($this->request->data['firm_name']).'%';
                }

                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['SupplierMaster.first_name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['mobile_no']) && !empty($this->request->data['mobile_no'])) {
                    $conditions['SupplierMaster.mobile_no LIKE'] = '%'.trim($this->request->data['mobile_no']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('SupplierMaster.first_name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('SupplierMaster.mobile_no '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('SupplierMaster.email_id '.$sortyType);
                                break;
                        default:
                                $orderBy = array('SupplierMaster.first_name '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('SupplierMaster.first_name ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->SupplierMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','firm_name','TM.name','first_name','middle_name','last_name','firm_name','mobile_no','email_id','COUNT(PurchaseMaster.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'purchase_masters',
                                            'alias' => 'PurchaseMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('SupplierMaster.id = PurchaseMaster.supplier_master_id','PurchaseMaster.status' => 1)
                                        ),
                                        array(
                                            'table' => 'title_masters',
                                            'alias' => 'TM',
                                            'type' => 'LEFT',
                                            'conditions' => array('SupplierMaster.title_master_id = TM.id','TM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('SupplierMaster.id'),
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->SupplierMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['SupplierMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['TM']['name'].' '.$tableDetails['SupplierMaster']['first_name'].' '.$tableDetails['SupplierMaster']['middle_name'].' '.$tableDetails['SupplierMaster']['last_name'];
                        $records[$key]['mobile_no'] = $tableDetails['SupplierMaster']['mobile_no'];
                        $records[$key]['firm_name'] = $tableDetails['SupplierMaster']['firm_name'];
                        $records[$key]['email_id'] = $tableDetails['SupplierMaster']['email_id'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','firm_name' => 'Firm Name','name'=>'Name','mobile_no'=>'Mobile No','email_id' => 'Email ID');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['SupplierMaster'] = $this->request->data;
                #pr($arrSaveRecords);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['SupplierMaster']['id']) && !empty($arrSaveRecords['SupplierMaster']['id'])) {
                    $arrSaveRecords['SupplierMaster']['id'] = $this->decryption($arrSaveRecords['SupplierMaster']['id']);
                }
                $arrSaveRecords['SupplierMaster']['branch_master_id'] = isset($arrSaveRecords['SupplierMaster']['branch_master_id']) ? $arrSaveRecords['SupplierMaster']['branch_master_id'] : '';
                $this->SupplierMaster->set($arrSaveRecords);
                if($this->SupplierMaster->validates()) {
                    $dataSource = $this->SupplierMaster->getDataSource();
                    $fieldList = array('id','title_master_id','firm_name','first_name','middle_name','last_name','mobile_no','email_id','branch_master_id','description','address');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['SupplierMaster']['id']) || empty($arrSaveRecords['SupplierMaster']['id'])) {
                            $this->SupplierMaster->create();
                        }
                        if(!$this->SupplierMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['SupplierMaster']['id']) && !empty($arrSaveRecords['SupplierMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->SupplierMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('title_master_id','firm_name','first_name','middle_name','last_name','mobile_no','email_id','description','address'),
                            'conditions' => array('status' => 1,'id' => $id,'branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->SupplierMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['SupplierMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('id','id'),'conditions' => array('status'=> 1,'supplier_master_id' => $this->request->data['id']));
                    $arrRecords = $this->PurchaseMaster->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_keys($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->SupplierMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['SupplierMaster.status'] = -1;
                            $updateFields['SupplierMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['SupplierMaster.id'] = $arrDeleteRecords;
                            if(!$this->SupplierMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
