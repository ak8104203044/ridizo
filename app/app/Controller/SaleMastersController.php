<?php
App::uses('AppController','Controller');
class SaleMastersController extends AppController {
    public $name = 'SaleMasters';
    public $layout = false;
    public $uses = array('SaleMaster','SaleTran','SaleTaxTran','ProductConsumptionTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $companyYearId = isset($this->request->data['company_year_id']) ? $this->request->data['company_year_id'] : '';
                $conditions = array('SaleMaster.status' => 1,'SaleMaster.branch_master_id' => $firmId,'SaleMaster.company_year_master_id' => $companyYearId);
                if(isset($this->request->data['unique_no']) && !empty($this->request->data['unique_no'])) {
                    $conditions['SaleMaster.unique_no LIKE'] = '%'.trim($this->request->data['unique_no']).'%';
                }

                if(isset($this->request->data['invoice_date']) && !empty($this->request->data['invoice_date'])) {
                    $conditions['SaleMaster.invoice_date'] = $this->AppUtilities->date_format(trim($this->request->data['invoice_date']),'yyyy-mm-dd');
                }

                if(isset($this->request->data['customer_master_id']) && !empty($this->request->data['customer_master_id'])) {
                    $conditions['SaleMaster.customer_master_id'] = trim($this->request->data['customer_master_id']);
                }

                if(isset($this->request->data['payment_type']) && !empty($this->request->data['payment_type'])) {
                    $conditions['SaleMaster.payment_type'] = intval($this->request->data['payment_type'] - 1);
                }

                $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                $totalRecords = $this->SaleMaster->find('count',$tableCountOptions);
                $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                $start = ($page - 1) * $length;
                $end = ($start + $length);
                $end = ($end > $totalRecords) ? $totalRecords : $end;
                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('SaleMaster.max_unique_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('SaleMaster.invoice_date '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('SaleMaster.amount '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('CM.name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('SaleMaster.max_unique_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('SaleMaster.max_unique_no ASC');
                }
                $tableOptions = array(
                                    'fields' => array('id','unique_no','payment_type','invoice_date','amount','CM.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'customer_masters',
                                            'alias' => 'CM',
                                            'type' => 'LEFT',
                                            'conditions' => array('SaleMaster.customer_master_id = CM.id','CM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('SaleMaster.id'),
                                    'limit' => $length,
                                    'offset' => $start,
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                $arrTableData = $this->SaleMaster->find('all',$tableOptions);
                if($totalRecords > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['SaleMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['unique_no'] = $tableDetails['SaleMaster']['unique_no'];
                        $records[$key]['invoice_date'] = $this->AppUtilities->date_format($tableDetails['SaleMaster']['invoice_date'],'dd-mm-yyyy');
                        $records[$key]['amount'] = $tableDetails['SaleMaster']['amount'];
                        $records[$key]['payment_type'] = $tableDetails['SaleMaster']['payment_type'];
                        $records[$key]['customer'] = $tableDetails['CM']['name'];
                        $records[$key]['is_exists'] = ($tableDetails['SaleMaster']['payment_type'] == 0) ? 0 : 1;
                    }
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    $statusCode = 200;
                } else {
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $this->request->data['random'] = isset($this->request->data['random']) ? $this->request->data['random'] : rand(1,9);
                usleep($this->request->data['random']);
                #pr($this->request->data);exit;
                $arrSaveRecords['SaleMaster'] = $this->request->data;
                if(isset($arrSaveRecords['SaleMaster']['id']) && !empty($arrSaveRecords['SaleMaster']['id'])) {
                    $arrSaveRecords['SaleMaster']['id'] = $this->decryption($arrSaveRecords['SaleMaster']['id']);
                }
                $arrSaveRecords['SaleMaster']['branch_master_id'] = isset($arrSaveRecords['SaleMaster']['branch_master_id']) ? $arrSaveRecords['SaleMaster']['branch_master_id'] : '';
                $arrSaveRecords['SaleMaster']['company_year_master_id'] = isset($arrSaveRecords['SaleMaster']['company_year_master_id']) ? $arrSaveRecords['SaleMaster']['company_year_master_id'] : '';
                $arrProductTranData = $arrProdctDetail = array();
                if(isset($arrSaveRecords['SaleMaster']['item_detail']) && !empty($arrSaveRecords['SaleMaster']['item_detail'])) {
                    $arrProdctDetail = json_decode($arrSaveRecords['SaleMaster']['item_detail'],true);
                    $arrSaveRecords['SaleMaster']['quantity'] = array_column($arrProdctDetail,'quantity');
                    $arrSaveRecords['SaleMaster']['price'] = array_column($arrProdctDetail,'price');
                    $arrSaveRecords['SaleMaster']['product_master_id'] = array_column($arrProdctDetail,'product_master_id');
                    unset($arrSaveRecords['SaleMaster']['item_detail']);
                }
                #pr($arrProdctDetail);
                #pr($arrSaveRecords);exit;
                $this->SaleMaster->set($arrSaveRecords);
                if($this->SaleMaster->validates()) {
                    unset($arrSaveRecords['SaleMaster']['product_master_id'],$arrSaveRecords['SaleMaster']['quantity'],$arrSaveRecords['SaleMaster']['price']);
                    $dataSource = $this->SaleMaster->getDataSource();
                    $fieldList = array('id','unique_no','max_unique_no','invoice_date','amount','customer_master_id','company_year_master_id','branch_master_id','description');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['SaleMaster']['id']) || empty($arrSaveRecords['SaleMaster']['id'])) {
                            $this->SaleMaster->create();
                            $firmId = $arrSaveRecords['SaleMaster']['branch_master_id'];
                            $tableName = 'AutoInvoice'.$firmId.'Code';
                            $arrSaveInvoiceData[$tableName]['name'] = $arrSaveRecords['SaleMaster']['invoice_date'];
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            if(!$this->$tableName->save($arrSaveInvoiceData)) {
                                throw new Exception(__('Auto Employee code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrCodeData = $this->AppUtilities->getUserSettingCode($firmId,2,$maxNumber);
                            if(!empty($arrCodeData)) {
                                $arrSaveRecords['SaleMaster']['unique_no'] = $arrCodeData['code'];
                                $arrSaveRecords['SaleMaster']['max_unique_no'] = $arrCodeData['maxNumber'];
                            }
                        } else {
                            $options = array(
                                            'fields' => array('product_master_id','id'),
                                            'conditions' => array('sale_master_id' => $arrSaveRecords['SaleMaster']['id'],'status' => 1)
                                        );
                            $arrProductConsuptionData = $this->ProductConsumptionTran->find('list',$options);
                        }
                        $arrSaveRecords['SaleMaster']['invoice_date'] = $this->AppUtilities->date_format($arrSaveRecords['SaleMaster']['invoice_date'],'yyyy-mm-dd');
                        #pr($arrSaveRecords);
                        if(!$this->SaleMaster->save($arrSaveRecords,array('validate' => false,'fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        
                        if(!isset($arrSaveRecords['SaleMaster']['id'])) {
                            $arrSaveRecords['SaleMaster']['id'] = $this->SaleMaster->getInsertID();
                        }

                        if(isset($arrProdctDetail) && count($arrProdctDetail) > 0) {
                            $arrSaleProductTranData = $arrSaleTaxTranData = $arrProductConsumptionTranData = array();
                            $arrProductId = array();
                            $count = 0;
                            $taxcount = 0;
                            foreach($arrProdctDetail as $key => $product) {
                                $product['subtotal'] = $product['total'];
                                $product['tax'] = (!empty($product['tax'])) ? $product['tax'] : 0;
                                $product['total'] = number_format($product['total'] + $product['tax'],2,'.','');
                                if(isset($product['sale_tran_id']) && !empty($product['sale_tran_id'])) {
                                    $arrSaleProductTranData[$count]['SaleTran']['id'] = $product['sale_tran_id'];
                                }
                                $arrProductId[$product['product_master_id']] = $product['product_master_id'];
                                $arrSaleProductTranData[$count]['SaleTran']['sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                $arrSaleProductTranData[$count]['SaleTran']['product_master_id'] = $product['product_master_id'];
                                $arrSaleProductTranData[$count]['SaleTran']['description'] = (isset($product['description'])) ? $product['description'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['quantity'] = (isset($product['quantity'])) ? $product['quantity'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['unit_price'] = (isset($product['price'])) ? $product['price'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['actual_price'] = (isset($product['item_price'])) ? $product['item_price'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['tax'] = (isset($product['tax'])) ? $product['tax'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['discount'] = (isset($product['discount'])) ? $product['discount'] : '0';
                                $arrSaleProductTranData[$count]['SaleTran']['discount_price'] = (isset($product['discountAmount'])) ? $product['discountAmount'] : '0';
                                $arrSaleProductTranData[$count]['SaleTran']['subtotal'] = $product['subtotal'];
                                $arrSaleProductTranData[$count]['SaleTran']['total'] = (isset($product['total'])) ? $product['total'] : '';
                                $arrSaleProductTranData[$count]['SaleTran']['branch_master_id'] = $arrSaveRecords['SaleMaster']['branch_master_id'];
                                if(isset($arrProductConsuptionData[$product['product_master_id']])) {
                                    $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['id'] = $arrProductConsuptionData[$product['product_master_id']];
                                }
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['product_master_id'] = $product['product_master_id'];
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['quantity'] = $product['quantity'];
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['date'] = date('Y-m-d');
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['branch_master_id'] = $arrSaveRecords['SaleMaster']['branch_master_id'];
                                $arrProductConsumptionTranData[$count]['ProductConsumptionTran']['company_year_master_id'] = $arrSaveRecords['SaleMaster']['company_year_master_id'];

                                if(isset($product['tax_detail']) && count($product['tax_detail']) > 0) {
                                    foreach($product['tax_detail'] as $taxkey => $taxDetail) {
                                        if(isset($taxDetail['sale_tax_tran_id']) && !empty($taxDetail['sale_tax_tran_id'])) {
                                            $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['id'] = $taxDetail['sale_tax_tran_id'];
                                        }
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['product_master_id'] = $product['product_master_id'];
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['tax_master_id'] = $taxDetail['id'];
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['tax'] = $taxDetail['tax_amount'];
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['total_tax'] = number_format($product['quantity'] * $taxDetail['tax_amount'],2,'.','');
                                        $arrSaleTaxTranData[$taxcount]['SaleTaxTran']['branch_master_id'] = $arrSaveRecords['SaleMaster']['branch_master_id'];
                                        ++$taxcount;
                                    }
                                }
                                ++$count;
                            }
                            #pr($arrSaleProductTranData);
                            #pr($arrProductConsumptionTranData);
                            #pr($arrSaleTaxTranData);exit;

                            if(isset($arrSaveRecords['SaleMaster']['id']) && !empty($arrSaveRecords['SaleMaster']['id']) && !empty($arrProductId)) {
                                $updateFields['SaleTran.status'] = 0;
                                $updateFields['SaleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['SaleTran.sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                $updateParams['NOT']['SaleTran.product_master_id'] = $arrProductId;
                                $this->SaleTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);

                                $updateFields['SaleTaxTran.status'] = 0;
                                $updateFields['SaleTaxTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['SaleTaxTran.sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                $updateParams['NOT']['SaleTaxTran.product_master_id'] = $arrProductId;
                                $this->SaleTaxTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);

                                $updateFields['ProductConsumptionTran.status'] = 0;
                                $updateFields['ProductConsumptionTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['ProductConsumptionTran.sale_master_id'] = $arrSaveRecords['SaleMaster']['id'];
                                $updateParams['NOT']['ProductConsumptionTran.product_master_id'] = $arrProductId;
                                $this->ProductConsumptionTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);
                            }

                            $this->SaleTran->saveAll($arrSaleProductTranData);
                            if(count($arrSaleTaxTranData) >0) {
                                $this->SaleTaxTran->saveAll($arrSaleTaxTranData);
                            }
                            if(count($arrProductConsumptionTranData) > 0) {
                                $this->ProductConsumptionTran->saveAll($arrProductConsumptionTranData);
                            }
                            
                        }
                        
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['SaleMaster']['id']) && !empty($arrSaveRecords['SaleMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->SaleMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function payment_detail($id = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get') && !empty($id)) {
                $options = array('fields' => array('id','amount','part_amount'),'conditions' => array('status' => 1,'id' => $id));
                $arrRecords = $this->SaleMaster->find('first',$options);
                if(count($arrRecords) > 0) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['SaleMaster']);
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('invalid_request_method',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_payment() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $this->request->data['id'] = isset($this->request->data['id']) ? $this->request->data['id'] : 0;
                $options = array('fields' => array('id','amount','part_amount','extra_info'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                $arrRecords = $this->SaleMaster->find('first',$options);
                #pr($arrRecords);
                #pr($this->request->data);exit;
                if(count($arrRecords) > 0) {
                    $dataSource = $this->SaleMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $arrPaymentDetail = (!empty($arrRecords['SaleMaster']['extra_info'])) ? json_decode($arrRecords['SaleMaster']['extra_info'],true) : array();
                        $totalAmount = number_format($arrRecords['SaleMaster']['amount'],2,'.','');
                        $paid = number_format($this->request->data['paid'] + $arrRecords['SaleMaster']['part_amount'],2,'.','');
                        if($totalAmount > $paid) {
                            $paymentType = 1;
                            $partAmount = $paid;
                            $shortFees = number_format($totalAmount - $paid,2,'.','');;
                        } else {
                            $partAmount = 0;
                            $shortFees = 0;
                            $paymentType = 2;
                        }
                        $count = count($arrPaymentDetail);
                        $arrPaymentDetail[$count]['payment_type'] = $paymentType;
                        $arrPaymentDetail[$count]['payment_mode'] = $this->request->data['payment_mode'];
                        $arrPaymentDetail[$count]['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                        $arrPaymentDetail[$count]['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                        $arrPaymentDetail[$count]['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                        $arrPaymentDetail[$count]['amount'] = number_format($this->request->data['paid'],2,'.','');
                        $arrPaymentDetail[$count]['short_fees'] =  $shortFees;
                        $arrPaymentDetail[$count]['payment_date'] = $this->AppUtilities->date_format($this->request->data['payment_date'],'yyyy-mm-dd');
                        if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                            $arrPaymentDetail[$count]['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                        }

                        $arrSaleData = array();
                        $arrSaleData['SaleMaster']['id'] = $arrRecords['SaleMaster']['id'];
                        $arrSaleData['SaleMaster']['payment_type'] = $paymentType;
                        $arrSaleData['SaleMaster']['payment_mode'] = $this->request->data['payment_mode'];
                        $arrSaleData['SaleMaster']['payment_reference_no'] = (isset($this->request->data['payment_reference_no'])) ? $this->request->data['payment_reference_no'] : '';
                        $arrSaleData['SaleMaster']['bank_master_id'] = (isset($this->request->data['bank_master_id'])) ? $this->request->data['bank_master_id'] : '';
                        $arrSaleData['SaleMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                        $arrSaleData['SaleMaster']['part_amount'] = $partAmount;
                        $arrSaleData['SaleMaster']['payment_date'] = $this->AppUtilities->date_format($this->request->data['payment_date'],'yyyy-mm-dd');
                        if(isset($this->request->data['payment_mode_date']) && !empty($this->request->data['payment_mode_date'])) {
                            $arrSaleData['SaleMaster']['payment_mode_date'] = $this->AppUtilities->date_format($this->request->data['payment_mode_date'],'yyyy-mm-dd');
                        }
                        $arrSaleData['SaleMaster']['extra_info'] = json_encode($arrPaymentDetail);
                        #pr($arrSaleData);exit;
                        $this->SaleMaster->save($arrSaleData,array('validate' => false));
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('payment is added successfully',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],$this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
    /**
     * $id is primary key of salesmaster table
     * $type 0 for edit ,1  for view 
     */
    public function record($id = null,$type = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $type = (int) $type;
                    $id = $this->decryption(trim($id));
                    if($type === 1) {
                        $options = array(
                            'fields' => array('id','unique_no','invoice_date','amount','customer_master_id','description','CustomerMaster.name','CustomerMaster.address','CustomerMaster.mobile_no','CustomerMaster.email_id'),
                            'joins' => array(
                                array(
                                    'table' => 'customer_masters',
                                    'alias' => 'CustomerMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('SaleMaster.customer_master_id = CustomerMaster.id','CustomerMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('SaleMaster.status' => 1,'SaleMaster.id' => $id),
                            'recursive' => -1
                        );
                    } else {
                        $options = array(
                            'fields' => array('id','unique_no','invoice_date','amount','customer_master_id','description'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                    }
                    $arrRecords = $this->SaleMaster->find('first',$options);
                    $fields = array('id','product_master_id','description','quantity','actual_price','unit_price','tax','discount','discount_price','subtotal','TM.name','TM.type','TM.value','SaleTaxTran.tax_master_id','SaleTaxTran.id','SaleTaxTran.tax','SaleTaxTran.total_tax');
                    $joins =  array(
                                    array(
                                        'table' => 'sale_tax_trans',
                                        'alias' => 'SaleTaxTran',
                                        'type' => 'LEFT',
                                        'conditions' => array('SaleTran.product_master_id = SaleTaxTran.product_master_id','SaleTaxTran.status' => 1,'SaleTaxTran.sale_master_id' => $id)
                                    ),
                                    array(
                                        'table' => 'tax_masters',
                                        'alias' => 'TM',
                                        'type' => 'LEFT',
                                        'conditions' => array('SaleTaxTran.tax_master_id = TM.id','TM.status' => 1)
                                    )
                            );
                    if($type == 1) {
                        $fields[] = 'ProductMaster.name';
                        $joins[] = array(
                                        'table' => 'product_masters',
                                        'alias' => 'ProductMaster',
                                        'type' => 'INNER',
                                        'conditions' => array('SaleTran.product_master_id = ProductMaster.id','ProductMaster.status' => 1)
                                    );
                    }
                    $options = array(
                                    'fields' => $fields,
                                    'joins' =>$joins,
                                    'conditions' => array('SaleTran.sale_master_id' => $id,'SaleTran.status' => 1)
                                );
                    $arrProductTranData = array();
                    $arrProductData = $this->SaleTran->find('all',$options);
                    if(!empty($arrProductData)) {
                        $arrTaxGroupData = array();
                        if($type === 0) {
                            $this->loadModel('ProductMaster');
                            $arrProductId = Hash::combine($arrProductData,'{n}.SaleTran.product_master_id','{n}.SaleTran.product_master_id');
                            $options = array(
                                            'fields' => array('id','name','TGM.name'),
                                            'joins' => array(
                                                array(
                                                    'table' => 'tax_group_masters',
                                                    'alias' => 'TGM',
                                                    'type' => 'LEFT',
                                                    'conditions' => array('ProductMaster.tax_group_master_id = TGM.id','TGM.status' => 1)
                                                )
                                            ),
                                            'conditions' => array('ProductMaster.status' => 1,'ProductMaster.id' => $arrProductId)
                                        );
                            $arrTaxGroup = $this->ProductMaster->find('all',$options);
                            if(!empty($arrTaxGroup)) {
                                foreach($arrTaxGroup as $key => $data) {
                                    $arrTaxGroupData[$data['ProductMaster']['id']]['name'] = $data['ProductMaster']['name'];
                                    $arrTaxGroupData[$data['ProductMaster']['id']]['tax_name'] = (!empty($data['TGM']['name'])) ? $data['TGM']['name'] : '';
                                }
                            }
                        }
                        
                        foreach($arrProductData as $key => $product) {
                            $productId = $product['SaleTran']['product_master_id'];
                            $arrProductTranData[$productId]['product_master_id'] = (int) $productId;
                            $arrProductTranData[$productId]['sale_tran_id'] = (int) $product['SaleTran']['id'];
                            $arrProductTranData[$productId]['description'] = $product['SaleTran']['description'];
                            $arrProductTranData[$productId]['quantity'] = (int) $product['SaleTran']['quantity'];
                            $arrProductTranData[$productId]['item_price'] = (float) $product['SaleTran']['actual_price'];
                            $arrProductTranData[$productId]['price'] = (float) $product['SaleTran']['unit_price'];
                            $arrProductTranData[$productId]['totalTax'] = (float) $product['SaleTran']['tax'];
                            $arrProductTranData[$productId]['discount'] = (float) $product['SaleTran']['discount'];
                            $arrProductTranData[$productId]['discountAmount'] = (float) $product['SaleTran']['discount_price'];
                            $arrProductTranData[$productId]['total'] = (float) $product['SaleTran']['subtotal'];
                            $stockQty = intval($this->AppUtilities->getProductStockQty($productId) + $product['SaleTran']['quantity']);
                            $arrProductTranData[$productId]['stockQty'] = $stockQty;
                            $arrProductTranData[$productId]['qtyColor'] = 'green';
                            if(isset($arrTaxGroupData[$productId])) {
                                $arrProductTranData[$productId]['tax_name'] = $arrTaxGroupData[$productId]['tax_name'];
                                $arrProductTranData[$productId]['pname'] = $arrTaxGroupData[$productId]['name'];
                            } else {
                                $arrProductTranData[$productId]['tax_name'] = '';
                            }

                            if(isset($product['ProductMaster']['name'])) {
                                $arrProductTranData[$productId]['pname'] = $product['ProductMaster']['name'];
                            }
                            if(!empty($product['SaleTaxTran']['id'])) {
                                $arrProductTax = array();
                                $arrProductTax['id'] =(int) $product['SaleTaxTran']['tax_master_id'];
                                $arrProductTax['sale_tax_tran_id'] =(int) $product['SaleTaxTran']['id'];
                                $arrProductTax['name'] = $product['TM']['name'];
                                $arrProductTax['type'] = (int) $product['TM']['type'];
                                $arrProductTax['value'] = (float) $product['TM']['value'];
                                $arrProductTax['price'] =   (float) $product['SaleTran']['unit_price'];
                                $arrProductTax['tax_amount'] = (float) $product['SaleTaxTran']['tax'];
                                $arrProductTranData[$productId]['tax_detail'][] = $arrProductTax; 
                            }
                        }
                    }
                
                    #pr($arrProductTranData);exit;
                    if(count($arrRecords) > 0) {
                        $statusCode = 200;
                        $arrProductTranData = array_values($arrProductTranData);
                        $arrRecords['SaleMaster']['invoice_date'] = $this->AppUtilities->date_format($arrRecords['SaleMaster']['invoice_date'],'dd-mm-yyyy');
                        if($type === 1) {
                            $arrRecords['SaleMaster']['inwords'] = $this->AppUtilities->getNumberFormat($arrRecords['SaleMaster']['amount']);
                            $arrRecords['SaleMaster']['customer_name'] = $arrRecords['CustomerMaster']['name'];
                            $arrRecords['SaleMaster']['mobile_no'] = $arrRecords['CustomerMaster']['mobile_no'];
                            $arrRecords['SaleMaster']['email_id'] = $arrRecords['CustomerMaster']['email_id'];
                            $arrRecords['SaleMaster']['address'] = $arrRecords['CustomerMaster']['address'];
                        }
                        unset($this->request->data);
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['SaleMaster'],'product' => $arrProductTranData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete_product($id = 0,$saleTranId = 0) {
        $statusCode = 400;
        try {
            if(!empty($id) && !empty($saleTranId)) {
                $options = array('fields' => array('id','product_master_id'),'conditions' => array('id' =>$saleTranId,'sale_master_id' => $id,'status' => 1));
                $arrProduct = $this->SaleTran->find('first',$options);
                if(!empty($arrProduct)) {
                    $productId = $arrProduct['SaleTran']['product_master_id'];
                    $dataSource = $this->SaleMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $updateFields['SaleTran.status'] = 0;
                        $updateFields['SaleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleTran.id'] = $saleTranId;
                        $updateParams['SaleTran.sale_master_id'] = $id;
                        $this->SaleTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams,$options);

                        $updateFields['SaleTaxTran.status'] = 0;
                        $updateFields['SaleTaxTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleTaxTran.product_master_id'] = $productId;
                        $updateParams['SaleTaxTran.sale_master_id'] = $id;
                        $this->SaleTaxTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $updateFields['ProductConsumptionTran.status'] = 0;
                        $updateFields['ProductConsumptionTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ProductConsumptionTran.product_master_id'] = $productId;
                        $updateParams['ProductConsumptionTran.sale_master_id'] = $id;
                        $this->ProductConsumptionTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $options = array('fields' => array('SUM(total) AS total'),'conditions' => array('sale_master_id' => $id,'status' => 1));
                        $saleTran = $this->SaleTran->find('first',$options);
                        $total = $saleTran[0]['total'];
                        $updateFields['SaleMaster.amount'] = $total;
                        $updateFields['SaleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleMaster.id'] = $id;
                        $this->SaleMaster->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $dataSource->commit();
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                }
                $statusCode = 200;
                $response = array('status' => 1,'message' => __('PRODUCT_DELETED',true));
            } else {
                $statusCode = 200;
                $response = array('status' => 1,'message' => __('PRODUCT_DELETED',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $dataSource = $this->SaleMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['SaleMaster.status'] = 0;
                        $updateFields['SaleMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleMaster.id'] = $this->request->data['id'];
                        $this->SaleMaster->updateAll($updateFields,$updateParams);

                        unset($updateFields,$updateParams);
                        $updateFields['SaleTran.status'] = 0;
                        $updateFields['SaleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleTran.sale_master_id'] = $this->request->data['id'];
                        $this->SaleTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $updateFields['SaleTaxTran.status'] = 0;
                        $updateFields['SaleTaxTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['SaleTaxTran.sale_master_id'] = $this->request->data['id'];
                        $this->SaleTaxTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $updateFields['ProductConsumptionTran.status'] = 0;
                        $updateFields['ProductConsumptionTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['ProductConsumptionTran.sale_master_id'] = $this->request->data['id'];
                        $this->ProductConsumptionTran->updateAll($updateFields,$updateParams);
                        unset($updateFields,$updateParams);

                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export($exporType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                $exportOptions = array(
                                    'fields' => array('id','unique_no','invoice_date','amount','SM.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'supplier_masters',
                                            'alias' => 'SM',
                                            'type' => 'LEFT',
                                            'conditions' => array('SaleMaster.supplier_master_id = SM.id','SM.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('SaleMaster.status' => 1),
                                    'order' => 'SaleMaster.unique_no ASC',
                                    'recursive' => -1
                                );
                $arrExportData = $this->SaleMaster->find('all',$exportOptions);
                if(count($arrExportData) > 0) {
                    $statusCode = 200;
                    $counter = 0;
                    $arrTableColumnValues = array();
                    foreach($arrExportData as $key => $exportDetails) {
                        ++$counter;
                        $arrTableColumnValues[$key]['sno'] = $counter.'.';
                        $arrTableColumnValues[$key]['unique_no'] = $exportDetails['SaleMaster']['unique_no'];
                        $arrTableColumnValues[$key]['date'] = $this->AppUtilities->date_format($exportDetails['SaleMaster']['invoice_date'],'dd-mm-yyyy');
                        $arrTableColumnValues[$key]['amount'] = $exportDetails['SaleMaster']['amount'];
                        $arrTableColumnValues[$key]['supplier'] = $exportDetails['SM']['name'];
                    }
                    $headers = array('sno'=>'S.No','unique_no'=>'Invoice No','date'=>'Date','amount'=>'Amount','supplier' => 'Supplier');
                    $response = array('status' => 1,'date' => date('Y-m-d-H:i:s'),'message' => __('RECORD_FETCHED',true),'data' => $arrTableColumnValues,'header' => $headers);
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>