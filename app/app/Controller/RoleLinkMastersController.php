<?php
App::uses('AppController','Controller');
class RoleLinkMastersController extends AppController {
    public $name = 'RoleLinkMasters';
    public $layout = false;
    public $uses = array('RoleLinkMaster','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $conditions = array('RoleLinkMaster.status' => 1,'RoleLinkMaster.is_show' => 1);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['RoleLinkMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['parent_id']) && !empty($this->request->data['parent_id'])) {
                    $conditions['RoleLinkMaster.parent_id'] = trim($this->request->data['parent_id']);
                }
                
                $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                $totalRecords = $this->RoleLinkMaster->find('count',$tableCountOptions);
                $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                $start = ($page - 1) * $length;
                $end = ($start + $length);
                $end = ($end > $totalRecords) ? $totalRecords : $end;
                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('RoleLinkMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('RoleLinkMaster.state '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('RoleLinkMaster.module_name '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('RoleLinkMaster.module_name '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ParentRoleLinkMaster.name '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('RoleLinkMaster.name ASC');
                }

                $tableOptions = array(
                                    'fields' => array('id','name','state','module_name','ParentRoleLinkMaster.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'role_link_masters',
                                            'alias' => 'ParentRoleLinkMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('RoleLinkMaster.parent_id = ParentRoleLinkMaster.id','ParentRoleLinkMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'limit' => $length,
                                    'offset' => $start,
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                $arrTableData = $this->RoleLinkMaster->find('all',$tableOptions);
                if($totalRecords > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['RoleLinkMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['RoleLinkMaster']['name'];
                        $records[$key]['state'] = $tableDetails['RoleLinkMaster']['state'];
                        $records[$key]['module'] = $tableDetails['RoleLinkMaster']['module_name'];
                        $records[$key]['parent'] = $tableDetails['ParentRoleLinkMaster']['name'];
                        $records[$key]['is_exists'] = 0;
                    }
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    $statusCode = 200;
                } else {
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['RoleLinkMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['RoleLinkMaster']['id']) && !empty($arrSaveRecords['RoleLinkMaster']['id'])) {
                    $arrSaveRecords['RoleLinkMaster']['id'] = $this->decryption($arrSaveRecords['RoleLinkMaster']['id']);
                }
    
                if(!isset($arrSaveRecords['RoleLinkMaster']['is_link'])) {
                    $arrSaveRecords['RoleLinkMaster']['is_link'] = 0;
                    $arrSaveRecords['RoleLinkMaster']['module_name'] = '';
                    $arrSaveRecords['RoleLinkMaster']['view'] = '';
                    $arrSaveRecords['RoleLinkMaster']['controller'] = '';
                    $arrSaveRecords['RoleLinkMaster']['service'] = '';
                    $arrSaveRecords['RoleLinkMaster']['action'] = json_encode(array());
                    $arrSaveRecords['RoleLinkMaster']['base_files'] = '';
                    $arrSaveRecords['RoleLinkMaster']['folder_name'] = '';
                } else {
                    $arrSaveRecords['RoleLinkMaster']['is_link'] = 1;
                    if(isset($arrSaveRecords['RoleLinkMaster']['router']) && count($arrSaveRecords['RoleLinkMaster']['router']) > 0) {
                        $arrRouterData = array();
                        $count = 0;
                        foreach($arrSaveRecords['RoleLinkMaster']['router'] as $key => $router) {
                            $arrRouterData[$count]['route'] = $router;
                            $arrRouterData[$count]['param'] = isset($arrSaveRecords['RoleLinkMaster']['param'][$key]) ? $arrSaveRecords['RoleLinkMaster']['param'][$key] : 0;
                            if(isset($arrSaveRecords['RoleLinkMaster']['files'][$key]) && count($arrSaveRecords['RoleLinkMaster']['files'][$key]) > 0) {
                                $arrRouterData[$count]['files'] = $this->AppUtilities->arrangeArray($arrSaveRecords['RoleLinkMaster']['files'][$key]);
                            }
                            ++$count;
                        }
                        unset($arrSaveRecords['RoleLinkMaster']['router']);
                        unset($arrSaveRecords['RoleLinkMaster']['param']);
                        unset($arrSaveRecords['RoleLinkMaster']['files']);
                        $arrSaveRecords['RoleLinkMaster']['action'] = json_encode($arrRouterData,JSON_UNESCAPED_SLASHES);
                    } else {
                        $arrSaveRecords['RoleLinkMaster']['action'] = json_encode(array());
                    }
                }
    
                if(isset($arrSaveRecords['RoleLinkMaster']['base_files']) && !empty($arrSaveRecords['RoleLinkMaster']['base_files'])) {
                    $arrSaveRecords['RoleLinkMaster']['base_files'] = json_encode($this->AppUtilities->arrangeArray($arrSaveRecords['RoleLinkMaster']['base_files']),JSON_UNESCAPED_SLASHES);
                }
    
                #pr($arrSaveRecords);exit;
                $this->RoleLinkMaster->set($arrSaveRecords);
                if($this->RoleLinkMaster->validates()) {
                    #pr($arrSaveRecords);exit;
                    $dataSource = $this->RoleLinkMaster->getDataSource();
                    $fieldList = array('id','name','state','module_name','folder_name','view','controller','parent_id','is_link','order_no','base_files','action','service','icon');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['RoleLinkMaster']['id']) || empty($arrSaveRecords['RoleLinkMaster']['id'])) {
                            $this->RoleLinkMaster->create();
                        }
    
                        if(!$this->RoleLinkMaster->save($arrSaveRecords,array('validate' => false,'fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['RoleLinkMaster']['id']) && !empty($arrSaveRecords['RoleLinkMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => 'role_link_masters','method' => 'save','request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->RoleLinkMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','state','module_name','folder_name','view','controller','service','parent_id','icon','is_link','base_files','action','order_no','ParentRoleLinkMaster.name AS parent'),
                            'joins' => array(
                                array(
                                    'table' => 'role_link_masters',
                                    'alias' => 'ParentRoleLinkMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('RoleLinkMaster.parent_id = ParentRoleLinkMaster.id','ParentRoleLinkMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('RoleLinkMaster.status' => 1,'RoleLinkMaster.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->RoleLinkMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $records = array_merge($arrRecords['RoleLinkMaster'],$arrRecords['ParentRoleLinkMaster']);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $records);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->RoleLinkMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['RoleLinkMaster.status'] = 0;
                        $updateFields['RoleLinkMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['RoleLinkMaster.id'] = $this->request->data['id'];
                        if(!$this->RoleLinkMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function build_recursive_parent($menus,$pname ='') {
        $arrMenuData = array();
        if(is_array($menus) && count($menus)) {
            foreach($menus as $key => $data) {
                if(count($data['children']) > 0) {
                    $arrMenuData[$data['RoleLinkMaster']['id']]['id'] =  $data['RoleLinkMaster']['id'];
                    $arrMenuData[$data['RoleLinkMaster']['id']]['name'] = $pname. " >> ".$data['RoleLinkMaster']['name'];
                    $pname = $pname. " >> ".$data['RoleLinkMaster']['name'];
                    $childMenuData = $this->build_recursive_parent($data['children'],$pname);
                    $arrMenuData = array_merge($arrMenuData,$childMenuData);
                } else {
                    $arrMenuData[$data['RoleLinkMaster']['id']]['id'] =  $data['RoleLinkMaster']['id'];
                    $arrMenuData[$data['RoleLinkMaster']['id']]['name'] = $pname." >> ".$data['RoleLinkMaster']['name'];
                }
            }
        }
        return $arrMenuData;
    }

    public function parent_role_link() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array(
                    'fields'=>array('id','name','parent_id'),
                    'conditions'=>array('status' => 1,'is_show' => 1),
                    'order'=>array('order_no','parent_id')
                );
                $arrRoleLinkData = $this->RoleLinkMaster->find('threaded',$options);
                if(count($arrRoleLinkData) > 0) {
                    $arrMenuData = array();
                    foreach($arrRoleLinkData as $key => $arrRoleMenu) {
                        if(count($arrRoleMenu['children']) > 0) {
                            $arrMenuData[$arrRoleMenu['RoleLinkMaster']['id']]['id'] =  $arrRoleMenu['RoleLinkMaster']['id'];
                            $arrMenuData[$arrRoleMenu['RoleLinkMaster']['id']]['name'] = $arrRoleMenu['RoleLinkMaster']['name'];
                            $pname = $arrRoleMenu['RoleLinkMaster']['name'];
                            $mapData = $this->build_recursive_parent($arrRoleMenu['children'],$pname);
                            $arrMenuData = array_merge($arrMenuData,$mapData);
                        } else {
                            $arrMenuData[$arrRoleMenu['RoleLinkMaster']['id']]['id'] =  $arrRoleMenu['RoleLinkMaster']['id'];
                            $arrMenuData[$arrRoleMenu['RoleLinkMaster']['id']]['name'] = $arrRoleMenu['RoleLinkMaster']['name'];
                        }
                    }
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => array_values($arrMenuData));
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_rolelink_menu() {
        $response = array('status' => 0,'message' => 'Invalid Response');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $response = $this->AppUtilities->save_links();
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function order() {
        $response = array('status' => 0,'message' => 'Invalid Response');
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $options = array(
                                'fields'=>array('id','name','icon','order_no','parent_id'),
                                'conditions'=> array('status' => 1),
                                'group'=>'RoleLinkMaster.id',
                                'order'=>array('RoleLinkMaster.parent_id','IFNULL(RoleLinkMaster.order_no,9999)')
                            );
                $arrRoleLinkData = $this->RoleLinkMaster->find('threaded',$options);
                if(count($arrRoleLinkData) > 0) {
                    $statusCode = 200;
                    $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRoleLinkData);
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_order() {
        $response = array('status' => 0,'message' => 'Invalid Response');
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['role_master_id']) && count($this->request->data['role_master_id']) > 0) {
                    $dataSource = $this->RoleLinkMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        #pr($this->request->data);exit;
                        $arrRoleLinkSaveData = array();
                        $count = 0;
                        foreach($this->request->data['role_master_id'] as $key => $roleMasterId) {
                            $arrRoleLinkSaveData[$count]['RoleLinkMaster']['id'] = $roleMasterId;
                            $arrRoleLinkSaveData[$count]['RoleLinkMaster']['icon'] = isset($this->request->data['icon'][$key]) ? $this->request->data['icon'][$key] : '';
                            $arrRoleLinkSaveData[$count]['RoleLinkMaster']['order_no'] = isset($this->request->data['order'][$key]) ? $this->request->data['order'][$key] : 0;
                            ++$count;
                        }
                        #pr($arrRoleLinkSaveData);exit;
                        $this->RoleLinkMaster->saveAll($arrRoleLinkSaveData,array('validate' => false));
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('Rink order is saved successfully',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function export() {
        $response = array('status' => 0,'message' => 'Invalid Response');
        $statusCode = 400;
        if($this->request->is('get')) {
            $exportOptions = array(
                                'fields' => array('id','name','state','module_name','ParentRoleLinkMaster.name'),
                                'joins' => array(
                                    array(
                                        'table' => 'role_link_masters',
                                        'alias' => 'ParentRoleLinkMaster',
                                        'type' => 'LEFT',
                                        'conditions' => array('RoleLinkMaster.parent_id = ParentRoleLinkMaster.id','ParentRoleLinkMaster.status' => 1)
                                    )
                                ),
                                'conditions' => array('RoleLinkMaster.status' => 1),
                                'recursive' => -1
                            );
            $arrExportData = $this->RoleLinkMaster->find('all',$exportOptions);
            if(count($arrExportData) > 0) {
                $statusCode = 200;
                $counter = 0;
                $arrTableColumnValues = array();
                foreach($arrExportData as $key => $exportDetails) {
                    ++$counter;
					$arrTableColumnValues[$key]['sno'] = $counter.'.';
					$arrTableColumnValues[$key]['name'] = $exportDetails['RoleLinkMaster']['name'];
                    $arrTableColumnValues[$key]['state'] = $exportDetails['RoleLinkMaster']['state'];
                    $arrTableColumnValues[$key]['module_name'] = $exportDetails['RoleLinkMaster']['module_name'];
					$arrTableColumnValues[$key]['parent'] = $exportDetails['ParentRoleLinkMaster']['name'];
                }
                $response = array('status' => 1,'date' => date('Y-m-d'),'message' => __('RECORD_FETCHED',true),'data' => $arrTableColumnValues);
            } else {
                $response = array('status' => 0,'message' => __('NO_RECORD',true));
            }
        } else {
            $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
