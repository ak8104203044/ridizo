<?php
App::uses('AppController','Controller');
class TaxMastersController extends AppController {
    public $name = 'TaxMasters';
    public $layout = false;
    public $uses = array('TaxMaster','TaxGroupTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : '-1';
                $conditions = array('TaxMaster.status' => 1,'TaxMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['TaxMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }
                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['TaxMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }
                if(isset($this->request->data['type']) && !empty($this->request->data['type'])) {
                    $conditions['TaxMaster.type'] = trim($this->request->data['type']);
                }
                if(isset($this->request->data['value']) && !empty($this->request->data['value'])) {
                    $conditions['TaxMaster.value LIKE'] = '%'.trim($this->request->data['value']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('TaxMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('TaxMaster.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('TaxMaster.type '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('TaxMaster.value '.$sortyType);
                                break;
                        default:
                                $orderBy = array('TaxMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('TaxMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1 );
                    $totalRecords = $this->TaxMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','type','value','order_no','COUNT(TGT.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'tax_group_trans',
                                            'alias' => 'TGT',
                                            'type' => 'LEFT',
                                            'conditions' => array('TaxMaster.id = TGT.tax_master_id','TGT.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('TaxMaster.id'),
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->TaxMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['TaxMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['TaxMaster']['name'];
                        $records[$key]['code'] = $tableDetails['TaxMaster']['code'];
                        $records[$key]['type'] = (intval($tableDetails['TaxMaster']['type']) === 1) ? 'Percentage':'Fixed';
                        $records[$key]['value'] = $tableDetails['TaxMaster']['value'];
                        $records[$key]['order_no'] = $tableDetails['TaxMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['TaxMaster']['order_no'] > $maxOrderNo) ? $tableDetails['TaxMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','type'=>'Type','value' => 'Value','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['TaxMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['TaxMaster']['id']) && !empty($arrSaveRecords['TaxMaster']['id'])) {
                    $arrSaveRecords['TaxMaster']['id'] = $this->decryption($arrSaveRecords['TaxMaster']['id']);
                }
                $arrSaveRecords['TaxMaster']['branch_master_id'] = isset($arrSaveRecords['TaxMaster']['branch_master_id']) ? $arrSaveRecords['TaxMaster']['branch_master_id'] : '';
                $this->TaxMaster->set($arrSaveRecords);
                if($this->TaxMaster->validates()) {
                    $dataSource = $this->TaxMaster->getDataSource();
                    $fieldList = array('id','name','code','type','value','branch_master_id','description','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['TaxMaster']['id']) || empty($arrSaveRecords['TaxMaster']['id'])) {
                            $this->TaxMaster->create();
                        }
                        if(!$this->TaxMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['TaxMaster']['id']) && !empty($arrSaveRecords['TaxMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->TaxMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
       try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','type','value','order_no'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->TaxMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['TaxMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
       } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
       }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('tax_master_id'),'conditions' => array('status'=> 1,'tax_master_id' => $this->request->data['id']));
                    $arrRecords = $this->TaxGroupTran->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->TaxMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['TaxMaster.status'] = 0;
                            $updateFields['TaxMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['TaxMaster.id'] = $arrDeleteRecords;
                            if(!$this->TaxMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $statusCode = 200;
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
