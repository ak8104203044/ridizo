<?php
App::uses('AppController','Controller');

class ExpenseReportController extends AppController {
    public $name = 'ExpenseReport';
    public $uses = array('ErrorLog','ExpenseMaster');
    public $components = array('AppUtilities');

    public function expense_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);
                $arrFieldData = $arrHeaderData = array();
                if(isset($this->request->data['expense']) && !empty($this->request->data['expense'])) {
                    $fcount = $count = 0;
                    if(isset($this->request->data['expense'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'voucher_no';
                        $arrFieldData[$count]['caption'] = 'Voucher No';
                        $arrFieldData[$count]['order'] = $this->request->data['expense'][0]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['expense'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'voucher_date';
                        $arrFieldData[$count]['caption'] = 'Voucher Date';
                        $arrFieldData[$count]['order'] = $this->request->data['expense'][0]['order'];
                        ++$count;
                    }

                    ++$fcount;
                    if(isset($this->request->data['expense'][$fcount]['field'])) {
                        $arrFieldData[$count]['fieldName'] = 'expense_type';
                        $arrFieldData[$count]['caption'] = 'Expense Type';
                        $arrFieldData[$count]['order'] = $this->request->data['expense'][0]['order'];
                        ++$count;
                    }
                }

                if(count($arrFieldData) > 0) {
                    uasort($arrFieldData,function($a,$b){
                        if($a['order'] == 0){
                            return 1;
                        }
                        if($b['order'] == 0){
                            return -1;
                        }
                        return ($a['order'] < $b['order']) ? -1 : 1;
                    });
                    foreach($arrFieldData as $key => $header) {
                        $arrHeaderData[] = $header;
                    }
                    $conditions = array('ExpenseMaster.status' => 1);
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                        $conditions['ExpenseMaster.voucher_date >='] = $fromDate;
                    }

                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                        $conditions['ExpenseMaster.voucher_date <='] = $endDate;
                    }

                    if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                        $conditions['ExpenseMaster.branch_master_id'] = $this->request->data['branch_master_id'];
                    }

                    if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                        $conditions['ExpenseMaster.company_year_master_id'] = $this->request->data['company_year_master_id'];
                    }
                    
                    if(isset($this->request->data['expense_head_master_id']) && !empty($this->request->data['expense_head_master_id'])) {
                        $conditions['ExpenseTran.expense_head_master_id'] = $this->request->data['expense_head_master_id'];
                    }

                    if(isset($this->request->data['expense_type']) && !empty($this->request->data['expense_type'])) {
                        $conditions['ExpenseMaster.expense_type'] = $this->request->data['expense_type'];
                    }

                    $orderBy =  array();
                    if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                        $sortBy = (int) $this->request->data['sort_by'];
                        $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                        switch($sortBy) {
                            case 1:
                                    $orderBy = array('order' => 'ExpenseMaster.voucher_no '.$sortyType);
                                    break;
                            case 2:
                                    $orderBy = array('order' => 'ExpenseMaster.voucher_date '.$sortyType);
                                    break;
                            case 3:
                                    $orderBy = array('order' => 'ExpenseMaster.expense_type '.$sortyType);
                                    break;
                            default:
                                    $orderBy = array('order' => 'ExpenseMaster.voucher_no '.$sortyType);
                                    break;
                        }
                    }
                    $options = array(
                                    'fields' => array(
                                            'ExpenseMaster.id','ExpenseMaster.voucher_no','DATE_FORMAT(ExpenseMaster.voucher_date,"%d-%m-%Y") as voucher_date','ExpenseMaster.expense_type','ExpenseMaster.amount',
                                            'ExpenseHeadMaster.id','ExpenseHeadMaster.name','ExpenseTran.price'
                                        ),
                                    'joins' => array(
                                        array(
                                            'table' => 'expense_trans',
                                            'alias' => 'ExpenseTran',
                                            'type' => 'INNER',
                                            'conditions' => array('ExpenseMaster.id = ExpenseTran.expense_master_id','ExpenseTran.status' => 1)
                                        ),
                                        array(
                                            'table' => 'expense_head_masters',
                                            'alias' => 'ExpenseHeadMaster',
                                            'type' => 'INNER',
                                            'conditions' => array('ExpenseTran.expense_head_master_id = ExpenseHeadMaster.id','ExpenseHeadMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'recursive' => -1
                                );
                    $options = array_merge($options,$orderBy);
                    $arrRecords = $this->ExpenseMaster->find('all',$options);
                    if(count($arrRecords) > 0) {
                        $arrContentData = $arrProductDetail = array();
                        foreach($arrRecords as $key => $data) {
                            $expenseId = $data['ExpenseMaster']['id'];
                            $expenseHeadId = $data['ExpenseHeadMaster']['id'];
                            $arrContentData[$expenseId]['total'] = $data['ExpenseMaster']['amount'];
                            $arrContentData[$expenseId]['voucher_no'] = $data['ExpenseMaster']['voucher_no'];
                            $arrContentData[$expenseId]['expense_type'] = ($data['ExpenseMaster']['expense_type'] == 1) ? 'Other' : 'Vehicle';
                            $arrContentData[$expenseId]['voucher_date'] = $data[0]['voucher_date'];
                            $arrContentData[$expenseId]['detail'][$expenseHeadId] = $data['ExpenseTran']['price'];
                            $arrProductDetail[$expenseHeadId]['id'] = $expenseHeadId;
                            $arrProductDetail[$expenseHeadId]['name'] = $data['ExpenseHeadMaster']['name'];
                        }
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => array_values($arrContentData),'expense' => array_values($arrProductDetail));
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
