<?php
App::import('Model', 'ConnectionManager');
App::uses('AppController','Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class TableSchemaController extends AppController {
    public $name = 'DefaultSchema';
    public $uses = array('User','TypeMaster','RoleMaster','RoleLinkMaster');
    public $components = array('UserManagement','AppUtilities');
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function save_recursive_menus($insertID,$children) {
        if(count($children) > 0) {
            foreach($children as $key => $links) {
                $links['parent_id'] = $insertID;
                if(isset($links['children']) && count($links['children']) > 0) {
                    $this->RoleLinkMaster->create();
                    $children = $links['children'];
                    unset($links['children']);
                    $arrSaveData['RoleLinkMaster'] =  $links;
                    $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                    $getLastID = $this->RoleLinkMaster->getInsertID();
                    $this->save_recursive_menus($getLastID,$children);
                } else {
                    $this->RoleLinkMaster->create();
                    $arrSaveData['RoleLinkMaster'] =  $links;
                    $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                }
            }
        }
    }

    public function index() {
        ini_set('max_execution_time',10000000000000);
        try {
            $defaultSql = file_get_contents('../Config/default_schema.sql');
            $db = ConnectionManager::getDataSource('default');
            if(!$db->query($defaultSql,false)) {
                echo "error";exit;
            }
            
            $insertEmployeeView = "DROP VIEW IF EXISTS employees;
                                    CREATE VIEW employees AS
                                    SELECT employee_masters.*,designation_masters.`name` AS `designation`,
                                    department_masters.`name` AS `department`,
                                    country_masters.`name` AS `country`,
                                    state_masters.`name` AS `state`,
                                    city_masters.`name` AS `city`,
                                    employee_trans.`designation_master_id`,
                                    department_masters.id AS department_master_id,
                                    CONCAT_WS(' ',title_masters.`name`,employee_masters.first_name,employee_masters.middle_name,employee_masters.last_name) AS employee_name,
                                    users.`username`
                                    FROM employee_masters
                                    INNER JOIN employee_trans ON(employee_masters.`id` = employee_trans.`employee_master_id`)
                                    INNER JOIN designation_masters ON(employee_trans.`designation_master_id` = designation_masters.`id` AND designation_masters.`status` = 1)
                                    INNER JOIN department_masters ON(department_masters.`id` = designation_masters.`department_master_id` AND department_masters.`status` = 1)
                                    INNER JOIN branch_masters ON(employee_masters.`branch_master_id` = branch_masters.`id` AND branch_masters.`status` = 1)
                                    INNER JOIN country_masters ON(employee_masters.`country_master_id` = country_masters.`id` AND country_masters.`status` = 1)
                                    INNER JOIN state_masters ON(employee_masters.`state_master_id` = state_masters.`id` AND state_masters.`status` = 1)
                                    INNER JOIN city_masters ON(employee_masters.`city_master_id` = city_masters.`id` AND city_masters.`status` = 1)
                                    INNER JOIN users ON(employee_masters.`user_id` = users.`id` AND users.`status` = 1)
                                    LEFT JOIN title_masters ON(employee_masters.`title_master_id`=title_masters.`id` AND title_masters.`status` = 1)
                                    WHERE employee_masters.`status` = 1;";
            $this->User->query($insertEmployeeView,false);
            
            $insertVehicleModelView = "DROP VIEW IF EXISTS vehicle_models;
                                    CREATE VIEW vehicle_models AS 
                                    SELECT vehicle_model_masters.*,
                                    manufacturer_masters.`name` AS manufacturer,
                                    vehicle_type_masters.`name` AS vehicle_type,
                                    vehicle_type_masters.`id` AS vehicle_type_id,
                                    manufacturer_masters.`id` AS manufacturer_id
                                    FROM vehicle_model_masters
                                    INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id`=manufacturer_trans.`id`)
                                    INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id`)
                                    INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id`)
                                    WHERE vehicle_model_masters.`status` = 1 AND manufacturer_trans.`status` = 1
                                    AND manufacturer_masters.`status` = 1 AND vehicle_type_masters.`status` = 1";
            $this->User->query($insertVehicleModelView,false);

            $insertRoleRightsView = "DROP VIEW IF EXISTS user_role_rights;
                                    CREATE VIEW user_role_rights AS 
                                    SELECT role_link_masters.`id`,
                                    role_link_operation_rights_trans.`role_master_id`,
                                    role_link_masters.`name`,
                                    role_link_masters.`module_name`,
                                    type_trans.`name` AS `type_name`,
                                    role_link_masters.`parent_id`,role_link_masters.`order_no`
                                    FROM role_link_masters 
                                    INNER JOIN role_link_operation_rights_trans ON(role_link_masters.`id` = role_link_operation_rights_trans.`role_link_master_id`)
                                    INNER JOIN type_trans ON(role_link_operation_rights_trans.`type_tran_id` = type_trans.`id`)
                                    WHERE role_link_masters.`status` = 1
                                    AND role_link_operation_rights_trans.`status` = 1 AND type_trans.`status` = 1";
            $this->User->query($insertRoleRightsView,false);

            $insertVehicleView = "DROP VIEW IF EXISTS vehicles;
                                CREATE VIEW vehicles AS
                                SELECT vehicle_masters.*,
                                vehicle_model_masters.`name` AS vehicle_model_name,
                                manufacturer_masters.`name` AS manufacturer_name,
                                manufacturer_masters.`id` AS manufacturer_id,
                                manufacturer_trans.`id` AS manufacturer_tran_id,
                                vehicle_type_masters.`name` AS vehicle_type_name,
                                vehicle_type_masters.`id` AS vehicle_type_id,
                                variant_masters.`name` AS variant_name,
                                grade_masters.`name` AS `grade`,vehicle_grade_trans.`grade_master_id` AS grade_master_id,
                                CONCAT_WS(' ',title_masters.`name`,`owner_first_name`,owner_middle_name,`owner_last_name`) AS `customer_name`,
                                customers.`name` AS cust_name,customers.`mobile_no` AS cust_mobile_no,customers.`email_id` AS cust_email_id,
                                customers.`residential_address` AS cust_address
                                FROM vehicle_masters
                                LEFT JOIN vehicle_grade_trans ON(vehicle_masters.`id` = vehicle_grade_trans.`vehicle_master_id` AND vehicle_grade_trans.`status` = 1 AND vehicle_grade_trans.`is_active` = 1)
                                LEFT JOIN grade_masters ON(grade_masters.`id` = vehicle_grade_trans.`grade_master_id` AND grade_masters.`status` = 1)
                                INNER JOIN vehicle_model_masters ON(vehicle_masters.`vehicle_model_master_id` = vehicle_model_masters.`id` AND vehicle_model_masters.`status` = 1)
                                INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id` = manufacturer_trans.`id` AND manufacturer_trans.`status` = 1)
                                INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id` AND manufacturer_masters.`status` = 1)
                                INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id` AND vehicle_type_masters.`status` = 1)
                                LEFT JOIN variant_masters ON(vehicle_masters.`variant_master_id` = variant_masters.`id` AND variant_masters.`status` = 1)
                                LEFT JOIN title_masters ON(vehicle_masters.`owner_title_master_id` = title_masters.`id` AND title_masters.`status` = 1)
                                LEFT JOIN customers ON(vehicle_masters.`customer_master_id` = customers.`id` AND vehicle_masters.`branch_master_id` = customers.`branch_master_id` AND customers.`status` = 1)
                                WHERE vehicle_masters.`status` = 1;";
            $this->User->query($insertVehicleView,false);

            $insertVehicleJobView = "DROP VIEW IF EXISTS vehicle_jobs;
                                    CREATE VIEW vehicle_jobs AS
                                    SELECT vehicle_job_masters.*,vehicle_masters.`unique_no` AS vehicle_code,vehicle_masters.`vehicle_number`,
                                    vehicle_masters.`chasis_number`,vehicle_masters.`engine_number`,vehicle_model_masters.`name` AS `vehicle_model`,
                                    manufacturer_masters.`name` AS `manufacturer`,vehicle_type_masters.`name` AS `vehicle_type`
                                    FROM vehicle_job_masters
                                    INNER JOIN vehicle_masters ON(vehicle_job_masters.`vehicle_master_id` = vehicle_masters.`id` AND vehicle_masters.`status` = 1)
                                    INNER JOIN vehicle_model_masters ON(vehicle_masters.`vehicle_model_master_id` = vehicle_model_masters.`id` AND vehicle_model_masters.`status` = 1)
                                    INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id` = manufacturer_trans.`id` AND manufacturer_trans.`status` = 1)
                                    INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id` AND manufacturer_masters.`status` = 1)
                                    INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id` AND vehicle_type_masters.`status` = 1)
                                    WHERE vehicle_job_masters.`status` = 1;";
            $this->User->query($insertVehicleJobView,false);
            
            $productStockTranView = "DROP VIEW IF EXISTS product_stock_trans;
                                    CREATE VIEW product_stock_trans AS
                                    (SELECT product_variant_trans.`id`,product_masters.`id` AS product_id,product_masters.`name`,product_masters.`code`,product_masters.`hsn_code`,
                                    product_masters.`tax_group_master_id`,product_variant_trans.`price`,product_variant_stock_trans.`quantity`,
                                    product_masters.`stock_type`,product_masters.`type`,product_variant_stock_trans.`id` AS product_stock_tran_id,product_masters.`branch_master_id`,
                                    product_variant_stock_trans.`date`
                                    FROM product_masters
                                    INNER JOIN product_variant_trans ON(product_masters.`id` = product_variant_trans.`product_master_id`)
                                    INNER JOIN product_variant_stock_trans ON(product_variant_trans.`id` = product_variant_stock_trans.`product_variant_tran_id`)
                                    WHERE product_masters.`status` = 1 AND product_variant_trans.`status` = 1 AND product_variant_stock_trans.`status` = 1
                                    AND product_variant_stock_trans.`parent_id` = 0
                                    ORDER BY product_variant_stock_trans.`date` ASC)
                                    UNION ALL
                                    (SELECT product_variant_trans.`id`,product_masters.`id` AS product_id,product_masters.`name`,product_masters.`code`,product_masters.`hsn_code`,
                                    product_masters.`tax_group_master_id`,product_variant_trans.`price`,product_variant_stock_trans.`quantity`,
                                    product_masters.`stock_type`,product_masters.`type`,product_variant_stock_trans.`id` AS product_stock_tran_id,product_masters.`branch_master_id`,
                                    product_variant_stock_trans.`date`
                                    FROM product_masters
                                    INNER JOIN product_variant_trans ON(product_masters.`id` = product_variant_trans.`product_master_id`)
                                    INNER JOIN product_variant_stock_trans ON(product_variant_trans.`id` = product_variant_stock_trans.`product_variant_tran_id`)
                                    WHERE product_masters.`status` = 1 AND product_variant_trans.`status` = 1 AND product_variant_stock_trans.`status` = 1
                                    AND product_variant_stock_trans.`parent_id` > 0
                                    ORDER BY product_variant_stock_trans.`date` ASC)
                                    UNION ALL
                                    (SELECT product_variant_trans.`id`,product_masters.`id` AS product_id,product_masters.`name`,product_masters.`code`,product_masters.`hsn_code`,
                                    product_masters.`tax_group_master_id`,product_variant_trans.`price`,tmp_product_variant_stock_trans.`quantity`,
                                    product_masters.`stock_type`,product_masters.`type`,tmp_product_variant_stock_trans.`product_variant_stock_tran_id` AS product_stock_tran_id,product_masters.`branch_master_id`,
                                    tmp_product_variant_stock_trans.`created`
                                    FROM product_masters
                                    INNER JOIN product_variant_trans ON(product_masters.`id` = product_variant_trans.`product_master_id`)
                                    INNER JOIN `tmp_product_variant_stock_trans` ON(product_variant_trans.`id` = tmp_product_variant_stock_trans.`product_variant_tran_id`)
                                    WHERE product_masters.`status` = 1 AND product_variant_trans.`status` = 1 AND tmp_product_variant_stock_trans.`status`= 1
                                    ORDER BY tmp_product_variant_stock_trans.`created` ASC);";
            $this->User->query($productStockTranView,false);

            $customerQuery = "DROP VIEW IF EXISTS customers;
                                CREATE VIEW customers AS
                                SELECT customer_masters.id,CONCAT_WS(' ',title_masters.`name`,first_name,middle_name,last_name) AS `name`,
                                first_name,middle_name,last_name,customer_masters.`adhaar_no`,customer_masters.`pancard_no`,
                                customer_masters.`mobile_no`,customer_masters.`email_id`,customer_masters.`residential_address`,
                                customer_masters.`permanent_address`,customer_masters.`status`,customer_masters.`branch_master_id`
                                FROM customer_masters
                                LEFT JOIN title_masters ON(customer_masters.`title_master_id` = title_masters.`id` AND title_masters.`status` = 1)
                                WHERE customer_masters.`status` = 1;";
            $this->User->query($customerQuery,false);

            $supplierQuery = "DROP VIEW IF EXISTS suppliers;
                                CREATE VIEW suppliers AS
                                SELECT supplier_masters.id,CONCAT_WS(' ',title_masters.`name`,first_name,middle_name,last_name) AS `name`,
                                first_name,middle_name,last_name,`mobile_no`,`email_id`,`address`,
                                supplier_masters.`status`,supplier_masters.`branch_master_id`
                                FROM supplier_masters
                                LEFT JOIN title_masters ON(supplier_masters.`title_master_id` = title_masters.`id` AND title_masters.`status` = 1)
                                WHERE supplier_masters.`status` = 1;";
            $this->User->query($supplierQuery,false);

            
            $updateTriggerQuery = "CREATE TRIGGER `after_insert_product` AFTER INSERT ON `product_variant_stock_trans`
                                    FOR EACH ROW BEGIN
                                    SET @product_id := 0;
                                    SET @exists := 0;
                                    SELECT product_master_id INTO @product_id FROM product_variant_trans WHERE id = NEW.product_variant_tran_id;
                                    SELECT IF(COUNT(id) > 0,1,0) INTO @exists FROM product_variant_stock_trans WHERE product_variant_tran_id = NEW.product_variant_tran_id
                                    AND `type` = 2 AND `status` = 1 AND branch_master_id = NEW.branch_master_id;
                                    UPDATE product_masters SET trans_exists = @exists WHERE id = @product_id;
                                    END";
            $this->User->query($updateTriggerQuery,false);
            
            $insertProductTriggerQuery = "CREATE TRIGGER `after_update_product` AFTER UPDATE ON `product_variant_stock_trans`
                                        FOR EACH ROW BEGIN
                                        SET @product_id := 0;
                                        SET @exists := 0;
                                        SELECT product_master_id INTO @product_id FROM product_variant_trans WHERE id = OLD.product_variant_tran_id;
                                        SELECT IF(COUNT(id) > 0,1,0) INTO @exists FROM product_variant_stock_trans WHERE product_variant_tran_id = OLD.product_variant_tran_id
                                        AND `type` = 2 AND `status` = 1 AND branch_master_id = OLD.branch_master_id;
                                        UPDATE product_masters SET trans_exists = @exists WHERE id = @product_id;
                                        END";
            $this->User->query($insertProductTriggerQuery,false);
    
            $arrTypeMasterData[0]['TypeMaster']['name'] = 'User Role';
            $arrTypeMasterData[0]['TypeMaster']['shortname'] = 'User Role';
            $arrTypeMasterData[0]['TypeMaster']['order_no'] = 1;

            $arrTypeMasterData[0]['TypeTran'][0]['name'] = 'Add';
            $arrTypeMasterData[0]['TypeTran'][0]['shortname'] = 'Add';
            $arrTypeMasterData[0]['TypeTran'][0]['role_type'] = 0;
            $arrTypeMasterData[0]['TypeTran'][0]['order_no'] = 1;

            $arrTypeMasterData[0]['TypeTran'][1]['name'] = 'Edit';
            $arrTypeMasterData[0]['TypeTran'][1]['shortname'] = 'Edit';
            $arrTypeMasterData[0]['TypeTran'][1]['role_type'] = 0;
            $arrTypeMasterData[0]['TypeTran'][1]['order_no'] = 2;

            $arrTypeMasterData[0]['TypeTran'][2]['name'] = 'View';
            $arrTypeMasterData[0]['TypeTran'][2]['shortname'] = 'View';
            $arrTypeMasterData[0]['TypeTran'][2]['role_type'] = 0;
            $arrTypeMasterData[0]['TypeTran'][2]['order_no'] = 3;

            $arrTypeMasterData[0]['TypeTran'][3]['name'] = 'Delete';
            $arrTypeMasterData[0]['TypeTran'][3]['shortname'] = 'Delete';
            $arrTypeMasterData[0]['TypeTran'][3]['role_type'] = 0;
            $arrTypeMasterData[0]['TypeTran'][3]['order_no'] = 4;

            $arrTypeMasterData[1]['TypeMaster']['name'] = 'Member Type';
            $arrTypeMasterData[1]['TypeMaster']['shortname'] = 'Member Type';
            $arrTypeMasterData[1]['TypeMaster']['order_no'] = 2;

            $arrTypeMasterData[1]['TypeTran'][0]['name'] = 'Developer';
            $arrTypeMasterData[1]['TypeTran'][0]['shortname'] = 'Developer';
            $arrTypeMasterData[1]['TypeTran'][0]['role_type'] = 1;
            $arrTypeMasterData[1]['TypeTran'][0]['order_no'] = 1;

            $arrTypeMasterData[1]['TypeTran'][1]['name'] = 'Super Admin';
            $arrTypeMasterData[1]['TypeTran'][1]['shortname'] = 'Super Admin';
            $arrTypeMasterData[1]['TypeTran'][1]['role_type'] = 2;
            $arrTypeMasterData[1]['TypeTran'][1]['order_no'] = 2;

            $arrTypeMasterData[1]['TypeTran'][2]['name'] = 'Franchise';
            $arrTypeMasterData[1]['TypeTran'][2]['shortname'] = 'Franchise';
            $arrTypeMasterData[1]['TypeTran'][2]['role_type'] = 3;
            $arrTypeMasterData[1]['TypeTran'][2]['order_no'] = 3;

            $arrTypeMasterData[1]['TypeTran'][3]['name'] = 'Service Center';
            $arrTypeMasterData[1]['TypeTran'][3]['shortname'] = 'Service Center';
            $arrTypeMasterData[1]['TypeTran'][3]['role_type'] = 3;
            $arrTypeMasterData[1]['TypeTran'][3]['order_no'] = 4;

            $arrTypeMasterData[1]['TypeTran'][4]['name'] = 'Employee';
            $arrTypeMasterData[1]['TypeTran'][4]['shortname'] = 'Employee';
            $arrTypeMasterData[1]['TypeTran'][4]['role_type'] = 4;
            $arrTypeMasterData[1]['TypeTran'][4]['order_no'] = 5;

            $arrTypeMasterData[2]['TypeMaster']['name'] = 'Vehicle Expenses';
            $arrTypeMasterData[2]['TypeMaster']['shortname'] = 'Vehicle Expenses';
            $arrTypeMasterData[2]['TypeMaster']['order_no'] = 3;

            $arrTypeMasterData[2]['TypeTran'][0]['name'] = 'Transporation';
            $arrTypeMasterData[2]['TypeTran'][0]['shortname'] = 'Transporation';
            $arrTypeMasterData[2]['TypeTran'][0]['role_type'] = 0;
            $arrTypeMasterData[2]['TypeTran'][0]['order_no'] = 1;

            $arrTypeMasterData[2]['TypeTran'][1]['name'] = 'Service Expenses';
            $arrTypeMasterData[2]['TypeTran'][1]['shortname'] = 'Service Expenses';
            $arrTypeMasterData[2]['TypeTran'][1]['role_type'] = 0;
            $arrTypeMasterData[2]['TypeTran'][1]['order_no'] = 2;

            $arrTypeMasterData[2]['TypeTran'][2]['name'] = 'Other Expenses';
            $arrTypeMasterData[2]['TypeTran'][2]['shortname'] = 'Other Expenses';
            $arrTypeMasterData[2]['TypeTran'][2]['role_type'] = 0;
            $arrTypeMasterData[2]['TypeTran'][2]['order_no'] = 3;

            $arrTypeMasterData[2]['TypeTran'][3]['name'] = 'RTO Expenses';
            $arrTypeMasterData[2]['TypeTran'][3]['shortname'] = 'RTO Expenses';
            $arrTypeMasterData[2]['TypeTran'][3]['role_type'] = 0;
            $arrTypeMasterData[2]['TypeTran'][3]['order_no'] = 4;

            $arrTypeMasterData[2]['TypeTran'][4]['name'] = 'Insurance';
            $arrTypeMasterData[2]['TypeTran'][4]['shortname'] = 'Insurance';
            $arrTypeMasterData[2]['TypeTran'][4]['role_type'] = 0;
            $arrTypeMasterData[2]['TypeTran'][4]['order_no'] = 5;

            $query = "SET FOREIGN_KEY_CHECKS = 0;
                        TRUNCATE TABLE type_masters;
                        TRUNCATE TABLE type_trans;
                        TRUNCATE TABLE role_masters;
                        TRUNCATE TABLE users;
                        TRUNCATE TABLE user_role_trans;
                        TRUNCATE TABLE role_link_masters;
                        TRUNCATE TABLE role_link_operation_rights_trans;
                        SET FOREIGN_KEY_CHECKS = 1;";
            $this->TypeMaster->query($query,false);

            $this->TypeMaster->bindModel(
                array(
                    'hasMany' => array(
                        'TypeTran' => array(
                            'className' => 'TypeTran',
                            'foreignKey' => 'type_master_id',
                            'type' =>'left' 
                        )
                    )
                )
            );
            $this->TypeMaster->saveAll($arrTypeMasterData,array('validate' => false,'deep' => true));
            $arrRoleMasterData = array();
            $arrRoleMasterData[0]['RoleMaster']['name'] = 'Developer';
            $arrRoleMasterData[0]['RoleMaster']['code'] = 'Developer';
            $arrRoleMasterData[0]['RoleMaster']['type_tran_id'] = 5;
            $arrRoleMasterData[0]['RoleMaster']['order_no'] = 1;
            $arrRoleMasterData[0]['RoleMaster']['is_default'] = 1;
            $arrRoleMasterData[0]['RoleMaster']['is_show'] = 0;
            
            $arrRoleMasterData[1]['RoleMaster']['name'] = 'Super Admin';
            $arrRoleMasterData[1]['RoleMaster']['code'] = 'Super Admin';
            $arrRoleMasterData[1]['RoleMaster']['type_tran_id'] = 6;
            $arrRoleMasterData[1]['RoleMaster']['order_no'] = 2;
            $arrRoleMasterData[1]['RoleMaster']['is_default'] = 1;
            $arrRoleMasterData[1]['RoleMaster']['is_show'] = 0;

            $arrRoleMasterData[2]['RoleMaster']['name'] = 'Franchise';
            $arrRoleMasterData[2]['RoleMaster']['code'] = 'Franchise';
            $arrRoleMasterData[2]['RoleMaster']['type_tran_id'] = 7;
            $arrRoleMasterData[2]['RoleMaster']['order_no'] = 3;
            $arrRoleMasterData[2]['RoleMaster']['is_default'] = 1;
            $arrRoleMasterData[2]['RoleMaster']['is_show'] = 1;

            $arrRoleMasterData[3]['RoleMaster']['name'] = 'Service Center';
            $arrRoleMasterData[3]['RoleMaster']['code'] = 'Service Center';
            $arrRoleMasterData[3]['RoleMaster']['type_tran_id'] = 8;
            $arrRoleMasterData[3]['RoleMaster']['order_no'] = 4;
            $arrRoleMasterData[3]['RoleMaster']['is_default'] = 1;
            $arrRoleMasterData[3]['RoleMaster']['is_show'] = 1;

            $this->RoleMaster->saveAll($arrRoleMasterData,array('validate' => false));
            unset($arrRoleMasterData);

            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $arrUserData = array();
            $arrUserData[0]['User']['username'] = 'developer';
            $arrUserData[0]['User']['password'] = $passwordHasher->hash('developer');
            $arrUserData[0]['User']['fullname'] = 'Developer';
            $arrUserData[0]['User']['user_type'] = 1;
            $arrUserData[0]['User']['is_active'] = 1;
            $arrUserData[0]['UserRoleTran'][0]['role_master_id'] = 1;

            $arrUserData[1]['User']['username'] = 'superadmin';
            $arrUserData[1]['User']['password'] = $passwordHasher->hash('123456');
            $arrUserData[1]['User']['fullname'] = 'Super Admin';
            $arrUserData[1]['User']['user_type'] = 2;
            $arrUserData[1]['User']['is_active'] = 1;
            $arrUserData[1]['UserRoleTran'][0]['role_master_id'] = 2;

            $this->User->bindModel(
                array(
                    'hasMany' => array(
                        'UserRoleTran' => array(
                            'className' => 'UserRoleTran',
                            'foreignKey' => 'user_id',
                            'type' =>'left' 
                        )
                    )
                )
            );
            $this->User->saveAll($arrUserData,array('validate' => false,'deep' => true));
            $arrRoleData = $this->build_role_link();
            if(count($arrRoleData) > 0) {
                foreach($arrRoleData as $key => $links) {
                    if(isset($links['children']) && count($links['children']) > 0) {
                        $this->RoleLinkMaster->create();
                        $children = $links['children'];
                        unset($links['children']);
                        $arrSaveData['RoleLinkMaster'] =  $links;
                        $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                        $getLastID = $this->RoleLinkMaster->getInsertID();
                        $this->save_recursive_menus($getLastID,$children);
                    } else {
                        $this->RoleLinkMaster->create();
                        $arrSaveData['RoleLinkMaster'] =  $links;
                        $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                    }
                }
            }

            $insertRoleLinkOperationQuery ="INSERT INTO role_link_operation_rights_trans(role_master_id,role_link_master_id,type_tran_id,created,modified)
                                            SELECT 1,role_link_masters.`id`,type_trans.`id`,NOW(),NOW() FROM role_link_masters,
                                            type_trans  WHERE type_trans.type_master_id = 1";
            $this->User->query($insertRoleLinkOperationQuery,false);
            $insertRoleLinkOperationQuery = "INSERT INTO role_link_operation_rights_trans(role_master_id,role_link_master_id,type_tran_id,created,modified)
                                            SELECT 2,role_link_masters.`id`,type_trans.`id`,NOW(),NOW() FROM role_link_masters,
                                            type_trans  WHERE type_trans.type_master_id = 1 AND role_link_masters.`is_show` = 1;";
            $this->User->query($insertRoleLinkOperationQuery,false);
            echo 'Operation executed successfully!.';
            exit;
        } catch(Exception $e) {
            pr($e);exit;
        }
    }

    public function execute_role() {
        $this->UserManagement->buildRoleRightsMenu(1);
        $this->UserManagement->buildRoleRightsMenu(2);
        echo 'Role Operation Excuted Successfully!.';exit;
    }

    public function save_role_links() {
        $response = $this->AppUtilities->save_links();
        echo json_encode($response);exit;
    }

    public function save_menus() {
        echo "processing.......<br>";
        $arrRoleData = $this->build_role_link();
        if(count($arrRoleData) > 0) {
            $query = "SET FOREIGN_KEY_CHECKS = 0;
                    TRUNCATE TABLE role_link_masters;
                    TRUNCATE TABLE role_link_operation_rights_trans;
                    SET FOREIGN_KEY_CHECKS = 1;";
            $this->User->query($query,false);
            foreach($arrRoleData as $key => $links) {
                if(isset($links['children']) && count($links['children']) > 0) {
                    $this->RoleLinkMaster->create();
                    $children = $links['children'];
                    unset($links['children']);
                    $arrSaveData['RoleLinkMaster'] =  $links;
                    $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                    $getLastID = $this->RoleLinkMaster->getInsertID();
                    $this->save_recursive_menus($getLastID,$children);
                } else {
                    $this->RoleLinkMaster->create();
                    $arrSaveData['RoleLinkMaster'] =  $links;
                    $this->RoleLinkMaster->save($arrSaveData,array('validate' => false));
                }
            }
        }
        $insertRoleLinkOperationQuery ="INSERT INTO role_link_operation_rights_trans(role_master_id,role_link_master_id,type_tran_id,created,modified)
                                        SELECT 1,role_link_masters.`id`,type_trans.`id`,NOW(),NOW() FROM role_link_masters,
                                        type_trans  WHERE type_trans.type_master_id = 1";
        $this->User->query($insertRoleLinkOperationQuery,false);
        $insertRoleLinkOperationQuery = "INSERT INTO role_link_operation_rights_trans(role_master_id,role_link_master_id,type_tran_id,created,modified)
                                        SELECT 2,role_link_masters.`id`,type_trans.`id`,NOW(),NOW() FROM role_link_masters,
                                        type_trans  WHERE type_trans.type_master_id = 1 AND role_link_masters.`is_show` = 1;";
        $this->User->query($insertRoleLinkOperationQuery,false);
        echo 'Operation executed successfully!.';
        exit;
    }

    public function build_role_link() {
        $count = 0;
        $arrRoleLInkData[$count]['name']  = 'Dashboard';
        $arrRoleLInkData[$count]['state']  = 'dashboard';
        $arrRoleLInkData[$count]['module_name']  = 'dashboard';
        $arrRoleLInkData[$count]['folder_name']  = 'dashboard';
        $arrRoleLInkData[$count]['view']  = 'index.html';
        $arrRoleLInkData[$count]['controller']  = 'DashboardController';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 1;
        $arrRoleLInkData[$count]['icon']  = 'icon-home';
        $arrRoleLInkData[$count]['base_file']  = '["/assets/global/plugins/morris/morris.css","/assets/admin/pages/css/tasks.css","/assets/global/plugins/morris/morris.min","assets/global/plugins/morris/raphael-min","assets/global/plugins/jquery.sparkline.min","assets/admin/pages/scripts/index3","assets/admin/pages/scripts/tasks","assets/global/plugins/backstretch/jquery.backstretch.min"]';
        $arrRoleLInkData[$count]['action']  = null;
        $arrRoleLInkData[$count]['order_no']  = 1;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Master';
        $arrRoleLInkData[$count]['state']  = 'master';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'icon-puzzle';
        $arrRoleLInkData[$count]['base_file']  = null;
        $arrRoleLInkData[$count]['action']  = null;
        $arrRoleLInkData[$count]['order_no']  = 2;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** start master child data */
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Title';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'title';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'title';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'title';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'TitleController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-indent';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Country';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'country';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'country';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'country';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CountryController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-institution';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'State';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'state';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'state';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'state';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'StateController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-institution';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'City';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'city';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'city';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'city';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CityController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-institution';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Financial Year';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'financial-year';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'company_year';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'company_year';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CompanyYearController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-calendar';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 5;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Unit';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'unit';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'unit';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'unit';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'UnitController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-cube';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 6;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Bank';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'bank';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'bank';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'bank';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'BankController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-bank';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 7;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Document';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'document';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'document';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'document';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'DocumentController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-file-text';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 8;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Colour';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'colour';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'variant';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'variant';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VariantController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-delicious';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css","assets/global/plugins/jquery-minicolors/jquery.minicolors.css","assets/global/plugins/bootstrap-selectsplitter/bootstrap-selectsplitter.min.js","assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js","assets/admin/pages/scripts/components-form-tools2.js"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css","assets/global/plugins/jquery-minicolors/jquery.minicolors.css","assets/global/plugins/bootstrap-selectsplitter/bootstrap-selectsplitter.min.js","assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js","assets/admin/pages/scripts/components-form-tools2.js"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 9;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Royalty Charge';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'royalty';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'royalty';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'royalty';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'RoyaltyController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-delicious';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 10;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Grade';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'grade';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'grade';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'grade';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'GradesController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-delicious';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 11;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end master child data */

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Settings';
        $arrRoleLInkData[$count]['state']  = 'settings';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'icon-settings';
        $arrRoleLInkData[$count]['base_file']  = null;
        $arrRoleLInkData[$count]['action']  = null;
        $arrRoleLInkData[$count]['order_no']  = 3;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** start setting's child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'General Settings';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'general-settings';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'general-settings';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'settings';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'general.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'SettingsController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-gear';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'User Settings';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'user-settings';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'user-settings';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'settings';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'user.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'SettingsController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-user-md';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end setting's child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Member Management';
        $arrRoleLInkData[$count]['state']  = 'member_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-users';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 4;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** stgart member management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Franchise';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'branch';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'branch';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'branch';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'BranchController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-user';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Member Dynamic Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'member_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'member_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/branch';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'member_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'BranchReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        ++$subchildcount;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Rolyalty Charge Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'royalty_charge_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'royalty_charge_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/branch';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'royalty_charge_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'BranchReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;
        $subchildcount = 0;
        /** end member management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Supplier Management';
        $arrRoleLInkData[$count]['state']  = 'supplier';
        $arrRoleLInkData[$count]['module_name']  = 'supplier';
        $arrRoleLInkData[$count]['folder_name']  = 'supplier';
        $arrRoleLInkData[$count]['view']  = 'index.html';
        $arrRoleLInkData[$count]['controller']  = 'SupplierController';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 1;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-tasks';
        $arrRoleLInkData[$count]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['order_no']  = 5;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Tax Management';
        $arrRoleLInkData[$count]['state']  = 'tax_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 6;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** stgart member management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Tax';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'tax';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'tax';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'tax';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'TaxController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-user';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Tax Group';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'taxgroup';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'taxgroup';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'taxgroup';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'TaxGroupController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-user';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end member management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Stock Management';
        $arrRoleLInkData[$count]['state']  = 'stock_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-building';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 7;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** start stock management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Category';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'category';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'category';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'category';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CategoryController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Product';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'product';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'product';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'product';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ProductController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'icon-handbag';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Purchase';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'purchase';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'purchase';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'purchase';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'PurchaseController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'icon-basket';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Perfoma Invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'purchase-order-invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'purchase-order-invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'purchase_order';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'PurchaseOrderController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'icon-basket';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Sale';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'sale';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'sale';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'sale';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'SalesController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-reddit-square';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 5;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Product Stock';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'stock';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'stock';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'stock';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ProductStockController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-reddit-square';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 6;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 7;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Product Dynamic Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'product_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'product_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/stock';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'product_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'StockReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Purchase Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'purchase_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'purchase_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/stock';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'purchase_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'StockReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Purchase Order Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'purchase_order_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'purchase_order_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/stock';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'purchase_order_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'StockReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Sale Dynamic Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'sale_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'sale_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/stock';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'sale_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'StockReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;
        /** end stock management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Agent Management';
        $arrRoleLInkData[$count]['state']  = 'agent';
        $arrRoleLInkData[$count]['module_name']  = 'agent';
        $arrRoleLInkData[$count]['folder_name']  = 'agent';
        $arrRoleLInkData[$count]['view']  = 'index.html';
        $arrRoleLInkData[$count]['controller']  = 'AgentController.js';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 1;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';;
        $arrRoleLInkData[$count]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['order_no']  = 8;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Vehicle Management';
        $arrRoleLInkData[$count]['state']  = 'vehicle_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-bus';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 9;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** start stock management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Vehicle Type';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'vehicle-type';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'vehicle_type';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle_type';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleTypeController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Manufacturer';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'manufacturer';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'manufacturer';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'manufacturer';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ManufacturerController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Maker';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'maker';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'maker';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle_model';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleModelController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Vehicle Specification';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'specification';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'specification';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'specification';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleSpecificationController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 5;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Vehicle Sale';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'vehicle_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'vehicle_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleSaleController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 6;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Bike ON Rent';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'bike-on-rent';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'vehicle_rent';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle_rent';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleRentController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css","assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css","assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 7;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Bike Refinance';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'bike-refinance';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'vehicle_finance';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'vehicle_finance';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleFinanceController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 8;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 9;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Vehicle Dynamic Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'vehicle_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'vehicle_dynamic_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'vehicle_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Dynamic Vehicle Sale Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_vehicle_sale_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_vehicle_sale_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'dynamic_vehicle_sale_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Dynamic Bike Rent Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_bikerent_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_bikerent_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'bike_rent_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        $subchildcount++;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Dynamic Bike Finance Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_bikefinance_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_bikefinance_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/vehicle';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'bike_finance_dynamic_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;
        /** end stock management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Expense Management';
        $arrRoleLInkData[$count]['state']  = 'expense_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 10;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;
        /** start expense management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Expense Head';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'expense-head';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'expense_head';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'expense_head';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ExpenseHeadController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Expenses';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'expenses';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'expenses';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'expenses';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ExpensesController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Report';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Expense Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'expense_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'expense_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/expense';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'expense_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'ExpenseReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;
        /** end expense management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Coupon Management';
        $arrRoleLInkData[$count]['state']  = 'coupon_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-bus';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 11;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** coupon management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Coupon Master';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'coupon-master';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'coupon';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'coupon';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CouponController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Coupon Sale';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'coupon_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'coupon_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'coupon_sale';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'CouponSalesController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Dynamic Coupon Sale Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_coupon_sale_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_coupon_sale_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/coupon';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'vehicle_coupon_sale_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleCouponReportController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Job Management';
        $arrRoleLInkData[$count]['state']  = 'job_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 12;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** start job master management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Service Group';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'service-group';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'service_group';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'service_group';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ServiceGroupController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Service';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'services';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'service';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'service';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'ServiceController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Inspection';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'inspection';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'inspection';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'inspection';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'InspectionController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end job master child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Manage Job';
        $arrRoleLInkData[$count]['state']  = 'manage_job';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 13;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** start manage job child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'add job';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'job_open';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleJobController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css","assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css","assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Inprogress Job';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'inprogress_job';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'inprogress_job';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'job_inprogress_list.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleJobController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css","assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"]},{"route":"view","param":"1","files":[]},{"route":"product_form","param":"1","files":[]},{"route":"spare_product_form","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        
        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Job Complete';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'job_complete';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'job_complete';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'job_complete_list.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleJobController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"view","param":1,"files":[]},{"route":"spare_product_form","param":"1","files":[]},{"route":"generate_invoice","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Job Invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'job_invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'job_invoice';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'job_invoice_list.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleJobController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"view","param":1,"files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'View Job';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'view_job';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'view_job';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'job';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'job_view_list.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'VehicleJobController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"view","param":1,"files":[]},{"route":"spare_product_form","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 5;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end manage job child data **/

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 6;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Dynamic Vehicle Job Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_vehiclejob_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_vehiclejob_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/vehicle_job';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'dynamic_vehicle_job_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'VehicleJobReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;


        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Employee Management';
        $arrRoleLInkData[$count]['state']  = 'employee_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 14;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** start employee management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Department';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'department';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'department';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'department';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'DepartmentController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Designation';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'designation';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'designation';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'designation';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'DesignationController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Employee';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'employee';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'employee';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'employee';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'EmployeeController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"edit","param":"1","files":["assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Reports';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'report';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-folder';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        $subchildcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['name']  = 'Employee Dynamic Report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['state']  = 'dynamic_employee_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['module_name']  = 'dynamic_employee_report';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['folder_name']  = 'reports/employee';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['view']  = 'dynamic_employee_form.html';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['controller']  = 'EmployeeReportsController';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['icon']  = 'fa fa-file';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['children'][$subchildcount]['is_show']  = 1;
        /** end employee management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'User Management';
        $arrRoleLInkData[$count]['state']  = 'user_management';
        $arrRoleLInkData[$count]['module_name']  = '';
        $arrRoleLInkData[$count]['folder_name']  = '';
        $arrRoleLInkData[$count]['view']  = '';
        $arrRoleLInkData[$count]['controller']  = '';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-pencil-square';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 15;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 1;

        /** start employee management child data **/
        $childcount = 0;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Role';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'role';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'role';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'role';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'RoleController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Role Link';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'rolelink';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'rolelink';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'rolelink';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'RoleLinkController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '["assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[{"route":"add","param":0,"files":[]},{"route":"edit","param":"1","files":[]},{"route":"view","param":"1","files":[]},{"route":"setorder","param":0,"files":[]}]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 2;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 0;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Assign Role';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'assign-role';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'assign_role';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'assign_role';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'AssignRoleController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 3;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;

        ++$childcount;
        $arrRoleLInkData[$count]['children'][$childcount]['name']  = 'Assign Role Rights';
        $arrRoleLInkData[$count]['children'][$childcount]['state']  = 'assign-role-rights';
        $arrRoleLInkData[$count]['children'][$childcount]['module_name']  = 'assign-role-rights';
        $arrRoleLInkData[$count]['children'][$childcount]['folder_name']  = 'assign-rights';
        $arrRoleLInkData[$count]['children'][$childcount]['view']  = 'index.html';
        $arrRoleLInkData[$count]['children'][$childcount]['controller']  = 'AssignRoleRightsController';
        $arrRoleLInkData[$count]['children'][$childcount]['service']  = '';
        $arrRoleLInkData[$count]['children'][$childcount]['is_link']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['icon']  = 'fa fa-database';
        $arrRoleLInkData[$count]['children'][$childcount]['base_file']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['action']  = '[]';
        $arrRoleLInkData[$count]['children'][$childcount]['order_no']  = 4;
        $arrRoleLInkData[$count]['children'][$childcount]['status']  = 1;
        $arrRoleLInkData[$count]['children'][$childcount]['is_show']  = 1;
        /** end employee management child data **/

        ++$count;
        $arrRoleLInkData[$count]['name']  = 'Error Log Management';
        $arrRoleLInkData[$count]['state']  = 'error_log_management';
        $arrRoleLInkData[$count]['module_name']  = 'error_log_management';
        $arrRoleLInkData[$count]['folder_name']  = 'errors';
        $arrRoleLInkData[$count]['view']  = 'index.html';
        $arrRoleLInkData[$count]['controller']  = 'ErrorLogsController';
        $arrRoleLInkData[$count]['service']  = '';
        $arrRoleLInkData[$count]['is_link']  = 0;
        $arrRoleLInkData[$count]['icon']  = 'fa fa-bug';
        $arrRoleLInkData[$count]['base_file']  = '[]';
        $arrRoleLInkData[$count]['action']  = '[]';
        $arrRoleLInkData[$count]['order_no']  = 16;
        $arrRoleLInkData[$count]['status']  = 1;
        $arrRoleLInkData[$count]['is_show']  = 0;
        return $arrRoleLInkData;
    }
}
?>
