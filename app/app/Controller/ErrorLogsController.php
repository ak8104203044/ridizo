<?php
App::uses('AppController','Controller');
class ErrorLogsController extends AppController {
    public $name = 'ErrorLogs';
    public $layout = false;
    public $uses = array('ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            #pr($this->request->data);exit;
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('ErrorLog.is_resolve' => 0);
                if(isset($this->request->data['controller']) && !empty($this->request->data['controller'])) {
                    $conditions['ErrorLog.controller LIKE'] = '%'.trim($this->request->data['controller']).'%';
                }

                if(isset($this->request->data['method']) && !empty($this->request->data['method'])) {
                    $conditions['ErrorLog.method LIKE'] = '%'.trim($this->request->data['method']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('ErrorLog.controller '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('ErrorLog.method '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ErrorLog.controller '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('ErrorLog.created DESC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->ErrorLog->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                $tableOptions = array(
                                    'fields' => array('id','controller','method','request','description','created'),
                                    'joins' => array(
                                        array(
                                            'table' => 'users',
                                            'alias' => 'User',
                                            'type' => 'INNER',
                                            'conditions' => array('ErrorLog.user_id = User.id','User.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => array('ErrorLog.id'),
                                    'order' => $orderBy
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->ErrorLog->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ErrorLog']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['controller'] = $tableDetails['ErrorLog']['controller']."<br>(<b>".date('d-M-Y H:i:s',strtotime($tableDetails['ErrorLog']['created']))."</b>)";
                        $records[$key]['method'] = $tableDetails['ErrorLog']['method'];
                        $records[$key]['request'] = $tableDetails['ErrorLog']['request'];
                        $records[$key]['description'] = $tableDetails['ErrorLog']['description'];
                        $records[$key]['is_exists'] = 0;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        $headers = array('count'=>'S.No','controller'=>'Controller','method'=>'Method','request' => 'Request','description'=>'Description');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $arrDeleteRecords = $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->ErrorLog->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['ErrorLog.is_resolve'] = 1;
                            $updateFields['ErrorLog.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ErrorLog.id'] = $arrDeleteRecords;
                            $this->ErrorLog->updateAll($updateFields,$updateParams);
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
