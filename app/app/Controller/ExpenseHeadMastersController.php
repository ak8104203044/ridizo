<?php
App::uses('AppController','Controller');
class ExpenseHeadMastersController extends AppController {
    public $name = 'ExpenseHeadMasters';
    public $layout = false;
    public $uses = array('ExpenseHeadMaster','ExpenseTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('ExpenseHeadMaster.status' => 1,'ExpenseHeadMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['ExpenseHeadMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['ExpenseHeadMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('ExpenseHeadMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('ExpenseHeadMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('ExpenseHeadMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('ExpenseHeadMaster.order_no ASC');
                }
                
                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->ExpenseHeadMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','COUNT(ET.id) AS count','type'),
                                    'joins' => array(
                                        array(
                                            'table' => 'expense_trans',
                                            'alias' => 'ET',
                                            'type' => 'LEFT',
                                            'conditions' => array('ExpenseHeadMaster.id = ET.expense_head_master_id','ET.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'ExpenseHeadMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->ExpenseHeadMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['ExpenseHeadMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['ExpenseHeadMaster']['name'];
                        $records[$key]['code'] = $tableDetails['ExpenseHeadMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['ExpenseHeadMaster']['order_no'];
                        $records[$key]['is_exists'] = ($tableDetails[0]['count'] > 0 || $tableDetails['ExpenseHeadMaster']['type'] == 2) ? 1 : 0;
                        $maxOrderNo = ($tableDetails['ExpenseHeadMaster']['order_no'] > $maxOrderNo) ? $tableDetails['ExpenseHeadMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['ExpenseHeadMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['ExpenseHeadMaster']['id']) && !empty($arrSaveRecords['ExpenseHeadMaster']['id'])) {
                    $arrSaveRecords['ExpenseHeadMaster']['id'] = $this->decryption($arrSaveRecords['ExpenseHeadMaster']['id']);
                }
                $this->ExpenseHeadMaster->set($arrSaveRecords);
                if($this->ExpenseHeadMaster->validates()) {
                    $dataSource = $this->ExpenseHeadMaster->getDataSource();
                    $fieldList = array('id','name','code','description','branch_master_id','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['ExpenseHeadMaster']['id']) || empty($arrSaveRecords['ExpenseHeadMaster']['id'])) {
                            $this->ExpenseHeadMaster->create();
                        }
                        if(!$this->ExpenseHeadMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['ExpenseHeadMaster']['id']) && !empty($arrSaveRecords['ExpenseHeadMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->ExpenseHeadMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','order_no'),
                            'conditions' => array('status' => 1,'id' => $id,'branch_master_id' => $firmId),
                            'recursive' => -1
                        );
                        $arrRecords = $this->ExpenseHeadMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['ExpenseHeadMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete($firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0 && !empty($firmId)) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $arrDeleteExpenseHeadData = array();
                    $options = array(
                                    'fields' => array('id','expense_head_master_id'),
                                    'conditions' => array('ExpenseTran.expense_head_master_id' => $this->request->data['id'],'ExpenseTran.branch_master_id'=> $firmId,'ExpenseTran.status' => 1)
                                );
                    $arrExpenseTranData = $this->ExpenseTran->find('list',$options);
                    if(count($arrExpenseTranData) > 0) {
                        $arrDeleteExpenseHeadData = array_diff($this->request->data['id'],array_values($arrExpenseTranData));
                    } else {
                        $arrDeleteExpenseHeadData = $this->request->data['id'];
                    }
                    if(count($arrDeleteExpenseHeadData) > 0) {
                        $dataSource = $this->ExpenseHeadMaster->getDataSource();
                        try {
                            $dataSource->begin();
                            $updateFields['ExpenseHeadMaster.status'] = -1;
                            $updateFields['ExpenseHeadMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ExpenseHeadMaster.id'] = $arrDeleteExpenseHeadData;
                            $updateParams['ExpenseHeadMaster.branch_master_id'] = $firmId;
                            if(!$this->ExpenseHeadMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
