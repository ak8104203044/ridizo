<?php
App::uses('AppController','Controller');
class InspectionMastersController extends AppController {
    public $name = 'InspectionMasters';
    public $layout = false;
    public $uses = array('InspectionMaster','VehicleJobChecklistTran','ErrorLog');
    public $components = array('AppUtilities');
    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = isset($this->request->data['firm_id']) ? $this->request->data['firm_id'] : '';
                $conditions = array('InspectionMaster.status' => 1,'InspectionMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['InspectionMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['InspectionMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('InspectionMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('InspectionMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('InspectionMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('InspectionMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->InspectionMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','COUNT(VJCT.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'vehicle_job_checklist_trans',
                                            'alias' => 'VJCT',
                                            'type' => 'LEFT',
                                            'conditions' => array('InspectionMaster.id = VJCT.inspection_master_id','VJCT.status' => 1)
                                        )
                                        ),
                                    'conditions' => $conditions,
                                    'group' => 'InspectionMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->InspectionMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['InspectionMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['InspectionMaster']['name'];
                        $records[$key]['code'] = $tableDetails['InspectionMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['InspectionMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['InspectionMaster']['order_no'] > $maxOrderNo) ? $tableDetails['InspectionMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['InspectionMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['InspectionMaster']['id']) && !empty($arrSaveRecords['InspectionMaster']['id'])) {
                    $arrSaveRecords['InspectionMaster']['id'] = $this->decryption($arrSaveRecords['InspectionMaster']['id']);
                }
                $arrSaveRecords['ServiceMaster']['branch_master_id'] = isset($arrSaveRecords['ServiceMaster']['branch_master_id']) ? $arrSaveRecords['ServiceMaster']['branch_master_id'] : '';
                $this->InspectionMaster->set($arrSaveRecords);
                if($this->InspectionMaster->validates()) {
                    $dataSource = $this->InspectionMaster->getDataSource();
                    $fieldList = array('id','name','code','description','branch_master_id','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['InspectionMaster']['id']) || empty($arrSaveRecords['InspectionMaster']['id'])) {
                            $this->InspectionMaster->create();
                        }
                        if(!$this->InspectionMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['InspectionMaster']['id']) && !empty($arrSaveRecords['InspectionMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->InspectionMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','order_no'),
                            'conditions' => array('status' => 1,'id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->InspectionMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['InspectionMaster']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('inspection_master_id'),'conditions' => array('status'=> 1,'inspection_master_id' => $this->request->data['id']));
                    $arrRecords = $this->VehicleJobChecklistTran->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->InspectionMaster->getDataSource();
                        try{
                            $dataSource->begin();	
                            $updateFields['InspectionMaster.status'] = 0;
                            $updateFields['InspectionMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['InspectionMaster.id'] = $arrDeleteRecords;
                            if(!$this->InspectionMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
