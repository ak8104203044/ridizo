<?php
App::uses('AppController','Controller');
class VehicleModelMastersController extends AppController {
    public $name = 'VehicleModelMasters';
    public $layout = false;
    public $uses = array('VehicleModelMaster','ErrorLog','ManufacturerTran','VehicleModel','VehicleMaster');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $conditions = array('VehicleModel.status' => 1);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['VehicleModel.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['VehicleModel.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                    $conditions['VehicleModel.vehicle_type_id'] =trim($this->request->data['vehicle_type_master_id']);
                }

                if(isset($this->request->data['manufacturer_master_id']) && !empty($this->request->data['manufacturer_master_id'])) {
                    $conditions['VehicleModel.manufacturer_id'] = trim($this->request->data['manufacturer_master_id']);
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('VehicleModel.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('VehicleModel.code '.$sortyType);
                                break;
                        case 3:
                                $orderBy = array('VehicleModel.code '.$sortyType);
                                break;
                        case 4:
                                $orderBy = array('VehicleModel.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('VehicleModel.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('VehicleModel.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1);
                    $totalRecords = $this->VehicleModel->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }
                
                $tableOptions = array(
                                    'fields' => array('id','name','code','vehicle_type','manufacturer','order_no','COUNT(VM.id) AS count'),
                                    'joins' =>array(
                                        array(
                                            'table' => 'vehicle_masters',
                                            'alias' => 'VM',
                                            'type' => 'LEFT',
                                            'conditions' => array('VehicleModel.id = VM.vehicle_model_master_id','VM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'VehicleModel.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->VehicleModel->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['VehicleModel']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['VehicleModel']['name'];
                        $records[$key]['code'] = $tableDetails['VehicleModel']['code'];
                        $records[$key]['vehicle_type'] = $tableDetails['VehicleModel']['vehicle_type'];
                        $records[$key]['manufacturer'] = $tableDetails['VehicleModel']['manufacturer'];
                        $records[$key]['order_no'] = $tableDetails['VehicleModel']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['VehicleModel']['order_no'] > $maxOrderNo) ? $tableDetails['VehicleModel']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','vehicle_type' => 'Vehicle Type','manufacturer' => 'Manufacturer','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrSaveRecords['VehicleModelMaster'] = $this->request->data;
                #pr($arrSaveRecords['VehicleModelMaster']);exit;
                unset($this->request->data);
                if(isset($arrSaveRecords['VehicleModelMaster']['id']) && !empty($arrSaveRecords['VehicleModelMaster']['id'])) {
                    $arrSaveRecords['VehicleModelMaster']['id'] = $this->decryption($arrSaveRecords['VehicleModelMaster']['id']);
                }
                $this->VehicleModelMaster->set($arrSaveRecords);
                if($this->VehicleModelMaster->validates()) {
                    $dataSource = $this->VehicleModelMaster->getDataSource();
                    $fieldList = array('id','name','code','manufacturer_tran_id','description','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['VehicleModelMaster']['id']) || empty($arrSaveRecords['VehicleModelMaster']['id'])) {
                            $this->VehicleModelMaster->create();
                        }
    
                        if(!$this->VehicleModelMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
    
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleModelMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleModelMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','order_no','manufacturer_tran_id','manufacturer','vehicle_type','vehicle_type_id as vehicle_type_master_id'),
                            'conditions' => array('VehicleModel.status' => 1,'VehicleModel.id' => $id),
                            'recursive' => -1
                        );
                        $arrRecords = $this->VehicleModel->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleModel']);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('vehicle_model_master_id'),'conditions' => array('status'=> 1,'vehicle_model_master_id' => $this->request->data['id']));
                    $arrRecords = $this->VehicleMaster->find('list',$options);
                    $arrDeleteRecords = (count($arrRecords) > 0) ? array_diff($this->request->data['id'],array_values($arrRecords)) : $this->request->data['id'];
                    if(count($arrDeleteRecords) > 0) {
                        $dataSource = $this->VehicleModelMaster->getDataSource();
                        try {
                            $dataSource->begin();	
                            $updateFields['VehicleModelMaster.status'] = 0;
                            $updateFields['VehicleModelMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleModelMaster.id'] = $arrDeleteRecords;
                            if(!$this->VehicleModelMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields,$arrDeleteRecords);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('TRANSACTION_PRESENT',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
