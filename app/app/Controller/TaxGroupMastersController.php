<?php
App::uses('AppController','Controller');
class TaxGroupMastersController extends AppController {
    public $name = 'TaxGroupMasters';
    public $layout = false;
    public $uses = array('TaxGroupMaster','TaxGroupTran','ErrorLog');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : '-1';
                $conditions = array('TaxGroupMaster.status' => 1,'TaxGroupMaster.branch_master_id' => $firmId);
                if(isset($this->request->data['name']) && !empty($this->request->data['name'])) {
                    $conditions['TaxGroupMaster.name LIKE'] = '%'.trim($this->request->data['name']).'%';
                }

                if(isset($this->request->data['code']) && !empty($this->request->data['code'])) {
                    $conditions['TaxGroupMaster.code LIKE'] = '%'.trim($this->request->data['code']).'%';
                }

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('TaxGroupMaster.name '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('TaxGroupMaster.code '.$sortyType);
                                break;
                        default:
                                $orderBy = array('TaxGroupMaster.order_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('TaxGroupMaster.order_no ASC');
                }

                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('id'),'conditions' => $conditions,'recursive' => -1 );
                    $totalRecords = $this->TaxGroupMaster->find('count',$tableCountOptions);
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $tableOptions = array(
                                    'fields' => array('id','name','code','order_no','COUNT(PM.id) AS count'),
                                    'joins' => array(
                                        array(
                                            'table' => 'product_masters',
                                            'alias' => 'PM',
                                            'type' => 'LEFT',
                                            'conditions' => array('TaxGroupMaster.id = PM.tax_group_master_id','PM.status' => 1)
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'group' => 'TaxGroupMaster.id',
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->TaxGroupMaster->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    $maxOrderNo = 0;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = $this->encryption($tableDetails['TaxGroupMaster']['id']);
                        $records[$key]['count'] = ++$count;
                        $records[$key]['id'] = $encryption;
                        $records[$key]['name'] = $tableDetails['TaxGroupMaster']['name'];
                        $records[$key]['code'] = $tableDetails['TaxGroupMaster']['code'];
                        $records[$key]['order_no'] = $tableDetails['TaxGroupMaster']['order_no'];
                        $records[$key]['is_exists'] = $tableDetails[0]['count'];
                        $maxOrderNo = ($tableDetails['TaxGroupMaster']['order_no'] > $maxOrderNo) ? $tableDetails['TaxGroupMaster']['order_no']: $maxOrderNo;
                    }
                    if($dataType === 1) {
                        $maxOrderNo = (int)$maxOrderNo  + 1;
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end,'max_orderno' => $maxOrderNo);
                    } else {
                        $headers = array('count'=>'S.No','name'=>'Name','code'=>'Code','order_no'=>'Order No');
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                $arrSaveRecords['TaxGroupMaster'] = $this->request->data;
                unset($this->request->data);
                if(isset($arrSaveRecords['TaxGroupMaster']['id']) && !empty($arrSaveRecords['TaxGroupMaster']['id'])) {
                    $arrSaveRecords['TaxGroupMaster']['id'] = $this->decryption($arrSaveRecords['TaxGroupMaster']['id']);
                }
                $arrSaveRecords['TaxGroupMaster']['branch_master_id'] = isset($arrSaveRecords['TaxGroupMaster']['branch_master_id']) ? $arrSaveRecords['TaxGroupMaster']['branch_master_id'] : '';
                $this->TaxGroupMaster->set($arrSaveRecords);
                if($this->TaxGroupMaster->validates()) {
                    $dataSource = $this->TaxGroupMaster->getDataSource();
                    $fieldList = array('id','name','code','description','branch_master_id','order_no');
                    try{
                        $dataSource->begin();
                        if(!isset($arrSaveRecords['TaxGroupMaster']['id']) || empty($arrSaveRecords['TaxGroupMaster']['id'])) {
                            $this->TaxGroupMaster->create();
                        }
                        if(!$this->TaxGroupMaster->save($arrSaveRecords,array('fieldList' => $fieldList))) {
                            throw new Exception(__('Tax Group could not saved properly, please try again later!',true));
                        }
                        
                        if(isset($arrSaveRecords['TaxGroupMaster']['id']) && !empty($arrSaveRecords['TaxGroupMaster']['id'])) {
                            $updateFields['TaxGroupTran.status'] = 0;
                            $updateFields['TaxGroupTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['TaxGroupTran.tax_group_master_id'] = $arrSaveRecords['TaxGroupMaster']['id'];
                            if(!$this->TaxGroupTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Tax group trans could not deleted properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams);
                            $taxGroupId = $arrSaveRecords['TaxGroupMaster']['id'];
                        } else {
                            $taxGroupId = $this->TaxGroupMaster->getInsertID();
                        }
                        
                        $arrTaxTranData = array();
                        if(isset($arrSaveRecords['TaxGroupMaster']['tax_master_id'][0]) && !empty($arrSaveRecords['TaxGroupMaster']['tax_master_id'][0])) {
                            $arrSaveRecords['TaxGroupMaster']['tax_master_id'] = explode(',',$arrSaveRecords['TaxGroupMaster']['tax_master_id'][0]);
                            foreach($arrSaveRecords['TaxGroupMaster']['tax_master_id'] as $key => $taxId) {
                                $arrTaxTranData[$key]['TaxGroupTran'] = array('tax_group_master_id' => $taxGroupId,'tax_master_id' => $taxId);
                            }
                        }
    
                        if(!empty($arrTaxTranData)) {
                            if(!$this->TaxGroupTran->saveAll($arrTaxTranData)) {
                                throw new Exception(__('Tax group trans could not saved properly, please try again later!',true));
                            }
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['TaxGroupMaster']['id']) && !empty($arrSaveRecords['TaxGroupMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($arrSaveRecords);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $arrSaveRecords,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
                    }
                } else {
                    $validationErrros = Set::flatten($this->TaxGroupMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function record($id = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('get')) {
                if(isset($id) && !empty($id)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                            'fields' => array('name','code','description','order_no','taxlist','tax_name'),
                            'joins' => array(
                                array(
                                    'table' => 'tax_group_trans',
                                    'alias' => 'TaxGroupTran',
                                    'type' => 'INNER',
                                    'conditions' => array('TaxGroupMaster.id = TaxGroupTran.tax_group_master_id','TaxGroupTran.status' => 1)
                                ),
                                array(
                                    'table' => 'tax_masters',
                                    'alias' => 'TaxMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('TaxGroupTran.tax_master_id = TaxMaster.id','TaxMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('TaxGroupMaster.status' => 1,'TaxGroupMaster.id' => $id),
                            'group' => 'TaxGroupMaster.id',
                            'recursive' => -1
                        );
                        $arrRecords = $this->TaxGroupMaster->find('first',$options);
                        if(count($arrRecords) > 0) {
                            $statusCode = 200;
                            unset($this->request->data);
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['TaxGroupMaster']);
                        } else {
                            $statusCode = 200;
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try{
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $dataSource = $this->TaxGroupMaster->getDataSource();
                    try{
                        $dataSource->begin();	
                        $updateFields['TaxGroupMaster.status'] = 0;
                        $updateFields['TaxGroupMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                        $updateParams['TaxGroupMaster.id'] = $this->request->data['id'];
                        if(!$this->TaxGroupMaster->updateAll($updateFields,$updateParams)) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data,$updateParams,$updateFields);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true),'newone' => 1);
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
