<?php
App::uses('Component','Controller');
class UserManagementComponent extends Component{
    public $components = array('Session');
	public function initialize(Controller $controller){
	    $this->Controller = $controller;
    }

    public function getUserRights($userId = null,$roleId = null) {
        $arrUserRoleRights = array();
        if(!empty($userId) && !empty($roleId)) {
            $options = array(
                'fields'=>array('UserRoleRight.id','UserRoleRight.type_name','UserRoleRight.name','UserRoleRight.module_name'),
                'conditions'=>array('UserRoleRight.role_master_id' => $roleId),
                'order'=>array('UserRoleRight.parent_id',"IF(UserRoleRight.order_no IS NULL OR UserRoleRight.order_no = '',9999,UserRoleRight.order_no)"),
                'recursive'=>-1
            );
            $userRoleRights = ClassRegistry::init('UserRoleRight')->find('all',$options);
            if(count($userRoleRights) > 0) {
                $isRights = true;
                $arrRoleLink = array();
                foreach($userRoleRights as $key => $userRoleDetailInfo) {
                    $arrRoleLink[$userRoleDetailInfo['UserRoleRight']['id']] = $userRoleDetailInfo['UserRoleRight']['id'];
                    $arrUserRoleRights[$userRoleDetailInfo['UserRoleRight']['module_name']][strtolower($userRoleDetailInfo['UserRoleRight']['type_name'])] = strtolower($userRoleDetailInfo['UserRoleRight']['type_name']);
                }
            }
        }
        return $arrUserRoleRights;
    }
    
    public function buildRoleRightsMenu($roleId = null) {
        if(!empty($roleId)) {
            $options = array(
                'fields'=>array('UserRoleRight.id','UserRoleRight.type_name'),
                'conditions'=>array('UserRoleRight.role_master_id' => $roleId),
                'order'=>array('UserRoleRight.parent_id',"IF(UserRoleRight.order_no IS NULL OR UserRoleRight.order_no = '',9999,UserRoleRight.order_no)"),
                'recursive'=>-1
            );
            $arrRoleRightsData = ClassRegistry::init('UserRoleRight')->find('list',$options);
            if(count($arrRoleRightsData) > 0) {
                $conditions = array();
                if($roleId != 1) {
                    $conditions = array('is_show' => 1);
                }
                $roleLinkOptions = array(
                    'fields'=>array('id','name','state','icon','parent_id'),
                    'conditions'=>array_merge(array('status' => 1,'id' => array_keys($arrRoleRightsData),$conditions)),
                    'order'=>array('order_no','parent_id')
                );
                $userRoleMenu = ClassRegistry::init('RoleLinkMaster')->find('threaded',$roleLinkOptions);
                if(count($userRoleMenu) > 0) {
                    $buildHtml = '<div class="page-sidebar navbar-collapse collapse">
                                    <ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{\'page-sidebar-menu-closed\': settings.layout.pageSidebarClosed}">
                                        <li class="sidebar-search-wrapper">
                                            <form class="sidebar-search sidebar-search-bordered" action="extra_search.html" method="POST">
                                                <a href="javascript:;" class="remove">
                                                <i class="icon-close"></i>
                                                </a>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search...">
                                                    <span class="input-group-btn">
                                                    <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                                                    </span>
                                                </div>
                                            </form>
                                        </li>';
                                            foreach($userRoleMenu as $key => $arrRoleMenu) {
                                                $icon = (!empty($arrRoleMenu['RoleLinkMaster']['icon'])) ? $arrRoleMenu['RoleLinkMaster']['icon'] : '';
                                                $name = (!empty($arrRoleMenu['RoleLinkMaster']['name'])) ? $arrRoleMenu['RoleLinkMaster']['name'] : '';
                                                $state = (!empty($arrRoleMenu['RoleLinkMaster']['state'])) ? $arrRoleMenu['RoleLinkMaster']['state'] : '';
                                                $activeClass = (strtolower($name) == 'dashboard') ? 'class="start active"' : '';
                                                if(count($arrRoleMenu['children']) > 0) {
                                                    $buildHtml  .=  '<li '.$activeClass.'>
                                                                        <a href="javascript:;">
                                                                        <i class="'.$icon.'"></i>
                                                                        <span class="title">'.$name.'</span>
                                                                        <span class="arrow "></span>
                                                                        </a>
                                                                        <ul class="sub-menu">';
                                                    $buildHtml  .= $this->recursiveChildMenu($arrRoleMenu['children']);
                                                    $buildHtml  .=   '   </ul>
                                                                    </li>';
                                                } else {
                                                    $sref = '';
                                                    $href = 'href="javascript:;"';
                                                    if(strtolower($name) == 'dashboard') {
                                                        $sref = 'ui-sref="'.$state.'"';
                                                    } else {
                                                        $sref = 'ui-sref=".'.$state.'"';
                                                    }
                                                    $buildHtml  .= '<li '.$activeClass.'>
                                                                    <a '.$href.' '.$sref.'>
                                                                    <i class="'.$icon.'"></i>
                                                                    <span class="title">'.$name.'</span>
                                                                    </a>
                                                                </li>';
                                                }
                                            }
                    $buildHtml  .= '    </ul>
                                    </div>';
                }
                
                $path = $_SERVER['DOCUMENT_ROOT'].'/'.Configure::read('FOLDERNAME').'/components/templates/sidebar/'.$roleId.'_sidebar.html';
                if(file_exists($path)) {
                    unlink($path);
                }
                touch($path);
                $handler = fopen($path,'w');
                fwrite($handler,$buildHtml);
                fclose($handler);
            }
        }
    }

    public function recursiveChildMenu($childMenu) {
        $buildHtml = '';
        if(isset($childMenu) && count($childMenu) > 0) {
            $buildHtml = '';
            foreach($childMenu as $key => $arrChildMenu) {
                if(count($arrChildMenu['children']) > 0) {
                    $buildHtml .=   '<li>
                                        <a href="javascript:;">
                                        <i class="'.$arrChildMenu['RoleLinkMaster']['icon'].'"></i>
                                        <span class="title">'.$arrChildMenu['RoleLinkMaster']['name'].'</span>
                                        <span class="arrow "></span>
                                        </a>
                                        <ul class="sub-menu">';
                $buildHtml .= $this->recursiveChildMenu($arrChildMenu['children']);
                    $buildHtml .=   '   </ul>
                                    </li>';
                } else {
                    $state = $arrChildMenu['RoleLinkMaster']['state'];
                    $href = 'href="javascript:;"';
                    $sref = 'ui-sref=".'.$state.'"';
                    $buildHtml.= '<li>
                                    <a '.$href.' '.$sref.'>
                                    <i class="'.$arrChildMenu['RoleLinkMaster']['icon'].'"></i>
                                    <span class="title">'.$arrChildMenu['RoleLinkMaster']['name'].'</span>
                                    </a>
                                </li>';
                }
            }
        }
        return $buildHtml;
    }


    public function saveUserLogs($userId = null,$fromType = 1) {
        $arrUserData = array();
        if(!empty($userId) && !empty($fromType)) {
            $UserLoggedTran = ClassRegistry::init('UserLoggedTran');
            $dataSource = $UserLoggedTran->getDataSource();
			try{
                $tokenId = bin2hex(openssl_random_pseudo_bytes(60));
                $sessId = $this->Session->id();
                $arrUserData = array('token_id' => $tokenId,'sessid' => $sessId);
                $userLoggedDetails['UserLoggedTran']['user_id'] = $userId;
                $userLoggedDetails['UserLoggedTran']['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $userLoggedDetails['UserLoggedTran']['login_time'] = date('Y-m-d H:i:s');
                $userLoggedDetails['UserLoggedTran']['token_id'] = $tokenId;
                $userLoggedDetails['UserLoggedTran']['sessid'] = $sessId;
                $userLoggedDetails['UserLoggedTran']['status'] = 1;
                
                $dataSource->begin();
                /** inactive previous logged in **/
                $params['user_id'] = $userId;
                $params['status'] = 1;
                $params['from_type'] = $fromType;
                $UserLoggedTran->updateAll(array('status' => 0),$params);
                unset($params);
                $UserLoggedTran->create();
                $UserLoggedTran->save($userLoggedDetails);
                $dataSource->commit();
			}
			catch(Exception $e){
                $arrUserData = array();
                $dataSource->rollback();
                $arrErrorLog['ErrorLog']['user_id'] = $userId;
                $arrErrorLog['ErrorLog']['controller'] = 'user_management_component';
                $arrErrorLog['ErrorLog']['method'] = 'saveUserLogs';
                $arrErrorLog['ErrorLog']['description'] = $e;
                $arrErrorLog['ErrorLog']['request'] = (!empty($this->request->data)) ? json_encode($this->request->data) : '';
                ClassRegistry::init('ErrorLog')->save($arrErrorLog);
			}
        }
        return $arrUserData;
    }

    public function logoutUserLogs($userId = null,$sessId = null) {
        if(!empty($userId) && !empty($sessId)) {
            $UserLoggedTran = ClassRegistry::init('UserLoggedTran');
            $dataSource = $UserLoggedTran->getDataSource();
            try{
                $updateFields['UserLoggedTran.logout_time'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                $updateFields['UserLoggedTran.status'] = 0;
                $updateFields['UserLoggedTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');

                $params['UserLoggedTran.user_id'] = $userId;
                $params['UserLoggedTran.sessid'] = $sessId;
                $params['UserLoggedTran.status'] = 1;
                $UserLoggedTran->updateAll($updateFields,$params);
            } catch(Exception $e) {
                $dataSource->rollback();
            }
        }
    }

    public function getUserMemberDetail($userId = null,$userType = 3) {
        $userType = (int) $userType;
        $arrRecords = $arrUserMemberData = array();
        if(!empty($userId)) {
            $userType = (int)$userType;
            if($userType === 3) {
                $options = array(
                            'fields' => array('id','firm_name','unique_no'),
                            'conditions' => array('user_id' => $userId,'status' => 1)
                        );
                $arrUserMemberData = ClassRegistry::init('BranchMaster')->find('first',$options);
            } else if($userType === 4) {
                $options = array(
                    'fields' => array('id','firm_name','unique_no','EmployeeMaster.id'),
                    'joins' => array(
                        array(
                            'table' => 'employee_masters',
                            'alias' => 'EmployeeMaster',
                            'type' =>'INNER',
                            'conditions' => array('BranchMaster.id = EmployeeMaster.branch_master_id','EmployeeMaster.status' => 1)
                        )
                    ),
                    'conditions' => array('EmployeeMaster.user_id' => $userId,'BranchMaster.status' => 1)
                );
                $arrUserMemberData = ClassRegistry::init('BranchMaster')->find('first',$options);
            } else {}
            if(!empty($arrUserMemberData)) {
                $arrRecords['id'] = $arrUserMemberData['BranchMaster']['id'];
                if(isset($arrUserMemberData['EmployeeMaster']['id'])) {
                    $arrRecords['employee_id'] = (int)$arrUserMemberData['EmployeeMaster']['id'];
                }
                $arrRecords['firm_name'] = $arrUserMemberData['BranchMaster']['firm_name'].' ('.$arrUserMemberData['BranchMaster']['unique_no'].')';
            }
        }
        return $arrRecords;
    }
}
?>
