<?php
App::uses('Component','Controller');
App::uses('CakeEmail', 'Network/Email');
#App::import('Vendor', 'smtp', array('file' =>'smtp'.DS.'class.phpmailer.php'));
class EmailComponent extends Component {

	public function initialize(Controller $controller){
	    $this->Controller = $controller;
    }

    public function sendEmail($config) {
        try {
            $email = new CakeEmail('emailConfig');
            $emailConfig = new EmailConfig();
		    $email->from(array($emailConfig->emailConfig['from'] => $emailConfig->emailConfig['name']));
			$email->to($config['from_email']);
			$subject = isset($config['subject']) ? $config['subject'] : '';
			$content = $config['content'];
			$email->subject($subject);
			$email->emailFormat('html');
            $email->send($content);
            $response = array('status' => 1,'message' => 'Email sent successfully !.');
        } catch(Exception $e) {
            $response = array('status' => 0,'message' => $e->errorMessage());
        }
        return $response;
    }

    /*public function sendEmail($emailId,$subject,$content) {
        try {
            $full_name = 'arvind';
            $mail = new PHPMailer(true); //New instance, with exceptions enabled
            $body = preg_replace('/\\\\/', '',$content); //Strip backslashes
            $mail->IsSMTP();                           // tell the class to use SMTP
            $mail->SMTPKeepAlive = true;
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = 'tls'; // sets the prefix to the servier
            $mail->SMTPDebug  = 0; 
            $mail->Port       = 587;                    // set the SMTP server port
            $mail->Host       = 'mail.rajliving.com'; // SMTP server smtp.dreamteam.co.in      
            $mail->Username   = 'info@rajliving.com';     // SMTP server username
            $mail->Password   = 'sPT$aZn5=)D)';
            $mail->AddReplyTo("noreply@fastrue.com", "Query");
            $mail->From = 'noreply@fastrue.com';
            $mail->FromName ='noreply';
            $mail->AddAddress($emailId);
            $mail->Subject = $subject;
            $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
            $mail->WordWrap = 80; // set word wrap
            $mail->MsgHTML($body);
            $mail->IsHTML(true); // send as HTML
            $mail->Send();
        } 
        catch (phpmailerException $e) {
            echo $e->errorMessage();exit;
        }
    }*/
}
?>