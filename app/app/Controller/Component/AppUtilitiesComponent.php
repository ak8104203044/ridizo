<?php
App::uses('Component','Controller');
App::uses('ConnectionManager', 'Model');
class AppUtilitiesComponent extends Component{
	
	public function initialize(Controller $controller){
	    $this->Controller = $controller;
    }

    public function saveErrorLogs(&$logs) {
        if(isset($logs) && is_array($logs) && count($logs) > 0) {
            $errorLog = ClassRegistry::init('ErrorLog');
            $dataSource = $errorLog->getDataSource();
            try{
                $dataSource->begin();
                $saveErrorLog['ErrorLog']['user_id'] = $logs['user_id'];
                $saveErrorLog['ErrorLog']['controller'] = $logs['controller'];
                $saveErrorLog['ErrorLog']['method'] = $logs['method'];
                $saveErrorLog['ErrorLog']['description'] = $logs['error'];
                $errorLog->create();
                $errorLog->save($saveErrorLog);
                $dataSource->commit();
            } catch(Exception $e) {
                $dataSource->rollback();
                $e->getMessage();
            }
        }
    }

    public function displayMessage(&$arrMessage) {
        $response = '';
        if(isset($arrMessage) && is_array($arrMessage) && count($arrMessage) > 0) {
            $response = '<ul><li>'.implode('</li><li>',$arrMessage).'</li></ul>';
        }
        return $response;
    }

    public function getUserOperation(){
		$options = array(
                        'fields'=>array('id','name'),
                        'conditions'=>array('status' => 1,'type_master_id' => Configure::read('rightsOperationMasterType')),
                        'recursive'=>-1
                    );
        $arrUserOperationData = ClassRegistry::init('TypeTran')->find('all',$options);
		return $arrUserOperationData;
    }
    
    public function date_format($getDate,$format='yyyy-mm-dd') {

        if(isset($getDate)) {
          $getDateFormat = null;
          if(strpos($getDate,'-') !== false) {
            $date = str_replace('/','-',$getDate);
          } else {
              $date = str_replace('/','/',$getDate);
          }
          try {
    
              $dateObj = new DateTime($date);
                  if(isset($date) && !empty($date)){
                    switch($format){
                      case 'yyyy-mm-dd': $getDateFormat = $dateObj->format('Y-m-d');
                                        break;
                      case 'mm-dd-yyyy': $getDateFormat = $dateObj->format('m-d-Y');
                                        break;
                      case 'dd-mm-yyyy':$getDateFormat = $dateObj->format('d-m-Y');
                                        break;
                      case 'dd/mm/yyyy':$getDateFormat = $dateObj->format('d/m/Y');
                                        break;
                      case 'yyyy'       :  $getDateFormat = $dateObj->format('Y');
                                        break;
                      case 'yy':  $getDateFormat=$dateObj->format('y');
                                break;
                      default : $getDateFormat = $dateObj->format('Y-m-d');
                                break;
                    }
                  }
                  return $getDateFormat;
          } catch (Exception $e) {
              return $getDate;
          }
        }
    }

    public function generateMemberUniqueNo() {
        $arrMemberCode = array();
        $options = array(
                        'fields' => array('field_name','field_value'),
                        'conditions' => array('status' => 1,'field_name' => array('franchise_unique_string','franchise_total_digit','franchise_digit_start_from'))
                    );
        $arrMemberData = ClassRegistry::init('GeneralSetting')->find('list',$options);
        if(!empty($arrMemberData)) {
            
            $string = $arrMemberData['franchise_unique_string'];
            $length = (intval($arrMemberData['franchise_total_digit']) > 0) ? intval($arrMemberData['franchise_total_digit']) : 1;
            $start = (intval($arrMemberData['franchise_digit_start_from']) > 0) ? intval($arrMemberData['franchise_digit_start_from']) : 1;
            $tableName = 'auto_member_codes';
            $dbConfig  = ConnectionManager::getDataSource('default')->config;
			$database = $dbConfig['database'];
            $query = "SELECT AUTO_INCREMENT AS maxNumber  FROM information_schema.`tables` AS AutoMemberCode
					 WHERE TABLE_SCHEMA='".$database."' AND table_name='".$tableName."'";
            $memberDetail = ClassRegistry::init('User')->query($query,false);
            if(!empty($memberDetail)) {
                $maxNumber = (($memberDetail[0]['AutoMemberCode']['maxNumber']) > 0) ? intval($memberDetail[0]['AutoMemberCode']['maxNumber']) : 1;
                $memberNumber = str_pad($maxNumber,$length,0,STR_PAD_LEFT);
                $replaceData['%AUTO_INCREMENT_DIGIT%'] = $memberNumber;
                $memberNo = strtr($string,$replaceData);
                $arrMemberCode['code'] = $memberNo;
            }
        }
        return $arrMemberCode;
    }

    public function getMemberUniqueNo($maxNumber = null) {
        $arrMemberCode = array();
        if(!empty($maxNumber) && $maxNumber > 0) {
            $options = array(
                            'fields' => array('field_name','field_value'),
                            'conditions' => array('status' => 1,'field_name' => array('franchise_unique_string','franchise_total_digit','franchise_digit_start_from'))
                        );
            $arrMemberData = ClassRegistry::init('GeneralSetting')->find('list',$options);
            if(!empty($arrMemberData)) {
                $maxNumber = (int)$maxNumber;
                $string = $arrMemberData['franchise_unique_string'];
                $length = (intval($arrMemberData['franchise_total_digit']) > 0) ? intval($arrMemberData['franchise_total_digit']) : 1;
                $start = (intval($arrMemberData['franchise_digit_start_from']) > 0) ? intval($arrMemberData['franchise_digit_start_from']) : 1;
                $memberNumber = str_pad($maxNumber,$length,0,STR_PAD_LEFT);
                $replaceData['%AUTO_INCREMENT_DIGIT%'] = $memberNumber;
                $memberNo = strtr($string,$replaceData);
                $arrMemberCode['code'] = $memberNo;
                $arrMemberCode['maxNumber'] = $maxNumber;
            }
        }
        return $arrMemberCode;
    }

    public function getVehicleUniqueNo($maxNumber = null) {
        $arrRecords = array();
        $options = array(
                        'fields' => array('field_name','field_value'),
                        'conditions' => array('status' => 1,'field_name' => array('vehicle_unique_string','vehicle_total_digit','vehicle_digit_start_from'))
                    );
        $arrVehicleData = ClassRegistry::init('GeneralSetting')->find('list',$options);
        if(!empty($arrVehicleData)) {
            $string = $arrVehicleData['vehicle_unique_string'];
            $length = (intval($arrVehicleData['vehicle_total_digit']) > 0) ? intval($arrVehicleData['vehicle_total_digit']) : 1;
            $start = (intval($arrVehicleData['vehicle_digit_start_from']) > 0) ? intval($arrVehicleData['vehicle_digit_start_from']) : 1;
            if(empty($maxNumber)) {
                $dbConfig  = ConnectionManager::getDataSource('default')->config;
                $database = $dbConfig['database'];
                $tableName = 'auto_vehicle_codes';
                $query = "SELECT AUTO_INCREMENT AS maxNumber  FROM information_schema.`tables` AS AutoVehicleCode
                        WHERE TABLE_SCHEMA='".$database."' AND table_name='".$tableName."'";
                $memberDetail = ClassRegistry::init('User')->query($query,false);
                if(!empty($memberDetail)) {
                    $maxNumber = (($memberDetail[0]['AutoVehicleCode']['maxNumber']) > 0) ? intval($memberDetail[0]['AutoVehicleCode']['maxNumber']) : 1;
                } else {
                    $maxNumber = 1;
                }
            }
            $digits = str_pad($maxNumber,$length,0,STR_PAD_LEFT);
            if(!empty($string)) {
                $replaceData['%AUTO_INCREMENT_DIGIT%'] = $digits;
                $code = strtr($string,$replaceData);
            } else {
                $code = $digits;
            }
            $arrRecords['code'] = $code;
            $arrRecords['maxNumber'] = $maxNumber;
        }
        return $arrRecords;
    }

    public function getActiveFinancialYear() {
        $arrYearData = array();
        $options = array('fields' => array('id','caption'),'conditions' => array('status' => 1,'is_current' => 1));
        $arrActiveYearData = ClassRegistry::init('CompanyYearMaster')->find('first',$options);
        if(!empty($arrActiveYearData)) {
            $arrYearData['name'] = $arrActiveYearData['CompanyYearMaster']['caption'];
            $arrYearData['id'] = $arrActiveYearData['CompanyYearMaster']['id'];
        }
        return $arrYearData;
    }

    /**
     * $firmId => User Member Type Id
     * $type 1 => employee code,2 => invoice code ,3 => vehicle job code
     * $maxNumber maxvalue when save the data
     */
    public function getUserSettingCode($firmId = null,$type = 1,$maxNumber = null) {
        $type = (int) $type;
        if($type === 1) {
            $field1 = 'employee_code'; 
            $field2 = 'employee_code_digit'; 
            $field3 = 'employee_code_start_from';
            $tableName = 'auto_employee'.$firmId.'_codes';
        } else if($type === 2) {
            $field1 = 'invoice_code'; 
            $field2 = 'invoice_code_digit'; 
            $field3 = 'invoice_code_start_from';
            $tableName = 'auto_invoice'.$firmId.'_codes';
        } else {
            $field1 = 'jobcard_code'; 
            $field2 = 'jobcard_code_digit'; 
            $field3 = 'jobcard_code_start_from';
            $tableName = 'auto_job'.$firmId.'_codes';
        }
        $arrRecords = array();
        if(!empty($firmId)) {
            $options = array(
                'fields' => array($field1,$field2,$field3),
                'conditions' => array('status' => 1,'branch_master_id' => $firmId)
            );
            $arrUserSettings = ClassRegistry::init('UserSetting')->find('first',$options);
            if(!empty($arrUserSettings)) {
                $string = $arrUserSettings['UserSetting'][$field1];
                $length = (intval($arrUserSettings['UserSetting'][$field2]) > 0) ? intval($arrUserSettings['UserSetting'][$field2]) : 1;
                $start = (intval($arrUserSettings['UserSetting'][$field3]) > 0) ? intval($arrUserSettings['UserSetting'][$field3]) : 1;
                if(empty($maxNumber)) {
                    $dbConfig  = ConnectionManager::getDataSource('default')->config;
                    $database = $dbConfig['database'];
                    $query = "SELECT AUTO_INCREMENT AS maxNumber  FROM information_schema.`tables` AS AutoEmployeeCode
                            WHERE TABLE_SCHEMA='".$database."' AND table_name='".$tableName."'";
                    $memberDetail = ClassRegistry::init('User')->query($query,false);
                    if(!empty($memberDetail)) {
                        $maxNumber = (($memberDetail[0]['AutoEmployeeCode']['maxNumber']) > 0) ? intval($memberDetail[0]['AutoEmployeeCode']['maxNumber']) : 1;
                    }
                }
                $arrCompanyYearData = $this->getCurrentFinancialYear();
                $fullYear = $shortYear = '';
                if(!empty($arrCompanyYearData)) {
                    $fullYear = $arrCompanyYearData['CompanyYearMaster']['caption'];
                    $shortYear = $this->date_format($arrCompanyYearData['CompanyYearMaster']['start_date'],'yy').'-'.$this->date_format($arrCompanyYearData['CompanyYearMaster']['end_date'],'yy');
                }
                $digits = str_pad($maxNumber,$length,0,STR_PAD_LEFT);
                if(!empty($string)) {
                    $replaceData['%AUTO_INCREMENT_DIGIT%'] = $digits;
                    $replaceData['%FULL_FINANCIAL_YEAR%'] = $fullYear;
                    $replaceData['%SHORT_FINANCIAL_YEAR%'] = $shortYear;
                    $code = strtr($string,$replaceData);
                } else {
                    $code = $digits;
                }
                $arrRecords['code'] = $code;
                $arrRecords['maxNumber'] = $maxNumber;
            }
        }
        return $arrRecords;
    }

    public function getCurrentFinancialYear() {
        $options = array(
            'fields' => array('id','name','caption','start_date','end_date'),
            'conditions' => array('status' => 1,'is_current' => 1)
        );
        $arrCompanyYearData = ClassRegistry::init('CompanyYearMaster')->find('first',$options);
        return $arrCompanyYearData;
    }

    public function getReportHeaderDetail($firmId = null) {
        $arrFirmDetail = array();
        if(!empty($firmId)) {
            $options = array(
                        'fields' => array('id','firm_name','gst_number','mobile_no','email_id','pincode','c_address','p_address','logo'),
                        'conditions' => array('BranchMaster.status' => 1,'BranchMaster.id' => $firmId)
                    );
            $arrFirmDetail = ClassRegistry::init('BranchMaster')->find('first',$options);
            if(count($arrFirmDetail) > 0) {
                return $arrFirmDetail['BranchMaster'];
            }
        }
    }

    public function getNumberFormat($number = 0) {
        $no = round($number);
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            0 => '',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Forty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
            } else {
                $str [] = null;
            }  
        }
        $Rupees = implode(' ', array_reverse($str));
        $paise = ($decimal) ? "And Paise " . ($words[$decimal - $decimal%10]) ." " .($words[$decimal%10])  : '';
        return ($Rupees ? 'Rupees ' . $Rupees : '') . $paise . " Only";
    }

    public function arrangeArray($data) {
        $arrangeArrayData = array();
        if(!empty($data)) {
            if(!is_array($data)) {
                $data = explode(',',$data);
            }
            foreach($data as $key => $value) {
                if(!empty($value) && strlen($value) > 3) {
                    $arrangeArrayData[] = trim($value);
                }
            }
        }
        return $arrangeArrayData;
    }

    public function getProductStockQty($productId = 0) {
        $stock = 0;
        if(!empty($productId)) {
            $query = "SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_stock_trans WHERE `status` = 1 AND product_master_id = '".$productId."'
                      UNION ALL
                      SELECT IFNULL(SUM(quantity),0) AS quantity FROM product_consumption_trans WHERE `status` = 1 AND product_master_id = '".$productId."'";
            $result = ClassRegistry::init('User')->query($query,false);
            if(!empty($result)) {
                #pr($result);
                $available = $result[0][0]['quantity'];
                $consumption = $result[1][0]['quantity'];
                $stock = ($available > $consumption) ? ($available - $consumption) : 0;
            }
        }
        return $stock;
    }

    public function save_links() {
        $arrRouterRoleLinkData = array();
        $savePath = $_SERVER['DOCUMENT_ROOT'].'/'.Configure::read('FOLDERNAME').'/modules/';
        $path = '';
        $options = array(
            'fields'=>array('id','name','state','module_name','folder_name','controller','service','view','base_files','action'),
            'conditions'=>array('status' => 1 ,'is_link' => 1),
            'order'=>array('parent_id','order_no')
        );
        $arrRoleLinkData = ClassRegistry::init('RoleLinkMaster')->find('all',$options);
        if(count($arrRoleLinkData) > 0) {
            $arrRouterRoleLinkData[] = array(
                'name' => 'login',
                'url' => '/login',
                'template_url' => $path.'components/templates/users/login.html',
                'controller' => 'UsersController',
                'title' => 'Login',
                'state' => 'login',
                'module' => 'login',
                'is_parent' => false,
                'files' => array(
                    $path.'assets/admin/pages/css/login-soft.css',
                    $path.'assets/global/plugins/backstretch/jquery.backstretch.min.js',
                    $path.'components/controllers/UsersController.js',
                    $path.'components/services/AppService.js'
                )
            );

            $arrRouterRoleLinkData[] = array(
                'name' => 'dashboard.change-password',
                'url' => '/change-password',
                'template_url' => $path.'components/templates/users/changepassword.html',
                'controller' => 'UsersController',
                'title' => 'Change Password',
                'state' => 'change-password',
                'module' => 'change-password',
                'is_parent' => true,
                'files' => array(
                    $path.'components/controllers/UsersController.js',
                    $path.'components/services/AppService.js'
                )
            );

            $arrRouterRoleLinkData[] = array(
                'name' => 'dashboard.leatherboard',
                'url' => '/leatherboard',
                'template_url' => $path.'components/templates/dashboard/dashboard.html',
                'controller' => 'DashboardController',
                'title' => 'Leatherboard',
                'state' => 'leatherboard',
                'module' => 'leatherboard',
                'is_parent' => true,
                'files' => array(
                    $path.'components/controllers/DashboardController.js',
                    $path.'components/services/AppService.js'
                )
            );

            foreach($arrRoleLinkData as $key => $roleLinkData) {
                $service = $path.'components/services/AppService.js';
                $controller = '';
                if(!empty($roleLinkData['RoleLinkMaster']['service'])) {
                    if(stripos($roleLinkData['RoleLinkMaster']['service'],'.js') !== false) {
                        $service = $path.'components/services/'.trim($roleLinkData['RoleLinkMaster']['service']);
                    } else {
                        $service = $path.'components/services/'.trim($roleLinkData['RoleLinkMaster']['service']).'.js';
                    }
                }

                if(!empty($roleLinkData['RoleLinkMaster']['controller'])) {
                    if(stripos($roleLinkData['RoleLinkMaster']['controller'],'.js') !== false) {
                        $controller = $path.'components/controllers/'.trim($roleLinkData['RoleLinkMaster']['controller']);
                    } else {
                        $controller = $path.'components/controllers/'.trim($roleLinkData['RoleLinkMaster']['controller']).'.js';
                    }
                }

                $arrBaseFiles = array_merge(array($controller,$service),array($path.'components/controllers/dataTableController.js'));
                if(!empty($roleLinkData['RoleLinkMaster']['base_files'])) {
                    $arrFiles = json_decode($roleLinkData['RoleLinkMaster']['base_files'],true);
                    if(!empty($arrFiles)) {
                        foreach($arrFiles as $key =>$file) {
                            if(!empty($file)) {
                                $arrBaseFiles[] = $path.trim($file);
                            }
                        }
                    }
                }
                $folderName = trim($roleLinkData['RoleLinkMaster']['folder_name']);
                
                if(!empty($roleLinkData['RoleLinkMaster']['action'])) {
                    
                    $arrRouterRoleLinkData[] = array(
                        'name' => 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']),
                        'url' => '/'.trim($roleLinkData['RoleLinkMaster']['state']),
                        'template_url' => $path.'components/templates/'.$folderName.'/'.trim($roleLinkData['RoleLinkMaster']['view']),
                        'controller' => trim($roleLinkData['RoleLinkMaster']['controller']),
                        'title' => trim($roleLinkData['RoleLinkMaster']['name']),
                        'is_parent' => true,
                        'state' => 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']),
                        'module' => trim($roleLinkData['RoleLinkMaster']['module_name']),
                        'files' => $arrBaseFiles
                    );

                    if(!empty($roleLinkData['RoleLinkMaster']['action'])) {
                        $arrRouterData = json_decode($roleLinkData['RoleLinkMaster']['action'],true);
                        if(!empty($arrRouterData) && count($arrRouterData) > 0) {
                            foreach($arrRouterData as $key => $router) {
                                $arrActionFiles = array($controller,$service);
                                if(!empty($router['files']) && count($router['files']) > 0) {
                                    $arrActionFiles = array_merge($arrActionFiles,$router['files']);
                                }

                                $route = strtolower(trim($router['route']));
                                $name  = ucwords($route);
                                $fileName = $route.'.html';
                                $arrRouter = array();
                                $arrRouter['name'] = 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']).'-'.$route;
                                if($router['param'] == 1) {
                                    $arrRouter['url'] = '/'.trim($roleLinkData['RoleLinkMaster']['state']).'/'.$route.'/:id';
                                } else {
                                    $arrRouter['url'] = '/'.trim($roleLinkData['RoleLinkMaster']['state']).'/'.$route;
                                }
                                $arrRouter['template_url'] = $path.'components/templates/'.$folderName.'/'.$fileName;
                                $arrRouter['controller'] = trim($roleLinkData['RoleLinkMaster']['controller']);
                                $arrRouter['title'] = $name.' '.trim($roleLinkData['RoleLinkMaster']['name']);
                                $arrRouter['is_parent'] = true;
                                $arrRouter['state'] = 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']);
                                $arrRouter['module'] = trim($roleLinkData['RoleLinkMaster']['module_name']);
                                $arrRouter['files'] = $arrActionFiles;

                                $arrRouterRoleLinkData[] = $arrRouter;
                            }
                        }
                    }
                } else {
                    $isParent = (strtolower($roleLinkData['RoleLinkMaster']['name']) === 'dashboard') ? false : true;
                    if($isParent === false) {
                        $arrRouterRoleLinkData[] = array(
                            'name' => trim($roleLinkData['RoleLinkMaster']['state']),
                            'url' => '/'.trim($roleLinkData['RoleLinkMaster']['state']),
                            'template_url' => $path.'components/templates/'.$folderName.'/'.trim($roleLinkData['RoleLinkMaster']['view']),
                            'controller' => trim($roleLinkData['RoleLinkMaster']['controller']),
                            'title' => trim($roleLinkData['RoleLinkMaster']['name']),
                            'is_parent' => $isParent,
                            'state' => trim($roleLinkData['RoleLinkMaster']['state']),
                            'module' => trim($roleLinkData['RoleLinkMaster']['module_name']),
                            'files' => $arrBaseFiles
                        );
                    } else {
                        $arrRouterRoleLinkData[] = array(
                            'name' => 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']),
                            'url' => '/'.trim($roleLinkData['RoleLinkMaster']['state']),
                            'template_url' => $path.'components/templates/'.$folderName.'/'.trim($roleLinkData['RoleLinkMaster']['view']),
                            'controller' => trim($roleLinkData['RoleLinkMaster']['controller']),
                            'title' => trim($roleLinkData['RoleLinkMaster']['name']),
                            'is_parent' => $isParent,
                            'state' => 'dashboard.'.trim($roleLinkData['RoleLinkMaster']['state']),
                            'module' => trim($roleLinkData['RoleLinkMaster']['module_name']),
                            'files' => $arrBaseFiles
                        );
                    }
                }
            }
            #echo json_encode($arrRouterRoleLinkData,JSON_UNESCAPED_SLASHES);exit;
            if(count($arrRouterRoleLinkData) > 0) {
                $path = $_SERVER['DOCUMENT_ROOT'].'/'.Configure::read('FOLDERNAME').'/modules/router.json';
                if(file_exists($path)) {
                    unlink($path);
                }
                touch($path);
                $handler = fopen($path,'w');
                fwrite($handler,json_encode($arrRouterRoleLinkData,JSON_UNESCAPED_SLASHES));
                fclose($handler);
                $statusCode = 200;
                $response = array('status' => 1,'message' => __('Role link information is saved successfully !..',true));
            } else {
                $response = array('status' => 0,'message' => __('no_record',true));
            }
        } else {
            $response = array('status' => 0,'message' => __('no_record',true));
        }
        return $response;
    }

    public function getVehicleCouponDetail($firmId = 0,$vehicleId = 0,$servicekm = '') {
        $vehicleCouponDetail = array();
        if(!empty($firmId) && !empty($vehicleId) && !empty($servicekm)) {
            $options = array(
                'fields' => array('VehicleCouponTran.id','VehicleCouponTran.name','VehicleCouponTran.date','VehicleCouponTran.km','CouponMaster.name'),
                'joins' => array(
                    array(
                        'table' => 'vehicle_coupon_trans',
                        'alias' => 'VehicleCouponTran',
                        'type' => 'INNER',
                        'conditions' => array('VehicleCouponMaster.id = VehicleCouponTran.vehicle_coupon_master_id')
                    ),
                    array(
                        'table' => 'coupon_masters',
                        'alias' => 'CouponMaster',
                        'type' => 'INNER',
                        'conditions' => array('VehicleCouponMaster.id = VehicleCouponTran.vehicle_coupon_master_id')
                    )
                ),
                'conditions' => array(
                                    'VehicleCouponMaster.branch_master_id' => $firmId,'VehicleCouponMaster.vehicle_master_id' => $vehicleId,'VehicleCouponMaster.status' => 1,
                                    'VehicleCouponTran.status' => 1,'VehicleCouponTran.type' => 0,'OR' => array('VehicleCouponTran.km >=' => $servicekm,'VehicleCouponTran.date >=' => date('Y-m-d'))
                                ),
                'order' => 'VehicleCouponTran.date ASC'
            );
            $arrVehicleCouponDetail = ClassRegistry::init('VehicleCouponMaster')->find('first',$options);
            if(count($arrVehicleCouponDetail) > 0) {
                $vehicleCouponDetail['id'] = $arrVehicleCouponDetail['VehicleCouponTran']['id'];
                $vehicleCouponDetail['name'] = $arrVehicleCouponDetail['CouponMaster']['name'];
                $vehicleCouponDetail['service'] = $arrVehicleCouponDetail['VehicleCouponTran']['name'].' - Service';
                $vehicleCouponDetail['expired'] = 0;
            } else {
                $options = array(
                    'fields' => array('VehicleCouponTran.id','VehicleCouponTran.name','VehicleCouponTran.date','VehicleCouponTran.km','CouponMaster.name'),
                    'joins' => array(
                        array(
                            'table' => 'vehicle_coupon_trans',
                            'alias' => 'VehicleCouponTran',
                            'type' => 'INNER',
                            'conditions' => array('VehicleCouponMaster.id = VehicleCouponTran.vehicle_coupon_master_id')
                        ),
                        array(
                            'table' => 'coupon_masters',
                            'alias' => 'CouponMaster',
                            'type' => 'INNER',
                            'conditions' => array('VehicleCouponMaster.id = VehicleCouponTran.vehicle_coupon_master_id')
                        )
                    ),
                    'conditions' => array(
                                        'VehicleCouponMaster.branch_master_id' => $firmId,'VehicleCouponMaster.vehicle_master_id' => $vehicleId,'VehicleCouponMaster.status' => 1,
                                        'VehicleCouponTran.status' => 1,'VehicleCouponTran.type' => 0
                                    ),
                    'order' => 'VehicleCouponTran.date ASC'
                );
                $arrVehicleCouponDetail = ClassRegistry::init('VehicleCouponMaster')->find('first',$options);
                if(count($arrVehicleCouponDetail) >0 ){
                    $vehicleCouponDetail['id'] = $arrVehicleCouponDetail['VehicleCouponTran']['id'];
                    $vehicleCouponDetail['name'] = $arrVehicleCouponDetail['CouponMaster']['name'];
                    $vehicleCouponDetail['installment'] = $arrVehicleCouponDetail['VehicleCouponTran']['name'];
                    $vehicleCouponDetail['expired'] = 1;
                }
            }
        }
        return $vehicleCouponDetail;
    }

    public function getRoyaltyChargeDetail($branchId = 0,$type = 1,$price = 0) {
        $arrChargeDetail = array();
        if(!empty($branchId) && $price > 0) {
            if($type == 1) {
                $fields = array('RCM.id','RCM.vehicle_price_type AS type','RCM.vehicle_price AS price');
            } else {
                $fields = array('RCM.id','RCM.service_price_type AS type','RCM.service_price AS price');
            }
            $options = array(
                            'fields' => $fields,
                            'joins' => array(
                                array(
                                    'table' => 'royalty_charge_masters',
                                    'alias' => 'RCM',
                                    'type' => 'INNER',
                                    'conditions' => array('BranchMaster.royalty_charge_master_id = RCM.id','RCM.status' => 1)
                                )
                            ),
                            'conditions' => array('BranchMaster.status' => 1,'BranchMaster.id' => $branchId)
                        );
            $arrRoyaltyChargeDetail = ClassRegistry::init('BranchMaster')->find('first',$options);
            if(count($arrRoyaltyChargeDetail) > 0) {
                $id = (int)$arrRoyaltyChargeDetail['RCM']['id'];
                $rtype = (int)$arrRoyaltyChargeDetail['RCM']['type'];
                $rprice = (float)$arrRoyaltyChargeDetail['RCM']['price'];
                if($rtype === 1) {
                    $amount = number_format(($price * $rprice) / 100,2,'.','');
                } else {
                    $amount = ($rprice > $price) ? $price : $rprice;
                }
                $arrChargeDetail['id'] = $id;
                $arrChargeDetail['amount'] = number_format($amount,2,'.','');
            }
        }
        return $arrChargeDetail;
    }
}
?>