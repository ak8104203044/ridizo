<?php
App::uses('AppController','Controller');
class VehicleCouponReportController extends AppController {
    public $name = 'VehicleCouponReport';
    public $layout = false;
    public $uses = array('InvoiceMaster','ErrorLog');
    public $components = array('AppUtilities');

    public function dynamic_vehicle_coupon_sale_report() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['coupon']) && !empty($this->request->data['coupon'])) {
                    $fcount = $count = 0;
                    $arrFieldData = $arrHeaderData = array();
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "DATE_FORMAT(InvoiceMaster.invoice_date,'%d-%m-%Y') AS invoice_date";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'invoice_date';
                        $arrFieldData[$count]['caption'] = 'Invoice Date';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "InvoiceMaster.invoice_no";
                        $arrFieldData[$count]['tableName'] = 'InvoiceMaster';
                        $arrFieldData[$count]['fieldName'] = 'invoice_no';
                        $arrFieldData[$count]['caption'] = 'Invoice No.';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "InvoiceMaster.amount";
                        $arrFieldData[$count]['tableName'] = 'InvoiceMaster';
                        $arrFieldData[$count]['fieldName'] = 'amount';
                        $arrFieldData[$count]['caption'] = 'Amount';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_type_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_type_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Type';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.manufacturer_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'manufacturer_name';
                        $arrFieldData[$count]['caption'] = 'Manufacturer';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_model_name";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_model_name';
                        $arrFieldData[$count]['caption'] = 'Vehicle Model';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.unique_no";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'unique_no';
                        $arrFieldData[$count]['caption'] = 'Vehicle Code';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.vehicle_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'vehicle_number';
                        $arrFieldData[$count]['caption'] = 'Vehicle No.';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.engine_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'engine_number';
                        $arrFieldData[$count]['caption'] = 'Engine No.';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.chasis_number";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'chasis_number';
                        $arrFieldData[$count]['caption'] = 'Chasis No.';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "CouponMaster.name";
                        $arrFieldData[$count]['tableName'] = 'CouponMaster';
                        $arrFieldData[$count]['fieldName'] = 'name';
                        $arrFieldData[$count]['caption'] = 'Coupon';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "(CASE InvoiceMaster.payment_mode WHEN 0 THEN 'N/A' WHEN 1 THEN 'Cash' WHEN 2 THEN 'Cheque' WHEN 3 THEN 'Credit/Debit' WHEN 4 THEN 'Netbanking' ELSE 'UPI' END) AS payment_mode";
                        $arrFieldData[$count]['tableName'] = 0;
                        $arrFieldData[$count]['fieldName'] = 'payment_mode';
                        $arrFieldData[$count]['caption'] = 'Payment Mode';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] ="Vehicle.customer_name as fullname";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'fullname';
                        $arrFieldData[$count]['caption'] = 'Customer Name';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.owner_mobile_no";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'owner_mobile_no';
                        $arrFieldData[$count]['caption'] = 'Mobile No.';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.owner_email_id";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'owner_email_id';
                        $arrFieldData[$count]['caption'] = 'Email ID';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    ++$fcount;
                    if(isset($this->request->data['coupon'][$fcount]['field'])) {
                        $arrFieldData[$count]['columnName'] = "Vehicle.owner_address";
                        $arrFieldData[$count]['tableName'] = 'Vehicle';
                        $arrFieldData[$count]['fieldName'] = 'owner_address';
                        $arrFieldData[$count]['caption'] = 'Address';
                        $arrFieldData[$count]['order'] = $this->request->data['coupon'][$fcount]['order'];
                        ++$count;
                    }
                    #pr($arrFieldData);exit;
                    if(count($arrFieldData) > 0) {
                        uasort($arrFieldData,function($a,$b){
                            if($a['order'] == 0){
                                return 1;
                            }
                            if($b['order'] == 0){
                                return -1;
                            }
                            return ($a['order'] < $b['order']) ? -1 : 1;
                        });
                        #pr($arrFieldData);exit;
                        foreach($arrFieldData as $key => $header) {
                            $arrHeaderData[] = array('field' => $header['fieldName'],'caption' => $header['caption']) ;
                        }
                        $conditions = array('InvoiceMaster.status' => 1,'InvoiceMaster.type' => 4);
                        if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                            $fromDate = $this->AppUtilities->date_format($this->request->data['from_date'],'yyyy-mm-dd');
                            $conditions['InvoiceMaster.invoice_date >='] = $fromDate;
                        }
    
                        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                            $endDate = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                            $conditions['InvoiceMaster.invoice_date <='] = $endDate;
                        }
    
                        if(isset($this->request->data['branch_master_id']) && !empty($this->request->data['branch_master_id'])) {
                            $conditions['InvoiceMaster.branch_master_id'] = $this->request->data['branch_master_id']; 
                        }
                        
                        if(isset($this->request->data['company_year_master_id']) && !empty($this->request->data['company_year_master_id'])) {
                            $conditions['InvoiceMaster.company_year_master_id'] = $this->request->data['company_year_master_id']; 
                        }

                        if(isset($this->request->data['vehicle_type_master_id']) && !empty($this->request->data['vehicle_type_master_id'])) {
                            $conditions['Vehicle.vehicle_type_id'] = $this->request->data['vehicle_type_master_id'];
                        }
                        
                        if(isset($this->request->data['manufacturer_tran_id']) && !empty($this->request->data['manufacturer_tran_id'])) {
                            $conditions['Vehicle.manufacturer_tran_id'] = $this->request->data['manufacturer_tran_id'];
                        }
    
                        if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                            $conditions['Vehicle.vehicle_model_master_id'] = $this->request->data['vehicle_model_master_id'];
                        }

                        if(isset($this->request->data['payment_status']) && !empty($this->request->data['payment_status'])) {
                            $conditions['InvoiceMaster.payment_status'] = intval($this->request->data['payment_status'] - 1);
                        }
                        if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                            $sortBy = (int) $this->request->data['sort_by'];
                            $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                            switch($sortBy) {
                                case 1:
                                        $orderBy = array('order' => 'InvoiceMaster.invoice_date '.$sortyType);
                                        break;
                                case 2:
                                        $orderBy = array('order' => 'InvoiceMaster.max_invoice_no '.$sortyType);
                                        break;
                                default:
                                        $orderBy = array('order' => 'InvoiceMaster.invoice_date '.$sortyType);
                                        break;
                            }
                        } else {
                            $orderBy = array('order' => 'InvoiceMaster.invoice_date ASC');
                        }
                        $options = array(
                            'fields' => array_column($arrFieldData,'columnName'),
                            'joins' => array(
                                array(
                                    'table' => 'vehicle_coupon_masters',
                                    'alias' => 'VehicleCouponMaster',
                                    'type' => 'INNER',
                                    'conditions' => array('InvoiceMaster.customer_master_id = VehicleCouponMaster.id','VehicleCouponMaster.status' => 1)
                                ),
                                array(
                                    'table' => 'coupon_masters',
                                    'alias' => 'CouponMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('VehicleCouponMaster.coupon_master_id = CouponMaster.id','CouponMaster.status' => 1)
                                ),
                                array(
                                    'table' => 'vehicles',
                                    'alias' => 'Vehicle',
                                    'type' => 'INNER',
                                    'conditions' => array('VehicleCouponMaster.vehicle_master_id = Vehicle.id','Vehicle.status' => 1)
                                )
                            ),
                            'conditions' => $conditions,
                        );
                        $options = array_merge($options,$orderBy);
                        $arrRecords = $this->InvoiceMaster->find('all',$options);
                        if(count($arrRecords) > 0) {
                            $arrContentData = array();
                            foreach($arrRecords as $key => $data) {
                                foreach($arrFieldData as $index => $fields) {
                                    if(isset($data[$fields['tableName']][$fields['fieldName']])) {
                                        $arrContentData[$key][$fields['fieldName']] = $data[$fields['tableName']][$fields['fieldName']];
                                    }
                                }
                            }
                            unset($arrFieldData);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'header' => $arrHeaderData,'data' => $arrContentData);
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('SELECT_MANADATORY_FIELDS',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
