<?php
App::uses('AppController','Controller');
class VehicleJobMastersController extends AppController {
    public $name = 'VehicleMasters';
    public $layout = false;
    public $uses = array('VehicleJobMaster','VehicleJob','VehicleMaster','VehicleCouponTran','VehicleCouponMaster','VehicleJobEmployeeTran','ErrorLog','VehicleJobTran','VehicleJobServiceTran','VehicleSpecificationTran','VehicleJobChecklistTran','ProductMaster','ProductVariantStockTran');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /** datatable grid **/
    public function index($type = 1,$dataType = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $dataType = (int) $dataType;
                $type = (int) $type;
                $jobStatus = (isset($this->request->data['job_status'])) ? (int)$this->request->data['job_status'] : 1;
                $comppanyYearId = (isset($this->request->data['company_year_id']) && !empty($this->request->data['company_year_id'])) ? $this->request->data['company_year_id'] : 0;
                $firmId = (isset($this->request->data['firm_id']) && !empty($this->request->data['firm_id'])) ? $this->request->data['firm_id'] : 0;
                $conditions = array('VehicleJob.status' => 1,'VehicleJob.company_year_master_id' => $comppanyYearId,'VehicleJob.branch_master_id' => $firmId);
                if($type === 1) {
                    $conditions['VehicleJob.job_status <='] = 3;
                } else if($type === 2) {
                    $conditions['VehicleJob.job_status'] = array(4,5,6);
                } else if($type === 3) {
                    $conditions['VehicleJob.job_status'] = array(7,8);
                    $conditions['VehicleJob.is_generate_invoice'] = 0;
                } else {}

                $arrVehicleId = array();
                if(isset($this->request->data['vehicle_code']) && !empty($this->request->data['vehicle_code'])) {
                    $conditions['VehicleJob.unique_no LIKE '] = '%'.trim($this->request->data['vehicle_code']).'%';
                }

                if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
                    $conditions['VehicleJob.job_date >= '] = $this->AppUtilities->date_format($this->request->data['start_date'],'yyyy-mm-dd');
                }

                if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                    $conditions['VehicleJob.job_date <= '] = $this->AppUtilities->date_format($this->request->data['end_date'],'yyyy-mm-dd');
                }

                if(isset($this->request->data['job_status']) && !empty($this->request->data['job_status'])) {
                    $conditions['VehicleJob.job_status'] = trim($this->request->data['job_status']);
                }

                if(isset($this->request->data['payment_type']) && !empty($this->request->data['payment_type'])) {
                    $conditions['IM.payment_status'] = intval($this->request->data['payment_type'] - 1);
                }

                if(isset($this->request->data['vehicle_master_id']) && !empty($this->request->data['vehicle_master_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_master_id'])] = trim($this->request->data['vehicle_master_id']);
                }

                /*if(isset($this->request->data['vehicle_number_id']) && !empty($this->request->data['vehicle_number_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_number_id'])] = trim($this->request->data['vehicle_number_id']);
                }

                if(isset($this->request->data['vehicle_model_master_id']) && !empty($this->request->data['vehicle_model_master_id'])) {
                    $conditions['Vehicle.vehicle_model_master_id'] = trim($this->request->data['vehicle_model_master_id']);
                }

                if(isset($this->request->data['vehicle_engine_id']) && !empty($this->request->data['vehicle_engine_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_engine_id'])] = trim($this->request->data['vehicle_engine_id']);
                }

                if(isset($this->request->data['vehicle_chasis_id']) && !empty($this->request->data['vehicle_chasis_id'])) {
                    $arrVehicleId[trim($this->request->data['vehicle_chasis_id'])] = trim($this->request->data['vehicle_chasis_id']);
                }*/

                $orderBy = array();
                if(isset($this->request->data['sort_by']) && !empty($this->request->data['sort_by'])) {
                    $sortBy = (int) $this->request->data['sort_by'];
                    $sortyType = (isset($this->request->data['sort_type']) && $this->request->data['sort_type'] == 1) ? 'ASC' : 'DESC'; 
                    switch($sortBy) {
                        case 1:
                                $orderBy = array('VehicleJob.max_unique_no '.$sortyType);
                                break;
                        case 2:
                                $orderBy = array('VehicleJob.vehicle_number '.$sortyType);
                                break;
                        default:
                                $orderBy = array('VehicleJob.max_unique_no '.$sortyType);
                                break;
                    }
                } else {
                    $orderBy = array('VehicleJob.max_unique_no ASC');
                }

                $joins = array();
                if(isset($this->request->data['employee_master_id']) && !empty($this->request->data['employee_master_id'])) {
                    $conditions['VehicleJobEmployeeTran.employee_master_id'] = trim($this->request->data['employee_master_id']);
                    $joins[] = array(
                                'table' => 'vehicle_job_employee_trans',
                                'alias' => 'VehicleJobEmployeeTran',
                                'type' => 'INNER',
                                'conditions' => array('VehicleJob.id = VehicleJobEmployeeTran.vehicle_job_master_id','VehicleJobEmployeeTran.status' => 1)
                            );
                }

                $fields = array();
                if($type === 5) {
                    $fields = array('IM.id','IM.invoice_no','IM.invoice_date','IM.amount','IM.payment_status','IM.part_amount');
                    $joins[] = array(
                                    'table' => 'invoice_masters',
                                    'alias' => 'IM',
                                    'type' => 'LEFT',
                                    'conditions' => array('VehicleJob.id = IM.customer_master_id','IM.type' => 3,'IM.status' => 1)
                                );
                }

                if(!empty($arrVehicleId)) {
                    $conditions['VehicleJob.vehicle_master_id'] = array_values($arrVehicleId);
                }
                $tableSortType = array();
                $start = 0;
                if($dataType === 1) {
                    $tableCountOptions = array('fields' => array('COUNT(DISTINCT VehicleJob.id) AS cnt'),'conditions' => $conditions,'joins' => $joins,'recursive' => -1);
                    $getResult = $this->VehicleJob->find('first',$tableCountOptions);
                    $totalRecords = $getResult[0]['cnt'];
                    $page = (isset($this->request->data['page'])) ? intval($this->request->data['page']) : 1;
                    $length = isset($this->request->data['length']) ? intval($this->request->data['length']) : 0;
                    $start = ($page - 1) * $length;
                    $end = ($start + $length);
                    $end = ($end > $totalRecords) ? $totalRecords : $end;
                    $tableSortType = array('limit' => $length,'offset' => $start);
                }

                $fields = array_merge(array('id','unique_no','job_date','job_time','employee_name','vehicle_number','job_status','amount','is_generate_invoice'),$fields);
                $tableOptions = array(
                                    'fields' => $fields,
                                    'joins' => $joins,
                                    'conditions' => $conditions,
                                    'group' => array('VehicleJob.id'),
                                    'order' => $orderBy,
                                    'recursive' => -1
                                );
                if(count($tableSortType) > 0) {
                    $tableOptions = array_merge($tableOptions,$tableSortType);
                }
                $arrTableData = $this->VehicleJob->find('all',$tableOptions);
                if(count($arrTableData) > 0) {
                    $records = array();
                    $count = $start;
                    foreach($arrTableData as $key => $tableDetails) {
                        $encryption = trim($this->encryption($tableDetails['VehicleJob']['id']));
                        $status = (int)$tableDetails['VehicleJob']['job_status'];
                        switch($status) {
                            case 1 : $btn = '<div class ="btn btn-xs btn-warning">OPEN</div>';break;
                            case 2 : $btn = '<div class ="btn btn-xs btn-primary">Assigned Senior Technician</div>';break;
                            case 3 : $btn = '<div class ="btn btn-xs btn-primary">Assigned Junior Technician</div>';break;
                            case 4 : $btn = '<div class ="btn btn-xs yellow">In progress</div>';break;
                            case 5 : $btn = '<div class ="btn btn-xs red">HOLD</div>';break;
                            case 6 : $btn = '<div class ="btn btn-xs btn-primary">Re assign</div>';break;
                            case 7 : $btn = '<div class ="btn btn-xs green">Job Done</div>';break;
                            case 8 : $btn = '<div class ="btn btn-xs blue">Completed</div>';break;
                            case 9 : $btn = '<div class ="btn btn-xs purple-plum">Released</div>';break;
                            default: $btn = '<div class ="btn btn-xs btn-warning">OPEN</div>';break;
                        }

                        if($status >= 2 && $status <= 7 && !empty($tableDetails['VehicleJob']['employee_name'])) {
                            $btn .= '<div class = "employee">'.$tableDetails['VehicleJob']['employee_name'].'</div>';
                        }
                        
                        $jobDate  = date('d-m-Y H:i:s',strtotime($tableDetails['VehicleJob']['job_date'].' '.$tableDetails['VehicleJob']['job_time']));
                        $records[$key]['count'] = ++$count;
                        $records[$key]['job_id'] = "<b>".$tableDetails['VehicleJob']['unique_no']."</b><div>".$jobDate."</div>";
                        if($type === 5 && isset($tableDetails['IM']['invoice_no']) && !empty($tableDetails['IM']['invoice_no'])) {
                            $invoiceDate = $this->AppUtilities->date_format($tableDetails['IM']['invoice_date'],'dd-mm-yyyy');
                            $records[$key]['invoice'] = "<b>".$tableDetails['IM']['invoice_no']."</b><div>".$invoiceDate."</div>";
                            $records[$key]['payment_type'] = (int) $tableDetails['IM']['payment_status'];
                            $records[$key]['invoice_id'] = $this->encryption($tableDetails['IM']['id']);
                            $records[$key]['amount'] = number_format($tableDetails['IM']['amount'],2,'.','');
                        } else {
                            $records[$key]['amount'] = number_format($tableDetails['VehicleJob']['amount'],2,'.','');
                        }
                        $records[$key]['id'] = $encryption;
                        $records[$key]['date'] = $jobDate;
                        $records[$key]['status'] = ($dataType == 1) ? $btn : str_replace(" ","\n",strip_tags($btn));
                        $records[$key]['job_code'] = $tableDetails['VehicleJob']['unique_no'];
                        $records[$key]['vehicle'] = $tableDetails['VehicleJob']['vehicle_number'];
                        $records[$key]['job_status'] = (int) $tableDetails['VehicleJob']['job_status'];
                        $records[$key]['generate_invoice'] = (int) $tableDetails['VehicleJob']['is_generate_invoice'];
                        $records[$key]['is_exists'] = ($tableDetails['VehicleJob']['job_status'] >= 7) ? 1 : 0;
                    }
                    if($dataType === 1) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'total' => $totalRecords,'start' => $start + 1,'end' => $end);
                    } else {
                        if($type === 1) {
                            $headers = array('count'=>'S.No','job_code'=>'Job ID','date' => 'Date','vehicle'=>'Vehicle No','amount' => 'Amount','status'=>'Job Status');
                        } else if($type === 2) {
                            $headers = array('count'=>'S.No','job_code'=>'Job ID','date' => 'Date','vehicle'=>'Vehicle No','amount' => 'Amount','status'=>'Job Status');
                        } else if($type === 3) {
                            $headers = array('count'=>'S.No','job_code'=>'Job ID','date' => 'Date','vehicle'=>'Vehicle No','amount' => 'Amount','status'=>'Job Status');
                        } else if($type === 5) {
                            $headers = array('count'=>'S.No','job_id'=>'Job ID','invoice' => 'Invoice No','vehicle'=>'Vehicle No','amount' => 'Amount','status'=>'Job Status');
                        } else {
                            $headers = array();
                        }
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'records' => $records,'headers' => $headers);
                    }
                    $statusCode = 200;
                } else {
                    $statusCode = 200;
                    $response = $records = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $arrSaveRecords['VehicleJobMaster'] = $this->request->data;
                if(isset($arrSaveRecords['VehicleJobMaster']['id']) && !empty($arrSaveRecords['VehicleJobMaster']['id'])) {
                    $arrSaveRecords['VehicleJobMaster']['id'] = $this->decryption($arrSaveRecords['VehicleJobMaster']['id']);
                }
                $arrSaveRecords['VehicleJobMaster']['in_house_vehicle'] = ($arrSaveRecords['VehicleJobMaster']['in_house_vehicle'] == 1) ? 1 : 0; 
                $arrSaveRecords['VehicleJobMaster']['vehicle_master_id'] = (isset($arrSaveRecords['VehicleJobMaster']['vehicle_master_id'])) ? $arrSaveRecords['VehicleJobMaster']['vehicle_master_id'] : '';
                $arrSaveRecords['VehicleJobMaster']['branch_master_id'] = (isset($arrSaveRecords['VehicleJobMaster']['branch_master_id'])) ? $arrSaveRecords['VehicleJobMaster']['branch_master_id'] : '';
                $arrSaveRecords['VehicleJobMaster']['company_year_master_id'] = isset($arrSaveRecords['VehicleJobMaster']['company_year_master_id']) ? $arrSaveRecords['VehicleJobMaster']['company_year_master_id'] : '';
                $arrSaveRecords['VehicleJobMaster']['job_json'] = "{}";
                
                $arrChecklistData =  array();
                if(isset($arrSaveRecords['VehicleJobMaster']['arr_checklist']) && !empty($arrSaveRecords['VehicleJobMaster']['arr_checklist'])) {
                    $arrChecklistData = $arrSaveRecords['VehicleJobMaster']['arr_checklist'];
                    unset($arrSaveRecords['VehicleJobMaster']['arr_checklist']);
                }

                if(isset($arrSaveRecords['VehicleJobMaster']['complain']) && !empty($arrSaveRecords['VehicleJobMaster']['complain'])) {
                    $remark = array();
                    $count = 0;
                    foreach($arrSaveRecords['VehicleJobMaster']['complain'] as $key => $complain) {
                        if(!empty($complain)) {
                            $remark['remark'][$key]['complain'] = $complain;
                            $remark['remark'][$key]['remark'] = isset($arrSaveRecords['VehicleJobMaster']['remark'][$key]) ? $arrSaveRecords['VehicleJobMaster']['remark'][$key] : '';
                            ++$count;
                        }
                    }
                    $arrSaveRecords['VehicleJobMaster']['extra_information'] = json_encode($remark);
                    unset($arrSaveRecords['VehicleJobMaster']['complain'],$arrSaveRecords['VehicleJobMaster']['remark']);
                }
                
                $this->VehicleJobMaster->set($arrSaveRecords);
                if($this->VehicleJobMaster->validates()) {
                    #pr($arrSaveRecords);exit;
                    $dataSource = $this->VehicleJobMaster->getDataSource();
                    try {
                        $arrVehicleJodCheclistDeleteRecords = array();
                        $firmId = $arrSaveRecords['VehicleJobMaster']['branch_master_id'];
                        unset($arrSaveRecords['VehicleJobMaster']['product_quantity'],$arrSaveRecords['VehicleJobMaster']['product_price'],$arrSaveRecords['VehicleJobMaster']['product_master_id']);
                        $arrSaveRecords['VehicleJobMaster']['job_date'] = $this->AppUtilities->date_format($arrSaveRecords['VehicleJobMaster']['job_date'],'yyyy-mm-dd');
                        $arrSaveRecords['VehicleJobMaster']['job_time'] = date('H:i:s',strtotime($arrSaveRecords['VehicleJobMaster']['job_time']));
                        $arrSaveRecords['VehicleJobMaster']['user_id'] = $this->Session->read('sessUserId');
                        #pr($arrSaveRecords);exit;
                        if(isset($arrSaveRecords['VehicleJobMaster']['id']) && !empty($arrSaveRecords['VehicleJobMaster']['id'])) {
                            $options = array('fields' => array('inspection_master_id','id'),'conditions' => array('status' => 1,'vehicle_job_master_id' => $arrSaveRecords['VehicleJobMaster']['id']));
                            $arrVehicleJobChecklistRecords = $this->VehicleJobChecklistTran->find('list',$options);
                            if(!empty($arrVehicleJobChecklistRecords)) {
                                $arrVehicleJodCheclistDeleteRecords = array_diff(array_keys($arrVehicleJobChecklistRecords),$arrChecklistData);
                            }
                        } else {
                            $tableName = 'AutoJob'.$firmId.'Code';
                            $this->loadModel($tableName);
                            $this->$tableName->create();
                            $arrVehicleCode[$tableName]['name'] = $arrSaveRecords['VehicleJobMaster']['vehicle_master_id'];
                            if(!$this->$tableName->save($arrVehicleCode)) {
                                throw new Exception(__('Auto Vehicle Code could not saved',true));
                            }
                            $maxNumber = $this->$tableName->getInsertID();
                            $arrVehicleJobCodeData = $this->AppUtilities->getUserSettingCode($firmId,3,$maxNumber);
                            if(!empty($arrVehicleJobCodeData)) {
                                $arrSaveRecords['VehicleJobMaster']['unique_no'] = $arrVehicleJobCodeData['code'];
                                $arrSaveRecords['VehicleJobMaster']['max_unique_no'] = $arrVehicleJobCodeData['maxNumber'];
                            }
                            $date = $arrSaveRecords['VehicleJobMaster']['job_date'].' '.$arrSaveRecords['VehicleJobMaster']['job_time'];
                            $arrSaveRecords['VehicleJobMaster']['job_status']  = 1;
                            $arrSaveRecords['VehicleJobMaster']['job_status_date']  = $date;
                            $arrHistory = array();
                            $arrHistory[] = array('status' => 1,'date' =>$date);
                            $arrSaveRecords['VehicleJobMaster']['history'] = json_encode($arrHistory);
                            $this->VehicleJobMaster->create();
                        }
                        #pr($arrSaveRecords);exit;
                        $fieldList = array('id','unique_no','max_unique_no','job_date','job_time','completion_date','completion_time','service_km','customer_name','customer_mobile_no','customer_email_id','customer_address','job_status','extra_information','job_status_date','amount','history','vehicle_master_id','employee_master_id','company_year_master_id','branch_master_id','in_house_vehicle','user_id');
                        if(!$this->VehicleJobMaster->save($arrSaveRecords,array('fieldList' => $fieldList,'validate' => false))) {
                            throw new Exception(__('Record could not saved properly, please try again later!',true));
                        }

                        if(!isset($arrSaveRecords['VehicleJobMaster']['id'])) {
                            $arrSaveRecords['VehicleJobMaster']['id'] = $this->VehicleJobMaster->getInsertID();
                            if(isset($arrSaveRecords['VehicleJobMaster']['employee_master_id']) && !empty($arrSaveRecords['VehicleJobMaster']['employee_master_id'])) {
                                $arrSaveEmployeeRecords['VehicleJobEmployeeTran']['vehicle_job_master_id'] = $arrSaveRecords['VehicleJobMaster']['id'];
                                $arrSaveEmployeeRecords['VehicleJobEmployeeTran']['employee_master_id'] = $arrSaveRecords['VehicleJobMaster']['employee_master_id'];
                                $arrSaveEmployeeRecords['VehicleJobEmployeeTran']['branch_master_id'] = $arrSaveRecords['VehicleJobMaster']['branch_master_id'];
                                if(!$this->VehicleJobEmployeeTran->save($arrSaveEmployeeRecords)) {
                                    throw new Exception(__('Vehicle Job Employee detail could not saved properly, please try again later!',true));
                                }
                                unset($arrSaveEmployeeRecords);
                            }
                        }

                        if(count($arrChecklistData) > 0) {
                            $arrVehicleChecklistTran = array();
                            $counter = 0;
                            foreach($arrChecklistData as $key => $checklistId) {
                                if(isset($arrVehicleJobChecklistRecords[$checklistId])) {
                                    $arrVehicleChecklistTran[$counter]['VehicleJobChecklistTran']['id'] = $arrVehicleJobChecklistRecords[$checklistId];
                                }
                                $arrVehicleChecklistTran[$counter]['VehicleJobChecklistTran']['vehicle_job_master_id'] = $arrSaveRecords['VehicleJobMaster']['id'];
                                $arrVehicleChecklistTran[$counter]['VehicleJobChecklistTran']['inspection_master_id'] = $checklistId;
                                $arrVehicleChecklistTran[$counter]['VehicleJobChecklistTran']['branch_master_id'] = $firmId;
                                ++$counter;
                            }
                            #pr($arrVehicleChecklistTran);exit;
                            $fieldList = array('id','vehicle_job_master_id','branch_master_id','inspection_master_id');
                            if(!$this->VehicleJobChecklistTran->saveAll($arrVehicleChecklistTran,array('fieldList' => $fieldList))) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($arrVehicleChecklistTran,$fieldList);
                        }

                        /** delete unused vehicle job checklist records **/
                        if(count($arrVehicleJodCheclistDeleteRecords)) {
                            $updateFields['VehicleJobChecklistTran.status'] = 0;
                            $updateFields['VehicleJobChecklistTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                            $updateParams['VehicleJobChecklistTran.inspection_master_id'] = $arrVehicleJodCheclistDeleteRecords;
                            if(!$this->VehicleJobChecklistTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            unset($updateFields,$updateParams,$arrVehicleJodCheclistDeleteRecords);
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        if(isset($arrSaveRecords['VehicleJobMaster']['id'])) {
                            $response = array('status' => 1,'message' => __('UPDATED_RECORD',true));
                        } else {
                            $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                        }
                        unset($this->request->data);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        unset($arrErrorLogs,$arrSaveRecords);
                    }
                } else {
                    $validationErrros = Set::flatten($this->VehicleJobMaster->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
            unset($this->request->data);
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_product() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                $this->request->data['id'] = (isset($this->request->data['id']) && !empty($this->request->data['id'])) ? $this->decryption($this->request->data['id']) : 0;
                $options = array('fields' => array('id','job_json'),'conditions' => array('id' => $this->request->data['id'],'status' => 1));
                $arrVehicleJobDetail = $this->VehicleJobMaster->find('first',$options);
                #pr($arrVehicleJobDetail);exit;
                if(count($arrVehicleJobDetail)) {
                    $dataSource = $this->VehicleJobMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $userId = $this->Session->read('sessUserId');
                        $branchId = $this->request->data['branch_master_id'];
                        $companyYearId = $this->request->data['company_year_master_id'];
                        $arrVehicleJob = $arrSpareProductData = array();
                        $arrSaveVehicleJobData['VehicleJobMaster']['id'] = $this->request->data['id'];
                        $this->request->data['trans_type'] = (isset($this->request->data['trans_type'])) ? $this->request->data['trans_type'] : 2;
                        if($this->request->data['trans_type'] == 2) {
                            $arrSaveVehicleJobData['VehicleJobMaster']['discount'] = number_format($this->request->data['discount'],2,'.','');
                            if(isset($this->request->data['coupon']) && !empty($this->request->data['coupon'])) {
                                $arrSaveVehicleJobData['VehicleJobMaster']['coupon'] = number_format($this->request->data['coupon'],2,'.','');
                            }
                            $arrSaveVehicleJobData['VehicleJobMaster']['amount'] = number_format($this->request->data['amount'],2,'.','');
                            if(isset($this->request->data['vehicle_coupon_tran_id']) && !empty($this->request->data['vehicle_coupon_tran_id'])) {
                                $arrSaveVehicleJobData['VehicleJobMaster']['vehicle_coupon_tran_id'] = $this->request->data['vehicle_coupon_tran_id'];
                            }
                            if(isset($this->request->data['vehicle_job_json']) && !empty($this->request->data['vehicle_job_json'])) {
                                $arrVehicleJobData = array();
                                $arrVehicleJob = json_decode($this->request->data['vehicle_job_json'],true);
                                $arrVehicleJobData = $arrVehicleJob;
                                if(!empty($arrVehicleJobDetail['VehicleJobMaster']['job_json'])) {
                                    $arrVehicleOldData = json_decode($arrVehicleJobDetail['VehicleJobMaster']['job_json'],true);
                                    if(isset($arrVehicleOldData['spare_product']) && !empty($arrVehicleOldData['spare_product'])) {
                                        $arrVehicleJobData['spare_product'] = $arrVehicleOldData['spare_product'];
                                    }
                                }
                                $arrSaveVehicleJobData['VehicleJobMaster']['job_json'] = json_encode($arrVehicleJobData);
                            }
                        } else if($this->request->data['trans_type'] == 1) {
                            if(isset($this->request->data['spare_product']) && !empty($this->request->data['spare_product'])) {
                                $arrVehicleJobData = array();
                                $arrSpareProductData['spare_product'] = json_decode($this->request->data['spare_product'],true);
                                if(!empty($arrVehicleJobDetail['VehicleJobMaster']['job_json'])) {
                                    $arrVehicleJobData = json_decode($arrVehicleJobDetail['VehicleJobMaster']['job_json'],true);
                                    unset($arrVehicleJobData['spare_product']);
                                }
                                $arrVehicleJobData['spare_product'] = $arrSpareProductData['spare_product'];
                                $arrSaveVehicleJobData['VehicleJobMaster']['job_json'] = json_encode($arrVehicleJobData);
                            }
                        } else {}
                        
                        #pr($arrSaveVehicleJobData);exit;
                        if(!$this->VehicleJobMaster->save($arrSaveVehicleJobData,array('validate' => false))) {
                            throw new Exception(__('vehicle job could not saved properly !.',true));
                        }
                        
                        $arrSaveVehicleJobTranData = array();
                        if(isset($arrVehicleJob['product']) && !empty($arrVehicleJob['product'])) {
                            $count = 0;
                            foreach($arrVehicleJob['product'] as $key => $product) {
                                if(isset($product['vehicle_job_tran_id'])) {
                                    $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['id'] = $product['vehicle_job_tran_id'];
                                }
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['vehicle_job_master_id'] = $this->request->data['id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['reference_tran_id'] = $product['product_variant_tran_id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['tax_group_master_id'] = (isset($product['tax_group_master_id'])) ? $product['tax_group_master_id'] : '';
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['unit_price'] = number_format($product['price'],2,'.','');
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['quantity'] = (int) $product['quantity'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['tax'] = $product['item_tax'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['discount'] = $product['discount'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['discount_price'] = number_format($product['discount_amount'],2,'.','');;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['subtotal'] = number_format($product['subtotal'],2,'.','');;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['total'] = number_format($product['total'],2,'.','');;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['type'] = 1;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['tax_json'] = json_encode($product['tax']);
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['branch_master_id'] = $branchId;
                                ++$count;
                            }
                        }

                        if(isset($arrVehicleJob['service']) && !empty($arrVehicleJob['service'])) {
                            $count = count($arrSaveVehicleJobTranData);
                            foreach($arrVehicleJob['service'] as $key => $product) {
                                if(isset($product['vehicle_job_tran_id'])) {
                                    $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['id'] = $product['vehicle_job_tran_id'];
                                }
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['vehicle_job_master_id'] = $this->request->data['id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['reference_tran_id'] = $product['id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['unit_price'] = $product['price'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['subtotal'] = $product['subtotal'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['type'] = 2;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['quantity'] = 1;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['tax_json'] = json_encode(array());
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['total'] = $product['total'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['branch_master_id'] = $branchId;
                                ++$count;
                            }
                        }

                        if(isset($arrSpareProductData['spare_product']) && count($arrSpareProductData['spare_product']) > 0) {
                            $count = 0;
                            foreach($arrSpareProductData['spare_product'] as $key => $product) {
                                if(isset($product['vehicle_job_tran_id'])) {
                                    $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['id'] = $product['vehicle_job_tran_id'];
                                }
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['vehicle_job_master_id'] = $this->request->data['id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['reference_tran_id'] = $product['product_variant_tran_id'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['unit_price'] = $product['price'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['subtotal'] = $product['subtotal'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['type'] = 1;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['product_type'] = 2;
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['quantity'] = $product['quantity'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['tax_json'] = json_encode(array());
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['total'] = $product['total'];
                                $arrSaveVehicleJobTranData[$count]['VehicleJobTran']['branch_master_id'] = $branchId;
                                ++$count;
                            }
                        }

                        if(count($arrSaveVehicleJobTranData) > 0) {
                            if(!$this->VehicleJobTran->saveAll($arrSaveVehicleJobTranData)) {
                                throw new Exception(__('vehicle job detail could not saved properly !.',true));
                            }
                        }

                        $date = date('Y-m-d');
                        $id = $this->request->data['id'];
                        if($this->request->data['trans_type'] == 2) {
                            $date = date('Y-m-d H:i:s');
                            $stockQuery = "INSERT INTO product_variant_stock_trans (`date`,`reference_master_id`,`product_variant_tran_id`,`old_product_variant_tran_id`,`parent_id`,`quantity`,`type`,`stock_type`,`tran_type`,`from_type`,`branch_master_id`,`company_year_master_id`,`user_id`,`created`,`modified`)
                                            SELECT '".$date."','".$id."',product_variant_tran_id,product_variant_tran_id,product_variant_stock_tran_id,quantity,1,2,3,3,'".$branchId."','".$companyYearId."','".$userId."',NOW(),NOW()
                                            FROM tmp_product_variant_stock_trans WHERE user_id = '".$userId."'";
                            $this->VehicleJobTran->query($stockQuery,false);

                            if(isset($this->request->data['vehicle_coupon_tran_id']) && !empty($this->request->data['vehicle_coupon_tran_id'])) {
                                $updateFields = $updateParams = array();
                                $updateFields['VehicleCouponTran.type'] = $this->request->data['expired'];
                                $updateFields['VehicleCouponTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['VehicleCouponTran.id'] = $this->request->data['vehicle_coupon_tran_id'];
                                $this->VehicleCouponTran->updateAll($updateFields,$updateParams);
                                unset($updateFields,$updateParams);
                            }
                        } else if($this->request->data['trans_type'] == 1) {
                            $date = date('Y-m-d H:i:s');
                            $stockQuery = "INSERT INTO product_variant_stock_trans (`date`,`reference_master_id`,`product_variant_tran_id`,`old_product_variant_tran_id`,`quantity`,`type`,`stock_type`,`tran_type`,`from_type`,`branch_master_id`,`company_year_master_id`,`user_id`,`created`,`modified`)
                                            SELECT '".$date."','".$id."',product_variant_tran_id,product_variant_tran_id,quantity,1,1,1,3,'".$branchId."','".$companyYearId."','".$userId."',NOW(),NOW()
                                            FROM tmp_product_variant_stock_trans WHERE user_id = '".$userId."'";
                            $this->VehicleJobTran->query($stockQuery,false);
                        } else {}

                        $this->loadModel('TmpProductVariantStockTran');
                        $this->TmpProductVariantStockTran->deleteAll(array('user_id' => $this->Session->read('sessUserId'),'branch_master_id' => $branchId),false);
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('JOB_PRODUCT_SAVED',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        unset($arrErrorLogs,$arrSaveRecords);
                    }
                } else {
                    $response = array('status' => 0,'message' => __('NO_RECORD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function edit($id = null,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(isset($id) && !empty($id) && !empty($firmId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                                    'fields' => array('id','unique_no','job_date','job_time','completion_date','completion_time','extra_information','vehicle_master_id','vehicle_code','vehicle_number','manufacturer','vehicle_model','engine_number','chasis_number','vehicle_type','customer_name','customer_mobile_no','customer_email_id','customer_address','in_house_vehicle','service_km'),
                                    'conditions' => array('VehicleJob.status' => 1,'VehicleJob.id' => $id,'VehicleJob.branch_master_id' => $firmId),
                                    'recursive' => -1
                                );
                    $arrRecords = $this->VehicleJob->find('first',$options);
                    if(count($arrRecords) > 0) {
                        $options = array(
                                        'fields' => array('inspection_master_id','id'),
                                        'conditions' => array('vehicle_job_master_id' => $arrRecords['VehicleJob']['id'],'status' => 1)
                                    );
                        $arrVehicleJobChecklistData = $this->VehicleJobChecklistTran->find('list',$options);
                        $statusCode = 200;
                        $arrRecords['VehicleJob']['id'] = $this->encryption($arrRecords['VehicleJob']['id']);
                        $arrRecords['VehicleJob']['job_date'] = $this->AppUtilities->date_format($arrRecords['VehicleJob']['job_date'],'dd-mm-yyyy');
                        $arrRecords['VehicleJob']['job_time'] = date('g:i:s A',strtotime($arrRecords['VehicleJob']['job_time']));
                        $arrRecords['VehicleJob']['completion_date'] = $this->AppUtilities->date_format($arrRecords['VehicleJob']['completion_date'],'dd-mm-yyyy');
                        $arrRecords['VehicleJob']['completion_time'] = date('g:i:s A',strtotime($arrRecords['VehicleJob']['completion_time']));
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleJob'],'checklist' => $arrVehicleJobChecklistData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'firmId' => $firmId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function view($id = null,$firmId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($firmId)) {
                    $id = $this->decryption(trim($id));
                    $options = array(
                                    'fields' => array('id','unique_no','amount','job_date','job_time','completion_date','completion_time','extra_information','vehicle_master_id','vehicle_code','vehicle_number','manufacturer','vehicle_model','engine_number','chasis_number','vehicle_type','customer_name','customer_mobile_no','customer_email_id','customer_address','job_json','service_km','in_house_vehicle'),
                                    'conditions' => array('VehicleJob.status' => 1,'VehicleJob.id' => $id,'VehicleJob.branch_master_id' => $firmId),
                                    'recursive' => -1
                                );
                    $arrRecords = $this->VehicleJob->find('first',$options);
                    if(count($arrRecords) > 0) {
                        $options = array(
                                        'fields' => array('id','IM.name'),
                                        'joins' => array(
                                            array(
                                                'table' => 'inspection_masters',
                                                'alias' => 'IM',
                                                'type' => 'INNER',
                                                'conditions' => array('VehicleJobChecklistTran.inspection_master_id = IM.id','IM.status' => 1)
                                            )
                                        ),
                                        'conditions' => array('VehicleJobChecklistTran.vehicle_job_master_id' => $arrRecords['VehicleJob']['id'],'VehicleJobChecklistTran.status' => 1)
                                    );
                        $arrVehicleJobChecklistData = $this->VehicleJobChecklistTran->find('list',$options);
                        $statusCode = 200;
                        $arrRecords['VehicleJob']['id'] = $this->encryption($arrRecords['VehicleJob']['id']);
                        $arrRecords['VehicleJob']['job_date'] = $this->AppUtilities->date_format($arrRecords['VehicleJob']['job_date'],'dd-mm-yyyy');
                        $arrRecords['VehicleJob']['job_time'] = date('g:i:s A',strtotime($arrRecords['VehicleJob']['job_time']));
                        $arrRecords['VehicleJob']['completion_date'] = $this->AppUtilities->date_format($arrRecords['VehicleJob']['completion_date'],'dd-mm-yyyy');
                        $arrRecords['VehicleJob']['completion_time'] = date('g:i:s A',strtotime($arrRecords['VehicleJob']['completion_time']));
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleJob'],'checklist' => $arrVehicleJobChecklistData);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'firmId' => $firmId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function vehicle_coupon_detail($vehicleId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($vehicleId)) {

                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $vehicleId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function view_status($id = 0,$firmId = 0,$type = 1) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                if(!empty($id) && !empty($firmId)) {
                    $id = $this->decryption(trim($id));
                    $type = (int) $type;
                    $options = array(
                                    'fields' => array('id','unique_no','job_date','job_time','job_status','vehicle_code','vehicle_number','manufacturer','vehicle_model','service_km','vehicle_master_id','customer_name','customer_mobile_no','job_json','vehicle_type','amount','discount','coupon'),
                                    'conditions' => array('VehicleJob.status' => 1,'VehicleJob.id' => $id,'VehicleJob.branch_master_id' => $firmId),
                                    'recursive' => -1
                                );
                    $arrRecords = $this->VehicleJob->find('first',$options);
                    if(!empty($arrRecords)) {
                        $arrRecords['VehicleJob']['id'] = $this->encryption($arrRecords['VehicleJob']['id']);
                        $arrJobChecklistData = $vehicleCouponDetail = $arrVehicleSpareProductDetail = array();
                        $arrRecords['VehicleJob']['amount'] = number_format($arrRecords['VehicleJob']['amount'],2,'.','');
                        if($type === 1) {
                            $options = array(
                                'fields' => array('id','IM.name'),
                                'joins' => array(
                                    array(
                                        'table' => 'inspection_masters',
                                        'alias' => 'IM',
                                        'type' => 'INNER',
                                        'conditions' => array('VehicleJobChecklistTran.inspection_master_id = IM.id','IM.status' => 1)
                                    )
                                ),
                                'conditions' => array('VehicleJobChecklistTran.vehicle_job_master_id' => $id,'VehicleJobChecklistTran.status' => 1),
                                'order' => 'IM.order_no'
                            );
                            $arrVehicleJobChecklistData = $this->VehicleJobChecklistTran->find('all',$options);
                            if(!empty($arrVehicleJobChecklistData)) {
                                foreach($arrVehicleJobChecklistData as $key => $jobList) {
                                    $arrJobChecklistData[$key]['id'] = $jobList['VehicleJobChecklistTran']['id'];
                                    $arrJobChecklistData[$key]['name'] = $jobList['IM']['name'];
                                    $arrJobChecklistData[$key]['value'] = 1;
                                }
                            }
                        } else if($type === 2) {
                            if(!empty($arrRecords['VehicleJob']['job_json'])) {
                                $arrVehicleJobDetail = json_decode($arrRecords['VehicleJob']['job_json'],true);
                                $arrVehicleJobDetail['product'] = Hash::combine($arrVehicleJobDetail['product'],'{n}.product_variant_tran_id','{n}');
                                $arrVehicleJobDetail['service'] = Hash::combine($arrVehicleJobDetail['service'],'{n}.id','{n}');
                                $options = array('fields' => array('id','reference_tran_id'),'conditions' => array('vehicle_job_master_id' => $id,'status' => 1,'product_type' => 1));
                                $arrVehicleJobTranDetail = $this->VehicleJobTran->find('all',$options);
                                if(!empty($arrVehicleJobTranDetail)) {
                                    foreach($arrVehicleJobTranDetail as $key => $product) {
                                        if(isset($arrVehicleJobDetail['product'][$product['VehicleJobTran']['reference_tran_id']])) {
                                            $arrVehicleJobDetail['product'][$product['VehicleJobTran']['reference_tran_id']]['vehicle_job_tran_id'] = $product['VehicleJobTran']['id'];
                                        }
                                        if(isset($arrVehicleJobDetail['service'][$product['VehicleJobTran']['reference_tran_id']])) {
                                            $arrVehicleJobDetail['service'][$product['VehicleJobTran']['reference_tran_id']]['vehicle_job_tran_id'] = $product['VehicleJobTran']['id'];
                                        }
                                    }
                                    if(isset($arrVehicleJobDetail['service_detail']['coupon_detail']) && !empty($arrVehicleJobDetail['service_detail']['coupon_detail'])) {
                                        $vehicleCouponDetail = $arrVehicleJobDetail['service_detail']['coupon_detail'];
                                    }
                                    $arrVehicleJobDetail['product'] = array_values($arrVehicleJobDetail['product']);
                                    $arrVehicleJobDetail['service'] = array_values($arrVehicleJobDetail['service']);
                                    $arrRecords['VehicleJob']['job_json'] = json_encode($arrVehicleJobDetail);
                                    unset($arrVehicleJobDetail);
                                }
                            } else {
                                $vehicleCouponDetail = $this->AppUtilities->getVehicleCouponDetail($firmId,$arrRecords['VehicleJob']['vehicle_master_id'],$arrRecords['VehicleJob']['service_km']);
                            }
                        } else if($type === 3) {
                            if(!empty($arrRecords['VehicleJob']['job_json'])) {
                                $arrVehicleJobDetail = json_decode($arrRecords['VehicleJob']['job_json'],true);
                                if(isset($arrVehicleJobDetail['spare_product']) && !empty($arrVehicleJobDetail['spare_product'])) {
                                    $arrVehicleSpareProductDetail = Hash::combine($arrVehicleJobDetail['spare_product'],'{n}.product_variant_tran_id','{n}');
                                    $options = array('fields' => array('id','reference_tran_id'),'conditions' => array('VehicleJobTran.vehicle_job_master_id' => $id,'VehicleJobTran.status' => 1,'VehicleJobTran.product_type' => 2));
                                    $arrVehicleJobTranDetail = $this->VehicleJobTran->find('all',$options);
                                    if(count($arrVehicleJobTranDetail)) {
                                        $count = 0;
                                        foreach($arrVehicleJobTranDetail as $key => $product) {
                                            if(isset($arrVehicleSpareProductDetail[$product['VehicleJobTran']['reference_tran_id']])) {
                                                $arrVehicleSpareProductDetail[$product['VehicleJobTran']['reference_tran_id']]['vehicle_job_tran_id'] = $product['VehicleJobTran']['id'];
                                            }
                                            ++$count;
                                        }
                                        $arrVehicleSpareProductDetail = array_values($arrVehicleSpareProductDetail);
                                    }
                                }
                            }
                        } else if($type === 4) {
                            $arrProductDetail = array();
                            $arrRecords['VehicleJob']['enable_service_discount'] = 1;
                            $arrRecords['VehicleJob']['amount'] = (float) $arrRecords['VehicleJob']['amount'];
                            $arrRecords['VehicleJob']['discount'] = (float) $arrRecords['VehicleJob']['discount'];
                            $arrRecords['VehicleJob']['coupon'] = (float) $arrRecords['VehicleJob']['coupon'];
                            $arrRecords['VehicleJob']['coupon_name'] = '';
                            if($arrRecords['VehicleJob']['coupon'] > 0) {
                                if(!empty($arrRecords['VehicleJob']['job_json'])) {
                                    $arrJobData = json_decode($arrRecords['VehicleJob']['job_json'],true);
                                    if(isset($arrJobData['service_detail']['coupon_detail']) && !empty($arrJobData['service_detail']['coupon_detail'])) {
                                        $arrRecords['VehicleJob']['coupon_name'] = $arrJobData['service_detail']['coupon_detail']['name'].' ('.$arrJobData['service_detail']['coupon_detail']['service'].')';
                                    }
                                }
                                $arrRecords['VehicleJob']['enable_service_discount'] = 0;
                            }
                            unset($arrRecords['VehicleJob']['job_json']);
                            $query = "SELECT sum(vehicle_job_trans.total) as amount,type
                                        FROM vehicle_job_trans
                                        WHERE vehicle_job_trans.`status` = 1 AND vehicle_job_trans.`vehicle_job_master_id` = '".$id."' AND vehicle_job_trans.`product_type` = 1
                                        AND vehicle_job_trans.`type` = 1
                                        UNION ALL
                                        SELECT sum(vehicle_job_trans.total) as amount,type
                                        FROM vehicle_job_trans
                                        WHERE vehicle_job_trans.`status` = 1 AND vehicle_job_trans.`vehicle_job_master_id` = '".$id."' AND vehicle_job_trans.`product_type` = 1
                                        AND vehicle_job_trans.`type` = 2;";
                            $vehicleJobData = $this->VehicleJobMaster->query($query,false);
                            if(!empty($vehicleJobData)) {
                                $arrRecords['VehicleJob']['part_amount'] = (isset($vehicleJobData[0][0]['amount'])) ? (float) number_format($vehicleJobData[0][0]['amount'],2,'.','') : 0;
                                $arrRecords['VehicleJob']['service_amount'] = (isset($vehicleJobData[1][0]['amount'])) ? (float) number_format($vehicleJobData[1][0]['amount'],2,'.','') : 0;
                            }
                        } else {}
                        $arrRecords['VehicleJob']['date']  = date('d-m-Y H:i:s',strtotime($arrRecords['VehicleJob']['job_date'].' '.$arrRecords['VehicleJob']['job_time']));
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords['VehicleJob'],'checklist' => $arrJobChecklistData,'coupon' => $vehicleCouponDetail,'spare_product' => $arrVehicleSpareProductDetail);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' => $id,'firmid' => $firmId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save_status() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && isset($this->request->data['branch_master_id']) && isset($this->request->data['job_status']) && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                    $dataSource = $this->VehicleJobMaster->getDataSource();
                    try {
                        $dataSource->begin();
                        $options = array('fields' => array('id','history'),'conditions' => array('id' => $this->request->data['id'],'branch_master_id' => $this->request->data['branch_master_id'],'status' => 1));
                        $arrVehicleJobData = $this->VehicleJobMaster->find('first',$options);
                        if(!empty($arrVehicleJobData)) {
                            $arrHistoryData = (!empty($arrVehicleJobData['VehicleJobMaster']['history'])) ? json_decode($arrVehicleJobData['VehicleJobMaster']['history'],true) : array();
                            $history['status'] = $this->request->data['job_status'];
                            $history['date']   = date('Y-m-d H:i:s');
                            $history['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                            if(isset($this->request->data['status_date']) && !empty($this->request->data['status_date'])) {
                                $history['status_date'] = $this->AppUtilities->date_format($this->request->data['status_date'],'yyyy-mm-dd');
                                $updateVehicleJobStatusData['VehicleJobMaster']['job_status_date'] = $this->AppUtilities->date_format($this->request->data['status_date'],'yyyy-mm-dd');
                            } else {
                                $updateVehicleJobStatusData['VehicleJobMaster']['job_status_date'] = date('Y-m-d H:i:s');
                            }
                            if(isset($this->request->data['next_service_date']) && !empty($this->request->data['next_service_date'])) {
                                $updateVehicleJobStatusData['VehicleJobMaster']['next_service_date'] = $this->AppUtilities->date_format($this->request->data['next_service_date'],'yyyy-mm-dd');
                            }
                            $arrVehicleEmployeeData = array();
                            if(isset($this->request->data['employee_master_id']) && !empty($this->request->data['employee_master_id'])) {
                                $history['department_master_id'] = $this->request->data['department_master_id'];
                                $history['designation_master_id'] = $this->request->data['designation_master_id'];
                                $history['employee_master_id'] = $this->request->data['employee_master_id'];
                                $history['employee_name'] = $this->request->data['employee_name'];
                                $arrVehicleEmployeeData['VehicleJobEmployeeTran']['employee_master_id'] = $this->request->data['employee_master_id'];
                                $arrVehicleEmployeeData['VehicleJobEmployeeTran']['vehicle_job_master_id'] = $this->request->data['id'];
                                $arrVehicleEmployeeData['VehicleJobEmployeeTran']['job_status'] = $this->request->data['job_status'];
                                $arrVehicleEmployeeData['VehicleJobEmployeeTran']['branch_master_id'] = $this->request->data['branch_master_id'];
                            }
                            $arrHistoryData[] = $history;
                            $updateVehicleJobStatusData['VehicleJobMaster']['history'] = json_encode($arrHistoryData);
                            $updateVehicleJobStatusData['VehicleJobMaster']['job_status'] = $this->request->data['job_status'];
                            $updateVehicleJobStatusData['VehicleJobMaster']['job_status_date'] = date('Y-m-d H:i:s');
                            $updateVehicleJobStatusData['VehicleJobMaster']['remark'] = (isset($this->request->data['remark'])) ? $this->request->data['remark'] : '';
                            $updateVehicleJobStatusData['VehicleJobMaster']['id'] = $this->request->data['id'];
                            if(isset($this->request->data['employee_master_id']) && !empty($this->request->data['employee_master_id'])) {
                                $updateVehicleJobStatusData['VehicleJobMaster']['employee_name'] = $this->request->data['employee_name'];
                            }
                            if(isset($this->request->data['completion_date']) && !empty($this->request->data['completion_date'])) {
                                $updateVehicleJobStatusData['VehicleJobMaster']['completion_date'] = $this->AppUtilities->date_format($this->request->data['completion_date'],'yyyy-mm-dd');
                            }
                            if(isset($this->request->data['completion_time']) && !empty($this->request->data['completion_time'])) {
                                $updateVehicleJobStatusData['VehicleJobMaster']['completion_time'] = date('H:i:s',strtotime($this->request->data['completion_time']));
                            }
                            #pr($arrVehicleEmployeeData);
                            #pr($updateVehicleJobStatusData);exit;
                            if(!$this->VehicleJobMaster->save($updateVehicleJobStatusData,array('validate'=>false))) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            
                            if(count($arrVehicleEmployeeData) > 0) {
                                if(!$this->VehicleJobEmployeeTran->save($arrVehicleEmployeeData)) {
                                    throw new Exception(__('Vehicle job employee could not saved properly, please try again later!',true));
                                }
                            }

                            if(isset($this->request->data['arrChecklistData']) && !empty($this->request->data['arrChecklistData'])) {
                                $arrSaveChecklistData = array();
                                $count = 0;
                                foreach($this->request->data['arrChecklistData'] as $key => $checklistData) {
                                    $arrSaveChecklistData[$count]['VehicleJobChecklistTran']['id'] = $checklistData['id'];
                                    $arrSaveChecklistData[$count]['VehicleJobChecklistTran']['review'] = $checklistData['value'];
                                    ++$count;
                                }
                                if(!$this->VehicleJobChecklistTran->saveAll($arrSaveChecklistData)) {
                                    throw new Exception(__('Vehicle job checklist record could not saved properly, please try again later!',true));
                                }
                            }
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('Job status has been changed successfully!.',true));
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                        $dataSource->commit();
                        unset($this->request->data);
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete_product_stock() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['id']) && isset($this->request->data['vehicle_job_tran_id']) && isset($this->request->data['vehicle_job_json']) && !empty($this->request->data['id'])) {
                    $this->request->data['id'] = $this->decryption($this->request->data['id']);
                    $dataSource = $this->VehicleJobTran->getDataSource();
                    $this->request->data['type'] = (isset($this->request->data['type'])) ? $this->request->data['type'] : 1;
                    try {
                        $dataSource->begin();
                        $options = array(
                                        'fields' => array('vehicle_job_master_id','reference_tran_id','VJM.job_json'),
                                        'joins' => array(
                                            array(
                                                'table' => 'vehicle_job_masters',
                                                'alias' => 'VJM',
                                                'type' => 'inner',
                                                'conditions' => array('VJM.id = VehicleJobTran.vehicle_job_master_id','VJM.status' => 1)
                                            )
                                        ),
                                        'conditions' => array('VehicleJobTran.vehicle_job_master_id' => $this->request->data['id'],'VehicleJobTran.id' => $this->request->data['vehicle_job_tran_id'],'VehicleJobTran.status' => 1)
                                    );
                        $vehicleJobDetail = $this->VehicleJobTran->find('first',$options);
                        #pr($vehicleJobDetail);exit;
                        if(count($vehicleJobDetail) > 0) {
                            $updateFields['VehicleJobTran.status'] = 0;
                            $updateFields['VehicleJobTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleJobTran.id'] = $this->request->data['vehicle_job_tran_id'];
                            if(!$this->VehicleJobTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('vehicle Job Tran record could not saved properly, please try again later!',true));
                            }
                            unset($updateParams,$updateFields);

                            if($this->request->data['type'] == 1 || $this->request->data['type'] == 3) {
                                $updateFields['ProductVariantStockTran.status'] = 0;
                                $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['ProductVariantStockTran.reference_master_id'] = $vehicleJobDetail['VehicleJobTran']['vehicle_job_master_id'];
                                $updateParams['ProductVariantStockTran.from_type'] = 3;
                                $updateParams['ProductVariantStockTran.product_variant_tran_id'] = $vehicleJobDetail['VehicleJobTran']['reference_tran_id'];
                                if(!$this->ProductVariantStockTran->updateAll($updateFields,$updateParams)) {
                                    throw new Exception(__('Product Stock Record could not saved properly, please try again later!',true));
                                }
                                unset($updateParams,$updateFields);
                            }
                            
                            $arrSaveVehicleJobData['VehicleJobMaster']['id'] = $vehicleJobDetail['VehicleJobTran']['vehicle_job_master_id'];
                            if($this->request->data['type'] == 1 || $this->request->data['type'] == 2) {
                                $arrSaveVehicleJobData['VehicleJobMaster']['coupon'] = $this->request->data['coupon'];
                                $arrSaveVehicleJobData['VehicleJobMaster']['amount'] = $this->request->data['amount'];
                                $arrSaveVehicleJobData['VehicleJobMaster']['discount'] = $this->request->data['discount'];
                                if(isset($this->request->data['vehicle_job_json']) && !empty($this->request->data['vehicle_job_json'])) {
                                    $vehicleJobData = json_decode($this->request->data['vehicle_job_json'],true);
                                    if(!empty($vehicleJobDetail['VJM']['job_json'])) {
                                        $vehicleOldData = json_decode($vehicleJobDetail['VJM']['job_json'],true);
                                        if(isset($vehicleOldData['spare_product']) && !empty($vehicleOldData['spare_product'])) {
                                            $vehicleJobData['spare_product'] = $vehicleOldData['spare_product'];
                                        }
                                    }
                                    $arrSaveVehicleJobData['VehicleJobMaster']['job_json'] = json_encode($vehicleJobData);
                                } else {
                                    $arrSaveVehicleJobData['VehicleJobMaster']['job_json'] = json_encode(array());
                                }
                            } else if($this->request->data['type'] == 2) {

                            } else {}
                            
                            if(!$this->VehicleJobMaster->save($arrSaveVehicleJobData,array('validate' => false))) {
                                throw new Exception(__('Vehicle Job Record could not saved properly, please try again later!',true));
                            }
                            $dataSource->commit();
                            $statusCode = 200;
                            if(isset($this->request->data['type']) && $this->request->data['type'] == 1) {
                                $response = array('status' => 1,'message' => __('PRODUCT_DELETED',true));
                            } else {
                                $response = array('status' => 1,'message' => __('SERVICE_DELETED',true));
                            }
                        } else {
                            $response = array('status' => 0,'message' => __('NO_RECORD',true));
                        }
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' =>$id,'vehicle_job_master_id' => $vehicleJobId),'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_PARAMS',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => array('id' =>$id,'vehicle_job_master_id' => $vehicleJobId),'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function view_job_list($firmId = 0,$comppanyYearId = 0) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(!empty($firmId) && !empty($comppanyYearId)) {
                    $conditions = array('VehicleJob.branch_master_id' => $firmId,'VehicleJob.company_year_master_id' => $comppanyYearId,'VehicleJob.status' => 1);
                    if(isset($this->request->data['from_date']) && !empty($this->request->data['from_date'])) {
                        $conditions['VehicleJob.job_date >= '] = date('Y-m-d',strtotime($this->request->data['from_date']));
                    }
                    if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
                        $conditions['VehicleJob.job_date <= '] = date('Y-m-d',strtotime($this->request->data['end_date']));
                    }
                    if(isset($this->request->data['job_code']) && !empty($this->request->data['job_code'])) {
                        $conditions['VehicleJob.unique_no'] = trim($this->request->data['job_code']);
                    }
                    if(isset($this->request->data['vehicle_code']) && !empty($this->request->data['vehicle_code'])) {
                        $conditions['VehicleJob.vehicle_code'] = trim($this->request->data['vehicle_code']);
                    }
                    if(isset($this->request->data['vehicle_no']) && !empty($this->request->data['vehicle_no'])) {
                        $conditions['VehicleJob.vehicle_number'] = trim($this->request->data['vehicle_no']);
                    }
                    if(isset($this->request->data['mobile_no']) && !empty($this->request->data['mobile_no'])) {
                        $conditions['VehicleJob.customer_mobile_no'] = trim($this->request->data['mobile_no']);
                    }
                    if(isset($this->request->data['engine_no']) && !empty($this->request->data['engine_no'])) {
                        $conditions['VehicleJob.engine_number'] = trim($this->request->data['engine_no']);
                    }
                    if(isset($this->request->data['chasis_no']) && !empty($this->request->data['chasis_no'])) {
                        $conditions['VehicleJob.chasis_number'] = trim($this->request->data['chasis_no']);
                    }
                    if(isset($this->request->data['payment_type']) && !empty($this->request->data['payment_type'])) {
                        $conditions['VehicleJob.payment_status'] = trim($this->request->data['payment_type']);
                    }
                    if(isset($this->request->data['job_status']) && !empty($this->request->data['job_status'])) {
                        $conditions['VehicleJob.job_status'] = trim($this->request->data['job_status']);
                    }
                    $tableOptions = array(
                                        'fields' => array('id','unique_no','vehicle_number','job_date','job_status','payment_status','total','is_generate_invoice'),
                                        'conditions' => $conditions,
                                        'order' => 'id',
                                        'recursive' => -1
                                    );
                    $arrTableData = $this->VehicleJob->find('all',$tableOptions);
                    if(count($arrTableData) > 0) {
                        $arrTableData = Hash::combine($arrTableData,'{n}.VehicleJob.id','{n}.VehicleJob');
                        $response = array('status' => 1,'date' => date('Y-m-d'),'message' => __('RECORD_FETCHED',true),'data' => array_values($arrTableData));
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_REQUEST_METHOD',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function delete($firmId = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                if(isset($this->request->data['id']) && isset($firmId) && count($this->request->data['id']) > 0) {
                    $this->request->data['id'] = $this->bulk_decryption($this->request->data['id']);
                    $options = array('fields' => array('id','vehicle_coupon_tran_id'),'conditions' => array('status' => 1,'id' => $this->request->data['id']));
                    $arrVehicleJobDetail = $this->VehicleJobMaster->find('list',$options);
                    if(count($arrVehicleJobDetail) > 0) {
                        $dataSource = $this->VehicleJobMaster->getDataSource();
                        try {
                            $dataSource->begin();

                            $updateFields['VehicleJobMaster.status'] = -1;
                            $updateFields['VehicleJobMaster.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleJobMaster.id'] = $this->request->data['id'];
                            if(!$this->VehicleJobMaster->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }

                            unset($updateFields,$updateParams);
                            $updateFields['VehicleJobEmployeeTran.status'] = 0;
                            $updateFields['VehicleJobEmployeeTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleJobEmployeeTran.vehicle_job_master_id'] = $this->request->data['id'];
                            if(!$this->VehicleJobEmployeeTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }

                            unset($updateFields,$updateParams);
                            $updateFields['VehicleJobTran.status'] = 0;
                            $updateFields['VehicleJobTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleJobTran.vehicle_job_master_id'] = $this->request->data['id'];
                            if(!$this->VehicleJobTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }

                            unset($updateFields,$updateParams);
                            $updateFields['VehicleJobChecklistTran.status'] = 0;
                            $updateFields['VehicleJobChecklistTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['VehicleJobChecklistTran.vehicle_job_master_id'] = $this->request->data['id'];
                            if(!$this->VehicleJobChecklistTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }
                            
                            unset($updateFields,$updateParams);
                            $updateFields['ProductVariantStockTran.status'] = 0;
                            $updateFields['ProductVariantStockTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                            $updateParams['ProductVariantStockTran.reference_master_id'] = $this->request->data['id'];
                            $updateParams['ProductVariantStockTran.from_type'] = 3;
                            if(!$this->ProductVariantStockTran->updateAll($updateFields,$updateParams)) {
                                throw new Exception(__('Record could not saved properly, please try again later!',true));
                            }

                            $arrCouponId = array();
                            foreach($arrVehicleJobDetail as $key => $couponTranId) {
                                if($couponTranId > 0) {
                                    $arrCouponId[] = $couponTranId;
                                }
                            }

                            if(count($arrCouponId) > 0) {
                                unset($updateFields,$updateParams);
                                $updateFields['VehicleCouponTran.type'] = 0;
                                $updateFields['VehicleCouponTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'), 'string');
                                $updateParams['VehicleCouponTran.id'] = $arrCouponId;
                                if(!$this->VehicleCouponTran->updateAll($updateFields,$updateParams)) {
                                    throw new Exception(__('Record could not saved properly, please try again later!',true));
                                }
                            }
                            $dataSource->commit();
                            unset($this->request->data,$updateParams,$updateFields);
                            $statusCode = 200;
                            $response = array('status' => 1,'message' => __('DELETED_RECORD',true));
                        } catch(Exception $e) {
                            $dataSource->rollback();
                            $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
                            $this->ErrorLog->saveErrorLog($arrErrorLogs);
                            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                        }
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
