<?php
App::uses('AppController','Controller');
class RoleLinkOperationRightsTransController extends AppController {
    public $name='RoleLinkOperationRightsTrans';
    public $layout = false;
    public $uses = array('RoleLinkOperationRightsTran','RoleLinkMaster','RoleMaster','ErrorLog');
    public $helpers = array('Html','Form');
    public $components = array('AppUtilities','UserManagement');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function get_role_list($userType = null) {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('get')) {
                $conditions = array('status' => 1,'is_show' => 1);
                if(!empty($userType) && $userType == 1) {
                    $conditions = array('status' => 1);
                }
                $options = array(
                                'fields'=>array('id','name'),
                                'conditions' => $conditions,
                                'order'=>array('RoleMaster.order_no')
                            );
                $arrRoleData = $this->RoleMaster->find('all',$options);
                $arrRoleData = Hash::combine($arrRoleData,'{n}.RoleMaster.id','{n}.RoleMaster');
                $arrRoleData = array_values($arrRoleData);
                $options = array(
                            'fields'=>array('id','name'),
                            'conditions' => array('status' => 1,'is_show' => 1,'OR' => array('RoleLinkMaster.parent_id IS NULL','RoleLinkMaster.parent_id' => '')),
                            'group'=>'RoleLinkMaster.id',
                            'order'=>array('RoleLinkMaster.parent_id','RoleLinkMaster.order_no')
                        );
                $arrRoleLinkData = $this->RoleLinkMaster->find('all',$options);
                $arrRoleLinkData = Hash::combine($arrRoleLinkData,'{n}.RoleLinkMaster.id','{n}.RoleLinkMaster');
                $arrRoleLinkData = array_merge(array(array('id' => '','name' => 'Select All Module')),array_values($arrRoleLinkData));
                $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'role' => $arrRoleData,'module' => $arrRoleLinkData);
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function search_modules() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #$this->RoleLinkMaster->recover();
                $conditions = array('RoleLinkMaster.status' => 1,'RoleLinkMaster.is_show' => 1);
                $roleRightsConditions = array('status' => 1,'role_master_id' => $this->request->data['role_master_id']);
                if(isset($this->request->data['module_id']) && !empty($this->request->data['module_id'])) {
                    $arrChildren = $this->RoleLinkMaster->children($this->request->data['module_id']);
                    $results = Hash::extract($arrChildren, '{n}.RoleLinkMaster.id');
                    $results[] = $this->request->data['module_id'];
                    $conditions['RoleLinkMaster.id'] = $results;
                    $roleRightsConditions['role_link_master_id'] = $results;
                }
                $options = array(
                                'fields'=>array('id','name','parent_id'),
                                'conditions'=>$conditions,
                                'group'=>'RoleLinkMaster.id',
                                'order'=>array('RoleLinkMaster.parent_id','RoleLinkMaster.order_no')
                            );
                $arrRoleLinkData = $this->RoleLinkMaster->find('threaded',$options);
                $options = array(
                                'fields'=>array('role_link_master_id','type_tran_id'),
                                'conditions'=> $roleRightsConditions,
                                'recursive'=>-1
                            );
                $arrRoleLinkRightsData = $this->RoleLinkOperationRightsTran->find('all',$options);
                $arrRoleLinkRightsData = Hash::combine($arrRoleLinkRightsData,'{n}.RoleLinkOperationRightsTran.type_tran_id','{n}.RoleLinkOperationRightsTran.type_tran_id','{n}.RoleLinkOperationRightsTran.role_link_master_id');
                $arrUserRoleOperationData = $this->AppUtilities->getUserOperation();
                $statusCode = 200;
                $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'operation' => $arrUserRoleOperationData,'modules' => $arrRoleLinkData,'rights' =>$arrRoleLinkRightsData);
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['role_master_id']) && isset($this->request->data['rights']) && count($this->request->data['rights']) > 0 && !empty($this->request->data['role_master_id'])) {
                    $roleMasterId = $this->request->data['role_master_id'];
                    $rights = $this->request->data['rights'];
                    $arrRoleLinkOperationData =array();
                    $counter = 0;
                    foreach($rights as $roleLinkMasterId => $arrRights) {
                        foreach($arrRights as $key =>$roleTypeTranId) {
                            $arrRoleLinkOperationData[$counter]['RoleLinkOperationRightsTran']['role_master_id'] = $roleMasterId;
                            $arrRoleLinkOperationData[$counter]['RoleLinkOperationRightsTran']['role_link_master_id'] = $roleLinkMasterId;
                            $arrRoleLinkOperationData[$counter]['RoleLinkOperationRightsTran']['type_tran_id'] = $roleTypeTranId;
                            ++$counter;
                        }
                    }
                    #pr($arrRoleLinkOperationData);exit;
                    $dataSource = $this->RoleLinkOperationRightsTran->getDataSource();
                    try{
                        $dataSource->begin();
                        $deleteParams['RoleLinkOperationRightsTran.role_master_id'] = $roleMasterId;
                        if(isset($this->request->data['module_id']) && !empty($this->request->data['module_id'])) {
                            $roleLinkData = $this->RoleLinkMaster->children($this->request->data['module_id']);
                            $results = Hash::extract($roleLinkData, '{n}.RoleLinkMaster.id');
                            $results[] = $this->request->data['module_id'];
                            $deleteParams['RoleLinkOperationRightsTran.role_link_master_id'] = $results;
                        }
                        if(!$this->RoleLinkOperationRightsTran->deleteAll($deleteParams)) {
                            throw new Exception(__('Record could not deleted properly, please try again later!'));
                        }

                        if(isset($arrRoleLinkOperationData) && count($arrRoleLinkOperationData)>0){
                            if(!$this->RoleLinkOperationRightsTran->saveAll($arrRoleLinkOperationData)){
                                throw new Exception(__('Record could not saved properly, please try again later!'));
                            }
                        }
                        $this->UserManagement->buildRoleRightsMenu($roleMasterId);
                        unset($deleteParams);
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('Rights is saved successfully !..',true));
                        $dataSource->commit();
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' =>$this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>