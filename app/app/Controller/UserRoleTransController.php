<?php
App::uses('AppController','Controller');
class UserRoleTransController extends AppController {
    public $name='UserRoleTrans';
    public $layout = false;
    public $uses = array('UserRoleTran','ErrorLog','BranchMaster','Employee');
    public $components = array('AppUtilities');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function search() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                $arrUserRoleData['UserRoleTran'] = $this->request->data;
                #pr($arrUserRoleData);
                $this->UserRoleTran->set($arrUserRoleData);
                if($this->UserRoleTran->validates()) {
                    unset($this->request->data);
                    $roleType = (int) $arrUserRoleData['UserRoleTran']['role_type'];
                    $arrRecords = array();
                    if($roleType === 3) {
                        $options = array(
                                    'fields' => array('id','first_name','middle_name','last_name','unique_no','User.id','UserRoleTran.role_master_id','RoleMaster.name'),
                                    'joins' => array(
                                        array(
                                            'table' => 'users',
                                            'alias' => 'User',
                                            'type' => 'INNER',
                                            'conditions' => array('BranchMaster.user_id = User.id','User.status' => 1)
                                        ),
                                        array(
                                            'table' => 'user_role_trans',
                                            'alias' => 'UserRoleTran',
                                            'type' => 'LEFT',
                                            'conditions' => array('BranchMaster.user_id = UserRoleTran.user_id','UserRoleTran.status' => 1)
                                        ),
                                        array(
                                            'table' => 'role_masters',
                                            'alias' => 'RoleMaster',
                                            'type' => 'LEFT',
                                            'conditions' => array('UserRoleTran.role_master_id = RoleMaster.id','RoleMaster.status' => 1)
                                        )
                                    ),
                                    'conditions' => array('BranchMaster.status' => 1,'BranchMaster.member_type_tran_id' => $arrUserRoleData['UserRoleTran']['user_member_type_id']),
                                    'group' => array('BranchMaster.id')
                                );
                        $arrUserMemberData = $this->BranchMaster->find('all',$options);
                        if(!empty($arrUserMemberData)) {
                            foreach($arrUserMemberData as $key => $userDetail) {
                                $name = $userDetail['BranchMaster']['first_name'].' '.$userDetail['BranchMaster']['middle_name'].' '.$userDetail['BranchMaster']['last_name'];
                                $arrRecords[] = array('user_id' => $userDetail['User']['id'],'name' => $name,'role' => $userDetail['RoleMaster']['name'],'unique_no' => $userDetail['BranchMaster']['unique_no'],'id' => $userDetail['BranchMaster']['id'],'role_id' => $userDetail['UserRoleTran']['role_master_id']);
                            }
                        }
                    } else if($roleType === 4) {
                        $options = array(
                            'fields' => array('id','first_name','middle_name','last_name','department','designation','unique_no','User.id','UserRoleTran.role_master_id','RoleMaster.name'),
                            'joins' => array(
                                array(
                                    'table' => 'users',
                                    'alias' => 'User',
                                    'type' => 'INNER',
                                    'conditions' => array('Employee.user_id = User.id','User.status' => 1)
                                ),
                                array(
                                    'table' => 'user_role_trans',
                                    'alias' => 'UserRoleTran',
                                    'type' => 'LEFT',
                                    'conditions' => array('Employee.user_id = UserRoleTran.user_id','UserRoleTran.status' => 1)
                                ),
                                array(
                                    'table' => 'role_masters',
                                    'alias' => 'RoleMaster',
                                    'type' => 'LEFT',
                                    'conditions' => array('UserRoleTran.role_master_id = RoleMaster.id','RoleMaster.status' => 1)
                                )
                            ),
                            'conditions' => array('Employee.status' => 1,'Employee.branch_master_id' => $arrUserRoleData['UserRoleTran']['user_type_id'])
                        );
                        $arrrEmployeeData = $this->Employee->find('all',$options);
                        if(!empty($arrrEmployeeData)) {
                            foreach($arrrEmployeeData as $key => $userDetail) {
                                $name = $userDetail['Employee']['first_name'].' '.$userDetail['Employee']['middle_name'].' '.$userDetail['Employee']['last_name'];
                                $arrRecords[] = array('user_id' => $userDetail['User']['id'],'role' => $userDetail['RoleMaster']['name'],'name' => $name,'unique_no' => $userDetail['Employee']['unique_no'],'department' => $userDetail['Employee']['department'],'designation' => $userDetail['Employee']['designation'],'id' => $userDetail['Employee']['id'],'role_id' => $userDetail['UserRoleTran']['role_master_id']);
                            }
                        }
                    } else {

                    }
                    if(count($arrRecords) > 0) {
                        $response = array('status' => 1,'message' => __('RECORD_FETCHED',true),'data' => $arrRecords);
                    } else {
                        $response = array('status' => 0,'message' => __('NO_RECORD',true));
                    }
                } else {
                    $validationErrros = Set::flatten($this->UserRoleTran->validationErrors);
                    $message = $this->AppUtilities->displayMessage($validationErrros);
                    $response = array('status' => 0,'message' => __($message,true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }

    public function save() {
        $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
        $statusCode = 400;
        try {
            if($this->request->is('post')) {
                #pr($this->request->data);exit;
                if(isset($this->request->data['role_master_id']) && !empty($this->request->data['role_master_id'])) {
                    $dataSource = $this->UserRoleTran->getDataSource();
                    try{
                        $arrDeleteRoleUserId = $arrUserRoleId =  array();
                        $arrUserIds = array();
                        if(isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
                            $arrUserIds = $this->request->data['user_id'];
                        }
                        
                        if(isset($this->request->data['role_user_id'][$this->request->data['role_master_id']])) {
                            $arrRoleUserId = $this->request->data['role_user_id'][$this->request->data['role_master_id']];
                            $arrAssignUserId = $arrUserIds;
                            $arrDeleteRoleUserId = array_diff($arrRoleUserId,$arrAssignUserId);
                            $arrUserRoleId = array_diff($arrAssignUserId,$arrRoleUserId);
                            #pr($arrUserRoleId);
                            #pr($arrDeleteRoleUserId);exit;
                        } else {
                            $arrUserRoleId = $arrUserIds;
                        }
                        if(!empty($arrDeleteRoleUserId)) {
                            $options = array('fields' => array('id'),'conditions' => array('role_master_id' => $this->request->data['role_master_id'],'user_id' => $arrDeleteRoleUserId));
                            $arrUserId = $this->UserRoleTran->find('list',$options);
                            if(!empty($arrUserId)) {
                                $updateFields['UserRoleTran.status'] = 0;
                                $updateFields['UserRoleTran.modified'] = $dataSource->value(date('Y-m-d H:i:s'),'string');
                                $updateParams['UserRoleTran.id'] = $arrUserId;
                                $this->UserRoleTran->updateAll($updateFields,$updateParams);
                            }
                        }
                        #pr($arrUserRoleId);exit;
                        if(!empty($arrUserRoleId)) {
                            $arrSaveUseRoleTran = array();
                            $count = 0;
                            foreach($arrUserRoleId as $key =>$userId) {
                                $arrSaveUseRoleTran[$count]['UserRoleTran'] = array('user_id' => $userId,'role_master_id' => $this->request->data['role_master_id']);
                                ++$count;
                            }
                            if(!$this->UserRoleTran->saveAll($arrSaveUseRoleTran,array('validate'=>false))) {
                                throw new Exception();
                            }
                        }
                        $dataSource->commit();
                        $statusCode = 200;
                        $response = array('status' => 1,'message' => __('SAVED_RECORD',true));
                    } catch(Exception $e) {
                        $dataSource->rollback();
                        $arrErrorLogs = array('user_id' => $this->Session->read('sessUserId'),'controller' => 'user_role_trans','method' => 'save','request' => $this->request->data,'description' => $e);
                        $this->ErrorLog->saveErrorLog($arrErrorLogs);
                        $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
                    }
                } else {
                    $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
                }
            } else {
                $response = array('status' => 0,'message' => __('INVALID_RESPONSE',true));
            }
        } catch(Exception $e) {
            $arrErrorLogs = array('user_id' =>$this->Session->read('sessUserId'),'controller' => $this->request->params['controller'],'method' => $this->request->params['action'],'request' => $this->request->data,'description' => $e);
            $this->ErrorLog->saveErrorLog($arrErrorLogs);
            $response = array('status' => 0,'message' => __('INTERNAL_SERVER_ERROR',true));
        }
        $this->bodyResponse($response,$statusCode);
    }
}
?>
