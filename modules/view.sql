DROP VIEW IF EXISTS employees;
CREATE VIEW employees AS
SELECT employee_masters.*,designation_masters.`name` AS `designation`,
department_masters.`name` AS `department`,
country_masters.`name` AS `country`,
state_masters.`name` AS `state`,
city_masters.`name` AS `city`,
employee_trans.`designation_master_id`,
department_masters.id AS department_master_id,
CONCAT_WS(' ',title_masters.`name`,employee_masters.first_name,employee_masters.middle_name,employee_masters.last_name) AS employee_name
FROM employee_masters
INNER JOIN employee_trans ON(employee_masters.`id` = employee_trans.`employee_master_id`)
INNER JOIN designation_masters ON(employee_trans.`designation_master_id` = designation_masters.`id` AND designation_masters.`status` = 1)
INNER JOIN department_masters ON(department_masters.`id` = designation_masters.`department_master_id` AND department_masters.`status` = 1)
INNER JOIN user_member_masters ON(employee_masters.`user_member_master_id` = user_member_masters.`id` AND user_member_masters.`status` = 1)
INNER JOIN country_masters ON(employee_masters.`country_master_id` = country_masters.`id` AND country_masters.`status` = 1)
INNER JOIN state_masters ON(employee_masters.`state_master_id` = state_masters.`id` AND state_masters.`status` = 1)
INNER JOIN city_masters ON(employee_masters.`city_master_id` = city_masters.`id` AND city_masters.`status` = 1)
LEFT JOIN title_masters ON(employee_masters.`title_master_id`=title_masters.`id` AND title_masters.`status` = 1)
WHERE employee_masters.`status` = 1;

DROP VIEW IF EXISTS vehicle_models;
CREATE VIEW vehicle_models AS 
SELECT vehicle_model_masters.*,
manufacturer_masters.`name` AS manufacturer,
vehicle_type_masters.`name` AS vehicle_type,
vehicle_type_masters.`id` AS vehicle_type_id,
manufacturer_masters.`id` AS manufacturer_id
FROM vehicle_model_masters
INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id`=manufacturer_trans.`id`)
INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id`)
INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id`)
WHERE vehicle_model_masters.`status` = 1 AND manufacturer_trans.`status` = 1
AND manufacturer_masters.`status` = 1 AND vehicle_type_masters.`status` = 1;


/** user role rights view **/
DROP VIEW IF EXISTS user_role_rights;
CREATE VIEW user_role_rights AS 
SELECT role_link_masters.`id`,role_link_masters.`name`,
role_link_masters.`module_name`,
type_trans.`name` AS `type_name`,
user_role_trans.`user_id`,
role_link_masters.`parent_id`,role_link_masters.`order_no`
FROM role_link_masters 
INNER JOIN role_link_operation_rights_trans ON(role_link_masters.`id` = role_link_operation_rights_trans.`role_link_master_id`)
INNER JOIN user_role_trans ON(role_link_operation_rights_trans.`role_master_id` = user_role_trans.`role_master_id`)
INNER JOIN type_trans ON(role_link_operation_rights_trans.`type_tran_id` = type_trans.`id`)
WHERE role_link_masters.`status` = 1 AND role_link_masters.`is_show` = 1 
AND role_link_operation_rights_trans.`status` = 1 AND user_role_trans.`status` = 1 
AND type_trans.`status` = 1;


/** create vehicle view **/
DROP VIEW IF EXISTS vehicles;
CREATE VIEW vehicles AS
SELECT vehicle_masters.*,
vehicle_model_masters.`name` AS vehicle_model_name,
manufacturer_masters.`name` AS manufacturer_name,
manufacturer_masters.`id` AS manufacturer_id,
manufacturer_trans.`id` AS manufacturer_tran_id,
vehicle_type_masters.`name` AS vehicle_type_name,
vehicle_type_masters.`id` AS vehicle_type_id,
variant_masters.`name` AS variant_name
FROM vehicle_masters
INNER JOIN vehicle_model_masters ON(vehicle_masters.`vehicle_model_master_id` = vehicle_model_masters.`id` AND vehicle_model_masters.`status` = 1)
INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id` = manufacturer_trans.`id` AND manufacturer_trans.`status` = 1)
INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id` AND manufacturer_masters.`status` = 1)
INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id` AND vehicle_type_masters.`status` = 1)
INNER JOIN variant_masters ON(vehicle_masters.`variant_master_id` = variant_masters.`id` AND variant_masters.`status` = 1)
WHERE vehicle_masters.`status` = 1 AND vehicle_masters.`vehicle_type` = 1;


DROP VIEW IF EXISTS vehicle_jobs;
CREATE VIEW vehicle_jobs AS
SELECT vehicle_job_masters.*,vehicle_masters.`unique_no` AS vehicle_code,vehicle_masters.`vehicle_number`,
vehicle_masters.`chasis_number`,vehicle_masters.`engine_number`,vehicle_model_masters.`name` AS `vehicle_model`,
manufacturer_masters.`name` AS `manufacturer`,vehicle_type_masters.`name` AS `vehicle_type`
FROM vehicle_job_masters
INNER JOIN vehicle_masters ON(vehicle_job_masters.`vehicle_master_id` = vehicle_masters.`id` AND vehicle_masters.`status` = 1)
INNER JOIN vehicle_model_masters ON(vehicle_masters.`vehicle_model_master_id` = vehicle_model_masters.`id` AND vehicle_model_masters.`status` = 1)
INNER JOIN manufacturer_trans ON(vehicle_model_masters.`manufacturer_tran_id` = manufacturer_trans.`id` AND manufacturer_trans.`status` = 1)
INNER JOIN manufacturer_masters ON(manufacturer_trans.`manufacturer_master_id` = manufacturer_masters.`id` AND manufacturer_masters.`status` = 1)
INNER JOIN vehicle_type_masters ON(manufacturer_trans.`vehicle_type_master_id` = vehicle_type_masters.`id` AND vehicle_type_masters.`status` = 1)
WHERE vehicle_job_masters.`status` = 1;






