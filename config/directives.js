/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
radizoApp.directive('ngSpinnerBar', ['$rootScope','$state',
    function($rootScope,$state) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function() {
                        Metronic.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);
                }, 10);

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]);

// Handle global LINK click
radizoApp.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});

// Handle Dropdown Hover Plugin Integration
radizoApp.directive('dropdownMenuHover', function() {
    return {
        link: function(scope, elem) {
            elem.dropdownHover();
        }
    };
});

radizoApp.directive('notificationAlert', function() {
    return {
        replace: true,
        scope: { notificationAlert: '@' },
        link: function(scope, element, attrs) {
            var notification = '';
            console.log('scope type')
            if (scope.type === 1) {

            } else if (scope.type === 2) {
                notification = '<div class="alert alert-success display-hide">\
                                    <button class="close" data-close="alert"></button>\
                                    <span ng-bind-html="displayMessage"></span>\
                                </div>';
            } else {

            }
            scope.$watch('notificationAlert', function() {
                element.html(notification);
            });
        }
    }
});

radizoApp.directive('formTool', function($compile) {
    return {
        restrict: 'E',
        replace:true,
        link: function($scope, element, attributes) {
            var formHtml = '';
            console.log('$scope :',$scope.caption);
            if($scope.userRights.add) {
                formHtml += '<button class="btn btn-sm default green tooltips" data-container="body" data-placement="top" data-original-title="Add Record" ng-click="redirectState(\'add\',\'\');"><i class="fa fa-plus"></i><span class="hidden-480">Add New</span></button>';
            }
            if($scope.userRights.delete) {
                if($scope.caption == 'Sale' || $scope.caption == 'Perfoma Invoice') {
                    formHtml += '<button class="btn btn-sm default red tooltips" data-container="body" data-placement="top" data-original-title="Cancel Invoice" ng-click="delete();"><i class="fa fa-trash-o"></i><span class="hidden-480">Cancel Invoice</span></button>';
                } else {
                    formHtml += '<button class="btn btn-sm default red tooltips" data-container="body" data-placement="top" data-original-title="Delete Record" ng-click="delete();"><i class="fa fa-trash-o"></i><span class="hidden-480">Delete</span></button>';
                }
            }

            if($scope.userRights.cancel_invoice) {
                formHtml += '<button class="btn btn-sm default red tooltips" data-container="body" data-placement="top" data-original-title="Cancel Invoice" ng-click="cancelInvoice();"><i class="fa fa-trash-o"></i><span class="hidden-480">Cancel Invoice</span></button>';
            }

            if($scope.arrFormTools) {
                var tools;
                for(tools of $scope.arrFormTools) {
                    formHtml += '<button class="btn btn-sm default '+tools.class+' tooltips" data-container="body" data-placement="top" data-original-title="'+tools.title+'" ng-click="'+tools.method+'"><i class="'+tools.icon+'"></i><span class="hidden-480">'+tools.name+'</span></button>';
                }
            }

            formHtml += '<button class="btn btn-sm grey-cascade tooltips" data-container="body" data-placement="top" data-original-title="Search Record" ng-click="searchCollapsed = !searchCollapsed"><i class="fa fa-search"></i><span class="hidden-480">Search</span></button>';
            element.html($compile(formHtml)($scope));
        }
    }
});


radizoApp.directive('tools', function() {
    return {
        replace: true,
        restrict: 'E',
        template: '<ul class="btn-group">' +
                    '<li style="display: inline-block !important;padding-right:10px;" class="tooltips" data-container="body" data-placement="top" data-original-title="Refresh Record"><a href="javascript:void(0);" title="refresh_data" class="reset-filter" ng-click="reset()"><i title="refresh_data" style="color:#000;font-size:23px;" class="fa fa-refresh"></i></a></li>' +
                    '<li style="display: inline-block !important;padding-right:10px;" class="tooltips" data-container="body" data-placement="top" data-original-title="Export To Excel"><a href="#" title="Export to excel" ng-click="export(1);"><i title="generate_excel" style="color:#26a69a;font-size:23px;" class="fa fa-file-excel-o"></i></a></li>' +
                    '<li style="display: inline-block !important;padding-right:10px;" class="tooltips" data-container="body" data-placement="top" data-original-title="Export To PDF"><a href="#" title="Export to pdf" ng-click="export(2);"><i title="generate_pdf" style="color:#b83a3e;font-size:23px;" class="fa fa-file-pdf-o"></i></a></li>' +
                  '</ul>'
    }
});

radizoApp.directive('reportHeader',['$rootScope','$http','GLOBAL_DIR',function($rootScope,$http,GLOBAL_DIR){
    var getReportHeader = function() {
        var userId = localStorage.getItem('userId');
        var path = $rootScope.ADMIN_API_URL + '/system/report_header/'+userId;
        var templateLoader = $http.get(path);
        return templateLoader;
    }

    var linker = function(scope, element, attrs) {
        if (localStorage.getItem('userSessId') != null) {
            var report = getReportHeader();
            var promise = report.success(function(response) {
                if(response.status == 1) {
                    var data = response.data;
                    var template = '<table style="width:100%;">';
                            template +='<tr>';
                                template += '<td style = "width:50%;text-align:center;">';
                                if(data.logo != '') {
                                    var firmLogo = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR + '/' + GLOBAL_DIR.LOGODIR + '/' + data.logo;
                                    template += '<img src = "'+firmLogo+'" style="width:100px;height:80px;">';
                                }
                                template += '</td>';
                                template += '<td style = "width:50%;">';
                                    template += '<div style="font-family:calibri;font-weight:bold;font-size:20px;">'+data.firm_name+'</div>';
                                    template += '<div style="font-family:calibri;font-weight:bold;font-size:17px;">GST: '+data.gst_number+'</div>';
                                    template += '<div style="font-family:calibri;font-weight:bold;font-size:16px;"><i class = "fa fa-home" style = "font-size:17px;"></i>&nbsp;&nbsp;'+data.c_address+'</div>';
                                    template += '<div style="font-family:calibri;font-weight:bold;font-size:16px;"><i class = "fa fa-mobile" style = "font-size:18px;"></i>&nbsp;&nbsp;'+data.mobile_no+'</div>';
                                    template += '<div style="font-family:calibri;font-weight:bold;font-size:16px;"><i class = "fa fa-envelope" style = "font-size:16px;"></i>&nbsp;&nbsp;'+data.email_id+'</div>';
                                template += '</td>';
                            template +='</tr>';
                    template +='</table>';
                    element.html(template);
                }
            }).then(function(response) {
            });
        }
    }

    return {
        restrict: 'E',
        scope: {
            post: '='
        },
        link: linker
    };

}]);


radizoApp.directive('sideBar', ['$rootScope', '$http', '$state','$compile', 'GLOBAL_DIR', function($rootScope, $http, $state,$compile, GLOBAL_DIR) {
    var getSidebar = function() {
        var roleId = localStorage.getItem('roleId');
        var path = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/sidebar/' + roleId + '_sidebar.html';
        var templateLoader = $http.get(path);
        return templateLoader;
    }

    var linker = function(scope, element, attrs) {
        if (localStorage.getItem('userSessId') != null) {
            var loader = getSidebar();
            var promise = loader.success(function(html) {
                element.html(html);
            }).then(function(response) {
                $rootScope.$watch('$viewContentLoaded', function() {
                    Metronic.initAjax();
                    Layout.initSidebar(); // init sidebar
                    Layout.fixContentHeight(); // init sidebar
                    $compile(element.contents())(scope);
                    var sidebarClosed = 0;
                    console.log('layout localstorage value is :',localStorage.getItem('sidebarClosed'));
                    if(localStorage.getItem('sidebarClosed') !== null) {
                        sidebarClosed = Number(localStorage.getItem('sidebarClosed'));
                    } else {
                        localStorage.setItem('sidebarClosed',sidebarClosed);
                    }
                    var body = $('body');
                    var sidebar = $('.page-sidebar');
                    var sidebarMenu = $('.page-sidebar-menu');
                    $(".sidebar-search", sidebar).removeClass("open");
                    if(sidebarClosed) {
                        body.removeClass("page-sidebar-closed");
                        sidebarMenu.removeClass("page-sidebar-menu-closed");
                        if ($.cookie) {
                            $.cookie('sidebar_closed', '0');
                        }
                    } else {
                        body.addClass("page-sidebar-closed");
                        sidebarMenu.addClass("page-sidebar-menu-closed");
                        if (body.hasClass("page-sidebar-fixed")) {
                            sidebarMenu.trigger("mouseleave");
                        }
                        if ($.cookie) {
                            $.cookie('sidebar_closed', '1');
                        }
                    }
                    $(window).trigger('resize');
                });
            });
        }
    }
    return {
        restrict: 'E',
        scope: {
            post: '='
        },
        link: linker
    };
}]);

radizoApp.directive('compareTo', function() {
    return {
        require: 'ngModel',
        scope: {
            confirmPassword: "=compareTo"
        },
        link: function(scope, element, attributes, modelVal) {
            modelVal.$validators.compareTo = function(val) {
                return val == scope.confirmPassword;
            };
            scope.$watch('confirmPassword', function() {
                modelVal.$validate();
            });
        }
    };
});

/** input file type validation directory **/
radizoApp.directive('validFile', function() {
    return {
        require: 'ngModel',
        link: function(scope, el, attrs, ngModel) {
            el.bind('change', function() {
                scope.$apply(function() {
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});

radizoApp.directive('dateValidate', function ($q, $timeout) {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.dateValidate = function(modelValue,viewValue) {
                if(scope.bindScopeForm.start_date !== undefined && scope.bindScopeForm.end_date !== undefined) {
                    console.log('scope :',scope.bindScopeForm);
                    var arrStartDate = scope.bindScopeForm.start_date.split('-');
                    var arrEndDate = scope.bindScopeForm.end_date.split('-');
                    var startDate = Date.parse(arrStartDate[2] + '-' + arrStartDate[1] + '-' + arrStartDate[0]);
                    var endDate = Date.parse(arrEndDate[2] + '-' + arrEndDate[1] + '-' + arrEndDate[0]);
                    var millisecDay = 60 * 60 * 24 * 1000;
                    var noofdays = (endDate - startDate) / millisecDay;
                    noofdays = parseInt(noofdays);
                    console.log('noofdays :',noofdays);
                    if (noofdays < 0) {
                        return false;
                    }
                    else{
                       return true;
                    }
                } else {
                    return true;
                }
            };
            scope.$watch('start_date', function() {
                control.$validate();
            });
            
        }
    };
});

radizoApp.directive('autoFocus', function($timeout) {
    return {
        link: function (scope, element, attrs) {
            attrs.$observe("autoFocus", function(newValue){
                if (newValue === "true")
                    $timeout(function(){element.focus()});
            });
        }
    };
});
