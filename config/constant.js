/** GLOBAL PATH DEFINE **/
var GLOBAL_DIR = {
    FOLDERNAME: 'javascript_code/temp',
    COMPONENT: 'components',
    WEB_API_FOLDER: 'app',
    UPLOAD_ROOT_DIR: 'uploads',
    LOGODIR: 'logo',
    MEMBERDIR: 'member',
    EMPLOYEEDIR: 'employee',
    PRODUCTDIR:'product',
    VEHICLEDIR: 'vehicle',
    CONFIG_FOLDER: 'app_config',
    MODULES_FOLDER: 'modules',
    PROMPT_MSG:'Ridizo',
    DEFAULT_TABLEGRID_LENGTH: 10,
    PAGEDATA:[{id:10,name:10},{id:20,name:20},{id:50,name:50},{id:100,name:100},{id:150,name:150},{id:1000,name:"All"}],
    MESSAGE : {
        RIGHTS : 'You don\'t have rights to access this location',
        VALID_MEMBER : 'PLEASE SELECT VALID MEMBER ID',
        VALID_COMPANY : 'PLEASE SELECT VALID COMPANY YEAR',
        MANDATORY_FIELDS : 'Please filled up manadatory fields !.',
        INVALID_EXPORT : 'Invalid Export Type',
        INVALID_RESPONSE : 'Please check the response !.',
        VALID_PRODUCCT : 'Please select valid product !.',
        SELECT_ATLEAST_VALIDATION : 'Please select atleast one record',
        VALID_DATA_VALIDATION : 'Please select valid data !.'
    }
};
var error = {
    SERVER:'INTERNAL SERVER ERROR, PLEASE CONTACT TO SERVER ADMINISTRATOR !.'
};
radizoApp.constant('ERROR_MESSAGE', error);
radizoApp.constant('GLOBAL_DIR', GLOBAL_DIR);
radizoApp.factory('appContant', function(GLOBAL_DIR) {
    var appContant = {};
    appContant.API_PATH = {
        'login': GLOBAL_DIR.WEB_API_FOLDER + '/users/login',
        'dashboard': {
            'sidebar': GLOBAL_DIR.WEB_API_FOLDER + '/dashboard/sidebar'
        }
    };
    return appContant;
});