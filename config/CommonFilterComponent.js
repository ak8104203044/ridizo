'use strict';
radizoApp.controller('CommonFilterComponent', ['$rootScope', '$scope', '$state', 'AppService','AppUtilities','$modal','GLOBAL_DIR','$window', function($rootScope, $scope,$state, AppService,AppUtilities,$modal,GLOBAL_DIR,$window) {
    
    $scope.arrProductId = [];
    $scope.arrServiceId = [];
    $scope.arrProductData = [];
    $scope.arrServiceData = [];
    $scope.serviceDetail = {};
    $scope.arrTaxDetail = [];
    $scope.arrExpeseHeadId = [];
    $scope.arrCustomerData = [];
    
    $scope.searchVehicleDetail = function(searchValue) {
        $scope.bindScopeForm.vehicle_master_id = null;
        $scope.spinner = 1;
        if(typeof $scope.isVehicleExpense !== "undefined" && $scope.isVehicleExpense == 1) {
            if(typeof $scope.bindScopeForm.invoice_id !== "undefined" && $scope.bindScopeForm.invoice_id !== null) {
                var expenseIndex = $scope.expenseRow.findIndex(function(element){
                    return (element.invoice_id == $scope.bindScopeForm.invoice_id);
                });
                if(expenseIndex >= 0) {
                    $scope.expenseRow.splice(expenseIndex,1);
                }
                $scope.bindScopeForm.invoice_id = null;
            }
        }
        var params = {search:searchValue};
        var path = $rootScope.ADMIN_API_URL + '/system/search_vehicle';
        params.vehicle_type = (typeof $scope.vehicleType !== "undefined" && $scope.vehicleType !== null) ? $scope.vehicleType : 1;
        if(typeof $scope.memberFirmId !== "undefined" && $scope.memberFirmId !== null) {
            params.branch_master_id = $scope.memberFirmId;
        }
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrVehicleData = angular.copy(response.data);
            }).error(function() {
                $scope.spinner = 0;
            });
    };

    $scope.getValue = function(index,$event) {
        $scope.getVehicleDetail = {};
        $scope.bindScopeForm.vehicle_name = '';
        $scope.bindScopeForm.vehicle_master_id = null;
        if(typeof $scope.arrVehicleData[index] !== 'undefined' && $scope.arrVehicleData[index] !== null) {
            $scope.getVehicleDetail = $scope.arrVehicleData[index];
            $scope.bindScopeForm.vehicle_name = $scope.arrVehicleData[index]['unique_no'];
            $scope.bindScopeForm.vehicle_master_id = $scope.arrVehicleData[index]['id'];
            $scope.vehicleValidationType = (typeof $scope.vehicleValidationType !== "undefined") ? $scope.vehicleValidationType : 0;
            if($scope.vehicleValidationType == 1) {
                $scope.bindScopeForm.customer_name = $scope.arrVehicleData[index]['name'];
                $scope.bindScopeForm.customer_mobile_no = $scope.arrVehicleData[index]['mobile_no'];
                $scope.bindScopeForm.customer_email_id = $scope.arrVehicleData[index]['email_id'];
                $scope.bindScopeForm.customer_address = $scope.arrVehicleData[index]['address'];
                $scope.bindScopeForm.in_house_vehicle = $scope.arrVehicleData[index]['in_house_vehicle'];
            } else if($scope.vehicleValidationType == 2) {
                $scope.getVehicleServiceExpenseDetail();
            } else if($scope.vehicleValidationType == 3) {
                //$scope.getVehicleExpenseDetail();
                $scope.bindScopeForm.vehicle_number = $scope.arrVehicleData[index]['vehicle_number'];
                $scope.bindScopeForm.purchase_price = AppUtilities.numberFormat($scope.arrVehicleData[index]['purchase_price'],2,'.','');
                $scope.bindScopeForm.actual_amount = AppUtilities.numberFormat($scope.arrVehicleData[index]['sale_price'],2,'.','');
                $scope.bindScopeForm.amount = AppUtilities.numberFormat($scope.arrVehicleData[index]['sale_price'],2,'.','');
            } else {}
            
            if(typeof $scope.couponDetail !== "undefined" && $scope.couponDetail !== null) {
                $scope.couponDetail.customer = Object.assign({},$scope.arrVehicleData[index]);
            }
            $scope.arrVehicleData = [];
        }
        $event.stopPropagation();
    };

    $scope.searchboxClicked = function($event,type = 1) {
        if(type == 1) {
            if($scope.bindScopeForm.vehicle_master_id === null) {
                $scope.bindScopeForm.vehicle_name = '';
            }
            $scope.arrVehicleData = [];
        } else if(type == 2) {
            $scope.arrProductData = [];
            $scope.arrServiceData = [];
        } else if(type == 3) {
            $scope.arrProductData = [];
        } else {}
        $event.stopPropagation();
    };

    $scope.vehicleForm = function() {
        $scope.tmpVehicleForm = {};
        $scope.tmpVehicleForm.fuel_type = 1;
        $scope.tmpVehicleForm.vehicle_start_method = 1;
        $scope.getTitleList(0);
        $scope.getVehicleTypeList(0);
        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/general_form/vehicle_form.html';
        $modal.open({
            templateUrl: url,
            size: 'lg',
            backdrop:'static',
            keyboard:false,
            windowClass: 'modal',
            scope:$scope,
            controller:function($scope,$modalInstance) {
                $scope.saveVehicle = function(form) {
                    if(form.$valid && $scope.memberFirmId > 0 && $scope.companyYearId > 0) {
                        AppUtilities.blockUI(1,'#vehicleFormProcess');
                        $('#update_btn').attr('disabled', true);
                        $('#update_btn').html('Processing....');
                        var savePath = $rootScope.ADMIN_API_URL + '/' + 'vehicle_masters'+ '/save';
                        var saveForms = new FormData($('#vehicleForm')[0]);
                        saveForms.append('branch_master_id',$scope.memberFirmId);
                        saveForms.append('company_year_master_id' ,$scope.companyYearId);
                        saveForms.append('vehicle_type',1);
                        saveForms.append('vehicle_from',2);
                        saveForms.append('registration_date',AppUtilities.getDateFormat(new Date()));
                        AppService.postHttpFormRequest(savePath, saveForms)
                            .success(function(response) {
                                AppUtilities.unblockUI('#vehicleFormProcess');
                                $modalInstance.close();
                                $('#update_btn').attr('disabled', false);
                                $('#update_btn').html('Save');
                                if(typeof $scope.vehicleType !== "undefined" && $scope.vehicleType == 3) {
                                    $scope.getVehicleDetail.fuel_type = $("input[name='fule_type']:checked").val();
                                    $scope.getVehicleDetail.vehicle_start_method = $("input[name='vehicle_start_method']:checked").val();
                                    var manufacturerData = $scope.arrManufacturerData.find(function(element) { return element.mtranid == $scope.tmpVehicleForm.manufacturer_tran_id });
                                    if (typeof manufacturerData !== 'undefined') {
                                        $scope.getVehicleDetail.manufacturer = manufacturerData.name;
                                    }
                                    var vehicleTypeData = $scope.arrVehicleTypeData.find(function(element) { return element.id == $scope.tmpVehicleForm.vehicle_type_master_id });
                                    if (typeof vehicleTypeData !== 'undefined') {
                                        $scope.getVehicleDetail.vehicle_type = vehicleTypeData.name;
                                    }
                                    var vehicleModelData = $scope.arrVehicleModelData.find(function(element) { return element.id == $scope.tmpVehicleForm.vehicle_model_master_id });
                                    if (typeof vehicleModelData !== 'undefined') {
                                        $scope.getVehicleDetail.model = vehicleModelData.name;
                                    }
                                    $scope.getVehicleDetail.address = $scope.tmpVehicleForm.address;
                                    $scope.getVehicleDetail.chasis_number = $scope.tmpVehicleForm.chasis_number;
                                    $scope.getVehicleDetail.engine_number = $scope.tmpVehicleForm.engine_number;
                                    $scope.getVehicleDetail.fuel_type = $scope.tmpVehicleForm.fuel_type;
                                    $scope.getVehicleDetail.mobile_no = $scope.tmpVehicleForm.mobile_no;
                                    $scope.getVehicleDetail.vehicle_number = $scope.tmpVehicleForm.vehicle_number;
                                    $scope.getVehicleDetail.name = $scope.tmpVehicleForm.first_name + ' '+(typeof $scope.tmpVehicleForm.middle_name !== "undefined" && $scope.tmpVehicleForm.middle_name !== null ? $scope.tmpVehicleForm.middle_name : '') + ' '+(typeof $scope.tmpVehicleForm.last_name !== "undefined" && $scope.tmpVehicleForm.last_name !== null ? $scope.tmpVehicleForm.last_name : '');
                                    $scope.getVehicleDetail.unique_no = response.vehicle_code;
                                    $scope.getVehicleDetail.email_id = $scope.tmpVehicleForm.email_id;
                                    $scope.tmpVehicleForm.name = $scope.getVehicleDetail.name;
                                    $scope.bindScopeForm.in_house_vehicle = 0;
                                    $scope.bindScopeForm.customer_name = $scope.tmpVehicleForm.name;
                                    $scope.bindScopeForm.customer_mobile_no = $scope.tmpVehicleForm.mobile_no;
                                    $scope.bindScopeForm.customer_email_id = $scope.tmpVehicleForm.email_id;
                                    $scope.bindScopeForm.customer_address = $scope.tmpVehicleForm.address;
                                }
                                $scope.bindScopeForm.vehicle_name = response.vehicle_code;
                                $scope.bindScopeForm.vehicle_master_id = response.id;
                                AppUtilities.handleResponse({status:1,message:'Vehicle added successfully'});
                            }).error(function(response) {
                                AppUtilities.unblockUI('#vehicleFormProcess');
                                AppUtilities.handleResponse(response);
                                $('#update_btn').attr('disabled', false);
                                $('#update_btn').html('Save');
                            });
                    } else {
                        AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
                    }
                };
                $scope.cancelModal = function() {
                    $modalInstance.close();
                }
            }
        });
    };

    $scope.searchProductStockList = function(searchValue) {
        $scope.isSelectedProduct = false;
        $scope.arrProductData = [];
        $scope.spinner = 1;
        var path = $rootScope.ADMIN_API_URL + '/system/product_stock_list/'+$scope.memberFirmId;
        var params = {searchText:searchValue,'id':$scope.arrProductId};
        if(typeof $scope.productType !== "undefined") {
            if($scope.productType == 1) {
                params.stock_type = [1,3];
            } else if($scope.productType == 2) {
                params.stock_type = [2,3];
            } else if($scope.productType == 3) {
                params.stock_type = [1,2,3];
                params.product_type = 2;
            } else {}
        }
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrProductData = angular.copy(response.data);
            }).error(function() {
                $scope.spinner = 0;
            });
    };

    $scope.getProductDetail = function(index,$event) {
        if(typeof $scope.arrProductData[index] !== 'undefined' && $scope.arrProductData[index] !== null) {
            if($scope.arrProductData[index]['stock'] > 0) {
                $scope.productDetail = angular.copy($scope.arrProductData[index]);
                $scope.productDetail.price = AppUtilities.numberFormat($scope.productDetail.price,2);
                $scope.productFilter.product = $scope.arrProductData[index]['name'];
                $scope.productFilter.tax_group_master_id = angular.copy( $scope.productDetail.tax_group_master_id);
                $scope.productFilter.quantity = 1;
                $scope.stockQty = Number($scope.productDetail.stock);
                $scope.productFilter.price = $scope.productDetail.price;
                $scope.arrProductData = [];
                $scope.isSelectedProduct = true;
                $scope.productTaxDetail();
            } else{
                var msg ="This product -"+ $scope.arrProductData[index]['name'] +' is out of stock !';
                AppUtilities.toaster({type:3,message:msg});
            }
        }
        $event.stopPropagation();
    };

    $scope.productTaxDetail = function() {
        $scope.productDetail.tax = [];
        $scope.productDetail.taxname = '';
        if($scope.productFilter.tax_group_master_id !== null) {
            var taxGroupDetail = $scope.arrTaxGroupData.find(function(element){
                return element.id == $scope.productFilter.tax_group_master_id;
            });
            if(taxGroupDetail) {
                $scope.productDetail.taxname = taxGroupDetail.name;
                AppUtilities.blockUI(1);
                var path = $rootScope.ADMIN_API_URL + '/system/product_tax_detail/' + $scope.memberFirmId+'/'+$scope.productFilter.tax_group_master_id;
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        AppUtilities.unblockUI();
                        $scope.productDetail.tax = angular.copy(response.data);
                    }).error(function() {
                        AppUtilities.unblockUI();
                    });
            }
        }
    };

    $scope.saveProductStockDetail = function() {
        var quantity = Number($scope.productFilter.quantity);
        var discount =  ($scope.productFilter.discount > 0) ? Number($scope.productFilter.discount) : 0;
        var price  = AppUtilities.numberFormat($scope.productFilter.price,2);
        var discountAmt = AppUtilities.numberFormat((price * discount)/100,2);
        var itemPrice = AppUtilities.numberFormat(price - discountAmt,2);
        $scope.productDetail.quantity = quantity;
        $scope.productDetail.trans_type = 2;
        $scope.productDetail.discount = discount;
        $scope.productDetail.price = price;
        $scope.productDetail.subtotal = AppUtilities.numberFormat(itemPrice * quantity,2);
        $scope.productDetail.discount_amount = AppUtilities.numberFormat(discountAmt * quantity,2);
        $scope.productDetail.tax_group_master_id = $scope.productFilter.tax_group_master_id;
        var totalTax = 0;
        if($scope.productDetail.tax.length > 0) {
            angular.forEach($scope.productDetail.tax,function(tax,index) {
                tax.price = itemPrice;
                var taxAmount = $scope.calculateTax(tax);
                var itemTax = (taxAmount * quantity);
                totalTax += itemTax;
                $scope.productDetail.tax[index]['amount'] = taxAmount;
                $scope.productDetail.tax[index]['total_tax'] = itemTax;
            });
        }
        $scope.productDetail.total = AppUtilities.numberFormat($scope.productDetail.subtotal + totalTax,2);
        $scope.saveTmpProductStock($scope.productDetail);
        $scope.productRow.push($scope.productDetail);
        $scope.productDetail = {};
        $scope.productFilter = {};
        $scope.calculation();
    };

    $scope.calculateTax = function(tax) {
        var taxAmount = 0;
        if(typeof tax !== 'undefined' && Object.keys(tax).length > 0) {
            var price = (tax.price > 0) ? AppUtilities.numberFormat(tax.price,2) : 0;
            var value = (tax.value > 0) ? AppUtilities.numberFormat(tax.value,2) : 0;
            var type = tax.type;
            if(type == 1) {
                value = (value > 100) ? 100 : value;
                taxAmount = AppUtilities.numberFormat((value * price) / 100,2);
            } else {
                value = (value > price) ? price : value;
                taxAmount = AppUtilities.numberFormat( (price - value),2);
            }
        }
        return taxAmount;
    };

    $scope.calculation = function() {
        $scope.arrTaxDetail = [];
        $scope.arrProductId = [];
        var totalTax = 0;
        var totalDiscount = 0;
        var subTotalAmt = 0;
        var totalAmt = 0;
        if($scope.productRow.length > 0) {
            angular.forEach($scope.productRow, function(rows,index) {
                $scope.arrProductId.push(rows.product_variant_tran_id);
                var quantity = Number(rows.quantity);
                totalDiscount += rows.discount_amount;
                subTotalAmt += rows.subtotal;
                totalAmt += rows.total;
                var productTax = 0;
                if(rows.tax.length > 0) {
                    angular.forEach(rows.tax,function(taxes,taxKey){
                        var itemTax = AppUtilities.numberFormat(taxes.amount * quantity,2);
                        totalTax += itemTax;
                        productTax += itemTax;
                        var taxIndex = $scope.arrTaxDetail.findIndex(function(element){
                            return element.id == taxes.id;
                        });

                        if(taxIndex !== -1) {
                            $scope.arrTaxDetail[taxIndex]['price'] = AppUtilities.numberFormat($scope.arrTaxDetail[taxIndex]['price'] + itemTax,2);
                        } else {
                            $scope.arrTaxDetail.push({
                                id:taxes.id,
                                name:taxes.name,
                                price:itemTax
                            });
                        }
                    });
                }
                $scope.productRow[index]['item_tax'] = AppUtilities.numberFormat(productTax,2);
            });
        }
        $scope.summary.tax = AppUtilities.numberFormat(totalTax,2);
        $scope.summary.discount = AppUtilities.numberFormat(totalDiscount,2);
        $scope.summary.subtotal = AppUtilities.numberFormat(subTotalAmt,2);
        $scope.summary.total = AppUtilities.numberFormat(totalAmt,2);
        $scope.bindScopeForm.amount = $scope.summary.total;
        $scope.bindScopeForm.actual_amount = $scope.summary.total;
        //console.log('summary :',$scope.summary);
    };

    $scope.saveTmpProductStock = function(getProductDetail) {
        AppUtilities.blockUI(1);
        var params = {
            id : getProductDetail.product_variant_tran_id,
            quantity:getProductDetail.quantity,
            branch_master_id:$scope.memberFirmId,
            company_year_master_id:$scope.companyYearId,
            trans_type:(typeof getProductDetail.trans_type === "undefined") ? 2 : getProductDetail.trans_type
        };
        getProductDetail.company_year_master_id = $scope.companyYearId;
        getProductDetail.branch_master_id = $scope.memberFirmId;
        var savePath = $rootScope.ADMIN_API_URL + '/invoice_masters/save_tmp_stock';
        AppService.postHttpRequest(savePath, params)
                .success(function(response) {
                    AppUtilities.unblockUI();
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                });
    };

    $scope.searchServiceDetail = function(searchValue = '') {
        $scope.isSelectedService = false;
        $scope.arrServiceData = [];
        $scope.spinner = 1;
        var path = $rootScope.ADMIN_API_URL + '/system/service_list/'+$scope.memberFirmId;
        var params = {searchText:searchValue,'id':$scope.arrServiceId};
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrServiceData = angular.copy(response.data);
            }).error(function() {
                $scope.spinner = 0;
            });
    };

    $scope.getServiceDetail = function(index,$event) {
        if(typeof $scope.arrServiceData[index] !== 'undefined' && $scope.arrServiceData[index] !== null) {
            $scope.serviceDetail = angular.copy($scope.arrServiceData[index]);
            $scope.serviceDetail.price = AppUtilities.numberFormat($scope.serviceDetail.price,2);
            $scope.serviceFilter.service = $scope.arrServiceData[index]['name'];
            $scope.serviceFilter.price = $scope.serviceDetail.price;
            $scope.serviceDetail.subtotal = $scope.serviceFilter.price;
            $scope.serviceDetail.total = $scope.serviceFilter.price;
            $scope.arrServiceData = [];
            $scope.arrServiceId.push($scope.serviceDetail.id)
            $scope.isSelectedService = true;
        }
        $event.stopPropagation();
    };

    $scope.saveServiceDetail = function() {
        $scope.serviceRow.push($scope.serviceDetail);
        $scope.serviceDetail = {};
        $scope.serviceFilter = {};
        $scope.calculateService();
    };

    $scope.calculateService = function() {
        var subTotal = 0,total = 0,coupon = 0;
        $scope.arrServiceId = [];
        if($scope.serviceRow.length > 0) {
            var isAppliedCoupon = 0;
            if(typeof $scope.vehicleCouponDetail.id !== "undefined" && $scope.vehicleCouponDetail.id !== null) {
                $scope.vehicleCouponDetail.price = 100;
                $scope.vehicleCouponDetail.type = 1;
                isAppliedCoupon = 1;
                $scope.services.coupon_detail = angular.copy($scope.vehicleCouponDetail);
            }
            angular.forEach($scope.serviceRow, function(service,index) {
                $scope.arrServiceId.push(service.id);
                var price = AppUtilities.numberFormat(service.price,2);
                subTotal +=  price;
                if(isAppliedCoupon == 1) {
                    coupon += price;
                    total += 0;
                } else {
                    total += price;
                }
            });
        }
        $scope.services.subtotal = AppUtilities.numberFormat(subTotal,2);
        $scope.services.total = AppUtilities.numberFormat(total,2);
        $scope.services.coupon = AppUtilities.numberFormat(coupon,2);
    };

    $scope.searchProduct = function(searchValue,type = 1) {
        $scope.isSelectedProduct = false;
        $scope.arrProductData = [];
        $scope.spinner = 1;
        var path = $rootScope.ADMIN_API_URL + '/system/product_list/'+$scope.memberFirmId;
        var params = {searchText:searchValue};
        if(type == 2) {
            params.type = 2;
        }
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrProductData = angular.copy(response.data);
                console.log('$scope.arrProductData :',$scope.arrProductData);
            }).error(function() {
                $scope.spinner = 0;
            });
    };
    
    $scope.selectedProductDetail = function(index,$event) {
        if(typeof $scope.arrProductData[index] !== 'undefined' && $scope.arrProductData[index] !== null) {
            $scope.productDetail = angular.copy($scope.arrProductData[index]);
            $scope.bindScopeForm.product = $scope.arrProductData[index]['name'];
            $scope.productFilter.product = $scope.arrProductData[index]['name'];
            $scope.productFilter.price = AppUtilities.numberFormat($scope.arrProductData[index]['sale_price'],2);
            $scope.productFilter.quantity = 1;
            
            $scope.arrProductData = [];
            $scope.isSelectedProduct = true;
        }
        $event.stopPropagation();
    };

    $scope.saveProductDetail = function(type = 2) {
        $scope.productDetail.trans_type = 1;
        $scope.productDetail.quantity = Number($scope.productFilter.quantity);
        $scope.productDetail.price = AppUtilities.numberFormat($scope.productFilter.price,2);
        $scope.productDetail.subtotal = $scope.productFilter.price;
        $scope.productDetail.total = $scope.productFilter.price;
        $scope.productRow.push($scope.productDetail);
        $scope.saveTmpProductStock($scope.productDetail);
        $scope.productDetail = {};
        $scope.productFilter = {};
    };

    $scope.searchExpenseHeadList = function(searchTxt = '') {
        $scope.arrExpensesData = [];
        $scope.isSelectedExpense = false;
        $scope.spinner = 1;
        var path = $rootScope.ADMIN_API_URL + '/system/search_expense_head';
        var params = {search:searchTxt,branch_master_id:$scope.memberFirmId,expense_head_master_id : $scope.arrExpeseHeadId};
        AppService.postHttpRequest(path,params)
            .success(function(response) {
                $scope.spinner = 0;
                $scope.arrExpensesData = angular.copy(response.data);
            }).error(function() {
                $scope.spinner = 0;
            });
    };

    $scope.getExpenseHeadDetail = function($index,$event) {
        $scope.isSelectedExpense = false;
        if(typeof $scope.arrExpensesData[$index] !== "undefined" && $scope.arrExpensesData[$index] !== null) {
            $scope.expenseHeadDetail = $scope.arrExpensesData[$index];
            $scope.expenseFilter.expense_head = $scope.arrExpensesData[$index]['name'];
            $scope.isSelectedExpense = true;
            $scope.arrExpensesData = [];
        }
        $event.stopPropagation();
    };

    $scope.saveExpenseHeadDetail = function() {
        $scope.expenseHeadDetail.amount = AppUtilities.numberFormat($scope.expenseFilter.price,2);
        $scope.expenseHeadDetail.description = (typeof $scope.expenseFilter.description !== "undefined") ? $scope.expenseFilter.description : '';
        $scope.expenseHeadDetail.is_delete = 1;
        $scope.expenseRow.push($scope.expenseHeadDetail);
        console.log('expenseHeadDetail',$scope.expenseHeadDetail);
        $scope.expenseHeadDetail = {};
        $scope.expenseFilter = {};
        $scope.expenseCalculation();
    };

    $scope.getVehicleServiceExpenseDetail = function() {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_service_detail/'+$scope.bindScopeForm.vehicle_master_id+'/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                console.log('response :',response);
                var expenses = angular.copy(response.data);
                $scope.bindScopeForm.invoice_id = expenses.invoice_id;
                expenses.is_delete = 0;
                $scope.expenseRow.push(expenses);
                $scope.expenseCalculation();
                AppUtilities.unblockUI();
            })
            .error(function(response){
                AppUtilities.unblockUI();
            });
    };

    $scope.getVehicleExpenseDetail = function() {
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_expense_detail/'+$scope.bindScopeForm.vehicle_master_id+'/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                console.log('response :',response);
                $scope.expenseRow = angular.copy(response.data.expense_head);
                $scope.bindScopeForm.amount = angular.copy(response.data.total);
                $scope.bindScopeForm.actual_amount = angular.copy(response.data.total);
                AppUtilities.unblockUI();
            })
            .error(function(response){
                AppUtilities.unblockUI();
            });
    };

    $scope.deleteExpenseRecord = function(rows,$index) {
        var text = 'Do you want to delete this expense head ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                $scope.expenseRow.splice($index, 1);
                $scope.expenseCalculation();
                AppUtilities.unblockUI();
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.expenseCalculation = function() {
        $scope.arrExpeseHeadId = [];
        var total = 0;
        if($scope.expenseRow.length > 0) {
            angular.forEach($scope.expenseRow,function(rows,key){
                $scope.arrExpeseHeadId.push(rows.id);
                total += AppUtilities.numberFormat(rows.amount,2);
            });
        }
        $scope.expenseTotal = AppUtilities.numberFormat(total,2);
    };

    $scope.lockReceipt = function(id = 0) {
        var text = 'Do you want to locak the invoice ?';
        AppUtilities.confirmBox(text)
            .success(function(data) {
                AppUtilities.blockUI(1);
                var savePath = $scope.statePath + '/lock_invoice/'+id;
                AppService.getHttpRequest(savePath)
                        .success(function(response) {
                            AppUtilities.unblockUI();
                            console.log('response :',response);
                            AppUtilities.handleResponse(response);
                            $state.go($scope.baseStateTo, {}, { reload: true });
                        })
                        .error(function(response) {
                            AppUtilities.unblockUI();
                            AppUtilities.handleResponse(response);
                        });
            }).error(function(response) {
                console.log(response);
            });
    };

    $scope.calculateAmt  = function(type = 1) {
        $scope.showShortFees = false;
        console.log('type is :',type);
        console.log('bindscropt : ',$scope.bindScopeForm);
        $scope.bindScopeForm.amount = ($scope.bindScopeForm.amount !== undefined) ? $scope.bindScopeForm.amount : "";
        if($scope.bindScopeForm.amount < $scope.bindScopeForm.actual_amount) {
            $scope.showShortFees = true;
            $scope.bindScopeForm.shortFees = AppUtilities.numberFormat($scope.bindScopeForm.actual_amount - $scope.bindScopeForm.amount,2);
        } else {
            $scope.bindScopeForm.amount = $scope.bindScopeForm.actual_amount;
            $scope.bindScopeForm.shortFees = 0;
        }
    };

    $scope.paymentForm = function(id) {
        AppUtilities.blockUI(1);
        $scope.arrPaymentModeData = [{ id: 1, name: 'Cash' },{ id: 2, name: 'Cheque' }, { id: 3, name: 'Debit Card/Credit Card' }, { id: 4, name: 'Net Banking' }, { id: 5, name: 'UPI' }];
        AppService.getHttpRequest($scope.statePath + '/payment_detail/' + id)
                .success(function(response) {
                    AppUtilities.unblockUI();
                    $scope.getBankList(0);
                    $scope.payment_mode = 1;
                    $scope.invoice_master_id = id;
                    var pdate = AppUtilities.getDateFormat(new Date());
                    $scope.payment_date = pdate;
                    $scope.payment_mode_date = pdate;
                    $scope.ptotalAmount = AppUtilities.numberFormat(response.data.amount,2);
                    $scope.partAmount = AppUtilities.numberFormat(response.data.part_amount,2);
                    $scope.totalDueAmount = AppUtilities.numberFormat(($scope.ptotalAmount - $scope.partAmount),2);
                    $scope.paid = $scope.totalDueAmount;
                    var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/general_form/payment_form.html';
                    $scope.modalInstance = $modal.open({
                        animation:true,
                        templateUrl: url,
                        backdrop:'static',
                        keyboard:false,
                        windowClass: 'modal',
                        size: 'lg',
                        scope: $scope,
                        controller:function($scope,$modalInstance) {
                            $scope.savePayment = function() {
                                if ($scope.paymentFormSubmission.$valid) {
                                    $('#update_btn').attr('disabled', true);
                                    $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
                                    var savePath = $scope.statePath + '/save_payment';
                                    var saveForms = new FormData($('#paymentFormSubmission')[0]);
                                    saveForms.append('total_amount',$scope.ptotalAmount);
                                    saveForms.append('id',$scope.invoice_master_id);
                                    AppService.postHttpFormRequest(savePath, saveForms)
                                        .success(function(response) {
                                            AppUtilities.unblockUI();
                                            $('#update_btn').attr('disabled', false);
                                            $('#update_btn').html('Save');
                                            $scope.modalInstance.close();
                                            $state.go($scope.baseStateTo, {}, { reload: true });
                                        })
                                        .error(function(response) {
                                            $('#update_btn').attr('disabled', false);
                                            $('#update_btn').html('Save');
                                            AppUtilities.unblockUI();
                                            AppUtilities.handleResponse(response);
                                        });
                                } else {
                                    AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
                                }
                            };
                            $scope.cancelModal = function() {
                                $modalInstance.close();
                            },
                            $scope.calculateAmt = function() {
                                var total = AppUtilities.numberFormat($scope.totalDueAmount,2);
                                var paid = AppUtilities.numberFormat($scope.paid,2);
                                $scope.shortFees = 0;
                                if(total > paid) {
                                    $scope.shortFees = AppUtilities.numberFormat(total - paid,2);
                                } else {
                                    $scope.paid = total;
                                }
                            }
                        }
                    });
                })
                .error(function(response) {
                    AppUtilities.unblockUI();
                    AppUtilities.handleResponse(response);
                    $state.go($scope.baseStateTo);
                });
    };

    $scope.printInvoice = function(id = 0,type = 1) {
        type = Number(type);
        AppUtilities.blockUI(1);
        var path = $rootScope.ADMIN_API_URL + '/invoice_masters/print_invoice/' + type + '/' + id + '/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                try {
                    if(type === 5) {
                        var receiptDetail = angular.copy(response.data);
                        var invoice = {
                            expense:JSON.parse(receiptDetail.invoice_json)
                        };
                        delete receiptDetail.invoice_json;
                        receiptDetail = Object.assign({},receiptDetail,invoice);
                        localStorage.setItem('arrReceiptData',JSON.stringify(receiptDetail));
                        localStorage.setItem('caption','Invoice');
                        console.log('type is :',type);
                    } else {
                        var receiptDetail = angular.copy(response.data);
                        var invoice = JSON.parse(receiptDetail.invoice_json);
                        delete receiptDetail.invoice_json;
                        receiptDetail = Object.assign({},receiptDetail,invoice);
                        localStorage.setItem('arrReceiptData',JSON.stringify(receiptDetail));
                        localStorage.setItem('caption','Invoice');
                        console.log('type is :',type);
                    }
                    
                    if(type === 1) {
                        localStorage.setItem('fileName','purchase_receipt.html');
                    } else if(type === 2) {
                        localStorage.setItem('fileName','sale_receipt.html');
                    } else if(type === 3) {
                        localStorage.setItem('fileName','vehicle_job_invoice.html');
                    } else if(type === 4) {
                        localStorage.setItem('fileName','coupon_invoice.html');
                    } else if(type === 5) {
                        localStorage.setItem('fileName','vehicle_sale_receipt.html');
                    } else {}

                    var path =  $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/receipt/receipt.html';
                    $scope.reportWindow = $window.open(path,'SaleReceipt','height=890,width=1280,resizable=1,location=no');
                } catch(e) {
                    AppUtilities.handleResponse({status:0,message:e.toString()});
                }
                AppUtilities.unblockUI();
            })
            .error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
            });
    };

    $scope.VehicleStatusForm = function(vehicleRows) {
        console.log('vehicleRows :',vehicleRows);
        $scope.getAgentList(0);
        $scope.getDocumentList(0);
        $scope.arrVehicleStatusData = [{id:1,name:'Assign Agent'},{id:2,name:'In-Progress'},{id:3,name:'Hold'},{id:4,name:'Completed'}];
        $scope.bindScopeForm = vehicleRows;
        $scope.bindScopeForm.current_vehicle_status = Number(vehicleRows.status) + 1;
        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/general_form/vehicle_status_form.html';
        $scope.modalInstance = $modal.open({
            animation:true,
            templateUrl: url,
            backdrop:'static',
            keyboard:false,
            windowClass: 'modal',
            size: 'lg',
            scope: $scope,
            controller:function($scope,$modalInstance) {
                $scope.saveVehicleStatus = function() {
                    if ($scope.vehicleStatusForm.$valid) {
                        $('#update_btn').attr('disabled', true);
                        $('#update_btn').html('<i class ="fa fa-circle-o-notch fa-spin"></i> Processing....');
                        var savePath = $scope.statePath + '/save_vehicle_sale_status';
                        var saveForms = new FormData($('#vehicleStatusForm')[0]);
                        saveForms.append('id',$scope.bindScopeForm.id);
                        var agentDetail = $scope.arrAgentData.find(function(element){
                            return (element.id  == $scope.bindScopeForm.agent_master_id);
                        });
                        if(typeof agentDetail !== "undefined") {
                            saveForms.append('agent_name',agentDetail.name);
                        }
                        AppService.postHttpFormRequest(savePath, saveForms)
                            .success(function(response) {
                                AppUtilities.unblockUI();
                                $('#update_btn').attr('disabled', false);
                                $('#update_btn').html('Save');
                                $scope.modalInstance.close();
                                $state.go($scope.baseStateTo, {}, { reload: true });
                            })
                            .error(function(response) {
                                $('#update_btn').attr('disabled', false);
                                $('#update_btn').html('Save');
                                AppUtilities.unblockUI();
                                AppUtilities.handleResponse(response);
                            });
                    } else {
                        AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
                    }
                };
                $scope.cancelModal = function() {
                    $modalInstance.close();
                };
            }
        });
    };

    $scope.getAgentList = function(flag = 0) {
        $scope.arrAgentData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_agent_list/' + $scope.memberFirmId;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrAgentData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.getDocumentList = function(flag = 0) {
        $scope.arrDocumentData = [];
        flag = (typeof flag !== 'undefined') ? flag : 0;
        var path = $rootScope.ADMIN_API_URL + '/system/get_document_list/' + $scope.memberFirmId;
        if (flag == 1) {
            AppUtilities.blockUI(1);
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
                $scope.arrDocumentData = angular.copy(response.data);
            }).error(function() {
                if (flag == 1) {
                    AppUtilities.unblockUI();
                }
            });
    };

    $scope.customerForm = function() {
        AppUtilities.blockUI(1);
        $scope.saveBtn = {name:'Save',is_disabled :false};
        $scope.getTitleList(0);
        var url = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.COMPONENT + '/templates/general_form/customer_form.html';
        $modal.open({
            templateUrl: url,
            size: 'lg',
            scope: $scope,
            controller:function($scope, $modalInstance) {
                $scope.saveCustomer = function(form) {
                    if (form.$valid && $scope.memberFirmId > 0) {
                        AppUtilities.blockUI(1,'#customerLoadingDiv');
                        $scope.submitBtn.name = '<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...';
                        $scope.submitBtn.is_disabled = true;
                        var savePath = $rootScope.ADMIN_API_URL + '/' + 'customer_masters/save';
                        var saveForms = new FormData($('#customerSubmission')[0]);
                        saveForms.append('branch_master_id',$scope.memberFirmId);
                        AppService.postHttpFormRequest(savePath, saveForms)
                            .success(function(response) {
                                $modalInstance.close();
                                var titleDetail = $scope.arrTitleData.find(function(element){
                                    return (element.id = $scope.bindCustomerForm.title_master_id);
                                });
                                var title = '';
                                if(typeof titleDetail !== "undefined") {
                                    title = titleDetail.name;
                                }
                                var name = title +' '+$scope.bindCustomerForm.first_name + ' '+ (typeof $scope.bindCustomerForm.middle_name !== "undefined" ? $scope.bindCustomerForm.middle_name : '') + ' '+ (typeof $scope.bindCustomerForm.last_name !== "undefined" ? $scope.bindCustomerForm.last_name : '');
                                $scope.bindScopeForm.customer_name = name;
                                $scope.bindScopeForm.customer_master_id = response.id;
                                AppUtilities.handleResponse({status:1,message:'Customer added successfully !.'});
                                $scope.submitBtn.name = 'Save';
                                $scope.submitBtn.is_disabled = false;
                                AppUtilities.unblockUI('#customerLoadingDiv');
                            }).error(function(response) {
                                $scope.submitBtn.name = 'Save';
                                $scope.submitBtn.is_disabled = false;
                                AppUtilities.handleResponse(response);
                                AppUtilities.unblockUI('#customerLoadingDiv');
                            });
                    } else {
                        AppUtilities.handleResponse({ status: 0, message: 'Please filled up manadatory fields !.' });
                    }
                };
                $scope.canelModal = function() {
                    $modalInstance.close();
                } 
            }
        });
        AppUtilities.unblockUI();
    };

    $scope.searchCustomerDetail = function(searchTxt = '') {
        $scope.customer_spinner = 1;
        $scope.bindScopeForm.customer_master_id = null;
        if($scope.memberFirmId !== null) {
            var path = $rootScope.ADMIN_API_URL + '/system/search_customer';
            var params = {branch_master_id:$scope.memberFirmId,search_text:searchTxt};
            AppService.postHttpRequest(path,params)
                .success(function(response) {
                    $scope.customer_spinner = 0;
                    $scope.arrCustomerData = angular.copy(response.data);
                }).error(function() {
                    $scope.customer_spinner = 0;
                });
        } else {
            $scope.customer_spinner =  0;
        }
    };

    $scope.getCustomerData = function(index,$event) {
        if(typeof $scope.arrCustomerData[index] !== "undefined" && $scope.arrCustomerData[index] !== null) {
            $scope.bindScopeForm.customer_name = $scope.arrCustomerData[index]['name'];
            $scope.bindScopeForm.customer_master_id = $scope.arrCustomerData[index]['id'];
            $scope.arrCustomerData = [];
        }
        $event.stopPropagation();
    };

    $scope.getTitleList = function(flag = 0) {
        AppUtilities.blockUI(1,'#titleDiv');
        $scope.arrTitleData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_title_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTitleData = angular.copy(response.data);
                AppUtilities.unblockUI('#titleDiv');
            })
            .error(function(response) {
                AppUtilities.unblockUI('#titleDiv');
            });
    };

    $scope.getCountryList = function(flag = 0) {
        AppUtilities.blockUI(1,'#countryDiv');
        $scope.arrCountryData = [];
        if(flag == 0) {
            $scope.arrStateData = [];
            $scope.arrCityData = [];
        }
        var path = $rootScope.ADMIN_API_URL + '/system/get_country_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI('#countryDiv');
                $scope.arrCountryData = response.data;
            }).error(function() {
                AppUtilities.unblockUI('#countryDiv');
                $scope.bindScopeForm.country_master_id = null;
            })
    };

    $scope.getStateList = function(flag = 0) {
        AppUtilities.blockUI(1,'#stateDiv');
        $scope.arrStateData = [];
        if (typeof $scope.bindScopeForm.country_master_id !== 'undefined' && $scope.bindScopeForm.country_master_id !== null) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_state_list/'+$scope.bindScopeForm.country_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    AppUtilities.unblockUI('#stateDiv');
                    $scope.arrStateData = response.data;
                }).error(function(response) {
                    AppUtilities.unblockUI('#stateDiv');
                    $scope.arrCityData = [];
                    $scope.bindScopeForm.state_master_id = null;
                    $scope.bindScopeForm.city_master_id = null;
                });
        } else {
            AppUtilities.unblockUI('#stateDiv');
            $scope.arrCityData = [];
            $scope.bindScopeForm.state_master_id = null;
            $scope.bindScopeForm.city_master_id = null;
        }
    };

    $scope.getCityList = function(flag = 0) {
        AppUtilities.blockUI(1,'#cityDiv');
        $scope.arrCityData = [];
        if (typeof $scope.bindScopeForm.state_master_id !== 'undefined' && $scope.bindScopeForm.state_master_id !== null) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_city_list/'+$scope.bindScopeForm.state_master_id ;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    AppUtilities.unblockUI('#cityDiv');
                    $scope.arrCityData = response.data;
                }).error(function(response) {
                    AppUtilities.unblockUI('#cityDiv');
                    $scope.bindScopeForm.city_master_id = null;
                });
        } else {
            AppUtilities.unblockUI('#cityDiv');
            $scope.bindScopeForm.city_master_id = null;
        }
    };

    $scope.getRoyaltyChargeList = function(flag = 0) {
        AppUtilities.blockUI(1,'#royaltyDiv');
        $scope.arrRoyaltyChargeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_royalty_charge_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrRoyaltyChargeData = angular.copy(response.data);
                    AppUtilities.unblockUI('#royaltyDiv');
            }).error(function(response) {
                    AppUtilities.unblockUI('#royaltyDiv');
            });
    };

    $scope.getMemberTypeList = function(type = 0) {
        AppUtilities.blockUI(1,'#memberDiv');
        $scope.arrMemberTypeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_franchise_type_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                AppUtilities.unblockUI('#memberDiv');
                $scope.arrMemberTypeData = angular.copy(response.data);
                if(type == 1) {
                    localStorage.setItem('arrMemberType',JSON.stringify($scope.arrMemberTypeData));
                    $scope.arrSearchMemberTypeData = angular.copy(response.data);
                }
            }).error(function(response) {
                AppUtilities.unblockUI('#memberDiv');
            });
    };

    $scope.getMemberCode = function(type = 1) {
        AppUtilities.blockUI(1);
        $scope.memberCode = null;
        var path;
        if(type == 1) {
            path = $rootScope.ADMIN_API_URL + '/system/member_unique_no';
        }
        AppService.getHttpRequest(path)
            .success(function(response) {
                if(response.code != '') {
                    $scope.memberCode = response.code;
                } else {
                    AppUtilities.handleResponse({status:0,message:'Member Code setting is not defined, please add member code first!.'});
                    $state.go($scope.baseStateTo, {}, { reload: true });
                }
                AppUtilities.unblockUI();
            }).error(function(response) {
                AppUtilities.unblockUI();
                AppUtilities.handleResponse(response);
                $state.go($scope.baseStateTo, {}, { reload: true });
                $scope.memberCode = null;
            });
    };

    $scope.getCategoryList = function(flag = 0) {
        $scope.arrCategoryData = [];
        AppUtilities.blockUI(1,'#categoryDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_category_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCategoryData = angular.copy(response.data);
                AppUtilities.unblockUI('#categoryDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#categoryDiv');
            });
    };

    $scope.getTaxGroupList = function(flag = 0) {
        $scope.arrTaxGroupData = [];
        AppUtilities.blockUI(1,'#taxGroupDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_tax_group_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrTaxGroupData = angular.copy(response.data);
                AppUtilities.unblockUI('#taxGroupDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#taxGroupDiv');
            });
    };

    $scope.getUnitList = function(flag = 0) {
        $scope.arrUnitData = [];
        AppUtilities.blockUI(1,'#unitDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_unit_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrUnitData = angular.copy(response.data);
                AppUtilities.unblockUI('#unitDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#unitDiv');
            });
    };

    $scope.getVehicleTypeList = function(flag = 0) {
        $scope.arrVehicleTypeData = [];
        AppUtilities.blockUI(1,'#vehicleTypeDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_type_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrVehicleTypeData = angular.copy(response.data);
                AppUtilities.unblockUI('#vehicleTypeDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#vehicleTypeDiv');
                $scope.arrManufacturerData = [];
                $scope.bindScopeForm.manufacturer_tran_id = null;
            });
    };

    $scope.getManufacturerList = function(flag = 0,type = 2) {
        $scope.arrManufacturerData = [];
        type = Number(type);
        if(type === 1) {
            if(typeof $scope.bindScopeForm.vehicle_type_master_id !== 'undefined' && $scope.bindScopeForm.vehicle_type_master_id > 0) {
                AppUtilities.blockUI(1,'#manufacturerDiv');
                var path = $rootScope.ADMIN_API_URL + '/system/get_manufacturer_vehicle_list/'+$scope.bindScopeForm.vehicle_type_master_id;
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        $scope.arrManufacturerData = angular.copy(response.data);
                        AppUtilities.unblockUI('#manufacturerDiv');
                    }).error(function(response) {
                        AppUtilities.unblockUI('#manufacturerDiv');
                        $scope.arrVehicleModelData = [];
                        $scope.bindScopeForm.vehicle_model_master_id = null;
                        $scope.bindScopeForm.manufacturer_tran_id = null;
                    });
            } else {
                $scope.arrVehicleModelData = [];
                $scope.bindScopeForm.vehicle_model_master_id = null;
                $scope.bindScopeForm.manufacturer_tran_id = null;
            }
        } else {
            if(typeof $scope.tmpVehicleForm.vehicle_type_master_id !== 'undefined' && $scope.tmpVehicleForm.vehicle_type_master_id > 0) {
                AppUtilities.blockUI(1,'#manufacturerDiv');
                var path = $rootScope.ADMIN_API_URL + '/system/get_manufacturer_vehicle_list/'+$scope.tmpVehicleForm.vehicle_type_master_id;
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        $scope.arrManufacturerData = angular.copy(response.data);
                        AppUtilities.unblockUI('#manufacturerDiv');
                    }).error(function(response) {
                        $scope.arrVehicleModelData = [];
                        $scope.tmpVehicleForm.vehicle_model_master_id = null;
                        $scope.tmpVehicleForm.manufacturer_tran_id = null;
                        AppUtilities.unblockUI('#manufacturerDiv');
                    });
            } else {
                $scope.arrVehicleModelData = [];
                $scope.tmpVehicleForm.vehicle_model_master_id = null;
                $scope.tmpVehicleForm.manufacturer_tran_id = null;
            }
        }
    };

    $scope.getVehicleModelList = function(flag = 0,type = 2) {
        $scope.arrVehicleModelData = [];
        type = Number(type);
        if(type === 1) {
            if (typeof $scope.bindScopeForm.manufacturer_tran_id !== 'undefined' && $scope.bindScopeForm.manufacturer_tran_id > 0) {
                AppUtilities.blockUI(1,'#modelDiv');
                var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_model_list/'+$scope.bindScopeForm.manufacturer_tran_id;
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        $scope.arrVehicleModelData = angular.copy(response.data);
                        AppUtilities.unblockUI('#modelDiv');
                    }).error(function() {
                        $scope.bindScopeForm.vehicle_model_master_id = null;
                        AppUtilities.unblockUI('#modelDiv');
                    })
            } else {
                $scope.bindScopeForm.vehicle_model_master_id = null;
            }
        } else {
            if (typeof $scope.tmpVehicleForm.manufacturer_tran_id !== 'undefined' && $scope.tmpVehicleForm.manufacturer_tran_id > 0) {
                AppUtilities.blockUI(1,'#modelDiv');
                var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_model_list/'+$scope.tmpVehicleForm.manufacturer_tran_id;
                AppService.getHttpRequest(path)
                    .success(function(response) {
                        $scope.arrVehicleModelData = angular.copy(response.data);
                        AppUtilities.unblockUI('#modelDiv');
                    }).error(function() {
                        $scope.tmpVehicleForm.vehicle_model_master_id = null;
                        AppUtilities.unblockUI('#modelDiv');
                    })
            } else {
                $scope.tmpVehicleForm.vehicle_model_master_id = null;
            }
        }
    };

    $scope.getVariantList = function(flag = 0) {
        AppUtilities.blockUI(1,'#variantDiv');
        $scope.arrVariantData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_variant_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrVariantData = angular.copy(response.data);
                AppUtilities.unblockUI('#variantDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#variantDiv');
            });
    };

    $scope.getGradeList = function(flag = 0) {
        AppUtilities.blockUI(1,'#gradeDiv');
        $scope.arrGradeData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_grade_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrGradeData = angular.copy(response.data);
                AppUtilities.unblockUI('#gradeDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#gradeDiv');
            });
    };
    
    $scope.vehicleSpecificationList = function() {
        $scope.arrVehicleSpecificationData = [];
        if (typeof $scope.bindScopeForm.vehicle_type_master_id !== 'undefined' && $scope.bindScopeForm.vehicle_type_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_vehicle_specification_list/'+$scope.bindScopeForm.vehicle_type_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrVehicleSpecificationData = angular.copy(response.data);
                }).error(function(response) {
                    $scope.arrVehicleSpecificationData = [];
                    return false;
                });
        }
    };

    $scope.getCouponList = function(flag = 0,type = 1) {
        $scope.arrCouponData = [];
        AppUtilities.blockUI(1,'#couponDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_coupon_detail/'+$scope.memberFirmId+'/' + type;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCouponData = angular.copy(response.data);
                AppUtilities.unblockUI('#couponDiv');
            }).error(function() {
                AppUtilities.unblockUI('#couponDiv');
            });
    };

    $scope.getDepartmentList = function(flag = 0) {
        AppUtilities.blockUI(1,'#departmentDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_department_list/' + $scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrDepartmentData = angular.copy(response.data);
                AppUtilities.unblockUI('#departmentDiv');
            }).error(function(response) {
                AppUtilities.unblockUI('#departmentDiv');
            });
    };

    $scope.getDesignationList = function(flag = 0) {
        AppUtilities.blockUI(1,'#designationDiv');
        $scope.arrDesignationData = [];
        if (typeof $scope.bindScopeForm.department_master_id !== 'undefined' && $scope.bindScopeForm.department_master_id > 0) {
            var path = $rootScope.ADMIN_API_URL + '/system/get_designation_list/' + $scope.memberFirmId + '/' +$scope.bindScopeForm.department_master_id;
            AppService.getHttpRequest(path)
                .success(function(response) {
                    $scope.arrDesignationData = angular.copy(response.data);
                    AppUtilities.unblockUI('#designationDiv');
                }).error(function() {
                    $scope.bindScopeForm.designation_master_id = '';
                    AppUtilities.unblockUI('#designationDiv');
                });
        } else {
            AppUtilities.unblockUI('#designationDiv');
            $scope.bindScopeForm.designation_master_id = '';
        }
    };

    $scope.getBankList = function(flag = 0) {
        $scope.arrBankData = [];
        AppUtilities.blockUI(1,'#bankDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/get_bank_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBankData = angular.copy(response.data);
                AppUtilities.unblockUI('#bankDiv');
            }).error(function() {
                AppUtilities.unblockUI('#bankDiv');
            });
    };

    $scope.getSupplierList = function(flag = 0) {
        $scope.arrSupplierData = [];
        AppUtilities.blockUI(1,'#supplierDiv');
        var path = $rootScope.ADMIN_API_URL + '/system/supplier_list/'+$scope.memberFirmId;
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrSupplierData = angular.copy(response.data);
                AppUtilities.unblockUI('#supplierDiv');
            }).error(function() {
                AppUtilities.unblockUI('#supplierDiv');
            });
    };

    $scope.getBranchList = function() {
        $scope.arrBranchData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_branch_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrBranchData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };

    $scope.getCompanyYearList = function() {
        $scope.arrCompnayYearData = [];
        var path = $rootScope.ADMIN_API_URL + '/system/get_company_year_list';
        AppService.getHttpRequest(path)
            .success(function(response) {
                $scope.arrCompnayYearData = angular.copy(response.data);
            })
            .error(function(response) {
            });
    };
}]);
