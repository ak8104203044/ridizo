"use strict";
var CommonComponent = {
    allowOnlyNumeric: function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    },
    priceValidate: function(Obj, e) {
        if (e.which == 46) {
            if (Obj.value.indexOf('.') != -1) {
                return false;
            }
        }
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    }
};