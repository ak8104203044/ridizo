'use strict';
var $stateProviderRef = null;
var $urlRouterProviderRef = null;
//var radizoApp = angular.module('radizoApp', ['ui.router', 'oc.lazyLoad', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'rt.select2']);
var radizoApp = angular.module('radizoApp', ['ui.router', 'oc.lazyLoad', 'ui.bootstrap', 'ngSanitize', 'rt.select2','ckeditor','jcs-autoValidate','angularUtils.directives.dirPagination']);
/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
radizoApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
radizoApp.config(['$controllerProvider', function($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/* Setup global settings */
radizoApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };
    $rootScope.settings = settings;
    return settings;
}]);

/* Setup App Main Controller */
radizoApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Metronic.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);

/* Setup Layout Part - Sidebar */
/*
radizoApp.controller('sidebarController', ['$scope', '$state', function($scope, $state) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar($state.current.name); // init sidebar
    });
}]);
*/

/* Setup Layout Part - Footer */
radizoApp.controller('footerController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Layout Part - Header */
radizoApp.controller('headerController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

radizoApp.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$locationProvider', 'GLOBAL_DIR',
    function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $locationProvider, GLOBAL_DIR, ) {
        // any unknown URLS go to 404
        $urlRouterProvider.deferIntercept();
        $urlRouterProvider.otherwise('/login');
        $stateProviderRef = $stateProvider;
        $urlRouterProviderRef = $urlRouterProvider;
        /*$locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });*/
    }
]);

/* Setup global settings */
radizoApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };
    $rootScope.settings = settings;
    return settings;
}]);

radizoApp.run(['$state', '$rootScope', '$location', '$urlRouter', '$stateParams', '$http','settings', 'GLOBAL_DIR','bootstrap3ElementModifier','validator','defaultErrorMessageResolver',
    function($state, $rootScope, $location, $urlRouter, $stateParams, $http,settings, GLOBAL_DIR,bootstrap3ElementModifier,validator,defaultErrorMessageResolver) {
        console.log('global settings run !....');
        var path = $location.protocol() + '://' + $location.host() + '/' + GLOBAL_DIR.FOLDERNAME + '/' + GLOBAL_DIR.MODULES_FOLDER + '/router.json';
        $rootScope.ADMIN_API_URL = $location.protocol() + '://' + $location.host() + '/' + GLOBAL_DIR.FOLDERNAME + '/' + GLOBAL_DIR.WEB_API_FOLDER;
        $rootScope.PROJECT_URL = $location.protocol() + '://' + $location.host() + '/' + GLOBAL_DIR.FOLDERNAME;
        $rootScope.IMAGE_URL = $rootScope.PROJECT_URL + '/' + GLOBAL_DIR.UPLOAD_ROOT_DIR;
        $rootScope.btnGroup = {btn1:'Submit',btn1_color:'green',btn2:'Submit & New',btn2_color:'green',btn_reset:'Cancel',reset_color:'default',is_disabled :false,status:1,processing:'<i class ="fa fa-circle-o-notch fa-spin"></i> Processing...'};
        bootstrap3ElementModifier.enableValidationStateIcons(true);
        validator.setValidElementStyling(false);
        
        defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
            errorMessages.patternNumberMessage = 'Please enter a valid Order No';
            errorMessages.validPincodeMessage = 'Please enter a valid pincode no';
            errorMessages.validMobileMessage = 'Please enter a valid mobile no';
            errorMessages.validAdhaarMessage = 'Please enter a valid adhaar no';
            errorMessages.validServiceMessage = 'Service K.M. must be number';
            errorMessages.validNumberMessage = 'Please enter a valid number';
            errorMessages.patternQuantityMessage = 'Please enter valid quantity';
            errorMessages.validUserName = 'Please enter valid name';
            errorMessages.validNamePattern = 'Please enter character only';
            errorMessages.validNameDotPattern = 'Please enter character,dots & quotes only';
            errorMessages.confirmPwdMessage = 'Confirm password must be same the new password';
        });


        $http.get(path, { cache: false })
            .success(function(data) {
                if (data !== null && data.length > 0) {
                    angular.forEach(data, function(stateConfig, key) {
                        var getExistingState = $state.get(stateConfig.name)
                        if (getExistingState !== null) {
                            return;
                        }
                        var stateName = stateConfig.name;
                        var setStateConfig = {
                            url: stateConfig.url,
                            cache: false,
                            data: { pageTitle: stateConfig.title,stateName:stateConfig.state,module:stateConfig.module },
                            controller: stateConfig.controller,
                            params: { id: null }
                        };
                        if (stateConfig.is_parent === true) {
                            setStateConfig.views = { 'sidebar_menu': { templateUrl: $rootScope.PROJECT_URL + '/' + stateConfig.template_url } };
                        } else {
                            setStateConfig.templateUrl = $rootScope.PROJECT_URL + '/' + stateConfig.template_url;
                        }
                        if (stateConfig.files.length > 0) {
                            var count;
                            for (count = 0; count < stateConfig.files.length; count++) {
                                stateConfig.files[count] = $rootScope.PROJECT_URL + '/' + stateConfig.files[count];
                            }
                            setStateConfig.resolve = {
                                desp: ['$ocLazyLoad', function($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'radizoApp',
                                        insertBefore: '#ng_load_plugins_before',
                                        files: stateConfig.files
                                    });
                                }]
                            };
                        }
                        $stateProviderRef.state(stateName, setStateConfig);
                    });
                }
                $urlRouter.sync();
                $urlRouter.listen();
            });

        /** set global $htpp header settings **/

        $http.defaults.headers.common['Accept'] = 'application/json; charset=UTF-8';
        $http.defaults.headers.common['Content-Type'] = 'application/json';
        if(!$http.defaults.headers.get) {
            $http.defaults.headers.get = {};
        }
        $http.defaults.headers.get['Cache-Control'] = 'no-cache';
        $http.defaults.headers.get['Pragma'] = 'no-cache';

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            $('.tooltips').tooltip('hide');
            if (toState.name === 'login') {
                localStorage.clear();
                $http.defaults.headers.common['app-secret-key'] = 'aa29169dfe9065546ebab7d345ec7f3654654dfskuydnkld';
                $http.defaults.headers.common['app-api-key'] = 'DYhG93b0qyJfIxfs2guVoUubWwvniR2G0FgaC9miJHuarids57834Ikdi';
                $http.defaults.headers.common['app-access-token'] = '63905628562308423786583240823235689234364834343';
                $rootScope.bodyclass = '';
                $rootScope.bodylayout = 'page-md login';
                $.backstretch([
                    "./assets/admin/pages/media/bg/1.jpg",
                    "./assets/admin/pages/media/bg/2.jpg",
                    "./assets/admin/pages/media/bg/3.jpg",
                    "./assets/admin/pages/media/bg/4.jpg"
                ], {
                    fade: 1000,
                    duration: 8000
                });
            } else {
                $rootScope.portletForm = 'blue';
                if (localStorage.getItem('userSessId') != null && localStorage.getItem('userId') > 0) {
                    $http.defaults.headers.common['app-sessid'] = localStorage.getItem('userSessId');
                    $http.defaults.headers.common['app-token-id'] = localStorage.getItem('userTokenId');
                    if (toState.name === 'dashboard') {
                        $state.go('dashboard.leatherboard');
                        $.backstretch('destroy');
                    }
                    $rootScope.bodylayout = 'page-header-fixed page-sidebar-closed-hide-logo page-quick-sidebar-over-content';
                } else {
                    $state.go('login');
                }
            }
        });
    }
]);
