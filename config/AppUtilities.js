'use strict';
radizoApp.factory('AppUtilities', function($q,$state,$http,GLOBAL_DIR) {
    var appUtilities = {};

    appUtilities.notificationAlert = function(params) {
        var type = (typeof params.type !== 'undefined' && params.type !== null) ? Number(params.type) : 1;
        var message = (typeof params.message !== 'undefined') ? params.message : '';
        var notification = '';
        if (type === 1) {
            // error
            notification = '<div class="alert alert-danger">';;
            notification += '<button class="close" data-close="alert"></button>';
            notification += '<span>' + message + '</span>';
            notification += '</div>';
        } else if (type === 2) {
            //success
            notification = '<div class="alert alert-success">';;
            notification += '<button class="close" data-close="alert"></button>';
            notification += '<span>' + message + '</span>';
            notification += '</div>';
        } else if (type === 3) {
            //info
            notification = '<div class="alert alert-info">';;
            notification += '<button class="close" data-close="alert"></button>';
            notification += '<span>' + message + '</span>';
            notification += '</div>';
        }
        $('notification-alert').html(notification);
    };

    appUtilities.blockUI = function(blockType = 1,target = '') {
        blockType = (typeof blockType !== 'undefined') ? Number(blockType) : 1;
        var params = {};
        if(blockType == 1) {
            params['animate'] = true;
        } else {
            params['boxed'] = true;
        }
        if(target != '') {
            params['target'] = target;
        }
        Metronic.blockUI(params);
    };

    appUtilities.unblockUI = function(target = '') {
        if(target != '') {
            Metronic.unblockUI(target);
        } else {
            Metronic.unblockUI();
        }
    };

    appUtilities.toaster = function(params) {
        var toastType = (typeof params.type !== 'undefined' && params.type !== null) ? Number(params.type) : 1;
        var message = (typeof params.message !== 'undefined') ? params.message : '';
        if (toastType === 1) {
            toastr.success(message, GLOBAL_DIR.PROMPT_MSG);
        } else if (toastType === 2) {
            toastr.error(message, GLOBAL_DIR.PROMPT_MSG);
        } else if (toastType === 3) {
            toastr.warning(message, GLOBAL_DIR.PROMPT_MSG);
        } else {
            toastr.info(message, GLOBAL_DIR.PROMPT_MSG);
        }
    };

    appUtilities.confirmDeletion = function(type = 1) {
        var promptMsg,dialogMsg;
        if(type == 2) {
            promptMsg = 'Please select any one record to cancel invoice';
            dialogMsg = 'Do you want to cancel this invoice ?';
        } else {
            promptMsg = 'Please select any one record to delete';
            dialogMsg = 'Do you want to delete this records ?';
        }
        //appUtilities.playAudio();
        var deferred = $q.defer();
        var promise = deferred.promise;
        var isConfirm = false;
        if ($('.selectAllCheckData:checked').length == 0) {
            bootbox.alert({
                title: GLOBAL_DIR.PROMPT_MSG +' Says',
                message: "<i class='fa-lg fa fa-warning' style='color:#b83a3e'></i> <b>"+promptMsg+"</b>",
                closeButton: false
            });
            deferred.reject(isConfirm);
        } else {
            bootbox.dialog({
                message: "<i class='fa-lg fa fa-warning' style='color:#b83a3e'></i> <b>"+dialogMsg+"</b>",
                title: GLOBAL_DIR.PROMPT_MSG +' Says',
                closeButton: false,
                buttons: {
                    success: {
                        label: "OK",
                        className: "green",
                        callback: function(result) {
                            isConfirm = true;
                            var arrDeleteData = [];
                            $('.selectAllCheckData').each(function() {
                                if ($(this).is(':checked') === true) {
                                    arrDeleteData.push($(this).val());
                                }
                            });
                            deferred.resolve(arrDeleteData);
                        }
                    },
                    danger: {
                        label: "Cancel",
                        className: "red",
                        callback: function() {
                            isConfirm = false;
                            deferred.reject(isConfirm);
                        }
                    }
                }
            });
        }
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };

    appUtilities.selectionBox = function(text = '') {
        text = (text != '') ? text : 'Please select one record to continue the operation !'
        var deferred = $q.defer();
        var promise = deferred.promise;
        var getId = 0;
        if ($('.selectAllCheckData:checked').length == 0 || $('.selectAllCheckData:checked').length > 1) {
            bootbox.alert({
                title: GLOBAL_DIR.PROMPT_MSG +' Says',
                message: "<i class='fa-lg fa fa-warning' style='color:#b83a3e'></i> <b>"+ text +"</b>",
                closeButton: false
            });
            deferred.reject(0);
        } else {
            $("input.selectAllCheckData:checked").each(function() {
                getId = $(this).val();
            });
            deferred.resolve(getId);
        }
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };

    appUtilities.export = function(headers, records, fileName, exportType) {
        exportType = (typeof exportType !== 'undefined') ? Number(exportType) : 1;
        fileName = (typeof fileName !== 'undefined') ? fileName : 'default';
        if (exportType === 1) {
            fileName = fileName + '.xlsx';
            var getRecord,hKey;
            var excelRecords = [],items;
            for (getRecord of records) {
                items = {};
                for (hKey in headers) {
                    items[headers[hKey]] = getRecord[hKey];
                }
                excelRecords.push(items);
            }
            alasql('SELECT * INTO XLSX("' + fileName + '",{headers:true}) FROM ?', [excelRecords]);
        } else if (exportType === 2) {
            if (Object.keys(records).length > 0) {
                var hKey;
                var rows = [],data;;
                for (getRecord of records) {
                    var headerKey;
                    data = [];
                    for (headerKey in headers) {
                        data.push(getRecord[headerKey]);
                    }
                    rows.push(data);
                }
            }
            var columns = Object.values(headers);
            var doc = new jsPDF();
            doc.autoTable(columns, rows);
            doc.save(fileName + '.pdf');
        } else {
        }
    };

    appUtilities.confirmBox = function(text) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        bootbox.dialog({
            message: "<i class='fa-lg fa fa-warning' style='color:#b83a3e'></i><b> " + text + "</b>",
            title:GLOBAL_DIR.PROMPT_MSG +' Says',
            closeButton: false,
            buttons: {
                success: {
                    label: "OK",
                    className: "green",
                    callback: function(result) {
                        deferred.resolve(true);
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "red",
                    callback: function() {
                        deferred.reject(false);
                    }
                }
            }
        });
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };

    appUtilities.roleRightsQuery = function(arrRoleRightsData, arrRoleOperationData, arrRights) {
        var roleBuildHtml = '';
        if (arrRoleRightsData.length > 0) {
            roleBuildHtml += '<div class="table-scrollable">';
            roleBuildHtml += '<table class="table-view table table-striped table-bordered table-advance table-hover">';
            roleBuildHtml += '<thead>';
            roleBuildHtml += '<tr>';
            roleBuildHtml += '<th class="table-head" style="width:25%;">Sub Menu</th>';
            roleBuildHtml += '<th class="table-head" style="width:5%;">&nbsp;</th>';
            if (arrRoleOperationData.length > 0) {
                var arrTypeData = [],operation;
                for (operation of arrRoleOperationData) {
                    arrTypeData.push(operation.TypeTran.id);
                    roleBuildHtml += '<th class="table-head" style="width:10%;text-align:center;">' + operation.TypeTran.name + '</th>';
                }
                roleBuildHtml += '<input type="hidden" id="operationId" value="'+arrTypeData.join(',')+'"/>';
            }
            roleBuildHtml += '</tr>';
            roleBuildHtml += '</thead>';
            roleBuildHtml += '<tbody>';
            var roleData;
            for (roleData of arrRoleRightsData) {
                roleBuildHtml += '<tr>';
                var roleLinkMasterId = roleData.RoleLinkMaster.id;
                roleBuildHtml += '<th class="table-head" nowrap>' + roleData.RoleLinkMaster.name + '</th>';
                roleBuildHtml += '<th class="table-head" style="text-align:center;"><input style="width:18px;height:18px;" onclick="roleChecked(this,1,'+roleLinkMasterId+',0)" class="roleCheckClass" type="checkbox"/></th>';
                for (operation of arrRoleOperationData) {
                    var typeTranId = operation.TypeTran.id;
                    var isRightsChecked = (typeof arrRights[roleLinkMasterId] !== 'undefined' && arrRights[roleLinkMasterId][typeTranId] !== 'undefined' && arrRights[roleLinkMasterId][typeTranId] > 0) ? "checked='true'" : '';
                    roleBuildHtml += '<th class="table-head" style="text-align:center;"><input style="width:18px;height:18px;" onclick="roleChecked(this,1,'+roleLinkMasterId+','+typeTranId+')" class="roleCheckClass '+roleLinkMasterId+typeTranId+'_menuRights  '+roleLinkMasterId+typeTranId+'_roleRights" type="checkbox" ' + isRightsChecked + ' name = "rights[' + roleData.RoleLinkMaster.id + '][' + operation.TypeTran.id + ']" value="' + operation.TypeTran.id + '"/></th>';
                }
                if (roleData.children.length > 0) {
                    var arrRoleId = [];
                    arrRoleId.push(roleLinkMasterId);
                    var spaces = '&nbsp;&nbsp;';
                    roleBuildHtml += appUtilities.roleRinkChild(roleData.children, arrRoleOperationData, arrRights,arrRoleId,spaces);
                }
                roleBuildHtml += '</tr>';
            }
            roleBuildHtml += '</tbody>';
            roleBuildHtml += '</table>';
            roleBuildHtml += '</div>';
        }
        return roleBuildHtml;
    };

    appUtilities.roleRinkChild = function(arrChildRoleData, arrRoleOperationData, arrRights,arrRoleId,spaces) {
        var childBuildHtml = '';
        if (arrChildRoleData.length > 0) {
            var roleData;
            for (roleData of arrChildRoleData) {
                var childLength = roleData.children.length;
                var roleLinkMasterId = roleData.RoleLinkMaster.id;
                var getRoleId = arrRoleId.join(',');
                childBuildHtml += '<tr>';
                var bold ='';
                if(childLength > 0) {
                    bold = 'font-weight:bold;';
                }
                childBuildHtml += '<td class="table-content" style="font-size:13px !important;'+bold+'">' +spaces+ roleData.RoleLinkMaster.name + '</td>';
                childBuildHtml += '<td class="table-content" style="font-size:13px !important;text-align:center;"><input type="checkbox" data-roleid = "'+getRoleId+'" class="roleCheckClass" onclick="roleChecked(this,0,'+roleLinkMasterId+',0)" style="width:18px;height:18px;"/></td>';
                var operation,j;
                for (operation of arrRoleOperationData) {
                    var typeTranId = operation.TypeTran.id;
                    var appendChildClass = '';
                    for(j = 0;j<arrRoleId.length;j++) {
                        appendChildClass += ' '+ arrRoleId[j]+typeTranId+'_menuRights';
                    }
                    var parentClass = '';
                    if(childLength > 0) {
                        parentClass = roleLinkMasterId+typeTranId+'_roleRights';
                    }
                    var isRightsChecked = (typeof arrRights[roleLinkMasterId] !== 'undefined' && arrRights[roleLinkMasterId][typeTranId] !== 'undefined' && arrRights[roleLinkMasterId][typeTranId] > 0) ? "checked='true'" : '';
                    childBuildHtml += '<td class="table-content" style="font-size:13px !important;text-align:center;"><input style="width:18px;height:18px;" data-roleid = "'+getRoleId+'" class="roleCheckClass '+appendChildClass+' '+roleLinkMasterId+typeTranId+'_menuRights  '+parentClass+'" onclick="roleChecked(this,0,'+roleLinkMasterId+','+typeTranId+')" type="checkbox" ' + isRightsChecked + ' name = "rights[' + roleData.RoleLinkMaster.id + '][' + operation.TypeTran.id + ']" value="' + operation.TypeTran.id + '"/></td>';
                }
                childBuildHtml += '</tr>';
                if (childLength > 0) {
                    spaces += '&nbsp;&nbsp;';
                    arrRoleId.push(roleLinkMasterId);
                    childBuildHtml += appUtilities.roleRinkChild(roleData.children, arrRoleOperationData, arrRights,arrRoleId,spaces);
                }
            }
        }
        return childBuildHtml;
    };

    appUtilities.setOrderRightsQuery = function(arrRoleRightsData) {
        var roleBuildHtml = '';
        if (arrRoleRightsData.length > 0) {
            roleBuildHtml += '<div class="table-scrollable">';
            roleBuildHtml += '<table class="table-view table table-striped table-bordered table-advance table-hover">';
            roleBuildHtml += '<thead>';
            roleBuildHtml += '<tr>';
            roleBuildHtml += '<th class="table-head" style="width:33%;">Sub Menu</th>';
            roleBuildHtml += '<th class="table-head" style="width:33%;">Icon</th>';
            roleBuildHtml += '<th class="table-head" style="width:33%;">Order</th>';
            roleBuildHtml += '</tr>';
            roleBuildHtml += '</thead>';
            roleBuildHtml += '<tbody>';
            var roleData;
            for (roleData of arrRoleRightsData) {
                roleBuildHtml += '<tr>';
                var roleLinkMasterId = roleData.RoleLinkMaster.id;
                var iconName = (roleData.RoleLinkMaster.icon != '' && roleData.RoleLinkMaster.icon != null) ? roleData.RoleLinkMaster.icon : '';
                var orderNo = (roleData.RoleLinkMaster.order_no > 0) ? roleData.RoleLinkMaster.order_no : '';
                roleBuildHtml += '<th class="table-head" nowrap>' + roleData.RoleLinkMaster.name + '<input type="hidden" name ="role_master_id[]" value ="'+roleLinkMasterId+'"/></th>';
                roleBuildHtml += '<th class="table-head" style="text-align:center;"><input type="text" class="form-control" name = "icon[]" value ="'+iconName+'"/></th>';
                roleBuildHtml += '<th class="table-head" style="text-align:center;"><input type="text" class="form-control" name = "order[]" value ="'+orderNo+'"/></th>';
                if (roleData.children.length > 0) {
                    var spaces = '&nbsp;&nbsp;';
                    roleBuildHtml += appUtilities.setOrderChildQuery(roleData.children,spaces);
                }
                roleBuildHtml += '</tr>';
            }
            roleBuildHtml += '</tbody>';
            roleBuildHtml += '</table>';
            roleBuildHtml += '</div>';
        }
        return roleBuildHtml;
    };

    appUtilities.setOrderChildQuery = function(arrChildRoleData,spaces) {
        var childBuildHtml = '';
        if (arrChildRoleData.length > 0) {
            var roleData;
            for (roleData of arrChildRoleData) {
                var childLength = roleData.children.length;
                var roleLinkMasterId = roleData.RoleLinkMaster.id;
                var iconName = (roleData.RoleLinkMaster.icon != '' && roleData.RoleLinkMaster.icon != null) ? roleData.RoleLinkMaster.icon : '';
                var orderNo = (roleData.RoleLinkMaster.order_no > 0) ? roleData.RoleLinkMaster.order_no : '';
                childBuildHtml += '<tr>';
                var bold ='';
                if(childLength > 0) {
                    bold = 'font-weight:bold;';
                }
                childBuildHtml += '<td class="table-content" style="font-size:13px !important;'+bold+'">'+ spaces + roleData.RoleLinkMaster.name + '<input type="hidden" name ="role_master_id[]" value ="'+roleLinkMasterId+'"/></td>';
                childBuildHtml += '<td class="table-content" style="font-size:13px !important;text-align:center;"><input type="text" name ="icon[]" value ="'+iconName+'"  class ="form-control"/></td>';
                childBuildHtml += '<td class="table-content" style="font-size:13px !important;text-align:center;"><input type="text" name ="order[]" value ="'+orderNo+'"  class ="form-control"/></td>';
                childBuildHtml += '</tr>';
                if (childLength > 0) {
                    spaces += '&nbsp;&nbsp;';
                    childBuildHtml += appUtilities.setOrderChildQuery(roleData.children,spaces);
                }
            }
        }
        return childBuildHtml;
    };

    appUtilities.handleResponse = function(response) {
        var params;
        if (response.status === 1) {
            params = { type: 1, message: response.message };
            appUtilities.toaster(params);
        } else if (response.status === 0) {
            params = { type: 2, message: response.message };
            appUtilities.toaster(params);
        } else {
            if(localStorage.getItem('userId') !== null) {
                localStorage.clear();
                params = { type: 2, message: 'Invalid authentication user,please login again!..' };
                appUtilities.toaster(params);
                $state.go('login');
            }
        }
    };

    appUtilities.uploadFiles = function(params) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        var fileName = params.getFile.value;
        var fileExt = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toString().toLowerCase();
        console.log('file ext. :', fileExt);
        var arrFileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        if (arrFileExtension.lastIndexOf(fileExt) == -1) {
            var response = { status: 0, message: 'please select valid image file type (format:.jpg,.jpeg,.png,.gif)!...' };
            deferred.reject(response);
        } else {
            var reader = new FileReader();
            reader.onload = function(event) {
                var formdata = new FormData();
                formdata.append('type', 1);
                formdata.append('file', params.getFile.files[0]);
                formdata.append('old_filename', params.old_file);
                $http({
                    url: params.path,
                    cache: true,
                    method: 'POST',
                    data: formdata,
                    transformRequest: angular.identity,
                    timeout: 10000,
                    headers: {
                        'Content-Type': undefined,
                        'Process-Data': false,
                        'radizo-sessid': localStorage.getItem('userSessId'),
                        'radizo-token-id': localStorage.getItem('userTokenId')
                    }
                }).success(function(data, status, headers, config) {
                    console.log('success :', data);
                    if (data.status === 1) {
                        deferred.resolve({ status: 1, src: reader.result, filename: data.file });
                    } else {
                        var params = (typeof data.status !== 'undefined') ? data : { message: 'Internal server error, please try again later !.', status: 0 };
                        deferred.reject(params);
                    }
                }).error(function(data, status, headers, config) {
                    var message = 'Internal server error, please try again later !.';
                    deferred.reject({ 'status': 0, 'message': message });
                });
            }
            reader.readAsDataURL(params.getFile.files[0]);
        }
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    };

    appUtilities.numberFormat = function(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? '' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            use_parse_float_flag = (typeof use_parse_float === 'undefined') ? '1' : use_parse_float,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        if (use_parse_float_flag == 1) {
            return parseFloat(s.join(dec), 10);
        } else {
            return s.join(dec);
        }
    };

    appUtilities.getDateFormat = function(getDate, format) {
        format = (typeof format !== 'undefined') ? format : 'dd-mm-yyyy';
        var msec = Date.parse(getDate);
        var currentTime = new Date(msec);
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        month = (month < 9) ? ('0' + month) : month;
        day = (day < 9) ? ('0' + day) : day;
        var date;
        switch (format) {
            case 'dd-mm-yyyy':
                date = day + '-' + month + '-' + year;
                break;
            case 'dd/mm/yyyy':
                date = day + '/' + month + '/' + year;
                break;
            default:
                date = day + '-' + month + '-' + year;
                break;
        }
        return date;
    }

    appUtilities.btnBehaviour = function(btn,type = 1) {
        if(type === 1) {
            if(btn.status === 1) {
                btn.temp = btn.btn1;
                btn.btn1 = btn.processing;
            } else {
                btn.temp = btn.btn2;
                btn.btn2 = btn.processing;
            }
            btn.is_disabled = true;
        } else {
            if(btn.status === 1) {
                btn.btn1 = btn.temp;
            } else {
                btn.btn2 = btn.temp;
            }
            btn.is_disabled = false;
        }
    };

    appUtilities.playAudio = function() {
        this.audio = new Audio('./sound/first.mp3');
        this.audio.type = "audio/mp3";
        this.audio.play();
        this.isplaying = true;
    };

    appUtilities.pauseAudio = function() {
        if(this.isplaying) {
            this.audio.pause();
        }
    };

    return appUtilities;
});
