var TableAjax = function() {

    var initPickers = function() {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function(apiPath, pageLength, arrNonSortable) {
        var grid = new Datatable();
        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
                $('.tooltips').tooltip({ html: true });
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": Number(pageLength), // default record count per page
                "columnDefs": [{ // set default column settings
                    'orderable': false,
                    'targets': arrNonSortable
                }, {
                    "searchable": true,
                    "targets": arrNonSortable
                }],
                "ajax": {
                    "url": apiPath, // ajax source
                },
                "order": [
                        [2, "asc"]
                    ] // set first column as a default sort by asc
            }
        });

        // handle group actionsubmit button click
        $('.table-group-action-submit').on('click', function(e) {
            e.preventDefault();
            var action = $("#searchField");
            var searchText = $('#searchText').val().trim();
            if (action.val() != "" && searchText != '') {
                $('.Metronic-alerts').remove();
                grid.setAjaxParam("searchType", "1");
                grid.setAjaxParam("searchFields", action.val());
                grid.setAjaxParam("searchText", searchText);
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                searchSlide();
                //grid.clearAjaxParams();
            } else if (action.val() == "") {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (searchText.val() == '') {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please Enter Text',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else {

            }
        });

        $('.reset-filter').click(function(e) {
            $('#searchField').val('');
            $('#searchText').val('');
            grid.setAjaxParam("searchType", "1");
            grid.setAjaxParam("searchFields", '');
            grid.setAjaxParam("searchText", '');
            grid.setAjaxParam("customActionType", "group_action");
            grid.setAjaxParam("customActionName", '');
            //grid.setAjaxParam("id", grid.getSelectedRows());
            grid.getDataTable().ajax.reload();
        });
    }

    return {
        //main function to initiate the module
        init: function(apiPath, pageLength, arrNonSortable) {
            //initPickers();
            handleRecords(apiPath, pageLength, arrNonSortable);
        }

    };

}();